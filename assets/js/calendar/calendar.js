import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';
import '@fullcalendar/timegrid/main.css';
import 'fullcalendar-year-view/dist/fullcalendar.css'

import {Calendar} from '@fullcalendar/core/main';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import frLocale from '@fullcalendar/core/locales/fr.js';
import 'fullcalendar-year-view/dist/fullcalendar.js';

$(document).ready(function () {
    let agenda = $("#fullCalendar");
    if (agenda[0].dataset.agendaIsYear === "true") {
        loadCalendar(agenda[0], true);
    } else {
        loadCalendar(agenda[0], false);
    }

});

function loadCalendar(agenda, isYearCalendar = false) {
    let date = new Date();
    let html = null;
    let agenda_type = agenda.dataset.agendaType;
    let agenda_tab_time_range_person = JSON.parse(agenda.dataset.agendaTimeRangePerson);
    let agenda_tab_natures_with_number_time_ranges = JSON.parse(agenda.dataset.agendaTabNaturesWithNumberTimeRanges);
    let agenda_number_alarm_this_day = 0;
    let defaultView = "dayGridMonth";

    for (let nature_label in agenda_tab_natures_with_number_time_ranges) {
        agenda_number_alarm_this_day += agenda_tab_natures_with_number_time_ranges[nature_label];
    }
    switch (agenda_type) {
        case 'staff':
            if (agenda_number_alarm_this_day !== 0) {
                let message = "<ul>";
                let nature_number;
                for (let nature_label in agenda_tab_natures_with_number_time_ranges) {
                    nature_number = agenda_tab_natures_with_number_time_ranges[nature_label];
                    if (nature_number > 0) {
                        message += "<li style='list-style-type: disc;color:inherit'><b>" + nature_number + "</b> " + nature_label + "</li>";
                    }
                }
                message += "</ul>";
                showNotification('top', 'center', "Rappels prévus pour aujourd'hui :", message);
            }
            $.ajax({
                url: "/core/timerange/nature/staff",
                type: "GET",
                dataType: "json",
                async: false,
                success: function (data) {
                    html = "<input type='hidden' name='event-nature' value='" + data.nature.key + "'>";
                    html += "<label>Description :</label><input type='text' class='form-control' name='event-description' max='255'>";
                },
                error: function () {
                    html = "<p>Vous n'êtes pas autorisé à choisir un type de période<p>";
                }
            });
            break;
        case 'personLegal':
        case 'personNatural':
            $.ajax({
                url: "/core/timerange/nature/person",
                type: "GET",
                dataType: "json",
                async: false,
                success: function (data) {
                    html = '<select id="select-timerange-nature" tabindex="-98">';
                    $.each(data.natures, function (key, value) {
                        html += '<option value="' + key + '">' + value + '</option>';
                    });
                    html += '</select>';
                },
                error: function () {
                    html = "<p>Vous n'êtes pas autorisé à choisir un type de période<p>";
                }
            });
            break;
    }

    let first_click_event = null;
    let agenda_id = agenda.dataset.agendaId;
    let def_id_tmp = null;
    let calendar = new Calendar(agenda, {
        defaultView: defaultView,
        selectable: agenda_type !== 'personLegal',
        eventSources: [
            {
                url: "/fc-load-events",
                method: "GET",
                extraParams: {
                    filters: JSON.stringify({'agendaId': agenda_id})
                },
                editable: true,
            },
        ],
        customButtons: {
            yearButton: {
                text: 'Année',
                click: function () {
                    window.location = "/core/agenda/annual/" + agenda_id + "/" + agenda_type;
                }
            }
        },
        minTime: '07:00:00',
        maxTime: '19:30:00',
        defaultDate: date,
        header: {
            left: 'title',
            center: 'yearButton,dayGridMonth,timeGridWeek,timeGridDay',
            right: 'prev,next,today',
        },
        plugins: [interactionPlugin, dayGridPlugin, timeGridPlugin],
        timeZone: 'UTC',
        eventLimit: true, // when too many events in a day, show the popover
        locale: frLocale,
        editable: true,
        droppable: true,
        eventRender: function (info) {
            if (info.event._def.defId !== def_id_tmp) {
                myEventRender(info.event, info.el);
            }
            def_id_tmp = info.event._def.defId;
        },
        eventDrop: function (info) {
            eventDragOrResize(info);
        },
        eventResize: function (info) {
            eventDragOrResize(info);
        },
        eventMouseEnter: function (mouseEnterInfo) {
            if (mouseEnterInfo.view.type === "dayGridMonth") {
                let element = mouseEnterInfo.el;
                let event_url_string = document.location + mouseEnterInfo.event.url;
                let event_url = new URL(event_url_string);
                let event_id = event_url.searchParams.get("id");

                let event_start = mouseEnterInfo.event.start.toLocaleString("fr-FR", {timeZone: 'UTC'});
                let event_end = event_start;
                if (mouseEnterInfo.event.end != null) {
                    event_end = mouseEnterInfo.event.end.toLocaleString("fr-FR", {timeZone: 'UTC'});
                }
                let event_object = agenda_tab_time_range_person[event_id];
                let client = "";
                if (event_object !== undefined) {
                    if (event_object.person.type === "natural") {
                        client = "<div>avec <strong>" + event_object.person.lastName + " " + event_object.person.firstName + "</strong></div>";
                    } else if (event_object.person.type === "legal") {
                        client = "<div>avec <strong>" + event_object.person.corporateName + "</strong></div>";
                    }
                }
                $(element).after("<div id='event-tooltip' class='event-tooltip' style='border-color:" + mouseEnterInfo.event.borderColor + ";display:none'><div>" + mouseEnterInfo.event.title + "</div><div><span class='font-weight-bold'>Du</span> <span class='font-italic'>" + event_start + "</span></div><div><span class='font-weight-bold'>au</span> <span class='font-italic'>" + event_end + "</span></div>" + client + "</div>");
                $('#event-tooltip').fadeIn(200);
            }
        },
        eventMouseLeave: function (mouseLeaveInfo) {
            if (mouseLeaveInfo.view.type === "dayGridMonth") {
                $("#event-tooltip").remove();
            }
        },
        select: function (event) {
            if (first_click_event == null) {
                first_click_event = event;
            } else {
                let options = {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                    timeZone: "UTC"
                };
                let swal_html;
                if (first_click_event.start.getTime() < event.end.getTime()) {
                    swal_html = '<h4>Du ' + first_click_event.start.toLocaleString('fr-FR', options) + ' au ' + event.end.toLocaleString('fr-FR', options) + '</h4>' + html;
                } else {
                    swal_html = '<h4>Du ' + event.start.toLocaleString('fr-FR', options) + ' au ' + first_click_event.end.toLocaleString('fr-FR', options) + '</h4>' + html;
                }
                swal({
                    title: 'Créer une période',
                    html: swal_html,
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    onOpen: () => {
                        callSelectPicker('#select-timerange-nature');
                    },
                    onClose: function () {
                        first_click_event = null;
                    }
                }).then(function () {
                    let event_data;
                    let select_time_range_value = "";
                    let selected_text = "";
                    let description = null;

                    if (agenda_type === "personNatural" || agenda_type === "personLegal") {
                        let select_time_range = $('#select-timerange-nature');
                        select_time_range_value = select_time_range.val();
                        selected_text = select_time_range.find('option:selected').html();
                    } else {
                        select_time_range_value = $("input[name='event-nature']").val();
                        description = $("input[name='event-description']").val();
                        if (description && description.length > 25) {
                            selected_text = description.substring(0, 25);
                            selected_text += "...";
                        } else {
                            selected_text = description;
                        }
                    }

                    if (select_time_range_value) {
                        if (first_click_event.start.getTime() > event.end.getTime()) {
                            event_data = {
                                title: selected_text,
                                start: event.start,
                                end: first_click_event.end,
                                textColor: 'white'
                            };
                        } else {
                            event_data = {
                                title: selected_text,
                                start: first_click_event.start,
                                end: event.end,
                                textColor: 'white'
                            };
                        }
                        ajaxCreateTimeRange(agenda_id, event_data, select_time_range_value, description, calendar, false);
                    }
                });
            }
        },
    });
    calendar.render();
}

function eventDragOrResize(info) {
    let new_evt_start = info.event.start;
    let new_evt_end = info.event.end;
    let url_string = document.location + info.event.url;
    let url = new URL(url_string);
    let id_time_range = url.searchParams.get("id");

    if (id_time_range === undefined) {
        swal({
            title: "Oups",
            text: "Vous ne pouvez pas modifier cet évènement tant qu'il n'est pas totalement créé !",
            type: "warning",
        });
    } else {
        // edit
        $.ajax({
            url: '/core/timerange/edit/' + id_time_range,
            type: "POST",
            dataType: "json",
            data: {
                start: new_evt_start.toLocaleString("fr-FR", {timeZone: 'UTC'}).replace(" à", ""),
                end: new_evt_end.toLocaleString("fr-FR", {timeZone: 'UTC'}).replace(" à", "")
            },
            async: true,
            error: function () {
                swal({
                    title: "Erreur",
                    text: "Une erreur est survenue lors de la modification de l'évènement. Rafraichissez la page ou réessayez plus tard.",
                    type: "error",
                });
            }
        });
    }
}

function formatDateYMD(date) {
    let new_date = new Date(date),
        month = '' + (new_date.getMonth() + 1),
        day = '' + new_date.getDate(),
        year = new_date.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function myEventRender(event, element) {
    let html_remove_btn = "<a class='event-remove-btn'><i class='tim-icons icon-simple-remove' style='color:" + event.textColor + "' title=\"supprimer l'évènement\"></i></a>";
    $(element).children("div").append(html_remove_btn);
    let remove_btn = $(element).find(".event-remove-btn");
    remove_btn.click(function (el) {
        el.preventDefault();
        if (!event.id) {
            swal({
                title: "Oups",
                text: "Vous ne pouvez pas supprimer cet évènement car il n'a pas d'indentifiant.",
                type: "warning",
            });
        } else {// delete
            remove_btn.html("<i class=\"fas fa-spinner spinner-icon\" style='color:" + event.textColor + "'></i>");
            $(element).addClass('deleting');
            $("#event-tooltip").hide();
            $.ajax({
                url: '/core/timerange/delete/' + event.id,
                type: "POST",
                dataType: "json",
                async: true,
                success: function () {
                    $(element).remove();
                    event.remove();
                    $("#event-tooltip").remove();
                },
                error: function () {
                    $(element).show();
                    remove_btn.html(html_remove_btn);
                    $("#event-tooltip").show();
                    swal({
                        title: "Erreur",
                        text: "Une erreur est survenue lors de la suppression de l'évènement. Rafraichissez la page ou réessayez plus tard.",
                        type: "error",
                    });
                },
                finally: function (){
                    $(element).removeClass('deleting');
                }
            });
        }
    });
}

function ajaxCreateTimeRange(agenda_id, event_data, select_time_range_value, description, calendar, isYearCalendar) {
    $.ajax({
        url: '/core/timerange/new/' + agenda_id,
        type: "POST",
        dataType: "json",
        data: {
            start: event_data.start.toLocaleString("fr-FR", {timeZone: 'UTC'}).replace(" à", ""),
            end: event_data.end.toLocaleString("fr-FR", {timeZone: 'UTC'}).replace(" à", ""),
            nature: select_time_range_value,
            description: description
        },
        async: true,
        success: function (result) {
            event_data = {
                ...event_data,
                ...result.eventData
            };
            if (isYearCalendar) {
                calendar.fullCalendar('renderEvent', event_data, true);
            } else {
                calendar.addEvent(event_data);
            }
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Oops...',
                text: "Vous n'êtes pas autorisé à créer une période"
            });
        }
    });
}