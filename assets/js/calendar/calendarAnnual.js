// contient un array du calendrier contenant des infos sur chaque mois ainsi que l'année (le nbr de jours dans le mois, la date de commencement, ...) selon la demande du client, si n +1 ou n - 1
// ex : l'utilisateur à cliquer sur le btn n + 1
// le array retourner va contenir toutes les infos de Juillet 2021 à Juin 2022 (le nbr de jours dans le mois, la date de commencement, ...)
// ces info reçu vont permettre de crée l'agenda complet dans le DOM
let array_calendar = $("#content-calendar-actual").data("calendarActual");
// agenda est un array d'objects de tous les mois présent dans le DOM
let month_tables = $('.agenda');
// sert a translate les mois anglais en fr
let month_trans = [
    {"month_en": "Jan", "month_fr": "Janvier"},
    {"month_en": "Feb", "month_fr": "Février"},
    {"month_en": "Mar", "month_fr": "Mars"},
    {"month_en": "Apr", "month_fr": "Avril"},
    {"month_en": "May", "month_fr": "Mai"},
    {"month_en": "Jun", "month_fr": "Juin"},
    {"month_en": "Jul", "month_fr": "Juillet"},
    {"month_en": "Aug", "month_fr": "Août"},
    {"month_en": "Sep", "month_fr": "Septembre"},
    {"month_en": "Oct", "month_fr": "Octobre"},
    {"month_en": "Nov", "month_fr": "Novembre"},
    {"month_en": "Dec", "month_fr": "Décembre"},
];
// donne la position du jour dans la semaine et translate le jour anglais en fr
let days_trans = [
    {"day_en": "Mon", "day_fr": "Lundi", "position": "1"},
    {"day_en": "Tue", "day_fr": "Mardi", "position": "2"},
    {"day_en": "Wed", "day_fr": "Mercredi", "position": "3"},
    {"day_en": "Thu", "day_fr": "Jeudi", "position": "4"},
    {"day_en": "Fri", "day_fr": "Vendredi", "position": "5"},
    {"day_en": "Sat", "day_fr": "Samedi", "position": "6"},
    {"day_en": "Sun", "day_fr": "Dimanche", "position": "7"},
];
// return la position du mois ( int )
// ex Janvier = 0, return donc 0
let position_month = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre"
];

function translateMonthAndDay(months) {
    for (let i = 0; i < months.length; i++) {
        let day = months[i].first_day_index + 1;
        let dateTranslated = translateDate(months[i].years + "-" + getMonthFromString(months[i].month) + "-01");
        months[i]["day_start"] = capitalize(dateTranslated.day_name);
        months[i]["month"] = capitalize(dateTranslated.month_name);
    }
    return months;
}

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getMonthFromString(month) {
    return new Date(Date.parse(month + " 1, 2000")).getMonth() + 1;
}

function translateDate(date) {
    let dateToTranslate = new Date(date);
    let translatedDate = new Intl.DateTimeFormat("fr", {
        month: 'long',
        weekday: 'long',
    }).formatToParts(dateToTranslate);
    return {
        month_name: translatedDate[0].value,
        day_name: translatedDate[2].value
    };
}

translateMonthAndDay(array_calendar);

function displayMonthGrid() {
    // est le header de chaque mois ou sera affiché son mois et son année
    // ex : Juillet 2020
    let month_titles = $('.title-month');
    for (let i = 0; i < month_titles.length; i++) {
        // display les mois dans le DOM avec l'année
        let month_title = $(month_titles[i]);
        // représente le text dans le header du calendrier ( le mois et l'année ex: Juillet 2021 )
        let month_title_text = array_calendar[i].month + " " + array_calendar[i].years;
        $(month_title).text(month_title_text);
    }
}

displayMonthGrid();

function CreateDomCalendar(array_calendar) {
    for (let k = 0; k < array_calendar.length; k++) {
        // crée un string pour récupérer un id plus tard d'un mois dans le calendrier
        // ex : Juillet-2020
        let month_years = array_calendar[k]['month'] + "-" + array_calendar[k]['years'];
        // récupère un object jquery qui correspond à chaque mois de l'année , cette object contient tous les semaines du mois
        $(month_tables[k]).attr('id', month_years);
        // ex pour le mois de Juillet 2020
        let month = $(month_tables[k]).children();
        // récup les semaines du mois
        let weeks = $(month).find('.agenda-week');
        // réprésente tous les jours des semaines
        let child_week = $(weeks).children();
        // récupère la position du premier jours du mois (ex: Lundi position 0);
        let day = parseInt(array_calendar[k].first_day_index);

        // boucle qui affiche les dates des jours dans le mois de chaque semaines
        //ex | Lundi | Mardi
        //   | 1     | 2
        // en gros ce code permet d'incorporer dans chaque bloc des mois leurs jours respectifs
        // on ne peut commencer n'importe ou dans le mois, chaque mois à sa date de de début ex le Juillet 2025 commence par un mardi
        // variable position_day_start va nous permettre de commencer à cette position dans le mois concerné une fois la boucle commencé celle ci va ensuite
        // Être incrémenté de 1 à chaque fois et va parcourir l'ensemble des jours dispo dans la variable  child_week
        // En gros : grâce a child_week qui contient tous les jours de semaines dans le mois on parcoure l'ensemble des jours
        // puis la variable days elle est le chiffre du jour, ex : le premier jour du mois est 1 ensuite 2 ensuite 3
        for (let day_number = 1; day_number < array_calendar[k].days + 1; day_number++) {
            // on ajoute le jour avec le jour de commencement
            $(child_week[day]).text(day_number);
            // add cursor pointer pour des futurs click dessus pour enregistrer des dates dans un form qui est dans une popup
            $(child_week[day]).addClass('cursor-pointer');
            day++;
        }
    }
}

CreateDomCalendar(array_calendar);

// Ceci a le même fonctionnement que le css hover
// ajoute une class css si le td contient une date
$(".agenda .agenda-week td").mouseover(function () {
    // si le tr (date) contient une date ces que le jour existe , les mois possèdent des cases vides des jours qui n'existes pas
    let exist = $(this).text().length;
    if (exist >= 1) {
        $(this).addClass('td-hover');
    }
});

// supprime la class css si le td contient une date, on enleve la class ajouté grâce au mouseover si cette date existe, les mois possèdent des cases vides des jours qui n'existes pas
$(".agenda .agenda-week td").mouseout(function () {
    // si le tr (date) contient une date ces que le jour existe
    let exist = $(this).text().length;
    if (exist >= 1) {
        $(this).removeClass('td-hover');
    }
});

// Popup add event
// Fonctionnement de 2 clicks aux totals, le 1er pour la date de début, le 2ème pour la date de fin puis affiche le pop-up avec les dates voulues
// va récuperer le nbr de click max 2 puis réinitialiser à 0
let click_event_calendar = 0;
// va contenir la date de debut et date de fin grâce au click event sur les date ci dessous (  $('.agenda td').click   )
let array_event_calendar = [];
// réprésente le txt dans le formulaire pour afficher une erreur ( si la date de fin est inférieur à la date de début )
let p_form_error = $("#error_date_calender_form");

// 2 modes d'ajout de date, soit pas le click sur un td (td est une date sur le calendrier ) soit par le changement dans le form directement dans une popup
// click sur la date choisie avec le td ( td = date )
$('.agenda td').click(function () {
    let day = $(this).text().padStart(2, '0');
    // on récupère l'id et l'année de la date choisie
    // ex: Juillet-2015
    let get_month_years = $(this).parent().parent().parent().attr('id');
    // récup juste sont année , ex : 2015
    let year = get_month_years.split('-')[1];
    // récup juste sont mois, cex : Juillet
    let month = get_month_years.split('-')[0];
    click_event_calendar++;

    // récup la position du mois concerné , + 1 car celle ci commence par 0
    let get_position_month = position_month.indexOf(month) + 1;

    get_position_month = get_position_month.toString().padStart(2, '0');

    // format de date complète pour l'incrémenter dans le input du formulaire qui sert à ajouter des evenements ( formulaire présent dans une popup )
    let txt_date = day + "/" + get_position_month + "/" + year;

    if (click_event_calendar === 1) {
        // récupère date début , 1er click
        $('#time_range_calendar_startDate').val(txt_date);
        array_event_calendar[0] = {
            "day_start": day,
            "month_start": get_position_month,
            "years": year
        };
    }
    if (click_event_calendar === 2) {
        // récupère date fin , 2ème click
        $('#time_range_calendar_endDate').val(txt_date);
        array_event_calendar[1] = {
            "day_end": day,
            "month_end": get_position_month,
            "years": year
        };
        // date de début
        let start = new Date(array_event_calendar[0].years + "-" + array_event_calendar[0].month_start + "-" + array_event_calendar[0].day_start);
        // date de fin
        let end = new Date(array_event_calendar[1].years + "-" + array_event_calendar[1].month_end + "-" + array_event_calendar[1].day_end);

        // p pour message d'erreur dans le DOM si la date de fin est inférieur à la date de début
        $(p_form_error).empty();
        if (start.getTime() > end.getTime()) {
            $(p_form_error).text("Erreur : La date de fin ne peut pas être inférieur à la date du début")
        }
        // ouvre la modal du formulaire
        $('#open_modal_btn').click();
        click_event_calendar = 0;
    }
});

// récupère les données du client concernant sont time_range
let time_range = $('#time-range-client').data("timeRange");

// recherche la position du mois dans le calendrier
// return un int (ex : Juillet = 0)
function searchPositionMonth(id_month) {
    for (let p = 0; p < $('.agenda').length; p++) {
        if (id_month === $('.agenda')[p]['id']) {
            return p;
        }
    }
}

// return un array de td de tous les jours dans un mois
function getAllDaysInMonth(td, week = null, timeRange = null) {
    // si week est défini
    // il nout faut sont time range pour récup la date complète de fin et de début, ce qui permet de  récup que les jours entres ceux des 2 dates
    // array qui va avoir tous les jours de chaque semaines d'un mois
    let array = [];
    // si week défini ( week concerne les client qui sont dispo selon une certaine demande par ex: le Lundi matin dispo , Mercredi aprèm dispo , ... )
    if (week === null) {
        // aucun week défini on passe tous les jours de chaque mois dans l'array
        for (let d = 0; d < td.length; d++) {
            if ($(td[d]).text().length > 0) {
                array.push(td[d]);
            }
        }
        return array;
    }

    let color = timeRange['color'];
    // on récup les semaines
    let semaines = $(td).parent();
    // on récup le mois et l'année ex: Juillet-2020
    let get_id_month_curent = $(semaines).parent().parent().attr('id');
    let substitutionCriterionTab = [];
    // formule magique prise dans wish controller
    for (let i = 0; i < 14; i++) {
        if ((week & (2 ** i)) !== 0) {
            substitutionCriterionTab[i] = 1;
        } else {
            substitutionCriterionTab[i] = 0;
        }
    }
    // va récupérer les jours de la semaine en fonction de leurs dispo pour chaque jour , matin , aprèm , ...
    // ex le tableau va ressembler à sa
    // [11,01,00,00,00,00,00]
    // Ici Le lundi est toute une journée , Mardi l'aprèm uniquement et les autres rien.
    let new_array = [];
    // h va récupérer dans le array substitutionCriterionTab la valeur suivante de p
    // ex ici quand la boucle commence la valeur substitutionCriterionTab[p] = 1, tandis que substitutionCriterionTab[p] = 0
    // [10,01,00,00,00,00,00]
    // avec cette méthode on peut voir dans la boucle ci-dessous que j'assemble une journée entière
    // ligne 282 :   let day = aprem + matin;
    // cela me permet de récupérer une journée entière de disponibilité d'un client
    let h = 0;
    // boucle sur le array de substitutionCriterionTab
    for (let p = 0; p < substitutionCriterionTab.length; p += 2) {
        h = p + 1;
        // récup l'aprem de chaque journée de la semaine , lundi , mardi , ... dimanche
        let afternoon = substitutionCriterionTab[p].toString();
        // récup le matin de chaque journée de la semaine , lundi , mardi , ... dimanche
        let morning = substitutionCriterionTab[h].toString();
        // assemble la journée avec sont matin et sont aprèm
        // ex [01]
        let day = afternoon + morning;
        new_array.push(day);
    }
    // maintenant notre variable new_array ressemble à un truc comme sa
    // [11,01,00,00,00,00,00]
    // elle contient pour chaque donnée, le jour ainsi que la dispo du matin ou l'aprèm
    for (let sp = 0; sp < semaines.length; sp++) {
        // récup chaque semaine du mois
        let semaine = $(semaines[sp]);
        // récup chaque jour de la semaine
        let day_td = $(semaine).children();
        // on check si le month et sont année existe ou non dans le DOM
        // ex id : Juillet-2021
        let get_month_by_id_start = timeRange['start_month_str'] + "-" + timeRange['start_years'];
        let get_month_by_id_end = timeRange['end_month_str'] + "-" + timeRange['end_years'];

        for (let dy = 0; dy < day_td.length; dy++) {
            // false on passe le jour au tableau, true on ne passe pas le jour au tableau car celui ci est trop grand par rapport à la date de fin du dernier mois
            let check = false;
            // récupère le jour en entier ex: 25
            // si le jour dépasse la date de fin du dernier mois ne pas ajouter le td (date) en cours dans la variable array qui contient tous les jours à colorier
            // pareille pour le début du mois, la date ne doit pas être inférieur à celle du start_day (date de début)
            let get_int_day = parseInt($(day_td[dy]).text());
            // arrive au dernier mois
            // va checker si la date en cours donc day_td[dy] n'est pas plus grand que la denier date du timerange
            if (get_month_by_id_end === get_id_month_curent) {
                if (get_int_day > parseInt(timeRange['end_day'])) {
                    check = true;
                }
            }
            // arrive au début du mois
            // va checker si la date en cours donc day_td[dy] n'est pas plus petite que la première date du timerange
            if (get_month_by_id_start === get_id_month_curent) {
                if (get_int_day < parseInt(timeRange['start_day'])) {
                    check = true;
                }
            }
            if (!check) {
                // si la date est juste et conforme on l'accepte
                // new_array[dy] est [11,01,00,00,00,00,00]
                // on prend chaque date représenté par des numéros
                // ex Lundi est ["11"]
                switch (new_array[dy]) {
                    case "10":
                        // matin
                        if ($(day_td[dy]).text().length >= 1) {
                            $(day_td[dy]).css('background', `linear-gradient(${color},#FFFFFF,#FFFFFF)`);
                            array.push(day_td[dy]);
                        }
                        break;
                    case "01":
                        // aprèm
                        if ($(day_td[dy]).text().length >= 1) {
                            $(day_td[dy]).css('background', `linear-gradient(#FFFFFF , #FFFFFF, ${color})`);
                            array.push(day_td[dy]);
                        }
                        break;
                    case "11":
                        // journée entière
                        if ($(day_td[dy]).text().length >= 1) {
                            $(day_td[dy]).css('background-color', color);
                            array.push(day_td[dy]);
                        }
                        break;
                    default:
                        // rien
                        break;
                }
            }
        }
    }
    // return un array qui contient des objects de date
    return array;
}

// colorie les datescolorDay
function colorDay(td, color) {
    // td représente toutes les dates d'un mois
    // couleur définie ( vacances , recherche , astreinte )
    if (color.length >= 1) {
        $(td).css('background-color', color);
    } else {
        // aucune couleur définie le jour est barrer ( non disponible )
        $(td).addClass('unavailability');
    }
}

for (let i = 0; i < time_range.length; i++) {
    // représente la couleur du time_rang
    let color = time_range[i].color;
    // prend le nbr de mois entre le mois de début et celui de fin
    let between;
    // va répresenter la position du mois start
    let position_month_start = 0;
    let copy_position_month_start = 0;
    // va répresenter la position du mois end
    let position_month_end = 0;

    // récupère le mois concerné dans le DOM la recherche ce fait ainsi par id, ex = Juillet-2020
    let month_start_translated = translateDate(time_range[i].start_years + '-' + getMonthFromString(time_range[i].start_month_str));
    let month_end_translated = translateDate(time_range[i].end_years + '-' + getMonthFromString(time_range[i].end_month_str));
    let id_month_start = capitalize(month_start_translated.month_name + "-" + time_range[i].start_years);
    let id_month_end = capitalize(month_end_translated.month_name + "-" + time_range[i].end_years);

    let exist_start = "#" + id_month_start;
    let exist_end = "#" + id_month_end;

    let day_start = parseInt(time_range[i].start_day);
    let day_end = parseInt(time_range[i].end_day);

    // si les 2 dates (month et years ) ne figurent pas dans le calendrier cela veut dire que les années décarts sont de 2, on colorie donc toutes l'année entière si elles sont comprises entre les 2 dates du time range
    // ex time range = 11 avril 2020 au 10 septembre 2024
    // on va checker si les années entre sont comprises dedans
    // ex : 11 avril 2020 au 10 septembre 2024 date de time range / calendier acutel : [ Juillet 2021 à Juin 2022 ] ,ces dates entre dans le time range ont colorie tous le calendrier
    if ($(exist_start).length === 0 && $(exist_end).length === 0) {
        let first_month = $('.agenda')[0].id;
        let last_month = $('.agenda')[11].id;
        // récupère l'année de début du calendier actuellement
        let first_years = first_month.split('-')[1];
        // récupère l'année de fin du calendier actuellement
        let last_years = last_month.split('-')[1];
        // récupère le mois de début du calendier actuellement
        first_month = first_month.split('-')[0];
        // récupère le mois de fin du calendier actuellement
        last_month = last_month.split('-')[0];

        // va colorier tous les mois entre les 2 dates de début et de fin si elles sont comprises dedans
        if (first_years >= parseInt(time_range[i].start_years) && last_years <= parseInt(time_range[i].end_years)) {
            for (let m = 0; m < 12; m++) {
                // représente toutes les semaines d'un mois
                let semaine = $(month_tables[m]).find('.agenda-week');
                // représente toutes les jours des semaines
                let td = $(semaine).find('td');
                // si un week est défini on colorie les jours de la semaine selon la demande

                if (time_range[i].week) {
                    getAllDaysInMonth(td, time_range[i].week, time_range[i]);
                } else {
                    // si aucun week défini ont colorie tous les mois
                    for (let t = 0; t < td.length; t++) {
                        if ($(td[t]).text().length > 0) {
                            $(td[t]).css('background-color', color);
                        }
                    }
                }
            }
        }
    } else if ($(exist_start).length === 0 && $(exist_end).length >= 1) {

        // check si le mois de début et l'année ne sont pas présent dans le DOM
        // si la date de début (month année) n'est pas dispo dans DOM c'est que la calendrier est à n + 1
        // on récupère le mois de fin ainsi que sont année dispo dans le DOM (ex: 12 Aôut 2021)
        // Il faut donc colorier tous les mois présent entre le premier moi n présent acutellement ( ex: Juillet ) jusqu'à la date end du moins end (ex: 12 Aôut 2021)
        // DONC colorie = 01 Juillet 2021 -> .... -> 12 Aôut 2021
        position_month_end = searchPositionMonth(id_month_end);

        for (let m = 0; m < position_month_end + 1; m++) {
            // représente toutes les semaines d'un mois
            let semaine = $(month_tables[m]).find('.agenda-week');
            // représente toutes les jours des semaines
            let td = $(semaine).find('td');
            // contient tous les jours du mois qui ne sont pas vide, enleve les cases vides du mois
            let array_td = getAllDaysInMonth(td, time_range[i].week, time_range[i]);

            // si position_month_end = 0 cela veut dire que le mois end est le suivant tandis que si m === 0 le mois end n'est pas directement le suiant
            if (position_month_end === 0) {
                for (let t = 0; t < day_end; t++) {
                    colorDay(array_td[t], color);
                }
            } else if (m === 0) {
                // premier mois
                // commencer par le jour du commencement du mois (ex le 15 septembre), start au 15ème jours
                for (let t = 0; t < td.length; t++) {
                    colorDay(array_td[t], color);
                }
            } else if (m === position_month_end) {
                // dernier mois
                for (let t = 0; t < day_end; t++) {
                    colorDay(array_td[t], color);
                }
            } else {
                // les mois entres
                for (let t = 0; t < td.length; t++) {
                    colorDay(array_td[t], color);
                }
            }
            copy_position_month_start++;
        }
    } else {
        // si les 2 mois ainsi que leurs années sont présent dans le DOM (ex: Juillet-2017 et Octobre-2017 )
        if (exist_start === exist_end) {
            position_month_start = searchPositionMonth(id_month_start);
            copy_position_month_start = position_month_start;

            // si les dates sont exactement les mêmes, même mois ,même dates ,même années, ex: commence le 21 Juin et fini le 21 Juin
            if (time_range[i].start_month_str === time_range[i].end_month_str && time_range[i].start_day === time_range[i].end_day) {
                let get = time_range[i].start_month_int[1];
                let semaine = $(month_tables[copy_position_month_start]).find('.agenda-week');
                let td = $(semaine).find('td');
                // contient tous les jours du mois qui ne sont pas vide, enleve les case vide du mois
                let array_td = getAllDaysInMonth(td);

                colorDay(array_td[day_start - 1], color);
            }

            // info : si les dates sont exactement les même ex: débute du 21 Juin fini le 21 Juin elle ne passe pas dans la boucle
            for (let m = day_start; m < day_end; m++) {
                let semaine = $(month_tables[copy_position_month_start]).find('.agenda-week');
                let td = $(semaine).find('td');
                // contient tous les jours du mois qui ne sont pas vide, enleve les case vide du mois
                let array_td = getAllDaysInMonth(td, time_range[i].week, time_range[i]);

                for (let t = day_start - 1; t < day_end; t++) {
                    colorDay(array_td[t], color);
                }
            }
        } else {

            // si les dates sont sur différents mois et années
            // check si le mois de fin et sont année sont présent dans le DOM, si non présent dans le DOM on modif ses données (mois , année) pour arriver au dernier mois du calendrier à sa dernière date
            // check si le mois de debut et sont année existe
            if ($(exist_end).length === 0 && $(exist_start).length >= 1) {
                // vue que le mois n'existe pas dans le DOM on récup le dernier mois présent dans le DOM
                // si le mois et l'année ne sont pas présents dans le DOM on modifie sa date et son année
                // ex l'année du calendrier est de Juillet 2020 à Juin 2021, si la date end est Août 2022 elle sera modifiée à fin Juin 2021, pk? car ele n'existe pas dans le DOM
                let last = array_calendar.slice(-1).pop();
                let positon = position_month.indexOf(last.month) + 1;

                if (positon < 10) {
                    positon = "0" + positon;
                }
                time_range[i].end = last.years + "/" + positon + "/" + last.days;
                time_range[i].end_day = last.days;
                time_range[i].end_month_int = positon;
                time_range[i].end_month_str = last.month;
                time_range[i].end_years = last.years;
                month_start_translated = translateDate(time_range[i].start_years + '-' + getMonthFromString(time_range[i].start_month_str));
                id_month_start = capitalize(month_start_translated.month_name + "-" + time_range[i].start_years);
                id_month_end = time_range[i].end_month_str + "-" + time_range[i].end_years;
                day_start = parseInt(time_range[i].start_day);
                day_end = parseInt(time_range[i].end_day);
            }
            // recherche la position du mois dans le calendrier basé sur le mois actuel et son année
            // return un int (ex : Juillet = 0)
            position_month_start = searchPositionMonth(id_month_start);
            position_month_end = searchPositionMonth(id_month_end);

            if ($(exist_start).length === 1) {
                // on récupère le nbr de mois entre, le nbr peut être négative si le mois et l'anné de début n'existe pas du coup on le rend positive avec abs()
                between = position_month_end - position_month_start + 1;
                between = Math.abs(between);
                copy_position_month_start = position_month_start;

                for (let m = 0; m < between; m++) {
                    let semaine = $(month_tables[copy_position_month_start]).find('.agenda-week');
                    let td = $(semaine).find('td');
                    // contient tous les jours du mois qui ne sont pas vide, enleve les cases vides du mois
                    let array_td = getAllDaysInMonth(td, time_range[i].week, time_range[i]);
                    // premier mois

                    if (m === 0) {
                        // commencer par le jour du commencement du mois (ex le 15 Septembre), start au 15ème jours
                        for (let t = day_start - 1; t < td.length; t++) {
                            colorDay(array_td[t], color);
                        }
                    } else if (m === between - 1) {
                        // dernier mois
                        for (let t = 0; t < day_end; t++) {
                            colorDay(array_td[t], color);
                        }
                    } else {
                        // ici sont les mois entres le mois de debut et le mois de fin
                        for (let t = 0; t < td.length; t++) {
                            colorDay(array_td[t], color);
                        }
                    }
                    copy_position_month_start++;
                }
            }
        }
    }
}
// est le formulaire de submit pour les time_rang
let form_sub_time_range = $('form').attr('name', 'time_range_calendar');
$(form_sub_time_range).submit(function (event) {
    // récup la date le mois l'année du start et du end pour pouvoir ensuite les comparer ( check si la date de fin n'est pas inférieur à la date de début )
    let all_date_start = $('#time_range_calendar_startDate').val();
    //ex : 15
    let days_start = all_date_start.split('/')[0];
    // ex : 05
    let month_start = all_date_start.split('/')[1];
    // ex: 2020
    let years_start = all_date_start.split('/')[2];
    years_start = years_start.substring(0, 4);
    let all_date_end = $('#time_range_calendar_endDate').val();
    //ex : 15
    let days_end = all_date_end.split('/')[0];
    // ex : 05
    let month_end = all_date_end.split('/')[1];
    // ex: 2020
    let years_end = all_date_end.split('/')[2];
    years_end = years_end.substring(0, 4);
    let start = new Date(years_start + "-" + month_start + "-" + days_start);
    let end = new Date(years_end + "-" + month_end + "-" + days_end);

    // si date début plus grand que date de fin on bloque le submit
    if (start.getTime() > end.getTime()) {
        // si les dates sont incorrectes on bloque le submit
        event.preventDefault();
        $(p_form_error).text('Erreur : La date de fin ne peut pas être inférieur à la date du début');
    } else {
        $(p_form_error).empty();
    }
});
