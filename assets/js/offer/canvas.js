$(document).ready(function(){
    $('.main-panel')[0].addEventListener('ps-scroll-y',(e) => {
        let criteriaListContainer = $('.canvas-row-list-element');
        if(criteriaListContainer.length > 0){
            criteriaListContainer = criteriaListContainer[0];
        }
        let criteriaListContainerWidth = $(criteriaListContainer).width();
        $(criteriaListContainer).removeClass('fixed-canvas-row-list-element');
        $('#dummy-div-canvas-construction').remove();
        $(criteriaListContainer).css({'left':'unset','width':'unset'});
        if($(criteriaListContainer).offset().top < 10){
            let dummyDiv = "<div id='dummy-div-canvas-construction' class='col-2'></div>";
            $(criteriaListContainer).parent().prepend(dummyDiv);
            $(criteriaListContainer).addClass('fixed-canvas-row-list-element');
            let dummyDivElement = $('#dummy-div-canvas-construction');
            $(criteriaListContainer).css({'left':dummyDivElement.offset().left+'px'});
            $(criteriaListContainer).width(criteriaListContainerWidth);
        }
    });
});