$(document).ready(function () {

    $(".cross-delete-one-row").click(function () {
        $(this).parent().remove();
    });

    criteriaRelevantness();
    criteriaRelevantnessOnChange();

    $("#btn-add-row-distance").click(function () {
        $("#distance-value-rows-container").append("<div class=\"distance-value-rows col-md-12 row\">" +
            "<input class=\"col-md-2 form-control\" type=\"number\" name=\"distance[km][]\"> " +
            "<div class=\"col-md-4\">par rapport à un code postal :</div>" +
            "<input class=\"form-control col-md-2\" type=\"text\" name=\"distance[cp][]\" pattern=\"[0-9]{5}\" title=\"Composé de 5 chiffres\">" +
            "<i class='fas fa-times fa-2x cross-delete-one-row'></i>" +
            "</div>");
        $(".cross-delete-one-row").click(function () {
            $(this).parent().remove();
        });
    });

    $("#wish_canvas").change(function () {
        showWaitingSpinner();
        $.ajax({
            type: "POST",
            url: "/offer/wish/ajax/" + $(this).val(),
            success: function (data) {
                hideWaitingSpinner();
                if (data === [])
                    return;
                let fields = "";
                $('#wishes-criteria-available').parent().children('.wish-criteria-container').html('');
                let relevantable_criteria = [];
                data.forEach(function (row) {
                    fields += "<div class='row col-md-12'>";
                    $(row).each(function (key, element) {
                        if (element['dependency'].length === 0) {
                            fields += "<div class='col-md-" + element['size'] + "' data-value='" + element['id'] + "' data-col='" + element['size'] + "'>";
                        } else {
                            if (Array.isArray(element['dependency'].value)) {
                                let new_value = JSON.stringify(element['dependency'].value);
                                fields += "<div class='col-md-" + element['size'] + " has-dependency d-none' data-dependency-id='" + element['dependency'].id + "' data-dependency-value='" + new_value.replace(/"/gi, "%20").replace(/'/gi, "%27") + "' data-value='" + element['id'] + "' data-col='" + element['size'] + "'>";
                            } else {
                                fields += "<div class='col-md-" + element['size'] + " has-dependency d-none' data-dependency-id='" + element['dependency'].id + "' data-dependency-value='" + element['dependency'].value + "' data-value='" + element['id'] + "' data-col='" + element['size'] + "'>";
                            }
                        }
                        let multiselect = false;
                        let div_criterion_type_constants = $("#criterion-type-constants");
                        switch (element['input_type']) {
                            case parseInt(div_criterion_type_constants.attr("data-bool")):
                                fields += "<label for='input-criterion-" + element['id'] + "' class='full-width'>" + element['label'] + "</label><input class='form-control bootstrap-switch full-width' data-on-label='Oui' data-off-label='Non' id='input-criterion-" + element['id'] + "' type='checkbox' checked>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-integer")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><input id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "]' class='form-control input-option-number full-width' type='number' min='0' placeholder='0'>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-entity")):
                                element['possible_value'] = JSON.stringify(element['possible_value']);
                            case parseInt(div_criterion_type_constants.attr("data-multi-select")):
                                multiselect = true;
                            case parseInt(div_criterion_type_constants.attr("data-select")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><select id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "]" + ((multiselect) ? "[]" : "") + "' class='form-control full-width selectpicker' " + ((multiselect) ? "multiple" : "") + ">";
                                if (!multiselect) {
                                    fields += "<option value='N/C'>N/C</option>";
                                }
                                $.each(JSON.parse(element['possible_value']), function (index_val, criterion_val) {
                                    fields += "<option value=\"" + criterion_val + "\">" + criterion_val + "</option>";
                                });
                                fields += "</select>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-date")):
                                if ($('#hidden_field_end_date').val() != '') {
                                    fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><input id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "]' class='form-control full-width' value='" + $('#hidden_field_end_date').val() + "' type='date'>";
                                } else {
                                    fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><input id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "]' class='form-control full-width' type='date'>";
                                }
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-specialty")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><input id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "]' class='form-control full-width' type='text' placeholder='" + element['label'] + "'>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-choice-week")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><select id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "][]' class='form-control full-width selectpicker' multiple><option value='0'>Lundi Matin</option><option value='1'>Lundi Après-midi</option><option value='2'>Mardi Matin</option><option value='3'>Mardi Après-midi</option><option value='4'>Mercredi Matin</option><option value='5'>Mercredi Après-midi</option><option value='6'>Jeudi Matin</option><option value='7'>Jeudi Après-midi</option><option value='8'>Vendredi Matin</option><option value='9'>Vendredi Après-midi</option><option value='10'>Samedi Matin</option><option value='11'>Samedi Après-midi</option><option value='12'>Dimanche Matin</option><option value='13'>Dimanche Après-midi</option></select>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-text")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><textarea id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "]' class='form-control full-width'>" + element['label'] + "</textarea>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-file")):
                                fields += "<div class=\"fileinput fileinput-new full-width text-center\" data-provides=\"fileinput\">" +
                                    "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label>" +
                                    "<div class=\"fileinput-new thumbnail\">" +
                                    "<img alt=\"image\">" +
                                    "</div>" +
                                    "<div class=\"fileinput-preview fileinput-exists thumbnail\"></div>" +
                                    "<div>" +
                                    "<span class=\"btn btn-rose btn-round btn-file\">" +
                                    "<span class=\"fileinput-new\">Select File</span>" +
                                    "<span class=\"fileinput-exists\">Change</span>" +
                                    "<input id='input-criterion-" + element['label'] + "' type=\"file\" name='criterion_wish[" + element['id'] + "]' accept='image/*'/>" +
                                    "</span>" +
                                    "<a class=\"btn btn-danger btn-round fileinput-exists\" data-dismiss=\"fileinput\"><i class=\"fa fa-times\"></i> Remove</a>" +
                                    "</div>" +
                                    "</div>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-location")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label><select id='input-criterion-" + element['id'] + "' name='criterion_wish[" + element['id'] + "][]' class='form-control full-width selectpicker' multiple>";
                                /**TODO à confirmer si N/C est la même chose que vide i.e sans région ni departement lié au souhait
                                 // fields += "<option value='N/C'>N/C</option>";*/
                                fields += "<option value='FE'>France Entière</option>";
                                element['possible_value'].forEach(region => {
                                    fields += "<optgroup label='" + region.nom + "'>";
                                    region.departments.forEach(department => {
                                        fields += "<option value='" + department.code + "'>" + department.code + ' - ' + department.nom + "</option>";
                                    });
                                    fields += "</optgroup>";
                                });
                                fields += "</select>";
                                break;
                            case parseInt(div_criterion_type_constants.attr("data-input")):
                                fields += "<label for='input-criterion-" + element['id'] + "'>" + element['label'] + "</label>" +
                                    "<input id='input-criterion-" + element['id'] + "' type='text' class='form-control full-width' name='criterion_wish[" + element['id'] + "]' placeholder='" + element['label'] + "'>";
                                break;
                        }
                        fields += "</div>";
                        let is_criterion_in_relevantable = false;
                        for (let i = 0; i < relevantable_criteria.length; i++) {
                            if (relevantable_criteria[i][0] === element['id']) {
                                is_criterion_in_relevantable = true;
                                break;
                            }
                        }
                        if (element['relevantable'] === true && !is_criterion_in_relevantable) {
                            relevantable_criteria.push([element['id'], element['label']]);
                        }
                    });
                    fields += "</div>";
                });
                relevantable_criteria.sort((a, b) => a[1].localeCompare(b[1], 'fr', {sensitivity: 'base'}));
                relevantable_criteria.forEach(function (element_datas) {
                    $('#wishes-criteria-available').append("<div id='relevantness-criterion-" + element_datas[0] + "' class='wishes-criteria wishes-criteria-card wish-criteria-disabled row justify-content-center'>" +
                        "<input type='hidden' name='criteria[0][]' value='" + element_datas[0] + "'>" +
                        "<div class='col-md-8 text-center wishes-criteria-label'>" + element_datas[1] + "</div>" +
                        "</div>");
                });
                $("#field-list").html("");
                $("#field-list").append(fields);
                $(".selectpicker").each(function () {
                    callSelectPicker($(this));
                });
                $(".wishes-criteria-card").hover(criteriaCardHover, function () {
                    $(".wishes-criteria-arrows").remove();
                    $(this).children(".wishes-criteria-label").removeClass('hover-wishes-criteria-label');
                });
                criteriaDependency();
                manageDepartmentSelection();
                criteriaRelevantnessOnChange();
            },
            error: function (error) {
                hideWaitingSpinner();
                swal("Affichage du canevas impossible", error.statusText + ' (' + error.status + ')', 'error');
            }
        });
    });

    $(".wishes-criteria-card").hover(criteriaCardHover, function () {
        $(".wishes-criteria-arrows").remove();
        $(this).children(".wishes-criteria-label").removeClass('hover-wishes-criteria-label');
    });
});

function criteriaCardHover() {
    switch ($(this).parent().attr("data-type")) {
        case '2':
            $(this).prepend("<div class='col-md-1 wishes-criteria-arrows'></div>");
            $(this).append("<div class='col-md-1 wishes-criteria-arrows'><i class='fas fa-arrow-right wishes-arrows'></i></div>");
            break;
        case '1':
            $(this).prepend("<div class='col-md-1 wishes-criteria-arrows'><i class='fas fa-arrow-left wishes-arrows'></i></div>");
            $(this).append("<div class='col-md-1 wishes-criteria-arrows'></div>");
            break;
        default:
            $(this).prepend("<div class='col-md-1 wishes-criteria-arrows'><i class='fas fa-arrow-left wishes-arrows'></i></div>");
            $(this).append("<div class='col-md-1 wishes-criteria-arrows'><i class='fas fa-arrow-right wishes-arrows'></i></div>");
            break;
    }
    $(this).children(".wishes-criteria-label").addClass('hover-wishes-criteria-label');
    $(".wishes-arrows").click(function () {
        let element = $(this).parent().parent();
        let category = element.parent().attr("data-type");
        let number_position;
        let position = [1, 0, 2];
        if ($(this).hasClass('fa-arrow-left')) {
            number_position = position[position.indexOf(parseInt(category)) + 1];
            moveCriterionRelevantness(element, number_position);
        } else {
            number_position = position[position.indexOf(parseInt(category)) - 1];
            moveCriterionRelevantness(element, number_position);
        }
        $(".wishes-criteria-arrows").remove();
        element.children(".wishes-criteria-label").removeClass('hover-wishes-criteria-label');
    });
}

function manageDepartmentSelection() {
    /*
 Gestion liste départements  dans le formulaire de création/edit de souhait
  */
    let select_department = $('select#input-criterion-18');
    let dropdown = select_department.prev().children();
    let all_option_departments = $('select#input-criterion-18 option');
    let list_dropdown = dropdown.children();

    //sélection par département
    dropdown.on('click', 'li[data-original-index][data-optgroup]', function () {
        if ($(this).data('original-index') !== 0) {
            $('select#input-criterion-18 option[value=FE]').prop('selected', false);
            $('div[data-value=18] li[data-original-index=0]').removeClass('selected');
        }
        refreshSelectPicker(select_department);
    });

    //délection france entière
    function selectFE(index, value) {
        all_option_departments.each(function () {
            if ($(this).val() !== value) {
                $(this).prop('selected', false);
            }
        });
        list_dropdown.each(function () {
            if ($(this).data('original-index') !== undefined || $(this).data('original-index') !== index) {
                $(this).removeClass('selected');
            }
        });
    }

    dropdown.on('click', 'li[data-original-index=0]', function () {
        if ($(this).hasClass('selected')) {
            selectFE(0, 'FE');
        }
        refreshSelectPicker(select_department);
    });

    //sélection par région
    dropdown.on('click', 'li.dropdown-header', function () {
        let group = $(this).data('optgroup');
        let current_region_departments = $('li[data-original-index][data-optgroup=' + group + ']');
        let selected_departments_number = $('li.selected[data-original-index][data-optgroup=' + group + ']').length;
        let current_region_option_group = ($('select#input-criterion-18 optgroup')[group - 1]).children;

        if (selected_departments_number < current_region_departments.length) {
            $('select#input-criterion-18 option[value=FE]').prop('selected', false);
            $('li[data-original-index=0]').removeClass('selected');
            for (let i = 0; i < current_region_option_group.length; i++) {
                current_region_option_group[i].selected = true;
            }
            current_region_departments.each(function () {
                $(this).addClass('selected');
            });
        } else {
            for (let i = 0; i < current_region_option_group.length; i++) {
                current_region_option_group[i].selected = false;
            }
            current_region_departments.each(function () {
                $(this).removeClass('selected');
            });
        }
        refreshSelectPicker(select_department);
    });

}

function criteriaRelevantness(){
    $("#field-list [id^='input-criterion']").each((index,element) => {
        let value = $(element).val();
        let splitted_element_html_id = $(element).attr('id').split('-');
        let criterion_id = splitted_element_html_id[splitted_element_html_id.length - 1];
        if(value.length > 0 && value !== 'N/C'){
            $("#relevantness-criterion-" + criterion_id).removeClass('wish-criteria-disabled');
        }
    });
}

function criteriaRelevantnessOnChange(){
    $("#field-list [id^='input-criterion']").on('change', function () {
        let value = $(this).val();
        let splitted_element_html_id = $(this).attr('id').split('-');
        let id_criterion = splitted_element_html_id[splitted_element_html_id.length - 1];
        let element = $("#relevantness-criterion-" + id_criterion);
        if (value.length > 0 && value !== 'N/C') {
            element.removeClass('wish-criteria-disabled');
        } else {
            element.addClass('wish-criteria-disabled');
            moveCriterionRelevantness(element, 0);
        }
    });
}

function moveCriterionRelevantness(element, index) {
    $(".wish-criteria-container[data-type='" + index + "']").append(element);
    element.children("input[type='hidden']").attr("name", "criteria[" + index + "][]");
}

export {manageDepartmentSelection};
