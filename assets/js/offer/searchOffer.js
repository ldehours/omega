$(document).ready(function () {
    let start_date = '';
    let end_date = '';

    document.getElementById('search_offer_startDate').addEventListener('change', (event) => {
        start_date = event.currentTarget.value;
        fixDatesChange(start_date, end_date);
    });

    document.getElementById('search_offer_endDate').addEventListener('change', (event) => {
        end_date = event.currentTarget.value;
        fixDatesChange(start_date, end_date);
    });
});

function fixDatesChange(start, end) {
    if (start !== '' && end !== '') {
        let convertedStart = convertStringToDate(start);
        let convertedEnd = convertStringToDate(end);
        if (convertedStart !== null && convertedEnd !== null) {
            if (areDatesLogical(convertedStart, convertedEnd) === false) {
                document.getElementById('search_offer_startDate').value = end;
                document.getElementById('search_offer_endDate').value = start;
            }
        }
    }
}

function areDatesLogical(start, end) {
    start = convertStringToDate(start);
    end = convertStringToDate(end);
    if (start === null || end === null) {
        return false;
    }
    return start <= end;
}

function convertStringToDate(stringDate) {
    if (!(stringDate instanceof Date)) {
        try {
            stringDate = new Date(stringDate);
        } catch (e) {
            return null;
        }
    }
    return stringDate;
}