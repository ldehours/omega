$(document).ready(function(){
    if($('#on-call-compensation-choice').val() === "oui"){
        $('#on-call-compensation-value-div').removeClass('d-none');
    }
    $('#on-call-compensation-choice').on('change',function(){
        let value = $(this).val();
        if(value === "oui"){
            $('#on-call-compensation-value-div').removeClass('d-none');
        }else{
            $('#on-call-compensation-value-div').addClass('d-none');
        }
    });

    let div_dates = $('.on-call-dates');
    div_dates.each(function(index){
        if($(this).attr('data-index') <= $('#on-call-nb-days').val()){
            $(this).removeClass('d-none');
        }
    });
    $('#on-call-nb-days').on('change',function(){
        let value = $(this).val();
        div_dates.addClass('d-none');
        div_dates.each(function(index){
            if($(this).attr('data-index') <= value){
                $(this).removeClass('d-none');
            }
        });
    });
});