$(document).ready(function () {
    $('.btn-send-mail-selected-wishes').click(function () { //fonction d'envoi des offres sélectionnées par mail.
        let dependency_table_id = $(this).attr("data-dependency-id");
        let wishes_selected = [];
        let offer_id = $(this).attr('data-offer-id');
        $("#" + dependency_table_id + "").find('.checkboxes-select-wishes').each(function () {
            if ($(this).is(":checked") && !wishes_selected.includes($(this).val())) {
                wishes_selected.push($(this).val());
            }
        });
        if (wishes_selected.length) {
            swal({
                title: 'Envoyer l\'offre n°' + offer_id + ' aux personnes sélectionnées ?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annuler',
                confirmButtonText: 'Confirmer'
            }).then(function (isConfirm) {
                if (isConfirm !== undefined && isConfirm === true) {
                    //send
                    sendOfferToMultiplePersonsAjax(offer_id, wishes_selected, false, true);
                }
            });
        } else {
            swal({
                title: "Attention",
                text: "Selectionnez au moins une personne",
                type: "error"
            });
        }
    });

    $('#btn-send-one-offer-to-persons').click(function(){
        sendOfferFromOfferEdit(this);
    })
});

function sendOfferFromOfferEdit(self){
    let persons_id = [];
    swal({
        title: "Entrez le numéro du client (puis 'Entrée')",
        html: '<div id="persons-id-container"></div><div id="loading-data"></div></br><div class="row justify-content-center"><div class="col-md-5"><input id="check-person-existence-input" type="number" class="form-control" min="0" max="999999999" required></div></div>',
        showCancelButton: true,
        confirmButtonText: 'Valider',
        cancelButtonText: 'Annuler',
        onOpen: function () {
            swal.disableConfirmButton();
            let input = $('#check-person-existence-input');
            input.focus();
            input.on('keyup', function (event) {
                let loading = $('#loading-data');
                if (event.keyCode === 13) { // si on appuie sur Entrée
                    event.preventDefault();
                    let value = $(this).val();
                    if (value !== '' && persons_id.includes(value) === false) {
                        let id_container = $('#persons-id-container');
                        loading.text('Verification...');
                        input.attr('disabled', true);
                        $.ajax({ //vérification de l'ID (pour savoir si une offre possède cet ID)
                            type: 'GET',
                            url: '/person/check/exists',
                            data: {id: value},
                            async: true,
                            success: (data) => {
                                loading.text('');
                                input.attr('disabled', false);
                                if (data.result === false) { //si l'ID n'existe pas
                                    loading.text("Aucun client pour le numéro : " + value);
                                } else {
                                    if (persons_id.includes(value) === false) {// si l'ID n'existe pas déjà
                                        persons_id.push(value);
                                        input.val('');
                                        id_container.append('<div>' + value + ' <i class="fa fa-times delete-person-id-icon" title="Supprimer ce numéro" data-value="' + value + '"></i></div>');
                                        swal.enableConfirmButton();
                                        $('.delete-person-id-icon').on('click', function () {// si on clic sur un icone permettant de supprimer un ID de la liste
                                            let actualValue = $(this).attr('data-value');
                                            $(this).parent().remove();
                                            let new_persons_id = [];
                                            for (let i = 0; i < persons_id.length; i++) {// on l'enlève du tableau
                                                if (persons_id[i] !== actualValue) {
                                                    new_persons_id.push(persons_id[i]);
                                                }
                                            }
                                            persons_id = new_persons_id;
                                            if (persons_id.length === 0) {// s'il n'y a plus d'ID dans le tableau
                                                swal.disableConfirmButton();
                                            }
                                        })
                                    }
                                }
                            },
                            error: (error) => {
                                console.error(error);
                                input.attr('disabled', false);
                                loading.text("Aucun client pour le numéro : " + value);
                            }
                        })
                    }
                }
                if ($(this).val() === '') {// clean up du message si l'input est vide
                    loading.text('');
                }
            })
        }
    }).then((data) => {
        let offer_id = $(self).attr('data-offer-id');
        sendOfferToMultiplePersonsAjax(offer_id, persons_id, false, true);
    });
}

function sendOfferToMultiplePersonsAjax(offer_id, person_naturals, is_SMS, is_email, message = null) {
    showWaitingSpinner();
    $.ajax({
        url: '/offer/offer/' + offer_id + '/multiple/send',
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            'isSMS': is_SMS,
            'isEmail': is_email,
            'message': message,
            'persons': person_naturals
        },
        success: function (data) {
            hideWaitingSpinner();
            if (data.success === true) {
                swal("Envoi groupé de mails", "L'offre a bien été envoyée", 'success');
            } else {
                swal("Attention", data.message, 'error');
            }
        },
        error: function () {
            hideWaitingSpinner();
            swal("", "Erreur lors de l'envoi des mails", 'error');
        }
    });
}