window.sendOffer = function (offerId, personNaturalId, isSMS = false, isEmail = false) {
    if (!isSMS && !isEmail) {
        swal("Offre numéro " + offerId, "Echec de l'envoi, aucun type n'a été choisi", 'error');
        return 0;
    }

    if (isSMS) {
        var SMSMaxLength = 160;

        swal({
            title: 'Saisissez le contenu du SMS',
            input: 'textarea',
            inputAttributes: {
                maxlength: SMSMaxLength,
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Valider',
            cancelButtonText: 'Annuler',
            cancelButtonColor: '#d33',
            onOpen: function () {
                $("<p id='textarea-warning'></p>").insertAfter(".swal2-textarea");
                $("<p id='textarea-counter'>0 caractère</p>").insertAfter(".swal2-textarea");
                $('.swal2-textarea').on('keypress keyup', function () {
                    var counter = $(this).val().length;
                    var counterSpecialChar = ($(this).val().match(/(\[|\\|\]|\^|\~|\{|\||\}|\€)/g) || []).length;
                    $(this).attr('maxLength', SMSMaxLength - counterSpecialChar);
                    counter += counterSpecialChar;
                    $('#textarea-counter').text(counter + ' caractères');
                    if (($(this).val().match(/(ê|ç)/g) || []).length > 0) {
                        $('#textarea-warning').html('Attention ! Le caractère <strong>ê</strong> sera convertit en simple <strong>e</strong> et le caractère <strong>ç</strong> sera convertit en <strong>Ç</strong>');
                    } else {
                        $('#textarea-warning').html('');
                    }
                });
            },
            inputValidator: (value) => {
                return new Promise((resolve, reject) => {
                    if (value.trim() == '' || value == null) {
                        reject('Le texte ne peut être vide');
                    } else {
                        resolve();
                    }
                })
            }
        }).then((result) => {
            sendOfferAjax(offerId, personNaturalId, isSMS, isEmail, result);
        })
    } else {
        sendOfferAjax(offerId, personNaturalId, isSMS, isEmail);
    }
};

function sendOfferAjax(offerId, personNaturalId, isSMS, isEmail, message = null) {
    let text = isSMS ? 'SMS' : 'Email';
    showWaitingSpinner();
    $.ajax({
        url: '/offer/offer/' + offerId + '/send/' + personNaturalId,
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            'isSMS': isSMS,
            'isEmail': isEmail,
            'message': message
        },
        success: function (data) {
            hideWaitingSpinner();
            if (data.success === true) {
                swal("Offre numéro " + offerId, "L'offre a bien été envoyée par " + text, 'success');
            } else {
                swal("Offre numéro " + offerId, data.message, 'error');
            }
        },
        error: function () {
            hideWaitingSpinner();
            swal("Offre numéro " + offerId, "Echec de l'envoi par " + text, 'error');
        }
    });
}

var ajaxRequestCanvas = function () {
    let select_canvas = $("#offer_canvas");
    let id_canvas = select_canvas.val();
    let old_canvas = 0;
    let input_file_hidden = $(".criterion-offer-old-file").clone();
    $('#on-call-form').addClass('d-none');
    $('#on-call-hidden-input').val('false');
    if($('#container-on-call-form').children().length === 0){
        $('#container-on-call-form').append($('#on-call-form'));
    }
    $("#canvas-offer").html("");
    let old_form = $("#offer-edit-canvas-old");
    if (old_form.length > 0) {
        old_canvas = old_form.attr("data-value");
        $("#hide-old-form").append(old_form);
    }
    if (id_canvas !== 0 && id_canvas != null) {
        if (id_canvas === old_canvas && old_canvas !== 0) {
            $("#show-old-form").append(old_form);
        } else {
            showWaitingSpinner();
            $.ajax({
                type: "POST",
                url: "/offer/offer/ajax/category/" + id_canvas,
                success: function (data) {
                    hideWaitingSpinner();
                    let went_through_type_number = 0;
                    $.each(data["canvas"]["formDisposition"], function (index_row, row) {
                        $("#canvas-offer").append("<div id='canvas-offer-row-" + index_row + "' class='row'></div>");
                        $.each(row, function (index, criterion) {
                            let nb_col_available = criterion[1];
                            let criterion_id = criterion[0];
                            let criterion_label = data["criterionArray"][criterion_id]["label"];
                            let criterion_type = data["criterionArray"][criterion_id]["type"];
                            let criterion_values = data["criterionArray"][criterion_id]["value"];
                            let criterion_dependency = data["criterionArray"][criterion_id]["dependency"];
                            let input_string;
                            if (!Array.isArray(criterion_dependency)) {
                                if (Array.isArray(criterion_dependency.value)) {
                                    let new_value = JSON.stringify(criterion_dependency.value);
                                    input_string = "<div class='form-group col-" + nb_col_available + " has-dependency d-none' data-dependency-id='" + criterion_dependency.id + "' data-dependency-value=\"" + new_value.replace(/"/gi, "%20").replace(/'/gi, "%27") + "\" data-value='" + criterion_id + "' data-col='" + nb_col_available + "'>";
                                } else {
                                    input_string = "<div class='form-group col-" + nb_col_available + " has-dependency d-none' data-dependency-id='" + criterion_dependency.id + "' data-dependency-value='" + criterion_dependency.value + "' data-value='" + criterion_id + "' data-col='" + nb_col_available + "'>";
                                }
                            } else {
                                input_string = "<div class='form-group col-" + nb_col_available + "' data-value='" + criterion_id + "' data-col='" + nb_col_available + "'>";
                            }
                            let div_criterion_type_constants = $("#criterion-type-constants");
                            let isMultiSelect = false;

                            switch (criterion_type) {
                                case  parseInt(div_criterion_type_constants.attr("data-bool")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "' class='full-width'>" + criterion_label + "</label><input type='hidden' name='criterion_offer[" + criterion_id + "]' value='off' /><input class='form-control bootstrap-switch' name='criterion_offer[" + criterion_id + "]' data-on-label='Oui' data-off-label='Non' id='input-criterion-" + criterion_id + "' type='checkbox' checked>";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-integer")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><input id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "]' class='form-control input-option-number full-width' type='number' min='0' placeholder='0' >";
                                    went_through_type_number = 1;
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-multi-select")):
                                    isMultiSelect = true;
                                case parseInt(div_criterion_type_constants.attr("data-select")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><select id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "]" + ((isMultiSelect) ? "[]" : "") + "' class='form-control full-width selectpicker' " + ((isMultiSelect) ? "multiple" : "") + " >";
                                    if (!isMultiSelect) {
                                        input_string += "<option value='N/C'>N/C</option>";
                                    }
                                    criterion_values = JSON.parse(criterion_values);
                                    $.each(criterion_values, function (index_val, crit_val) {
                                        input_string += "<option value='" + crit_val + "'>" + crit_val + "</option>";
                                    });
                                    input_string += "</select>";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-date")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><input id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "]' class='form-control full-width' type='date' >";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-specialty")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><input id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "]' class='form-control full-width' type='text' placeholder='" + criterion_label + "' >";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-choice-week")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><select id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "][]' class='form-control full-width selectpicker' multiple><option value='0'>Lundi Matin</option><option value='1'>Lundi Après-midi</option><option value='2'>Mardi Matin</option><option value='3'>Mardi Après-midi</option><option value='4'>Mercredi Matin</option><option value='5'>Mercredi Après-midi</option><option value='6'>Jeudi Matin</option><option value='7'>Jeudi Après-midi</option><option value='8'>Vendredi Matin</option><option value='9'>Vendredi Après-midi</option><option value='10'>Samedi Matin</option><option value='11'>Samedi Après-midi</option><option value='12'>Dimanche Matin</option><option value='13'>Dimanche Après-midi</option></select>";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-text")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><textarea id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "]' class='form-control full-width' >" + criterion_label + "</textarea>";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-file")):
                                    input_string += "<div class=\"fileinput fileinput-new full-width text-center\" data-provides=\"fileinput\">" +
                                        "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label>" +
                                        "<div class=\"fileinput-new thumbnail\">" +
                                        "<img alt=\"image\">" +
                                        "</div>" +
                                        "<div class=\"fileinput-preview fileinput-exists thumbnail\"></div>" +
                                        "<div>" +
                                        "<span class=\"btn btn-rose btn-round btn-file\">" +
                                        "<span class=\"fileinput-new\">Select File</span>" +
                                        "<span class=\"fileinput-exists\">Change</span>" +
                                        "<input id='input-criterion-" + criterion_id + "' type=\"file\" name='criterion_offer[" + criterion_id + "]' accept='image/*'/>" +
                                        "</span>" +
                                        "<a class=\"btn btn-danger btn-round fileinput-exists\" data-dismiss=\"fileinput\"><i class=\"fa fa-times\"></i> Remove</a>" +
                                        "</div>" +
                                        "</div>";
                                    $(".fileinput").parent().addClass("text-center");
                                    $(".fileinput").append(input_file_hidden);
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-location")):
                                case parseInt(div_criterion_type_constants.attr("data-json")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label><select id='input-criterion-" + criterion_id + "' name='criterion_offer[" + criterion_id + "][]' class='form-control full-width selectpicker'  multiple>";
                                    input_string += "<option value='N/C'>N/C</option>";
                                    input_string += "<option value='FE'>France Entière</option>";
                                    $.each(criterion_values, function (key, division) {
                                        input_string += "<optgroup label='" + key + "'>";
                                        $.each(division, function (index_val, criterion_val) {
                                            input_string += "<option value='" + key.substr(0, 1) + criterion_val['code'] + "'>" + criterion_val['code'] + ' - ' + criterion_val['nom'] + "</option>";
                                        });
                                        input_string += "</optgroup>";
                                    });
                                    input_string += "</select>";
                                    break;
                                case parseInt(div_criterion_type_constants.attr("data-input")):
                                    input_string += "<label for='input-criterion-" + criterion_id + "'>" + criterion_label + "</label>" +
                                        "<input id='input-criterion-" + criterion_id + "' type='text' class='form-control full-width' name='criterion_offer[" + criterion_id + "]' placeholder='" + criterion_label + "'>";
                                    break;
                            }
                            input_string += "</div>";
                            $("#canvas-offer-row-" + index_row + "").append(input_string);
                            $(".selectpicker").each(function () {
                                callSelectPicker($(this));
                            });
                        });
                    });
                    if (went_through_type_number == 1) {
                        $(".input-option-number").each(function () {
                            $(this).keypress(function (event) {
                                let key_code = (event.which) ? event.which : event.keyCode;
                                let accept = '0123456789';
                                return accept.indexOf(String.fromCharCode(key_code)) >= 0;
                            });
                        });
                    }
                    let select = $("#offer_type").parent().children(".dropdown-menu").children("ul").children('li');
                    select.each(function () {
                        $(this).show();
                        $(this).removeClass("selected");
                        $(this).children('a').attr('aria-selected', false);
                        if (!data.substitutionTypes.includes(parseInt($(this).attr('data-original-index'), 10) + 100)) {//+100 car dans l'entité, on stocke les constantes de type avec des entiers 101,102,103,... (en gros c'est 100+id)
                            $(this).hide();
                            $("#offer_type option[value=" + (parseInt($(this).attr('data-original-index'), 10) + 1) + "]").attr('selected', false);
                            $(this).removeClass('selected');
                            $(this).children('a').attr('aria-selected', false);
                        }
                    });
                    $("#offer_type").parent().children(".dropdown-toggle").attr('title', '');
                    $("#offer_type").parent().children(".dropdown-toggle").children('span').text('');

                    if (data.substitutionHeadType !== undefined && data.substitutionHeadType === 'PS') {
                        $("#title-offer-pricing-type").text('HT');
                        $("#offer_coordinatesRemoval").parent().parent().removeClass('bootstrap-switch-off');
                        $("#offer_coordinatesRemoval").parent().parent().addClass('bootstrap-switch-on');
                    } else {
                        $("#title-offer-pricing-type").text('TTC');
                        $("#offer_coordinatesRemoval").parent().parent().removeClass('bootstrap-switch-on');
                        $("#offer_coordinatesRemoval").parent().parent().addClass('bootstrap-switch-off');
                    }
                    let criterion_garde_id = $('#criterion_garde_id').val();
                    $("#input-criterion-"+criterion_garde_id).on('change',handleGardeCriterionClick); //on gère le formulaire de garde
                    criteriaDependency();
                },
                error: function () {
                    hideWaitingSpinner();
                    swal({
                        type: 'error',
                        title: 'Erreur',
                        text: 'Echec dans le chargement du canevas.'
                    });
                }
            });
        }

    }
};

$(document).ready(function () {
    $("#offer_canvas").change(ajaxRequestCanvas);

    $(".btn-remove-file").click(function () {
        $(this).parent().parent().children(".file-input-edit").html("");
    });
    $(".btn-change-file").click(function () {
        $(this).parent().parent().children(".file-input-edit").html("");
    });

    $("#offer_type").parent().children(".dropdown-menu").children("ul").children('li').click(function () {
        let text = $(this).children('a').children('.text').text();
        $(this).parent().parent().parent().children('.dropdown-toggle').attr('title', text);
        $("#offer_type").parent().children(".dropdown-toggle").children('span').text(text);
    });

    $('.view-person-offer-link').click(function () { //view[Substitute/Based]Offer.html.twig
        window.location = $(this).attr('data-href');
    });

    let criterion_garde_id = $('#criterion_garde_id').val();
    let input_criterion_garde = $("#input-criterion-"+criterion_garde_id);
    if(input_criterion_garde.val() === 'Oui'){
        moveOnCallForm(input_criterion_garde,$('#on-call-form'));
    }
    input_criterion_garde.on('change',handleGardeCriterionClick); //on gère le formulaire de garde

    $('#select-date-type').on('change', function () {
        if ($(this).is(':checked')) {
            $('.offer-form-pick-date').addClass('d-none');
            let new_date = new Date();
            let month = new_date.getUTCMonth();
            month++;
            month = month.toString();
            if (month <= 9) {
                month = '0' + month;
            }
            new_date = new_date.getUTCFullYear() + '-' + month + '-' + ((new_date.getUTCDate() > 0 && new_date.getUTCDate() < 10) ? '0' : '') + new_date.getUTCDate();
            $('#offer_startingDate').val(new_date);
        } else {
            $('.offer-form-pick-date').removeClass('d-none');
        }
    });

    $('#reinstate-matching-btn').click(function(){
        let url = $(this).attr('data-url');
        let offerId = $(this).attr('data-offer-id');
        showWaitingSpinner();
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            async: true,
            success: function (data) {
                hideWaitingSpinner();
                if (data.success === true) {
                    swal("Offre numéro " + offerId, "L'offre a bien été réintégrée au matching", 'success');
                } else {
                    swal("Offre numéro " + offerId, data.message, 'error');
                }
            },
            error: function (error) {
                console.error(error);//pour la maintenance et aider à debug
                hideWaitingSpinner();
                swal("Offre numéro " + offerId, "Echec de la réintégration au matching ", 'error');
            }
        });
    });

    let uid = parseInt($('#offer-skill-add-item').attr('data-number-item'));
    $("#offer-skill-add-item").click(function(){
        let itemModel = $('#offer-skill-item-model');
        let item = itemModel.clone();
        item.removeAttr('id');
        item.removeClass('d-none');
        $(this).before(item);
        item.find('.offer-skill-input').attr('required','required');
        let rateInput = item.find('input.offer-skill-input');
        let selectInput = item.find('select');
        callSelectPicker(selectInput);
        selectInput.attr('name','offer_skills['+uid+'][specialty]');
        rateInput.attr('name','offer_skills['+uid+'][rate]');
        item.children('.offer-skill-delete-item').click(bindSkillsDelete);
        uid++;
    });
    $('.offer-skill-delete-item').click(bindSkillsDelete);
});

function bindSkillsDelete(){
    $(this).parent().remove();
}

function handleGardeCriterionClick(){
    let value = $(this).val();
    let garde_form = $('#on-call-form');
    if(value === 'Oui'){
        moveOnCallForm($(this),garde_form);
    }else{
        garde_form.addClass('d-none');
        $('#on-call-hidden-input').val('false');
    }
}

function moveOnCallForm(garde_input,garde_form){
    garde_form.removeClass('d-none');
    garde_input.parent().parent().parent().after(garde_form);
    $('#on-call-hidden-input').val('true');
}
