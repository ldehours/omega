$(document).ready(function () {
    $('#offer-id-search-field').val('');
    $('#offer-id-search-field').on('keypress', function (event) {
        let current_input = event.originalEvent.key;
        if (isInputInvalid(current_input, event.originalEvent.code)) {
            return false;
        }
    });
    let xhr;
    let offer_to_send;
    $('#offer-id-search-field').on('keyup', function (event) {
        let current_input = event.originalEvent.key;
        if (isInputInvalid(current_input, event.originalEvent.keyCode)) {
            return false;
        }

        let value = $(this).val();
        if (xhr !== undefined) {
            xhr.abort();
        }
        $('#error-offer-search').addClass('d-none');
        $('#result-research-offer').addClass('d-none');
        $('#add_offer_to_person_natural_submit-container').addClass('d-none');
        if (value !== '') {
            let loader = $('#loader-search-container');
            loader.removeClass('d-none');
            xhr = $.ajax({
                type: 'POST',
                url: $('#offer-id-search-field').attr('data-href'),
                data: {
                    offerId : value
                },
                success: function (data) {
                    loader.addClass('d-none');
                    if (data.success !== undefined && data.success === false) {
                        if (data.message !== undefined) {
                            $('#error-offer-search').text(data.message);
                            $('#error-offer-search').removeClass('d-none');
                        }
                    } else {
                        offer_to_send = data;
                        $('#result-research-offer').removeClass('d-none');
                        $('#add_offer_to_person_natural_submit-container').removeClass('d-none');
                        $('#search-offer-send-offer-btn').removeClass('d-none');
                        if (!offer_to_send.person.has_mail) {
                            $('#search-offer-send-offer-btn').addClass('d-none');
                        }

                        let result_research_offer_id = $('#result-research-offer-id');
                        let result_research_offer_state = $('#result-research-offer-state');
                        let result_research_offer_person = $('#result-research-offer-person');

                        result_research_offer_id.text(offer_to_send.id);
                        result_research_offer_id.attr('href', result_research_offer_id.attr('data-href').slice(0, -1) + offer_to_send.id);
                        result_research_offer_state.text(offer_to_send.state);
                        result_research_offer_state.attr('href', result_research_offer_id.attr('data-href').slice(0, -1) + offer_to_send.id);
                        result_research_offer_person.text(offer_to_send.person.name);
                        result_research_offer_person.attr('href', result_research_offer_person.attr('data-href').slice(0, -1) + offer_to_send.person.id);
                        $('#offer_to_associate').val(offer_to_send.id);
                    }
                },
                error: function (error) {
                    loader.addClass('d-none');
                    let text = '';
                    switch (error.status) {
                        case 403:
                            text = "Il semblerait que vous n'ayez pas les droits pour faire la recherche.";
                            break;
                        case 404:
                            text = 'Aucune offre trouvée sous ce numéro.';
                            break;
                        default:
                            text = 'Une erreur est survenue lors de la recherche. Code erreur : ' + error.status + ' (contactez le support en fournissant ce code si besoin)';
                            break;
                    }
                    let error_div = $('#error-offer-search');
                    error_div.text(text);
                    error_div.removeClass('d-none');
                }
            });
        }
    });

    $('#search-offer-send-offer-btn').click(function () {
        if (offer_to_send !== null) {
            swal({
                title: 'Attention',
                text: "Souhaitez-vous vraiment envoyer l'offre " + offer_to_send.id + ' à ' + offer_to_send.person.name + ' ?',
                type: 'info',
                confirmButtonText: 'Confirmer',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Annuler'
            }).then((isConfirm) => {
                if (isConfirm === true) {
                    //TODO : send offer (faire pareil que dans le matching)
                    swal({
                        title: 'Succès',
                        text: 'Offre envoyée',
                        type: 'success'
                    });
                }
            });
        } else {
            swal({
                'title': 'Erreur',
                'text': 'Vous ne pouvez pas faire ça pour l\'instant',
                'type': 'error'
            });
        }
    })
});

function isInputInvalid(current_input, inputKeyCode) { //vérifie le seul input de la page
    let good_values = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    return (!good_values.includes(current_input) && inputKeyCode !== KeyboardEvent.DOM_VK_BACK_SPACE);
}