$(document).ready(function () {
    let selectedOffersForWish = []; // initialisation du tableau des offres selectionnées.
    $('.datatables-matching').each(function () { // on défini autant de lignes dans le tableau que de table/wish dans le résultat du matching.
        let wishId = parseInt($(this).attr('data-wish-id'));
        selectedOffersForWish.push({
            'wishId': wishId,
            'selectedOffers': []
        });
    });
    $('table[id^="resultMatching-table"]').click(function (event) { // on défini un champ large pour le clic sinon toutes les checkbox n'auront pas le bind de l'évènement lors du $(document).ready(...)
        let target = event.target;
        if ($(target).hasClass('checkboxes-select-offers')) { // si on clique sur une checkbox dans une des tables résultant du matching
            let wishId = $(target).attr('data-wish-id');
            let selectedOffersIndex = getSelectedOffersIndexFromWishTable(selectedOffersForWish, wishId); // index de la ligne correspondante pour ce wish dans le tableau selectOffersForWish
            if (selectedOffersIndex !== null) {
                if ($(target).is(':checked')) { // si on coche : on ajoute l'ID de l'offre au tableau
                    selectedOffersForWish[selectedOffersIndex]['selectedOffers'].push($(target).val());
                } else { // sinon on l'enlève si elle existe
                    for (let i = 0; i < selectedOffersForWish[selectedOffersIndex]['selectedOffers'].length; i++) {
                        if (selectedOffersForWish[selectedOffersIndex]['selectedOffers'][i] === $(target).val()) {
                            selectedOffersForWish[selectedOffersIndex]['selectedOffers'].splice(i, 1);
                            break;
                        }
                    }
                }
            }
        }
    });
    $('.btn-send-mail-selected-offers').click(function () { //fonction d'envoi des offres sélectionnées par mail.
        let selectedOffersIndex = getSelectedOffersIndexFromWishTable(selectedOffersForWish, $(this).attr('data-wish-id'));
        if (selectedOffersIndex !== null) {
            let offers_selected = selectedOffersForWish[selectedOffersIndex]['selectedOffers'];
            let person_id = $(this).attr('data-person-id');
            if (offers_selected.length) {
                confirmMailContentAndSend(offers_selected, person_id, false, true);
            } else {
                swal({
                    title: "Attention",
                    text: "Selectionnez au moins une offre",
                    type: "error"
                });
            }
        } else {
            console.error('selectedOffersIndex est `null` : envoi de mail impossible');
        }
    });

    $('.btn-remove-from-matching').click(function () {//retirer des offres du matching
        let wish_id = $(this).attr('data-wish-id');
        let selectedOffersIndex = getSelectedOffersIndexFromWishTable(selectedOffersForWish, wish_id);
        if (selectedOffersIndex !== null) {
            let offers_selected = selectedOffersForWish[selectedOffersIndex]['selectedOffers'];
            if (offers_selected.length) {
                swal({
                    title: 'Attention',
                    type: 'warning',
                    text: 'Etes-vous sûr de retirer du matching les offres sélectionnées ?',
                    showCancelButton: true,
                    cancelButtonText: 'Annuler',
                    confirmButtonText: 'Confirmer',
                }).then(function (isConfirm) {
                    if (isConfirm !== undefined && isConfirm === true) {
                        showWaitingSpinner();
                        $.ajax({
                            type: "POST",
                            url: "/offer/offer/remove-from-matching",
                            data: {
                                offerIds: offers_selected,
                                wishId: wish_id
                            },
                            dataType: 'json',
                            success: function (data) {
                                hideWaitingSpinner();
                                if (data.success === true) {
                                    swal({
                                        type: 'success',
                                        title: 'Succès',
                                        text: 'offre(s) retirée(s) du matching'
                                    });
                                    offers_selected.forEach(function (element) {
                                        $("#row-offer-" + element + "-" + wish_id).remove();
                                    });
                                } else {
                                    swal({
                                        type: 'error',
                                        title: 'Erreur',
                                        text: data.message
                                    });
                                }
                            },
                            error: function (error) {
                                hideWaitingSpinner();
                                swal({
                                    title: "Erreur",
                                    type: "error",
                                    text: "Code erreur : " + error.status
                                });
                            }
                        })
                    }
                });
            } else {
                swal({
                    title: "Attention",
                    text: "Selectionnez au moins une offre",
                    type: "error"
                });
            }
        } else {
            console.error('selectedOffersIndex est `null` : impossible de retirer du matching les offres');
        }
    });

    $('#btn-send-one-offer').click(function () {
        sendOfferFromPersonView(this);
    });
});

function sendOfferFromPersonView(self) {
    let offers_id = [];
    swal({
        title: "Entrez le numéro de l'offre (puis 'Entrée')",
        html: '<div id="offers-id-container"></div><div id="loading-data"></div></br><div class="row justify-content-center"><div class="col-md-5"><input id="check-offer-existence-input" type="number" class="form-control" min="0" max="999999999" required></div></div>',
        showCancelButton: true,
        confirmButtonText: 'Valider',
        cancelButtonText: 'Annuler',
        onOpen: function () {
            swal.disableConfirmButton();
            let input = $('#check-offer-existence-input');
            input.focus();
            input.on('keyup', function (event) {
                let loading = $('#loading-data');
                if (event.keyCode === 13) { // si on appuie sur Entrée
                    event.preventDefault();
                    let value = $(this).val();
                    if (value !== '' && offers_id.includes(value) === false) {
                        let id_container = $('#offers-id-container');
                        loading.text('Verification...');
                        input.attr('disabled', true);
                        $.ajax({ //vérification de l'ID (pour savoir si une offre possède cet ID)
                            type: 'GET',
                            url: '/offer/offer/check/exists',
                            data: {id: value},
                            async: true,
                            success: (data) => {
                                loading.text('');
                                input.attr('disabled', false);
                                if (data.result === false) { //si l'ID n'existe pas
                                    loading.text("Aucune offre pour le numéro : " + value);
                                } else {
                                    if (offers_id.includes(value) === false) {// si l'ID n'existe pas déjà
                                        offers_id.push(value);
                                        input.val('');
                                        id_container.append('<div>' + value + ' <i class="fa fa-times delete-offer-id-icon" title="Supprimer ce numéro" data-value="' + value + '"></i></div>');
                                        swal.enableConfirmButton();
                                        $('.delete-offer-id-icon').on('click', function () {// si on clic sur un icone permettant de supprimer un ID de la liste
                                            let actualValue = $(this).attr('data-value');
                                            $(this).parent().remove();
                                            let new_offers_id = [];
                                            for (let i = 0; i < offers_id.length; i++) {// on l'enlève du tableau
                                                if (offers_id[i] !== actualValue) {
                                                    new_offers_id.push(offers_id[i]);
                                                }
                                            }
                                            offers_id = new_offers_id;
                                            if (offers_id.length === 0) {// s'il n'y a plus d'ID dans le tableau
                                                swal.disableConfirmButton();
                                            }
                                        })
                                    }
                                }
                            },
                            error: (error) => {
                                console.error(error);
                                input.attr('disabled', false);
                                loading.text("Aucune offre pour le numéro : " + value);
                            }
                        })
                    }
                }
                if ($(this).val() === '') {// clean up du message si l'input est vide
                    loading.text('');
                }
            })
        }
    }).then((data) => {
        let person_id = $(self).attr('data-person-id');
        confirmMailContentAndSend(offers_id, person_id, false, true);
    });
}

//renvoie l'index correspondant à la bonne ligne du tableau selectedOffersForWish pour le souhait ayant l'ID wishId
function getSelectedOffersIndexFromWishTable(selectedOffersForWish, wishId) {
    let indexReturned = null;
    wishId = parseInt(wishId);
    for (let i = 0; i < selectedOffersForWish.length; i++) {
        if (selectedOffersForWish[i]['wishId'] === wishId) {
            indexReturned = i;
            break;
        }
    }
    return indexReturned;
}

function sendMultipleOffersAjax(offers, person_natural_id, mail_content, isSMS, isEmail, message = null) {
    let text = isSMS ? 'SMS' : 'Email';
    showWaitingSpinner();
    $.ajax({
        url: '/offer/offer/multiple/send/' + person_natural_id,
        type: "POST",
        dataType: "json",
        async: true,
        data: {
            'isSMS': isSMS,
            'isEmail': isEmail,
            'message': message,
            'offers': offers,
            'mail_content': mail_content
        },
        success: function (data) {
            hideWaitingSpinner();
            if (data.success === true) {
                let receiver = '';
                if (data.receiver) {
                    receiver = ' au Dr ' + data.receiver;
                }
                if (offers.length === 1) {
                    swal("Envoi d'offre", "L'offre a bien été envoyée par " + text + receiver, 'success');
                } else {
                    swal("Envoi groupé d'offres", "Les offres ont bien été envoyées par " + text + receiver, 'success');
                }
            } else {
                swal("", "Erreur lors de l'envoi de(s) offre(s)", 'error');
            }
        },
        error: function () {
            hideWaitingSpinner();
            swal("", "Erreur lors de l'envoi de(s) offre(s)", 'error');
        }
    });
}

function confirmMailContentAndSend(offers, person_natural_id, isSMS, isEmail, message = null) {
    showWaitingSpinner();
    $.ajax({
        url: '/offer/offer/ajax/get-mail-content/' + person_natural_id,
        type: 'POST',
        dataType: 'html',
        data: {
            'offers': offers
        },
        showLoaderOnConfirm: true,
        async: true,
        success: function (data) {
            hideWaitingSpinner();
            if (data.includes('"success":false')) {
                try {
                    let decoded_data = JSON.parse(data);
                    swal({
                        type: 'error',
                        title: 'Erreur',
                        text: decoded_data.message
                    });
                } catch (e) {
                    console.log(e);
                }
            } else {
                swal({
                    title: 'Contenu du mail',
                    html: data,
                    showCancelButton: true,
                    confirmButtonText: 'Envoyer',
                    cancelButtonText: 'Annuler',
                    onOpen: function () {
                        $('.swal2-modal').addClass('popup-send-mail');
                        let isOpened = false;
                        $('.mail-content-alterable').click(function () {
                            if (!isOpened) {
                                isOpened = true;
                                swal.disableConfirmButton();
                                let text_html = $(this).html();
                                text_html = text_html.replace(/<br>/gi, "\n");
                                let hidden_content = $(this);
                                hidden_content.addClass('d-none');
                                $(this).after("<div><textarea class='alterable-content-input'>" + text_html + "</textarea><button class='confirm-content btn btn-primary'>OK</button><button class='btn btn-danger dismiss-action'>Annuler</button></div>");
                                $('.confirm-content').on('click', function () {
                                    let new_text = $(this).parent().children('.alterable-content-input').val();
                                    new_text = new_text.replace(/\n/g, "<br>");

                                    $(this).parent().remove();
                                    hidden_content.html(new_text);
                                    hidden_content.removeClass('d-none');
                                    isOpened = false;
                                    swal.enableConfirmButton();
                                });
                                $('.dismiss-action').on('click', function () {
                                    $(this).parent().remove();
                                    hidden_content.removeClass('d-none');
                                    isOpened = false;
                                    swal.enableConfirmButton();
                                });
                            }
                        })
                    }
                }).catch(function (error) {
                    console.error(error);
                }).then(function (isConfirm) {
                    if (isConfirm !== undefined && isConfirm === true) {
                        //send
                        let mail_content = {
                            greetings: $('#mail-content-greetings').html(),
                            offers: $('#mail-content-offers').html(),
                            thanks: $('#mail-content-thanks').html(),
                            signature: $('#mail-content-signature').html()
                        };
                        sendMultipleOffersAjax(offers, person_natural_id, mail_content, isSMS, isEmail);
                    }
                });
            }
        },
        error: function (error) {
            console.log(error);
            hideWaitingSpinner();
            swal({
                title: 'Erreur',
                text: "Une erreur est survenue lors de la récupération des données de l'offre / des offres.",
                type: 'error'
            });
        }
    });
}