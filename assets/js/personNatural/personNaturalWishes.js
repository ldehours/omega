$(document).ready(function () {

    /* ------------ APPEL API ------------ */
    $('.btn-api-call').click(function () {
        var name_api = $(this).attr('data-nameapi');
        var url = '/core/api/' + name_api;
        $.ajax({
            type: 'GET',
            url: url,
            success: function () {
                swal({
                    title: 'Succès',
                    type: 'success',
                    text: 'Mise à jour terminée :)'
                }).then(function () {
                    window.location = "";
                });
            },
            error: function () {
                swal({
                    title: 'Erreur',
                    type: 'error',
                    text: 'Un problème est survenu lors de l\'appel des APIs.'
                }).then(function () {
                    window.location = "";
                });
            },
        });
    });
    /****************************************/

    /* Buttons displaying the right form */
    $('.btn-wishes').click(function () {
        var type = $(this).attr("data-type");
        if (!$(this).hasClass('active')) {
            $(".btn-wishes").removeClass("active");
            $(this).addClass('active');
            $(".content-wishes").addClass("d-none");
            $('#content-' + type + '').removeClass('d-none');
        }
    });
    /************************************/

    $(".cross-delete-one-row").click(function () {
        $(this).parent().parent().remove();
    });

    $('.nb-actes-type').click(function () {
        var type = $(this).attr("data-type");
        $(".nb-actes-rows").addClass('d-none');
        $("#nb-actes-" + type + "-row").removeClass('d-none');
    });

    $("#btn-add-row-distance").click(function () {
        var type = $(this).attr('data-type');
        var tab_infos;
        if (type == "liberal") {
            tab_infos = {
                "class_name": "distance",
                "input_name": "distance",
                "width_col_label": "2",
                "width_col_content": "10"
            };
        } else if (type == "wage") {
            tab_infos = {
                "class_name": "zone-mobilite",
                "input_name": "zone_mobilite",
                "width_col_label": "3",
                "width_col_content": "9"
            };
        }
        $(this).parent().before("<div class=\"" + tab_infos['class_name'] + "-value-rows col-md-12 row\">" +
            "<div class=\"col-md-" + tab_infos['width_col_label'] + "\"></div>" +
            "<div class=\"col-md-" + tab_infos['width_col_content'] + " row\">" +
            "<input class=\"col-md-2 form-control\" type=\"number\" name=\"" + tab_infos['input_name'] + "[km][]\"> " +
            "<div class=\"col-md-4\">par rapport à un code postal :</div>" +
            "<input class=\"form-control col-md-2\" type=\"number\" name=\"" + tab_infos['input_name'] + "[cp][]\">" +
            "<i class='fas fa-times fa-2x cross-delete-one-row'></i></div>" +
            "</div>");
        $(".cross-delete-one-row").click(function () {
            $(this).parent().parent().remove();
        });
    });

    $(".form-informatique-radios").click(function () {
        $(".form-informatique-contents").addClass('d-none');
        if ($("#form-informatique-radio-selection").is(':checked')) {
            $("#form-informatique-content-selection").removeClass('d-none');
        } else if ($("#form-informatique-radio-saisie").is(':checked')) {
            $("#form-informatique-content-saisie").removeClass('d-none');
        }
    });

    $(".wishes-criteria-card").hover(criteriaCardHover, function () {
        var type = "";
        if ($(this).hasClass('wishes-criteria-wage')) {
            type = "-wage";
        }
        $(".wishes-criteria" + type + "-arrows").remove();
        $(this).children(".wishes-criteria-label").removeClass('hover-wishes-criteria-label');
    });

    $(".radios-hiding-content").click(function () {
        var class_to_hide = $(this).attr("data-class-to-hide");
        $("." + class_to_hide + "").addClass('d-none');
    });
    $(".radios-displaying-content").click(function () {
        var class_to_show = $(this).attr("data-class-to-display");
        $("." + class_to_show + "").removeClass('d-none');
    });

    /* ------ Specific to Form Wage ------ */
    $("#btn-add-row-software").click(function () {
        $(this).parent().before("<div class=\"col-md-12 row\">" +
            "<div class=\"col-md-3\"></div>" +
            "<div class=\"col-md-6\">" +
            "<input type=\"text\" class=\"form-control\" name=\"connaissance_logiciels_info[]\" value=\"\">" +
            "</div>" +
            "<div><i class='fas fa-times fa-2x cross-delete-one-row'></i></div>" +
            "</div>");
        $(".cross-delete-one-row").click(function () {
            $(this).parent().parent().remove();
        });
    });

    $(".btn-pretentions").click(function () {
        var type = $(this).attr("data-type");
        $(".pretentions-rows").addClass('d-none');
        $("#pretentions-" + type + "-row").removeClass('d-none');
    });
    /************************************/
});

function criteriaCardHover() {
    var type = "";
    if ($(this).hasClass('wishes-criteria-wage')) {
        type = "-wage";
    }
    switch ($(this).parent().attr("data-type")) {
        case '3':
            $(this).prepend("<div class='col-md-1 wishes-criteria" + type + "-arrows'></div>");
            $(this).append("<div class='col-md-1 wishes-criteria" + type + "-arrows'><i class='fas fa-arrow-right wishes" + type + "-arrows'></i></div>");
            break;
        case '1':
            $(this).prepend("<div class='col-md-1 wishes-criteria" + type + "-arrows'><i class='fas fa-arrow-left wishes" + type + "-arrows'></i></div>");
            $(this).append("<div class='col-md-1 wishes-criteria" + type + "-arrows'></div>");
            break;
        default:
            $(this).prepend("<div class='col-md-1 wishes-criteria" + type + "-arrows'><i class='fas fa-arrow-left wishes" + type + "-arrows'></i></div>");
            $(this).append("<div class='col-md-1 wishes-criteria" + type + "-arrows'><i class='fas fa-arrow-right wishes" + type + "-arrows'></i></div>");
            break;
    }
    $(this).children(".wishes-criteria-label").addClass('hover-wishes-criteria-label');
    $(".wishes" + type + "-arrows").click(function () {
        var element = $(this).parent().parent();
        var category = element.parent().attr("data-type");
        var number;
        if ($(this).hasClass('fa-arrow-left')) {
            number = (parseInt(category) + 1);
            $(".wish-criteria" + type + "-container[data-type='" + number + "']").append(element);
            element.children("input[type='hidden']").attr("name", "criteria[" + number + "][]");
        } else {
            number = (parseInt(category) - 1);
            $(".wish-criteria" + type + "-container[data-type='" + number + "']").append(element);
            element.children("input[type='hidden']").attr("name", "criteria[" + number + "][]");
        }
        $(".wishes-criteria" + type + "-arrows").remove();
        element.children(".wishes-criteria-label").removeClass('hover-wishes-criteria-label');
    });
}