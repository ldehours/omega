"use strict";
$(document).ready(function () {

    handleJobName(document.getElementById('person_natural_mainJobName').value);
    document.getElementById('person_natural_mainJobName').addEventListener('change', (e) => handleJobName(e.currentTarget.value));

    let current_tab = $('.tab-pane.active.show').attr('id');
    let matchAddress = false;
    if (typeof loadDataForPerson === 'function') {
        loadDataForPerson();
    }

    function skipAddressTabOnPrevious() {
        setTimeout(function () {
            $('.btn-previous').trigger('click');
        }, 250);
    }

    function skipAddressChecking() {
        getCurrentTab();
        setTimeout(function () {
            $('.btn-next').trigger('click');
        }, 250);
        current_tab = 'account';
    }

    $(document).on('click', '.delete-btn', function () {
        getCurrentTab();
        if ($('#address-fields-list').children().length === 0) {
            matchAddress = false;
        }
    });

    function getCurrentTab() {
        setTimeout(function () {
            current_tab = $('.tab-pane.active.show').attr('id');
        }, 500);
    }

    $(document).on('click', 'input.newPersonLegalCheckbox', function () {
        $('#newPersonLegalCorporateName' + $(this).attr('id')).toggleClass('d-none');
    });

    $('.btn-previous').on('click', function () {
        getCurrentTab();
        if (current_tab === 'account' && (!matchAddress || $('#address-fields-list').children().length < 0)) {
            showWaitingSpinner();
            skipAddressTabOnPrevious();
            current_tab = 'about';
        } else if (current_tab === 'link') {
            current_tab = 'about';
        }
        hideWaitingSpinner();
    });

    $('.btn-next').on('click', function () {

        if (current_tab === 'about') {
            showWaitingSpinner();
            let cabinet_group = $('#link');
            let list_person_found = "";
            let mainJobName = $('#person_natural_mainJobName').val();
            if ($('#address-fields-list').children().length > 0) {
                let type_address = [];

                let all_type_addresses = [];
                let select_type_addresses = $('#address-fields-list select[id$=domiciliations]').first().children();
                select_type_addresses.each(function () {
                    all_type_addresses.push($(this).val());
                });
                let line_address1 = [];
                let line_address2 = [];
                let postal_code = [];
                let locality = [];

                let isCedex = [];
                $('#address-fields-list >li.row div.form-address').each(function () {

                    type_address = $(this).find('select[id$=domiciliations]').val();
                    if (arrayContainsArray(all_type_addresses, type_address)) {
                        line_address1.push($(this).find('input[id$=addressLine1]').val());
                        line_address2.push($(this).find('input[id$=addressLine2]').val());
                        postal_code.push($(this).find('input[id$=postalCode]').val());
                        locality.push($(this).find('input[id$=locality]').val());
                        isCedex.push($(this).find('input[id$=isCedex]').prop('checked'));
                    }
                });
                cabinet_group.html('Veuillez patienter ...');
                $.ajax({
                    type: "POST",
                    url: "/core/address",
                    data: {
                        addressType: type_address,
                        addressLine1: line_address1,
                        addressLine2: line_address2,
                        postalCode: postal_code,
                        locality: locality,
                        mainJobName: mainJobName,
                        isCedex: isCedex
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.success === true) {
                            matchAddress = data.hasOneMatch ?? null;
                            if (data.hasOneMatch) {
                                let result = "";
                                if (data.personLegalFound.length > 0) {

                                    for (let i = 0; i < data.personLegalFound.length; i++) {
                                        let input_person_legal_found;
                                        let input_address;

                                        if (data.personLegalFound[i]['linked']) {
                                            input_person_legal_found = "<input class='form-check-input personLegal-checkbox' name='personLegals["+ i +"][id]' value='" + data.personLegalFound[i]['id'] + "' type='checkbox' checked='checked'>";
                                        } else {
                                            input_person_legal_found = "<input class='form-check-input personLegal-checkbox' name='personLegals["+ i +"][id]' value='" + data.personLegalFound[i]['id'] + "' type='checkbox'>";
                                        }
                                        input_address = "<label class='form-check-label'><input class='d-none' type='checkbox' name='personLegals["+ i +"][idAddress]' value='" + data.personLegalFound[i]['idAddress'] + "' checked='checked'></label>";
                                        list_person_found += "<div class='form-check'>" +
                                            "    <label class='form-check-label'>" + input_person_legal_found +
                                            "    <span class='form-check-sign'></span>" + data.personLegalFound[i]['corporateName'] + " (" + data.personLegalFound[i]['address'] + ")" +
                                            "</label>" +
                                            "</div>" +
                                            "<div class='form-check'>" + input_address + "</div>";
                                    }
                                    list_person_found += "<input class='d-none' type ='checkbox' checked='checked' name='personLegalNumber' value='" + data.personLegalFound.length + "'>";
                                    let personLegal = "<h5>Associer la personne aux cabinets : </h5>" + list_person_found + "<br>";
                                    result += personLegal;
                                }

                                if (data.personNotAssociatedButWithSameAddress.length > 0) {
                                    $('.newPersonLegalForm').removeClass('d-none');

                                    list_person_found = "";

                                    for (let i = 0; i < data.personNotAssociatedButWithSameAddress.length; i++) {
                                        list_person_found += "<div class='form-check newPersonLegalForm'>" +
                                            "<label class='form-check-label'>" +
                                            "<input id='" + data.personNotAssociatedButWithSameAddress[i]['id'] + "' class='newPersonLegalCheckbox' name='newPersonLegals["+ i +"][create]' type='checkbox'>" +
                                            "<span class='form-check-sign'></span>" +
                                            data.personNotAssociatedButWithSameAddress[i]['lastName'] + " " + data.personNotAssociatedButWithSameAddress[i]['firstName'] +
                                            " (" + data.personNotAssociatedButWithSameAddress[i]['address'] + ")" +
                                            "<input class='d-none' name='newPersonLegals["+ i +"][personNatural]' value='" + data.personNotAssociatedButWithSameAddress[i]['id'] + "'>" +
                                            "<input class='d-none' type='checkbox' name='newPersonLegals["+ i +"][idAddress]' value='" + data.personNotAssociatedButWithSameAddress[i]['idAddress'] + "' checked='checked'>" +
                                            "</label>" +
                                            "</div>" +
                                            "<input class='form-control d-none' type='text' name='newPersonLegals["+ i +"][corporateName]' id='newPersonLegalCorporateName" + data.personNotAssociatedButWithSameAddress[i]['id'] + "' placeholder='Nom du cabinet ou de la société'><br>";
                                    }
                                    list_person_found += "<input class='d-none' name='personSameAddressNumber' value='" + data.personNotAssociatedButWithSameAddress.length + "' type='checkbox' checked='checked'>";

                                    result += "<br><h5>Créer un (des) cabinet(s) pour rattacher la personne avec :</h5>" + list_person_found;
                                }
                                cabinet_group.html("<div class='row justify-content-center mt-lg-5'>" +
                                    "<div class='col-md-6 col-sm-12 text-left' id='group-cab'>" + result + "</div></div>");
                            } else {
                                cabinet_group.html('<div class="row justify-content-center mt-lg-5">Aucune personne ayant la même adresse</div>');
                                skipAddressChecking();
                            }
                        } else {
                            swal({
                                title: "Attention",
                                text: data.message,
                                type: 'warning'
                            });
                        }
                    },
                    complete: () => {
                        hideWaitingSpinner();
                    }
                });
            } else {
                cabinet_group.html('<div class="row justify-content-center mt-lg-5">Aucune adresse à rechercher</div>');
                skipAddressChecking();
            }
        }
        hideWaitingSpinner();
        getCurrentTab();
    });

    /**
     * Vérification doublons
     */
    let last_name_input = $('#person_natural_lastName');
    let first_name_input = $('#person_natural_firstName');

    last_name_input.keyup(function () {
        if ($(this).val().length > 3 && first_name_input.val().length > 3) {
            onFullNameChange();
        }
    });

    first_name_input.keyup(function () {
        if ($(this).val().length > 3 && first_name_input.val().length > 3) {
            onFullNameChange();
        }
    });

    let ajax_request_check_name;

    function onFullNameChange() {
        let first_name = first_name_input.val();
        let last_name = last_name_input.val();

        if (ajax_request_check_name && ajax_request_check_name.readyState != 4) {
            ajax_request_check_name.abort();
        }

        ajax_request_check_name = $.ajax({
            type: "POST",
            url: "/core/person/natural/ajax/uniqueness",
            data: {
                'last_name': last_name,
                'first_name': first_name
            },
            dataType: 'json',
            success: function (data) {
                if(data !== false){
                    let data_tab = data.result;
                    if (data_tab.length > 0) {
                        let text = "<div>Personnes ayant la même combinaison nominale :</div>";
                        let title = "";
                        $.each(data_tab, function (key, element) {
                            title += '[' + element.id + '] ' + element.last_name + " " + element.first_name;
                            title += " / ";
                            title += (element.mails !== undefined) ? element.mails.join(', ') : 'Aucun mail';
                            title += " / ";
                            title += (element.phone_numbers !== undefined) ? element.phone_numbers.join(', ') : 'Aucun téléphone';
                        });
                        $.each(data_tab, function (key, element) {
                            text += "<small><hr style='border-top-color:lightgrey'><div class='row justify-content-center' title='" + title + "'>";
                            text += "<div class='col-md-4 text-center'><a href='/core/person/natural/" + element.id + "' target='_blank'>" + element.last_name + " " + element.first_name + "</a></div>";
                            text += "<div class='col-md-4 text-center'>";
                            $.each(element.mails, function (mail_key, mail) {
                                text += "<div>" + mail + "</div>";
                            });
                            text += "</div><div class='col-md-4 text-center'>"+element.postalCode+ ", "+ element.city +"</div></div></small>";
                        });
                        confirmStopAndRedirect(text,"/core/person/natural/");
                    }
                }
            }
        });
    }
})
;

/*Return TRUE si un ou plusieurs éléments du tableau subset est dans le tableau superset et FALSE sinon*/
function arrayContainsArray(superset, subset) {
    if (0 === subset.length) {
        return false;
    }
    return subset.every(function (value) {
        return (superset.indexOf(value) >= 0);
    });
}

/**
 * Determines wether or not the address tab must be displayed
 * @param value
 */
function handleJobName(value){
    const substituteJobNames = JSON.parse(document.getElementById('substitute-jobnames').getAttribute('data-value'));
    if(substituteJobNames.includes(parseInt(value))){
        hideCheckAddressTab();
    }else{
        showCheckAddresstab();
    }
}

function hideCheckAddressTab()
{
    document.getElementById('person-natural-step-search-group-nav').classList.add('invisible');
}

function showCheckAddresstab()
{
    document.getElementById('person-natural-step-search-group-nav').classList.remove('invisible');
}