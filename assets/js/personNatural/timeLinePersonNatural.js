var discussions = $("li[class^=discussion-]");
var period_types = [];
$("#select-discussion-period option[value!='']").each(function(){
    period_types.push($(this).val());
});
filterDiscussion();
$("#select-discussion-type").change(filterDiscussion);
$("#select-discussion-service").change(filterDiscussion);
$("#select-discussion-period").change(filterDiscussion);

function filterDiscussion() {
    var id_list_type = $("#select-discussion-type").children("option:selected").val();
    var id_list_service = $("#select-discussion-service").children("option:selected").val();
    var period = $("#select-discussion-period").children("option:selected").val();
    var stop = false;

    discussions.addClass("d-none");
    if (id_list_type === "") {
        if(id_list_service === ""){
            if(period === ""){
                discussions.removeClass("d-none");
            }else{
                period_types.forEach(function(type){
                    if(!stop){
                        discussions.each(function(){
                            if($(this).hasClass("discussion-period-" + type)){
                                $(this).removeClass("d-none");
                            }
                        });
                    }
                    if(type === period){
                        stop = true;
                    }
                });
            }
        }else{
            if(period === ""){
                discussions.each(function () {
                    if ($(this).hasClass("discussion-service-" + id_list_service)) {
                        $(this).removeClass("d-none");
                    }
                });
            }else{
                period_types.forEach(function(type){
                    if(!stop) {
                        discussions.each(function () {
                            if ($(this).hasClass("discussion-period-" + type) && $(this).hasClass("discussion-service-" + id_list_service)) {
                                $(this).removeClass("d-none");
                            }
                        });
                    }
                    if(type === period){
                        stop = true;
                    }
                });
            }
        }
    } else {
        if(id_list_service === ""){
            if(period === ""){
                discussions.each(function () {
                    if ($(this).hasClass("discussion-type-" + id_list_type)) {
                        $(this).removeClass("d-none");
                    }
                });
            }else{
                period_types.forEach(function(type){
                    if(!stop) {
                        discussions.each(function () {
                            if ($(this).hasClass("discussion-period-" + type) && $(this).hasClass("discussion-type-" + id_list_type)) {
                                $(this).removeClass("d-none");
                            }
                        });
                    }
                    if(type === period){
                        stop = true;
                    }
                });
            }
        }else{
            if(period === ""){
                discussions.each(function () {
                    if ($(this).hasClass("discussion-type-" + id_list_type) && $(this).hasClass("discussion-service-" + id_list_service)) {
                        $(this).removeClass("d-none");
                    }
                });
            }else{
                period_types.forEach(function(type){
                    if(!stop) {
                        discussions.each(function () {
                            if ($(this).hasClass("discussion-period-" + type) && $(this).hasClass("discussion-type-" + id_list_type) && $(this).hasClass("discussion-service-" + id_list_service)) {
                                $(this).removeClass("d-none");
                            }
                        });
                    }
                    if(type === period){
                        stop = true;
                    }
                });
            }
        }
    }
}