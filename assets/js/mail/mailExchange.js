'use strict';

import PerfectScrollbar from "perfect-scrollbar";

new PerfectScrollbar('body');
new PerfectScrollbar('#mail-message');
new PerfectScrollbar('#inBox');
new PerfectScrollbar('#sentBox');
new PerfectScrollbar('#attachments-list');

$(document).ready(function () {
    let mail_page = 1;
    let page_number = 0;
    let sent_box = $(".link-sentBox");
    let listMail = $(".list-mail-sample");

    //CHOISIR LE TYPE DE MAIL A ENVOYER
    listMail.on('click', function () {
        listMail.each(function () {
            $(this).removeClass('selected-mail');
        });
        let pre_written_mail_iD = $(this).data('mail-sample-id');
        if (typeof pre_written_mail_iD !== 'undefined') {
            $.ajax({
                url: '/mail/manager/show/mail',
                type: 'POST',
                dataType: 'json',
                data: {'preWrittenMailId': pre_written_mail_iD},
                success: function (data) {
                    $("#subject").val(data.title);
                    tinymce.get("send-message").setContent(data.content);
                }
            });
        } else {
            $("#subject").val('');
            tinymce.get("send-message").setContent('');
        }
        $("#attachments-received").html('');
        $("#sending-mail-form").removeClass('d-none');
        $("#email-address").val($(this).data('client-mail'));
        $(".error").html('');
        $("#message-box").addClass('d-none');
        $(".mail-content").removeClass('d-none');
    });

    //BASCULER ENTRE MAILS RECUS ET ENVOYES
    $(".box-choice").on('click', function () {
        $("#message-box").addClass('d-none');
        $("#new-mail").removeClass('active');
        $("#sending-mail-form").addClass('d-none');
        $(".error").html('');
        $("#mail-message").html('');
        $(".mail-content").addClass('d-none');
        $('.box-choice').each(function () {
            $(this).removeClass('bg-' + badge_colors[$('.badge.filter.active').data('color')]);
        });
        $('#new-mail').removeClass('bg-' + badge_colors[$('.badge.filter.active').data('color')]);
    });

    //BASCULER VERS LA REDACTION DE MAIL
    $("#new-mail").on('click', function () {
        $(".pagination").addClass('d-none');
        $('.box-choice').removeClass('bg-' + badge_colors[$('.badge.filter.active').data('color')]);
    });

    //BASCULER VERS LES MAILS RECUS
    $(".link-inBox").on('click', function () {
        $("#answer").removeClass('d-none');
        $(".pagination").addClass('d-none');
    });

    //BASCULER VERS LES MAILS ENVOYES
    sent_box.on('click', function () {
        $("#answer").addClass('d-none');
        $("#attachments-received").html('');
        $(".pagination").removeClass('d-none');
        $("#current-mail-page").html(mail_page);
        if (!$(this).hasClass('active')) {
            if ($(this).data('client-id') != null) {
                showListMail(mail_page, -1, $(this).data('client-id'));
            } else {
                showListMail(mail_page, $(this).data('staff-id'), -1);
            }
        }
    });

    //REPONDRE A UN MAIL
    $("#answer").on('click', function () {
        $("#sending-mail-form").removeClass('d-none');
        tinymce.get("send-message").setContent(($('#mail-message').html().trim()));
        $(this).addClass('d-none');
        $(".error").html('');
        $("#message-box").addClass('d-none');
    });

    //ABANDON D'ENVOI DE MAIL
    $("#cancel").on('click', function () {
        $("#sending-mail-form").addClass('d-none');
        $("#answer").removeClass('d-none');
        tinymce.get("send-message").setContent('');
        $(".error").html('');
        $(".mail-content").addClass('d-none');
    });

    //LECTURE DES MAILS ENVOYES
    $("#sentBox").on('click', '.list-mail', function () {

        let current_mail = $(this);
        $(".list-mail").each(function () {
            if ($(this) !== current_mail) {
                $(this).removeClass('bg-' + badge_colors[$('.badge.filter.active').data('color')]);
            }
        });
        let attachments = [];
        let mail_uid = $(this).data('mail-uid');
        let mail_box_name = $(this).data('mail-box');
        $.ajax({
            url: '/mail/manager/read/mail',
            type: 'POST',
            dataType: 'json',
            data: {
                'mailBox': mail_box_name,
                'mailSender': $(this).data('mail-user-mail'),
                'mailUid': mail_uid,
                'mailReceiver': $(this).data('mail-to'),
                'mailAttachments': JSON.stringify(attachments),
            }, success: function (data) {
                let attachmentsList;
                $("#received-card").removeClass('d-none');
                $("#sender-address").html('<label>Destinataire: </label> ' + data.replyTo);
                $("#subject-message").html('<label>Objet: </label> ' + data.subject);
                $("#subject").val(data.subject);
                $("#email-address").val(data.replyTo);
                $("#mail-message").html(data.message);

                if (data.attachments.length > 0) {
                    attachmentsList = "<br><span class='btn btn-round btn-sm'>Pièces Jointes</span><br>";
                    data.attachments.forEach((attachment) => {
                        attachmentsList += '<a title="' + attachment.file + '" href="' + attachment.link + '" target="_blank">' + substrfile_name(attachment.file) + '</a><br>';
                    });
                } else {
                    attachmentsList = "";
                }
                $("#attachments-received").html(attachmentsList);
                $("#message-box").removeClass('d-none');
                $(".mail-content").removeClass('d-none');
            }
        });
        $(this).removeClass("unseen");
        $(this).addClass('bg-' + badge_colors[$('.badge.filter.active').data('color')]);
        $(".link-" + mail_box_name).addClass('active');
        $("#new-mail").removeClass('active');
        if (!($("#sending-mail-form").hasClass('d-none'))) {
            $("#sending-mail-form").addClass('d-none');
        }
    });

    //GESTION DES PIECES JOINTES
    function substrfile_name(full_file_name) {
        let file_name = full_file_name;
        if (full_file_name.length > 13) {
            file_name = full_file_name.substr(0, 10);
            return file_name.padEnd(13, '.');
        }
        return file_name.padEnd(13, ' ');
    }

    let all_attachments = [];
    $('#new-attachment').click(function () {
        $('#upload-input-attachment').click();
    });

    $('#upload-input-attachment').change(function () {
        for (let i = 0; i < $(this)[0].files.length; ++i) {
            all_attachments.push($(this)[0].files[i]);
            let full_file_name = $(this)[0].files[i].name;
            let file_name = substrfile_name(full_file_name);
            $("#attachments-list").append('<div class="file-uploaded">' +
                '<small  title="' + full_file_name + '">' + file_name + '</small>' +
                '<a class="delete-attachment" href="#" title="supprimer" data-file-id="' + i + '">' +
                '<i class="fa fa-times"></i>' +
                '</a>' +
                '</div>');
        }
    });

    function removeArrayElement(array, index) {
        let resultArray = [];
        for (let i = 0; i < array.length; i++) {
            if (index !== i) {
                resultArray.push(array[i]);
            }
        }
        return resultArray;
    }

    $("#attachments-list").on('click', '.delete-attachment', function (e) {
        e.preventDefault();
        if (all_attachments.length === 1) {
            all_attachments = [];
            $("#attachments-list").html('Aucune pièce jointe');
        } else {
            all_attachments = removeArrayElement(all_attachments, $(this).data('file-id'));
        }
        $(this).parent().addClass('d-none');
    });

    //ENVOI DE MAIL
    $("#send-mail").on('submit', function (e) {
        e.preventDefault();
        $(".error").html('');
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let subject = $("#subject").val();
        let recepient_address = $("#email-address").val();
        let message = tinymce.get("send-message").getContent();
        let client_id = $("#email-address").data('client-id');
        let formData = new FormData();

        if (all_attachments.length === 0) {
            formData.delete("attachments[]");
        } else {
            all_attachments.forEach((value, index) => {
                if (index == 0) {
                    formData.set("attachments[]", value);
                } else {
                    formData.append("attachments[]", value);
                }
            });
        }

        formData.append('mailMessage', message);
        formData.append('mailSubject', subject);
        formData.append('mailReceiver', recepient_address);
        formData.append('client_id', client_id);
        showWaitingSpinner();
        if (subject !== '' && reg.test(recepient_address) === true && message !== '') {
            $.ajax({
                type: 'POST',
                url: '/mail/manager/send/mail',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    hideWaitingSpinner();
                    if (data.status) {
                        swal({
                            type: 'success',
                            text: data.message
                        });
                        tinymce.get("send-message").setContent('');
                        $("#subject").val('');
                        formData.delete("attachments[]");
                        all_attachments = [];
                        $("#attachments-list").html('');
                    } else {
                        swal({
                            type: 'error',
                            title: 'Echec',
                            text: data.message
                        });
                    }
                },
                error: function () {
                    hideWaitingSpinner();
                    swal({
                        type: 'error',
                        title: 'Oups...!',
                        text: 'Une erreur est survenue'
                    });
                }
            });
        } else {
            if (reg.test(recepient_address) === false) {
                $("#error-mail").html('Adresse email non valide');
            }
            if (subject === '') {
                $("#error-subject").html('Objet du mail obligatoire');
            }
            if (message === '') {
                $("#error-message").html('Message du mail obligatoire');
            }
        }
        hideWaitingSpinner();
    });

    //pagination des mails
    $(".pagination").on('click', function (e) {
        if (!$("#page-search").hasClass('d-none') && (e.pageX < 440 || 650 < e.pageX)) {
            $("#page-search").addClass('d-none');
            $("#current-mail-page").removeClass('d-none');
        }
    });

    $("#next-mail-page").on('click', function () {
        if (mail_page < page_number) {
            mail_page++;
            if (sent_box.data('client-id') != null) {
                showListMail(mail_page, -1, sent_box.data('client-id'));
            } else {
                showListMail(mail_page, sent_box.data('staff-id'), -1);
            }
        }
    });

    $("#previous-mail-page").on('click', function () {
        if (mail_page > 1) {
            mail_page--;
            if (sent_box.data('client-id') != null) {
                showListMail(mail_page, -1, sent_box.data('client-id'));
            } else {
                showListMail(mail_page, sent_box.data('staff-id'), -1);
            }
        }
    });

    $("#first-mail-page").on('click', function () {
        if (mail_page > 1) {
            mail_page = 1;
            if (sent_box.data('client-id') != null) {
                showListMail(mail_page, -1, sent_box.data('client-id'));
            } else {
                showListMail(mail_page, sent_box.data('staff-id'), -1);
            }
        }
    });

    $("#last-mail-page").on('click', function () {
        if (page_number < 1) {
            page_number = Math.ceil($(".pagination").data('page-number'));
        }
        if (mail_page < page_number) {
            mail_page = page_number;
            if (sent_box.data('client-id') != null) {
                showListMail(mail_page, -1, sent_box.data('client-id'));
            } else {
                showListMail(mail_page, sent_box.data('staff-id'), -1);
            }
        }
    });

    //recherche d'une page de la liste des mails
    $("#current-mail-page").dblclick(function () {
        $(this).addClass('d-none');
        $("#input-page-number").val(mail_page);
        $("#page-search").removeClass('d-none');
        if (page_number < 1) {
            page_number = 1;
        } else {
            page_number = Math.ceil($(".pagination").data('page-number'));
        }
    });

    $("#page-search").on('submit', function (e) {
        e.preventDefault();
        let page = parseInt($("#input-page-number").val());
        if (page != mail_page && 0 < page && page <= page_number) {
            mail_page = page;
            if (sent_box.data('client-id') != null) {
                showListMail(mail_page, -1, sent_box.data('client-id'));
            } else {
                showListMail(mail_page, sent_box.data('staff-id'), -1);
            }
        }
        $("#current-mail-page").removeClass('d-none');
        $(this).addClass('d-none');
    });

    //afficher la liste des mails
    function showListMail(mail_page, staffId, personId) {
        $.ajax({
            url: '/mail/manager/paginate/mail',
            type: 'POST',
            data: {'page': mail_page, 'staffId': staffId, 'personId': personId},
            success: function (data) {
                let color = $('body').hasClass('white-content') ? 'list-mail-color' : '';
                page_number = data.pageNumber;
                let list = "";
                data.items.forEach((item) => {
                    list += '<div id="heading' + item['tag'] + '" class="btn list-mail ' + color + '"' +
                        '         data-mail-box="sentBox"' +
                        '         data-mail-uid="' + item['tag'] + '"' +
                        '         data-mail-to="' + item['clientMail'] + '"' +
                        '         data-user-mail="' + item['userMail'] + '"' +
                        '         data-staff-id="' + staffId + '"' +
                        '         data-person-id="' + personId + '"' +
                        '         data-mail-subject="' + item['subject'] + '">' +
                        '        <div class="subject">' + item['subject'] + '</div><br>' +
                        '        <small>Destinataire: ' + item['clientMail'] +
                        '        </small><br>' +
                        '        <small>Envoyé le ' + item['creationDate'] + '</small><br>' +
                        '        <small>' + item['status'] + ' le ' + item['statusDate'] + '</small><br>' +
                        '    </div>'
                });
                $("#current-mail-page").html(mail_page);
                $("#sentBox").html('');
                return $("#sentBox").append(list);
            }
        });
    }
});
