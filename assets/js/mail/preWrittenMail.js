$(document).ready(function () {

    //ENREGISTRER LE CONTENU DU MAIL PREDEFINI
    $("#mail-sample-form").on('submit', function (e) {
        e.preventDefault();
        let mail_title = $("#mail-sample-title").val();
        let mail_content = $("#mail-sample-content").val();
        let form_status = $(this).data('status');
        let mail_id = $(this).data('mail-sample-id');

        $.ajax({
            url: '/mail/manager/save/mail',
            type: 'POST',
            dataType: 'json',
            data: {
                'mail-title': mail_title,
                'mail-content': mail_content,
                'form-status': form_status,
                'mail-id': mail_id
            },
            success: function (data) {

                if (!data.save) {
                    $("#sample-saving-error").html(data.message);
                } else {
                    $("#sample-saving-error").html('');
                    swal({
                        type: 'success',
                        text: "Le contenu du mail " + data.message + " a bien été enregistré"
                    });
                    $("#mail-sample-form").addClass('d-none');
                    tinymce.get("mail-sample-content").setContent('');
                    $("#mail-sample-title").val('');
                }
            }
        });
    });

    //AFFICHER LE CONTENU DU MAIL PREDEFINI
    $(".list-mail-sample").on('click', function () {
        let mail_id = $(this).data('mail-sample-id');
        $.ajax({
            url: '/mail/manager/show/mail',
            type: 'POST',
            dataType: 'json',
            data: {'preWrittenMailId': mail_id},
            success: function (data) {
                $("#mail-sample-form").removeClass('d-none');
                $("#mail-sample-form").attr("data-mail-sample-id", mail_id);
                $("#mail-sample-title").val(data.title);
                tinymce.get("mail-sample-content").setContent(data.content);
            }
        });
    });

    //ABANDON CREATION OU MODIFICATION MAIL PREDEFINI
    $("#cancel-sample-mail").on('click', function () {
        $("#mail-sample-form").addClass('d-none');
        tinymce.get("mail-sample-content").setContent('');
        $("#mail-sample-title").val('');
    })
});
