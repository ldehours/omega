import tinymce from 'tinymce/tinymce.min';
import icons from 'tinymce/icons/default/icons.min';
import silver from 'tinymce/themes/silver';
import link from 'tinymce/plugins/link';
import image from 'tinymce/plugins/image';
import print from 'tinymce/plugins/print';
import table from 'tinymce/plugins/table';
import lists from 'tinymce/plugins/lists';
import advlist from 'tinymce/plugins/advlist/plugin';
import spellchecker from 'tinymce/plugins/spellchecker';
import pagebreak from 'tinymce/plugins/pagebreak/plugin';
import preview from 'tinymce/plugins/preview';
import anchor from 'tinymce/plugins/anchor';
import hr from 'tinymce/plugins/hr';

//INITIALISATION DE L'EDITEUR TINYMCE
tinymce.init({
    selector: '.tinymce',
    theme: 'silver',
    min_height: 350,
    plugins: ['link', 'print', 'table', 'lists', 'advlist', 'spellchecker', 'preview', 'pagebreak', 'anchor', 'hr'],
    menubar: "file edit view insert format",
    paste_data_images: true,
    toolbar2: 'formatselect fontsizeselect fontselect bold italic underline strikethrough superscript subscript forecolor backcolor alignleft aligncenter alignright alignjustify numlist bullist outdent indent table lists date link print preview',
    image_advtab: true,
    file_picker_callback: function (callback, value, meta) {
        if (meta.filetype == 'image') {
            $('#upload-image').trigger('click');
            $('#upload-image').on('change', function () {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    callback(e.target.result, {
                        alt: ''
                    });
                };
                reader.readAsDataURL(file);
            });
        }
    }
});
