import '../css/base.css';

/**
 * Create a html element with the font-awesome classes to display a spinner icon
 * + add the spinner-icon class to animate it
 *
 * @param extraClasses
 * @returns {HTMLElement}
 */
export function createSpinner(extraClasses = []) {
    const spinner = document.createElement('i');
    const classes = ['fas', 'fa-spinner', 'spinner-icon', ...extraClasses];
    spinner.classList.add(...classes);
    return spinner;
}

export const Placement = {
    top: 'top',
    bottom: 'bottom',
    right: 'right',
    left: 'left'
};

/**
 * Build a tooltiped icon
 *
 * @param iconClass
 * @param title
 * @param placement
 * @param extraClasses
 * @param extraOptions
 */
export function createTooltipedIcon(
    iconClass = ['fas', 'fa-user'],
    title = 'This is a tooltip',
    placement = 'top',
    extraClasses = [],
    extraOptions = []
) {
    const element = document.createElement('i');
    const classes = [...iconClass, ...extraClasses];
    element.classList.add(...classes);
    element.setAttribute('title', title);
    element.setAttribute('data-placement', placement);
    element.setAttribute('data-toggle', 'tooltip');
    extraOptions.forEach(option => {
        element.setAttribute(option.attribute, option.value);
    });

    return element;
}

/**
 * Returns an object that matches the requirements for an html element creation
 * @param attribute
 * @param value
 * @returns {{attribute: *, value: *}}
 */
export function newAttribute(attribute, value) {
    return {
        attribute: attribute,
        value: value
    };
}

const elementDefaultOptions = {
    tag: 'div',
    text: '',
    classes: [],
    attributes: [],
    children: []
};

/**
 * Merge the elementDefaultOptions object with the given options
 * @param options
 * @returns {{children: [], classes: [], attributes: [], tag: string, text: string}}
 */
function mergeOptions(options) {
    return {...elementDefaultOptions, ...options};
}

/**
 * Creates a DOM element
 * @param options
 */
export function createDOMElement(options) {
    options = mergeOptions(options);
    const element = document.createElement(options.tag);
    element.innerHTML = options.text;
    element.classList.add(...options.classes);
    options.attributes.forEach(data => {
        element.setAttribute(data.attribute, data.value);
    });
    if (options.children.length > 0) {
        element.append(...options.children);
    }
    return element;
}

/**
 * Returns a random number between 0 and max
 * @param max
 * @returns {number}
 */
export function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
