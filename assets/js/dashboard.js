$(document).ready(function(){
    let info_client = $('.content-personnel .client-name-hover');
    let info_bubble = $('#info-bubble');

    // attribution d'id au services cards et au div qui leur appartient
    let cards_services = $('.all-cards-services').children();
    let cards_services_div = $('.all-card-div').children();

    for(let i = 0; i < cards_services.length; i ++){
        let int = i;
        $(cards_services[i]).attr('id' , 'service_one_card_' + int);
        $(cards_services_div[i]).attr('id' , 'service_one_card_' + int + "_div");
    }

    $('.cards-services .card-service-cr').click(function () {
        // récupère l'id de la card
        let get_id_service_card = $(this).attr("id");
        // crée l'id de la div concernant la card
        get_id_service_card = get_id_service_card + "_div";
        // ajoute class display none à toutes les div de service sauf à celle demandée
        for(let i = 0; i < cards_services_div.length; i ++){
            let element = $(cards_services_div[i]).attr('id');
            if (element != get_id_service_card) {
                $("#" + element).addClass('display-none');
                $("#" + element).removeClass('display-block');
            }
            if (element === get_id_service_card) {
                $("#" + element).addClass('display-block');
                $("#" + element).removeClass('display-none');
            }
        }

    });

    $(".table").on('mouseover', ".personnal-info", function (event) {
        let client = $(this);
        let datas = $(client).data("infoClient");
        let p_last_lastcontact = $('#fiche-client-lastcontact');
        let p_last_lastcontactDate = $('#fiche-client-lastcontactDate');
        let ul_fiche_clients = $('#fiche_client_notes');

        let top = $(this).offset().top;
        let left = $(this).offset().left;

        $(info_bubble).removeClass('display-none');
        $(info_bubble).addClass('display-block');

        $(ul_fiche_clients).empty();
        $(p_last_lastcontact).text(datas.lastFirstNameContact + " " + datas.lastLastNameContact);
        $(p_last_lastcontactDate).text(datas.lastContactDate);

        if (datas.commentaires != undefined) {
            // si des commentaires existent
            if (datas.commentaires.length >= 1) {
                for (let i = 0; i < datas.commentaires.length; i++) {
                    let li = $('<li class="li-style">');
                    $(li).text(datas.commentaires[i]);
                    $(ul_fiche_clients).append(li);
                }
            } else {
                $(ul_fiche_clients).text("Aucune info");
            }
        }
        $(info_bubble).offset({
            top: top - 300,
            left: left + 65
        })
    });

    $(".table").on('mouseleave', info_client, function () {
        $(info_bubble).removeClass('display-block');
        $(info_bubble).addClass('display-none');
    });
})
