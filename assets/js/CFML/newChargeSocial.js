$(function () {

    // URSSAF
    let title_th_table_urssaf = $('#title_th_table_urssaf');
    // tbody du table
    let tbody_table_urssaf = $('#tbody_table_urssaf');
    //////////

    // CARMF
    let title_th_table_carmf = $('#title_th_table_carmf');
    // tbody du table
    let tbody_table_carmf = $('#tbody_table_carmf');

    function createTable(year) {
        // input année
        let input_table_years = $('<input>');
        $(input_table_years).val(year);
        let txt_input_table_years = $('<p>Année</p>');
        let div_input_table_years = $('<div>');
        $(div_input_table_years).append(txt_input_table_years, input_table_years);

        // input maladie
        let input_table_disease = $('<input>');
        let txt_input_table_disease = $('<p>MALADIE</p>');
        if (year === 2020) {
            $(input_table_disease).val('CPAM');
        }
        let div_input_table_disease = $('<div>');
        $(div_input_table_disease).append(txt_input_table_disease, input_table_disease);

        // array pour tableau carmf ///
        let title_th_table_urssaf_array_carmf = [
            "",
            "RETRAITE",
            "MAJORATION",
            "ETAT"
        ];
        let title_th_table_urssaf_side_carmf = [
            "RB" + year,
            "CV" + year,
            "ASV " + year,
            "ID " + year,
            "REGUL  " + (year - 1),
            "DIV 1",
            "DIV 2",
            "DIV 3",
            "DIV 4",
            "DIV 5",
            "TOTAUX"
        ];
        let data_input_carmf = {
            2019: [
                "100.00",
                "0.00"
            ],
            2020: [
                "200.00",
                "0.00"
            ]
        };
        //// fin carmf array ///

        // array pour tableau urssaf ///
        let title_th_table_urssaf_array_urssaf = [
            "Année",
            "AF",
            "MALADIE",
            "CSG déductible",
            "CSG non déductible",
            "CFP",
            "CURPS",
            "MAJORATION",
            "TOTAL",
            "ETAT"
        ];
        let title_th_table_urssaf_side_urssaf = [
            "1T" + year,
            "2T" + year,
            "3T" + year,
            "4T" + year,
            "DIV 1",
            "DIV 2",
            "DIV 3",
            "DIV 4",
            "DIV 5",
            "DIV 6",
            "TOTAUX",
            "PROVISIONNELLES " + year,
            "REGULARISATIONS " + (year - 1),
            "DIVERS",
            "TOTAUX"
        ];
        let data_input_urssaf = {
            2019: [
                "10.00",
                "20.00",
                "30.00",
                "40.00",
                "50.00",
                "0.00",
                "0.00",
                "150.00"
            ],
            2020: [
                "10.00",
                "20.00",
                "30.00",
                "40.00",
                "50.00",
                "0.00",
                "0.00",
                "150.00"
            ]
        };
        //// fin urssaf array ///

        // For pour urssaf ////////////////////
        // for pour inserer les td du dessus
        for (let i = 0; i < 10; i++) {
            // pour les th qui sont au dessus
            let th_above = $('<th scope="col" class="th-padding bg-gradient-secondary">');
            let p_above = $('<p>');
            $(p_above).append(title_th_table_urssaf_side_urssaf[i]);

            // pour les th qui sont au dessus
            switch (i) {
                case 0:
                    $(th_above).append(div_input_table_years);
                    $(title_th_table_urssaf).append(th_above);
                    break;
                case 2:
                    $(th_above).append(div_input_table_disease);
                    $(title_th_table_urssaf).append(th_above);
                    break;
                default:
                    $(th_above).text(title_th_table_urssaf_array_urssaf[i]);
                    $(title_th_table_urssaf).append(th_above);
            }
        }
        // for pour inserer les td sur le côté
        for (let i = 0; i < 15; i++) {

            // pour les th qui sont sur le cotés
            let tr_side = $('<tr>');
            let th_side = $('<th scope="col" class="bg-gradient-secondary text-center">');
            let p_side = $('<p>');
            let input_side = $('<input>');
            $(p_side).append(title_th_table_urssaf_side_urssaf[i]);

            // pour les th qui sont sur le cotés pour les 4 premiers aucun input à ajouté
            if (i <= 3) {
                $(th_side).append(p_side);
                $(tr_side).append(th_side);
                $(tbody_table_urssaf).append(tr_side);

            } else {
                // ajout input dans le th_side
                if (i >= 10) {
                    // sans input
                    $(th_side).append(p_side);
                } else {
                    // avec input
                    $(th_side).append(p_side, input_side);
                }

                $(tr_side).append(th_side);
                $(tbody_table_urssaf).append(tr_side)
            }
        }
        // for pour insérer dans le tableau tous les inputs ou select
        let tbody_table_urssaf_child = $('#tbody_table_urssaf').children();
        for (let i = 0; i < 15; i++) {
            for (let p = 0; p < 9; p++) {
                let tr = tbody_table_urssaf_child[i];
                let th = $('<th scope="col" class="border text-center bg-gradient-primary">');
                let select = $('<select class="form-control" id="exampleFormControlSelect1">' +
                    '<option>Estimation</option>' +
                    '<option class="warning">Doc reçu</option>' +
                    '<option class="green">Confirmation</option>' +
                    '</select>');
                let input = $('<input class="input-data">');

                // disabled pour la 7 colonne le input
                switch (p) {
                    case 7 :
                        $(input).attr('disabled', true);
                        $(input).val(data_input_urssaf[year][p]);
                        $(th).append(input);
                        $(tr).append(th);
                        break;
                    case 8 :
                        if (i < 10) {
                            $(th).append(select);
                            $(tr).append(th);
                        } else {
                            $(th).append(" ");
                            $(tr).append(th);
                        }
                        break;
                    default:
                        $(input).val(data_input_urssaf[year][p]);
                        $(th).append(input);
                        $(tr).append(th);
                }
            }
        }
        // Fin For pour ursssaf ///////////////

        // For pour carmf ////////////////////
        // for pour inserer les td du dessus
        for (let i = 0; i < 4; i++) {
            // pour les th qui sont au dessus
            let th_above = $('<th scope="col" class="th-padding bg-gradient-secondary">');
            $(th_above).text(title_th_table_urssaf_array_carmf[i]);
            $(title_th_table_carmf).append(th_above);
        }
        // for pour inserer les td sur le côté
        for (let i = 0; i < 11; i++) {
            // pour les th qui sont sur le cotés
            let tr_side = $('<tr>');
            let th_side = $('<th scope="col" class="bg-gradient-secondary text-center">');
            let p_side = $('<p>');
            let input_side = $('<input>');
            $(p_side).append(title_th_table_urssaf_side_carmf[i]);

            // pour les th qui sont sur le cotés pour les 4 premiers aucun input à ajouté
            if (i <= 4) {
                $(th_side).append(p_side);
                $(tr_side).append(th_side);
                $(tbody_table_carmf).append(tr_side);

            } else {
                // ajout input dans le th_side
                if (i > 4 && i < 10) {
                    // avec input
                    $(th_side).append(p_side, input_side);
                } else {
                    // sans input
                    $(th_side).append(p_side);
                }
                $(tr_side).append(th_side);
                $(tbody_table_carmf).append(tr_side)
            }
        }
        // for pour insérer dans le tableau tous les inputs ou select
        // cette partie concerne le table ayant pour titre  "CARMF"
        let tbody_table_carmf_children = $('#tbody_table_carmf').children();
        for (let i = 0; i < 14; i++) {
            for (let x = 0; x < 3; x++) {
                let tr = tbody_table_carmf_children[i];
                let th = $('<th scope="col" class="border text-center bg-gradient-primary">');
                let select = $('<select class="form-control" id="exampleFormControlSelect1">' +
                    '<option>Estimation</option>' +
                    '<option class="warning">Doc reçu</option>' +
                    '<option class="green">Confirmation</option>' +
                    '</select>');
                let input = $('<input class="input-data">');

                switch (x) {
                    case 2 :
                        $(th).append(select);
                        $(tr).append(th);
                        break;
                    default:
                        $(input).val(data_input_carmf[year][x]);
                        $(th).append(input);
                        $(tr).append(th);
                }
            }
        }
        //Fin carmf

    }

    // création du tableau
    createTable(2020);

    // au click sur le btn 2020 ou 2019 celui s'affiche avec les données de sont année qui lui corresponde
    $('.btn-header').click(function () {
        $('.btn-header').removeClass('btn-primary');
        $(this).addClass('btn-primary');
        let year = $(this).attr("id");
        year = parseInt(year);
        // vide tous les tableau urssaf , carmf , ...
        $(title_th_table_urssaf).empty();
        $(tbody_table_urssaf).empty();
        $(title_th_table_carmf).empty();
        $(tbody_table_carmf).empty();
        // une fois le tableau vider on le recréer avec son année
        createTable(year);
    })
});
