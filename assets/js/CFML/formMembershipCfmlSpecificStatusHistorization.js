$(document).ready(function () {
    const formMembershipCfmlSpecificStatusHistorization = $('#formMembershipCfmlSpecificStatusHistorization');
    const urlFormMembershipCfmlSpecificStatusHistorization = $(formMembershipCfmlSpecificStatusHistorization).attr('action');
    const btnFormMembershipCfmlSpecificStatusHistorization = $('#membership_cfml_specific_status_historization_submit');
    let msg_event = $('#msg_event_form_membership_cfml_specific_status_historization_type');
    let before_last_specific_status = $('#before_last_specific_status');

    $(btnFormMembershipCfmlSpecificStatusHistorization).click(function (e) {
        e.preventDefault();
        $(msg_event).text('');
        $(msg_event).removeClass("text-success text-danger")
        let specificStatusChoice = $("#membership_cfml_specific_status_historization_oldSpecificStatus").val();
        $.ajax({
            url: urlFormMembershipCfmlSpecificStatusHistorization,
            type: 'POST',
            dataType: 'json',
            data: {'oldSpecificStatus': specificStatusChoice},
            success: function (data) {
                if (data.success) {
                    $(msg_event).addClass('text-success');
                    $(msg_event).text('Modification effectuée');
                    $(before_last_specific_status).text(data.status);
                    $("#close_modal_specific_status").click();
                } else {
                    $(msg_event).addClass('text-danger');
                    $(msg_event).text(data.message);
                }
            },
            error: function (error) {
                $(msg_event).text(error);
            }
        });
    })
})