//all btn header
let btns_header = $('.btn-header');

$(btns_header).click(function () {
    $(btns_header).removeClass('btn-primary');
    let current_id = $(this).attr('id');
    let content_divs = $('.all-div-content').children();
    $("#" + current_id).addClass('btn-primary');

    for (let i = 0; i < content_divs.length; i++) {
        let id = $(content_divs[i]).attr('id');

        if (id === current_id) {
            $(content_divs[i]).addClass('d-block');
        } else {
            if ($(content_divs[i]).hasClass('d-block')) {
                $(content_divs[i]).removeClass('d-block')
            }
            $(content_divs[i]).addClass('d-none');
        }

    }
})
