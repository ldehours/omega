$(function () {
    // btn principal
    let btns_header = $('.btn-header');
    $(btns_header).click(function () {
        let current_id = $(this).attr('id');
        $(btns_header).removeClass('btn-primary');
        $(this).addClass('btn-primary');
        let content_divs = $('.divs-display').children();
        for (let i = 0; i < content_divs.length; i++) {
            let id = $(content_divs[i]).attr('id');
            if (id === current_id) {
                $(content_divs[i]).addClass('d-block');
            } else {
                if ($(content_divs[i]).hasClass('d-block')) {
                    $(content_divs[i]).removeClass('d-block')
                }
                $(content_divs[i]).addClass('d-none');
            }
        }
    });

    // btn pour le suivi des pièces
    let btn_follow_part = $('.btn-header-suiv');

    $(btn_follow_part).click(function () {
        let current_id = $(this).attr('id');
        $(btn_follow_part).removeClass('btn-primary');
        $(this).addClass('btn-primary');
        let list_of_parts_received = $('#list-of-parts-received-div');
        let list_of_parts_not_requested = $('#list-of-parts-not-requested-div');
        current_id = current_id + "-div";
        switch (current_id) {
            case "list-of-parts-received-div":
                current_id = "#" + current_id;
                $(current_id).removeClass('d-none');
                $(current_id).addClass('d-block');

                $(list_of_parts_not_requested).removeAttr('class');
                $(list_of_parts_not_requested).addClass('d-none');
                break;

            case "list-of-parts-not-requested-div":
                current_id = "#" + current_id;
                $(current_id).removeClass('d-none');
                $(current_id).addClass('d-block');

                $(list_of_parts_received).removeAttr('class');
                $(list_of_parts_received).addClass('d-none');
                break;
        }
    });
})
;