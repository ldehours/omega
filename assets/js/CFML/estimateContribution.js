// all btn header ( urssaf , carmf)
let tabs_header = $('.tab-header');

$(tabs_header).click(function () {
    let current_id = $(this).attr('id');
    $(tabs_header).removeClass('btn-primary');
    let content_div = $('.content-div-urssaf-and-carmf').children();
    $(this).addClass('btn-primary');

    for (let i = 0; i < content_div.length; i++) {
        let id = $(content_div[i]).attr('id');

        if (id === current_id) {
            $(content_div[i]).addClass('d-block');
        } else {
            if ($(content_div[i]).hasClass('d-block')) {
                $(content_div[i]).removeClass('d-block')
            }
            $(content_div[i]).addClass('d-none');
        }
    }
});

// all btn child of urssaf
let tabs_child_urssaf = $('.tab-header-child-urssaf');
// array data about urssaf btn children
let data_urssaf = {
    2020: {
        "years_urssaf": "2020",
        "date_urssaf": "05-12-2019",
        "name_urssaf": "Christelle",
        "date_urssaf_mail": "05-12-2019",
        "mail_urssaf": "mail"
    },
    2019: {
        "years_urssaf": "2019",
        "date_urssaf": "18-03-2019 ",
        "name_urssaf": "Auriane",
        "date_urssaf_mail": "18-03-2019",
        "mail_urssaf": "mail"
    },
    2018: {
        "years_urssaf": "2018",
        "date_urssaf": "21-02-2019",
        "name_urssaf": "Karine",
        "date_urssaf_mail": "00-00-0000",
        "mail_urssaf": "mail"
    }
};
$(tabs_child_urssaf).click(function () {
    let current_id = $(this).attr('id');
    $(tabs_child_urssaf).removeClass('btn-primary');
    $(this).addClass('btn-primary');
    let data = data_urssaf[current_id];

    let years_urssaf = $('#years_urssaf');
    let date_urssaf = $('#date_urssaf');
    let name_urssaf = $('#name_urssaf');
    let date_urssaf_mail = $('#date_urssaf_mail');
    let mail_urssaf = $('#mail_urssaf');

    $(years_urssaf).text(data['years_urssaf']);
    $(date_urssaf).text(data['date_urssaf']);
    $(name_urssaf).text(data['name_urssaf']);
    $(date_urssaf_mail).text(data['date_urssaf_mail']);
    $(mail_urssaf).text(data['mail_urssaf']);

});

// all btn child of carmf
let tabs_child_carmf = $('.tab-header-child-carmf');
// array data about carmf btn children
let data_carmf = {
    2020: {
        "years_carmf": "2020",
        "date_carmf": "05-12-2019",
        "name_carmf": "Christelle",
        "date_carmf_mail": "05-12-2019",
        "mail_carmf": "mail"
    },
    2018: {
        "years_carmf": "2018",
        "date_carmf": "21-02-2019",
        "name_carmf": "Jocelyn",
        "date_carmf_mail": "00-00-0000 ",
        "mail_carmf": "mail"
    }
};

$(tabs_child_carmf).click(function () {
    let current_id = $(this).attr('id');
    $(tabs_child_carmf).removeClass('btn-primary');
    $(this).addClass('btn-primary');
    let data = data_carmf[current_id];

    let years_carmf = $('#years_carmf');
    let date_carmf = $('#date_carmf');
    let name_carmf = $('#name_carmf');
    let date_carmf_mail = $('#date_carmf_mail');
    let mail_carmf = $('#mail_carmf');

    $(years_carmf).text(data['years_carmf']);
    $(date_carmf).text(data['date_carmf']);
    $(name_carmf).text(data['name_carmf']);
    $(date_carmf_mail).text(data['date_carmf_mail']);
    $(mail_carmf).text(data['mail_carmf']);
});
