let persons_naturals = $('#persons_naturals');
let get_array_persons_session = $(persons_naturals).val();
let display_count_checked_persons = $("#countCheckedPersons");
// tous les checkbox
let persons_checkbox = $('.checkBoxAddPerson');
let btn_add_choice_staff = $('#nextBtnAddStaff');
let count_checked_persons = 0;
// array qui va contenir tous les clients à ajouter au staff concerné
let persons_attributed_to_staff = [{
    "members_id": []
}];

// du coup rien ne change en back j'envoi le array members_id qui contient les clients ex : [1,14] et l'id du staff choisi
function addCountCheckedPerson() {
    $(display_count_checked_persons).text(count_checked_persons);
}

if (get_array_persons_session) {
    get_array_persons_session = JSON.parse(get_array_persons_session);
}

if (typeof get_array_persons_session['members_id'] !== "undefined" && get_array_persons_session['members_id'].length) {
    get_array_persons_session['members_id'].forEach(clientId =>
        persons_attributed_to_staff[0]['members_id'].push(clientId)
    );
    $(btn_add_choice_staff).removeAttr('disabled');
    count_checked_persons = $(get_array_persons_session['members_id']).length;
    addCountCheckedPerson();
}

$(document).ready(function () {

    $(persons_checkbox).click(function () {
        let isChecked = $(this).is(":checked");
        let client_id = $(this).attr('id');
        // add client to array persons_attributed_to_staff
        if (isChecked) {
            if (!persons_attributed_to_staff[0]['members_id'].includes(client_id)) {
                count_checked_persons += 1;
                addCountCheckedPerson();
                persons_attributed_to_staff[0]['members_id'].push(client_id);
                $(persons_naturals).val(JSON.stringify(...persons_attributed_to_staff));
            }
        } else {
            if (persons_attributed_to_staff[0]['members_id'].includes(client_id)) {
                count_checked_persons -= 1;
                addCountCheckedPerson();
                let indexOf = persons_attributed_to_staff[0]['members_id'].indexOf(client_id);
                persons_attributed_to_staff[0]['members_id'].splice(indexOf, 1);
                $(persons_naturals).val(JSON.stringify(...persons_attributed_to_staff));
            }
        }
        // si 1 ou + client checked on rend le btn cliquable
        if (persons_attributed_to_staff[0]['members_id'].size != 0) {
            if ($(btn_add_choice_staff).attr('disabled')) {
                $(btn_add_choice_staff).removeAttr('disabled');
            }
        } else {
            // si aucun client checked on rend le btn non cliquable
            $(btn_add_choice_staff).attr('disabled', true);
        }
    });

    $("#uncheckAllClient").click(function (e) {
        let uncheck = confirm('Vous voulez vraiment décocher les clients ?');
        if (!uncheck) {
            e.preventDefault();
        }
    });
});

