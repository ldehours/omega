import {getRealErrorMessage} from "../error";
import {createDOMElement, createSpinner, createTooltipedIcon, newAttribute, Placement} from "../base";

$(document).ready(function(){
    document.getElementsByClassName('opinions-show').forEach(element => {
        element.addEventListener('click', handleShowOpinion);
    });

    document.getElementsByClassName('opinions-delete').forEach(element => {
        element.addEventListener('click', handleDeleteOpinion);
    });

    document.getElementsByClassName('btn-create-opinion').forEach(element => {
        element.addEventListener('click', handleCreateOpinion);
    });
});

/**
 * Popup that displays the opinion's text
 * @param e
 */
function handleShowOpinion(e){
    swal({ text: e.currentTarget.getAttribute('data-text') });
}

/**
 * Popup that asks the needed informations in order to create a new opinion
 * @param e
 */
function handleCreateOpinion(e){
    const element = e.currentTarget;
    const type = element.getAttribute('data-type');
    const tableBody = document.getElementById('opinions-body-' + type);
    swal({
        title: 'Nouvel avis',
        html: `<input type="number" required min="0" id="opinion-${type}-person" class="swal2-input" placeholder="Identifiant de la personne">
                <input type="text" required id="opinion-${type}-text" class="swal2-input" placeholder="Contenu de l'avis">`,
        confirmButtonText: 'Ajouter',
        showCancelButton: true,
        cancelButtonText: 'Annuler',
        focusConfirm: false,
        showLoaderOnConfirm: true,
        preConfirm: () => {
            const personId = document.getElementById(`opinion-${type}-person`).value;
            const text = document.getElementById(`opinion-${type}-text`).value;
            return createOpinion(element, text, personId, type).then(result => {
                if(result.success === false){
                    throw new Error(result.message);
                }
                return result;
            }).catch(error => {
                throw new Error(getRealErrorMessage(error))
            });
        }
    }).then((result) => {
        if(result.success === true){
            document.getElementById(`opinions-${type}-empty-row`).classList.add('d-none');
            tableBody.appendChild(createOpinionDOMElement(result.data, type));
            setUpTooltips();
        }
    })
}

/**
 * Ajax method that create a new opinion
 * @param element
 * @param text
 * @param personId
 * @param type
 * @returns {jQuery|{getAllResponseHeaders: function(): (*|null), abort: function(*=): this,
 *      setRequestHeader: function(*=, *): this, readyState: number, getResponseHeader: function(*): (null|*),
 *      overrideMimeType: function(*): this, statusCode: function(*=): this}|HTMLElement}
 */
function createOpinion(element, text, personId, type) {
    const spinner = createSpinner(['fa-2x']);
    element.classList.add('d-none');
    element.parentNode.appendChild(spinner);

    let receiverId, giverId;
    if(type === 'give'){
        receiverId = personId;
        giverId = element.getAttribute('data-person-id');
    }else{
        receiverId = element.getAttribute('data-person-id');
        giverId = personId;
    }
    return $.ajax({
        url: element.getAttribute('data-url'),
        method: 'POST',
        data: {
            text: text,
            receiverId: receiverId,
            giverId: giverId
        },
        complete: () => {
            element.parentNode.removeChild(spinner);
            element.classList.remove('d-none');
        },
    })
}

/**
 * Create a row element that contains all needed informations of an opinion
 * @param data
 * @param type
 * @returns {HTMLTableRowElement}
 */
function createOpinionDOMElement(data, type){
    const person = (type === 'give' ? data.receiver : data.giver );

    const idTd = createDOMElement({tag : 'td', text : person.id});
    const linkPerson = createDOMElement({tag : 'a', text : person.name, attributes : [newAttribute('href', person.url)]});
    const nameTd = createDOMElement({tag : 'td', children : [linkPerson]});
    const dateTd = createDOMElement({tag : 'td', text : data.createdAt});

    const seeOpinion = createTooltipedIcon(['fas', 'fa-eye'],"Voir l'avis", Placement.top,
        ['text-primary','given-opinions-show', 'mr-1'],[newAttribute('data-text', data.text)]);
    const deleteOpinion = createTooltipedIcon(['fas', 'fa-trash-alt'],"Supprimer l'avis", Placement.top,
        ['text-danger','given-opinions-delete'],[newAttribute('data-url', data.deleteUrl)]);
    seeOpinion.addEventListener('click', handleShowOpinion);
    deleteOpinion.addEventListener('click', handleDeleteOpinion);
    const actionsTd = createDOMElement({tag : 'td', children : [seeOpinion, deleteOpinion]});

    return createDOMElement({tag : 'tr',children : [idTd, nameTd, dateTd, actionsTd]});
}

/**
 * Popup that confirms the opinion's deletion
 * @param e
 */
function handleDeleteOpinion(e){
    const element = e.currentTarget;
    swal({
        type: 'warning',
        title: 'Etes-vous certain de supprimer ce commentaire ?',
        confirmButtonText: 'Oui, supprimer',
        showCancelButton: true,
        cancelButtonText: 'Annuler'
    }).then(result => {
        if(result){
            deleteOpinion(element);
        }
    });
}

/**
 * Make the request to delete an opinion
 * @param element
 */
function deleteOpinion(element){
    const spinner = createSpinner();
    const grand_parent = element.parentNode.parentNode;
    grand_parent.classList.add('deleting');
    element.classList.add('d-none');
    element.parentNode.appendChild(spinner);
    $.ajax({
        url: element.getAttribute('data-url'),
        method: 'DELETE',
        success: () => {
            grand_parent.remove();
        },
        error: error => {
            element.parentNode.removeChild(spinner);
            element.classList.remove('d-none');
            grand_parent.classList.remove('deleting');
            swal({type: 'error', title: 'Suppression impossible : '+ getRealErrorMessage(error)});
            console.error(error);
        }
    })
}