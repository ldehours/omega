import {createDOMElement, newAttribute} from "../base";

$(document).ready(function(){
    const container = document.getElementById('already-hired-persons-container');
    if(container !== null){
        fecthPersonAlreadyHired(container);
    }
});

/**
 * Returns the persons already hired by the actual viewed person
 * @param container
 */
function fecthPersonAlreadyHired (container) {
    const tableBody = document.getElementById('already-hired-persons-table').querySelector('tbody');
    document.getElementById('already-hired-persons-loading-row').classList.remove('d-none');
    document.getElementById('already-hired-persons-error').innerHTML = '';
    document.getElementById('already-hired-persons-empty').innerHTML = '';
    $.ajax({
        url: container.getAttribute('data-url'),
        method: 'GET',
        success: result => {
            result.data.forEach( item => {
                tableBody.append(newListElement(item));
            });
            if(result.data.length === 0){
                document.getElementById('already-hired-persons-empty').innerHTML = 'Aucune personne trouvée';
            }
        },
        error: error => {
            console.error(error);
            document.getElementById('already-hired-persons-error').appendChild(errorElement(error.statusText, container));
        },
        complete : () => {
            document.getElementById('already-hired-persons-loading-row').classList.add('d-none');
        }
    });
}

/**
 * Creates a new row element that displays the person information
 * @param person
 * @param offer
 * @returns {HTMLElement}
 */
function newListElement ({person, offer}) {
    const linkPerson = createDOMElement({tag: 'a', text: person.name, attributes: [newAttribute('href','/core/person/natural/' + person.id)]});
    const tdName = createDOMElement({tag: 'td', children: [linkPerson]});

    const tdId = createDOMElement({tag : 'td', text : person.id});
    const tdContract = createDOMElement({tag :'td', text : offer.substitutionType});
    const tdDates = createDOMElement({tag : 'td', text : offer.startingDate + ' - ' + offer.endDate});
    const tdFill = createDOMElement({tag : 'td', text : '~~~'});

    return createDOMElement({tag :'tr', children : [tdId,tdName,tdContract,tdDates,tdFill]});
}

/**
 * Creates a new error element
 * @param error
 * @param container
 * @returns {HTMLElement}
 */
function errorElement (error, container) {
    const message = createDOMElement({
        tag : 'p',
        text : 'Une erreur est survenue lors de la récupération des données : ' + error,
        classes : ['text-danger']
    });

    const resetBtn = createDOMElement({
        tag : 'button',
        text : 'Réessayer <i class="fas fa-undo-alt ml-2 text-white"></i>',
        classes : ['btn', 'btn-primary']
    });
    resetBtn.addEventListener('click', () => fecthPersonAlreadyHired(container));

    return createDOMElement({tag : 'div', children : [message, resetBtn]});
}