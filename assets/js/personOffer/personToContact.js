import {getRealErrorMessage} from "../error";
import {createDOMElement, createSpinner, newAttribute} from "../base";

$(document).ready(function(){
    const personContactForm = document.getElementById('person-to-contact-form');
    if(personContactForm){
        personContactForm.addEventListener('submit', handlePersonToContactForm);
    }

    document.getElementsByClassName('person-to-contact-delete-btn').forEach(element => {
        element.addEventListener('click', handleDeletePersonToContact);
        element.addEventListener('mouseenter', () => {$(element.parentNode).next().css('text-decoration-line', 'line-through');});
        element.addEventListener('mouseleave', () => {$(element.parentNode).next().css('text-decoration-line', 'none');});
    });
});

/**
 * Method that add an element to the list
 * @param e
 */
const handlePersonToContactForm = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const target = e.currentTarget;
    const error_element = document.getElementById('person-to-contact-error');
    const input = target.querySelector('input[name=new-person-to-contact]');
    const button = target.querySelector('input[type=submit]');
    const button_old_text = button.value;
    button.setAttribute('disabled','true');
    button.value = 'Traitement...';
    error_element.innerHTML = '';
    $.ajax({
        url: target.getAttribute('data-url'),
        method: 'PUT',
        data: {
            personToContact: input.value,
            isRemoval: false
        },
        success: result => {
            if(result.success === false){
                error_element.innerHTML = displayError(result.message);
                return;
            }
            document.getElementById('person-to-contact-list').appendChild(newListElement(input.value));
            input.value = '';
        },
        error : error => {
            console.error(error);
            error_element.innerHTML = displayError(getRealErrorMessage(error));
        },
        complete : () => {
            button.removeAttribute('disabled');
            button.value = button_old_text;
        }
    });
}

/**
 * Method that remove an element of the list
 * @param e
 */
const handleDeletePersonToContact = (e) => {
    const target = e.currentTarget;
    const grand_parent =  target.parentNode.parentNode;
    const spinner = createSpinner(['mr-2']);
    const error_element = document.getElementById('person-to-contact-list-error');

    target.classList.add('d-none');
    grand_parent.classList.add('deleting');
    target.parentNode.appendChild(spinner);
    error_element.innerHTML = '';
    $.ajax({
        url: document.getElementById('person-to-contact-form').getAttribute('data-url'),
        method: 'PUT',
        data: {
            personToContact: target.getAttribute('data-value'),
            isRemoval: true
        },
        success: () => {
            grand_parent.parentNode.removeChild(grand_parent);
        },
        error : error => {
            console.error(error);
            error_element.innerHTML = displayError(error.statusText);
            target.classList.remove('d-none');
            grand_parent.classList.remove('deleting');
            target.parentNode.removeChild(spinner);
        }
    });
}

/**
 * Returns the html element that will be displayed to print the error to the user
 * @param error
 * @returns {string}
 */
const displayError = (error) => {
    return "<div class=\"text-danger text-center mb-2\" role='alert'>Erreur : " + error + "</div>";
};

/**
 * Returns a new element that will be displayed in the list
 * @param value
 * @returns {HTMLDivElement}
 */
const newListElement = (value) => {
    const valueEl = createDOMElement({tag : 'div', text : value, classes : ['my-auto']});

    const btn = createDOMElement({
        tag : 'i',
        classes : ['fas', 'fa-times', 'my-auto', 'mr-2', 'person-to-contact-delete-btn'],
        attributes : [newAttribute('data-value', value)]
    });
    btn.addEventListener('click', handleDeletePersonToContact);
    btn.addEventListener('mouseenter', () => {valueEl.style.textDecorationLine = 'line-through'});
    btn.addEventListener('mouseleave', () => {valueEl.style.textDecorationLine = 'none'});

    const btnContainer = createDOMElement({tag : 'div', classes : ['flex', 'my-auto'], children : [btn]});

    return createDOMElement({tag : 'div', classes : ['d-flex'], children : [btnContainer, valueEl]});
};