import PerfectScrollbar from 'perfect-scrollbar';

new PerfectScrollbar('body');
new PerfectScrollbar('.card');

/*cocher les permissions une par une*/

$('.permission-checkbox').click(function () {
        if ($(this).data('ajax') !== "") {
            var id_role = $(this).data('role-id');
            var id_feature = $(this).data('feature-id');
            var checked = $(this).prop('checked');
            var column_all_checked = true;
            var row_all_checked = true;
            if (!checked) {
                $('input[name=role' + id_role + ']').prop('checked', false);
                $('input[name=feature' + id_feature + ']').prop('checked', false);
                $('#check-all').children('input').prop('checked', false);
            } else {
                $('input[data-role-id=' + id_role + ']').each(function () {
                    if (!($(this).prop('checked'))) {
                        column_all_checked = false;
                    }
                });
                $('input[data-feature-id=' + id_feature + ']').each(function () {
                    if (!($(this).prop('checked'))) {
                        row_all_checked = false;
                    }
                });

                if (column_all_checked) {
                    $('input[name=role' + id_role + ']').prop('checked', true);
                }
                if (row_all_checked) {
                    $('input[name=feature' + id_feature + ']').prop('checked', true);
                }

                if (column_all_checked && row_all_checked) {
                    $('#check-all').children('input').prop('checked', true);
                }
            }

            var url = '/core/permission/toggle';
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    role_id: id_role,
                    feature_id: id_feature
                },
                dataType: "json"
            });
        }
    }
);

/* cocher le droits par colonne */
$('.roles-header').click(function () {
    var column_roles = [];
    var all_roles_checked = 0;
    var id_role = $(this).attr('id');
    var column_checked = $(this).children('input').prop('checked');
    $(this).children('input').prop('checked', !column_checked);
    $('input[data-role-id=' + id_role + ']').each(function () {
        if (!column_checked) {
            $(this).prop('checked', true);
            column_roles.push($(this).attr('data-feature-id'));
        } else {
            $(this).prop('checked', false);
        }
    });

    $('.roles-header > input').each(function () {
        if (!($(this).prop('checked'))) {
            all_roles_checked += 1;
        }
    });

    if (all_roles_checked < 1) {
        $('.check-all-row').prop('checked', true);
        $('#check-all').children('input').prop('checked', true);
    } else {
        $('.check-all-row').prop('checked', false);
        $('#check-all').children('input').prop('checked', false);
    }

    var url = '/core/permission/toggleAllByColumn';
    $.ajax({
        url: url,
        type: "POST",
        data: {
            role_id: id_role,
            features_id: column_roles
        },
        dataType: "json"
    });
});

/*cocher les permissions par ligne*/
$('.features-column').click(function () {
    var row_roles = [];
    var all_features_checked = 0;
    var id_feature = $(this).attr('id');
    var row_checked = $(this).children('input').prop('checked');
    $(this).children('input').prop('checked', !row_checked);

    $('input[data-feature-id=' + id_feature + '] ').each(function () {
        if (!row_checked) {
            $(this).prop('checked', true);
            row_roles.push($(this).attr('name'));
        } else {
            $(this).prop('checked', false);
        }
    });

    $('.features-column > input').each(function () {
        if (!($(this).prop('checked'))) {
            all_features_checked += 1;
        }
    });

    if (all_features_checked < 1) {
        $('.check-all-column').prop('checked', true);
        $('#check-all').children('input').prop('checked', true);
    } else {
        $('.check-all-column').prop('checked', false);
        $('#check-all').children('input').prop('checked', false);
    }

    var url = '/core/permission/toggleAllByRow';
    $.ajax({
        url: url,
        type: "POST",
        data: {
            roles_id: row_roles,
            feature_id: id_feature
        },
        dataType: "json"
    });
});

/*cocher toutes les permissions*/
$("#check-all").click(function () {
    var checked_all_input = $(this).children('input').prop('checked', !$(this).children('input').prop('checked')).prop('checked');
    $('input[type=checkbox]').prop('checked', checked_all_input);
    var url = '/core/permission/toggleAll';
    $.ajax({
        url: url,
        type: "POST",
        data: {
            checked_all: checked_all_input
        },
        dataType: "json"
    });
});

/*mise à jour du fichier permission.json*/
$("#submit-permissions").click(function () {
    $.ajax({
        url: '/core/permission/update/json',
        type: "POST",
        dataType: "json",
        success: function ($data) {
            if ($data.success) {
                swal("Fonctionnalités bien attribuées aux rôles");
            } else {
                swal("Une erreur s'est produite");
            }
        }
    });
});