import PerfectScrollbar from 'perfect-scrollbar';

new PerfectScrollbar('body');
new PerfectScrollbar('.card');

/*début vérification au chargement si tous les droits sont déjà cochés*/
let all_accesses_checked = true;
$(".accesses-checkbox").each(function () {
    if (!$(this).prop('checked')) {
        all_accesses_checked = false;
    }
});
if (all_accesses_checked) {
    $("#check-all-input").click();
}
/*fin vérification au chargement si tous les droits sont déjà cochés*/

/*cocher les droits un par un*/
$(".accesses-checkbox").click(function () {
    let access_id = $(this).data('access-id');
    let jobname_id = $(this).data('jobname-id');
    let checked = $(this).prop('checked');
    let column_all_checked = true;
    let row_all_checked = true;
    if (!checked) {
        $('input[name=access' + access_id + ']').prop('checked', false);
        $('input[name=jobname' + jobname_id + ']').prop('checked', false);
        $("#check-all").children('input').prop('checked', false);
    } else {

        $("input[data-access-id=" + access_id + "] ").each(function () {
            if (!($(this).prop('checked'))) {
                column_all_checked = false;
            }
        });
        $("input[data-jobname-id=" + jobname_id + "] ").each(function () {
            if (!($(this).prop('checked'))) {
                row_all_checked = false;
            }
        });

        if (column_all_checked) {
            $('input[name=access' + access_id + ']').prop('checked', true);
        }
        if (row_all_checked) {
            $('input[name=jobname' + jobname_id + ']').prop('checked', true);
        }

        if (column_all_checked && row_all_checked) {
            $("#check-all").children('input').prop('checked', true);
        }
    }

    let url = $(this).data('one-url');
    $.ajax({
        url: url,
        type: "POST",
        data: {
            access_id: access_id,
            jobname_id: jobname_id
        },
        dataType: "json"
    });
});

/* cocher le droits par colonne */
$(".accesses-header").click(function () {
    let column_accesses = [];
    let number_accesses_checked = 0;
    let access_id = $(this).attr('id');
    let column_checked = $(this).children('input').prop('checked');

    $(this).children('input').prop('checked', !column_checked);
    $('input[data-access-id=' + access_id + ']').each(function () {
        if (!column_checked) {
            $(this).prop('checked', true);
            column_accesses.push($(this).attr('data-jobname-id'));
        } else {
            $(this).prop('checked', false);
        }
    });

    $('.accesses-header > input ').each(function () {
        if (!($(this).prop('checked'))) {
            number_accesses_checked += 1;
        }
    });

    if (number_accesses_checked < 1) {
        $('.check-all-row').prop('checked', true);
        $("#check-all").children('input').prop('checked', true);
    } else {
        $('.check-all-row').prop('checked', false);
        $("#check-all").children('input').prop('checked', false);
    }

    let url = $(this).data('column-url');

    $.ajax({
        url: url,
        type: "POST",
        data: {
            access_id: access_id,
            jobnames_id: column_accesses
        },
        dataType: "json"
    });
});

/*cocher les droits par lignes*/
$(".job-names-column").click(function () {
    let row_accesses = [];
    let number_jobnames_checked = 0;
    let jobname_id = $(this).attr('id');
    let row_checked = $(this).children('input').prop('checked');
    $(this).children('input').prop('checked', !row_checked);

    $("input[data-jobname-id=" + jobname_id + "] ").each(function () {
        if (!row_checked) {
            $(this).prop('checked', true);
            row_accesses.push($(this).attr('name'));
        } else {
            $(this).prop('checked', false);
        }
    });

    $('.job-names-column > input ').each(function () {
        if (!($(this).prop('checked'))) {
            number_jobnames_checked += 1;
        }
    });

    if (number_jobnames_checked < 1) {
        $('.check-all-column').prop('checked', true);
        $("#check-all").children('input').prop('checked', true);
    } else {
        $('.check-all-column').prop('checked', false);
        $("#check-all").children('input').prop('checked', false);
    }

    let url = $(this).data('row-url');
    $.ajax({
        url: url,
        type: "POST",
        data: {
            accesses_id: row_accesses,
            jobname_id: jobname_id
        },
        dataType: "json"
    });
});

/*cocher tous les droits*/
$("#check-all").click(function () {
    let input_child = $(this).children('input');
    let checked_all_input = input_child.prop('checked', !input_child.prop('checked')).prop('checked');
    $('input[type=checkbox]').prop('checked', checked_all_input);
    let url = input_child.data('all-url');
    $.ajax({
        url: url,
        type: "POST",
        data: {
            checked_all: checked_all_input
        },
        dataType: "json"
    });
});


