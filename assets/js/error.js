import {getRandomInt} from "./base";

/**
 * Return the real error message from a symfony exception response (which is a full html document built like this :
 * <!-- {error message} ({status (e.g : 404, 500, etc...)} {statusText (e.g : Internal Server Error, etc...)}) -->
 * {Rest of HTML error response}
 * )
 *
 * So, basically, this function is returning the text inside the "<!-- ... -->" delimiters
 *
 * @param error
 * @returns {string|null|*}
 */
export function getRealErrorMessage(error){
    const response = error.responseText;
    if(response === undefined){
        return typeof error === 'string' ? error : (error.message ?? 'Une erreur est survenue');
    }
    const searchDelimiter = response.indexOf('-->');
    if(searchDelimiter === -1){
        return null;
    }
    return response.substring(4, searchDelimiter).trim();
}

$(document).ready(function(){
    const error_tab = {
        403:["Bien tenté ;)","Cette page n'est pas destinée à la plèbe","Comme disent les jeunes : CHEH"],
        404:["On n'a pas pu trouver Charlie","Cette page est avec le chat de la mère Michelle","Cette page s'est perdue dans l'espace-temps"],
        500:["Et bah alors, les devs ne savent coder ?","https://openclassrooms.com/fr/courses/3178966-apprendre-a-coder-pour-les-vrais-debutants","Le bunker est en train de brûler"]
    };
    let random_text = "Ah shit, here we go again.";
    switch($('#error-status-code').text()){
        case '403':
            random_text = error_tab[403][getRandomInt(error_tab[403].length)];
            break;
        case '404':
            random_text = error_tab[404][getRandomInt(error_tab[404].length)];
            break;
        case '500':
            random_text = error_tab[500][getRandomInt(error_tab[500].length)];
            break;
    }
    $('#error-funny-text small').text(random_text);
});
