;'use strict';

/**
 * Returns whether or not a selector has selected something or not (i.e. does the selection exists ?)
 * @returns {boolean}
 */
window.$.fn.exists = function () {
    return this.length !== 0;
};