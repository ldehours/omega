var Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('customContent', './assets/custom_modules/custom_jquery.js')
    .addEntry('base', './assets/js/base.js')
    .addEntry('login', './assets/css/login.css')
    .addEntry('cfml/app', './assets/css/CFML/app.css')
    .addEntry('formMembershipCfmlSpecificStatusHistorization', './assets/js/CFML/formMembershipCfmlSpecificStatusHistorization.js')
    .addEntry('cfml/verificationFile', './assets/css/CFML/verificationFile.css')
    .addEntry('cfml/payementUrssaf', './assets/css/CFML/payementUrssaf.css')
    .addEntry('cfml/socialStatement', './assets/css/CFML/socialStatement.css')
    .addEntry('cfml/managementCarmf', './assets/css/CFML/managementCarmf.css')
    .addEntry('cfml/disease', './assets/css/CFML/disease.css')
    .addEntry('cfml/curps', './assets/css/CFML/curps.css')
    .addEntry('cfml/estimateContribution', ['./assets/js/CFML/estimateContribution.js', './assets/css/CFML/estimateContribution.css'])
    .addEntry('cfml/urssaf', ['./assets/css/CFML/urssaf.css', './assets/js/CFML/urssaf.js'])
    .addEntry('cfml/dataTax', './assets/css/CFML/dataTax.css')
    .addEntry('cfml/profil', './assets/css/CFML/profil.css')
    .addEntry('toolBox', './assets/css/toolBox.css')
    .addEntry('calendar', ['./assets/js/calendar/calendar.js', './assets/css/calendar.css'])
    .addEntry('annualCalendar', ['./assets/css/calendarAnnual.css', './assets/js/calendar/calendarAnnual.js'])
    .addEntry('offer', './assets/js/offer/offer.js')
    .addEntry('cfml/bill', './assets/css/CFML/bill.css')
    .addEntry('cfml/membership', ['./assets/css/CFML/membership.css', './assets/js/CFML/membership.js'])
    .addEntry('offerHistorization', './assets/css/offerHistorization.css')
    .addEntry('searchOffer', './assets/js/offer/searchOffer.js')
    .addEntry('dashboard', ['./assets/css/dashboard.css', './assets/js/dashboard.js'])
    .addEntry('cfml/membershipSituation', ['./assets/js/CFML/membershipSituation.js', './assets/css/CFML/membershipSituation.css'])
    .addEntry('cfml/newChargeSocial', ['./assets/css/CFML/newChargeSocial.css', './assets/js/CFML/newChargeSocial.js'])
    .addEntry('cfml/toolbox', './assets/css/CFML/toolbox.css')
    .addEntry('personNatural', './assets/js/personNatural/personNatural.js')
    .addEntry('personLegal', './assets/js/personLegal/personLegal.js')
    .addEntry('personToContact', './assets/js/personOffer/personToContact.js')
    .addEntry('personAlreadyHired', './assets/js/personOffer/personAlreadyHired.js')
    .addEntry('opinions', './assets/js/personOffer/opinions.js')
    .addEntry('personNaturalWishes', './assets/js/personNatural/personNaturalWishes.js')
    .addEntry('permission', ['./assets/js/permission/permission.js', './assets/css/permission.css'])
    .addEntry('error', ['./assets/css/error.css', './assets/js/error.js'])
    .addEntry('criterion', './assets/js/offer/criterion.js')
    .addEntry('onCall', ['./assets/js/offer/onCallForm.js', './assets/css/onCallForm.css'])
    .addEntry('timeLinePersonNatural', './assets/js/personNatural/timeLinePersonNatural.js')
    .addEntry('canvas', ['./assets/css/canvas.css', './assets/js/offer/canvas.js'])
    .addEntry('mailExchange', ['./assets/css/mailExchange.css', './assets/js/mail/mailExchange.js'])
    .addEntry('tinymceTextarea', './assets/js/mail/tinymceTextarea.js')
    .addEntry('tinymceCss', ['./node_modules/tinymce/skins/ui/oxide/skin.css', './node_modules/tinymce/skins/ui/oxide/content.css', './node_modules/tinymce/skins/content/default/content.css'])
    .addEntry('wish', ['./assets/css/wish.css', './assets/js/offer/wish.js'])
    .addEntry('matchingSubstitute', ['./assets/css/matchingSubstitute.css', './assets/js/personNatural/matchingSubstitute.js'])
    .addEntry('matchingOffers', ['./assets/css/matchingOffers.css', './assets/js/offer/matchingOffers.js'])
    .addEntry('personAccess', ['./assets/js/permission/personAccess.js', './assets/css/personAccess.css'])
    .addEntry('preWrittenMail', ['./assets/js/mail/preWrittenMail.js', './assets/css/preWrittenMail.css'])
    .addEntry('personNaturalAssociateOffer', ['./assets/js/personNatural/personNaturalAssociateOffer.js', './assets/css/personNaturalAssociateOffer.css'])
    //.addEntry('page2', './assets/js/page2.js')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    //.enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {
    }, {
        useBuiltIns: 'usage',
        corejs: 3
    })

    // enables Sass/SCSS support
    // .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

// uncomment if you use API Platform Admin (composer req api-admin)
//.enableReactPreset()
//.addEntry('admin', './assets/js/admin.js')
;

module.exports = Encore.getWebpackConfig();
