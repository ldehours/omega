<?php

if (isset($_ENV['BOOTSTRAP_DATABASE_CREATION'])) {
    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:database:drop --force --quiet --env=%s',
            __DIR__,
            $_ENV['BOOTSTRAP_DATABASE_CREATION']
        )
    );

    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:database:create --quiet --if-not-exists --env=%s',
            __DIR__,
            $_ENV['BOOTSTRAP_DATABASE_CREATION']
        )
    );

    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:schema:create  --quiet --no-interaction --env=%s',
            __DIR__,
            $_ENV['BOOTSTRAP_DATABASE_CREATION']
        )
    );

    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:schema:update --force --no-interaction --quiet --env=%s',
            __DIR__,
            $_ENV['BOOTSTRAP_DATABASE_CREATION']
        )
    );

    $dsn = explode(';', $_ENV['DATABASE_SITE_URL']);
    preg_match('/dbname=([\w_]+);/', $_ENV['DATABASE_SITE_URL'], $testDatabaseName);

    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:query:sql "DROP SCHEMA IF EXISTS %s ;"',
            __DIR__,
            $testDatabaseName[1] // 1st index contains only the database name
        )
    );

    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:query:sql "CREATE SCHEMA %s;"',
            __DIR__,
            $testDatabaseName[1] // 1st index contains only the database name
        )
    );
}

if (isset($_ENV['BOOTSTRAP_DATABASE_FIXTURES'])) {
    passthru(
        sprintf(
            'php "%s/../../../bin/console" doctrine:fixture:load --no-interaction --env=%s',
            __DIR__,
            $_ENV['BOOTSTRAP_DATABASE_FIXTURES']
        )
    );
}
