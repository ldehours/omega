<?php

namespace App\Tests\Unit\PHP\Service;

use App\Service\DatabaseManager;
use App\Service\DatabaseResult;
use App\Service\WebsiteDatabase;
use PHPUnit\Framework\TestCase;

class DatabaseManagerTest extends TestCase
{
    public function testWorking()
    {
        $db = new WebsiteDatabase();
        $dbManager = new DatabaseManager([]);
        $this->assertEmpty($dbManager->getDatabases());

        $dbManager->addDatabase($db);
        $this->assertNotEmpty($dbManager->getDatabases());
        $this->assertSame(1, count($dbManager->getDatabases()));

        $dbManager->removeDatabase($db);
        $this->assertEmpty($dbManager->getDatabases());
    }

    /**
     * @dataProvider areExpectedNumberLinesProvider
     * @param bool $expected
     * @param array $databaseResults
     * @param int $expectedNumberLines
     * @param string $operator
     */
    public function testAreExpectedNumberLines(
        bool $expected,
        array $databaseResults,
        int $expectedNumberLines,
        string $operator
    ) {
        $this->assertSame($expected, DatabaseManager::areExpectedNumberLines($databaseResults, $expectedNumberLines, $operator));
    }

    /**
     * @return array|array[]
     */
    public function areExpectedNumberLinesProvider(): array
    {
        $dbr1 = new DatabaseResult(1, []);
        $dbr2 = new DatabaseResult(2, []);
        $dbr3 = new DatabaseResult(3, []);
        return [
            [true, [$dbr2], 2, '=='],
            [true, [$dbr3], 2, '>'],
            [true, [$dbr1], 2, '<'],
            [true, [$dbr1], 2, '<='],
            [true, [$dbr3], 2, '>='],
            [true, [$dbr1], 2, '!='],
            [false, [$dbr3], 2, '=='],
            [false, [$dbr2], 2, '>'],
            [false, [$dbr2], 2, '<'],
            [false, [$dbr3], 2, '<='],
            [false, [$dbr1], 2, '>='],
            [false, [$dbr2], 2, '!='],
        ];
    }
}
