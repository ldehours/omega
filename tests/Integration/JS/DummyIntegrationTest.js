'use strict';

const assert = require('chai').assert;

describe('String Test Suite', function () {
    describe('Assertion on strings', function () {
        it('Assert that two strings are equals', function () {
            assert.equal('abcdef', 'abcdef');
        });
    });
});