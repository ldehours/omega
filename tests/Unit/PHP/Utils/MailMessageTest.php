<?php


namespace App\Tests\Unit\PHP\Utils;


use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Utils\MailMessage;
use PHPUnit\Framework\TestCase;

/**
 * Class MailMessageTest
 * @package App\Tests\Unit\PHP\Utils
 */
class MailMessageTest extends TestCase
{
    /**
     * @dataProvider personProvider
     * @param Person $person
     * @param $expected
     */
    public function testGetGreeting(Person $person, $expected): void
    {
        $this->assertSame($expected, MailMessage::getGreeting($person));
    }

    /**
     * @dataProvider cuamDataProvider
     * @param string $email
     * @param string $password
     * @param bool|null $isRecruitmentConsultant
     * @param string|null $telephoneRecruitmentConsultant
     * @param string $expected
     */
    public function testGetCUAM(
        string $email,
        string $password,
        string $expected,
        ?bool $isRecruitmentConsultant = false,
        ?string $telephoneRecruitmentConsultant = ''
    ): void {
        $this->assertSame($expected, MailMessage::getCUAM($email, $password, $isRecruitmentConsultant, $telephoneRecruitmentConsultant));
    }

    /**
     * @return array[]
     */
    public function personProvider(): array
    {
        return [
            'Person natural' => [
                (new PersonNatural())->setFirstName('Jean')->setLastName('Valjean'),
                "Docteur <span class='mail-content-greetings-name'>Jean VALJEAN</span>,"
            ],
            'Person legal' => [
                new PersonLegal(),
                "Bonjour,"
            ],
        ];
    }

    public function cuamDataProvider(): array
    {
        return [
            'cuam only mail & password' => [
                'test@test.com',
                '12password34',
                'Suite à notre entretien téléphonique concernant votre recherche de remplacements, je vous confirme que votre compte personnel a été créé sur le site de Média-Santé.<br>
                Pour y accéder, cliquez <a href="https://media-sante.com">ici</a>, rendez-vous ensuite dans la rubrique "Mon compte" puis "Connexion". 
                <br>
                <br>
                <strong>Vos codes d\'accès sont</strong> :<br>
                Email : test@test.com<br>
                Mot de Passe : 12password34<br>
                <br>
                Une fois identifié, toutes les informations des offres de remplacements libérales, postes salariés, associations de médecins et cessions de cabinets vous seront accessibles. Vous n’avez plus qu’à faire votre choix et à planifier votre agenda professionnel !<br>
                <br>
                Vous ne trouvez pas le remplacement qu’il vous faut ? Appelez-moi directement et je contacterai pour vous d’autres confrères dont le secteur géographique et les critères proposés habituellement seront susceptibles de vous convenir.'
            ],
            'cuam with all parameters' => [
                'test@test.com',
                '12password34',
                'Suite à notre entretien téléphonique concernant votre recherche de remplacements, je vous confirme que votre compte personnel a été créé sur le site de Média-Santé.<br>
                Pour y accéder, cliquez <a href="https://media-sante.com">ici</a>, rendez-vous ensuite dans la rubrique "Mon compte" puis "Connexion". 
                <br>
                <br>
                <strong>Vos codes d\'accès sont</strong> :<br>
                Email : test@test.com<br>
                Mot de Passe : 12password34<br>
                <br>
                Une fois identifié, toutes les informations des offres de remplacements libérales, postes salariés, associations de médecins et cessions de cabinets vous seront accessibles. Vous n’avez plus qu’à faire votre choix et à planifier votre agenda professionnel !<br>
                <br>
                Vous ne trouvez pas le remplacement qu’il vous faut ? Appelez directement la CR du secteur au 0605040302 qui contactera pour vous d’autres confrères dont le secteur géographique et les critères proposés habituellement seront susceptibles de vous convenir.',
                true,
                '0605040302'
            ],
            'cuam with all parameters but false to isRecruitementConsultant' => [
                'test@test.com',
                '12password34',
                'Suite à notre entretien téléphonique concernant votre recherche de remplacements, je vous confirme que votre compte personnel a été créé sur le site de Média-Santé.<br>
                Pour y accéder, cliquez <a href="https://media-sante.com">ici</a>, rendez-vous ensuite dans la rubrique "Mon compte" puis "Connexion". 
                <br>
                <br>
                <strong>Vos codes d\'accès sont</strong> :<br>
                Email : test@test.com<br>
                Mot de Passe : 12password34<br>
                <br>
                Une fois identifié, toutes les informations des offres de remplacements libérales, postes salariés, associations de médecins et cessions de cabinets vous seront accessibles. Vous n’avez plus qu’à faire votre choix et à planifier votre agenda professionnel !<br>
                <br>
                Vous ne trouvez pas le remplacement qu’il vous faut ? Appelez-moi directement et je contacterai pour vous d’autres confrères dont le secteur géographique et les critères proposés habituellement seront susceptibles de vous convenir.',
                false,
                '0605040302'
            ],
        ];
    }
}