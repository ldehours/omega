<?php

namespace App\Tests\Unit\PHP\Utils;

use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Utils\PHPHelper;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\Error\Notice;
use PHPUnit\Framework\TestCase;
use PHPUnit\Util\Exception;

class PHPHelperTest extends TestCase
{
    /**
     * @dataProvider conditionProvider
     * @param bool $expected
     * @param $value1
     * @param string $operator
     * @param $value2
     */
    public function testCondition(bool $expected, $value1, string $operator, $value2)
    {
        $this->assertSame($expected,PHPHelper::condition($value1,$operator,$value2));
    }

    /**
     * @return array|array[]
     */
    public function conditionProvider():array
    {
        return [
            [true,3,'<',5],
            [false,5,'<',3],
            [false,5,'<',5],
            [true,'abc','<','bac'],
            [false,'bac','<','abc'],
            [true,3,'<=',4],
            [true,3,'<=',3],
            [false,3,'<=',2],
            [true,'abc','<=','bac'],
            [true,'abc','<=','abc'],
            [false,'bac','<=','abc'],
            [true,5,'>',3],
            [false,3,'>',5],
            [false,3,'>',3],
            [true,'bac','>','abc'],
            [false,'abc','>','bac'],
            [true,5,'>=',3],
            [true,5,'>=',5],
            [false,5,'>=',6],
            [true,'bac','>=','abc'],
            [true,'bac','>=','bac'],
            [false,'bac','>=','cab'],
            [true,3,'==',3],
            [true,3,'==','3'],
            [false,3,'==',5],
            [false,3,'==','5'],
            [true,'abc','==','abc'],
            [false,'abc','==','bac'],
            [true,3,'===',3],
            [false,3,'===','3'],
            [false,3,'===',5],
            [false,3,'===','5'],
            [true,'abc','===','abc'],
            [false,'abc','===','bac'],
            [true,3,'!=',5],
            [true,3,'!=','5'],
            [false,3,'!=',3],
            [false,3,'!=','3'],
            [true,'abc','!=','bac'],
            [false,'abc','!=','abc'],
            [true,3,'!==',5],
            [true,3,'!==','5'],
            [false,3,'!==',3],
            [true,3,'!==','3'],
            [true,'abc','!==','bac'],
            [false,'abc','!==','abc'],
            [false,3,'$',5],
            [false,3,'_',5],
            [false,3,'-',5],
            [false,3,'azy',5],
            [false,3,'ouais re',5],
        ];
    }

    /**
     * @dataProvider convertObjectClassProvider
     * @param $object
     * @param $expected
     */
    public function testConvertObjectClass($expected,$object)
    {
        if(is_bool($expected)){
            $this->expectException(Notice::class);
            $this->assertSame($expected,PHPHelper::convertObjectClass($object,$expected));
        }else{
            $this->assertInstanceOf($expected,PHPHelper::convertObjectClass($object,$expected));
        }
    }

    /**
     * @return array|array[]
     */
    public function convertObjectClassProvider():array
    {
        return [
            [PersonNatural::class,new PersonLegal()],
            [false,'ouais re'],
        ];
    }

    /**
     * @dataProvider strToUpperNoAccentProvider
     * @param string $expected
     * @param string $string
     */
    public function testStrToUpperNoAccent(string $expected, string $string)
    {
        $this->assertSame($expected,PHPHelper::strToUpperNoAccent($string));
    }

    /**
     * @return array
     */
    public function strToUpperNoAccentProvider():array
    {
        return [
            ['JEAN','jean'],
            ['JEAN','jEaN'],
            ['RENE','rené'],
            ['JEAN-RENE','jean-rené'],
            ['JEAN RENE','jean rené'],
            ['AEEIOU','àèéïôù'],
        ];
    }

    /**
     * @dataProvider sortArrayFromValueProvider
     * @param array $expected
     * @param array $haystack
     * @param int $type
     */
    public function testSortArrayFromValue(array $expected, array $haystack, int $type = PHPHelper::ASC)
    {
        $this->assertSame($expected,PHPHelper::sortArrayFromValue($haystack,$type));
    }

    /**
     * @return array
     */
    public function sortArrayFromValueProvider():array
    {
        return [
            [[],[]],
            [["allo"],["allo"]],
            [[2=>"allo",3=>"bonjour",1=>"coucou",0=>"ouais re"],[0=>"ouais re",1=>"coucou",2=>"allo",3=>"bonjour"]],
            [[3=>2,2=>3,0=>5,1=>8],[0=>5,1=>8,2=>3,3=>2]],
            [[],[],PHPHelper::DESC],
            [["allo"],["allo"],PHPHelper::DESC],
            [[0=>"ouais re",1=>"coucou",3=>'bonjour',2=>'allo'],[0=>"ouais re",1=>"coucou",2=>"allo",3=>"bonjour"],PHPHelper::DESC],
            [[1=>8,0=>5,2=>3,3=>2],[0=>5,1=>8,2=>3,3=>2],PHPHelper::DESC],
        ];
    }

    /**
     * @dataProvider sortArrayFromKeyProvider
     * @param array $expected
     * @param array $haystack
     * @param int $type
     */
    public function testSortArrayFromKey(array $expected, array $haystack, int $type = PHPHelper::ASC)
    {
        $this->assertSame($expected,PHPHelper::sortArrayFromKey($haystack,$type));
    }

    /**
     * @return array
     */
    public function sortArrayFromKeyProvider():array
    {
        return [
            [[],[]],
            [[0 => "allo"],[0 => "allo"]],
            [[0 => "ouais re", 1 => "allo", 2 => "coucou", 3 => "bonjour"],[0 => "ouais re",2 => "coucou",1 => "allo",3 => "bonjour"]],
            [["a" => "b","b" => "d","c" => "a","d" => "b"],["c" => "a", "a" => "b", "b" => "d", "d" => "b"]],
            [[],[],PHPHelper::DESC],
            [[0 => "allo"],[0 => "allo"],PHPHelper::DESC],
            [[3 => "bonjour",2 => "coucou",1 => "allo",0 => "ouais re"],[0 => "ouais re",2 => "coucou",1 => "allo",3 => "bonjour"],PHPHelper::DESC],
            [["d" => "b","c" => "a","b" => "d","a" => "b"],["c" => "a", "a" => "b", "b" => "d", "d" => "b"],PHPHelper::DESC],
        ];
    }

    /**
     * @dataProvider sortEntitiesFromMethodProvider
     * @param ArrayCollection $expected
     * @param ArrayCollection $haystack
     * @param string $method
     * @param int $type
     */
    public function testSortEntitiesFromMethod(ArrayCollection $expected, ArrayCollection $haystack, string $method, int $type = PHPHelper::DESC)
    {
        $this->assertEquals($expected, PHPHelper::sortEntitiesFromMethod($haystack,$method,$type));
    }

    /**
     * @return array
     */
    public function sortEntitiesFromMethodProvider():array
    {
        $personLegal1 = (new PersonLegal())->setCorporateName("aac");
        $personLegal2 = (new PersonLegal())->setCorporateName("abc");
        $personLegal3 = (new PersonLegal())->setCorporateName("bac");
        $personLegal4 = (new PersonLegal())->setCorporateName("cab");

        $collection = new ArrayCollection();
        $collection->add($personLegal3);
        $collection->add($personLegal1);
        $collection->add($personLegal2);
        $collection->add($personLegal4);

        $collectionExpected1 = new ArrayCollection();
        $collectionExpected1->add($personLegal1);
        $collectionExpected1->add($personLegal2);
        $collectionExpected1->add($personLegal3);
        $collectionExpected1->add($personLegal4);

        $collectionExpected2 = new ArrayCollection();
        $collectionExpected2->add($personLegal4);
        $collectionExpected2->add($personLegal3);
        $collectionExpected2->add($personLegal2);
        $collectionExpected2->add($personLegal1);
        return [
            [new ArrayCollection(),new ArrayCollection(),'dummyMethod'],
            [
                $collectionExpected1,
                $collection,
                'getCorporateName'
            ],
            [
                $collectionExpected2,
                $collection,
                'getCorporateName',
                PHPHelper::ASC
            ],
        ];
    }
}
