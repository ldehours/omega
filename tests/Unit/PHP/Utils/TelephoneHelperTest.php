<?php


namespace App\Tests\Unit\PHP\Utils;


use App\Entity\Core\Telephone;
use App\Utils\TelephoneHelper;
use PHPUnit\Framework\TestCase;

/**
 * Class TelephoneHelperTest
 * @package App\Tests\Unit\PHP\Utils
 */
class TelephoneHelperTest extends TestCase
{
    /**
     * @dataProvider telephoneProvider
     * @param Telephone $telephone
     * @param string|null $expected
     */
    public function testInternationalizeTelephoneNumber(Telephone $telephone, ?string $expected): void
    {
        $this->assertSame($expected, TelephoneHelper::internationalizeTelephoneNumber($telephone));
    }

    /**
     * @dataProvider telephoneNumberProvider
     * @param string|null $number
     * @param string|null $expected
     */
    public function testFormatPhoneNumber(?string $number, ?string $expected): void
    {
        $this->assertSame($expected, TelephoneHelper::formatPhoneNumber($number));
    }

    /**
     * @return array[]
     */
    public function telephoneProvider(): array
    {
        return [
            'no number' => [new Telephone(), null],
            'short number' => [new Telephone('0605040302'), '0033605040302'],
            'short -non 06- number' => [new Telephone('0905040302'), '0033905040302'],
            'medium number' => [new Telephone('06050403020102'), '00336050403020102'],
            'long number' => [new Telephone('06050403020605040302'), '00336050403020605040302']
        ];
    }

    /**
     * @return array
     */
    public function telephoneNumberProvider(): array
    {
        return [
            'no number' => [null, '00 00 00 00 00'],
            'too short number' => ['0605', '06 05 00 00 00'],
            'short number' => ['0605040302', '06 05 04 03 02'],
            'short -non 06- number' => ['0905040302', '09 05 04 03 02'],
            'medium number' => ['06050403020102', '06 05 04 03 02 01 02'],
            'long number' => ['06050403020605040302', '06 05 04 03 02 06 05 04 03 02']
        ];
    }
}