<?php

namespace App\Tests\Unit\PHP\Utils;

use App\Utils\DateTime;
use Exception;
use PHPUnit\Framework\TestCase;

/**
 * Class DateTimeTest
 * @package App\Tests\Unit\PHP\Utils
 */
class DateTimeTest extends TestCase
{
    /**
     * @dataProvider constructProvider
     * @param $type
     * @param $actual
     * @param $expected
     * @throws Exception
     */
    public function testConstruct($type, $actual, $expected): void
    {
        switch ($type) {
            case 'not same':
                $this->assertNotSame($expected, $actual);
                break;
            default:
                $this->assertSame($expected, $actual);
                break;
        }
    }

    /**
     * @dataProvider getFormatProvider
     * @param $actual
     * @param $expected
     */
    public function testGetFormat($actual, $expected): void
    {
        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider createFromStringProvider
     * @param $type
     * @param string $actual
     */
    public function testCreateFromString($type, string $actual): void
    {
        switch ($type) {
            case 'null':
                $this->assertNull(DateTime::createFromString($actual));
                break;
            default:
                $this->assertInstanceOf(DateTime::class, DateTime::createFromString($actual));
                break;
        }
    }

    /**
     * @return array|array[]
     * @throws Exception
     */
    public function constructProvider(): array
    {
        return [
            ['same', (new \DateTime())->getTimestamp(), (new DateTime())->getTimestamp()],
            [
                'same',
                (new DateTime('2003-05-20'))->getTimestamp(),
                (new \DateTime('2003-05-20', new \DateTimeZone("Europe/Paris")))->getTimestamp()
            ],
            [
                'same',
                (new DateTime('2003-05-20', new \DateTimeZone('Europe/London')))->getTimestamp(),
                (new \DateTime('2003-05-20', new \DateTimeZone('Europe/London')))->getTimestamp()
            ],
            [
                'not same',
                (new DateTime('2003-05-20', new \DateTimeZone('Europe/London')))->getTimestamp(),
                (new \DateTime('2003-05-20', new \DateTimeZone("Europe/Paris")))->getTimestamp()
            ],
        ];
    }

    /**
     * @return array
     */
    public function createFromStringProvider(): array
    {
        return [
            ['null', '0000-00-00'],
            ['null', '2a-c-2'],
            ['null', '20'],
            ['instance', '2003-05-20'],
            ['instance', '2003-05-20 15:45:21'],
            ['instance', '20/05/2003'],
            ['instance', '20/05/2003 15:45:21'],
            ['instance', '20/05/2003 15:45'],
        ];
    }

    /**
     * @return array
     */
    public function getFormatProvider(): array
    {
        return [
            [(new DateTime('2003-05-20'))->getEnglishFormat(), '2003-05-20'],
            [(new DateTime('2003-05-20'))->getFrenchFormat(), '20/05/2003'],
            [(new DateTime('3-5-7'))->getEnglishFormat(), '2003-05-07'],
            [(new DateTime('3-5-7'))->getFrenchFormat(), '07/05/2003'],
        ];
    }
}
