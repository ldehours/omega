<?php

namespace App\Tests\Unit\PHP\Utils;

use App\Utils\ConvertionHelper;
use App\Utils\DateTime;
use PHPUnit\Framework\TestCase;

/**
 * Class ConvertionHelperTest
 * @package App\Tests\Unit\PHP\Utils
 */
class ConvertionHelperTest extends TestCase
{
    public function testStrToNoAccent(): void
    {
        $actual = " éèêâîôûïëç¤'-ÉÈÊÂÎÔÛÏËÇ ";
        $this->assertSame("eeeaiouiec   EEEAIOUIEC", ConvertionHelper::strToNoAccent($actual));
    }

    public function testStringToTitleFormat(): void
    {
        $actual = "le matching, c'est compliqué";
        $this->assertSame("Le Matching, C'est Compliqué", ConvertionHelper::stringToTitleFormat($actual));
    }

    public function testTimestampToDateTime(): void
    {
        $date = ConvertionHelper::timestampToDateTime(1598872604);
        $this->assertInstanceOf(DateTime::class, $date);
        $this->assertSame($date->getTimestamp(), 1598872604);
    }

    /**
     * @dataProvider timestampMillisecondsToSecondProvider
     * @param $expected
     * @param $actual
     */
    public function testTimestampMillisecondsToSeconds($expected, $actual): void
    {
        $this->assertSame($expected, ConvertionHelper::timestampMillisecondsToSeconds($actual));
    }

    /**
     * @return array
     */
    public function timestampMillisecondsToSecondProvider(): array
    {
        return [
            [1598872604, 1598872604],
            [1598872604, 159887260456],
            [1598872604, 159887260456465489],
            [15988726, 15988726]
        ];
    }
}
