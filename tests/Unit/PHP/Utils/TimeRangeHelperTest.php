<?php


namespace App\Tests\Unit\PHP\Utils;


use App\Entity\Core\TimeRange;
use App\Utils\TimeRangeHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TimeRangeHelperTest extends KernelTestCase
{
    /**
     * @dataProvider getEventDataFromTimeRangeProvider
     * @param TimeRange $timeRange
     * @param array $expected
     */
    public function testGetEventDataFromTimeRange(TimeRange $timeRange, array $expected): void
    {
        self::bootKernel();
        $this->assertSame($expected, self::$container->get(TimeRangeHelper::class)->getEventDataFromTimeRange($timeRange));
    }

    /**
     * @return array|array[]
     */
    public function getEventDataFromTimeRangeProvider():array
    {
        //comme ici les new TimeRange() ne possèdent pas d'ID, les routes n'auront pas l'ID à la fin mais normalement c'est /core/agenda/{id}
        return [
            [
                (new TimeRange())->setNature(TimeRange::NATURE_HOLLIDAY),
                [
                    'id' => null,
                    'backgroundColor' => '#FFFF00',
                    'borderColor' => '#FFFF00',
                    'textColor' => 'grey',
                    'url' => '/core/agenda/',
                ]
            ],
            [
                (new TimeRange())->setNature(TimeRange::NATURE_ON_CALL),
                [
                    'id' => null,
                    'backgroundColor' => '#FA8B34',
                    'borderColor' => '#FA8B34',
                    'textColor' => 'white',
                    'url' => '/core/agenda/',
                ]
            ],
            [
                (new TimeRange())->setNature(TimeRange::NATURE_INTERNAL_MEETING),
                [
                    'id' => null,
                    'backgroundColor' => 'orange',
                    'borderColor' => 'orange',
                    'textColor' => 'white',
                    'url' => '/core/agenda/',
                ]
            ],
            [
                (new TimeRange())->setNature(TimeRange::NATURE_CALL_ARRIVED),
                [
                    'id' => null,
                    'backgroundColor' => 'darkblue',
                    'borderColor' => 'darkblue',
                    'textColor' => 'white',
                    'url' => '/core/agenda/',
                ]
            ],
        ];
    }
}