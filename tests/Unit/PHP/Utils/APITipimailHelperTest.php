<?php

namespace App\Tests\Unit\PHP\Utils;

use App\Utils\APITipimailHelper;
use PHPUnit\Framework\TestCase;

/**
 * Class APITipimailHelperTest
 * @package App\Tests\Unit\PHP\Utils
 */
class APITipimailHelperTest extends TestCase
{
    /**
     * @dataProvider confirmMessageSentProvider
     * @param $expected
     * @param $mail
     * @param $status
     */
    public function testConfirmMessageSent($expected, $mail, $status): void
    {
        $this->assertSame($expected, APITipimailHelper::confirmMessageSent($mail, $status));
    }

    /**
     * @dataProvider convertStatusProvider
     * @param $expected
     * @param string $status
     */
    public function testConvertStatus($expected, string $status): void
    {
        $this->assertSame($expected, APITipimailHelper::convertStatus($status));
    }

    /**
     * @return array
     */
    public function confirmMessageSentProvider(): array
    {
        return [
            ['Le mail a bien été envoyé à l\'adresse test@test.com', 'test@test.com', 1],
            ['Le mail a bien été envoyé à l\'adresse test@test.com', 'test@test.com', true],
            ['Le mail a bien été envoyé à l\'adresse @test.com', '@test.com', 1],
            ['Le mail a bien été envoyé à l\'adresse test@.com', 'test@.com', 1],
            ['Le mail a bien été envoyé à l\'adresse .com', '.com', 1],
            ['Le mail a bien été envoyé à l\'adresse yo', 'yo', 1],
            ['Le mail a bien été envoyé à l\'adresse ', '', 1],
            ['Le mail n\'a pas été envoyé', 'test@test.com', 0],
            ['Le mail n\'a pas été envoyé', 'test@test.com', false],
        ];
    }

    /**
     * @return array[]
     */
    public function convertStatusProvider(): array
    {
        return [
            [0, 'delivered'],
            [1, 'opened'],
            [2, 'clicked'],
            [3, 'rejected'],
            [4, 'hardbounced'],
            [7, 'filtered'],
            [0, 'other'],
            [0, 'ouais re']
        ];
    }
}
