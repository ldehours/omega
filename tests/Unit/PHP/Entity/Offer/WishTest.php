<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Core\Department;
use App\Entity\Offer\OfferWishMatching;
use App\Entity\Offer\Wish;
use PHPUnit\Framework\TestCase;

class WishTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $currentEntity = new Wish();
        $entity = new $class();
        $this->assertEmpty($currentEntity->{$getMethod}());

        $currentEntity->{'add' . $field}($entity);
        $this->assertCount(1, $currentEntity->{$getMethod}());

        $currentEntity->{'remove' . $field}($entity);
        $this->assertEmpty($currentEntity->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [Department::class, 'Department', 'getDepartments'],
            [OfferWishMatching::class, 'OfferWishMatching', 'getOfferWishMatchings'],
        ];
    }
}
