<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Offer\Offer;
use App\Entity\Offer\SubstitutionCriterion;
use PHPUnit\Framework\TestCase;

class SubstitutionTest extends TestCase
{
    public function testSubstitutionCriteria(): void
    {
        $offer = new Offer();
        $substitutionCriterion = new SubstitutionCriterion();
        $this->assertEmpty($offer->getSubstitutionCriteria());

        $offer->addSubstitutionCriterion($substitutionCriterion);
        $this->assertCount(1, $offer->getSubstitutionCriteria());

        $offer->removeSubstitutionCriterion($substitutionCriterion);
        $this->assertEmpty($offer->getSubstitutionCriteria());
    }
}
