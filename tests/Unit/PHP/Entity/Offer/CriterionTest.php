<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Offer\Criterion;
use App\Entity\Offer\CriterionPossibleValue;
use App\Entity\Offer\SubstitutionCriterion;
use PHPUnit\Framework\TestCase;

class CriterionTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $currentEntity = new Criterion();
        $entity = new $class();
        $this->assertEmpty($currentEntity->{$getMethod}());

        $currentEntity->{'add' . $field}($entity);
        $this->assertCount(1, $currentEntity->{$getMethod}());

        $currentEntity->{'remove' . $field}($entity);
        $this->assertEmpty($currentEntity->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [CriterionPossibleValue::class, 'CriterionPossibleValue', 'getCriterionPossibleValues'],
            [SubstitutionCriterion::class, 'SubstitutionCriterion', 'getSubstitutionCriteria'],
            [Criterion::class, 'CriteriaDependency', 'getCriteriaDependencies'],
        ];
    }

    public function testToString(): void
    {
        $criterion = new Criterion();
        $label = 'ouais re';
        $criterion->setLabel($label);
        $this->assertSame($label, $criterion->__toString());
    }

}
