<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\CriterionPossibleValue;
use PHPUnit\Framework\TestCase;

class CriterionPossibleValueTest extends TestCase
{
    /**
     * @dataProvider criterionTypeProvider
     * @param string $expected
     * @param int $criterionType
     */
    public function testCriterionType(string $expected, int $criterionType): void
    {
        $criterionPossibleValue = new CriterionPossibleValue();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $criterionPossibleValue->setCriterionType($criterionType);
        if ($expected !== 'exception') {
            $this->assertSame($criterionType, $criterionPossibleValue->getCriterionType());
        }
    }

    /**
     * @dataProvider substitutionProvider
     * @param string $expected
     * @param int $substitutionNature
     */
    public function testSubstitutionNature(string $expected, int $substitutionNature): void
    {
        $criterionPossibleValue = new CriterionPossibleValue();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $criterionPossibleValue->setSubstitutionNature($substitutionNature);
        if ($expected !== 'exception') {
            $this->assertSame($substitutionNature, $criterionPossibleValue->getSubstitutionNature());
        }
    }

    /**
     * @return array|array[]
     */
    public function substitutionProvider(): array
    {
        return [
            ['set', Canvas::CANVAS_WISH],
            ['set', Canvas::CANVAS_OFFER],
            ['exception', 156],
        ];
    }

    /**
     * @return array|array[]
     */
    public function criterionTypeProvider(): array
    {
        $results = [];
        foreach (Criterion::TYPES as $index => $type) {
            $results[] = ['set', $type];
        }
        $results[] = ['exception', 548];
        return $results;
    }
}
