<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Core\Note;
use App\Entity\Core\PersonNatural;
use App\Entity\Offer\Offer;
use App\Entity\Offer\OfferWishMatching;
use PHPUnit\Framework\TestCase;

class OfferTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $currentEntity = new Offer();
        $entity = new $class();
        $this->assertEmpty($currentEntity->{$getMethod}());

        $currentEntity->{'add' . $field}($entity);
        $this->assertCount(1, $currentEntity->{$getMethod}());

        $currentEntity->{'remove' . $field}($entity);
        $this->assertEmpty($currentEntity->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [Note::class, 'Note', 'getNotes'],
            [PersonNatural::class, 'Contractor', 'getContractors'],
            [OfferWishMatching::class, 'OfferWishMatching', 'getOfferWishMatchings'],
        ];
    }

    /**
     * @dataProvider stateProvider
     * @param string $expected
     * @param int $state
     */
    public function testState(string $expected, int $state): void
    {
        $offer = new Offer();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $offer->setState($state);
        if ($expected !== 'exception') {
            $this->assertSame($state, $offer->getState());
        }
    }

    /**
     * @return array
     */
    public function stateProvider(): array
    {
        $result = [];
        foreach (Offer::STATES as $index => $state) {
            $result[] = ['set', $state];
        }
        $result[] = ['exception', 456];
        return $result;
    }

    public function testToString(): void
    {
        $offer = new Offer();
        $this->assertSame('', $offer->__toString());
    }

    /**
     * @dataProvider numberOfferWishMatchingNotActiveProvider
     * @param int $nbActive
     * @param int $nbNotActive
     */
    public function testNumberOfferWishMatchingNotActive(int $nbActive, int $nbNotActive): void
    {
        $offer = new Offer();
        for ($i = 0; $i < $nbActive; $i++) {
            $offer->addOfferWishMatching((new OfferWishMatching())->setIsActive(true));
        }
        for ($i = 0; $i < $nbNotActive; $i++) {
            $offer->addOfferWishMatching((new OfferWishMatching())->setIsActive(false));
        }

        $this->assertSame($nbNotActive, $offer->getNumberOfferWishMatchingNotActive());
    }

    /**
     * @return array
     */
    public function numberOfferWishMatchingNotActiveProvider(): array
    {
        return [
            [0, 0],
            [0, 2],
            [2, 0],
            [4, 6],
        ];
    }

    /**
     * @dataProvider startingDateOrAsapProvider
     * @param string $expected
     * @param Offer $offer
     */
    public function testStartingDateOrAsap(string $expected, Offer $offer): void
    {
        $this->assertSame($expected, $offer->getStartingDateOrAsap());
    }

    /**
     * @return array|array[]
     */
    public function startingDateOrAsapProvider(): array
    {
        return [
            ['Pas de date de début', (new Offer())],
            ['Dès que possible', (new Offer())->setAsSoonAsPossible(true)],
            ['Dès que possible', (new Offer())->setAsSoonAsPossible(true)->setStartingDate(new \DateTime('2020-09-02'))],
            ['02/09/2020', (new Offer())->setAsSoonAsPossible(false)->setStartingDate(new \DateTime('2020-09-02'))],
            ['02/09/2020', (new Offer())->setStartingDate(new \DateTime('2020-09-02'))],
        ];
    }
}
