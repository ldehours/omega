<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Offer\SubstitutionCriterion;
use PHPUnit\Framework\TestCase;

class SubstitutionCriterionTest extends TestCase
{
    /**
     * @dataProvider relevantProvider
     * @param string $expected
     * @param int $relevantness
     */
    public function testIsRelevant(string $expected, int $relevantness): void
    {
        $substitutionCriterion = new SubstitutionCriterion();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $substitutionCriterion->setIsRelevant($relevantness);
        if ($expected !== 'exception') {
            $this->assertSame($relevantness, $substitutionCriterion->getIsRelevant());
        }
    }

    /**
     * @return array|array[]
     */
    public function relevantProvider(): array
    {
        return [
            ['set', SubstitutionCriterion::AVAILABLE],
            ['set', SubstitutionCriterion::SECONDARY],
            ['set', SubstitutionCriterion::IMPORTANT],
            ['exception', 154],
        ];
    }
}
