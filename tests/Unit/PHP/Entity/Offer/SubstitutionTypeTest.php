<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\CriterionPossibleValue;
use App\Entity\Offer\Offer;
use App\Entity\Offer\SubstitutionType;
use App\Entity\Offer\Wish;
use PHPUnit\Framework\TestCase;

class SubstitutionTypeTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $currentEntity = new SubstitutionType();
        $entity = new $class();
        $this->assertEmpty($currentEntity->{$getMethod}());

        $currentEntity->{'add' . $field}($entity);
        $this->assertCount(1, $currentEntity->{$getMethod}());

        $currentEntity->{'remove' . $field}($entity);
        $this->assertEmpty($currentEntity->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [CriterionPossibleValue::class, 'CriterionPossibleValue', 'getCriterionPossibleValues'],
            [Offer::class, 'Substitution', 'getSubstitutions'],
            [Wish::class, 'Substitution', 'getSubstitutions'],
            [Canvas::class, 'Canva', 'getCanvas'],
        ];
    }

    /**
     * @dataProvider toStringProvider
     * @param string $expected
     * @param int $substitutionType
     */
    public function testToString(string $expected, int $substitutionType): void
    {
        $currentSubstitutionType = new SubstitutionType();
        $currentSubstitutionType->setSubstitutionType($substitutionType);
        $this->assertSame($expected, (string)$currentSubstitutionType);
    }

    /**
     * @return array
     */
    public function toStringProvider(): array
    {
        $result = [];
        foreach (SubstitutionType::LABEL as $type => $translate) {
            if (!array_key_exists($type, SubstitutionType::TYPES_POSSIBLE)) {
                $result[] = [$translate, $type];
            }
        }
        return $result;
    }

    /**
     * @dataProvider substitutionTypeProvider
     * @param string $expected
     * @param int $substitutionType
     */
    public function testSubstitutionType(string $expected, int $substitutionType): void
    {
        $currentSubstitutionType = new SubstitutionType();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $currentSubstitutionType->setSubstitutionType($substitutionType);
        if ($expected !== 'exception') {
            $this->assertSame($substitutionType, $currentSubstitutionType->getSubstitutionType());
        }
    }

    /**
     * @return array
     */
    public function substitutionTypeProvider(): array
    {
        $results = [];
        foreach (SubstitutionType::LABEL as $type => $translate) {
            if (!array_key_exists($type, SubstitutionType::TYPES_POSSIBLE)) {
                $results[] = ['set', $type];
            }
        }
        $results[] = ['exception', 4568];
        return $results;
    }
}
