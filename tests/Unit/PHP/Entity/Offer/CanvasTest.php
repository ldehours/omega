<?php

namespace App\Tests\Unit\PHP\Entity\Offer;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;
use PHPUnit\Framework\TestCase;

class CanvasTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $currentEntity = new Canvas();
        $entity = new $class();
        $this->assertEmpty($currentEntity->{$getMethod}());

        $currentEntity->{'add' . $field}($entity);
        $this->assertCount(1, $currentEntity->{$getMethod}());

        $currentEntity->{'remove' . $field}($entity);
        $this->assertEmpty($currentEntity->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [Criterion::class, 'Criterion', 'getCriteria'],
            [Offer::class, 'Substitution', 'getSubstitutions'],
            [Wish::class, 'Substitution', 'getSubstitutions'],
        ];
    }

    /**
     * @dataProvider canvasTypeProvider
     * @param string $expected
     * @param int $canvasType
     */
    public function testCanvasType(string $expected, int $canvasType): void
    {
        $canvas = new Canvas();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $canvas->setCanvasType($canvasType);
        if ($expected !== 'exception') {
            $this->assertSame($canvasType, $canvas->getCanvasType());
        }
    }

    /**
     * @return array|array[]
     */
    public function canvasTypeProvider(): array
    {
        return [
            ['set', Canvas::CANVAS_WISH],
            ['set', Canvas::CANVAS_OFFER],
            ['exception', 56]
        ];
    }
}
