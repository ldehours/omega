<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\MedicalSpecialtyQualification;
use PHPUnit\Framework\TestCase;

class MedicalSpecialtyQualificationTest extends TestCase
{
    public function test(): void
    {
        $medicalSpecialtyQualification = new MedicalSpecialtyQualification();
        $medicalSpecialty = new MedicalSpecialty();
        $this->assertEmpty($medicalSpecialtyQualification->getMedicalSpecialties());
        $this->assertEmpty($medicalSpecialty->getRelatedType());

        $medicalSpecialtyQualification->addMedicalSpecialty($medicalSpecialty);
        $this->assertCount(1, $medicalSpecialtyQualification->getMedicalSpecialties());
        $this->assertCount(1, $medicalSpecialty->getRelatedType());

        $medicalSpecialtyQualification->removeMedicalSpecialty($medicalSpecialty);
        $this->assertEmpty($medicalSpecialtyQualification->getMedicalSpecialties());
        $this->assertEmpty($medicalSpecialty->getRelatedType());
    }

    public function testToString(): void
    {
        $medicalSpecialtyQualification = new MedicalSpecialtyQualification();
        $label = 'ouais re';
        $medicalSpecialtyQualification->setLabel($label);
        $this->assertSame($label, $medicalSpecialtyQualification->__toString());
        $this->assertSame($medicalSpecialtyQualification->getLabel(), $medicalSpecialtyQualification->__toString());
    }
}
