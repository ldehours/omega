<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Agenda;
use App\Entity\Core\TimeRange;
use PHPUnit\Framework\TestCase;

class AgendaTest extends TestCase
{
    public function testAddRemoveTimeRange(): void
    {
        $agenda = new Agenda();
        $this->assertEmpty($agenda->getTimeRanges());

        $timeRange = new TimeRange();
        $agenda->addTimeRange($timeRange);
        $this->assertCount(1, $agenda->getTimeRanges());
        $this->assertNotNull($timeRange->getAgenda());

        $agenda->removeTimeRange($timeRange);
        $this->assertEmpty($agenda->getTimeRanges());
        $this->assertNull($timeRange->getAgenda());
    }
}
