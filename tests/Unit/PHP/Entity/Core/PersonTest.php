<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Address;
use App\Entity\Core\Discussion;
use App\Entity\Core\Enrolment;
use App\Entity\Core\JobName;
use App\Entity\Core\Mail;
use App\Entity\Core\Note;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Service;
use App\Entity\Core\Telephone;
use App\Entity\Core\TimeRange;
use App\Entity\Legacy\Legacy;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $person = new PersonNatural();
        $entity = new $class();
        $this->assertEmpty($person->{$getMethod}());

        $person->{'add' . $field}($entity);
        $this->assertCount(1, $person->{$getMethod}());

        $person->{'remove' . $field}($entity);
        $this->assertEmpty($person->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [Address::class, 'Address', 'getAddresses'],
            [Telephone::class, 'Telephone', 'getTelephones'],
            [Service::class, 'Service', 'getServices'],
            [Enrolment::class, 'Enrolment', 'getEnrolments'],
            [Discussion::class, 'Discussion', 'getDiscussions'],
            [Legacy::class, 'Legacy', 'getLegacies'],
            [JobName::class, 'JobName', 'getJobNames'],
            [TimeRange::class, 'TimeRange', 'getTimeRanges'],
            [Offer::class, 'Substitution', 'getSubstitutions'],
            [Wish::class, 'Substitution', 'getSubstitutions'],
            [Mail::class, 'Mail', 'getMails'],
        ];
    }

    /**
     * @dataProvider telephoneTypeProvider
     * @param int $expectedNumberValues
     * @param int $telephoneType
     */
    public function testGetTelephoneByType(int $expectedNumberValues, int $telephoneType): void
    {
        $person = new PersonNatural();
        $telWork = (new Telephone())->setPhoneType(Telephone::WORK);
        $telWork2 = (new Telephone())->setPhoneType(Telephone::WORK);
        $telHome = (new Telephone())->setPhoneType(Telephone::HOME);
        $telCell = (new Telephone())->setPhoneType(Telephone::CELL);

        $person->addTelephone($telWork)->addTelephone($telWork2)->addTelephone($telHome)->addTelephone($telCell);

        $this->assertCount($expectedNumberValues, $person->getTelephonesByType($telephoneType));
    }

    /**
     * @dataProvider defaultMailProvider
     * @param string $expected
     * @param bool $isDefault
     */
    public function testDefaultMail(string $expected, bool $isDefault): void
    {
        $person = new PersonNatural();
        $mail = new Mail();
        $mail2 = (new Mail())->setIsDefault($isDefault);
        $person->addMail($mail)->addMail($mail2);

        $this->{'assert' . $expected}($person->getDefaultMail());
    }

    /**
     * @dataProvider additionalInformationsProvider
     * @param $expected
     * @param string|null $additionalInformation
     */
    public function testAdditionalInformation($expected, ?string $additionalInformation): void
    {
        $person = new PersonNatural();
        $note = (new Note())->setText($additionalInformation);
        $person->setAdditionalInformation($note);

        $this->assertSame($expected, $person->getAdditionalInformation() ? $person->getAdditionalInformation()->getText() : null);
    }

    /**
     * @dataProvider addressesProvider
     * @param int $nbNotDefaultAddresses
     * @param bool $isOneDefault
     * @throws \Exception
     */
    public function testDefaultAddress(int $nbNotDefaultAddresses, bool $isOneDefault): void
    {
        $person = new PersonNatural();
        for ($i = 0; $i < $nbNotDefaultAddresses; $i++) {
            $person->addAddress((new Address()));
        }
        if ($isOneDefault === true) {
            $person->addAddress((new Address())->setUsages([Address::FISCAL]));
        }

        if ($isOneDefault === true || $nbNotDefaultAddresses > 0) {
            $this->assertInstanceOf(Address::class, $person->getDefaultAddress());
        } else {
            $this->assertNull($person->getDefaultAddress());
        }
    }

    /**
     * @dataProvider offersWishesProvider
     * @param int $nbOffers
     * @param int $nbOffersModel
     * @param int $nbWish
     * @param int $nbWishModel
     */
    public function testGetOffersWishes(int $nbOffers, int $nbOffersModel, int $nbWish, int $nbWishModel): void
    {
        $person = new PersonNatural();
        for ($i = 0; $i < $nbOffers; $i++) {
            $offer = new Offer();
            $person->addSubstitution($offer);
        }
        for ($i = 0; $i < $nbOffersModel; $i++) {
            $offer = (new Offer())->setIsModel(true);
            $person->addSubstitution($offer);
        }
        for ($i = 0; $i < $nbWish; $i++) {
            $wish = new Wish();
            $person->addSubstitution($wish);
        }
        for ($i = 0; $i < $nbWishModel; $i++) {
            $wish = (new Wish())->setIsModel(true);
            $person->addSubstitution($wish);
        }
        $this->assertCount($nbOffers + $nbOffersModel, $person->getOffers());
        $this->assertCount($nbOffers, $person->getRealOffers());
        $this->assertCount($nbOffersModel, $person->getModelsOffers());
        $this->assertCount($nbWish + $nbWishModel, $person->getWishes());
        $this->assertCount($nbWish, $person->getRealWishes());
        $this->assertCount($nbWishModel, $person->getModelsWishes());
    }

    /**
     * @dataProvider personNamesProvider
     * @param string $expected
     * @param Person $person
     */
    public function testGetName(string $expected, Person $person): void
    {
        $this->assertSame($expected, $person->getName());
    }

    /**
     * @return array|array[]
     */
    public function personNamesProvider(): array
    {
        return [
            ['Centre de santé du nord', (new PersonLegal())->setCorporateName('Centre de santé du nord')],
            ['Clinique sud', (new PersonLegal())->setCorporateName('Clinique sud')],
            ['HEPAD de Paris', (new PersonLegal())->setCorporateName('HEPAD de Paris')],
            ['', (new PersonLegal())->setCorporateName('')],
            ['VALJEAN Jean', (new PersonNatural())->setFirstName('Jean')->setLastName('Valjean')],
            ['ECUBERT Jean-rené', (new PersonNatural())->setFirstName('Jean-René')->setLastName('Ecubert')],
            ['DARK Vador', (new PersonNatural())->setFirstName('Vador')->setLastName('Dark')],
            ['THYLENE Bledemé', (new PersonNatural())->setFirstName('Bledemé')->setLastName('Thylène')],
            ['AH Oh', (new PersonNatural())->setFirstName('oh')->setLastName('ah')],
            [' ', (new PersonNatural())->setFirstName('')->setLastName('')],
        ];
    }

    /**
     * @return array
     */
    public function offersWishesProvider(): array
    {
        return [
            [0, 0, 0, 0],
            [0, 0, 0, 5],
            [0, 0, 5, 0],
            [0, 5, 0, 0],
            [5, 0, 0, 0],
            [0, 1, 0, 1],
            [2, 0, 2, 0],
            [2, 1, 5, 2],
            [1, 5, 2, 3],
            [2, 1, 0, 0],
            [0, 0, 2, 1],
        ];
    }

    /**
     * @return array|array[]
     */
    public function addressesProvider(): array
    {
        return [
            'not null 1' => [2, true],
            'not null 3' => [0, true],
            'not null 4' => [0, false],
            'not null 5' => [3, false],
            'null' => [0, false],
        ];
    }

    /**
     * @return array|array[]
     */
    public function additionalInformationsProvider(): array
    {
        return [
            [null, null],
            [null, ''],
            ['ouais re', 'ouais re'],
        ];
    }

    /**
     * @return array|array[]
     */
    public function defaultMailProvider(): array
    {
        return [
            ['Null', false],
            ['NotNull', true],
        ];
    }

    /**
     * @return array|array[]
     */
    public function telephoneTypeProvider(): array
    {
        return [
            [2, Telephone::WORK],
            [1, Telephone::HOME],
            [1, Telephone::CELL],
            [0, Telephone::FAX],
        ];
    }
}
