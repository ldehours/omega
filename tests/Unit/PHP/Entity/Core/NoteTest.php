<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Note;
use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase
{
    /**
     * @dataProvider noteTypeProvider
     * @param string $expected
     * @param int $noteType
     */
    public function testNoteType(string $expected, int $noteType): void
    {
        $note = new Note();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }

        $note->setNoteType($noteType);

        if ($expected !== 'exception') {
            $this->assertSame($noteType, $note->getNoteType());
        }
    }

    public function testToString(): void
    {
        $note = new Note();
        $this->assertSame((string)$note->getId(), $note->__toString());
    }

    /**
     * @return array|array[]
     */
    public function noteTypeProvider(): array
    {
        return [
            ['set', Note::TYPE_CONFIDENTIAL],
            ['set', Note::TYPE_DIFFUSION],
            ['set', Note::TYPE_INFORMATION],
            ['exception', 542],
        ];
    }
}
