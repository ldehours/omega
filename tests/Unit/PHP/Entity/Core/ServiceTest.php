<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Enrolment;
use App\Entity\Core\Service;
use App\Entity\Core\Staff;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{
    public function testAddRemoveEnrolment(): void
    {
        $service = new Service();
        $enrolment = new Enrolment();
        $this->assertEmpty($service->getEnrolments());

        $service->addEnrolment($enrolment);
        $this->assertCount(1, $service->getEnrolments());

        $service->removeEnrolment($enrolment);
        $this->assertEmpty($service->getEnrolments());
    }

    public function testAddRemoveStaff(): void
    {
        $service = new Service();
        $staff = new Staff();
        $this->assertEmpty($service->getStaff());

        $service->addStaff($staff);
        $this->assertCount(1, $service->getStaff());

        $service->removeStaff($staff);
        $this->assertEmpty($service->getStaff());
    }

    /**
     * @dataProvider nameProvider
     * @param string $expected
     * @param string $name
     */
    public function testToString(string $expected, string $name): void
    {
        $service = new Service();
        $service->setName($name);
        $this->assertSame($expected, $service->__toString());
    }

    /**
     * @return array|array[]
     */
    public function nameProvider(): array
    {
        return [
            ['ouais re', 'ouais re'],
            ['', ''],
        ];
    }
}
