<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Telephone;
use PHPUnit\Framework\TestCase;

class TelephoneTest extends TestCase
{
    /**
     * @dataProvider constructProvider
     * @param string|null $number
     */
    public function testConstruct(?string $number = null): void
    {
        $telephone = new Telephone($number);
        if ($number !== null) {
            $this->assertNotNull($telephone->getNumber());
        }
        $this->assertSame($number, $telephone->getNumber());
    }

    /**
     * @dataProvider phoneTypeProvider
     * @param string $expected
     * @param int $phoneType
     */
    public function testPhoneType(string $expected, int $phoneType): void
    {
        $telephone = new Telephone();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $telephone->setPhoneType($phoneType);
        if ($expected !== 'exception') {
            $this->assertSame($phoneType, $telephone->getPhoneType());
        }
    }

    /**
     * @return array|array[]
     */
    public function phoneTypeProvider(): array
    {
        return [
            ['set', Telephone::WORK],
            ['set', Telephone::HOME],
            ['set', Telephone::FAX],
            ['set', Telephone::CELL],
            ['exception', 254],
        ];
    }

    /**
     * @return array
     */
    public function constructProvider(): array
    {
        return [
            ['0605040302'],
            [''],
            [null],
            [],
        ];
    }
}
