<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\ContactInstitution;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use PHPUnit\Framework\TestCase;

class PersonLegalTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $person = new PersonLegal();
        $entity = new $class();
        $this->assertEmpty($person->{$getMethod}());

        $person->{'add' . $field}($entity);
        $this->assertCount(1, $person->{$getMethod}());

        $person->{'remove' . $field}($entity);
        $this->assertEmpty($person->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [PersonNatural::class, 'PersonNatural', 'getPersonNaturals'],
            [ContactInstitution::class, 'ContactInstitution', 'getContactInstitutions'],
        ];
    }

    /**
     * @dataProvider contactProvider
     * @param int $nbContact
     */
    public function testGetContact(int $nbContact): void
    {
        $person = new PersonLegal();
        for ($i = 0; $i < $nbContact; $i++) {
            $ci = (new ContactInstitution())->setPersonNatural((new PersonNatural()));
            $person->addContactInstitution($ci);
        }
        $contacts = $person->getContacts();
        $this->assertCount($nbContact, $contacts);
        for ($i = 0; $i < $nbContact; $i++) {
            $this->assertInstanceOf(PersonNatural::class, $contacts[$i]);
        }
    }

    public function testToString(): void
    {
        $person = new PersonLegal();
        $label = 'Clinique des Harpèges';
        $person->setCorporateName($label);
        $this->assertSame($label, $person->__toString());
        $this->assertSame($person->getCorporateName(), $person->__toString());
    }

    /**
     * @return array
     */
    public function contactProvider(): array
    {
        return [
            [0],
            [1],
            [2],
            [10],
        ];
    }
}
