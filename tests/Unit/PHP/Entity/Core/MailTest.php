<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Mail;
use App\Entity\Core\PersonNatural;
use PHPUnit\Framework\TestCase;

class MailTest extends TestCase
{
    /**
     * @dataProvider mailTypeProvider
     * @param string $expected
     * @param int $mailType
     */
    public function testMailType(string $expected, int $mailType): void
    {
        $mail = new Mail();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $mail->setMailType($mailType);
        if ($expected !== 'exception') {
            $this->assertSame($mailType, $mail->getMailType());
        }
    }

    public function testAddRemovePerson(): void
    {
        $mail = new Mail();
        $person = new PersonNatural();
        $this->assertEmpty($mail->getPersons());
        $this->assertEmpty($person->getMails());

        $mail->addPerson($person);
        $this->assertCount(1, $mail->getPersons());
        $this->assertCount(1, $person->getMails());

        $mail->removePerson($person);
        $this->assertEmpty($mail->getPersons());
        $this->assertEmpty($person->getMails());
    }

    /**
     * @return array|array[]
     */
    public function mailTypeProvider(): array
    {
        return [
            ['set', Mail::PRO],
            ['set', Mail::PERSO],
            ['exception', 0],
            ['exception', 53]
        ];
    }
}
