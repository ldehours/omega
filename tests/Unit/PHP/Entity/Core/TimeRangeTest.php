<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use PHPUnit\Framework\TestCase;

class TimeRangeTest extends TestCase
{
    public function test(): void
    {
        /*
         * Actuellement aucun test utile à faire dans l'entité.
         * On garde quand même au cas où il y en aurait par la suite
         * (notamment avec l'ajout de plusieurs souhaits pour une meme période).
         */
        $this->assertTrue(true);
    }
}
