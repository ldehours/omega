<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Feature;
use App\Entity\Core\Role;
use PHPUnit\Framework\TestCase;

class FeatureTest extends TestCase
{
    public function testAddRemoveRole(): void
    {
        $feature = new Feature();
        $role = new Role();
        $this->assertEmpty($feature->getRoles());
        $this->assertEmpty($role->getFeatures());

        $feature->addRole($role);
        $this->assertCount(1, $feature->getRoles());
        $this->assertCount(1, $role->getFeatures());

        $feature->removeRole($role);
        $this->assertEmpty($feature->getRoles());
        $this->assertEmpty($role->getFeatures());
    }
}
