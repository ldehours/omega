<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\ContactInstitution;
use App\Entity\Core\ContactService;
use PHPUnit\Framework\TestCase;

class ContactServiceTest extends TestCase
{
    public function testAddRemoveContactInstitution(): void
    {
        $contactService = new ContactService();
        $contactInstitution = new ContactInstitution();
        $this->assertEmpty($contactService->getContactInstitutions());

        $contactService->addContactInstitution($contactInstitution);
        $this->assertCount(1, $contactService->getContactInstitutions());
        $this->assertNotNull($contactInstitution->getService());

        $contactService->removeContactInstitution($contactInstitution);
        $this->assertEmpty($contactService->getContactInstitutions());
        $this->assertNull($contactInstitution->getService());
    }

    public function testToString(): void
    {
        $contactService = new ContactService();
        $label = 'ouais re';
        $contactService->setLabel($label);
        $this->assertSame($label, $contactService->__toString());
        $this->assertSame($contactService->getLabel(), $contactService->__toString());
    }
}
