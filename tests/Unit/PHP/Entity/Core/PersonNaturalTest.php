<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\ContactInstitution;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Offer\Offer;
use PHPUnit\Framework\TestCase;

class PersonNaturalTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $person = new PersonNatural();
        $entity = new $class();
        $this->assertEmpty($person->{$getMethod}());

        $person->{'add' . $field}($entity);
        $this->assertCount(1, $person->{$getMethod}());

        $person->{'remove' . $field}($entity);
        $this->assertEmpty($person->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [PersonLegal::class, 'PersonLegal', 'getPersonLegals'],
            [Offer::class, 'ProvidedOffer', 'getProvidedOffers'],
            [ContactInstitution::class, 'ContactInstitution', 'getContactInstitutions'],
        ];
    }

    /**
     * @dataProvider genderProvider
     * @param string $expected
     * @param int $gender
     * @throws \Exception
     */
    public function testGender(string $expected, int $gender): void
    {
        $person = new PersonNatural();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }

        $person->setGender($gender);

        if ($expected !== 'exception') {
            $this->assertSame($gender, $person->getGender());
        }
    }

    /**
     * @dataProvider firstNamesProvider
     * @param string $expected
     * @param string $firstName
     */
    public function testFirstName(string $expected, string $firstName): void
    {
        $person = new PersonNatural();
        $person->setFirstName($firstName);
        $this->assertSame($expected, $person->getFirstName());
    }

    /**
     * @dataProvider lastNamesProvider
     * @param string $expected
     * @param string $lastName
     */
    public function testLastName(string $expected, string $lastName): void
    {
        $person = new PersonNatural();
        $person->setLastName($lastName);
        $this->assertSame($expected, $person->getLastName());
    }

    /**
     * @dataProvider namesProvider
     * @param string $expected
     * @param string $lastName
     * @param string $firstName
     */
    public function testToString(?string $expected, string $lastName, string $firstName): void
    {
        $person = new PersonNatural();
        $person->setLastName($lastName)->setFirstName($firstName);
        $this->assertSame($expected, $person->__toString());
    }

    /**
     * @dataProvider birthDateProvider
     * @param int|null $expected
     * @param \DateTime|null $dateTime
     * @throws \Exception
     */
    public function testAge(?int $expected, ?\DateTime $dateTime): void
    {
        $person = new PersonNatural();
        $person->setBirthDate($dateTime);

        if ($dateTime === null) {
            $this->assertNull($person->getAge());
        } else {
            $this->assertSame($expected, $person->getAge());
        }
    }

    /**
     * @dataProvider jobStatusProvider
     * @param string $expected
     * @param array $jobStatus
     * @throws \Exception
     */
    public function testJobStatus(string $expected, array $jobStatus): void
    {
        $person = new PersonNatural();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }

        $person->setJobStatus($jobStatus);

        if ($expected !== 'exception') {
            $this->assertSame($jobStatus, $person->getJobStatus());
        }
    }

    /**
     * @dataProvider diplomaProvider
     * @param string $expected
     * @param int|null $diploma
     * @throws \Exception
     */
    public function testJobDiploma(string $expected, ?int $diploma): void
    {
        $person = new PersonNatural();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }

        $person->setDiploma($diploma);

        if ($expected !== 'exception') {
            $this->assertSame($diploma, $person->getDiploma());
        }
    }

    /**
     * @return array[]
     */
    public function diplomaProvider(): array
    {
        return [
            ['set', PersonNatural::DIPLOMA_LICENCE],
            ['set', PersonNatural::DIPLOMA_THESIS],
            ['set', null],
            ['exception', 12],
            ['exception', 548],
        ];
    }

    /**
     * @return array|array[]
     */
    public function jobStatusProvider(): array
    {
        return [
            ['set', [PersonNatural::JOB_STATUS_LIBERAL]],
            ['set', [PersonNatural::JOB_STATUS_SALARIED]],
            ['set', [PersonNatural::JOB_STATUS_LIBERAL, PersonNatural::JOB_STATUS_SALARIED]],
            ['set', [PersonNatural::JOB_STATUS_LIBERAL, null, null, PersonNatural::JOB_STATUS_SALARIED]],
            ['set', [null, null, PersonNatural::JOB_STATUS_LIBERAL]],
            ['set', []],
            ['exception', [10, 20]],
            ['exception', [0, 523]],
        ];
    }

    /**
     * @return array|array[]
     */
    public function birthDateProvider(): array
    {
        return [
            [30, (new \DateTime())->sub(new \DateInterval('P30Y'))],
            [50, (new \DateTime())->sub(new \DateInterval('P50Y'))],
            [29, (new \DateTime())->add(new \DateInterval('P30Y'))],
            [null, null],
        ];
    }

    /**
     * @return array
     */
    public function namesProvider(): array
    {
        return [
            ['VALJEAN Jean', 'Valjean', 'Jean'],
            ['VALJEAN Jean', 'vAljEan', 'jeaN'],
            ['THYLENE Bledemé', 'Thylène', 'Bledemé'],
            ['BELCOURT Jean-rené', 'Belcourt', 'Jean-René'],
            ['', '', ''],
        ];
    }

    /**
     * @return array|array[]
     */
    public function lastNamesProvider(): array
    {
        return [
            ['VALJEAN', 'Valjean'],
            ['VALJEAN', 'ValJean'],
            ['VALJEAN', 'VaLjEAn'],
            ['DE MONTESCOURT', 'De Montescourt'],
            ['DE RIBIERE', 'De Ribière'],
            ['DE-RIBIERE', 'De-Ribière'],
            ['', '']
        ];
    }

    /**
     * @return array|array[]
     */
    public function firstNamesProvider(): array
    {
        return [
            ['Jean', 'Jean'],
            ['Jean', 'JeAn'],
            ['Jean', 'JeaN'],
            ['Jean', 'JEan'],
            ['Jean', 'JEAN'],
            ['Jean-rené', 'Jean-René'],
            ['', '']
        ];
    }

    /**
     * @return array|array[]
     */
    public function genderProvider(): array
    {
        return [
            ['set', PersonNatural::GENDER_MALE],
            ['set', PersonNatural::GENDER_FEMALE],
            ['set', PersonNatural::GENDER_OTHER],
            ['exception', 100],
            ['exception', 0],
            ['exception', 75],
            ['exception', 986],
        ];
    }
}
