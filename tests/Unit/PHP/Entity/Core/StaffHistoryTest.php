<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\StaffHistory;
use PHPUnit\Framework\TestCase;

class StaffHistoryTest extends TestCase
{
    /**
     * @dataProvider genderProvider
     * @param string $expected
     * @param int|null $gender
     * @throws \Exception
     */
    public function testGender(string $expected, ?int $gender): void
    {
        $staffHistory = new StaffHistory();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }
        $staffHistory->setGender($gender);
        if ($expected !== 'exception') {
            $this->assertSame($gender, $staffHistory->getGender());
        }
    }

    /**
     * @return array|array[]
     */
    public function genderProvider(): array
    {
        return [
            ['set', StaffHistory::GENDER_MALE],
            ['set', StaffHistory::GENDER_FEMALE],
            ['set', StaffHistory::GENDER_OTHER],
            ['exception', 0],
            ['exception', null],
            ['exception', 278],
        ];
    }
}
