<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\MedicalSpecialtyQualification;
use App\Entity\Core\PersonNatural;
use PHPUnit\Framework\TestCase;

class MedicalSpecialtyTest extends TestCase
{
    /**
     * @dataProvider specialtyProvider
     * @param string $expected
     * @param string $label
     */
    public function testSpecialty(string $expected, string $label): void
    {
        $medicalSpecialty = new MedicalSpecialty();
        $medicalSpecialtyQualification = new MedicalSpecialtyQualification();
        $medicalSpecialtyQualification->setLabel($label);
        $medicalSpecialty->addRelatedType($medicalSpecialtyQualification);
        $person = new PersonNatural();
        $this->assertEmpty($medicalSpecialty->getSpecialtyPersonNaturals());
        $this->assertNull($person->getSpecialty());

        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }

        $medicalSpecialty->addSpecialtyPersonNatural($person);
        if ($expected !== 'exception') {
            $this->assertCount(1, $medicalSpecialty->getSpecialtyPersonNaturals());
            $this->assertNotNull($person->getSpecialty());
            $this->assertInstanceOf(MedicalSpecialty::class, $person->getSpecialty());

            $medicalSpecialty->removeSpecialtyPersonNatural($person);
            $this->assertEmpty($medicalSpecialty->getSpecialtyPersonNaturals());
        }
    }

    /**
     * @dataProvider otherSpecialtiesProvider
     * @param string $expected
     * @param string $type
     * @param array $methods
     * @param string $invertedMethod
     */
    public function testOtherSpecialties(string $expected, string $type, array $methods, string $invertedMethod): void
    {
        $medicalSpecialty = new MedicalSpecialty();
        $medicalSpecialtyQualification = new MedicalSpecialtyQualification();
        $medicalSpecialtyQualification->setLabel($type);
        $medicalSpecialty->addRelatedType($medicalSpecialtyQualification);
        $person = new PersonNatural();
        $this->assertEmpty($medicalSpecialty->{$methods['get']}());
        $this->assertEmpty($person->{$invertedMethod}());

        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }

        $medicalSpecialty->{$methods['add']}($person);
        if ($expected !== 'exception') {
            $this->assertCount(1, $medicalSpecialty->{$methods['get']}());
            $this->assertCount(1, $person->{$invertedMethod}());

            $medicalSpecialty->{$methods['remove']}($person);
            $this->assertEmpty($medicalSpecialty->{$methods['get']}());
            $this->assertEmpty($person->{$invertedMethod}());
        }
    }

    /**
     * @return array|array[]
     */
    public function specialtyProvider(): array
    {
        return [
            ['set', 'Spécialité'],
            ['exception', 'Formation'],
        ];
    }

    /**
     * @return array|array[]
     */
    public function otherSpecialtiesProvider(): array
    {
        return [
            [
                'set',
                'Formation',
                [
                    'get' => 'getFormationPersonNaturals',
                    'add' => 'addFormationPersonNatural',
                    'remove' => 'removeFormationPersonNatural'
                ],
                'getFormations'
            ],
            [
                'exception',
                'Capacité',
                [
                    'get' => 'getFormationPersonNaturals',
                    'add' => 'addFormationPersonNatural',
                    'remove' => 'removeFormationPersonNatural'
                ],
                'getFormations'
            ],
            [
                'set',
                'Expérience',
                [
                    'get' => 'getOrientationPersonNaturals',
                    'add' => 'addOrientationPersonNatural',
                    'remove' => 'removeOrientationPersonNatural'
                ],
                'getOrientations'
            ],
            [
                'exception',
                'Capacité',
                [
                    'get' => 'getOrientationPersonNaturals',
                    'add' => 'addOrientationPersonNatural',
                    'remove' => 'removeOrientationPersonNatural'
                ],
                'getOrientations'
            ],
            [
                'set',
                'Compétence',
                [
                    'get' => 'getSkillPersonNaturals',
                    'add' => 'addSkillPersonNatural',
                    'remove' => 'removeSkillPersonNatural'
                ],
                'getSkills'
            ],
            [
                'exception',
                'Capacité',
                [
                    'get' => 'getSkillPersonNaturals',
                    'add' => 'addSkillPersonNatural',
                    'remove' => 'removeSkillPersonNatural'
                ],
                'getSkills'
            ],
            [
                'set',
                'Capacité',
                [
                    'get' => 'getCapacityPersonNaturals',
                    'add' => 'addCapacityPersonNatural',
                    'remove' => 'removeCapacityPersonNatural'
                ],
                'getCapacities'
            ],
            [
                'exception',
                'Formation',
                [
                    'get' => 'getCapacityPersonNaturals',
                    'add' => 'addCapacityPersonNatural',
                    'remove' => 'removeCapacityPersonNatural'
                ],
                'getCapacities'
            ],
        ];
    }
}
