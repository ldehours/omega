<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Department;
use App\Entity\Core\Region;
use PHPUnit\Framework\TestCase;

class RegionTest extends TestCase
{
    public function testAddRemoveDepartment(): void
    {
        $region = new Region();
        $department = new Department();
        $this->assertEmpty($region->getDepartments());

        $region->addDepartment($department);
        $this->assertCount(1, $region->getDepartments());

        $region->removeDepartment($department);
        $this->assertEmpty($region->getDepartments());
    }
}
