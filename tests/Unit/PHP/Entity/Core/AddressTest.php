<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Address;
use App\Entity\Core\PersonLegal;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    public function testConstructor(): void
    {
        $address = new Address();
        $this->assertEmpty($address->getPerson());
    }

    public function testAddRemovePerson(): void
    {
        $address = new Address();
        $person = new PersonLegal();
        $this->assertEmpty($address->getPerson());

        $address->addPerson($person);
        $this->assertCount(1, $address->getPerson());

        $address->removePerson($person);
        $this->assertEmpty($address->getPerson());
    }

    /**
     * @dataProvider domiciliationsProvider
     * @param string $expected
     * @param array $domiciliations
     */
    public function testDomiciliations(string $expected, array $domiciliations): void
    {
        $address = new Address();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $address->setDomiciliations($domiciliations);
        if ($expected !== 'exception') {
            $this->assertSame($domiciliations, $address->getDomiciliations());
        }
    }

    /**
     * @dataProvider usagesProvider
     * @param string $expected
     * @param array $usages
     */
    public function testUsages(string $expected, array $usages): void
    {
        $address = new Address();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $address->setUsages($usages);
        if ($expected !== 'exception') {
            $this->assertSame($usages, $address->getUsages());
        }
    }

    /**
     * @return array|array[]
     */
    public function domiciliationsProvider(): array
    {
        return [
            ['set', [Address::OFFICE, Address::HOME]],
            ['set', [Address::OTHER]],
            ['set', [Address::OTHER, Address::OFFICE, Address::HOME]],
            ['exception', [0, 2]],
            ['exception', [Address::OFFICE, 5]]
        ];
    }

    /**
     * @return array|array[]
     */
    public function usagesProvider(): array
    {
        return [
            ['set', [Address::FISCAL, Address::CORRESPONDENCE]],
            ['set', [Address::BILLING]],
            ['set', [Address::BILLING, Address::FISCAL, Address::CORRESPONDENCE]],
            ['exception', [0, 2]],
            ['exception', [Address::FISCAL, 5]]
        ];
    }
}
