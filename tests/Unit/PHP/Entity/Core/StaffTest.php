<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Discussion;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Core\Telephone;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;
use PHPUnit\Framework\TestCase;

class StaffTest extends TestCase
{
    /**
     * @dataProvider entityCollectionProvider
     * @param $class
     * @param string $field
     * @param string $getMethod
     */
    public function testAddRemove($class, string $field, string $getMethod): void
    {
        $currentEntity = new Staff();
        $entity = new $class();
        $this->assertEmpty($currentEntity->{$getMethod}());

        $currentEntity->{'add' . $field}($entity);
        $this->assertCount(1, $currentEntity->{$getMethod}());

        $currentEntity->{'remove' . $field}($entity);
        $this->assertEmpty($currentEntity->{$getMethod}());
    }

    /**
     * @return array
     */
    public function entityCollectionProvider(): array
    {
        return [
            [Telephone::class, 'ExternalPhone', 'getExternalPhones'],
            [PersonNatural::class, 'ClientPortfolio', 'getClientPortfolio'],
            [Discussion::class, 'Discussion', 'getDiscussions'],
            [PersonNatural::class, 'PersonsCreated', 'getPersonsCreated'],
            [PersonLegal::class, 'PersonsCreated', 'getPersonsCreated'],
            [Offer::class, 'Substitution', 'getSubstitutions'],
            [Wish::class, 'Substitution', 'getSubstitutions'],
        ];
    }

    public function testUsername(): void
    {
        $staff = new Staff();
        $label = 'nicknametest';
        $staff->setNickname($label);
        $this->assertSame($label, $staff->getUsername());
    }

    /**
     * @dataProvider rolesProvider
     * @param array $expected
     * @param array $roles
     */
    public function testRoles(array $expected, array $roles): void
    {
        $staff = new Staff();
        $staff->setRoles($roles);
        $this->assertEquals($expected, $staff->getRoles());
    }

    public function testRemoveAllExternalPhones(): void
    {
        $staff = new Staff();
        $phone = new Telephone();
        $phone2 = new Telephone();
        $this->assertEmpty($staff->getExternalPhones());

        $staff->addExternalPhone($phone)->addExternalPhone($phone2);
        $this->assertCount(2, $staff->getExternalPhones());

        $staff->removeAllExternalPhones();
        $this->assertEmpty($staff->getExternalPhones());
    }

    /**
     * @dataProvider namesProvider
     * @param string $expected
     * @param string $lastName
     * @param string $firstName
     */
    public function testToString(string $expected, string $lastName, string $firstName): void
    {
        $staff = new Staff();
        $staff->setLastName($lastName)->setFirstName($firstName);
        $this->assertSame($expected, $staff->__toString());
    }

    /**
     * @dataProvider genderProvider
     * @param string $expected
     * @param int|null $gender
     * @throws \Exception
     */
    public function testGender(string $expected, ?int $gender): void
    {
        $staff = new Staff();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }
        $staff->setGender($gender);
        if ($expected !== 'exception') {
            $this->assertSame($gender, $staff->getGender());
        }
    }

    /**
     * @return array|array[]
     */
    public function genderProvider(): array
    {
        return [
            ['set', Staff::GENDER_MALE],
            ['set', Staff::GENDER_FEMALE],
            ['set', Staff::GENDER_OTHER],
            ['exception', 0],
            ['exception', 238],
        ];
    }

    /**
     * @return array
     */
    public function namesProvider(): array
    {
        return [
            ['Valjean Jean', 'Valjean', 'Jean'],
            ['Thylène Bledemé', 'Thylène', 'Bledemé'],
            ['Belcourt Jean-René', 'Belcourt', 'Jean-René'],
        ];
    }

    /**
     * @return array
     */
    public function rolesProvider(): array
    {
        return [
            [
                ['ROLE_USER', 'ROLE_CFML', 'ROLE_COMMERCIAL'],
                ['ROLE_USER', 'ROLE_CFML', 'ROLE_COMMERCIAL']
            ],
            [
                ['ROLE_CFML'],
                ['ROLE_CFML']
            ],
            [
                ['ROLE_USER'],
                []
            ],
        ];
    }
}
