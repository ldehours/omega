<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\JobName;
use App\Entity\Core\PersonAccess;
use PHPUnit\Framework\TestCase;

class PersonAccessTest extends TestCase
{
    /**
     * @dataProvider typeProvider
     * @param string $expected
     * @param int $type
     */
    public function testType(string $expected, int $type): void
    {
        $personAccess = new PersonAccess();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }

        $personAccess->setType($type);

        if ($expected !== 'exception') {
            $this->assertSame($type, $personAccess->getType());
        }
    }

    public function testAddRemoveJobName(): void
    {
        $personAccess = new PersonAccess();
        $jobName = new JobName();
        $this->assertEmpty($personAccess->getJobName());

        $personAccess->addJobName($jobName);
        $this->assertCount(1, $personAccess->getJobName());

        $personAccess->removeJobName($jobName);
        $this->assertEmpty($personAccess->getJobName());
    }

    /**
     * @return array|array[]
     */
    public function typeProvider(): array
    {
        return [
            ['set', PersonAccess::DEPOSIT_LIBERAL_ANNOUNCE],
            ['set', PersonAccess::DEPOSIT_SALARIED_ANNOUNCE],
            ['set', PersonAccess::DEPOSIT_TO_CESSION],
            ['set', PersonAccess::CANDIDATE_TO_LIBERAL_ANNOUNCE],
            ['set', PersonAccess::CANDIDATE_TO_SALARIED_ANNOUNCE],
            ['set', PersonAccess::CANDIDATE_TO_CESSION],
            ['set', PersonAccess::SUBSCRIBE_TO_CFML],
            ['set', PersonAccess::SUBSCRIBE_TO_LMS],
            ['set', PersonAccess::SUBSCRIBE_TO_BNC],
            ['set', PersonAccess::SUBSCRIBE_TO_CONTRACT_RELAY],
            ['set', PersonAccess::SUBSCRIBE_TO_CONTRACT_PRIVILEGE],
            ['exception', 25],
            ['exception', 546],
            ['exception', 0],
            ['exception', 988],
        ];
    }
}
