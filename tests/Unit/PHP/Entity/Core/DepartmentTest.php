<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Address;
use App\Entity\Core\Department;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;
use PHPUnit\Framework\TestCase;

class DepartmentTest extends TestCase
{
    public function testAddRemoveAddresses(): void
    {
        $department = new Department();
        $address = new Address();
        $this->assertEmpty($department->getAddresses());

        $department->addAddress($address);
        $this->assertCount(1, $department->getAddresses());
        $this->assertNotNull($address->getDepartment());

        $department->removeAddress($address);
        $this->assertEmpty($department->getAddresses());
        $this->assertNull($address->getDepartment());
    }

    public function testAddRemoveWishes(): void
    {
        $department = new Department();
        $wish = new Wish();
        $this->assertEmpty($department->getWishes());
        $this->assertEmpty($wish->getDepartments());

        $department->addWish($wish);
        $this->assertCount(1, $department->getWishes());
        $this->assertCount(1, $wish->getDepartments());

        $department->removeWish($wish);
        $this->assertEmpty($department->getWishes());
        $this->assertEmpty($wish->getDepartments());
    }

    public function testAddRemoveOffers(): void
    {
        $department = new Department();
        $offer = new Offer();
        $this->assertEmpty($department->getOffers());

        $department->addOffer($offer);
        $this->assertCount(1, $department->getOffers());
        $this->assertNotNull($offer->getDepartment());

        $department->removeOffer($offer);
        $this->assertEmpty($department->getOffers());
        $this->assertNull($offer->getDepartment());
    }
}
