<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Origin;
use App\Entity\Core\PersonNatural;
use App\Entity\Offer\Offer;
use PHPUnit\Framework\TestCase;

class OriginTest extends TestCase
{
    /**
     * @dataProvider usagesProvider
     * @param string $expected
     * @param array $usages
     */
    public function testUsages(string $expected, array $usages): void
    {
        $origin = new Origin();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $origin->setUsages($usages);
        if ($expected !== 'exception') {
            $this->assertSame($usages, $origin->getUsages());
        }
    }

    public function testAddRemovePerson(): void
    {
        $origin = new Origin();
        $person = new PersonNatural();
        $this->assertEmpty($origin->getPersons());

        $origin->addPerson($person);
        $this->assertCount(1, $origin->getPersons());

        $origin->removePerson($person);
        $this->assertEmpty($origin->getPersons());
    }

    public function testAddRemoveOffer(): void
    {
        $origin = new Origin();
        $offer = new Offer();
        $this->assertEmpty($origin->getOffers());

        $origin->addOffer($offer);
        $this->assertCount(1, $origin->getOffers());

        $origin->removeOffer($offer);
        $this->assertEmpty($origin->getOffers());
    }

    public function testToString(): void
    {
        $origin = new Origin();
        $label = 'ouais re';
        $origin->setLabel($label);
        $this->assertSame($label, $origin->__toString());
        $this->assertSame($origin->getLabel(), $origin->__toString());
    }

    /**
     * @return array|array[]
     */
    public function usagesProvider(): array
    {
        return [
            ['set', [Origin::OFFER]],
            ['set', [Origin::PERSON]],
            ['set', [Origin::PERSON, Origin::OFFER]],
            ['set', []],
            ['exception', [12, 895]],
        ];
    }
}
