<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\Feature;
use App\Entity\Core\Role;
use PHPUnit\Framework\TestCase;

class RoleTest extends TestCase
{
    public function testAddRemoveFeature(): void
    {
        $role = new Role();
        $feature = new Feature();
        $this->assertEmpty($role->getFeatures());

        $role->addFeature($feature);
        $this->assertCount(1, $role->getFeatures());

        $role->removeFeature($feature);
        $this->assertEmpty($role->getFeatures());
    }

    /**
     * @dataProvider nameProvider
     * @param string $expected
     * @param string|null $label
     */
    public function testToString(string $expected, ?string $label): void
    {
        $role = new Role();
        if ($label !== null) {
            $role->setName($label);
        }
        $this->assertSame($expected, $role->__toString());
    }

    public function nameProvider(): array
    {
        return [
            ['ouais re', 'ouais re'],
            ['', ''],
            ['', null],
        ];
    }
}
