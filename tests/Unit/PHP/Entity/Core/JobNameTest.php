<?php

namespace App\Tests\Unit\PHP\Entity\Core;

use App\Entity\Core\JobName;
use App\Entity\Core\PersonAccess;
use App\Entity\Core\PersonLegal;
use PHPUnit\Framework\TestCase;

class JobNameTest extends TestCase
{
    /**
     * @dataProvider classificationProvider
     * @param $expected
     * @param $classification
     */
    public function testClassifications($expected, $classification): void
    {
        $jobName = new JobName();
        if ($expected === 'exception') {
            $this->expectException(\InvalidArgumentException::class);
        }
        $jobName->setClassification($classification);
        if ($expected !== 'exception') {
            $this->assertSame($classification, $jobName->getClassification());
        }
    }

    public function testAddRemovePerson(): void
    {
        $jobName = new JobName();
        $person = new PersonLegal();
        $this->assertEmpty($jobName->getPersons());
        $this->assertEmpty($person->getJobNames());

        $jobName->addPerson($person);
        $this->assertCount(1, $jobName->getPersons());
        $this->assertCount(1, $person->getJobNames());

        $jobName->removePerson($person);
        $this->assertEmpty($jobName->getPersons());
        $this->assertEmpty($person->getJobNames());
    }

    public function testToString(): void
    {
        $jobName = new JobName();
        $label = 'ouais re';
        $jobName->setLabel($label);
        $this->assertSame($label, $jobName->__toString());
        $this->assertSame($jobName->getLabel(), $jobName->__toString());
    }

    public function testAddRemovePersonAccess(): void
    {
        $jobName = new JobName();
        $personAccess = new PersonAccess();
        $this->assertEmpty($jobName->getPersonAccesses());
        $this->assertEmpty($personAccess->getJobName());

        $jobName->addPersonAccess($personAccess);
        $this->assertCount(1, $jobName->getPersonAccesses());
        $this->assertCount(1, $personAccess->getJobName());

        $jobName->removePersonAccess($personAccess);
        $this->assertEmpty($jobName->getPersonAccesses());
        $this->assertEmpty($personAccess->getJobName());
    }

    /**
     * @return array|array[]
     */
    public function classificationProvider(): array
    {
        return [
            ['set', JobName::BASED],
            ['set', JobName::SUBSTITUTE],
            ['set', JobName::INSTITUTION],
            ['set', JobName::OTHER],
            ['set', JobName::INSTITUTION_FUNCTION],
            ['exception', 1],
            ['exception', 158],
        ];
    }
}
