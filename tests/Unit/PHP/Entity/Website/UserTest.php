<?php

namespace App\Tests\Unit\PHP\Entity\Website;

use App\Entity\Website\User;
use App\Utils\DateTime;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @dataProvider dateCreaProvider
     * @param string $expected
     * @param string|null $date
     * @throws \Exception
     */
    public function testDateCrea(string $expected, ?string $date): void
    {
        $user = new User();
        if ($date !== null) {
            $user->setDatecrea(new DateTime($date));
        }
        $this->assertSame($expected, $user->getDatecrea()->format('Y-m-d'));
    }

    /**
     * @return array[]
     */
    public function dateCreaProvider(): array
    {
        return [
            [(new DateTime())->format('Y-m-d'), null],
            ['2020-05-16', '2020-05-16'],
        ];
    }

    /**
     * @dataProvider statutProvider
     * @param string $expected
     * @param string $statut
     * @throws \Exception
     */
    public function testStatut(string $expected, string $statut): void
    {
        $user = new User();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }
        $user->setStatut($statut);
        if ($expected !== 'exception') {
            $this->assertSame($statut, $user->getStatut());
        }
    }

    /**
     * @return \string[][]
     */
    public function statutProvider(): array
    {
        return [
            ['set', ''],
            ['set', 'a'],
            ['set', 'ab'],
            ['exception', 'abc'],
        ];
    }
}
