<?php

namespace App\Tests\Unit\PHP\Entity\Website;

use App\Entity\Website\Coord;
use PHPUnit\Framework\TestCase;

class CoordTest extends TestCase
{
    /**
     * @dataProvider adProvider
     * @param string $expected
     * @param string|null $dummyString
     */
    public function testStringNotNullGetSet(string $expected, ?string $dummyString): void
    {
        $coord = new Coord();
        if ($dummyString !== null) {
            $coord->setAd2($dummyString)->setAd3($dummyString)
                ->setTel($dummyString)->setPort($dummyString)->setFax($dummyString)
                ->setLic($dummyString)->setRpps($dummyString)->setOdm($dummyString);
        }
        $this->assertSame($expected, $coord->getAd3());
        $this->assertSame($expected, $coord->getAd2());
        $this->assertSame($expected, $coord->getTel());
        $this->assertSame($expected, $coord->getPort());
        $this->assertSame($expected, $coord->getFax());
        $this->assertSame($expected, $coord->getLic());
        $this->assertSame($expected, $coord->getRpps());
        $this->assertSame($expected, $coord->getOdm());
    }

    /**
     * @return array
     */
    public function adProvider(): array
    {
        return [
            ['', null],
            ['ouais re', 'ouais re'],
        ];
    }

    /**
     * @dataProvider cpProvider
     * @param string $expected
     * @param string $cp
     * @throws \Exception
     */
    public function testCP(string $expected, string $cp): void
    {
        $coord = new Coord();
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }
        $coord->setCp($cp);
        if ($expected !== 'exception') {
            $this->assertSame($cp, $coord->getCp());
        }
    }

    /**
     * @return array|\string[][]
     */
    public function cpProvider(): array
    {
        return [
            ['set', ''],
            ['set', '1'],
            ['set', '12'],
            ['set', '123'],
            ['set', '1234'],
            ['set', '12345'],
            ['exception', '123456'],
        ];
    }

    /**
     * @dataProvider anneeNaissanceProvider
     * @param string $expected
     * @param int $annee
     * @throws \Exception
     */
    public function testAnneeNaissance(string $expected, int $annee): void
    {
        $coord = new Coord();
        $this->assertSame(0, $coord->getAnnee_Naissance());
        if ($expected === 'exception') {
            $this->expectException(\Exception::class);
        }
        $coord->setAnnee_Naissance($annee);
        if ($expected !== 'exception') {
            $this->assertSame($annee, $coord->getAnnee_Naissance());
        }
    }

    /**
     * @return array|array[]
     */
    public function anneeNaissanceProvider(): array
    {
        return [
            ['set', 0],
            ['set', 10],
            ['set', 100],
            ['set', 1000],
            ['set', 2008],
            ['exception', 10000],
        ];
    }
}
