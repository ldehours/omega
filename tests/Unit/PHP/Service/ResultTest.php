<?php

namespace App\Tests\Unit\PHP\Service;

use App\Service\Result;
use PHPUnit\Framework\TestCase;

/**
 * Class ResultTest
 * @package App\Tests\Unit\PHP\Service
 */
class ResultTest extends TestCase
{
    /**
     * @dataProvider resultDataProvider
     * @param bool $success
     * @param string $message
     * @param array $data
     */
    public function testContruct(bool $success, string $message, array $data = []): void
    {
        $result = new Result($success, $message, $data);
        $this->assertSame($success, $result->getSuccess());
        $this->assertSame($message, $result->getMessage());
        $this->assertSame($data, $result->getData());
        $this->assertCount(count($data), $result->getData());
    }

    /**
     * @dataProvider resultDataProvider
     * @param bool $success
     * @param string $message
     * @param array $data
     */
    public function testGetSetWorking(bool $success, string $message, array $data = []): void
    {
        $result = new Result(true, '');
        $result->setSuccess($success)
            ->setMessage($message)
            ->setData($data);

        $this->assertSame($success, $result->getSuccess());
        $this->assertSame($message, $result->getMessage());
        $this->assertSame($data, $result->getData());
        $this->assertCount(count($data), $result->getData());
    }

    /**
     * @return array|array[]
     */
    public function resultDataProvider(): array
    {
        return [
            [true, 'gg à tous',],
            [true, 'gg à tous', []],
            [true, 'gg à tous', ['ouais re', 'je sais pas', 54, '', false]],
            [false, 'gg à tous',],
            [false, 'gg à tous', []],
            [false, 'gg à tous', ['ouais re', 'je sais pas', 54, '', false]],
            [true, '',],
            [true, '', []],
            [true, '', ['ouais re', 'je sais pas', 54, '', false]],
            [false, '',],
            [false, '', []],
            [false, '', ['ouais re', 'je sais pas', 54, '', false]],
        ];
    }
}
