<?php

namespace App\Tests\Unit\PHP\Service;

use App\Service\SMSBox;
use App\Service\SMSManager;
use PHPUnit\Framework\TestCase;

/**
 * Class SMSManagerTest
 * @package App\Tests\Unit\PHP\Service
 */
class SMSManagerTest extends TestCase
{
    public function testWorking(): void
    {
        $smsManager = new SMSManager(new SMSBox($_ENV['SMS_API'], 'ouais re'));
        $smsManager->addRecipient('0605040302');
        $smsManager->addRecipient('0605040301');
        $smsManager->addRecipient('0605040303');
        $this->assertCount(3, $smsManager->getRecipients());

        $smsManager->removeRecipient('0605040301');
        $this->assertCount(2, $smsManager->getRecipients());
        $this->assertFalse(array_search('0605040301', $smsManager->getRecipients(), true));

        $smsManager->setRecipients([]);
        $this->assertEmpty($smsManager->getRecipients());
    }
}
