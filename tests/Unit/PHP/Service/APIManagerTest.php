<?php

namespace App\Tests\Unit\PHP\Service;

use App\Service\APIManager;
use App\Utils\API;
use PHPUnit\Framework\TestCase;

class APIManagerTest extends TestCase
{
    public function testConstruct(): void
    {
        $apiManager = new APIManager();
        $this->assertCount(2, $apiManager->getAPIs());
        foreach ($apiManager->getAPIs() as $api) {
            $this->assertInstanceOf(API::class, $api);
        }
    }
}
