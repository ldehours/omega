<?php

namespace App\Tests\Unit\PHP\Service;

use App\Service\DatabaseErrorInfo;
use App\Service\DatabaseResult;
use PHPUnit\Framework\TestCase;

/**
 * Class DatabaseResultTest
 * @package App\Tests\Unit\PHP\Service
 */
class DatabaseResultTest extends TestCase
{
    /**
     * @dataProvider dbResultDataProvider
     * @param $numberLines
     * @param $data
     * @param DatabaseErrorInfo|null $error
     */
    public function testWorking($numberLines, $data, DatabaseErrorInfo $error = null): void
    {
        $databaseResult = new DatabaseResult($numberLines, $data, $error);
        $this->assertSame($numberLines, $databaseResult->getNumberLines());
        $this->assertSame($data, $databaseResult->getData());
        $this->assertSame($error, $databaseResult->getError());
    }

    /**
     * @dataProvider dbResultDataProvider
     * @param $numberLines
     * @param $data
     * @param DatabaseErrorInfo|null $error
     */
    public function testSetMethods($numberLines, $data, DatabaseErrorInfo $error = null): void
    {
        $databaseResult = new DatabaseResult(0, []);
        $databaseResult->setData($data);
        if ($error !== null) {
            $databaseResult->setError($error);
        }
        $this->assertSame($data, $databaseResult->getData());
        $this->assertSame(count($databaseResult->getData()), $databaseResult->getNumberLines());
        $this->assertSame($error, $databaseResult->getError());

        $databaseResult->setNumberLines($numberLines);
        $this->assertSame($numberLines, $databaseResult->getNumberLines());
    }

    /**
     * @return array|array[]
     */
    public function dbResultDataProvider(): array
    {
        return [
            [4, ['', '', '', ''], new DatabaseErrorInfo(1, 'une erreur (:')],
            [3, ['', '', '']],
            [0, [], new DatabaseErrorInfo(1, 'une erreur (:')],
            [2, ['', ''], new DatabaseErrorInfo(1, 'une erreur (:')]
        ];
    }
}
