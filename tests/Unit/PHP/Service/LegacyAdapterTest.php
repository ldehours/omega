<?php

namespace App\Tests\Unit\PHP\Service;

use App\Entity\Core\JobName;
use App\Service\LegacyAdapter;
use PHPUnit\Framework\TestCase;

/**
 * Class LegacyAdapterTest
 * @package App\Tests\Unit\PHP\Service
 */
class LegacyAdapterTest extends TestCase
{
    /**
     * @dataProvider statusInitialProvider
     * @param string $expected
     * @param int $status
     * @param bool $returnLower
     */
    public function testGetStatusInitial(string $expected, int $status, bool $returnLower = false): void
    {
        $this->assertSame($expected, LegacyAdapter::getStatusInitial($status, $returnLower));
    }

    /**
     * @return array
     */
    public function statusInitialProvider(): array
    {
        return [
            ['I', JobName::BASED, false],
            ['R', JobName::SUBSTITUTE, false],
            ['E', JobName::INSTITUTION, false],
            ['A', JobName::OTHER, false],
            ['A', JobName::INSTITUTION_FUNCTION, false],
            ['i', JobName::BASED, true],
            ['r', JobName::SUBSTITUTE, true],
            ['e', JobName::INSTITUTION, true],
            ['a', JobName::OTHER, true],
            ['a', JobName::INSTITUTION_FUNCTION, true],
            ['I', JobName::BASED],
            ['R', JobName::SUBSTITUTE],
        ];
    }
}
