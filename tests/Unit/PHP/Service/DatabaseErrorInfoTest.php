<?php

namespace App\Tests\Unit\PHP\Service;

use App\Service\DatabaseErrorInfo;
use PHPUnit\Framework\TestCase;

/**
 * Class DatabaseErrorInfoTest
 * @package App\Tests\Unit\PHP\Service
 */
class DatabaseErrorInfoTest extends TestCase
{
    /**
     * @dataProvider dbErrorDataProvider
     * @param int $code
     * @param string $message
     */
    public function testWorking(int $code, string $message): void
    {
        $databaseErrorInfo = new DatabaseErrorInfo($code, $message);
        $this->assertSame($code, $databaseErrorInfo->getCode());
        $this->assertSame($message, $databaseErrorInfo->getMessage());
    }

    /**
     * @return array|array[]
     */
    public function dbErrorDataProvider(): array
    {
        return [
            [12, 'salut'],
            [0, 'hahaha ouais'],
            [13, ''],
        ];
    }
}
