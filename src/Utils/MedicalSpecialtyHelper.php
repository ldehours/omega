<?php


namespace App\Utils;


use App\Repository\Core\MedicalSpecialtyRepository;

class MedicalSpecialtyHelper
{
    /**
     * @var MedicalSpecialtyRepository
     */
    private $medicalSpecialtyRepository;

    /**
     * MedicalSpecialtyHelper constructor.
     * @param MedicalSpecialtyRepository $medicalSpecialtyRepository
     */
    public function __construct(MedicalSpecialtyRepository $medicalSpecialtyRepository)
    {
        $this->medicalSpecialtyRepository = $medicalSpecialtyRepository;
    }

    /**
     * @param array|null $requestedSkills
     * @return array
     */
    public function getFormattedOfferSkills(?array $requestedSkills):array
    {
        $newSkills = [];
        if(isset($requestedSkills) && !empty($requestedSkills)){
            $skillsIds = $this->medicalSpecialtyRepository->getIdsFromLabel('Compétence');
            foreach ($requestedSkills as $skill){
                $rate = $skill['rate'];
                if(in_array(['id' => $skill['specialty']],$skillsIds) && !empty($rate)){
                    if(intval($rate) > 100){
                        $skill['rate'] = '100';
                    }elseif(intval($rate) < 0){
                        $skill['rate'] = '0';
                    }
                    $newSkills[] = $skill;
                }
            }
        }
        return $newSkills;
    }
}