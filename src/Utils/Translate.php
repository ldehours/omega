<?php


namespace App\Utils;


class Translate
{

    /**
     * @var $labels array
     */
    private $labels;

    /**
     * @var $icons array
     */
    private $icons;

    /**
     * Translate constructor.
     * @param array $labels
     * @param array $icons = null
     */
    public function __construct(array $labels, array $icons = [])
    {
        $this->labels = $labels;
        $this->icons = $icons;
    }

    /**
     * @param int $classConstant
     * @param bool $useFallbackLabel
     * @return string
     */
    public function getIcon(int $classConstant, bool $useFallbackLabel = false): string
    {
        return $this->icons[$classConstant] ?? (($useFallbackLabel) ? $this->getLabel($classConstant) : "");
    }


    /**
     * @param int|null $classConstant
     * @return string
     */
    public function getLabel(?int $classConstant): string
    {
        if ($classConstant == null) {
            return "";
        }
        return $this->labels[$classConstant] ?? "";
    }
}