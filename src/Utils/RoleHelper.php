<?php


namespace App\Utils;


use App\Entity\Core\Staff;
use Symfony\Component\Security\Core\Security;

class RoleHelper
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param array $roles
     * @param Staff|null $user
     * @return bool
     */
    public function userHasRoles(array $roles, ?Staff $user = null): bool
    {
        foreach ($roles as $role){
            if($this->security->isGranted($role, $user)){
                return true;
            }
        }
        return false;
    }
}