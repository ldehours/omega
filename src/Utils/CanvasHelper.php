<?php

namespace App\Utils;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\Offer;
use App\Repository\Offer\CriterionPossibleValueRepository;
use App\Repository\Offer\CriterionRepository;

class CanvasHelper
{

    /**
     * @var CriterionRepository
     */
    private $criterionRepository;
    /**
     * @var CriterionPossibleValueRepository
     */
    private $criterionPossibleValueRepository;

    /**
     * CanvasHelper constructor.
     * @param CriterionRepository $criterionRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     */
    public function __construct(
        CriterionRepository $criterionRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository
    ){
        $this->criterionRepository = $criterionRepository;
        $this->criterionPossibleValueRepository = $criterionPossibleValueRepository;
    }

    /**
     * @param Offer $offer
     * @return array|array[]
     */
    public function getFormDispositionForOffer(Offer $offer):array{
        $canvas = $offer->getCanvas();
        $criterionArray = [];
        $tabForOldFileInput = [];
        if($canvas !== null){
            $formDisposition = $canvas->getFormDisposition();
            $listCriterion = [];
            foreach ($formDisposition as $row) {
                foreach ($row as $value) {
                    $listCriterion[] = $this->criterionRepository->findOneById($value[0]);
                }
            }
            foreach ($listCriterion as $criterion) {
                if($criterion === null){
                    continue;
                }
                foreach ($criterion->getCriterionPossibleValues() as $criterionPossibleValue) {
                    if ($criterionPossibleValue->getSubstitutionType() === $canvas->getSubstitutionType()) {
                        $criterionArray[$criterion->getId()] = [
                            'label' => $criterion->getLabel(),
                            'type' => $criterionPossibleValue->getCriterionType(),
                            'value' => json_decode($criterionPossibleValue->getPossibleValue()),
                            'dependency' => []
                        ];
                        $dependency = $criterion->getDependency();
                        if (!empty($dependency)) {
                            $criterionArray[$criterion->getId()]['dependency'] = [
                                'id' => array_keys($dependency)[0],
                                'value' => $dependency[array_keys($dependency)[0]]
                            ];
                        }
                        if ($criterion->getLabel() == 'Département') {
                            $jsonData = [];
                            foreach (json_decode($criterionPossibleValue->getPossibleValue()) as $source) {
                                $sourceKey = array_reverse(explode('/', explode('.', $source)[1]))[0];
                                foreach (json_decode(file_get_contents($source), true) as $a) {
                                    $jsonData[$sourceKey][$a['code']] = $a['nom'];
                                }
                            }
                            $criterionArray[$criterion->getId()]['criterionValue'] = $jsonData;
                        }
                    }
                }
            }
            foreach ($offer->getSubstitutionCriteria() as $substitutionCriterion) {
                $criterion = $this->criterionRepository->findOneById($substitutionCriterion->getCriterion()->getId());

                $criterionPossibleValue = $this->criterionPossibleValueRepository->findOneBy(
                    [
                        'criterion' => $criterion,
                        'substitutionType' => $canvas->getSubstitutionType(),
                        'substitutionNature' => Canvas::CANVAS_OFFER
                    ]
                );

                switch ($criterionPossibleValue->getCriterionType()) {
                    case Criterion::TYPE_CHOICE_WEEK:
                        $substitutionCriterionTab = [];
                        for ($i = 0; $i < 14; $i++) {
                            if (($substitutionCriterion->getValue() & pow(2, $i)) != 0) {
                                $substitutionCriterionTab[$i] = 1;
                            } else {
                                $substitutionCriterionTab[$i] = 0;
                            }
                        }
                        break;
                    case Criterion::TYPE_JSON:
                        $jsonData = [];
                        foreach (json_decode($criterionPossibleValue->getPossibleValue()) as $source) {
                            $sourceKey = array_reverse(explode('/', explode('.', $source)[1]))[0];
                            foreach (json_decode(file_get_contents($source), true) as $a) {
                                $jsonData[$sourceKey][$a['code']] = $a['nom'];
                            }
                        }
                        $substitutionCriterionTab = $jsonData;

                        $criterionArray[$substitutionCriterion->getCriterion()->getId()]['selected'] = $substitutionCriterion->getValue();
                        break;
                    case Criterion::TYPE_FILE:
                        $tabForOldFileInput[$substitutionCriterion->getCriterion()->getId()] = $substitutionCriterion->getValue();
                        break;
                    default:
                        $substitutionCriterionTab = $substitutionCriterion->getValue();
                        break;
                }
                $criterionArray[$substitutionCriterion->getCriterion()->getId()]['criterionValue'] = $substitutionCriterionTab;
            }
        }

        return [
            'criterionArray' => $criterionArray,
            'tabForOldFileInput' => $tabForOldFileInput
        ];
    }
}