<?php


namespace App\Utils;


use Symfony\Component\HttpFoundation\JsonResponse;

class API
{
    private $url;
    private $APIHelper;

    /**
     * API constructor.
     * @param string $url
     * @param string|null $token
     */
    public function __construct(string $url, string $token = null)
    {
        $this->url = $url;
        $this->APIHelper = new APIHelper();
    }

    /**
     * @return JsonResponse
     */
    public function call(): JsonResponse
    {
        return $this->APIHelper->get($this->url);
    }
}