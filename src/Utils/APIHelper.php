<?php


namespace App\Utils;


use Symfony\Component\HttpFoundation\JsonResponse;

class APIHelper
{

    CONST METHOD_POST = 1;
    CONST METHOD_PUT = 2;
    CONST METHOD_GET = 3;

    /**
     * @param int $method
     * @param string $url
     * @param array $data
     * @return bool|string
     */
    public function send(int $method, string $url, array $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        switch ($method) {
            case self::METHOD_POST:
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data != null) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case self::METHOD_PUT:
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case self::METHOD_GET:
            default:
                if ($data != null) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    /**
     * @param string $url
     * @return JsonResponse
     */
    public function get(string $url):JsonResponse
    {

        $curl = curl_init();

        $opts = [
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_CONNECTTIMEOUT => 30
        ];

        curl_setopt_array($curl, $opts);

        $response = curl_exec($curl);
        curl_close($curl);
        return new JsonResponse($response);
    }
}