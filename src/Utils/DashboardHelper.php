<?php

namespace App\Utils;

use App\Entity\Core\Discussion;
use App\Entity\Core\JobName;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Offer\Offer;
use App\Entity\Offer\SubstitutionType;
use App\Repository\Core\DiscussionRepository;
use App\Repository\Offer\OfferRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

class DashboardHelper
{
    /** @var array */
    private $arrayDataDiscussions = [];

    /** @var array */
    private $dataCR4 = [];

    /** @var array */
    private $dataCR3 = [];

    /** @var array */
    private $dataCR5 = [];

    /** @var array */
    private $dataCommercial = [];

    /** @var OfferRepository $offerRepository */
    private $offerRepository;

    /** @var DiscussionRepository $discussionRepository */
    private $discussionRepository;

    /**
     * DashboardHelper constructor.
     * @param ManagerRegistry $entityManager
     */
    public function __construct(ManagerRegistry $entityManager)
    {
        $this->offerRepository = $entityManager->getRepository(Offer::class);
        $this->discussionRepository = $entityManager->getRepository(Discussion::class);
    }

    /**
     * @param Person $person
     * @return Discussion[]|Collection|null
     */
    private function getLastFiveDiscussions(Person $person)
    {
        return $this->discussionRepository->findFiveLastDiscussionByTypeAndPerson($person);
    }

    /**
     * @param Person $person
     * @return mixed
     * @throws NonUniqueResultException
     */
    private function getLastDiscussion(Person $person)
    {
        return $this->discussionRepository->findOneLastDiscussionByTypeAndPerson($person);
    }

    /**
     * @param Staff $staff
     * @throws NonUniqueResultException
     */
    public function getOfferByCreator(Staff $staff): void
    {
        $offers = $this->offerRepository->findBy(['creator' => $staff]);

        if ($offers === null) {
            return;
        }

        foreach ($offers as $offer) {
            /** @var Person $person */
            $person = $offer->getPerson();

            $lastFiveDiscussions = $this->getLastFiveDiscussions($person);

            $lastDiscussion = $this->getLastDiscussion($person);

            $this->setArrayDataGetLastDiscussions($person, $lastFiveDiscussions, $lastDiscussion);

            //cr4
            $this->getReplacementOffersMonth($person, $offer);

            //cr3
            $this->getReplacementOffersThreeYears($person, $offer);

            //cr5
            $this->getArrayReplacementOffersQP($person, $offer);

            //commercial
            $this->getArrayCommercialFollowedContract($person, $offer, $lastDiscussion);
        }
    }

    /**
     * @param Person $person
     * @param array|null $fiveLastDiscussion
     * @param Discussion|null $lastDiscussion
     */
    public function setArrayDataGetLastDiscussions(Person $person, ?array $fiveLastDiscussion, ?Discussion $lastDiscussion): void
    {
        $this->arrayDataDiscussions['bubble'] = [];
        $this->arrayDataDiscussions['bubble']['commentaires'] = [];

        if ($fiveLastDiscussion) {
            foreach ($fiveLastDiscussion as $discussion) {
                $this->arrayDataDiscussions['bubble']['commentaires'][] = $discussion->getMemo();
            }

            $this->arrayDataDiscussions['bubble'] += [
                'lastFirstNameContact' => $lastDiscussion->getCreator()->getFirstName(),
                'lastLastNameContact' => $lastDiscussion->getCreator()->getLastName(),
                'lastContactDate' => $lastDiscussion->getCreationDate()->format('d/m/Y'),
                'person_id' => $person->getId()
            ];
        } else {
            // aucune info disponible
            $this->arrayDataDiscussions['bubble'] += [
                'lastFirstNameContact' => 'Aucune info',
                'lastLastNameContact' => '',
                'lastContactDate' => 'Aucune info',
                'person_id' => $person->getId()
            ];
        }
    }

    /**
     * CR 1
     * @param PersonNatural $person
     * @return array|null
     * @throws NonUniqueResultException
     */
    public function getPreviousWeekOrMonth(PersonNatural $person): ?array
    {
        $this->setArrayDataGetLastDiscussions($person, $this->getLastFiveDiscussions($person), $this->getLastDiscussion($person));

        $previousWeek = new DateTime();
        $previousWeek->format('Y-m-d');
        $previousWeek->modify('-1 week');

        $previousMonth = new DateTime();
        $previousMonth->format('Y-m-d');
        $previousMonth->modify('-1 month');

        // array data global pour CR1
        $dataCR1 = [];
        // array data global pour CR1 pour la table
        $dataCR1['table'] = [];

        // si un client correspond, previousDate va être égal previousWeek ou previousMonth pour CR1
        $previousDate = null;

        // prend les clients créés il y à moins d'une semaine
        if ($previousWeek < $person->getCreationDate()) {
            $previousDate = 'previousWeek';
            $dataCR1['table'] += [
                'firstname' => $person->getFirstName(),
                'lastname' => $person->getLastName()
            ];
        }

        // prend les clients créés il y au plus d'une semaine et inférieur à un mois
        if ($previousMonth < $person->getCreationDate() && $previousWeek > $person->getCreationDate()) {
            $previousDate = 'previousMonth';
            $dataCR1['table'] += [
                'firstname' => $person->getFirstName(),
                'lastname' => $person->getLastName(),
                'id' => $person->getId()
            ];
        }

        if ($previousDate) {
            //  discussions trouvées
            $dataCR1 += $this->arrayDataDiscussions;
            return [
                'period_length' => $previousDate,
                'person_data' => $dataCR1
            ];
        }

        return null;
    }

    /**
     * CR 3
     * @param PersonNatural $person
     * @param Offer $offer
     */
    public function getReplacementOffersThreeYears(PersonNatural $person, Offer $offer): void
    {
        $classification = $person->getMainJobName()->getClassification();

        if ($classification === JobName::SUBSTITUTE) {
            $substitution = $offer->getSubstitutionType()->getSubstitutionType();
            $TYPE_LIBERAL_REPLACEMENT = SubstitutionType::TYPES_POSSIBLE;
            $typeLiberal = array_search($substitution, $TYPE_LIBERAL_REPLACEMENT[SubstitutionType::TYPE_LIBERAL_REPLACEMENT], true);
            $typeSalaried = array_search($substitution, $TYPE_LIBERAL_REPLACEMENT[SubstitutionType::TYPE_SALARIED_POSITION], true);

            // array data global pour cr3
            $dataCR3 = [];
            // data concernant les notes pour cr3
            $dataCR3['table'] = [];

            $dateStart = $offer->getStartingDate();
            // récupère des remplacement libéraux ou salariés PAR MS dans les 3 dernières années
            if (($typeSalaried === 0 || $typeLiberal === 0) &&
                $offer->getState() === Offer::STATE_FILLED_BY_US &&
                ($dateStart === (new \DateTime())->modify('-3 years'))) {
                $dataCR3['table'] += [
                    'firstname' => $person->getfirstName(),
                    'lastname' => $person->getlastName()
                ];

                //  discussions trouvées
                $dataCR3 += $this->arrayDataDiscussions;
                $this->dataCR3[] = $dataCR3;
            }
        }
    }

    /**
     * CR 4
     * @param PersonNatural $person
     * @param Offer $offer
     */
    public function getReplacementOffersMonth(PersonNatural $person, Offer $offer): void
    {
        $classification = $person->getMainJobName()->getClassification();

        if ($classification === JobName::SUBSTITUTE) {
            $current_month = new DateTime();
            $current_month = $current_month->format('Y-m');
            $dateStart = $offer->getStartingDate();
            $dateStart = $dateStart->format('Y-m');
            $dataCR4['table'] = [];
            // si l'année et le mois sont égaux
            if ($dateStart === $current_month) {
                $dataCR4['table'] += [
                    'firstname' => $person->getFirstName(),
                    'lastname' => $person->getLastName()
                ];
                $dataCR4 += $this->arrayDataDiscussions;
                $this->dataCR4[] = $dataCR4;
            }
        }
    }

    /**
     * CR 5
     * @param PersonNatural $person
     * @param Offer $offer
     */
    public function getArrayReplacementOffersQP(PersonNatural $person, Offer $offer): void
    {
        $classification = $person->getMainJobName()->getClassification();

        // if ($classification === JobName::SUBSTITUTE || $classification === JobName::INSTITUTION) {
        // array data global pour cr5
        $dataCR5 = [];
        // data concernant les notes pour cr5
        $dataCR5['table'] = [];
        // data concernant les notes pour cr5
        $dataCR5['table']['offer'] = [];
        if ($offer->getState() === Offer::STATE_ALMOST_FILL) {
            $substitution = $offer->getCanvas()->getSubstitutionType()->getSubstitutionType();
            $substitution = SubstitutionType::LABEL[$substitution];
            $dataCR5['table'] += [
                'firstname' => $person->getFirstName(),
                'lastname' => $person->getLastName()
            ];

            $dataCR5['table']['offer'] += [
                'label' => $substitution
            ];
            $dataCR5['table']['offer'] += [
                'created_at' => $offer->getStartingDate()->format('Y-m-d')
            ];
            $dataCR5['table']['offer'] += [
                'starting_at' => $offer->getStartingDate()->format('Y-m-d')
            ];

            //  discussions trouvées
            $dataCR5 += $this->arrayDataDiscussions;
            $this->dataCR5[] = $dataCR5;
        }
        // }
    }

    /**
     * Commercial
     * @param PersonNatural $person
     * @param Offer $offer
     * @param Discussion|null $lastDiscussion
     */

    public function getArrayCommercialFollowedContract(Person $person, Offer $offer, ?Discussion $lastDiscussion): void
    {
        $substitution = $offer->getCanvas()->getSubstitutionType()->getSubstitutionType();

        $dataCommercial = [];
        // data concernant les notes pour commercial
        $dataCommercial['table'] = [];
        $dataCommercial['bubble'] = [];

        // réussite sans accord
        if ($substitution === Offer::TYPE_SUCCESS_WITHOUT_AGREEMENT) {
            $LabelSubstitution = Offer::TYPES_LABEL[$substitution];
            // rappel à J+3
            $reminderDay = new DateTime();
            $reminderDay = $reminderDay->modify('+3 day')->format('d-m-Y');
            $reminder = 'Non';
            if ($lastDiscussion) {
                $dateLastDiscussions = $lastDiscussion->getCreationDate()->format('d-m-Y');
                if ($dateLastDiscussions <= $reminderDay) {
                    $reminder = 'Oui';
                }
            }

            $dataCommercial['table'] += [
                'firstname' => $person->getFirstName(),
                'lastname' => $person->getLastName(),
                'reminder' => $reminder,
                'label' => $LabelSubstitution,
            ];

            $dataCommercial['bubble'] += [
                'person_id' => $person->getId()
            ];

            $this->dataCommercial[] = $dataCommercial;
        }
    }

    /**
     * @return array
     */
    public function getDataCR4(): array
    {
        return $this->dataCR4;
    }

    /**
     * @return array
     */
    public function getDataCR3(): array
    {
        return $this->dataCR3;
    }

    /**
     * @return array
     */
    public function getDataCR5(): array
    {
        return $this->dataCR5;
    }

    /**
     * @return array
     */
    public function getCommercial(): array
    {
        return $this->dataCommercial;
    }

}