<?php


namespace App\Utils;


class Pagination
{

    /**
     * @var $page int
     * Number of the current page
     */
    public $page;

    /**
     * @var $nbPages int
     * Number of all pages
     */
    public $nbPages;

    /**
     * @var $routeName string
     * Route name to get the other pages
     */
    public $routeName;

    /**
     * @var $routeParams array
     * Route param for route name
     */
    public $routeParams;

    /**
     * Pagination constructor.
     * @param int $page
     * @param int $nbPages
     * @param string $routeName
     * @param array $routeParams
     */
    public function __construct(int $page, int $nbPages, string $routeName, array $routeParams)
    {
        $this->page = $page;
        $this->nbPages = $nbPages;
        $this->routeName = $routeName;
        $this->routeParams = $routeParams;
    }
}