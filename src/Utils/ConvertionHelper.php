<?php


namespace App\Utils;


class ConvertionHelper
{

    /**
     * @param string $string
     * @return string
     */
    public static function strToNoAccent(string $string)
    {

        $string = str_replace("-", " ", $string);
        $string = str_replace("'", " ", $string);

        $string = str_replace("é", "e", $string);
        $string = str_replace("è", "e", $string);
        $string = str_replace("ê", "e", $string);
        $string = str_replace("â", "a", $string);
        $string = str_replace("î", "i", $string);
        $string = str_replace("ô", "o", $string);
        $string = str_replace("û", "u", $string);
        $string = str_replace("ï", "i", $string);
        $string = str_replace("ë", "e", $string);
        $string = str_replace("ç", "c", $string);

        $string = str_replace("É", "E", $string);
        $string = str_replace("È", "E", $string);
        $string = str_replace("Ê", "E", $string);
        $string = str_replace("Â", "A", $string);
        $string = str_replace("Î", "I", $string);
        $string = str_replace("Ô", "O", $string);
        $string = str_replace("Û", "U", $string);
        $string = str_replace("Ï", "I", $string);
        $string = str_replace("Ë", "E", $string);
        $string = str_replace("Ç", "C", $string);

        $string = str_replace("¤", " ", $string);
        $string = trim($string);
        return ($string);
    }

    /**
     * @param $string
     * @return bool|false|string|string[]|null
     */
    public static function stringToTitleFormat($string)
    {
        return mb_convert_case($string, MB_CASE_TITLE);
    }

    /**
     * @param int $timestamp
     * @return DateTime
     */
    public static function timestampToDateTime(int $timestamp)
    {
        $date = new DateTime();
        return $date->setTimestamp($timestamp);
    }

    /**
     * @param int $timestampMilliseconds
     * @return int
     */
    public static function timestampMillisecondsToSeconds(int $timestampMilliseconds)
    {
        return (int)substr((string)$timestampMilliseconds, 0, 10);
    }
}