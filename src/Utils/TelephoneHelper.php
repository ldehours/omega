<?php


namespace App\Utils;


use App\Entity\Core\Telephone;

class TelephoneHelper
{

    /**
     * @param Telephone $telephone
     * @return string|null
     */
    public static function internationalizeTelephoneNumber(Telephone $telephone): ?string
    {
        $number = $telephone->getNumber();
        if (preg_match('/^0[1-9]\d+$/', $number) === 1) {
            $number = '0033' . substr($number, 1);
        }

        return $number;
    }

    /**
     * @param $number
     * @return string
     */
    public static function formatPhoneNumber($number): string
    {
        $number = str_pad($number, 10, "0", STR_PAD_RIGHT);
        $number = str_replace(' ', '', $number);
        $result = "";
        for ($i = 0; $i < strlen($number); $i = $i + 2) {
            $space = '';
            if ($i !== 0) {
                $space = ' ';
            }
            $result .= $space . substr($number, $i, 2);
        }
        return $result;
    }
}