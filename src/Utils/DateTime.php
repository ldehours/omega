<?php


namespace App\Utils;

use DateTimeZone;

/**
 * Classe DateTime pour appeler automatiquement le timezone approprié avec notre fuseau horaire
 * @package App\Utils
 */
class DateTime extends \DateTime
{
    /**
     * DateTime constructor.
     * @param string $time
     * @param DateTimeZone|null $timezone
     * @throws \Exception
     */
    public function __construct($time = 'now', DateTimeZone $timezone = null)
    {
        if ($timezone == null) {
            $timezone = new DateTimeZone("Europe/Paris");
        }
        parent::__construct($time, $timezone);
    }

    /**
     * Prend une string et la convertit en App\Utils\DateTime si le format de date est standard
     * @param string $date
     * @return \App\Utils\DateTime|null
     */
    public static function createFromString(string $date): ?DateTime
    {
        if ($date == "0000-00-00") {
            return null;
        }

        $formats = array('Y-m-d', 'Y-m-d H:i:s', 'd/m/Y', 'd/m/Y H:i:s', 'd/m/Y H:i');
        foreach ($formats as $format) {
            $datetime = self::createFromFormat($format, $date);
            if ($datetime !== false) {
                return PHPHelper::convertObjectClass($datetime, DateTime::class);
            }
        }
        return null;
    }

    /**
     * Retourne la date au format anglais Y-m-d
     * @return string
     */
    public function getEnglishFormat(): string
    {
        return $this->format('Y-m-d');
    }

    /**
     * Retourne la date au format français d/m/Y
     * @return string
     */
    public function getFrenchFormat(): string
    {
        return $this->format('d/m/Y');
    }
}