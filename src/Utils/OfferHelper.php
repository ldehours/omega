<?php

namespace App\Utils;

use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Offer\Offer;

class OfferHelper
{
    /**
     * @param Person $person
     * @param Staff $staff
     * @return Offer
     * @throws \Exception
     */
    public static function createDefaultOffer(Person $person, Staff $staff): Offer
    {
        $offer = new Offer();
        $offer->setCreationDate(new \DateTimeImmutable());
        $offer->setCreator($staff);
        $offer->setPerson($person);
        if ($person instanceof PersonNatural && $person->getSpecialty() !== null) {
            $offer->setSpecialty($person->getSpecialty());
        }
        $personDefaultAddress = $person->getDefaultAddress();
        if ($personDefaultAddress !== null) {
            $offer->setDepartment($personDefaultAddress->getDepartment());
        }
        return $offer;
    }
}