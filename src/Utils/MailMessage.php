<?php


namespace App\Utils;

use App\Entity\Core\Discussion;
use App\Entity\Core\JobName;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Telephone;
use App\Entity\Offer\Offer;
use App\Repository\Core\DiscussionRepository;
use App\Repository\Mail\PreWrittenMailRepository;
use Doctrine\Persistence\ObjectManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Tipimail\Exceptions\TipimailException;

class MailMessage
{//TODO : créer un / des templates pour les constructions de mail et utiliser un renderView afin d'enlever le maximum de html dans les classes
    /**
     * @param string $email
     * @param string $password
     * @param bool $isRecruitmentConsultant
     * @param string $telephoneRecruitmentConsultant
     * @return string
     */
    public static function getCUAM(
        string $email,
        string $password,
        bool $isRecruitmentConsultant = false,
        string $telephoneRecruitmentConsultant = ''
    ) {
        $message = 'Suite à notre entretien téléphonique concernant votre recherche de remplacements, je vous confirme que votre compte personnel a été créé sur le site de Média-Santé.<br>
                Pour y accéder, cliquez <a href="https://media-sante.com">ici</a>, rendez-vous ensuite dans la rubrique "Mon compte" puis "Connexion". 
                <br>
                <br>
                <strong>Vos codes d\'accès sont</strong> :<br>
                Email : ' . $email . '<br>
                Mot de Passe : ' . $password . '<br>
                <br>
                Une fois identifié, toutes les informations des offres de remplacements libérales, postes salariés, associations de médecins et cessions de cabinets vous seront accessibles. Vous n’avez plus qu’à faire votre choix et à planifier votre agenda professionnel !<br>
                <br>
                Vous ne trouvez pas le remplacement qu’il vous faut ? ';

        if ($isRecruitmentConsultant) {
            $message .= 'Appelez directement la CR du secteur au ' . $telephoneRecruitmentConsultant . ' qui contactera ';
        } else {
            $message .= 'Appelez-moi directement et je contacterai ';
        }
        $message .= 'pour vous d’autres confrères dont le secteur géographique et les critères proposés habituellement seront susceptibles de vous convenir.';

        return $message;
    }

    /**
     * @return string
     */
    public static function getThanks()
    {
        return 'Je reste bien évidemment à votre disposition pour tout complément d\'information.<br>Bien cordialement,';
    }

    /**
     * @param Person $person
     * @return string
     */
    public static function getGreeting(Person $person)
    {
        return ($person instanceof PersonNatural) ? "Docteur <span class='mail-content-greetings-name'>" . $person->getFirstName(
            ) . ' ' . $person->getLastName() . '</span>,' : 'Bonjour,';
    }

    /**
     * @param Offer[] $offers
     * @return string
     */
    public static function getOffers($offers)
    {
        $criteria = "";
        $message = 'Pourriez-vous me tenir informée si vous convenez avec ce médecin.';
        if (count($offers) > 1) {
            $message = 'Pourriez-vous me tenir informée si vous convenez avec un des médecins.';
        }
        foreach ($offers as $offer) {
            $criteria .= "<br><p><strong>Réf : " . $offer->getId() . "</strong></p>";
            $criteria .= "<div class=\"mail-content-alterable\" title=\"Modifier le texte\">";
            foreach ($offer->getSubstitutionCriteria() as $substitutionCriterion) {
                $value = $substitutionCriterion->getValue();
                if (is_array($substitutionCriterion->getValue())) {
                    $value = json_encode($substitutionCriterion->getValue());
                }
                $criteria .= $substitutionCriterion->getCriterion()->getLabel() . ' : ' . $value . '<br>';
            }
            $criteria .= "</div>";
            if ($offer->getPerson() instanceof PersonNatural) {
                $criteria .= self::getContactPersonNatural($offer->getPerson());
            } elseif ($offer->getPerson() instanceof PersonLegal) {
                $criteria .= self::getContactPersonLegal($offer->getPerson());
            }
        }

        return "<div id=\"mail-content-offers\"><p class='mail-content-criteria'>" . $criteria . "</p><br>
                <p class=\"mail-content-alterable\" title=\"Modifier le texte\">" . $message . "</p>
                <p class=\"mail-content-alterable\" title=\"Modifier le texte\">Cela me permettra de retirer l'annonce de notre site et de vous garantir des offres toujours disponibles et actualisées.</p></div>";
    }

    /**
     * @param PersonNatural $personNatural
     * @return string
     */
    public static function getContactPersonNatural(PersonNatural $personNatural)
    {
        $address = '';
        if (count($personNatural->getAddresses()) > 0) {
            $fullAddress = $personNatural->getAddresses()->first();
            $address = ' - ' . $fullAddress->getAddressLine1() . ' - ' . $fullAddress->getPostalCode() . ' ' . $fullAddress->getLocality();
        }

        $telephones = '';
        if (count($workTelephones = $personNatural->getTelephonesByType(Telephone::WORK)) > 0) {
            $telephones .= 'Cabinet : ' . $workTelephones->first();
        }
        if (count($cellTelephones = $personNatural->getTelephonesByType(Telephone::CELL)) > 0) {
            $telephones .= 'Portable : ' . $cellTelephones->first();
        }

        $email = '';
        if ($personNatural->getDefaultMail() != null) {
            $email = '<br>mail : ' . $personNatural->getDefaultMail()->getAddress();
        }

        return '<p>Dr.' . strtoupper($personNatural->getLastName()) . ' ' . ucwords($personNatural->getFirstName()) . $address . $email . '<br>
        ' . $telephones . '</p>';
    }

    /**
     * @param PersonLegal $personLegal
     * @return string
     */
    public static function getContactPersonLegal(PersonLegal $personLegal)
    {
        $address = '';
        if (count($personLegal->getAddresses()) > 0) {
            $fullAddress = $personLegal->getAddresses()->first();
            $address = ' - ' . $fullAddress->getAddressLine1() . ' - ' . $fullAddress->getPostalCode() . ' ' . $fullAddress->getLocality();
        }

        $telephones = '';
        if (count($workTelephones = $personLegal->getTelephonesByType(Telephone::WORK)) > 0) {
            $telephones .= 'Cabinet : ' . $workTelephones->first();
        }
        if (count($cellTelephones = $personLegal->getTelephonesByType(Telephone::CELL)) > 0) {
            $telephones .= 'Portable : ' . $cellTelephones->first();
        }

        $email = '';
        if ($personLegal->getDefaultMail() !== null) {
            $email = '<br>mail : ' . $personLegal->getDefaultMail()->getAddress();
        }

        return '<p>' . JobName::LABEL[$personLegal->getMainJobName()->getClassification()] . ' ' . $personLegal->getMainJobName()->getLabel() .
            ' : ' . strtoupper($personLegal->getCorporateName()) . ' ' . $address . $email . '<br>' . $telephones . '</p>';
    }

    /**
     * @param PaginatorInterface $paginator
     * @param $page
     * @param null $staff
     * @param DiscussionRepository $discussionRepository
     * @param null $person
     * @return PaginationInterface
     */
    public static function showListMails(
        DiscussionRepository $discussionRepository,
        PaginatorInterface $paginator,
        $page,
        $staff = null,
        $person = null
    ) {
        $queryBuilder = $discussionRepository->getListDiscussion(Discussion::MAIL_SENT, $staff,
            $person);
        return $paginator->paginate($queryBuilder, $page, 4);
    }

    /**
     * @param DiscussionRepository $discussionRepository
     * @param ObjectManager $entityManager
     * @param Person|PersonNatural|PersonLegal $person
     * @param $staff
     * @param PreWrittenMailRepository $preWrittenMailRepository
     * @param PaginatorInterface $paginator
     * @return array
     * @throws TipimailException
     */
    public static function mailExchanges(
        DiscussionRepository $discussionRepository,
        ObjectManager $entityManager,
        $person,
        $staff,
        PreWrittenMailRepository $preWrittenMailRepository,
        PaginatorInterface $paginator
    ) {
        $userMail = $staff->getEmail();
        $clientMail = $person->getDefaultMail() == null ? "nomail" : $person->getDefaultMail()->getAddress();

        $inBox = [];//En attente pour l'instant
        APITipimailHelper::getMailSent($discussionRepository, $staff, [$userMail], $entityManager, $person);
        $pagination = self::showListMails($discussionRepository, $paginator, 1/*page par défaut*/, $staff, $person);
        //sentBox vide car gére par PAGINATOR
        $mailBox = ['inBox' => $inBox, 'sentBox' => []];

        return [
            'person' => $person,
            'clientMail' => $clientMail,
            'staff' => $staff,
            'mailBoxes' => $mailBox,
            'pagination' => $pagination,
            'preWrittenMailList' => $preWrittenMailRepository->findAll()
        ];
    }
}