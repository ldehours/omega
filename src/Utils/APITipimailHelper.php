<?php


namespace App\Utils;

use App\Entity\Core\Discussion;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Repository\Core\DiscussionRepository;
use App\Repository\Core\PersonLegalRepository;
use App\Repository\Core\PersonNaturalRepository;
use Symfony\Component\Filesystem\Filesystem;
use Tipimail\Messages\Message;
use Tipimail\Tipimail;


class APITipimailHelper
{
    /**
     * @return mixed
     */
    public static function getTipimailAPIPassword()
    {
        return $_ENV['TIPIMAIL_API_PASSWORD'];
    }

    /**
     * @return mixed
     */
    public static function getTipimailAPIUserName()
    {
        return $_ENV['TIPIMAIL_API_USERNAME'];
    }

    public static function getAttachmentsDir()
    {
        return $_ENV['ATTACHMENT_DIR'];
    }

    /**
     * @return Tipimail
     */
    public static function getTipimailInstance()
    {
        return new Tipimail(self::getTipimailAPIUserName(), self::getTipimailAPIPassword());
    }

    /**
     * @param $mailId
     * @return \Tipimail\StatisticsMessageDetail
     * @throws \Tipimail\Exceptions\TipimailException
     */
    public static function getMailDetailFromTipimail($mailId)
    {
        return self::getTipimailInstance()->getStatisticsService()->getMessageDetail($mailId);
    }

    /**
     * @param DiscussionRepository $discussionRepository
     * @param $staff
     * @param array $senders
     * @param $entityManager
     * @param Person|PersonNatural|PersonLegal $person
     * @throws \Tipimail\Exceptions\TipimailException
     */
    public static function getMailSent(
        DiscussionRepository $discussionRepository,
        $staff,
        array $senders,
        $entityManager,
        $person = null
    ): void
    {
        $tipimail = self::getTipimailInstance();
        $now = (int)(string)round(microtime(true) * 1000);

        //PARAMETRES DES MAILS A RECUPERER DE TIPIMAIL
        $page = null;
        $pageSize = null;
        $dateBegin = $now - (90 * 24 * 60 * 60 * 1000);//timestamp en millisecondes de 3 mois avant
        $dateEnd = $now;
        $froms = $senders;
        $apiPasswords = [APITipimailHelper::getTipimailAPIPassword()];
        $tags = null;
        $results = $tipimail->getStatisticsService()->getMessages($page, $pageSize, null, $dateEnd, $froms, $tags,
            $apiPasswords);
        $mails = [];
        if ($person != null) {
            $personMail = $person->getDefaultMail()->getAddress();
            foreach ($results->getMessages() as $message) {
                if ($tipimail->getStatisticsService()->getMessageDetail($message->getId())->getMsg()->getEmail() == $personMail) {
                    $mails[] = $message;
                }
            }
        } else {
            $mails = $results->getMessages();
        }

        if ($mails != null) {
            krsort($mails);

            //RECUPERATION DE TOUS LES MAILS ENVOYES PAR LE STAFF
            foreach ($mails as $message) {

                $mailId = $message->getId();
                $mailSubject = $tipimail->getStatisticsService()->getMessageDetail($message->getId())->getMsg()->getSubject();
                $discussion = $discussionRepository->findOneBy([
                    'tag' => $message->getId()
                ]);

                if ($discussion == null) {
                    $discussion = new Discussion();
                    $mailContent = "Mail envoyé avant l'implémentation de la sauvegarde! ";
                    $discussion->setMemo($mailContent);
                    $discussion->setStatus(self::convertStatus($message->getLastState()));
                    $discussion->setSubject($mailSubject);
                    $discussion->setCreationDate(ConvertionHelper::timestampToDateTime(ConvertionHelper::timestampMillisecondsToSeconds($message->getCreatedDate())));
                    $discussion->setCreator($staff);
                    $discussion->setPerson($person);
                    $discussion->setService($staff->getService());
                    $discussion->setTag($mailId);
                    $discussion->setType(Discussion::MAIL_SENT);
                    $discussion->setStatusDate(ConvertionHelper::timestampToDateTime(ConvertionHelper::timestampMillisecondsToSeconds($message->getLastStateDate())));
                    $entityManager->persist($discussion);
                    $entityManager->flush();

                } else {
                    if ($discussion->getStatus() != self::convertStatus($message->getLastState())) {
                        $discussion->setStatus(self::convertStatus($message->getLastState()));
                        $discussion->setStatusDate(ConvertionHelper::timestampToDateTime(ConvertionHelper::timestampMillisecondsToSeconds($message->getLastStateDate())));
                        $entityManager->persist($discussion);
                        $entityManager->flush();
                    }
                }
            }
        }
    }

    /**
     * @param $user
     * @param $recepientMail
     * @param $recepientId
     * @param $subject
     * @param $messageMail
     * @param PersonLegalRepository|PersonNaturalRepository $personRepository
     * @param $entityManager
     * @param $allAttachments
     * @return array
     * @throws \Tipimail\Exceptions\TipimailException
     */
    public static function sendMailWithAPI(
        $user,
        $recepientMail,
        $recepientId,
        $subject,
        $messageMail,
        $personRepository,
        $entityManager,
        $allAttachments = []
    )
    {
        //ENVOI MAIL VIA TIPTIMAIL
        $userName = $user->getLastName() . " " . $user->getFirstName();
        $userMail = $user->getEmail();
        $service = $user->getService();
        $tipimail = APITipimailHelper::getTipimailInstance();
        $data = new Message();
//        $data->addTo($recepientMail, 'to');
        //for testing
        $data->addTo($userMail, 'to');
        $data->setFrom($userMail, $userName);
        $data->setSubject($subject);
        $data->setReplyTo($userMail, $userName);

        if ($allAttachments != [] && $allAttachments['exist']) {
            $allAttachmentsType = $allAttachments['allAttachmentsType'];
            $allAttachmentsTmp_names = $allAttachments['allAttachmentsTmp_names'];
            $allAttachmentsNames = $allAttachments['allAttachmentsNames'];

            foreach ($allAttachmentsType as $index => $attachmentName) {
                if (strpos($allAttachmentsType[$index], "image/")) {
                    $data->addImageFromFile($allAttachmentsTmp_names[$index], $allAttachmentsNames[$index],
                        $allAttachmentsTmp_names[$index] . time());
                } else {
                    $data->addAttachmentFromFile($allAttachmentsTmp_names[$index], $allAttachmentsNames[$index]);
                }
            }
        }
        $data->setHtml($messageMail);
        $data->setApiKey(self::getTipimailAPIPassword());
        $data->enableTrackingOpen();
        $data->enableTrackingClick();

        //ENVOI MAIL VIA TIPTIMAIL
        //date avant envoi du mail en millisecondes sans les chiffres après la virgule
        $dateBegin = (int)substr(microtime(true) * 10000, 0, 13);
        $tipimail->getMessagesService()->send($data);

        //$dateBegin et $dateEnd : interval de temps comprenant la date de création du mail par Tipimail pour ensuite pourvoir récupérer les informations
        //concernant le mail qui vient d'être envoyé et ainsi pouvoir faire correspondre exactement ce qui sera dans la base de données de OMEGA et de Tipimail
        $messageSentInfo = self::getSendingStatusAndMessage($userMail, $dateBegin);
        $sendingMailStatus = $messageSentInfo['sendingStatus'];
        //SAUVEGARDE DU MAIL ENVOYE
        if ($sendingMailStatus) {
            $messageSent = $messageSentInfo['messageSent']->getMessages()[0];
            $mailId = $messageSent->getId();
            $discussion = new Discussion();
            $discussion->setType(2);
            $discussion->setSubject($subject);
            $discussion->setMemo($messageMail);
            $discussion->setCreator($user);
            $discussion->setService($service);
            if ($recepientId != null) {
                $person = $personRepository->findOneBy(['id' => $recepientId]);
                if ($person != null) {
                    $discussion->setPerson($person);
                }
            }

            //le format de date venant de Tipimail est en millisecondes (13 chiffres) et le timestamp de la BD est en secondes (10 chiffres)
            $discussion->setCreationDate((ConvertionHelper::timestampToDateTime(ConvertionHelper::timestampMillisecondsToSeconds($messageSent->getCreatedDate()))));
            $discussion->setStatus(APITipimailHelper::convertStatus($messageSent->getLastState()));
            $discussion->setStatusDate((ConvertionHelper::timestampToDateTime(ConvertionHelper::timestampMillisecondsToSeconds($messageSent->getLastStateDate()))));
            $discussion->setTag($mailId);
            $entityManager->persist($discussion);
            $entityManager->flush();

            self::savingAttachments($allAttachments, $mailId);
        }
        $message = self::confirmMessageSent($recepientMail, $sendingMailStatus);

        return [
            'deliveryStatus' => $messageSentInfo['sendingStatus'],
            'message' => $message
        ];
    }

    /**
     * @param $userMail
     * @param $dateBegin
     * @return array
     * @throws \Tipimail\Exceptions\TipimailException
     */
    public static function getSendingStatusAndMessage(
        $userMail,
        $dateBegin
    )
    {
        sleep(5);//sleep(5) ici permet de s'assurer que les détails du mail envoyé sont disponibles après son envoi pour pouvoir ensuite les sauvergarder dans OMEGA
        $messagesSent = self::getTipimailInstance()->getStatisticsService()->getMessages(null, null, $dateBegin,
            null,
            [$userMail], null, [self::getTipimailAPIPassword()]);
        return [
            'sendingStatus' => count($messagesSent->getMessages()) >= 1,
            'messageSent' => $messagesSent
        ];
    }

    /**
     * @param string $recepientMail
     * @param $sendingMailStatus
     * @return string
     */
    public static function confirmMessageSent(
        string $recepientMail,
        $sendingMailStatus
    )
    {
        if ($sendingMailStatus) {
            $message = 'Le mail a bien été envoyé à l\'adresse ' . $recepientMail;
        } else {
            $message = 'Le mail n\'a pas été envoyé';
        }
        return $message;
    }

    /**
     * @param $allAttachments
     * @param $mailId
     */
    public static function savingAttachments(
        $allAttachments,
        $mailId
    )
    {
        $dir = self::getAttachmentsDir() . 'sent/' . $mailId . "/";
        $fileSystem = new Filesystem();
        $fileSystem->mkdir($dir);
        if ($allAttachments['exist']) {
            //SAUVEGARE DES PIECES JOINTES
            foreach ($allAttachments['allAttachmentsNames'] as $index => $attachmentName) {
                if (!file_exists($dir . $attachmentName)) {
                    move_uploaded_file($allAttachments['allAttachmentsTmp_names'][$index], $dir . $attachmentName);
                }
            }
        }
    }

    /**
     * @param string $status
     * @return int|null
     */
    public static function convertStatus(
        string $status
    )
    {
        switch ($status) {
            case "delivered":
                return 0;
            case "opened":
                return 1;
            case "clicked":
                return 2;
            case "rejected":
                return 3;
            case "hardbounced":
                return 4;
            case "filtered":
                return 7;
            default:
                return 0;
        }
    }
}