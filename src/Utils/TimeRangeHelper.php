<?php


namespace App\Utils;


use App\Entity\Core\TimeRange;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TimeRangeHelper
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param TimeRange $timeRange
     * @return array
     */
    public function getEventDataFromTimeRange(TimeRange $timeRange): array
    {
        return [
            'id' => $timeRange->getId(),
            'backgroundColor' => TimeRange::NATURES_COLOR[$timeRange->getNature()],
            'borderColor' => TimeRange::NATURES_COLOR[$timeRange->getNature()],
            'textColor' => TimeRange::NATURES_COLOR_TEXT[$timeRange->getNature()],
            'url' => $this->router->generate('agenda_view', [
                'id' => $timeRange->getId(),
            ])
        ];
    }
}