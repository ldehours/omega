<?php


namespace App\Utils;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class PHPHelper
{
    public const ASC = 1;
    public const DESC = 2;

    /**
     * @param $value1
     * @param $value2
     * @param string $operator
     * @return bool
     */
    public static function condition($value1, string $operator, $value2): bool
    {
        switch ($operator) {
            case '<':
                return $value1 < $value2;
            case '<=':
                return $value1 <= $value2;
            case '>':
                return $value1 > $value2;
            case '>=':
                return $value1 >= $value2;
            case '==':
                return $value1 == $value2;
            case '===':
                return $value1 === $value2;
            case '!=':
                return $value1 != $value2;
            case '!==':
                return $value1 !== $value2;
            default:
                return false;
        }
    }

    /**
     * @param $object
     * @param $targetClassName
     * @return mixed
     */
    public static function convertObjectClass($object, $targetClassName)
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($targetClassName),
            $targetClassName,
            strstr(strstr(serialize($object), '"'), ':')
        ));
    }

    /**
     * @param $string
     * @return string
     */
    public static function strToUpperNoAccent($string)
    {
        $string = mb_convert_case($string,MB_CASE_UPPER);
        $search = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý'];
        $replace = ['A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y'];
        return str_replace($search, $replace, strtoupper($string));
    }

    /**
     * @param array $haystack
     * @param int $type
     * @return array
     */
    public static function sortArrayFromValue(array $haystack, int $type = self::ASC):array
    {
        switch($type){
            case self::DESC:
                arsort($haystack);
                break;
            default:
                asort($haystack);
                break;
        }
        return $haystack;
    }

    /**
     * @param array $haystack
     * @param int $type
     * @return array
     */
    public static function sortArrayFromKey(array $haystack, int $type = self::ASC):array
    {
        switch($type){
            case self::DESC:
                krsort($haystack);
                break;
            default:
                ksort($haystack);
                break;
        }
        return $haystack;
    }

    /**
     * @param ArrayCollection $haystack
     * @param string $getMethod
     * @param int $type
     * @return ArrayCollection
     */
    public static function sortEntitiesFromMethod(ArrayCollection $haystack, string $getMethod, int $type = self::DESC):ArrayCollection
    {
        if(!empty($haystack) && count($haystack) > 1){
            $numberElements = count($haystack);
            $operator = ($type === self::DESC ? '>' : '<');
            for($i = 0; $i < $numberElements - 1; $i++){
                for($j = 0; $j < ($numberElements - 1 - $i); $j++){
                    $elementValue = $haystack[$j]->$getMethod();
                    $nextElementValue = $haystack[$j+1]->$getMethod();
                    if(!(is_string($elementValue) || is_numeric($elementValue))
                        || !(is_numeric($nextElementValue) || is_string($nextElementValue))){
                        continue;
                    }
                    if (self::condition($elementValue,$operator,$nextElementValue)) {
                        $temp = $haystack[$j+1];
                        $haystack[$j+1] = $haystack[$j];
                        $haystack[$j] = $temp;
                    }
                }
            }
        }
        return $haystack;
    }

    /**
     * Reproduction of array_filter but for objects collection
     * @param Collection $collection
     * @param callable $callback
     * @return ArrayCollection
     */
    public static function collectionFilter(Collection $collection, callable $callback)
    {
        $result = new ArrayCollection();
        foreach ($collection as $item){
            if($callback($item)){
                $result[] = $item;
            }
        }
        return $result;
    }

    /**
     * Reproduction of array_map but for objects collection
     * @param Collection $collection
     * @param callable $callback
     * @return ArrayCollection
     */
    public static function collectionMap(Collection $collection, callable $callback)
    {
        $result = new ArrayCollection();
        foreach ($collection as $item){
            $result[] = $callback($item);
        }
        return $result;
    }
}