<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class ArrayExtension extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('all_in_array', [$this, 'allInArray']),
            new TwigFunction('in_array', [$this, 'inArray']),
            new TwigFunction('is_array', [$this, 'isArray']),
            new TwigFunction('array_to_json', [$this, 'arrayToJson']),
        ];
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('is_array', [$this, 'isArray']),
        ];
    }

    /**
     * @param $array1
     * @param $array2
     * @return bool
     */
    public function allInArray($array1, $array2)
    {
        if ($array1 === null || $array2 === null) {
            return false;
        }

        if (!is_array($array1)) {
            $array1 = [$array1];
        }
        if (!is_array($array2)) {
            $array2 = [$array2];
        }

        foreach ($array1 as $value) {
            if (!in_array($value, $array2)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $haystack
     * @return bool
     */
    public function isArray($haystack):bool
    {
        return is_array($haystack);
    }

    /**
     * @param $array1
     * @param $array2
     * @return bool
     */
    public function inArray($array1, $array2)
    {
        if ($array1 === null || $array2 === null) {
            return false;
        }

        if (!is_array($array1)) {
            $array1 = [$array1];
        }
        if (!is_array($array2)) {
            $array2 = [$array2];
        }

        foreach ($array1 as $value) {
            if (in_array($value, $array2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $val
     * @return false|string
     */
    public function arrayToJson($val)
    {
        return json_encode($val);
    }
}