<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TestInstance extends AbstractExtension
{
    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('instanceof', [$this, 'instanceOf']),
        ];
    }

    /**
     * @param $var
     * @param $instance
     * @return bool
     */
    public function instanceOf($var, $instance)
    { //usage in Twig : {% if object|instanceof('Class') %}
        $longClassName = get_class($var);
        $tabClassName = explode("\\", $longClassName);
        $className = $tabClassName[count($tabClassName) - 1];
        if ($className == $instance) {
            return true;
        }
        return false;
    }
}