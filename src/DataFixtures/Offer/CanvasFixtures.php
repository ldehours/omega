<?php

namespace App\DataFixtures\Offer;

use App\DataFixtures\BaseFixture;
use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\SubstitutionType;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class CanvasFixtures extends BaseFixture implements FixtureGroupInterface
{

    protected function loadData(ObjectManager $manager)
    {
        $canvasDatas = [
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_PUNCTUAL => [
                'label' => 'Souhait libéral ponctuel nantu (104)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["2","4"],"1":["1","4"],"2":["22","4"]},{"0":["8","4"],"1":["25","4"],"2":["3","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["23","4"],"1":["5","4"],"2":["32","4"]},{"0":["6","4"],"1":["24","4"],"2":["7","4"]},{"0":["39","4"],"1":["15","4"]}]'
            ],
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_REGULAR => [
                'label' => 'Souhait libéral régulier nantu (105)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["61","4"],"1":["3","4"]},{"0":["2","4"],"1":["1","4"],"2":["22","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["23","4"],"1":["5","4"],"2":["32","4"]},{"0":["6","4"],"1":["24","4"],"2":["7","4"]},{"0":["39","4"],"1":["15","4"]}]'
            ],
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_ON_CALL => [
                'label' => 'Souhait libéral garde nantu (106)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["25","4"],"1":["8","4"],"2":["22","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["2","3"],"1":["7","3"],"2":["1","3"],"3":["6","3"]}]'
            ],
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_LONG_TIME => [
                'label' => 'Souhait libéral longue durée nantu (107)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["1","4"],"1":["2","4"],"2":["22","4"]},{"0":["25","4"],"1":["8","4"],"2":["3","4"]},{"0":["23","4"],"1":["5","4"],"2":["32","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["6","4"],"1":["24","4"],"2":["7","4"]},{"0":["39","4"],"1":["15","4"]}]'
            ],
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_PARTNERSHIP => [
                'label' => 'Souhait libéral collaboration nantu (108)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["1","4"],"1":["2","4"],"2":["22","4"]},{"0":["8","4"],"1":["25","4"],"2":["3","4"]},{"0":["23","4"],"1":["5","4"],"2":["32","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["6","4"],"1":["24","4"],"2":["7","4"]},{"0":["39","4"],"1":["15","4"]}]'
            ],
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_TEMPORARY_PARTNERSHIP => [
                'label' => 'Souhait libéral association temporaire nantu (109)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["1","4"],"1":["2","4"],"2":["22","4"]},{"0":["25","4"],"1":["8","4"],"2":["3","4"]},{"0":["23","4"],"1":["5","4"],"2":["32","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["6","4"],"1":["24","4"],"2":["7","4"]},{"0":["39","4"],"1":["15","4"]}]'
            ],
            SubstitutionType::TYPE_LIBERAL_REPLACEMENT_DECEASED_REPLACEMENT => [
                'label' => 'Souhait libéral tenue de poste nantu (110)',
                'formDisposition' => '[{"0":["9","4"],"1":["18","4"],"2":["4","4"]},{"0":["12","3"],"1":["13","3"],"2":["10","3"],"3":["11","3"]},{"0":["1","4"],"1":["2","4"],"2":["22","4"]},{"0":["25","4"],"1":["8","4"],"2":["3","4"]},{"0":["23","4"],"1":["5","4"],"2":["32","4"]},{"0":["16","4"],"1":["17","4"],"2":["14","4"]},{"0":["6","4"],"1":["24","4"],"2":["7","4"]},{"0":["39","4"],"1":["15","4"]}]'
            ],
            SubstitutionType::TYPE_SALARIED_POSITION_FIXED_TERM_CONTRACT => [
                'label' => 'Souhait salariat CDD nantu (111)',
                'formDisposition' => '[{"0":["18","4"],"1":["30","4"],"2":["31","4"]},{"0":["26","4"],"1":["27","4"],"2":["28","4"]},{"0":["25","4"],"1":["8","4"],"2":["29","4"]},{"0":["33","4"],"1":["34","4"]},{"0":["32","4"],"1":["16","4"],"2":["17","4"]}]'
            ],
            SubstitutionType::TYPE_SALARIED_POSITION_PERMANENT_CONTRACT => [
                'label' => 'Souhait salariat CDI nantu (112)',
                'formDisposition' => '[{"0":["18","4"],"1":["30","4"],"2":["31","4"]},{"0":["26","4"],"1":["28","4"],"2":["27","4"]},{"0":["25","4"],"1":["8","4"],"2":["29","4"]},{"0":["16","4"],"1":["17","4"]},{"0":["32","4"],"1":["33","4"],"2":["34","4"]}]'
            ],
            SubstitutionType::TYPE_SALARIED_POSITION_SHIFT => [
                'label' => 'Souhait salariat vacations nantu (113)',
                'formDisposition' => '[{"0":["18","4"],"1":["30","4"],"2":["31","4"]},{"0":["26","4"],"1":["28","4"],"2":["27","4"]},{"0":["25","4"],"1":["8","4"],"2":["29","4"]},{"0":["16","4"],"1":["17","4"]},{"0":["32","4"],"1":["33","4"],"2":["34","4"]}]'
            ],
            SubstitutionType::TYPE_SELL_ASSOCIATION_SESSION => [
                'label' => 'Souhait cession de cabinet nantu (114)',
                'formDisposition' => '[{"0":["9","4"],"1":["10","4"],"2":["11","4"]},{"0":["22","4"],"1":["23","4"],"2":["7","4"]},{"0":["6","4"],"1":["62","4"],"2":["2","4"]}]'
            ],
            SubstitutionType::TYPE_SELL_ASSOCIATION_DOCTOR_PARTNERSHIP => [
                'label' => 'Souhait association de médecins nantu (115)',
                'formDisposition' => '[{"0":["9","4"],"1":["10","4"],"2":["11","4"]},{"0":["22","4"],"1":["23","4"],"2":["7","4"]},{"0":["6","4"],"1":["62","4"],"2":["2","4"]}]'
            ],
            SubstitutionType::TYPE_SELL_ASSOCIATION_LOCATION => [
                'label' => 'Souhait location de local professionnel nantu (116)',
                'formDisposition' => '[{"0":["9","4"],"1":["10","4"],"2":["11","4"]},{"0":["22","4"],"1":["23","4"],"2":["7","4"]},{"0":["6","4"],"1":["62","4"],"2":["2","4"]}]'
            ],
            SubstitutionType::TYPE_SELL_ASSOCIATION_SELLING => [
                'label' => 'Souhait vente local professionnel nantu (117)',
                'formDisposition' => '[{"0":["9","4"],"1":["10","4"],"2":["11","4"]},{"0":["22","4"],"1":["23","4"],"2":["7","4"]},{"0":["6","4"],"1":["62","4"],"2":["2","4"]}]'
            ],
        ];

        $canvas = new Canvas();
        /** @var SubstitutionType $substitutionType */
        $substitutionType = $manager->getRepository(SubstitutionType::class)
            ->findOneBy(['substitutionType' => SubstitutionType::TYPE_LIBERAL_REPLACEMENT_PUNCTUAL]);
        $canvas->setLabel('Offre libérale ponctuelle');
        $canvas->setSubstitutionType($substitutionType);
        $manager->persist($canvas);
        $this->addReference('liberal offer canvas', $canvas);

        foreach ($canvasDatas as $key => $data) {
            $canvas = new Canvas();
            $arrayDisposition = json_decode($data['formDisposition'], true);
            $canvas->setFormDisposition($arrayDisposition);
            foreach ($arrayDisposition as $row) {
                foreach ($row as $element) {
                    /** @var Criterion $criterion */
                    $criterion = $manager->getRepository(Criterion::class)->findOneBy(['id' => $element[0]]);
                    if ($criterion === null) {
                        continue;
                    }
                    $canvas->addCriterion($criterion);
                }
            }
            $canvas->setLabel($data['label']);
            $canvas->setCanvasType(Canvas::CANVAS_WISH);
            $substitutionType = $manager->getRepository(SubstitutionType::class)
                ->findOneBy(['substitutionType' => $key]);
            $canvas->setSubstitutionType($substitutionType);
            $manager->persist($canvas);
        }
        $manager->flush();
    }

    /**
     * @return array|string[]
     */
    public static function getGroups(): array
    {
        return [
            'canvas'
        ];
    }
}
