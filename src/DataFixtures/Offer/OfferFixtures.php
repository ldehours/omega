<?php

namespace App\DataFixtures\Offer;

use App\DataFixtures\BaseFixture;
use App\DataFixtures\Core\PersonLegalFixtures;
use App\DataFixtures\Core\PersonNaturalFixtures;
use App\DataFixtures\Core\StaffFixtures;
use App\Entity\Core\Department;
use App\Entity\Core\JobName;
use App\Entity\Core\Origin;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Offer\Offer;
use App\Entity\Offer\SubstitutionType;
use App\Utils\DateTime;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class OfferFixtures extends BaseFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            100,
            ['offer'],
            function ($i, ObjectManager $manager) {
                $offer = new Offer();
                $offer->setCreator($this->getRandomEntityElement(Staff::class));
                $offer->setType($this->faker->randomElement(Offer::TYPES));
                $offer->setDepartment($this->getRandomEntityElement(Department::class));

                /** @var Person $person */
                $person = $this->faker->randomElement($manager->getRepository(PersonNatural::class)->searchByClassification(JobName::BASED))
                    ?? $this->getRandomPerson(70);
                $offer->setPerson($person);
                if($person instanceof PersonNatural){
                    $offer->setSpecialty($person->getSpecialty());
                }
                $offer->setOrigin($this->getRandomEntityElement(Origin::class));
                $offer->setCanvas($this->getReference('liberal offer canvas'));
                $offer->setSubstitutionType(
                    $manager->getRepository(SubstitutionType::class)
                        ->findOneBy(['substitutionType' => SubstitutionType::TYPE_LIBERAL_REPLACEMENT_PUNCTUAL])
                );

                $offer->setCreationDate(new \DateTimeImmutable('-5 years'));
                $offer->setLastUpdateDate(new DateTime());
                $offer->setLastFullUpdateAt(new DateTime());
                $startDate = $this->faker->dateTimeBetween('-3 years', '+1 years');
                $offer->setStartingDate($startDate);
                if ($this->faker->boolean(30)) {
                    $offer->setStartingDateModularity($this->faker->numberBetween(1, 5));
                    $offer->setStartingDateModularityType($this->faker->randomElement(Offer::MODULARITY_TYPES));
                }
                if ($this->faker->boolean(80)) {
                    $endDate = clone $startDate;
                    $offer->setEndDate($endDate->add(new \DateInterval('P' . $this->faker->numberBetween(1, 12) . 'M')));
                    if ($this->faker->boolean(30)) {
                        $offer->setEndDateModularity($this->faker->numberBetween(1, 5));
                        $offer->setEndDateModularityType($this->faker->randomElement(Offer::MODULARITY_TYPES));
                    }
                }

                $offer->setState($this->faker->randomElement(Offer::STATES));
                $offer->setPricing($this->faker->randomFloat(2, 0, 1000000));
                $offer->setCoordinatesRemoval($this->faker->boolean(10));
                if ($this->faker->boolean(30)) {
                    $offer->addContractor($this->getRandomReference('substitute'));
                }
                $offer->setIsModel($this->faker->boolean(10));

                return $offer;
            }
        );
    }

    public static function getGroups(): array
    {
        return [
            'offer'
        ];
    }

    public function getDependencies()
    {
        return [
            PersonNaturalFixtures::class,
            PersonLegalFixtures::class,
            CanvasFixtures::class,
            StaffFixtures::class
        ];
    }
}
