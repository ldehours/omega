<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

/**
 * Class BaseFixture
 * @package App\DataFixtures
 */
abstract class BaseFixture extends Fixture
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var Generator
     */
    protected $faker;

    /**
     * @var array
     */
    private $referencesIndex = [];

    /**
     * @param ObjectManager $manager
     * @return mixed
     */
    abstract protected function loadData(ObjectManager $manager);

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->faker = Factory::create('fr_FR');

        $this->loadData($manager);
    }

    /**
     * Create many objects at once:
     *
     *      $this->createMany(10, function(int $i) {
     *          $user = new User();
     *          $user->setFirstName('Ryan');
     *
     *           return $user;
     *      });
     *
     * @param int $count
     * @param string[] $groupNames Tag these created objects with this group name,
     *                            and use this later with getRandomReference(s)
     *                            to fetch only from this specific group.
     * @param callable $factory
     */
    protected function createMany(int $count, array $groupNames, callable $factory): void
    {
        for ($i = 0; $i < $count; $i++) {
            $entity = $factory($i,$this->manager);

            if (null === $entity) {
                throw new \LogicException('Did you forget to return the entity object from your callback to BaseFixture::createMany()?');
            }

            $this->manager->persist($entity);

            // store for usage later as groupName_#COUNT#
            foreach ($groupNames as $name){
                $ref = sprintf('%s_%d', $name, $i);
                if(!$this->hasReference($ref)){
                    $this->addReference($ref, $entity);
                }else{
                    $this->setReference($ref,$entity);
                }
            }

        }
        $this->manager->flush();
    }

    /**
     * @param $entityClass
     * @param array $condition
     * @return mixed
     */
    public function getRandomEntityElement($entityClass, array $condition = [])
    {
        if(!empty($condition)){
            return $this->faker->randomElement($this->manager->getRepository($entityClass)->findBy($condition));
        }
        return $this->faker->randomElement($this->manager->getRepository($entityClass)->findAll());
    }

    /**
     * @param int $personNaturalChances
     * @return object
     */
    public function getRandomPerson(int $personNaturalChances = 50){
        return $this->faker->boolean($personNaturalChances) ? $this->getRandomReference('personNatural') :
            $this->getRandomReference('personLegal');
    }

    /**
     * @param string $groupName
     * @return object
     */
    protected function getRandomReference(string $groupName)
    {
        if (!isset($this->referencesIndex[$groupName])) {
            $this->referencesIndex[$groupName] = [];

            foreach ($this->referenceRepository->getReferences() as $key => $ref) {
                if (strpos($key, $groupName . '_') === 0) {
                    $this->referencesIndex[$groupName][] = $key;
                }
            }
        }

        if (empty($this->referencesIndex[$groupName])) {
            throw new \InvalidArgumentException(sprintf('Did not find any references saved with the group name "%s"', $groupName));
        }

        $randomReferenceKey = $this->faker->randomElement($this->referencesIndex[$groupName]);

        return $this->getReference($randomReferenceKey);
    }

    /**
     * @param string $className
     * @param int $count
     * @return array
     */
    protected function getRandomReferences(string $className, int $count): array
    {
        $references = [];
        while (count($references) < $count) {
            $references[] = $this->getRandomReference($className);
        }

        return $references;
    }
}