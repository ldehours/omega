<?php

namespace App\DataFixtures\Core;

use App\Entity\Core\JobName;
use App\Entity\Core\PersonAccess;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class PersonAccessFixtures extends Fixture implements FixtureGroupInterface
{


    /**
     * @param ObjectManager $manager
     * @return mixed|void
     */
    public function load(ObjectManager $manager)
    {
        $allInstitutionJobNames = $manager->getRepository(JobName::class)->findBy([
            'classification' => [
                JobName::INSTITUTION,
                JobName::INSTITUTION_FUNCTION
            ]
        ]);
        $accessAnnounce = $manager->getRepository(PersonAccess::class)->findBy([
            'type' => [
                PersonAccess::DEPOSIT_LIBERAL_ANNOUNCE,
                PersonAccess::DEPOSIT_SALARIED_ANNOUNCE,
                PersonAccess::DEPOSIT_TO_CESSION
            ]
        ]);

        foreach ($allInstitutionJobNames as $institution) {
            $institution->setPersonAccess($accessAnnounce);
            $manager->persist($institution);
        }

        //Attribution des droits aux remplaçants
        $personSubstituteJobNames = $manager->getRepository(JobName::class)->findBy(['classification' => JobName::SUBSTITUTE]);
        $substituteAccesses = $manager->getRepository(PersonAccess::class)->findBy([
            'type' =>
                [
                    PersonAccess::CANDIDATE_TO_LIBERAL_ANNOUNCE,
                    PersonAccess::CANDIDATE_TO_SALARIED_ANNOUNCE,
                    PersonAccess::SUBSCRIBE_TO_CFML,
                    PersonAccess::SUBSCRIBE_TO_LMS,
                    PersonAccess::SUBSCRIBE_TO_BNC
                ]
        ]);
        foreach ($personSubstituteJobNames as $jobName) {
            //droits de bases des remplaçants
            $allSubstituteAccesses = [];
            switch ($jobName->getLabel()) {
                //Candidature à une annonce de cession ajouter aux droits d'un Médecin Remplaçant/candidat
                case 'Médecin Remplaçant/Candidat':
                    $allSubstituteAccesses = array_merge($substituteAccesses,
                        $manager->getRepository(PersonAccess::class)->findBy(['type' => [PersonAccess::CANDIDATE_TO_CESSION]]));
                    break;
                default:
                    $allSubstituteAccesses = $substituteAccesses;
                    break;
            }

            $jobName->setPersonAccess($allSubstituteAccesses);
            $manager->persist($jobName);
        }

        //Attribution des droits aux installés (adhésions)
        $personBasedJobNames = $manager->getRepository(JobName::class)->findBy(['classification' => JobName::BASED]);
        $basedAccesses = $manager->getRepository(PersonAccess::class)->findBy([
            'type' =>
                [
                    PersonAccess::SUBSCRIBE_TO_CFML,
                    PersonAccess::SUBSCRIBE_TO_LMS,
                    PersonAccess::SUBSCRIBE_TO_BNC
                ]
        ]);

        foreach ($personBasedJobNames as $jobName) {
            $allBasedAccesses = null;
            switch ($jobName->getLabel()) {

                case 'Médecin Collaborateur':
                    $allBasedAccesses = array_merge($basedAccesses,
                        $manager->getRepository(PersonAccess::class)->findBy([
                            'type' => [
                                PersonAccess::DEPOSIT_LIBERAL_ANNOUNCE,
                                PersonAccess::DEPOSIT_TO_CESSION,
                                PersonAccess::CANDIDATE_TO_CESSION
                            ]
                        ]));
                    break;
                case 'Médecin Collaborateur effectuant des remplacements':
                    $allBasedAccesses = array_merge($basedAccesses,
                        $manager->getRepository(PersonAccess::class)->findBy([
                            'type' => [
                                PersonAccess::DEPOSIT_LIBERAL_ANNOUNCE,
                                PersonAccess::DEPOSIT_TO_CESSION,
                                PersonAccess::CANDIDATE_TO_LIBERAL_ANNOUNCE,
                                PersonAccess::CANDIDATE_TO_CESSION
                            ]
                        ]));
                    break;
                case 'Médecin Installé':
                    $allBasedAccesses = array_merge($basedAccesses,
                        $manager->getRepository(PersonAccess::class)->findBy([
                            'type' => [
                                PersonAccess::DEPOSIT_LIBERAL_ANNOUNCE,
                                PersonAccess::DEPOSIT_TO_CESSION,
                                PersonAccess::CANDIDATE_TO_SALARIED_ANNOUNCE,
                                PersonAccess::CANDIDATE_TO_CESSION,
                                PersonAccess::SUBSCRIBE_TO_CONTRACT_RELAY,
                                PersonAccess::SUBSCRIBE_TO_CONTRACT_PRIVILEGE
                            ]
                        ]));
                    break;
                case 'Médecin Installé effectuant des remplacements':
                    $allBasedAccesses = array_merge($basedAccesses,
                        $manager->getRepository(PersonAccess::class)->findBy([
                            'type' => [
                                PersonAccess::DEPOSIT_LIBERAL_ANNOUNCE,
                                PersonAccess::DEPOSIT_TO_CESSION,
                                PersonAccess::CANDIDATE_TO_SALARIED_ANNOUNCE,
                                PersonAccess::CANDIDATE_TO_LIBERAL_ANNOUNCE,
                                PersonAccess::SUBSCRIBE_TO_CONTRACT_RELAY,
                                PersonAccess::SUBSCRIBE_TO_CONTRACT_PRIVILEGE
                            ]
                        ]));
                    break;
                // par défaut Médecin Retraité installé
                default:
                    $allBasedAccesses = array_merge($basedAccesses,
                        $manager->getRepository(PersonAccess::class)->findBy([
                            'type' => [
                                PersonAccess::CANDIDATE_TO_SALARIED_ANNOUNCE,
                                PersonAccess::CANDIDATE_TO_CESSION,
                                PersonAccess::CANDIDATE_TO_LIBERAL_ANNOUNCE
                            ]
                        ]));
                    break;
            }

            $jobName->setPersonAccess($allBasedAccesses);
            $manager->persist($jobName);
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['PersonAccess'];
    }

}

