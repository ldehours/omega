<?php

namespace App\DataFixtures\Core;

use App\DataFixtures\BaseFixture;
use App\Entity\Core\Discussion;
use App\Entity\Core\Service;
use App\Entity\Core\Staff;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class DiscussionFixtures extends BaseFixture implements FixtureGroupInterface, DependentFixtureInterface
{

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            100,
            ['discussion'],
            function ($i, ObjectManager $manager) {
                $discussion = new Discussion();
                $date = $this->faker->dateTimeBetween('-5 years', 'now');
                $discussion->setType($this->faker->randomElement(Discussion::TYPES));
                $discussion->setPerson($this->getRandomPerson());
                $discussion->setCreator($this->getRandomEntityElement(Staff::class));
                $discussion->setCreationDate($date);
                $discussion->setMemo($this->faker->text(200));
                $discussion->setSubject($this->faker->title);
                $discussion->setService($this->getRandomEntityElement(Service::class));
                $statusArray = Discussion::STATUS;
                $indexStatus = [];
                foreach ($statusArray as $status => $translate) {
                    $indexStatus[] = $status;
                }
                $discussion->setStatus($this->faker->randomElement($indexStatus));
                $discussion->setStatusDate($date->add(new \DateInterval('P1D')));
                $discussion->setTag($this->faker->numberBetween(0, 999999999));

                return $discussion;
            }
        );
    }

    /**
     * @return array|string[]
     */
    public static function getGroups(): array
    {
        return [
            'discussion'
        ];
    }

    /**
     * @return string[]
     */
    public function getDependencies()
    {
        return [
            PersonNaturalFixtures::class,
            PersonLegalFixtures::class,
            StaffFixtures::class
        ];
    }
}
