<?php

namespace App\DataFixtures\Core;

use App\Command\RestoreCommand;
use App\DataFixtures\BaseFixture;
use App\Entity\Core\Agenda;
use App\Entity\Core\Service;
use App\Entity\Core\Staff;
use App\Entity\Core\Telephone;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class StaffFixtures extends BaseFixture implements FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     * @return void
     * @throws \Exception
     */
    protected function loadData(ObjectManager $manager): void
    {
        $services = $manager->getRepository(Service::class)->findAll();

        /** @var Service $service */
        foreach ($services as $service) {
            $telephone = new Telephone();
            $telephone->setNumber($this->faker->phoneNumber);
            $telephone->setPhoneType($this->faker->randomElement(Telephone::TYPES));
            $staff = new Staff();
            $staff->setNickname($this->faker->userName);
            $lastname = $this->faker->lastName;
            $staff->setLastName($lastname);
            $staff->setFirstName($this->faker->firstName);
            $staff->setEmail($lastname . uniqid() . '@media-sante.com');
            $staff->setJobTitle($this->faker->jobTitle);
            $staff->setContract($this->faker->numberBetween(30, 39));
            $staff->setDateStart($this->faker->dateTimeBetween('-35 years', 'now'));
            $staff->setDateOfHiring($this->faker->dateTimeBetween('-35 years', 'now'));
            $staff->setInternalPhone((string)$this->faker->numberBetween(1, 99));
            $staff->setGender($this->faker->randomElement(Staff::GENDER));
            $staff->addExternalPhone($telephone);
            $staff->setIp($this->faker->localIpv4);
            $staff->setIsActive(true);
            $staff->setIsArchived(false);
            $staff->setRoles($this->rolesFromService($service));
            $staff->setPassword(
                $this->passwordEncoder->encodePassword(
                    $staff,
                    'johndoe'
                )
            );
            $staff->setAgenda(new Agenda());
            $staff->setService($service);
            $manager->persist($staff);
        }
        $manager->flush();
    }

    /**
     * @param Service $service
     * @return array
     */
    private function rolesFromService(Service $service): array
    {
        $result = [];
        foreach (RestoreCommand::SERVICES as $infosService) {
            if ($infosService['name'] === $service->getName()) {
                $result = $infosService['roles'];
                break;
            }
        }
        return $result;
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return [
            'staff'
        ];
    }
}
