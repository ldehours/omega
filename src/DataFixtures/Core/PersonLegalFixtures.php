<?php

namespace App\DataFixtures\Core;

use App\DataFixtures\BaseFixture;
use App\Entity\Core\Agenda;
use App\Entity\Core\JobName;
use App\Entity\Core\Origin;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\Staff;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PersonLegalFixtures extends BaseFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            20,
            ['personLegal'],
            function (int $i, ObjectManager $manager) {
                $agenda = new Agenda();
                $staff = $this->getRandomEntityElement(Staff::class);

                $personLegal = new PersonLegal();
                $personLegal->setCorporateName($this->faker->company);
                $personLegal->setPersonStatus($this->faker->randomElement(PersonLegal::STATUS));
                $personLegal->setOrigin($this->getRandomEntityElement(Origin::class));
                $personLegal->setMainJobName($this->getRandomEntityElement(JobName::class, ['classification' => JobName::INSTITUTION]));
                $personLegal->setCreationDate($this->faker->dateTimeBetween('-5 years', 'now'));
                if ($this->faker->boolean()) {
                    $randomNumber = $this->faker->numberBetween(1, 3);
                    for ($i = 0; $i < $randomNumber; $i++) {
                        $personLegal->addPersonNatural($this->getRandomReference('based'));
                    }
                }
                $personLegal->setAgenda($agenda);
                $personLegal->setCreator($staff);
                $staff->addPersonsCreated($personLegal);
                $manager->persist($personLegal);
                return $personLegal;
            }
        );
    }

    public static function getGroups(): array
    {
        return [
            'person',
            'personLegal'
        ];
    }

    public function getDependencies()
    {
        return [
            PersonNaturalFixtures::class,
            StaffFixtures::class
        ];
    }
}
