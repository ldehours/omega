<?php

namespace App\DataFixtures\Core;

use App\DataFixtures\BaseFixture;
use App\DataFixtures\Offer\OfferFixtures;
use App\Entity\Core\Note;
use App\Entity\Core\Staff;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class NoteFixtures extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            50,
            ['note'],
            function ($i, ObjectManager $manager) {
                $note = new Note();
                $note->setCreator($this->getRandomEntityElement(Staff::class));
                $note->setOffer($this->getRandomReference('offer'));
                $note->setNoteType($this->faker->randomElement(Note::TYPES));
                $note->setText($this->faker->text(200));
                return $note;
            }
        );
    }

    /**
     * @return string[]
     */
    public function getDependencies()
    {
        return [
            OfferFixtures::class,
            StaffFixtures::class
        ];
    }
}
