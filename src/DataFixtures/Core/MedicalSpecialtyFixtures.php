<?php

namespace App\DataFixtures\Core;

use App\DataFixtures\BaseFixture;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\MedicalSpecialtyQualification;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class MedicalSpecialtyFixtures extends BaseFixture implements FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    protected function loadData(ObjectManager $manager): void
    {
        $this->createMany(
            15,
            ['medicalSpecialty'],
            function ($i, ObjectManager $manager) {
                $medicalSpecialty = new MedicalSpecialty();
                $medicalSpecialty->setJobTitle($this->faker->sentence($this->faker->numberBetween(3, 6)));
                $words = '';
                foreach ($this->faker->words($this->faker->numberBetween(2, 3)) as $word) {
                    $words .= ucfirst($word) . ' ';
                }
                $medicalSpecialty->setOfficialLabel($words);
                /** @var MedicalSpecialtyQualification $msq */
                foreach ($manager->getRepository(MedicalSpecialtyQualification::class)->findAll() as $msq) {
                    $medicalSpecialty->addRelatedType($msq);
                }
                return $medicalSpecialty;
            }
        );
    }

    /**
     * This method must return an array of groups
     * on which the implementing class belongs to
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return [
            'medicalSpecialty'
        ];
    }
}
