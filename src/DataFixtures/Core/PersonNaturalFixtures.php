<?php

namespace App\DataFixtures\Core;

use App\DataFixtures\BaseFixture;
use App\Entity\Core\Address;
use App\Entity\Core\Agenda;
use App\Entity\Core\Department;
use App\Entity\Core\JobName;
use App\Entity\Core\Mail;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\Origin;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PersonNaturalFixtures extends BaseFixture implements FixtureGroupInterface, DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @return mixed|void
     * @throws \Exception
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            100,
            ['based', 'personNatural'],
            function (int $i, ObjectManager $manager) {
                return $this->createPersonNatural($i, $manager, JobName::BASED);
            }
        );

        $this->createMany(
            100,
            ['substitute', 'personNatural'],
            function (int $i, ObjectManager $manager) {
                return $this->createPersonNatural($i, $manager, JobName::SUBSTITUTE);
            }
        );
    }

    public  function createPersonNatural (int $i, ObjectManager $manager, int $classification) {
        $medicalSpecialtyRepository = $manager->getRepository(MedicalSpecialty::class);
        $staffRepository = $manager->getRepository(Staff::class);

        $personNatural = new PersonNatural();
        $lastName = $this->faker->lastName;
        $firstName = $this->faker->firstName;
        $personNatural->setLastName($lastName);
        $personNatural->setFirstName($firstName);
        $personNatural->setGender($this->faker->randomElement(PersonNatural::GENDER));

        $mail = new Mail();
        $mail->setAddress($lastName . '.' . $firstName . $this->faker->numberBetween(1, 999) . '@domaine.com');
        $mail->setIsDefault(true);
        $mail->setMailType($this->faker->randomElement(Mail::TYPES));
        $personNatural->addMail($mail);

        $address = new Address();
        $address->setDomiciliations($this->faker->randomElements(Address::DOMICILIATIONS));
        $address->setUsages($this->faker->randomElements(Address::USAGES));
        $address->setIsCedex($this->faker->boolean);
        $address->setCountryCode($this->faker->countryCode);
        $address->setLocality($this->faker->city);
        $address->setPostalCode(str_replace(' ','',$this->faker->postcode));
        $address->setAddressLine1($this->faker->streetAddress);
        $address->setAddressLine2($this->faker->boolean === true ? 'bis' : '');
        $address->setDepartment($this->getRandomEntityElement(Department::class));
        $personNatural->addAddress($address);

        $personNatural->setPersonStatus($this->faker->randomElement(PersonNatural::STATUS));
        $personNatural->setBirthDate($this->faker->dateTimeBetween('-70 years', '-15 years'));
        $personNatural->setCreationDate($this->faker->dateTimeBetween('-5 years', 'now'));
        $personNatural->setOrigin($this->getRandomEntityElement(Origin::class));
        $personNatural->setMainJobName(
            $this->faker->randomElement($manager->getRepository(JobName::class)->searchByClassification([$classification]))
        );

        $medicalSpecialties = $medicalSpecialtyRepository->findAll();
        $personNatural->setSpecialty($this->faker->randomElement($medicalSpecialties));
        $personNatural->addSkill($this->faker->randomElement($medicalSpecialties));
        $personNatural->addFormation($this->faker->randomElement($medicalSpecialties));
        $personNatural->addOrientation($this->faker->randomElement($medicalSpecialties));

        $staffs = $staffRepository->findAll();
        $personNatural->setAdvisor($this->faker->randomElement($staffs));
        $personNatural->setCreator($this->faker->randomElement($staffs));
        $agenda = new Agenda();
        $personNatural->setAgenda($agenda);
        return $personNatural;
    }

    public static function getGroups(): array
    {
        return [
            'person',
            'personNatural',
            'based',
            'substitute'
        ];
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return string[] class-string[]
     */
    public function getDependencies()
    {
        return [
            StaffFixtures::class,
            MedicalSpecialtyFixtures::class
        ];
    }
}
