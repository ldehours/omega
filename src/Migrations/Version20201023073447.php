<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201023073447 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person ADD person_to_contact LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', CHANGE additional_information_id additional_information_id INT DEFAULT NULL, CHANGE main_job_name_id main_job_name_id INT DEFAULT NULL, CHANGE person_status person_status INT DEFAULT NULL');
        $this->addSql('CREATE TABLE opinion (id INT AUTO_INCREMENT NOT NULL, giver_id INT NOT NULL, receiver_id INT NOT NULL, text LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_AB02B02775BD1D29 (giver_id), INDEX IDX_AB02B027CD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT FK_AB02B02775BD1D29 FOREIGN KEY (giver_id) REFERENCES person (id)');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT FK_AB02B027CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES person (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE opinion');
        $this->addSql('ALTER TABLE person DROP person_to_contact, CHANGE additional_information_id additional_information_id INT DEFAULT NULL, CHANGE main_job_name_id main_job_name_id INT DEFAULT NULL, CHANGE person_status person_status INT DEFAULT NULL');
    }
}
