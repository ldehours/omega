<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201112080837 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE membership_cfml_historization (id INT AUTO_INCREMENT NOT NULL, membership_id INT NOT NULL, dispatcher_id INT NOT NULL, receiver_id INT NOT NULL, modified_field VARCHAR(255) NOT NULL, date_historize DATETIME NOT NULL, INDEX IDX_3BF574D61FB354CD (membership_id), INDEX IDX_3BF574D6D1534C73 (dispatcher_id), INDEX IDX_3BF574D6CD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE membership_cfml_historization ADD CONSTRAINT FK_3BF574D61FB354CD FOREIGN KEY (membership_id) REFERENCES membership_cfml (id)');
        $this->addSql('ALTER TABLE membership_cfml_historization ADD CONSTRAINT FK_3BF574D6D1534C73 FOREIGN KEY (dispatcher_id) REFERENCES staff (id)');
        $this->addSql('ALTER TABLE membership_cfml_historization ADD CONSTRAINT FK_3BF574D6CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES staff (id)');
        $this->addSql('ALTER TABLE membership_cfml DROP FOREIGN KEY FK_ABE53D31D4D57CD');
        $this->addSql('DROP INDEX IDX_ABE53D31D4D57CD ON membership_cfml');
        $this->addSql('ALTER TABLE membership_cfml ADD creation_date DATETIME NOT NULL, DROP dispatch_date, CHANGE staff_id creator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE membership_cfml ADD CONSTRAINT FK_ABE53D3161220EA6 FOREIGN KEY (creator_id) REFERENCES staff (id)');
        $this->addSql('CREATE INDEX IDX_ABE53D3161220EA6 ON membership_cfml (creator_id)');
        $this->addSql('ALTER TABLE offer_historization CHANGE date_change date_change DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE membership_cfml_historization');
        $this->addSql('ALTER TABLE membership_cfml DROP FOREIGN KEY FK_ABE53D3161220EA6');
        $this->addSql('DROP INDEX IDX_ABE53D3161220EA6 ON membership_cfml');
        $this->addSql('ALTER TABLE membership_cfml ADD dispatch_date DATETIME DEFAULT NULL, DROP creation_date, CHANGE creator_id staff_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE membership_cfml ADD CONSTRAINT FK_ABE53D31D4D57CD FOREIGN KEY (staff_id) REFERENCES staff (id)');
        $this->addSql('CREATE INDEX IDX_ABE53D31D4D57CD ON membership_cfml (staff_id)');
        $this->addSql('ALTER TABLE offer_historization CHANGE date_change date_change DATETIME DEFAULT NULL');
    }
}
