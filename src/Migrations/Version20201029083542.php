<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201029083542 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE membership_cfml (id INT AUTO_INCREMENT NOT NULL, member_id INT NOT NULL, main_representative_id INT DEFAULT NULL, secondary_representative_id INT DEFAULT NULL, staff_id INT DEFAULT NULL, dispatch_date DATETIME DEFAULT NULL, INDEX IDX_ABE53D31D4D57CD (staff_id), INDEX IDX_ABE53D319B09227B (main_representative_id), INDEX IDX_ABE53D317597D3FE (member_id), INDEX IDX_ABE53D319678D782 (secondary_representative_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE membership_cfml ADD CONSTRAINT FK_ABE53D317597D3FE FOREIGN KEY (member_id) REFERENCES person_natural (id)');
        $this->addSql('ALTER TABLE membership_cfml ADD CONSTRAINT FK_ABE53D319678D782 FOREIGN KEY (secondary_representative_id) REFERENCES staff (id)');
        $this->addSql('ALTER TABLE membership_cfml ADD CONSTRAINT FK_ABE53D319B09227B FOREIGN KEY (main_representative_id) REFERENCES staff (id)');
        $this->addSql('ALTER TABLE membership_cfml ADD CONSTRAINT FK_ABE53D31D4D57CD FOREIGN KEY (staff_id) REFERENCES staff (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE membership_cfml');
    }
}
