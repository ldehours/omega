<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201119094400 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE membership_cfml_specific_status_historization (id INT AUTO_INCREMENT NOT NULL, membership_id INT NOT NULL, dispatcher_id INT NOT NULL, old_specific_status INT NOT NULL, date_historize DATETIME NOT NULL, INDEX IDX_46BCEC1FB354CD (membership_id), INDEX IDX_46BCECD1534C73 (dispatcher_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE membership_cfml_specific_status_historization ADD CONSTRAINT FK_46BCEC1FB354CD FOREIGN KEY (membership_id) REFERENCES membership_cfml (id)');
        $this->addSql('ALTER TABLE membership_cfml_specific_status_historization ADD CONSTRAINT FK_46BCECD1534C73 FOREIGN KEY (dispatcher_id) REFERENCES staff (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE membership_cfml_specific_status_historization');
    }
}
