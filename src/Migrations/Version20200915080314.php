<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200915080314 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE offer_medical_specialty');
        $this->addSql('ALTER TABLE address CHANGE administrative_area administrative_area VARCHAR(255) DEFAULT NULL, CHANGE dependent_locality dependent_locality VARCHAR(255) DEFAULT NULL, CHANGE sorting_code sorting_code VARCHAR(255) DEFAULT NULL, CHANGE address_line2 address_line2 VARCHAR(255) DEFAULT NULL, CHANGE locale locale VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE department CHANGE region_id region_id INT DEFAULT NULL, CHANGE code code VARCHAR(5) DEFAULT NULL');
        $this->addSql('ALTER TABLE discussion CHANGE person_id person_id INT DEFAULT NULL, CHANGE status status INT DEFAULT NULL, CHANGE status_date status_date DATETIME DEFAULT NULL, CHANGE tag tag VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE note CHANGE offer_id offer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE origin CHANGE usages usages JSON NOT NULL');
        $this->addSql('ALTER TABLE person CHANGE additional_information_id additional_information_id INT DEFAULT NULL, CHANGE main_job_name_id main_job_name_id INT DEFAULT NULL, CHANGE person_status person_status INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person_natural CHANGE advisor_id advisor_id INT DEFAULT NULL, CHANGE specialty_id specialty_id INT DEFAULT NULL, CHANGE gender gender SMALLINT DEFAULT NULL, CHANGE last_name last_name VARCHAR(255) DEFAULT NULL, CHANGE first_name first_name VARCHAR(255) DEFAULT NULL, CHANGE website_id website_id INT DEFAULT NULL, CHANGE birth_date birth_date DATETIME DEFAULT NULL, CHANGE rppsnumber rppsnumber VARCHAR(11) DEFAULT NULL, CHANGE job_status job_status JSON NOT NULL, CHANGE diploma diploma INT DEFAULT NULL, CHANGE years_experience years_experience INT DEFAULT NULL');
        $this->addSql('ALTER TABLE staff CHANGE roles roles JSON NOT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE internal_phone internal_phone VARCHAR(10) DEFAULT NULL, CHANGE gender gender SMALLINT DEFAULT NULL, CHANGE date_end date_end DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE staff_history CHANGE roles roles JSON NOT NULL, CHANGE first_name first_name VARCHAR(255) DEFAULT NULL, CHANGE email email VARCHAR(255) DEFAULT NULL, CHANGE internal_phone internal_phone VARCHAR(255) DEFAULT NULL, CHANGE ip ip VARCHAR(255) DEFAULT NULL, CHANGE gender gender SMALLINT DEFAULT NULL, CHANGE date_end date_end DATETIME DEFAULT NULL, CHANGE personal_phones personal_phones JSON NOT NULL');
        $this->addSql('ALTER TABLE time_range CHANGE week_day_id week_day_id INT DEFAULT NULL, CHANGE person_id person_id INT DEFAULT NULL, CHANGE end_date end_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE legacy CHANGE person_id person_id INT DEFAULT NULL, CHANGE id_cfml id_cfml INT DEFAULT NULL');
        $this->addSql('ALTER TABLE canvas CHANGE form_disposition form_disposition JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE criterion CHANGE criterion_head_dependency_id criterion_head_dependency_id INT DEFAULT NULL, CHANGE dependency dependency LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE substitution ADD last_full_update_at DATETIME NOT NULL, CHANGE end_date end_date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE offer ADD specialty_id INT DEFAULT NULL, ADD skills JSON NOT NULL, CHANGE department_id department_id INT DEFAULT NULL, CHANGE nearest_city nearest_city VARCHAR(255) DEFAULT NULL, CHANGE starting_date_modularity_type starting_date_modularity_type INT DEFAULT NULL, CHANGE on_call on_call JSON DEFAULT NULL, CHANGE end_date_modularity_type end_date_modularity_type INT DEFAULT NULL');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E9A353316 FOREIGN KEY (specialty_id) REFERENCES medical_specialty (id)');
        $this->addSql('CREATE INDEX IDX_29D6873E9A353316 ON offer (specialty_id)');
        $this->addSql('ALTER TABLE wish CHANGE time_range_id time_range_id INT DEFAULT NULL, CHANGE distance distance JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE notifiable_notification CHANGE notification_id notification_id INT DEFAULT NULL, CHANGE notifiable_entity_id notifiable_entity_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notification CHANGE message message VARCHAR(4000) DEFAULT NULL, CHANGE link link VARCHAR(4000) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE offer_medical_specialty (offer_id INT NOT NULL, medical_specialty_id INT NOT NULL, INDEX IDX_10E46A31BFC81879 (medical_specialty_id), INDEX IDX_10E46A3153C674EE (offer_id), PRIMARY KEY(offer_id, medical_specialty_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE offer_medical_specialty ADD CONSTRAINT FK_10E46A3153C674EE FOREIGN KEY (offer_id) REFERENCES offer (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE offer_medical_specialty ADD CONSTRAINT FK_10E46A31BFC81879 FOREIGN KEY (medical_specialty_id) REFERENCES medical_specialty (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE address CHANGE administrative_area administrative_area VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE dependent_locality dependent_locality VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE sorting_code sorting_code VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE address_line2 address_line2 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE locale locale VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE canvas CHANGE form_disposition form_disposition LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE criterion CHANGE criterion_head_dependency_id criterion_head_dependency_id INT DEFAULT NULL, CHANGE dependency dependency LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE department CHANGE region_id region_id INT DEFAULT NULL, CHANGE code code VARCHAR(5) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE discussion CHANGE person_id person_id INT DEFAULT NULL, CHANGE status status INT NOT NULL, CHANGE status_date status_date DATETIME DEFAULT \'NULL\', CHANGE tag tag VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE legacy CHANGE person_id person_id INT DEFAULT NULL, CHANGE id_cfml id_cfml INT DEFAULT NULL');
        $this->addSql('ALTER TABLE note CHANGE offer_id offer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notifiable_notification CHANGE notification_id notification_id INT DEFAULT NULL, CHANGE notifiable_entity_id notifiable_entity_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notification CHANGE message message VARCHAR(4000) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE link link VARCHAR(4000) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E9A353316');
        $this->addSql('DROP INDEX IDX_29D6873E9A353316 ON offer');
        $this->addSql('ALTER TABLE offer DROP specialty_id, DROP skills, CHANGE department_id department_id INT DEFAULT NULL, CHANGE nearest_city nearest_city VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE starting_date_modularity_type starting_date_modularity_type INT DEFAULT NULL, CHANGE on_call on_call LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`, CHANGE end_date_modularity_type end_date_modularity_type INT DEFAULT NULL');
        $this->addSql('ALTER TABLE origin CHANGE usages usages LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE person CHANGE additional_information_id additional_information_id INT DEFAULT NULL, CHANGE main_job_name_id main_job_name_id INT DEFAULT NULL, CHANGE person_status person_status INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person_natural CHANGE advisor_id advisor_id INT DEFAULT NULL, CHANGE specialty_id specialty_id INT DEFAULT NULL, CHANGE gender gender SMALLINT DEFAULT NULL, CHANGE last_name last_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE first_name first_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE website_id website_id INT DEFAULT NULL, CHANGE birth_date birth_date DATETIME DEFAULT \'NULL\', CHANGE rppsnumber rppsnumber VARCHAR(11) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE job_status job_status LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE diploma diploma INT DEFAULT NULL, CHANGE years_experience years_experience INT DEFAULT NULL');
        $this->addSql('ALTER TABLE staff CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE internal_phone internal_phone VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE gender gender SMALLINT DEFAULT NULL, CHANGE date_end date_end DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE staff_history CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE first_name first_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE internal_phone internal_phone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE ip ip VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE gender gender SMALLINT DEFAULT NULL, CHANGE date_end date_end DATETIME DEFAULT \'NULL\', CHANGE personal_phones personal_phones LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
        $this->addSql('ALTER TABLE substitution DROP last_full_update_at, CHANGE end_date end_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE time_range CHANGE week_day_id week_day_id INT DEFAULT NULL, CHANGE person_id person_id INT DEFAULT NULL, CHANGE end_date end_date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE wish CHANGE time_range_id time_range_id INT DEFAULT NULL, CHANGE distance distance LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_bin`');
    }
}
