<?php

namespace App\Entity\CFML;

use App\Entity\Core\Staff;
use App\Service\Historization\HistorizationHandlerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CFML\MembershipCfmlHistorizationRepository")
 */
class MembershipCfmlHistorization implements HistorizationHandlerInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CFML\MembershipCfml")
     * @ORM\JoinColumn(nullable=false)
     */
    private $membership;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dispatcher;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     * @ORM\JoinColumn(nullable=false)
     */
    private $receiver;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modifiedField;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_historize;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return MembershipCfml|null
     */
    public function getMembership(): ?MembershipCfml
    {
        return $this->membership;
    }

    /**
     * @param MembershipCfml|null $membership
     * @return $this
     */
    public function setMembership(?MembershipCfml $membership): self
    {
        $this->membership = $membership;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getDispatcher(): ?Staff
    {
        return $this->dispatcher;
    }

    /**
     * @param Staff|null $dispatcher
     * @return $this
     */
    public function setDispatcher(?Staff $dispatcher): self
    {
        $this->dispatcher = $dispatcher;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getReceiver(): ?Staff
    {
        return $this->receiver;
    }

    /**
     * @param Staff|null $receiver
     * @return $this
     */
    public function setReceiver(?Staff $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModifiedField()
    {
        return $this->modifiedField;
    }

    /**
     * @param mixed $modifiedField
     */
    public function setModifiedField($modifiedField): void
    {
        $this->modifiedField = $modifiedField;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateHistorize(): ?\DateTimeInterface
    {
        return $this->date_historize;
    }

    /**
     * @param \DateTimeInterface $date_historize
     * @return $this
     */
    public function setDateHistorize(\DateTimeInterface $date_historize): self
    {
        $this->date_historize = $date_historize;

        return $this;
    }
}

