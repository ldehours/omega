<?php

namespace App\Entity\CFML;

use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Service\Historization\HistorizableInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Traits\TranslatableTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CFML\MembershipRepository")
 */
class MembershipCfml implements HistorizableInterface
{
    use TranslatableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\PersonNatural")
     * @ORM\JoinColumn(nullable=false)
     */
    private $member;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     */
    private $mainRepresentative;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     */
    private $secondaryRepresentative;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     */
    private $creator;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $specificStatus;

    const SPECIFIC_STATUS_PARTICULAR_CASE = 501;
    const SPECIFIC_STATUS_DO_NOT_PROCESS = 502;
    const SPECIFIC_STATUS_DOCUMENTS_NOT_RETURNED = 503;

    const SPECIFIC_STATUS = [
        self::SPECIFIC_STATUS_PARTICULAR_CASE,
        self::SPECIFIC_STATUS_DO_NOT_PROCESS,
        self::SPECIFIC_STATUS_DOCUMENTS_NOT_RETURNED
    ];

    const LABEL = [
        self::SPECIFIC_STATUS_PARTICULAR_CASE => "Cas particulier",
        self::SPECIFIC_STATUS_DO_NOT_PROCESS => "Ne pas traiter",
        self::SPECIFIC_STATUS_DOCUMENTS_NOT_RETURNED => "Pièces non retournées"
    ];

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return PersonNatural|null
     */
    public function getMember(): ?PersonNatural
    {
        return $this->member;
    }

    /**
     * @param PersonNatural $personNatural
     * @return $this
     */
    public function setMember(PersonNatural $personNatural): self
    {
        $this->member = $personNatural;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getSecondaryRepresentative(): ?Staff
    {
        return $this->secondaryRepresentative;
    }

    /**
     * @param Staff|null $secondaryRepresentative
     * @return $this
     */
    public function setSecondaryRepresentative(?Staff $secondaryRepresentative): self
    {
        $this->secondaryRepresentative = $secondaryRepresentative;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getMainRepresentative(): ?Staff
    {
        return $this->mainRepresentative;
    }

    /**
     * @param Staff|null $mainRepresentative
     * @return $this
     */
    public function setMainRepresentative(?Staff $mainRepresentative): self
    {
        $this->mainRepresentative = $mainRepresentative;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getCreator(): ?Staff
    {
        return $this->creator;
    }

    /**
     * @param Staff|null $staff
     * @return $this
     */
    public function setCreator(?Staff $staff): self
    {
        $this->creator = $staff;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getSpecificStatus()
    {
        return $this->specificStatus;
    }

    /**
     * @param mixed $specificStatus
     */
    public function setSpecificStatus($specificStatus): void
    {
        $this->specificStatus = $specificStatus;
    }
}
