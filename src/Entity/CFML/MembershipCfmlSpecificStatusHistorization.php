<?php

namespace App\Entity\CFML;

use App\Entity\Core\Staff;
use App\Service\Historization\HistorizableInterface;
use App\Service\Serializer\Serializable;
use Doctrine\ORM\Mapping as ORM;
use App\Service\Historization\HistorizationHandlerInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CFML\MembershipCfmlSpecificStatusHistorizationRepository")
 */
class MembershipCfmlSpecificStatusHistorization implements HistorizationHandlerInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CFML\MembershipCfml")
     * @ORM\JoinColumn(nullable=false)
     */
    private $membership;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     * @ORM\JoinColumn(nullable=false)
     */
    private $dispatcher;

    /**
     * @ORM\Column(type="integer")
     */
    private $oldSpecificStatus;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateHistorize;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return MembershipCfml|null
     */
    public function getMembership(): ?MembershipCfml
    {
        return $this->membership;
    }

    /**
     * @param MembershipCfml|null $membership
     * @return $this
     */
    public function setMembership(?MembershipCfml $membership): self
    {
        $this->membership = $membership;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getDispatcher(): ?Staff
    {
        return $this->dispatcher;
    }

    /**
     * @param Staff|null $dispatcher
     * @return $this
     */
    public function setDispatcher(?Staff $dispatcher): self
    {
        $this->dispatcher = $dispatcher;

        return $this;
    }

    /**
     * @return mixed |bool
     */
    public function getOldSpecificStatus()
    {
        return $this->oldSpecificStatus;
    }

    /**
     * @param mixed $oldSpecificStatus
     */
    public function setOldSpecificStatus($oldSpecificStatus): void
    {
        $this->oldSpecificStatus = $oldSpecificStatus;
    }

    /**
     * @return mixed
     */
    public function getDateHistorize()
    {
        return $this->dateHistorize;
    }

    /**
     * @param mixed $dateHistorize
     */
    public function setDateHistorize($dateHistorize): void
    {
        $this->dateHistorize = $dateHistorize;
    }

}
