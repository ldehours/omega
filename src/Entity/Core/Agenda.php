<?php

namespace App\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\AgendaRepository")
 */
class Agenda
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", unique=true)
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\TimeRange", mappedBy="agenda", orphanRemoval=true)
     */
    private $timeRanges;

    /**
     * Agenda constructor.
     */
    public function __construct()
    {
        $this->timeRanges = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|TimeRange[]
     */
    public function getTimeRanges(): Collection
    {
        return $this->timeRanges;
    }

    public function addTimeRange(TimeRange $timeRange): self
    {
        if (!$this->timeRanges->contains($timeRange)) {
            $this->timeRanges[] = $timeRange;
            $timeRange->setAgenda($this);
        }

        return $this;
    }

    /**
     * @param TimeRange $timeRange
     * @return Agenda
     */
    public function removeTimeRange(TimeRange $timeRange): self
    {
        if ($this->timeRanges->contains($timeRange)) {
            $this->timeRanges->removeElement($timeRange);
            // set the owning side to null (unless already changed)
            if ($timeRange->getAgenda() === $this) {
                $timeRange->setAgenda(null);
            }
        }

        return $this;
    }
}
