<?php

namespace App\Entity\Core;

use App\Service\Historization\HistorizationHandlerInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\StaffHistoryRepository")
 */
class StaffHistory implements HistorizationHandlerInterface
{
    const GENDER_MALE = 201;
    const GENDER_FEMALE = 202;
    const GENDER_OTHER = 203;
    const GENDER = [
        self::GENDER_MALE,
        self::GENDER_FEMALE,
        self::GENDER_OTHER
    ];

    const LABEL = [
        self::GENDER_MALE => "Homme",
        self::GENDER_FEMALE => "Femme",
        self::GENDER_OTHER => "Indéfini"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $creator;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfSave;

    /**
     * TEXT type with the maximum length possible for this type
     * @ORM\Column(type="text", length=65535)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $staffId;

    /**
     * @ORM\Column(type="string")
     */
    private $nickname;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $internalPhone;

    /**
     * @ORM\Column(type="string" , nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="integer")
     */
    private $contract;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfHiring;

    /**
     * @ORM\Column(type="string")
     */
    private $service;

    /**
     * @ORM\Column(type="json")
     */
    private $personalPhones = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return String
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     * @return StaffHistory
     */
    public function setCreator($creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateOfSave(): ?\DateTimeInterface
    {
        return $this->dateOfSave;
    }

    /**
     * @param \DateTimeInterface $dateOfSave
     * @return StaffHistory
     */
    public function setDateOfSave(\DateTimeInterface $dateOfSave): self
    {
        $this->dateOfSave = $dateOfSave;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return StaffHistory
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStaffId(): ?int
    {
        return $this->staffId;
    }

    /**
     * @param int $staffId
     * @return StaffHistory
     */
    public function setStaffId(int $staffId): self
    {
        $this->staffId = $staffId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return StaffHistory
     */
    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return StaffHistory
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return StaffHistory
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return StaffHistory
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return StaffHistory
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    /**
     * @param string $jobTitle
     * @return StaffHistory
     */
    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInternalPhone(): ?string
    {
        return $this->internalPhone;
    }

    /**
     * @param string|null $internalPhone
     * @return StaffHistory
     */
    public function setInternalPhone(?string $internalPhone): self
    {
        $this->internalPhone = $internalPhone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return StaffHistory
     */
    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return StaffHistory
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @param Agenda $agenda
     * @return StaffHistory
     */
    public function setAgenda(Agenda $agenda): self
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getGender(): ?int
    {
        return $this->gender;
    }

    /**
     * @param int|null $gender
     * @return StaffHistory
     * @throws \Exception
     */
    public function setGender(?int $gender): self
    {
        if (!in_array($gender, self::GENDER)) {
            throw new \Exception("This gender hasn't in the GENDER array");
        }
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getContract(): ?int
    {
        return $this->contract;
    }

    /**
     * @param int $contract
     * @return StaffHistory
     */
    public function setContract(int $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateStart(): ?DateTimeInterface
    {
        return $this->dateStart;
    }

    /**
     * @param DateTimeInterface $dateStart
     * @return StaffHistory
     */
    public function setDateStart(DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateEnd(): ?DateTimeInterface
    {
        return $this->dateEnd;
    }

    /**
     * @param DateTimeInterface|null $dateEnd
     * @return StaffHistory
     */
    public function setDateEnd(?DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateOfHiring(): ?DateTimeInterface
    {
        return $this->dateOfHiring;
    }

    /**
     * @param DateTimeInterface $dateOfHiring
     * @return StaffHistory
     */
    public function setDateOfHiring(DateTimeInterface $dateOfHiring): self
    {
        $this->dateOfHiring = $dateOfHiring;

        return $this;
    }

    /**
     * @return string
     */
    public function getService(): ?string
    {
        return $this->service;
    }

    /**
     * @param String $service
     * @return StaffHistory
     */
    public function setService($service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return array
     */
    public function getPersonalPhones(): array
    {
        $personalPhones = $this->personalPhones;
        return array_unique($personalPhones);
    }

    /**
     * @param array $personalPhones
     * @return StaffHistory
     */
    public function setPersonalPhones(array $personalPhones): self
    {
        $this->personalPhones = $personalPhones;

        return $this;
    }
}
