<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\MailRepository")
 */
class Mail
{
    use TranslatableTrait;

    const PRO = 1;
    const PERSO = 2;
    const TYPES = array(
        self::PRO,
        self::PERSO
    );
    const LABEL = array(
        self::PRO => 'Professionnel',
        self::PERSO => 'Personnel',
    );

    const ICON = array(
        self::PRO => '<i class="fas fa-briefcase-medical" title="Professionnel"></i>',
        self::PERSO => '<i class=\'fas fa-home\' title="Personnel"></i>',
    );
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     */
    private $mailType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDefault;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Person", cascade={"persist"}, mappedBy="mails")
     */
    private $persons;


    public function __construct()
    {
        $this->persons = new ArrayCollection();
    }


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Mail
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMailType(): ?int
    {
        return $this->mailType;
    }

    /**
     * @param int $mailType
     * @return Mail
     */
    public function setMailType(int $mailType): self
    {
        if (!in_array($mailType, self::TYPES)) {
            throw new \InvalidArgumentException('$mailType must be defined in TYPES array');
        }
        $this->mailType = $mailType;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDefault(): ?bool
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     * @return Mail
     */
    public function setIsDefault(bool $isDefault): self
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    /**
     * @param Person $person
     * @return Mail
     */
    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
            $person->addMail($this);
        }
        return $this;
    }

    /**
     * @param Person $person
     * @return Mail
     */
    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            $person->removeMail($this);
        }
        return $this;
    }
}
