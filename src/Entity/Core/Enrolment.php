<?php

namespace App\Entity\Core;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\EnrolmentRepository")
 */
class Enrolment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="enrolments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personEnrolled;

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="enrolments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $relatedService;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @param \DateTimeInterface $startDate
     * @return Enrolment
     */
    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @param \DateTimeInterface $endDate
     * @return Enrolment
     */
    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPersonEnrolled(): ?Person
    {
        return $this->personEnrolled;
    }

    /**
     * @param Person|null $personEnrolled
     * @return Enrolment
     */
    public function setPersonEnrolled(?Person $personEnrolled): self
    {
        $this->personEnrolled = $personEnrolled;

        return $this;
    }

    /**
     * @return Service|null
     */
    public function getRelatedService(): ?Service
    {
        return $this->relatedService;
    }

    /**
     * @param Service|null $relatedService
     * @return Enrolment
     */
    public function setRelatedService(?Service $relatedService): self
    {
        $this->relatedService = $relatedService;

        return $this;
    }
}
