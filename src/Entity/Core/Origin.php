<?php

namespace App\Entity\Core;

use App\Entity\Offer\Offer;
use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\OriginRepository")
 */
class Origin
{
    use TranslatableTrait;

    const OFFER = 101;
    const PERSON = 102;
    const ENTITY_USAGES = array(
        self::OFFER,
        self::PERSON
    );

    const LABEL = array(
        self::OFFER => 'Offre',
        self::PERSON => 'Client',
    );

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="json")
     */
    private $usages = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Person", mappedBy="origin")
     */
    private $persons;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Offer", mappedBy="origin")
     */
    private $offers;

    /**
     * Origin constructor.
     */
    public function __construct()
    {
        $this->persons = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Origin
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getUsages(): ?array
    {
        return $this->usages;
    }

    /**
     * @param array $usages
     * @return Origin
     */
    public function setUsages(array $usages): self
    {
        foreach ($usages as $usage) {
            if (!in_array($usage, self::ENTITY_USAGES)) {
                throw new \InvalidArgumentException('$usage must be contained element from USAGES array');
            }
        }
        $this->usages = $usages;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Origin
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->label;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    /**
     * @param Person $person
     * @return Origin
     */
    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
            $person->setOrigin($this);
        }

        return $this;
    }

    /**
     * @param Person $person
     * @return Origin
     */
    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            // set the owning side to null (unless already changed)
            if ($person->getOrigin() === $this) {
                $person->setOrigin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    /**
     * @param Offer $offer
     * @return Origin
     */
    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setOrigin($this);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return Origin
     */
    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getOrigin() === $this) {
                $offer->setOrigin(null);
            }
        }

        return $this;
    }
}
