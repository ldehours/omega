<?php

namespace App\Entity\Core;

use App\Entity\Legacy\Legacy;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Substitution;
use App\Entity\Offer\Wish;
use App\Traits\TranslatableTrait;
use App\Utils\DateTime;
use App\Utils\PHPHelper;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\OrderBy;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\PersonRepository")
 * @InheritanceType( "JOINED" )
 * @DiscriminatorColumn( name = "PERSON_TYPE", type = "string" )
 * @DiscriminatorMap( { "personLegal" = "PersonLegal",
 *                      "personNatural" = "PersonNatural" } )
 */
abstract class Person
{
    use TranslatableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Address",cascade={"persist"},inversedBy="person")
     */
    protected $addresses;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Telephone",cascade={"persist"}, inversedBy="persons")
     */
    protected $telephones;

    /**
     * @ORM\ManyToMany(targetEntity="Service")
     */
    protected $services;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Enrolment", mappedBy="personEnrolled")
     */
    protected $enrolments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Discussion", mappedBy="person",cascade={"persist"}, orphanRemoval=true)
     * @OrderBy({"creationDate"="DESC"})
     */
    protected $discussions;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $personStatus;

    /**
     * Référence à l'entitée Legacy, pour l'historique
     * @ORM\OneToMany(targetEntity="App\Entity\Legacy\Legacy", mappedBy="person")
     */
    protected $legacies;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $creation_date;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\JobName", inversedBy="persons")
     */
    protected $jobNames;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Origin", inversedBy="persons")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $origin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\Note", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $additionalInformation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\Agenda", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $agenda;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff", inversedBy="personsCreated")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $creator;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\TimeRange", mappedBy="person")
     */
    protected $timeRanges;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Mail",cascade={"persist"},inversedBy="persons")
     */
    protected $mails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Substitution", mappedBy="person")
     */
    private $substitutions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\JobName")
     */
    protected $mainJobName;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offer\Offer", inversedBy="receivers")
     */
    private $offersSent;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $personToContact = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Opinion", mappedBy="giver")
     */
    private $opinionsGiven;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Opinion", mappedBy="receiver")
     */
    private $opinionsReceived;

    /**
     * Person constructor.
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->telephones = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->enrolments = new ArrayCollection();
        $this->discussions = new ArrayCollection();
        $this->legacies = new ArrayCollection();
        $this->creation_date = new DateTime();
        $this->timeRanges = new ArrayCollection();
        $this->substitutions = new ArrayCollection();
        $this->mails = new ArrayCollection();
        $this->jobNames = new ArrayCollection();
        $this->offersSent = new ArrayCollection();
        $this->opinionsGiven = new ArrayCollection();
        $this->opinionsReceived = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getPersonStatus(): ?int
    {
        return $this->personStatus;
    }

    /**
     * @param int|null $personStatus
     * @return Person
     */
    public function setPersonStatus(?int $personStatus): self
    {
        $this->personStatus = $personStatus;

        return $this;
    }

    /**
     * @return ArrayCollection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    /**
     * @param Address $address
     * @return Person
     */
    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->addPerson($this);
        }

        return $this;
    }

    /**
     * @param Address $address
     * @return Person
     */
    public function removeAddress(Address $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            $address->removePerson($this);
        }

        return $this;
    }

    /**
     * @param Telephone $telephone
     * @return Person
     */
    public function addTelephone(Telephone $telephone): self
    {
        if (!$this->telephones->contains($telephone)) {
            $this->telephones[] = $telephone;
        }

        return $this;
    }

    /**
     * @param Telephone $telephone
     * @return Person
     */
    public function removeTelephone(Telephone $telephone): self
    {
        if ($this->telephones->contains($telephone)) {
            $this->telephones->removeElement($telephone);
        }

        return $this;
    }

    /**
     * @param int $type
     * @return ArrayCollection|Telephone[]
     */
    public function getTelephonesByType(int $type): Collection
    {
        $result = new ArrayCollection();
        foreach ($this->getTelephones() as $telephone) {
            if ($telephone->getPhoneType() === $type) {
                $result[] = $telephone;
            }
        }
        return $result;
    }

    /**
     * @return ArrayCollection|Telephone[]
     */
    public function getTelephones(): Collection
    {
        return $this->telephones;
    }

    /**
     * @return ArrayCollection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    /**
     * @param Service $service
     * @return Person
     */
    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
        }

        return $this;
    }

    /**
     * @param Service $service
     * @return Person
     */
    public function removeService(Service $service): self
    {
        if ($this->services->contains($service)) {
            $this->services->removeElement($service);
        }

        return $this;
    }

    /**
     * @return Collection|Enrolment[]
     */
    public function getEnrolments(): Collection
    {
        return $this->enrolments;
    }

    /**
     * @param Enrolment $enrolment
     * @return Person
     */
    public function addEnrolment(Enrolment $enrolment): self
    {
        if (!$this->enrolments->contains($enrolment)) {
            $this->enrolments[] = $enrolment;
            $enrolment->setPersonEnrolled($this);
        }

        return $this;
    }

    /**
     * @param Enrolment $enrolment
     * @return Person
     */
    public function removeEnrolment(Enrolment $enrolment): self
    {
        if ($this->enrolments->contains($enrolment)) {
            $this->enrolments->removeElement($enrolment);
            // set the owning side to null (unless already changed)
            if ($enrolment->getPersonEnrolled() === $this) {
                $enrolment->setPersonEnrolled(null);
            }
        }

        return $this;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $executionContext
     */
    public function validate(ExecutionContextInterface $executionContext)
    {
        if ($this->mails->count() > 0 && $this->getDefaultMail() === null) {
            $executionContext->buildViolation('Une adresse mail par défault est obligatoire')
                ->atPath("mails")
                ->addViolation();
        }
    }

    /**
     * @return Mail|null
     */
    public function getDefaultMail(): ?Mail
    {
        /**
         * @var Mail $mail
         */
        foreach ($this->mails as $mail) {
            if ($mail->getIsDefault()) {
                return $mail;
            }
        }
        return null;
    }

    /**
     * @return Collection|Discussion[]
     */
    public function getDiscussions(): Collection
    {
        return $this->discussions;
    }

    /**
     * @param Discussion $discussion
     * @return Person
     */
    public function addDiscussion(Discussion $discussion): self
    {
        if (!$this->discussions->contains($discussion)) {
            $this->discussions[] = $discussion;
            $discussion->setPerson($this);
        }
        return $this;
    }

    /**
     * @param Discussion $discussion
     * @return Person
     */
    public function removeDiscussion(Discussion $discussion): self
    {
        if ($this->discussions->contains($discussion)) {
            $this->discussions->removeElement($discussion);
            // set the owning side to null (unless already changed)
            if ($discussion->getPerson() === $this) {
                $discussion->setPerson(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Legacy[]
     */
    public function getLegacies(): Collection
    {
        return $this->legacies;
    }

    /**
     * @param Legacy $legacy
     * @return Person
     */
    public function addLegacy(Legacy $legacy): self
    {
        if (!$this->legacies->contains($legacy)) {
            $this->legacies[] = $legacy;
            $legacy->setPerson($this);
        }
        return $this;
    }

    /**
     * @param Legacy $legacy
     * @return Person
     */
    public function removeLegacy(Legacy $legacy): self
    {
        if ($this->legacies->contains($legacy)) {
            $this->legacies->removeElement($legacy);
            // set the owning side to null (unless already changed)
            if ($legacy->getPerson() === $this) {
                $legacy->setPerson(null);
            }
        }
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreationDate(): ?DateTimeInterface
    {
        return $this->creation_date;
    }

    /**
     * @param DateTimeInterface $creation_date
     * @return Person
     */
    public function setCreationDate(DateTimeInterface $creation_date): self
    {
        $this->creation_date = $creation_date;
        return $this;
    }

    /**
     * @return Collection|JobName[]
     */
    public function getJobNames(): Collection
    {
        return $this->jobNames;
    }

    /**
     * @param JobName $jobName
     * @return Person
     */
    public function addJobName(JobName $jobName): self
    {
        if (!$this->jobNames->contains($jobName)) {
            $this->jobNames[] = $jobName;
        }

        return $this;
    }

    /**
     * @param JobName $jobName
     * @return Person
     */
    public function removeJobName(JobName $jobName): self
    {
        if ($this->jobNames->contains($jobName)) {
            $this->jobNames->removeElement($jobName);
        }

        return $this;
    }

    /**
     * @return Origin|null
     */
    public function getOrigin(): ?Origin
    {
        return $this->origin;
    }

    /**
     * @param Origin|null $origin
     * @return Person
     */
    public function setOrigin(?Origin $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return Note|null
     */
    public function getAdditionalInformation(): ?Note
    {
        return $this->additionalInformation;
    }

    /**
     * @param Note|null $additionalInformation
     * @return Person
     */
    public function setAdditionalInformation(?Note $additionalInformation): self
    {
        if ($additionalInformation->getText() === '') {
            $this->additionalInformation = null;
        } else {
            $this->additionalInformation = $additionalInformation;
        }
        return $this;
    }

    /**
     * @return Agenda | null
     */
    public function getAgenda(): ?Agenda
    {
        return $this->agenda;
    }

    /**
     * @param Agenda $agenda
     * @return Person
     */
    public function setAgenda(Agenda $agenda): self
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * @return Address|null
     * @throws Exception
     */
    public function getDefaultAddress(): ?Address
    {
        $addresses = $this->getAddresses();
        if ($addresses != null) {
            foreach ($addresses as $address) {
                $addressLabel = [];
                $addressLabel[] = $address::LABEL;
                if (in_array(Address::FISCAL, $addressLabel) || in_array(Address::CORRESPONDENCE, $addressLabel)) {
                    return $address;
                }
            }
        } else {
            return null;
        }
        return $addresses[0];
    }

    /**
     * @return Staff
     */
    public function getCreator(): Staff
    {
        return $this->creator;
    }

    /**
     * @param Staff $creator
     * @return Person
     */
    public function setCreator(?Staff $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection|TimeRange[]
     */
    public function getTimeRanges(): Collection
    {
        return $this->timeRanges;
    }

    /**
     * @param TimeRange $timeRange
     * @return Person
     */
    public function addTimeRange(TimeRange $timeRange): self
    {
        if (!$this->timeRanges->contains($timeRange)) {
            $this->timeRanges[] = $timeRange;
            $timeRange->setPerson($this);
        }

        return $this;
    }

    /**
     * @param TimeRange $timeRange
     * @return Person
     */
    public function removeTimeRange(TimeRange $timeRange): self
    {
        if ($this->timeRanges->contains($timeRange)) {
            $this->timeRanges->removeElement($timeRange);
            // set the owning side to null (unless already changed)
            if ($timeRange->getPerson() === $this) {
                $timeRange->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Substitution[]
     */
    public function getSubstitutions(): Collection
    {
        return $this->substitutions;
    }

    /**
     * @param Substitution $substitution
     * @return Person
     */
    public function addSubstitution(Substitution $substitution): self
    {
        if (!$this->substitutions->contains($substitution)) {
            $this->substitutions[] = $substitution;
            $substitution->setPerson($this);
        }

        return $this;
    }

    /**
     * @param Substitution $substitution
     * @return Person
     */
    public function removeSubstitution(Substitution $substitution): self
    {
        if ($this->substitutions->contains($substitution)) {
            $this->substitutions->removeElement($substitution);
            // set the owning side to null (unless already changed)
            if ($substitution->getPerson() === $this) {
                $substitution->setPerson(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Mail[]
     */
    public function getMails(): Collection
    {
        return $this->mails;
    }

    /**
     * @param Mail $mail
     * @return Person
     */
    public function addMail(Mail $mail): self
    {
        if (!$this->mails->contains($mail)) {
            $this->mails[] = $mail;
            $mail->addPerson($this);
        }
        return $this;
    }

    /**
     * @param Mail $mail
     * @return Person
     */
    public function removeMail(Mail $mail): self
    {
        if ($this->mails->contains($mail)) {
            $this->mails->removeElement($mail);
            $mail->removePerson($this);
        }
        return $this;
    }

    /**
     * @return JobName|null
     */
    public function getMainJobName(): ?JobName
    {
        return $this->mainJobName;
    }

    /**
     * @param JobName|null $mainJobName
     * @return Person
     */
    public function setMainJobName(?JobName $mainJobName): self
    {
        $this->mainJobName = $mainJobName;

        return $this;
    }

    /**
     * @return Offer[]|Collection
     */
    public function getOffers(): Collection
    {
        $substitutions = clone $this->getSubstitutions();
        if (!$substitutions->isEmpty()) {
            foreach ($substitutions as $substitution) {
                if (!($substitution instanceof Offer)) {
                    $substitutions->removeElement($substitution);
                }
            }
        }
        return $substitutions;
    }

    /**
     * @return Offer[]|Collection
     */
    public function getRealOffers(): Collection
    {
        $substitutions = clone $this->getSubstitutions();
        if (!$substitutions->isEmpty()) {
            foreach ($substitutions as $substitution) {
                if (!($substitution instanceof Offer) || $substitution->getIsModel()) {
                    $substitutions->removeElement($substitution);
                }
            }
        }
        return $substitutions;
    }

    /**
     * @param int $state
     * @return ArrayCollection
     */
    public function getOfferFromState(int $state)
    {
        $offers = new ArrayCollection();
        /** @var Offer $offer */
        foreach ($this->getRealOffers() as $offer){
            if($offer->getState() === $state){
                $offers->add($offer);
            }
        }
        return $offers;
    }

    /**
     * @return Offer[]|Collection
     */
    public function getUpcomingOffers(): Collection
    {
        $now = new DateTime();
        return PHPHelper::collectionFilter($this->getRealOffers(), function($offer) use ($now){
            return $offer->getStartingDate() >= $now;
        });
    }

    /**
     * @return Collection
     */
    public function getArchivedOffers(): Collection
    {
        $now = new DateTime();
        return PHPHelper::collectionFilter($this->getRealOffers(), function($offer) use ($now){
            return $offer->getStartingDate() < $now;
        });
    }

    /**
     * @return Substitution[]|Collection
     */
    public function getModelsOffers(): Collection
    {
        $substitutions = clone $this->getSubstitutions();
        if (!$substitutions->isEmpty()) {
            foreach ($substitutions as $substitution) {
                if (!($substitution instanceof Offer) || !$substitution->getIsModel()) {
                    $substitutions->removeElement($substitution);
                }
            }
        }
        return $substitutions;
    }

    /**
     * @return Substitution[]|Collection
     */
    public function getWishes(): Collection
    {
        $substitutions = clone $this->getSubstitutions();
        if (!$substitutions->isEmpty()) {
            foreach ($substitutions as $substitution) {
                if (!($substitution instanceof Wish)) {
                    $substitutions->removeElement($substitution);
                }
            }
        }
        return $substitutions;
    }

    /**
     * @return Substitution[]|Collection
     */
    public function getRealWishes(): Collection
    {
        $substitutions = clone $this->getSubstitutions();
        if (!$substitutions->isEmpty()) {
            foreach ($substitutions as $substitution) {
                if (!($substitution instanceof Wish) || $substitution->getIsModel()) {
                    $substitutions->removeElement($substitution);
                }
            }
        }
        return $substitutions;
    }

    /**
     * @return Substitution[]|Collection
     */
    public function getModelsWishes(): Collection
    {
        $substitutions = clone $this->getSubstitutions();
        if (!$substitutions->isEmpty()) {
            foreach ($substitutions as $substitution) {
                if (!($substitution instanceof Wish) || !$substitution->getIsModel()) {
                    $substitutions->removeElement($substitution);
                }
            }
        }
        return $substitutions;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffersSent(): Collection
    {
        return $this->offersSent;
    }

    /**
     * @param Offer $offersSent
     * @return $this
     */
    public function addOffersSent(Offer $offersSent): self
    {
        if (!$this->offersSent->contains($offersSent)) {
            $this->offersSent[] = $offersSent;
        }

        return $this;
    }

    /**
     * @param Offer $offersSent
     * @return $this
     */
    public function removeOffersSent(Offer $offersSent): self
    {
        if ($this->offersSent->contains($offersSent)) {
            $this->offersSent->removeElement($offersSent);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $name = '';
        if ($this instanceof PersonNatural) {
            $name = $this->getLastName() . ' ' . $this->getFirstName();
        } elseif ($this instanceof PersonLegal) {
            $name = $this->getCorporateName();
        }
        return $name;
    }

    /**
     * @return array|null
     */
    public function getPersonToContact(): ?array
    {
        return $this->personToContact;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function addPersonToContact(string $value): self
    {
        if(!in_array($value, $this->personToContact)){
            $this->personToContact[] = $value;
        }
        return $this;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function removePersonToContact(string $value): self
    {
        $this->setPersonToContact(array_filter($this->personToContact, function($item) use ($value){
            return $item !== $value;
        }));
        return $this;
    }

    /**
     * @param array|null $personToContact
     * @return $this
     */
    public function setPersonToContact(?array $personToContact): self
    {
        $this->personToContact = $personToContact;

        return $this;
    }

    /**
     * @return Collection|Opinion[]
     */
    public function getOpinionsGiven(): Collection
    {
        return $this->opinionsGiven;
    }

    /**
     * @param Opinion $opinionsGiven
     * @return $this
     */
    public function addOpinionsGiven(Opinion $opinionsGiven): self
    {
        if (!$this->opinionsGiven->contains($opinionsGiven)) {
            $this->opinionsGiven[] = $opinionsGiven;
            $opinionsGiven->setGiver($this);
        }

        return $this;
    }

    /**
     * @param Opinion $opinionsGiven
     * @return $this
     */
    public function removeOpinionsGiven(Opinion $opinionsGiven): self
    {
        if ($this->opinionsGiven->contains($opinionsGiven)) {
            $this->opinionsGiven->removeElement($opinionsGiven);
            // set the owning side to null (unless already changed)
            if ($opinionsGiven->getGiver() === $this) {
                $opinionsGiven->setGiver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Opinion[]
     */
    public function getOpinionsReceived(): Collection
    {
        return $this->opinionsReceived;
    }

    /**
     * @param Opinion $opinionsReceived
     * @return $this
     */
    public function addOpinionsReceived(Opinion $opinionsReceived): self
    {
        if (!$this->opinionsReceived->contains($opinionsReceived)) {
            $this->opinionsReceived[] = $opinionsReceived;
            $opinionsReceived->setReceiver($this);
        }

        return $this;
    }

    /**
     * @param Opinion $opinionsReceived
     * @return $this
     */
    public function removeOpinionsReceived(Opinion $opinionsReceived): self
    {
        if ($this->opinionsReceived->contains($opinionsReceived)) {
            $this->opinionsReceived->removeElement($opinionsReceived);
            // set the owning side to null (unless already changed)
            if ($opinionsReceived->getReceiver() === $this) {
                $opinionsReceived->setReceiver(null);
            }
        }

        return $this;
    }
}
