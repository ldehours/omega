<?php

namespace App\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\MedicalSpecialtyRepository")
 */
class MedicalSpecialty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $officialLabel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobTitle;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\MedicalSpecialtyQualification", inversedBy="medicalSpecialties")
     */
    private $relatedType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\PersonNatural", mappedBy="specialty")
     */
    private $specialtyPersonNaturals;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonNatural", mappedBy="skills")
     */
    private $skillPersonNaturals;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonNatural", mappedBy="formations")
     */
    private $formationPersonNaturals;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonNatural", mappedBy="orientations")
     */
    private $orientationPersonNaturals;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonNatural", mappedBy="capacities")
     */
    private $capacityPersonNaturals;


    /**
     * MedicalSpecialty constructor.
     */
    public function __construct()
    {
        $this->relatedType = new ArrayCollection();
        $this->specialtyPersonNaturals = new ArrayCollection();
        $this->skillPersonNaturals = new ArrayCollection();
        $this->formationPersonNaturals = new ArrayCollection();
        $this->orientationPersonNaturals = new ArrayCollection();
        $this->capacityPersonNaturals = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getOfficialLabel(): ?string
    {
        return $this->officialLabel;
    }

    /**
     * @param string $officialLabel
     * @return MedicalSpecialty
     */
    public function setOfficialLabel(string $officialLabel): self
    {
        $this->officialLabel = $officialLabel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    /**
     * @param string $jobTitle
     * @return MedicalSpecialty
     */
    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * @return Collection|MedicalSpecialtyQualification[]
     */
    public function getRelatedType(): Collection
    {
        return $this->relatedType;
    }

    /**
     * @param MedicalSpecialtyQualification $relatedType
     * @return MedicalSpecialty
     */
    public function addRelatedType(MedicalSpecialtyQualification $relatedType): self
    {
        if (!$this->relatedType->contains($relatedType)) {
            $this->relatedType[] = $relatedType;
        }

        return $this;
    }

    /**
     * @param MedicalSpecialtyQualification $relatedType
     * @return MedicalSpecialty
     */
    public function removeRelatedType(MedicalSpecialtyQualification $relatedType): self
    {
        if ($this->relatedType->contains($relatedType)) {
            $this->relatedType->removeElement($relatedType);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->officialLabel;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getSpecialtyPersonNaturals(): Collection
    {
        return $this->specialtyPersonNaturals;
    }

    /**
     * @param PersonNatural $specialtyPersonNatural
     * @return MedicalSpecialty
     * @throws \InvalidArgumentException
     */
    public function addSpecialtyPersonNatural(PersonNatural $specialtyPersonNatural): self
    {
        if (!$this->specialtyPersonNaturals->contains($specialtyPersonNatural)) {
            $this->specialtyPersonNaturals[] = $specialtyPersonNatural;
            $specialtyPersonNatural->setSpecialty($this);
        }

        return $this;
    }

    /**
     * @param PersonNatural $specialtyPersonNatural
     * @return MedicalSpecialty
     * @throws \InvalidArgumentException
     */
    public function removeSpecialtyPersonNatural(PersonNatural $specialtyPersonNatural): self
    {
        if ($this->specialtyPersonNaturals->contains($specialtyPersonNatural)) {
            $this->specialtyPersonNaturals->removeElement($specialtyPersonNatural);
            // set the owning side to null (unless already changed)
            if ($specialtyPersonNatural->getSpecialty() === $this) {
                $specialtyPersonNatural->setSpecialty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getSkillPersonNaturals(): Collection
    {
        return $this->skillPersonNaturals;
    }

    /**
     * @param PersonNatural $skillPersonNatural
     * @return MedicalSpecialty
     * @throws \InvalidArgumentException
     */
    public function addSkillPersonNatural(PersonNatural $skillPersonNatural): self
    {
        if (!$this->skillPersonNaturals->contains($skillPersonNatural)) {
            $this->skillPersonNaturals[] = $skillPersonNatural;
            $skillPersonNatural->addSkill($this);
        }

        return $this;
    }

    /**
     * @param PersonNatural $skillPersonNatural
     * @return MedicalSpecialty
     */
    public function removeSkillPersonNatural(PersonNatural $skillPersonNatural): self
    {
        if ($this->skillPersonNaturals->contains($skillPersonNatural)) {
            $this->skillPersonNaturals->removeElement($skillPersonNatural);
            $skillPersonNatural->removeSkill($this);
        }

        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getFormationPersonNaturals(): Collection
    {
        return $this->formationPersonNaturals;
    }

    /**
     * @param PersonNatural $formationPersonNatural
     * @return MedicalSpecialty
     * @throws \InvalidArgumentException
     */
    public function addFormationPersonNatural(PersonNatural $formationPersonNatural): self
    {
        if (!$this->formationPersonNaturals->contains($formationPersonNatural)) {
            $this->formationPersonNaturals[] = $formationPersonNatural;
            $formationPersonNatural->addFormation($this);
        }

        return $this;
    }

    /**
     * @param PersonNatural $formationPersonNatural
     * @return MedicalSpecialty
     */
    public function removeFormationPersonNatural(PersonNatural $formationPersonNatural): self
    {
        if ($this->formationPersonNaturals->contains($formationPersonNatural)) {
            $this->formationPersonNaturals->removeElement($formationPersonNatural);
            $formationPersonNatural->removeFormation($this);
        }

        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getOrientationPersonNaturals(): Collection
    {
        return $this->orientationPersonNaturals;
    }

    /**
     * @param PersonNatural $orientationPersonNatural
     * @return MedicalSpecialty
     * @throws \InvalidArgumentException
     */
    public function addOrientationPersonNatural(PersonNatural $orientationPersonNatural): self
    {
        if (!$this->orientationPersonNaturals->contains($orientationPersonNatural)) {
            $this->orientationPersonNaturals[] = $orientationPersonNatural;
            $orientationPersonNatural->addOrientation($this);
        }

        return $this;
    }

    /**
     * @param PersonNatural $orientationPersonNatural
     * @return MedicalSpecialty
     */
    public function removeOrientationPersonNatural(PersonNatural $orientationPersonNatural): self
    {
        if ($this->orientationPersonNaturals->contains($orientationPersonNatural)) {
            $this->orientationPersonNaturals->removeElement($orientationPersonNatural);
            $orientationPersonNatural->removeOrientation($this);
        }

        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getCapacityPersonNaturals(): Collection
    {
        return $this->capacityPersonNaturals;
    }

    /**
     * @param PersonNatural $capacityPersonNatural
     * @return MedicalSpecialty
     * @throws \InvalidArgumentException
     */
    public function addCapacityPersonNatural(PersonNatural $capacityPersonNatural): self
    {
        if (!$this->capacityPersonNaturals->contains($capacityPersonNatural)) {
            $this->capacityPersonNaturals[] = $capacityPersonNatural;
            $capacityPersonNatural->addCapacity($this);
        }

        return $this;
    }

    /**
     * @param PersonNatural $capacityPersonNatural
     * @return MedicalSpecialty
     */
    public function removeCapacityPersonNatural(PersonNatural $capacityPersonNatural): self
    {
        if ($this->capacityPersonNaturals->contains($capacityPersonNatural)) {
            $this->capacityPersonNaturals->removeElement($capacityPersonNatural);
            $capacityPersonNatural->removeCapacity($this);
        }

        return $this;
    }
}
