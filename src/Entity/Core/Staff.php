<?php

namespace App\Entity\Core;

use App\Entity\Offer\OfferHistorization;
use App\Entity\Offer\Substitution;
use App\Service\Historization\HistorizableInterface;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\StaffRepository")
 * @UniqueEntity(fields={"nickname"}, message="Un compte existe déjà avec ce pseudonyme")
 * @UniqueEntity(fields={"email"}, message="Un compte existe déjà avec cette adresse mail")
 * @Notifiable(name="staff")
 */
class Staff implements UserInterface, NotifiableInterface, HistorizableInterface
{
    const NOTIFICATION_TYPE = [
        "Création Souhaits",
        "Modification Souhaits",
        "Création nouvelles périodes sur Agenda",
        "Modification Agenda",
        "Autre"
    ];

    const GENDER_MALE = 201;
    const GENDER_FEMALE = 202;
    const GENDER_OTHER = 203;
    const GENDER = [
        self::GENDER_MALE,
        self::GENDER_FEMALE,
        self::GENDER_OTHER
    ];

    const LABEL = [
        self::GENDER_MALE => "Homme",
        self::GENDER_FEMALE => "Femme",
        self::GENDER_OTHER => "Indéfini"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Type(type="alnum")
     */
    private $nickname;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Entrez un mot de passe")
     * @Assert\Length(min=6, max=4096, minMessage="Votre mot de passe doit conenir au moins 6 caractères")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^(\p{Ll}|\p{Lu}|[ ',-])+$/")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true , unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Regex(pattern="/^[0-9]*$/")
     */
    private $internalPhone;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Telephone", cascade={"persist"})
     */
    private $externalPhones;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\Regex(pattern="/^[0-9a-fA-F:\.]*$/")
     */
    private $ip;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\PersonNatural", mappedBy="advisor")
     */
    private $clientPortfolio;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Discussion", mappedBy="creator")
     */
    private $discussions;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\Agenda", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $agenda;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Service", inversedBy="staff", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $service;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Person", mappedBy="creator")
     */
    private $personsCreated;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Substitution", mappedBy="creator")
     */
    private $substitutions;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="integer")
     */
    private $contract;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfHiring;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\OfferHistorization", mappedBy="staff", orphanRemoval=true)
     */
    private $offerHistorizations;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     */
    private $isArchived;

    /**
     * Staff constructor.
     */
    public function __construct()
    {
        $this->externalPhones = new ArrayCollection();
        $this->clientPortfolio = new ArrayCollection();
        $this->discussions = new ArrayCollection();
        $this->personsCreated = new ArrayCollection();
        $this->substitutions = new ArrayCollection();
        $this->offerHistorizations = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return Staff
     */
    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->nickname;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return Staff
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @param string|null $password
     * @return Staff
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Staff
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Staff
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Staff
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    /**
     * @param string $jobTitle
     * @return Staff
     */
    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInternalPhone(): ?string
    {
        return $this->internalPhone;
    }

    /**
     * @param string|null $internalPhone
     * @return Staff
     */
    public function setInternalPhone(?string $internalPhone): self
    {
        $this->internalPhone = $internalPhone;

        return $this;
    }

    /**
     * @param Telephone $externalPhone
     * @return Staff
     */
    public function addExternalPhone(Telephone $externalPhone): self
    {
        if (!$this->externalPhones->contains($externalPhone)) {
            $this->externalPhones[] = $externalPhone;
        }

        return $this;
    }

    /**
     * @param Telephone $externalPhone
     * @return Staff
     */
    public function removeExternalPhone(Telephone $externalPhone): self
    {
        if ($this->externalPhones->contains($externalPhone)) {
            $this->externalPhones->removeElement($externalPhone);
        }

        return $this;
    }

    /**
     * @return Staff
     */
    public function removeAllExternalPhones(): self
    {
        $this->externalPhones = new ArrayCollection();
        return $this;
    }

    /**
     * @return Collection|Telephone[]
     */
    public function getExternalPhones(): Collection
    {
        return $this->externalPhones;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return Staff
     */
    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Staff
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getClientPortfolio(): Collection
    {
        return $this->clientPortfolio;

    }

    /**
     * @param PersonNatural $clientPortfolio
     * @return Staff
     */
    public function addClientPortfolio(PersonNatural $clientPortfolio): self
    {
        if (!$this->clientPortfolio->contains($clientPortfolio)) {
            $this->clientPortfolio[] = $clientPortfolio;
            $clientPortfolio->setAdvisor($this);
        }

        return $this;
    }

    /**
     * @param PersonNatural $clientPortfolio
     * @return Staff
     */
    public function removeClientPortfolio(PersonNatural $clientPortfolio): self
    {
        if ($this->clientPortfolio->contains($clientPortfolio)) {
            $this->clientPortfolio->removeElement($clientPortfolio);
            if ($clientPortfolio->getAdvisor() === $this) {
                $clientPortfolio->setAdvisor(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->lastName . ' ' . $this->firstName;
    }


    /**
     * @return Collection|Discussion[]
     */
    public function getDiscussions(): Collection
    {
        return $this->discussions;
    }

    /**
     * @param Discussion $discussion
     * @return Staff
     */
    public function addDiscussion(Discussion $discussion): self
    {
        if (!$this->discussions->contains($discussion)) {
            $this->discussions[] = $discussion;
            $discussion->setCreator($this);
        }

        return $this;
    }

    /**
     * @param Discussion $discussion
     * @return Staff
     */
    public function removeDiscussion(Discussion $discussion): self
    {
        if ($this->discussions->contains($discussion)) {
            $this->discussions->removeElement($discussion);
            // set the owning side to null (unless already changed)
            if ($discussion->getCreator() === $this) {
                $discussion->setCreator(null);
            }
        }

        return $this;
    }

    /*
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): void
    {
        // Useless in our case but necessary with this interface
    }

    /**
     * @return Agenda | null
     */
    public function getAgenda(): ?Agenda
    {
        return $this->agenda;
    }

    /**
     * @param Agenda $agenda
     * @return Staff
     */
    public function setAgenda(Agenda $agenda): self
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * @return Service|null
     */
    public function getService(): ?Service
    {
        return $this->service;
    }

    /**
     * @param Service|null $service
     * @return Staff
     */
    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersonsCreated(): Collection
    {
        return $this->personsCreated;
    }

    /**
     * @param Person $personsCreated
     * @return Staff
     */
    public function addPersonsCreated(Person $personsCreated): self
    {
        if (!$this->personsCreated->contains($personsCreated)) {
            $this->personsCreated[] = $personsCreated;
            $personsCreated->setCreator($this);
        }

        return $this;
    }

    /**
     * @param Person $personsCreated
     * @return Staff
     */
    public function removePersonsCreated(Person $personsCreated): self
    {
        if ($this->personsCreated->contains($personsCreated)) {
            $this->personsCreated->removeElement($personsCreated);
            // set the owning side to null (unless already changed)
            if ($personsCreated->getCreator() === $this) {
                $personsCreated->setCreator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Substitution[]
     */
    public function getSubstitutions(): Collection
    {
        return $this->substitutions;
    }

    /**
     * @param Substitution $substitution
     * @return Staff
     */
    public function addSubstitution(Substitution $substitution): self
    {
        if (!$this->substitutions->contains($substitution)) {
            $this->substitutions[] = $substitution;
            $substitution->setCreator($this);
        }

        return $this;
    }

    /**
     * @param Substitution $substitution
     * @return Staff
     */
    public function removeSubstitution(Substitution $substitution): self
    {
        if ($this->substitutions->contains($substitution)) {
            $this->substitutions->removeElement($substitution);
            // set the owning side to null (unless already changed)
            if ($substitution->getCreator() === $this) {
                $substitution->setCreator(null);
            }
        }
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGender(): ?int
    {
        return $this->gender;
    }

    /**
     * @param int|null $gender
     * @return Staff
     * @throws \Exception
     */
    public function setGender(?int $gender): self
    {
        if (!in_array($gender, self::GENDER)) {
            throw new \Exception("The gender '" . $gender . "' could not be found in the GENDER array");
        }
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getContract(): ?int
    {
        return $this->contract;
    }

    /**
     * @param int $contract
     * @return Staff
     */
    public function setContract(int $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateStart(): ?DateTimeInterface
    {
        return $this->dateStart;
    }

    /**
     * @param DateTimeInterface $dateStart
     * @return Staff
     */
    public function setDateStart(DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateEnd(): ?DateTimeInterface
    {
        return $this->dateEnd;
    }

    /**
     * @param DateTimeInterface|null $dateEnd
     * @return Staff
     */
    public function setDateEnd(?DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateOfHiring(): ?DateTimeInterface
    {
        return $this->dateOfHiring;
    }

    /**
     * @param DateTimeInterface $dateOfHiring
     * @return Staff
     */
    public function setDateOfHiring(DateTimeInterface $dateOfHiring): self
    {
        $this->dateOfHiring = $dateOfHiring;

        return $this;
    }

    /**
     * @return string
     */
    public function getInitials(): string
    {
        $initials = '';
        $lastName = $this->getLastName();
        $firstName = $this->getFirstName();
        if (!empty($firstName)) {
            $initials .= strtoupper($firstName[0]);
        }
        if (!empty($lastName)) {
            $initials .= strtoupper($lastName[0]);
        }
        return $initials;

    }

    /**
     * @return Collection|OfferHistorization[]
     */
    public function getOfferHistorizations(): Collection
    {
        return $this->offerHistorizations;
    }

    /**
     * @param OfferHistorization $offerHistorization
     * @return $this
     */
    public function addOfferHistorization(OfferHistorization $offerHistorization): self
    {
        if (!$this->offerHistorizations->contains($offerHistorization)) {
            $this->offerHistorizations[] = $offerHistorization;
            $offerHistorization->setStaff($this);
        }

        return $this;
    }

    /**
     * @param OfferHistorization $offerHistorization
     * @return $this
     */
    public function removeOfferHistorization(OfferHistorization $offerHistorization): self
    {
        if ($this->offerHistorizations->contains($offerHistorization)) {
            $this->offerHistorizations->removeElement($offerHistorization);
            // set the owning side to null (unless already changed)
            if ($offerHistorization->getStaff() === $this) {
                $offerHistorization->setStaff(null);
            }
        }
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    /**
     * @param bool $isArchived
     * @return $this
     */
    public function setIsArchived(bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }
}
