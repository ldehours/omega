<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\PersonLegalRepository")
 */
class PersonLegal extends Person
{
    use TranslatableTrait;

    const PRIVATE_PROFIT = 101;
    const PRIVATE_NON_PROFIT = 102;
    const PUBLIC = 103;
    const OTHER = 104;
    const STATUS = array(
        self::PRIVATE_PROFIT,
        self::PRIVATE_NON_PROFIT,
        self::PUBLIC,
        self::OTHER
    );

    const LABEL = array(
        self::PRIVATE_PROFIT => 'Privé lucratif',
        self::PRIVATE_NON_PROFIT => 'Privé non lucratif',
        self::PUBLIC => 'Public',
        self::OTHER => 'Autre'
    );


    /**
     * @ORM\Id @ORM\OneToOne(targetEntity="App\Entity\Core\Person")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $corporateName;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonNatural", inversedBy="personLegals")
     */
    private $personNaturals;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\ContactInstitution", mappedBy="personLegal")
     */
    private $contactInstitutions;

    /**
     * PersonLegal constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->personNaturals = new ArrayCollection();
        $this->contactInstitutions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCorporateName(): ?string
    {
        return $this->corporateName;
    }

    /**
     * @param string $corporateName
     * @return PersonLegal
     */
    public function setCorporateName(string $corporateName): self
    {
        $this->corporateName = $corporateName;

        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getPersonNaturals(): Collection
    {
        return $this->personNaturals;
    }

    /**
     * @param PersonNatural $personNatural
     * @return PersonLegal
     */
    public function addPersonNatural(PersonNatural $personNatural): self
    {
        if (!$this->personNaturals->contains($personNatural)) {
            $this->personNaturals[] = $personNatural;
        }

        return $this;
    }

    /**
     * @param PersonNatural $personNatural
     * @return PersonLegal
     */
    public function removePersonNatural(PersonNatural $personNatural): self
    {
        if ($this->personNaturals->contains($personNatural)) {
            $this->personNaturals->removeElement($personNatural);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getContacts(){
        $persons = new ArrayCollection();
        foreach ($this->getContactInstitutions() as $contact){
            $persons[] = $contact->getPersonNatural();
        }
        return $persons;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->corporateName;
    }

    /**
     * @return Collection|ContactInstitution[]
     */
    public function getContactInstitutions(): Collection
    {
        return $this->contactInstitutions;
    }

    /**
     * @param ContactInstitution $contactInstitution
     * @return $this
     */
    public function addContactInstitution(ContactInstitution $contactInstitution): self
    {
        if (!$this->contactInstitutions->contains($contactInstitution)) {
            $this->contactInstitutions[] = $contactInstitution;
            $contactInstitution->setPersonLegal($this);
        }

        return $this;
    }

    /**
     * @param ContactInstitution $contactInstitution
     * @return $this
     */
    public function removeContactInstitution(ContactInstitution $contactInstitution): self
    {
        if ($this->contactInstitutions->contains($contactInstitution)) {
            $this->contactInstitutions->removeElement($contactInstitution);
            // set the owning side to null (unless already changed)
            if ($contactInstitution->getPersonLegal() === $this) {
                $contactInstitution->setPersonLegal(null);
            }
        }

        return $this;
    }
}
