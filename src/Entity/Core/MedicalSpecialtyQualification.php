<?php

namespace App\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\MedicalSpecialtyQualificationRepository")
 */
class MedicalSpecialtyQualification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\MedicalSpecialty", mappedBy="relatedType")
     */
    private $medicalSpecialties;

    /**
     * MedicalSpecialtyQualification constructor.
     */
    public function __construct()
    {
        $this->medicalSpecialties = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return MedicalSpecialtyQualification
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|MedicalSpecialty[]
     */
    public function getMedicalSpecialties(): Collection
    {
        return $this->medicalSpecialties;
    }

    /**
     * @param MedicalSpecialty $medicalSpecialty
     * @return MedicalSpecialtyQualification
     */
    public function addMedicalSpecialty(MedicalSpecialty $medicalSpecialty): self
    {
        if (!$this->medicalSpecialties->contains($medicalSpecialty)) {
            $this->medicalSpecialties[] = $medicalSpecialty;
            $medicalSpecialty->addRelatedType($this);
        }

        return $this;
    }

    /**
     * @param MedicalSpecialty $medicalSpecialty
     * @return MedicalSpecialtyQualification
     */
    public function removeMedicalSpecialty(MedicalSpecialty $medicalSpecialty): self
    {
        if ($this->medicalSpecialties->contains($medicalSpecialty)) {
            $this->medicalSpecialties->removeElement($medicalSpecialty);
            $medicalSpecialty->removeRelatedType($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->label;
    }
}
