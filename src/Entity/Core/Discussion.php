<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use App\Utils\DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\DiscussionRepository")
 */
class Discussion
{
    use TranslatableTrait;

    /*tous les received en impairs et tous les sent en pairs de manière à avoir les actions émises par nous d'un côté et ceux reçus par le client d'un autre*/
    const MAIL_RECEIVED = 1;
    const MAIL_SENT = 2;
    const PHONECALL_RECEIVED = 3;
    const PHONECALL_SENT = 4;
    const MEMO_RECRUITMENT_DEPT = 6;
    const MEMO_BUSINESS_DEPT = 8;

    const TYPES = [
        self::MAIL_RECEIVED,
        self::MAIL_SENT,
        self::PHONECALL_RECEIVED,
        self::PHONECALL_SENT
    ];

    const LABEL = [
        self::MAIL_RECEIVED => 'Email reçu',
        self::MAIL_SENT => 'Email envoyé',
        self::PHONECALL_RECEIVED => 'Appel téléphonique reçu',
        self::PHONECALL_SENT => 'Appel téléphonique émis',
        self::MEMO_RECRUITMENT_DEPT => 'Notes Chargées de clientèle',
        self::MEMO_BUSINESS_DEPT => 'Notes Commerciales'
    ];

    const PERIOD_ONE_MONTH = 1;
    const PERIOD_THREE_MONTH = 2;
    const PERIOD_SIX_MONTH = 3;
    const PERIOD_ONE_YEAR = 4;
    const PERIOD_THREE_YEAR = 5;
    const PERIOD_OLDEST = 6;

    const PERIODS = [
        self::PERIOD_ONE_MONTH,
        self::PERIOD_THREE_MONTH,
        self::PERIOD_SIX_MONTH,
        self::PERIOD_ONE_YEAR,
        self::PERIOD_THREE_YEAR,
        self::PERIOD_OLDEST
    ];

    const PERIODS_LABELS = [
        self::PERIOD_ONE_MONTH => "1 mois",
        self::PERIOD_THREE_MONTH => "3 mois",
        self::PERIOD_SIX_MONTH => "6 mois",
        self::PERIOD_ONE_YEAR => "1 an",
        self::PERIOD_THREE_YEAR => "3 ans",
        self::PERIOD_OLDEST => ""
    ];

    const ICON = [
        self::MAIL_RECEIVED => '<i class=\'fas fa-inbox timeline-icon\' title="Email reçu"></i>',
        self::MAIL_SENT => '<i class=\'fas fa-paper-plane timeline-icon\' title="Email envoyé"></i>',
        self::PHONECALL_RECEIVED => '<i class=\'fas fa-phone timeline-icon\' title="Appel téléphonique reçu"></i>',
        self::PHONECALL_SENT => '<i class=\'fas fa-phone timeline-icon\' title="Appel téléphonique émis"></i>'
    ];

    /*statut des mails*/
    const DELIVERED = 0;
    const OPENED = 1;
    const CLICKED = 2;
    const REJECTED = 3;
    const HARDBOUNCED = 4;
    const FILTERED = 7;
    /*statut des appels*/
    const FINISHED = 5;
    const SCHEDULED = 6;

    const STATUS = [
        self::DELIVERED => "délivré",
        self::OPENED => "ouvert",
        self::CLICKED => "cliqué",
        self::REJECTED => "rejeté",
        self::HARDBOUNCED => "non délivré",
        self::FILTERED => "filtré",
        self::FINISHED => "terminé",
        self::SCHEDULED => "programmé"
    ];


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="text")
     */
    private $memo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="discussions")
     * @ORM\JoinColumn(nullable=true)
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff", inversedBy="discussions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Service")
     * @ORM\JoinColumn(nullable=false)
     */
    private $service;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $statusDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subject;

    /**
     * C'est l'ID du mail de chez tipimail (si c'est un mail)
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tag;


    public function __construct()
    {
        $this->creationDate = new DateTime('now');
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Discussion
     * @throws \Exception
     */
    public function setType(int $type): self
    {
        if (!in_array($type, self::TYPES)) {
            throw new \Exception("$type doesnt exist in Discussion::TYPES");
        }
        $this->type = $type;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getCreationDate(): ?DateTimeInterface
    {
        return $this->creationDate;
    }

    /**
     * @param DateTimeInterface $creationDate
     * @return Discussion
     */
    public function setCreationDate(DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMemo(): ?string
    {
        return $this->memo;
    }

    /**
     * @param string $memo
     * @return Discussion
     */
    public function setMemo(string $memo): self
    {
        $this->memo = $memo;

        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person|null $person
     * @return Discussion
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getCreator(): ?Staff
    {
        return $this->creator;
    }

    /**
     * @param Staff|null $creator
     * @return Discussion
     */
    public function setCreator(?Staff $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Service|null
     */
    public function getService(): ?Service
    {
        return $this->service;
    }

    /**
     * @param Service|null $service
     * @return Discussion
     */
    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Discussion
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getStatusDate(): ?\DateTimeInterface
    {
        return $this->statusDate;
    }

    /**
     * @param DateTimeInterface $statusDate
     * @return Discussion
     */
    public function setStatusDate(\DateTimeInterface $statusDate): self
    {
        $this->statusDate = $statusDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return Discussion
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return Discussion
     */
    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }
}
