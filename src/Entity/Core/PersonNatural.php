<?php

namespace App\Entity\Core;

use App\Entity\Offer\Offer;
use App\Service\Serializer\Serializable;
use App\Traits\TranslatableTrait;
use App\Utils\PHPHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\PersonNaturalRepository")
 */
class PersonNatural extends Person implements Serializable
{
    use TranslatableTrait;

    const IN_ACTIVITY = 101;
    const NO_ACTIVITY = 102;
    const DECEASED = 103;
    const STATUS = [
        self::IN_ACTIVITY,
        self::NO_ACTIVITY,
        self::DECEASED
    ];

    const GENDER_MALE = 201;
    const GENDER_FEMALE = 202;
    const GENDER_OTHER = 203;
    const GENDER = [
        self::GENDER_MALE,
        self::GENDER_FEMALE,
        self::GENDER_OTHER
    ];

    const JOB_STATUS_LIBERAL = 301;
    const JOB_STATUS_SALARIED = 302;
    const JOB_STATUS = [
        self::JOB_STATUS_LIBERAL,
        self::JOB_STATUS_SALARIED
    ];

    const DIPLOMA_LICENCE = 401;
    const DIPLOMA_THESIS = 402;
    const DIPLOMA = [
        self::DIPLOMA_LICENCE,
        self::DIPLOMA_THESIS
    ];

    const LABEL = [
        self::IN_ACTIVITY => "En activité",
        self::NO_ACTIVITY => "N'est pas en activité",
        self::DECEASED => "Décédé",
        self::GENDER_MALE => "Homme",
        self::GENDER_FEMALE => "Femme",
        self::GENDER_OTHER => "Indéfini",
        self::JOB_STATUS_LIBERAL => "Libéral",
        self::JOB_STATUS_SALARIED => "Salarié",
        self::DIPLOMA_LICENCE => "Licence",
        self::DIPLOMA_THESIS => "Thèsé"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff", inversedBy="clientPortfolio" , fetch="EXTRA_LAZY")
     */
    private $advisor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $websiteId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonLegal", mappedBy="personNaturals", cascade={"persist"} , fetch="EXTRA_LAZY")
     */
    private $personLegals;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birthDate;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\MedicalSpecialty", inversedBy="specialtyPersonNaturals" , fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="person_natural_medical_specialty_specialty")
     */
    private $specialty;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\MedicalSpecialty", inversedBy="skillPersonNaturals" , fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="person_natural_medical_specialty_skill")
     */
    private $skills;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\MedicalSpecialty", inversedBy="formationPersonNaturals" , fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="person_natural_medical_specialty_formation")
     */
    private $formations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\MedicalSpecialty", inversedBy="orientationPersonNaturals" , fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="person_natural_medical_specialty_orientation")
     */
    private $orientations;

    /**
     * @ORM\Column(type="string", length=11, nullable=true )
     * @Assert\Length(max=11,min=11)
     * @Assert\Regex(
     *     pattern     = "/[0-9]{11}/",
     *     htmlPattern = "[0-9]{11}",
     *     message     = "Le numéro RPPS doit contenir 11 caractères numériques"
     * )
     */
    private $RPPSNumber;

    /**
     * @ORM\Column(type="json")
     */
    private $jobStatus = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $diploma;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min="0",max = 99, minMessage="L'année d'expérience ne peut être inférieur à 0",maxMessage = "Le nombre d'année d'expérience ne peut être supérieur à 99")\
     */
    private $yearsExperience;

    /**
     * @ORM\ManyToMany(targetEntity = "App\Entity\Offer\Offer", mappedBy="contractors", cascade={"persist"} , fetch="EXTRA_LAZY")
     */
    private $providedOffers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\MedicalSpecialty", inversedBy="capacityPersonNaturals" , fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="person_natural_medical_specialty_capacity")
     */
    private $capacities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\ContactInstitution", mappedBy="personNatural" , fetch="EXTRA_LAZY")
     */
    private $contactInstitutions;

    /**
     * PersonNatural constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->personLegals = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->formations = new ArrayCollection();
        $this->orientations = new ArrayCollection();
        $this->providedOffers = new ArrayCollection();
        $this->capacities = new ArrayCollection();
        $this->contactInstitutions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getGender(): ?int
    {
        return $this->gender;
    }

    /**
     * @param int|null $gender
     * @return PersonNatural
     * @throws \Exception
     */
    public function setGender(?int $gender): self
    {
        if (!in_array($gender, self::GENDER)) {
            throw new \Exception("This gender hasn't in the GENDER array");
        }
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getAdvisor(): ?Staff
    {
        return $this->advisor;
    }

    /**
     * @param Staff|null $staff
     * @return $this
     */
    public function setAdvisor(?Staff $staff): self
    {
        $this->advisor = $staff;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWebsiteId(): ?int
    {
        return $this->websiteId;
    }

    /**
     * @param int|null $websiteId
     * @return PersonNatural
     */
    public function setWebsiteId(?int $websiteId): self
    {
        $this->websiteId = $websiteId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString(): string
    {
        if (!empty($this->getFirstName()) && !empty($this->getLastName())) {
            $result = $this->getLastName() . ' ' . $this->getFirstName();
        } else {
            $result = $this->getId();
        }
        return $result ?? '';
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return ucfirst(mb_strtolower($this->firstName));
    }

    /**
     * @param string|null $firstName
     * @return PersonNatural
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = ucfirst($firstName);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return PHPHelper::strToUpperNoAccent($this->lastName);
    }

    /**
     * @param string|null $lastName
     * @return PersonNatural
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = PHPHelper::strToUpperNoAccent($lastName);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|PersonLegal[]
     */
    public function getPersonLegals(): Collection
    {
        return $this->personLegals;
    }

    /**
     * @param PersonLegal $personLegal
     * @return PersonNatural
     */
    public function addPersonLegal(PersonLegal $personLegal): self
    {
        if (!$this->personLegals->contains($personLegal)) {
            $this->personLegals[] = $personLegal;
            $personLegal->addPersonNatural($this);
        }

        return $this;
    }

    /**
     * @param PersonLegal $personLegal
     * @return PersonNatural
     */
    public function removePersonLegal(PersonLegal $personLegal): self
    {
        if ($this->personLegals->contains($personLegal)) {
            $this->personLegals->removeElement($personLegal);
            $personLegal->removePersonNatural($this);
        }

        return $this;
    }

    /**
     * @return MedicalSpecialty|null
     */
    public function getSpecialty(): ?MedicalSpecialty
    {
        return $this->specialty;
    }

    /**
     * @param MedicalSpecialty|null $specialty
     * @return PersonNatural
     * @throws \InvalidArgumentException
     */
    public function setSpecialty(?MedicalSpecialty $specialty): self
    {
        if ($specialty == null) {
            return $this;
        }

        $isValidType = false;
        foreach ($specialty->getRelatedType() as $medicalSpecialtyQualification) {
            if ($medicalSpecialtyQualification->getLabel() == 'Spécialité') {
                $isValidType = true;
            }
        }

        if (!$isValidType) {
            throw new \InvalidArgumentException("This specialty hasn't the 'Spécialité' qualification");
        }

        $this->specialty = $specialty;

        return $this;
    }

    /**
     * @return Collection|MedicalSpecialty[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    /**
     * @param MedicalSpecialty $skill
     * @return PersonNatural
     * @throws \InvalidArgumentException
     */
    public function addSkill(MedicalSpecialty $skill): self
    {
        $isValidType = false;
        foreach ($skill->getRelatedType() as $medicalSpecialtyQualification) {
            if ($medicalSpecialtyQualification->getLabel() == 'Compétence') {
                $isValidType = true;
            }
        }

        if (!$isValidType) {
            throw new \InvalidArgumentException("This specialty hasn't the 'Compétence' qualification");
        }

        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
        }

        return $this;
    }

    /**
     * @param MedicalSpecialty $skill
     * @return PersonNatural
     */
    public function removeSkill(MedicalSpecialty $skill): self
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
        }
        return $this;
    }

    /**
     * @return Collection|MedicalSpecialty[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    /**
     * @param MedicalSpecialty $formation
     * @return PersonNatural
     * @throws \InvalidArgumentException
     */
    public function addFormation(MedicalSpecialty $formation): self
    {
        $isValidType = false;
        foreach ($formation->getRelatedType() as $medicalSpecialtyQualification) {
            if ($medicalSpecialtyQualification->getLabel() == 'Formation') {
                $isValidType = true;
            }
        }

        if (!$isValidType) {
            throw new \InvalidArgumentException("This specialty hasn't the 'Formation' qualification");
        }

        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
        }
        return $this;
    }

    /**
     * @param MedicalSpecialty $formation
     * @return PersonNatural
     */
    public function removeFormation(MedicalSpecialty $formation): self
    {
        if ($this->formations->contains($formation)) {
            $this->formations->removeElement($formation);
        }
        return $this;
    }

    /**
     * @return Collection|MedicalSpecialty[]
     */
    public function getOrientations(): Collection
    {
        return $this->orientations;
    }

    /**
     * @param MedicalSpecialty $orientation
     * @return PersonNatural
     * @throws \InvalidArgumentException
     */
    public function addOrientation(MedicalSpecialty $orientation): self
    {
        $isValidType = false;
        foreach ($orientation->getRelatedType() as $medicalSpecialtyQualification) {
            if ($medicalSpecialtyQualification->getLabel() == 'Expérience') {
                $isValidType = true;
            }
        }

        if (!$isValidType) {
            throw new \InvalidArgumentException("This specialty hasn't the 'Expérience' qualification");
        }

        if (!$this->orientations->contains($orientation)) {
            $this->orientations[] = $orientation;
        }
        return $this;
    }

    /**
     * @param MedicalSpecialty $orientation
     * @return PersonNatural
     */
    public function removeOrientation(MedicalSpecialty $orientation): self
    {
        if ($this->orientations->contains($orientation)) {
            $this->orientations->removeElement($orientation);
        }
        return $this;
    }

    /**
     * @return int|null
     * @throws \Exception
     */
    public function getAge(): ?int
    {
        if ($this->getBirthDate() == null) {
            return null;
        }
        $dateInterval = $this->birthDate->diff(new \DateTime());
        return $dateInterval->y;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTimeInterface|null $birthDate
     * @return PersonNatural
     */
    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getProvidedOffers(): Collection
    {
        return $this->providedOffers;
    }

    /**
     * @param Offer $offer
     * @return PersonNatural
     */
    public function addProvidedOffer(Offer $offer): self
    {
        if (!$this->providedOffers->contains($offer)) {
            $this->providedOffers[] = $offer;
            $offer->addContractor($this);
        }

        return $this;
    }

    /**
     * @param Offer $offer
     * @return PersonNatural
     */
    public function removeProvidedOffer(Offer $offer): self
    {
        if ($this->providedOffers->contains($offer)) {
            $this->providedOffers->removeElement($offer);
            $offer->removeContractor($this);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getRPPSNumber(): ?int
    {
        return $this->RPPSNumber;
    }

    /**
     * @param int|null $RPPSNumber
     * @return PersonNatural
     */
    public function setRPPSNumber(?int $RPPSNumber): self
    {
        $this->RPPSNumber = $RPPSNumber;

        return $this;
    }

    /**
     * @return array
     */
    public function getJobStatus(): array
    {
        return $this->jobStatus;
    }

    /**
     * @param array $jobStatus
     * @return PersonNatural
     * @throws \Exception
     */
    public function setJobStatus(array $jobStatus): self
    {
        foreach ($jobStatus as $oneJobStatus) {
            if (!in_array($oneJobStatus, self::JOB_STATUS) && $oneJobStatus != null) {
                throw new \Exception("The jobsSatus '" . $oneJobStatus . "' could not be found in the JOB_STATUS array");
            }
        }
        $this->jobStatus = $jobStatus;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDiploma(): ?int
    {
        return $this->diploma;
    }

    /**
     * @param int|null $diploma
     * @return PersonNatural
     * @throws \Exception
     */
    public function setDiploma(?int $diploma): self
    {
        if (!in_array($diploma, self::DIPLOMA) && $diploma != null) {
            throw new \Exception("The diploma '" . $diploma . "' could not be found in the DIPLOMA array");
        }
        $this->diploma = $diploma;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getYearsExperience(): ?int
    {
        return $this->yearsExperience;
    }

    /**
     * @param int|null $yearsExperience
     * @return PersonNatural
     */
    public function setYearsExperience(?int $yearsExperience): self
    {
        $this->yearsExperience = $yearsExperience;

        return $this;
    }

    /**
     * @return Collection|MedicalSpecialty[]
     */
    public function getCapacities(): Collection
    {
        return $this->capacities;
    }

    /**
     * @param MedicalSpecialty $capacity
     * @return PersonNatural
     */
    public function addCapacity(MedicalSpecialty $capacity): self
    {
        $isValidType = false;
        foreach ($capacity->getRelatedType() as $medicalSpecialtyQualification) {
            if ($medicalSpecialtyQualification->getLabel() == 'Capacité') {
                $isValidType = true;
            }
        }

        if (!$isValidType) {
            throw new \InvalidArgumentException("This specialty hasn't the 'Capacité' qualification");
        }

        if (!$this->capacities->contains($capacity)) {
            $this->capacities[] = $capacity;
        }

        return $this;
    }

    /**
     * @param MedicalSpecialty $capacity
     * @return PersonNatural
     */
    public function removeCapacity(MedicalSpecialty $capacity): self
    {
        if ($this->capacities->contains($capacity)) {
            $this->capacities->removeElement($capacity);
        }

        return $this;
    }

    /**
     * @return Collection|ContactInstitution[]
     */
    public function getContactInstitutions(): Collection
    {
        return $this->contactInstitutions;
    }

    /**
     * @param ContactInstitution $contactInstitution
     * @return $this
     */
    public function addContactInstitution(ContactInstitution $contactInstitution): self
    {
        if (!$this->contactInstitutions->contains($contactInstitution)) {
            $this->contactInstitutions[] = $contactInstitution;
            $contactInstitution->setPersonNatural($this);
        }

        return $this;
    }

    /**
     * @param ContactInstitution $contactInstitution
     * @return $this
     */
    public function removeContactInstitution(ContactInstitution $contactInstitution): self
    {
        if ($this->contactInstitutions->contains($contactInstitution)) {
            $this->contactInstitutions->removeElement($contactInstitution);
            // set the owning side to null (unless already changed)
            if ($contactInstitution->getPersonNatural() === $this) {
                $contactInstitution->setPersonNatural(null);
            }
        }

        return $this;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'specialty' => $this->getSpecialty()->getOfficialLabel(),
            'mail' => $this->getDefaultMail()->getAddress(),
            'address' => $this->getDefaultAddress() !== null ? $this->getDefaultAddress()->getReadableAddress() : '',
            'jobName' => $this->getMainJobName()->getLabel(),
            'gender' => $this->getGender(),
            'advisor' => $this->getAdvisor()->getNickname(),
            'diploma' => $this->getDiploma(),
            'age' => $this->getAge(),
            'telephones' => $this->getTelephones()->count() > 0 ? $this->getTelephones()[0]->getNumber() : '',
            'origin' => $this->getOrigin()->getLabel(),
            'creator' => $this->getCreator()->getNickname(),
            'agenda' => $this->getAgenda()->getId(),
            'websiteId' => $this->getWebsiteId(),
            'rpps' => $this->getRPPSNumber(),
            'birthdate' => $this->getBirthDate()
        ];
    }
}