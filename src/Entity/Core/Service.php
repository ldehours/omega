<?php

namespace App\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\ServiceRepository")
 */
class Service
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $codification;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Enrolment", mappedBy="relatedService")
     */
    private $enrolments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Staff", mappedBy="service")
     */
    private $staff;

    /**
     * Service constructor.
     */
    public function __construct()
    {
        $this->enrolments = new ArrayCollection();
        $this->staff = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Service
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCodification(): ?string
    {
        return $this->codification;
    }

    /**
     * @param string $codification
     * @return Service
     */
    public function setCodification(string $codification): self
    {
        $this->codification = $codification;

        return $this;
    }

    /**
     * @return Collection|Enrolment[]
     */
    public function getEnrolments(): Collection
    {
        return $this->enrolments;
    }

    /**
     * @param Enrolment $enrolment
     * @return Service
     */
    public function addEnrolment(Enrolment $enrolment): self
    {
        if (!$this->enrolments->contains($enrolment)) {
            $this->enrolments[] = $enrolment;
            $enrolment->setRelatedService($this);
        }

        return $this;
    }

    /**
     * @param Enrolment $enrolment
     * @return Service
     */
    public function removeEnrolment(Enrolment $enrolment): self
    {
        if ($this->enrolments->contains($enrolment)) {
            $this->enrolments->removeElement($enrolment);
            // set the owning side to null (unless already changed)
            if ($enrolment->getRelatedService() === $this) {
                $enrolment->setRelatedService(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return Collection|Staff[]
     */
    public function getStaff(): Collection
    {
        return $this->staff;
    }

    public function addStaff(Staff $staff): self
    {
        if (!$this->staff->contains($staff)) {
            $this->staff[] = $staff;
            $staff->setService($this);
        }

        return $this;
    }

    public function removeStaff(Staff $staff): self
    {
        if ($this->staff->contains($staff)) {
            $this->staff->removeElement($staff);
            // set the owning side to null (unless already changed)
            if ($staff->getService() === $this) {
                $staff->setService(null);
            }
        }

        return $this;
    }
}
