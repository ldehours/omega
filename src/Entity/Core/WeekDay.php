<?php

namespace App\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\WeekDayRepository")
 */
class WeekDay
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * /!\ la valeur est un nombre qui, une fois converti en masque binaire sur 14 bytes, représente
     * 7 paires de matin et après midi, les paires représentent les jours de la semaine du lundi au dimanche/!\
     *
     * Ex : 1027 correspond à lundi toute la journée et le samedi matin
     * @ORM\Column(type="integer", length=5)
     * @Assert\Range(min=0, max=16383)
     */
    private $value;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return WeekDay
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }
}
