<?php

namespace App\Entity\Core;

use App\Entity\Offer\Offer;
use App\Traits\TranslatableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\NoteRepository")
 */
class Note
{
    use TranslatableTrait;

    const TYPE_CONFIDENTIAL = 1;
    const TYPE_DIFFUSION = 2;
    const TYPE_INFORMATION = 3;

    const TYPES = array(
        self::TYPE_CONFIDENTIAL,
        self::TYPE_DIFFUSION,
        self::TYPE_INFORMATION
    );

    const LABEL = array(
        self::TYPE_CONFIDENTIAL => 'Confidentielle',
        self::TYPE_DIFFUSION => 'Diffusion',
        self::TYPE_INFORMATION => 'Information'
    );

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $noteType;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Offer", inversedBy="notes")
     */
    private $offer;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getNoteType(): ?int
    {
        return $this->noteType;
    }

    /**
     * @param int $noteType
     * @return Note
     */
    public function setNoteType(int $noteType): self
    {
        if (!in_array($noteType, self::TYPES)) {
            throw new \InvalidArgumentException('$noteType must be defined in TYPES array');
        }
        $this->noteType = $noteType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text ?? '';
    }

    /**
     * @param string|null $text
     * @return Note
     */
    public function setText(?string $text): self
    {
        $this->text = ($text ?? '');

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getCreator(): ?Staff
    {
        return $this->creator;
    }

    /**
     * @param Staff|null $creator
     * @return Note
     */
    public function setCreator(?Staff $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Offer|null
     */
    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer|null $offer
     * @return Note
     */
    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->id;
    }
}
