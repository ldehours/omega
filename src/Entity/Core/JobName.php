<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\JobNameRepository")
 */
class JobName
{
    use TranslatableTrait;

    const BASED = 101;
    const SUBSTITUTE = 102;
    const INSTITUTION = 103;
    const OTHER = 104;
    const INSTITUTION_FUNCTION = 105;
    const CLASSIFICATIONS = array(
        self::BASED,
        self::SUBSTITUTE,
        self::INSTITUTION,
        self::OTHER,
        self::INSTITUTION_FUNCTION
    );

    const LABEL = array(
        self::BASED => 'Médecin Installé',
        self::SUBSTITUTE => 'Médecin Remplaçant',
        self::INSTITUTION => 'Etablissement',
        self::OTHER => 'Autre',
        self::INSTITUTION_FUNCTION => 'Contact établissement'
    );

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="integer")
     */
    private $classification;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Person", mappedBy="jobNames")
     */
    private $persons;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonAccess", mappedBy="jobName")
     */
    private $personAccesses;

    /**
     * JobName constructor.
     */
    public function __construct()
    {
        $this->persons = new ArrayCollection();
        $this->personAccesses = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return JobName
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getClassification(): ?int
    {
        return $this->classification;
    }

    /**
     * @param int $classification
     * @return JobName
     */
    public function setClassification(int $classification): self
    {
        if (!in_array($classification, self::CLASSIFICATIONS)) {
            throw new InvalidArgumentException('$classification must be defined in CLASSIFICATIONS array [BASED, SUBSTITUTE, OTHER, INSTITUTION_FUNCTION]');
        }
        $this->classification = $classification;
        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    /**
     * @param Person $person
     * @return JobName
     */
    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
            $person->addJobName($this);
        }

        return $this;
    }

    /**
     * @param Person $person
     * @return JobName
     */
    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            $person->removeJobName($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->label;
    }


    /**
     * @return Collection|PersonAccess[]
     */
    public function getPersonAccesses(): Collection
    {
        return $this->personAccesses;
    }

    /**
     * @param Collection|PersonAccess[]
     * @return JobName
     */
    public function setPersonAccess(array $personAccess): self
    {
        foreach ($personAccess as $access) {
            $this->addPersonAccess($access);
        }
        return $this;
    }

    /**
     * @param PersonAccess $personAccess
     * @return JobName
     */
    public function addPersonAccess(PersonAccess $personAccess): self
    {
        if (!$this->personAccesses->contains($personAccess)) {
            $this->personAccesses[] = $personAccess;
            $personAccess->addJobName($this);
        }

        return $this;
    }

    /**
     * @param PersonAccess $personAccess
     * @return JobName
     */
    public function removePersonAccess(PersonAccess $personAccess): self
    {
        if ($this->personAccesses->contains($personAccess)) {
            $this->personAccesses->removeElement($personAccess);
            $personAccess->removeJobName($this);
        }

        return $this;
    }
}
