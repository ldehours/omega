<?php

namespace App\Entity\Core;

use App\Entity\Offer\Wish;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Time;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\TimeRangeRepository")
 */
class TimeRange
{
    CONST NATURE_OTHER = 0;
    CONST NATURE_HOLLIDAY = 1;
    CONST NATURE_SEARCH = 2;
    CONST NATURE_ON_CALL = 3;
    CONST NATURE_NOT_AVAILABLE = 4;
    CONST NATURE_APPOINTMENT_CUSTOMER = 5;
    CONST NATURE_INTERNAL_MEETING = 6;
    CONST NATURE_CALL_ARRIVED = 7;
    CONST NATURE_BILLING_TRACKING = 8;
    CONST NATURE_SUBSTITUTION_PROVIDED_MS = 9;
    CONST NATURE_SUBSTITUTION_ALMOST_PROVIDED_MS = 10;
    CONST NATURE_SUBSTITUTION_HMS = 11;
    CONST NATURE_WAGE_PROVIDED_MS = 12;
    CONST NATURE_WAGE_ALMOST_PROVIDED_MS = 13;
    CONST NATURE_WAGE_HMS = 14;
    CONST NATURE_BIRTHDAY = 15;

    CONST NATURES_PERSON = [
        self::NATURE_HOLLIDAY,
        self::NATURE_SEARCH,
        self::NATURE_ON_CALL,
        self::NATURE_NOT_AVAILABLE
    ];

    CONST NATURES_STAFF = [
        self::NATURE_HOLLIDAY,
        self::NATURE_APPOINTMENT_CUSTOMER,
        self::NATURE_INTERNAL_MEETING,
        self::NATURE_CALL_ARRIVED,
        self::NATURE_BILLING_TRACKING,
        self::NATURE_OTHER
    ];

    CONST NATURES_STAFF_ALARM = [
        self::NATURE_APPOINTMENT_CUSTOMER,
        self::NATURE_CALL_ARRIVED,
        self::NATURE_BILLING_TRACKING
    ];

    CONST NATURES_COLOR = [
        self::NATURE_HOLLIDAY => '#FFFF00',
        self::NATURE_SEARCH => '#3ECA18',
        self::NATURE_ON_CALL => '#FA8B34',
        self::NATURE_NOT_AVAILABLE => '',
        self::NATURE_APPOINTMENT_CUSTOMER => 'purple',
        self::NATURE_INTERNAL_MEETING => 'orange',
        self::NATURE_CALL_ARRIVED => 'darkblue',
        self::NATURE_BILLING_TRACKING => 'orangered',
        self::NATURE_OTHER => 'darkorchid',
        self::NATURE_SUBSTITUTION_PROVIDED_MS => 'blue',
        self::NATURE_SUBSTITUTION_ALMOST_PROVIDED_MS => 'lightblue',
        self::NATURE_SUBSTITUTION_HMS => 'grey',
        self::NATURE_WAGE_PROVIDED_MS => 'purple',
        self::NATURE_WAGE_ALMOST_PROVIDED_MS => 'violet',
        self::NATURE_WAGE_HMS => 'grey',
        self::NATURE_BIRTHDAY => 'pink'
    ];

    CONST NATURES_COLOR_TEXT = [
        self::NATURE_HOLLIDAY => 'grey',
        self::NATURE_SEARCH => 'white',
        self::NATURE_ON_CALL => 'white',
        self::NATURE_NOT_AVAILABLE => 'white',
        self::NATURE_APPOINTMENT_CUSTOMER => 'white',
        self::NATURE_INTERNAL_MEETING => 'white',
        self::NATURE_CALL_ARRIVED => 'white',
        self::NATURE_BILLING_TRACKING => 'white',
        self::NATURE_OTHER => 'white',
        self::NATURE_SUBSTITUTION_PROVIDED_MS => 'white',
        self::NATURE_SUBSTITUTION_ALMOST_PROVIDED_MS => 'blue',
        self::NATURE_SUBSTITUTION_HMS => 'lightblue',
        self::NATURE_WAGE_PROVIDED_MS => 'white',
        self::NATURE_WAGE_ALMOST_PROVIDED_MS => 'purple',
        self::NATURE_WAGE_HMS => 'violet',
        self::NATURE_BIRTHDAY => 'white'
    ];

    CONST LABEL = [
        self::NATURE_HOLLIDAY => 'Vacances',
        self::NATURE_SEARCH => 'En recherche',
        self::NATURE_ON_CALL => 'En astreinte',
        self::NATURE_NOT_AVAILABLE => 'Non disponible',
        self::NATURE_APPOINTMENT_CUSTOMER => 'Rendez-vous Client',
        self::NATURE_INTERNAL_MEETING => 'Réunion Interne',
        self::NATURE_CALL_ARRIVED => "Appel de 'Bien arrivé'",
        self::NATURE_BILLING_TRACKING => 'Suivi de facturation',
        self::NATURE_OTHER => "Autre",
        self::NATURE_SUBSTITUTION_PROVIDED_MS => 'Remplacement pourvu par MS',
        self::NATURE_SUBSTITUTION_ALMOST_PROVIDED_MS => 'Remplacement quasiment pourvu par MS',
        self::NATURE_SUBSTITUTION_HMS => 'Remplacement HMS',
        self::NATURE_WAGE_PROVIDED_MS => 'Salariat pourvu par MS',
        self::NATURE_WAGE_ALMOST_PROVIDED_MS => 'Salariat quasiment pourvu par MS',
        self::NATURE_WAGE_HMS => 'Salariat HMS',
        self::NATURE_BIRTHDAY => 'Anniversaire'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $nature = self::NATURE_OTHER;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\WeekDay")
     */
    private $weekDay;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Agenda", inversedBy="timeRanges",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $agenda;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="timeRanges")
     */
    private $person;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Offer\Wish", mappedBy="timeRange")
     */
    private $wish;

    public function __construct()
    {
        $this->wishes = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * @param \DateTimeInterface $startDate
     * @return TimeRange
     */
    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @param \DateTimeInterface|null $endDate
     * @return TimeRange
     */
    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getNature(): ?int
    {
        return $this->nature;
    }

    /**
     * @param int $nature
     * @return TimeRange
     */
    public function setNature(int $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getCreator(): ?Staff
    {
        return $this->creator;
    }

    /**
     * @param Staff|null $creator
     * @return TimeRange
     */
    public function setCreator(?Staff $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return WeekDay|null
     */
    public function getWeekDay(): ?WeekDay
    {
        return $this->weekDay;
    }

    /**
     * @param WeekDay|null $weekDay
     * @return TimeRange
     */
    public function setWeekDay(?WeekDay $weekDay): self
    {
        $this->weekDay = $weekDay;

        return $this;
    }

    /**
     * @return Agenda|null
     */
    public function getAgenda(): ?Agenda
    {
        return $this->agenda;
    }

    /**
     * @param Agenda|null $agenda
     * @return TimeRange
     */
    public function setAgenda(?Agenda $agenda): self
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person|null $person
     * @return TimeRange
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return TimeRange
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Wish|null
     */
    public function getWish(): ?Wish
    {
        return $this->wish;
    }

    /**
     * @param Wish|null $wish
     * @return TimeRange
     */
    public function setWish(?Wish $wish): self
    {
        $this->wish = $wish;

        return $this;
    }
}
