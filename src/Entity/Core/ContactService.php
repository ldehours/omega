<?php

namespace App\Entity\Core;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\ContactServiceRepository")
 */
class ContactService
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\ContactInstitution", mappedBy="service")
     */
    private $contactInstitutions;

    public function __construct()
    {
        $this->contactInstitutions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|ContactInstitution[]
     */
    public function getContactInstitutions(): Collection
    {
        return $this->contactInstitutions;
    }

    /**
     * @param ContactInstitution $contactInstitution
     * @return $this
     */
    public function addContactInstitution(ContactInstitution $contactInstitution): self
    {
        if (!$this->contactInstitutions->contains($contactInstitution)) {
            $this->contactInstitutions[] = $contactInstitution;
            $contactInstitution->setService($this);
        }

        return $this;
    }

    /**
     * @param ContactInstitution $contactInstitution
     * @return $this
     */
    public function removeContactInstitution(ContactInstitution $contactInstitution): self
    {
        if ($this->contactInstitutions->contains($contactInstitution)) {
            $this->contactInstitutions->removeElement($contactInstitution);
            // set the owning side to null (unless already changed)
            if ($contactInstitution->getService() === $this) {
                $contactInstitution->setService(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->label;
    }
}
