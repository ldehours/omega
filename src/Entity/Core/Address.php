<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\AddressRepository")
 */
class Address extends \CommerceGuys\Addressing\Address
{
    use TranslatableTrait;

    const OFFICE = 101;
    const HOME = 102;
    const OTHER = 103;
    const DOMICILIATIONS = [
        self::OFFICE,
        self::HOME,
        self::OTHER
    ];

    const TYPEADRESSFORM = [
        self::OFFICE,
        self::BILLING
    ];

    const FISCAL = 201;
    const CORRESPONDENCE = 202;
    const BILLING = 203;
    const USAGES = [
        self::FISCAL,
        self::CORRESPONDENCE,
        self::BILLING
    ];

    const LABEL = [
        self::OFFICE => 'Cabinet/Établissement',
        self::HOME => 'Domicile',
        self::OTHER => 'Autre',
        self::FISCAL => 'Fiscale',
        self::CORRESPONDENCE => 'Correspondance',
        self::BILLING => 'Facturation',
    ];

    const ICON = [
        self::OFFICE => '<i class="fas fa-briefcase-medical" title="Cabinet"></i>',
        self::HOME => '<i class="fas fa-home" title="Domicile"></i>',
        self::FISCAL => '<i class="fas fa-landmark" title="Fiscale"></i>',
        self::CORRESPONDENCE => '<i class="fas fa-address-card" title="Correspondance"></i>',
        self::BILLING => '<i class="tim-icons icon-ic_euro_symbol_24px" title="Facturation"></i>',
    ];

    /**
     * @ORM\Column(type="string", length=2)
     */
    protected $countryCode;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $administrativeArea;
    /**
     * @ORM\Column(type="string")
     */
    protected $locality;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dependentLocality;
    /**
     * @ORM\Column(type="string")
     */
    protected $postalCode;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sortingCode;
    /**
     * @ORM\Column(type="string")
     */
    protected $addressLine1;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $addressLine2;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $locale;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isCedex = false;
    /**
     * @ORM\Column(type="array")
     */
    private $domiciliations = [];
    /**
     * @ORM\Column(type="array")
     */
    private $usages = [];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Person", mappedBy="addresses")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Department", inversedBy="addresses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $department;

    public function __construct()
    {
        parent::__construct();
        $this->person = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool|null
     */
    public function getIsCedex(): ?bool
    {
        return $this->isCedex;
    }

    /**
     * @param bool $isCedex
     * @return Address
     */
    public function setIsCedex(bool $isCedex): self
    {
        $this->isCedex = $isCedex;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getDomiciliations(): ?array
    {
        return $this->domiciliations;
    }

    /**
     * @param array $domiciliations
     * @return Address
     */
    public function setDomiciliations(array $domiciliations): self
    {
        foreach ($domiciliations as $domiciliation) {
            if (!in_array($domiciliation, self::DOMICILIATIONS)) {
                throw new \InvalidArgumentException(
                    'Address->setDomiciliations() : $domiciliations parameter must contain elements from DOMICILIATIONS array'
                );
            }
        }

        $this->domiciliations = $domiciliations;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getUsages(): ?array
    {
        return $this->usages;
    }

    /**
     * @param array $usages
     * @return Address
     */
    public function setUsages(array $usages): self
    {
        foreach ($usages as $usage) {
            if (!in_array($usage, self::USAGES)) {
                throw new \InvalidArgumentException('Address->setUsages() : $usages parameter must contain elements from USAGES array');
            }
        }

        $this->usages = $usages;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return mixed
     */
    public function getAdministrativeArea()
    {
        return $this->administrativeArea;
    }

    /**
     * @param mixed $administrativeArea
     */
    public function setAdministrativeArea($administrativeArea): void
    {
        $this->administrativeArea = $administrativeArea;
    }

    /**
     * @return mixed
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * @param mixed $locality
     */
    public function setLocality($locality): void
    {
        $this->locality = $locality;
    }

    /**
     * @return mixed
     */
    public function getDependentLocality()
    {
        return $this->dependentLocality;
    }

    /**
     * @param mixed $dependentLocality
     */
    public function setDependentLocality($dependentLocality): void
    {
        $this->dependentLocality = $dependentLocality;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @param mixed $addressLine1
     */
    public function setAddressLine1($addressLine1): void
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @param mixed $addressLine2
     */
    public function setAddressLine2($addressLine2): void
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @return mixed
     */
    public function getSortingCode()
    {
        return $this->sortingCode;
    }

    /**
     * @param mixed $sortingCode
     */
    public function setSortingCode($sortingCode): void
    {
        $this->sortingCode = $sortingCode;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPerson(): Collection
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return Address
     */
    public function addPerson(Person $person): self
    {
        if (!$this->person->contains($person)) {
            $this->person[] = $person;
        }

        return $this;
    }

    /**
     * @param Person $person
     * @return Address
     */
    public function removePerson(Person $person): self
    {
        if ($this->person->contains($person)) {
            $this->person->removeElement($person);
        }

        return $this;
    }

    /**
     * @return Department|null
     */
    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    /**
     * @param Department|null $department
     * @return $this
     */
    public function setDepartment(?Department $department): self
    {
        $this->department = $department;
        if ($department !== null) {
            $department->addAddress($this);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getReadableAddress(): string
    {
        $readableString = $this->addressLine1;

        if ($this->addressLine2 !== null || $this->addressLine2 !== '') {
            $readableString .= ' ' . $this->addressLine2;
        }

        if ($this->locality !== null || $this->locality !== '') {
            $readableString .= ', ' . $this->locality;
        }

        if ($this->postalCode !== null || $this->postalCode !== '') {
            $readableString .= ' ' . $this->postalCode;
        }
        return $readableString;
    }
}
