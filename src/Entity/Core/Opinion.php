<?php

namespace App\Entity\Core;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\OpinionRepository")
 */
class Opinion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="opinionsGiven")
     * @ORM\JoinColumn(nullable=false)
     */
    private $giver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="opinionsReceived")
     * @ORM\JoinColumn(nullable=false)
     */
    private $receiver;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getGiver(): ?Person
    {
        return $this->giver;
    }

    public function setGiver(?Person $giver): self
    {
        $this->giver = $giver;

        return $this;
    }

    public function getReceiver(): ?Person
    {
        return $this->receiver;
    }

    public function setReceiver(?Person $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }
}
