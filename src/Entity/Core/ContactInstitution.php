<?php

namespace App\Entity\Core;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\ContactInstitutionRepository")
 */
class ContactInstitution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * C'est l'intitulé custom pour un contact genre "Monsieur le Duc de la clinique"
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\PersonNatural", inversedBy="contactInstitutions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $personNatural;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\PersonLegal", inversedBy="contactInstitutions")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $personLegal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\JobName")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $function;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\ContactService", inversedBy="contactInstitutions")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $service;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return PersonNatural|null
     */
    public function getPersonNatural(): ?PersonNatural
    {
        return $this->personNatural;
    }

    /**
     * @param PersonNatural|null $personNatural
     * @return $this
     */
    public function setPersonNatural(?PersonNatural $personNatural): self
    {
        $this->personNatural = $personNatural;

        return $this;
    }

    /**
     * @return PersonLegal|null
     */
    public function getPersonLegal(): ?PersonLegal
    {
        return $this->personLegal;
    }

    /**
     * @param PersonLegal|null $personLegal
     * @return $this
     */
    public function setPersonLegal(?PersonLegal $personLegal): self
    {
        $this->personLegal = $personLegal;

        return $this;
    }

    /**
     * @return JobName|null
     */
    public function getFunction(): ?JobName
    {
        return $this->function;
    }

    /**
     * @param JobName|null $function
     * @return $this
     */
    public function setFunction(?JobName $function): self
    {
        $this->function = $function;

        return $this;
    }

    /**
     * @return ContactService|null
     */
    public function getService(): ?ContactService
    {
        return $this->service;
    }

    /**
     * @param ContactService|null $service
     * @return $this
     */
    public function setService(?ContactService $service): self
    {
        $this->service = $service;

        return $this;
    }
}
