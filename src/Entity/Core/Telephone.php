<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use App\Utils\TelephoneHelper;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\TelephoneRepository")
 */
class Telephone
{
    use TranslatableTrait;

    const WORK = 1;
    const HOME = 2;
    const FAX = 3;
    const CELL = 4;
    const TYPES = array(
        self::WORK,
        self::HOME,
        self::FAX,
        self::CELL
    );

    const LABEL = array(
        self::WORK => 'Professionnel',
        self::HOME => 'Personnel',
        self::FAX => 'Fax',
        self::CELL => 'Mobile',

    );

    const ICON = array(
        self::WORK => '<i class=\'fas fa-briefcase-medical\' title="Professionnel"></i>',
        self::HOME => '<i class=\'fas fa-home\' title="Personnel"></i>',
        self::FAX => '<i class=\'fas fa-fax\' title="Fax"></i>',
        self::CELL => '<i class=\'fas fa-mobile-alt\' title="Mobile"></i>',

    );
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $phoneType;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank
     * @Assert\Regex("/[0-9]{10,20}/")
     */
    private $number;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Person",cascade={"persist"}, mappedBy="telephones")
     */
    private $persons;

    public function __construct($telephoneNumber = null)
    {
        if($telephoneNumber !== null){
            $this->setNumber($telephoneNumber);
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getPhoneType(): ?int
    {
        return $this->phoneType;
    }

    /**
     * @param int $phoneType
     * @return Telephone
     */
    public function setPhoneType(int $phoneType): self
    {
        if (!in_array($phoneType, self::TYPES)) {
            throw new \InvalidArgumentException('$phoneType must be defined in TYPES array');
        }
        $this->phoneType = $phoneType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Telephone
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    /**
     * @param mixed $persons
     */
    public function setPersons($persons): void
    {
        $this->persons = $persons;
    }
}
