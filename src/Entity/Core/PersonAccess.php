<?php

namespace App\Entity\Core;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Core\PersonAccessRepository")
 */
class PersonAccess
{
    use TranslatableTrait;

    const DEPOSIT_LIBERAL_ANNOUNCE = 101;
    const DEPOSIT_SALARIED_ANNOUNCE = 102;
    const DEPOSIT_TO_CESSION = 103;
    const CANDIDATE_TO_LIBERAL_ANNOUNCE = 201;
    const CANDIDATE_TO_SALARIED_ANNOUNCE = 202;
    const CANDIDATE_TO_CESSION = 203;
    const SUBSCRIBE_TO_CFML = 301;
    const SUBSCRIBE_TO_LMS = 302;
    const SUBSCRIBE_TO_BNC = 303;
    const SUBSCRIBE_TO_CONTRACT_RELAY = 304;
    const SUBSCRIBE_TO_CONTRACT_PRIVILEGE = 305;
    const ACCESSES = [
        self::DEPOSIT_LIBERAL_ANNOUNCE,
        self::DEPOSIT_SALARIED_ANNOUNCE,
        self::DEPOSIT_TO_CESSION,
        self::CANDIDATE_TO_LIBERAL_ANNOUNCE,
        self::CANDIDATE_TO_SALARIED_ANNOUNCE,
        self::CANDIDATE_TO_CESSION,
        self::SUBSCRIBE_TO_CFML,
        self::SUBSCRIBE_TO_LMS,
        self::SUBSCRIBE_TO_BNC,
        self::SUBSCRIBE_TO_CONTRACT_RELAY,
        self::SUBSCRIBE_TO_CONTRACT_PRIVILEGE
    ];
    const LABEL = [
        self::DEPOSIT_LIBERAL_ANNOUNCE => 'Dépôt annonce libérale',
        self::DEPOSIT_SALARIED_ANNOUNCE => 'Dépôt annonce salariat',
        self::DEPOSIT_TO_CESSION => 'Dépôt annonce cession association et location',
        self::CANDIDATE_TO_LIBERAL_ANNOUNCE => 'Candidature à une annonce libérale',
        self::CANDIDATE_TO_SALARIED_ANNOUNCE => 'Candidature à une annonce salariat',
        self::CANDIDATE_TO_CESSION => 'Candidature à une annonce cession',
        self::SUBSCRIBE_TO_CFML => 'Adhésion au CFML',
        self::SUBSCRIBE_TO_LMS => 'Adhésion à la LMS',
        self::SUBSCRIBE_TO_BNC => 'Adhésion à BNC Conseil',
        self::SUBSCRIBE_TO_CONTRACT_RELAY => 'Adhésion au contrat relais',
        self::SUBSCRIBE_TO_CONTRACT_PRIVILEGE => 'Adhésion au contrat privilège'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\JobName", inversedBy="personAccesses")
     */
    private $jobName;

    /**
     * PersonAccess constructor.
     */
    public function __construct()
    {
        $this->jobName = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return PersonAccess
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return PersonAccess
     * @throw InvalidArgumentException
     */
    public function setType(int $type): self
    {
        if (!in_array($type, self::ACCESSES)) {
            throw new InvalidArgumentException($type . ' must be defined in ACCESS array [
       DEPOSIT_LIBERAL_ANNOUNCE,
       DEPOSIT_SALARIED_ANNOUNCE,
       DEPOSIT_TO_CESSION,
       CANDIDATE_TO_LIBERAL_ANNOUNCE,
       CANDIDATE_TO_SALARIED_ANNOUNCE,
       CANDIDATE_TO_CESSION,
       SUBSCRIBE_TO_CFML,
       SUBSCRIBE_TO_LMS,
       SUBSCRIBE_TO_BNC,
       SUBSCRIBE_TO_CONTRACT_RELAY,
       SUBSCRIBE_TO_CONTRACT_PRIVILEGE
    ]');
        }
        $this->type = $type;
        return $this;
    }

    /**
     * @return Collection|JobName[]
     */
    public function getJobName(): Collection
    {
        return $this->jobName;
    }

    /**
     * @param JobName $jobName
     * @return PersonAccess
     */
    public function addJobName(JobName $jobName): self
    {
        if (!$this->jobName->contains($jobName)) {
            $this->jobName[] = $jobName;
        }

        return $this;
    }

    /**
     * @param JobName $jobName
     * @return PersonAccess
     */
    public function removeJobName(JobName $jobName): self
    {
        if ($this->jobName->contains($jobName)) {
            $this->jobName->removeElement($jobName);
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getLabel();
    }
}
