<?php


namespace App\Entity\Website;

use App\Utils\DateTime;
use Symfony\Component\Validator\Constraints\Date;

class User
{
    /**
     * @var integer $id
     * id website account
     */
    private $id;

    /**
     * @var DateTime $datecrea
     * User's creation date
     */
    private $datecrea;

    /**
     * @var string $login
     * mail address used to login
     */
    private $login;

    /**
     * @var string $pass
     * password not hashed
     */
    private $pass;

    /**
     * @var DateTime $cus_envoi
     * Date when the End User Licence Agreement were send
     */
    private $cus_envoi;

    /**
     * @var string $statut
     * possible values: R - Rempla | I - Installé | E - établissement | A - autres | N - Nouveau | S - Salarié | X - Spéciale
     */
    private $statut;

    /**
     * @var integer $cle_nantu
     * Reference key to nantu's application
     */
    private $cle_nantu;

    /**
     * @var datetime $ut
     * Used for automated process (maybe more). "ut" = update time
     */
    private $ut;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return DateTime
     * @throws \Exception
     */
    public function getDatecrea(): DateTime
    {
        if ($this->datecrea == null) {
            $this->setDatecrea(new DateTime('now'));
        }

        return $this->datecrea;
    }

    /**
     * @param DateTime $datecrea
     * @return User
     */
    public function setDatecrea(DateTime $datecrea): self
    {
        $this->datecrea = $datecrea;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return User
     */
    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     * @return User
     */
    public function setPass(string $pass): self
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCus_envoi(): ?DateTime
    {
        return $this->cus_envoi;
    }

    /**
     * @param DateTime|null $cus_envoi
     * @return User
     */
    public function setCus_envoi(?DateTime $cus_envoi): self
    {
        $this->cus_envoi = $cus_envoi;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatut(): string
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     * @return User
     * @throws \Exception
     */
    public function setStatut(string $statut): self
    {
        if (strlen($statut) > 2) {
            throw new \Exception("You can't set a statut over 2 characters");
        }

        $this->statut = $statut;

        return $this;
    }

    /**
     * @return int
     */
    public function getCle_nantu(): int
    {
        if ($this->cle_nantu == null) {
            $this->setCle_nantu(0);
        }

        return $this->cle_nantu;
    }

    /**
     * @param int $cle_nantu
     * @return User
     */
    public function setCle_nantu(int $cle_nantu): self
    {
        $this->cle_nantu = $cle_nantu;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUt(): DateTime
    {
        return $this->ut;
    }

    /**
     * @param DateTime $ut
     * @return User
     */
    public function setUt($ut): self
    {
        $this->ut = $ut;

        return $this;
    }
}