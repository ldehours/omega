<?php


namespace App\Entity\Website;


use App\Utils\DateTime;

class Coord
{

    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $nom
     */
    private $nom;

    /**
     * @var string $prenom
     */
    private $prenom;

    /**
     * @var string $adresse
     */
    private $adresse;

    /**
     * @var string $ad2
     * Second line of an address
     */
    private $ad2;

    /**
     * @var string $ad3
     * Third line of an address
     */
    private $ad3;

    /**
     * @var string $cp
     * Postal code
     */
    private $cp;

    /**
     * @var string $ville
     */
    private $ville;

    /**
     * @var string $tel
     * Phone number
     */
    private $tel;

    /**
     * @var string $port
     * Mobile phone number
     */
    private $port;

    /**
     * @var string $fax
     * Fax phone number
     */
    private $fax;

    /**
     * @var string $lic
     * Licence number
     */
    private $lic;

    /**
     * @var string $rpps
     * person's Id in the medical order
     */
    private $rpps;

    /**
     * @var string $odm
     * Inscription to the medical order
     */
    private $odm;

    /**
     * @var int $annee_naissance
     */
    private $annee_naissance;

    /**
     * @var DateTime $ut
     * update time, date of the last update of the row
     */
    private $ut;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Coord
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Coord
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     * @return Coord
     */
    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     * @return Coord
     */
    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return string
     */
    public function getAd2(): string
    {
        if ($this->ad2 == null) {
            $this->setAd2('');
        }
        return $this->ad2;
    }

    /**
     * @param string $ad2
     * @return Coord
     */
    public function setAd2(string $ad2): self
    {
        $this->ad2 = $ad2;

        return $this;
    }

    /**
     * @return string
     */
    public function getAd3(): string
    {
        if ($this->ad3 == null) {
            $this->setAd3('');
        }
        return $this->ad3;
    }

    /**
     * @param string $ad3
     * @return Coord
     */
    public function setAd3(string $ad3): self
    {
        $this->ad3 = $ad3;

        return $this;
    }

    /**
     * @return string
     */
    public function getCp(): string
    {
        return $this->cp;
    }

    /**
     * @param string $cp
     * @return Coord
     * @throws \Exception
     */
    public function setCp(string $cp): self
    {
        if (strlen($cp) > 5) {
            throw new \Exception("You can't set a cp over 5 characters");
        }
        $this->cp = $cp;

        return $this;
    }

    /**
     * @return string
     */
    public function getVille(): string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     * @return Coord
     */
    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return string
     */
    public function getTel(): string
    {
        if ($this->tel == null) {
            $this->setTel('');
        }
        return $this->tel;
    }

    /**
     * @param string $tel
     * @return Coord
     */
    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        if ($this->port == null) {
            $this->setPort('');
        }
        return $this->port;
    }

    /**
     * @param string $port
     * @return Coord
     */
    public function setPort(string $port): self
    {
        $this->port = $port;

        return $this;
    }

    /**
     * @return string
     */
    public function getFax(): string
    {
        if ($this->fax == null) {
            $this->setFax('');
        }
        return $this->fax;
    }

    /**
     * @param string $fax
     * @return Coord
     */
    public function setFax(string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getLic(): string
    {
        if ($this->lic == null) {
            $this->setLic('');
        }
        return $this->lic;
    }

    /**
     * @param string $lic
     * @return Coord
     */
    public function setLic(string $lic): self
    {
        $this->lic = $lic;

        return $this;
    }

    /**
     * @return string
     */
    public function getRpps(): string
    {
        if ($this->rpps == null) {
            $this->setRpps('');
        }
        return $this->rpps;
    }

    /**
     * @param string $rpps
     * @return Coord
     */
    public function setRpps(string $rpps): self
    {
        $this->rpps = $rpps;

        return $this;
    }

    /**
     * @return string
     */
    public function getOdm(): string
    {
        if ($this->odm == null) {
            $this->setOdm('');
        }
        return $this->odm;
    }

    /**
     * @param string $odm
     * @return Coord
     */
    public function setOdm(string $odm): self
    {
        $this->odm = $odm;

        return $this;
    }

    /**
     * @return int
     */
    public function getAnnee_Naissance(): int
    {
        if ($this->annee_naissance == null) {
            return 0;
        }
        return $this->annee_naissance;
    }

    /**
     * @param int $annee_naissance
     * @return Coord
     * @throws \Exception
     */
    public function setAnnee_Naissance(int $annee_naissance): self
    {
        if (strlen((string)$annee_naissance) > 4) {
            throw new \Exception("You can't set an annee_naissance over 4 characters");
        }
        $this->annee_naissance = $annee_naissance;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUt(): DateTime
    {
        return $this->ut;
    }

    /**
     * @param DateTime $ut
     * @return Coord
     */
    public function setUt(DateTime $ut): self
    {
        $this->ut = $ut;

        return $this;
    }

}