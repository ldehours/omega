<?php

namespace App\Entity\Website;

use App\Utils\DateTime;

class Adh
{
    /**
     * @var integer $id
     * id website account
     */
    private $id;

    /**
     * @var null|DateTime $ml
     * end date of access ml's ad
     */
    private $ml;
    /**
     * @var null|DateTime $cs
     * end date of access cs's ad
     */
    private $cs;
    /**
     * @var null|DateTime $ps
     * end date of access ps's ad
     */
    private $ps;
    /**
     * @var null|DateTime $cfml
     * end date of access to the cfml
     */
    private $cfml;

    /**
     * @var null|DateTime $bnc
     * end date of access to the bnc
     */
    private $bnc;

    /**
     * @var null|DateTime $lms
     * end date of access to the lms
     */
    private $lms;

    /**
     * @var null|DateTime $hotline_fiscale
     * end date of access to the hotline_fiscale
     */
    private $hotline_fiscale;

    /**
     * @var null|DateTime $re
     * end date of access to the relay service
     */
    private $re;

    /**
     * @var null|DateTime $pr
     * end date of access to the privilege service
     */
    private $pr;

    /**
     * @var null|DateTime $cus_valide
     * end date of access to the ads portal
     */
    private $cus_valide;

    /**
     * @var string $statut
     * possible values: R - Rempla | I - Installé |  X - Spéciale
     */
    private $statut;

    /**
     * @var integer $cle_nantu
     * Reference key to nantu's application
     */
    private $cle_nantu;


    /**
     * @var null|DateTime $contact_nantu
     * Last date contact
     */
    private $contact_nantu;

    /**
     * @var null|DateTime $ut
     * Used for automated process (maybe more). "ut" = update time
     */
    private $ut;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Adh
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getMl(): ?DateTime
    {
        return $this->ml;
    }

    /**
     * @param DateTime|null $ml
     * @return Adh
     */
    public function setMl(?DateTime $ml): self
    {
        $this->ml = $ml;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCs(): ?DateTime
    {
        return $this->cs;
    }

    /**
     * @param DateTime|null $cs
     * @return Adh
     */
    public function setCs(?DateTime $cs): self
    {
        $this->cs = $cs;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getPs(): ?DateTime
    {
        return $this->ps;
    }

    /**
     * @param DateTime|null $ps
     * @return Adh
     */
    public function setPs(?DateTime $ps): self
    {
        $this->ps = $ps;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCfml(): ?DateTime
    {
        return $this->cfml;
    }

    /**
     * @param DateTime|null $cfml
     * @return Adh
     */
    public function setCfml(?DateTime $cfml): self
    {
        $this->cfml = $cfml;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getBnc(): ?DateTime
    {
        return $this->bnc;
    }

    /**
     * @param DateTime|null $bnc
     * @return Adh
     */
    public function setBnc(?DateTime $bnc): self
    {
        $this->bnc = $bnc;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLms(): ?DateTime
    {
        return $this->lms;
    }

    /**
     * @param DateTime|null $lms
     * @return Adh
     */
    public function setLms(?DateTime $lms): self
    {
        $this->lms = $lms;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getHotline_Fiscale(): ?DateTime
    {
        return $this->hotline_fiscale;
    }

    /**
     * @param DateTime|null $hotline_fiscale
     * @return Adh
     */
    public function setHotline_Fiscale(?DateTime $hotline_fiscale): self
    {
        $this->hotline_fiscale = $hotline_fiscale;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getRe(): ?DateTime
    {
        return $this->re;
    }

    /**
     * @param DateTime|null $re
     * @return Adh
     */
    public function setRe(?DateTime $re): self
    {
        $this->re = $re;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getPr(): ?DateTime
    {
        return $this->pr;
    }

    /**
     * @param DateTime|null $pr
     * @return Adh
     */
    public function setPr(?DateTime $pr): self
    {
        $this->pr = $pr;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCus_Valide(): ?DateTime
    {
        return $this->cus_valide;
    }

    /**
     * @param DateTime|null $cus_valide
     * @return Adh
     */
    public function setCus_Valide(?DateTime $cus_valide): self
    {
        $this->cus_valide = $cus_valide;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatut(): string
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     * @return Adh
     */
    public function setStatut(string $statut): self
    {
        $this->statut = $statut;
        return $this;
    }

    /**
     * @return int
     */
    public function getCle_Nantu(): int
    {
        return $this->cle_nantu;
    }

    /**
     * @param int $cle_nantu
     * @return Adh
     */
    public function setCle_Nantu(int $cle_nantu): self
    {
        $this->cle_nantu = $cle_nantu;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUt(): ?DateTime
    {
        return $this->ut;
    }

    /**
     * @param DateTime|null $ut
     * @return Adh
     */
    public function setUt(?DateTime $ut): self
    {
        $this->ut = $ut;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getContact_Nantu(): ?DateTime
    {
        return $this->contact_nantu;
    }

    /**
     * @param DateTime|null $contact_nantu
     * @return Adh
     */
    public function setContact_Nantu(?DateTime $contact_nantu): self
    {
        $this->contact_nantu = $contact_nantu;
        return $this;
    }


}