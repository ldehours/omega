<?php

namespace App\Entity\Legacy;

use App\Entity\Offer\Criterion;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Legacy\LegacyCriterionRepository")
 */
class LegacyCriterion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Offer\Criterion", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $criterion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identifier;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Criterion|null
     */
    public function getCriterion(): ?Criterion
    {
        return $this->criterion;
    }

    /**
     * @param Criterion $criterion
     * @return LegacyCriterion
     */
    public function setCriterion(Criterion $criterion): self
    {
        $this->criterion = $criterion;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return LegacyCriterion
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }
}
