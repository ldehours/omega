<?php

namespace App\Entity\Legacy;

use App\Entity\Core\Staff;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Legacy\LegacyStaffRepository")
 */
class LegacyStaff
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $initials;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\Staff", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getInitials(): ?string
    {
        return $this->initials;
    }

    /**
     * @param string $initials
     * @return LegacyStaff
     */
    public function setInitials(string $initials): self
    {
        $this->initials = $initials;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getPerson(): ?Staff
    {
        return $this->person;
    }

    /**
     * @param Staff $person
     * @return LegacyStaff
     */
    public function setPerson(Staff $person): self
    {
        $this->person = $person;

        return $this;
    }
}
