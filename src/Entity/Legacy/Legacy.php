<?php

namespace App\Entity\Legacy;

use App\Entity\Core\Person;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Legacy\LegacyRepository")
 */
class Legacy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Réfère à l'id dans la base de donnée de nantu
     * Ce n'est cependant pas forcément correspondant à la cle_nantu dans cette-dite BDD
     * @ORM\Column(type="integer")
     */
    private $idNantu;

    /**
     * Réfère à l'id dans la base de donnée de cfr
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idCfml;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="legacies")
     */
    private $person;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getIdNantu(): ?int
    {
        return $this->idNantu;
    }

    /**
     * @param int $idNantu
     * @return Legacy
     */
    public function setIdNantu(int $idNantu): self
    {
        $this->idNantu = $idNantu;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIdCfml(): ?int
    {
        return $this->idCfml;
    }

    /**
     * @param int|null $idCfml
     * @return Legacy
     */
    public function setIdCfml(?int $idCfml): self
    {
        $this->idCfml = $idCfml;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Legacy
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person|null $person
     * @return Legacy
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}
