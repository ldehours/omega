<?php

namespace App\Entity\Offer;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\OfferWishMatchingRepository")
 */
class OfferWishMatching
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Offer", inversedBy="offerWishMatchings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Wish", inversedBy="offerWishMatchings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $wish;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Offer|null
     */
    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer|null $offer
     * @return OfferWishMatching
     */
    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return Wish|null
     */
    public function getWish(): ?Wish
    {
        return $this->wish;
    }

    /**
     * @param Wish|null $wish
     * @return OfferWishMatching
     */
    public function setWish(?Wish $wish): self
    {
        $this->wish = $wish;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return OfferWishMatching
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
