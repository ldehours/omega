<?php

namespace App\Entity\Offer;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\CriterionPossibleValueRepository")
 */
class CriterionPossibleValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\SubstitutionType", inversedBy="criterionPossibleValues",cascade={"all"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $substitutionType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Criterion", inversedBy="criterionPossibleValues",cascade={"all"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $criterion;

    /**
     * @ORM\Column(type="json_array")
     */
    private $possibleValue;

    /**
     * @ORM\Column(type="integer")
     */
    private $criterionType;

    /**
     * @ORM\Column(type="integer")
     */
    private $substitutionNature;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return SubstitutionType|null
     */
    public function getSubstitutionType(): ?SubstitutionType
    {
        return $this->substitutionType;
    }

    /**
     * @param SubstitutionType|null $substitutionType
     * @return CriterionPossibleValue
     */
    public function setSubstitutionType(?SubstitutionType $substitutionType): self
    {
        $this->substitutionType = $substitutionType;

        return $this;
    }

    /**
     * @return Criterion|null
     */
    public function getCriterion(): ?Criterion
    {
        return $this->criterion;
    }

    /**
     * @param Criterion|null $criterion
     * @return CriterionPossibleValue
     */
    public function setCriterion(?Criterion $criterion): self
    {
        $this->criterion = $criterion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPossibleValue()
    {
        return $this->possibleValue;
    }

    /**
     * @param $possibleValue
     * @return CriterionPossibleValue
     */
    public function setPossibleValue($possibleValue): self
    {
        $this->possibleValue = $possibleValue;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCriterionType(): ?int
    {
        return $this->criterionType;
    }

    /**
     * @param int $criterionType
     * @return CriterionPossibleValue
     */
    public function setCriterionType(int $criterionType): self
    {
        if(!in_array($criterionType,Criterion::TYPES)){
            throw new \InvalidArgumentException($criterionType.' must exist in Criterion::TYPES array');
        }
        $this->criterionType = $criterionType;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubstitutionNature(): ?int
    {
        return $this->substitutionNature;
    }

    /**
     * @param int $substitutionNature
     * @return CriterionPossibleValue
     */
    public function setSubstitutionNature(int $substitutionNature): self
    {
        if(!in_array($substitutionNature,Canvas::CANVAS_TYPES)){
            throw new \InvalidArgumentException($substitutionNature.' must exist in Canvas::CANVAS_TYPES array');
        }
        $this->substitutionNature = $substitutionNature;
        return $this;
    }
}
