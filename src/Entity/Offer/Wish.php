<?php

namespace App\Entity\Offer;

use App\Entity\Core\Department;
use App\Entity\Core\TimeRange;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\WishRepository")
 */
class Wish extends Substitution
{
    /**
     * @ORM\Id @ORM\OneToOne(targetEntity="App\Entity\Offer\Substitution")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $distance = [];

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Core\TimeRange", inversedBy="wish", cascade={"persist"})
     */
    private $timeRange;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\OfferWishMatching", mappedBy="wish")
     */
    private $offerWishMatchings;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Department", inversedBy="wishes")
     */
    private $departments;

    public function __construct()
    {
        parent::__construct();
        $this->offerWishMatchings = new ArrayCollection();
        $this->isActive = true;
        $this->departments = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return array|null
     */
    public function getDistance(): ?array
    {
        return $this->distance;
    }

    /**
     * @param array|null $distance
     * @return Wish
     */
    public function setDistance(?array $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * @return TimeRange|null
     */
    public function getTimeRange(): ?TimeRange
    {
        return $this->timeRange;
    }

    /**
     * @param TimeRange|null $timeRange
     * @return Wish
     */
    public function setTimeRange(?TimeRange $timeRange): self
    {
        $this->timeRange = $timeRange;

        return $this;
    }

    /**
     * @return Collection|OfferWishMatching[]
     */
    public function getOfferWishMatchings(): Collection
    {
        return $this->offerWishMatchings;
    }

    public function addOfferWishMatching(OfferWishMatching $offerWishMatching): self
    {
        if (!$this->offerWishMatchings->contains($offerWishMatching)) {
            $this->offerWishMatchings[] = $offerWishMatching;
            $offerWishMatching->setWish($this);
        }

        return $this;
    }

    public function removeOfferWishMatching(OfferWishMatching $offerWishMatching): self
    {
        if ($this->offerWishMatchings->contains($offerWishMatching)) {
            $this->offerWishMatchings->removeElement($offerWishMatching);
            // set the owning side to null (unless already changed)
            if ($offerWishMatching->getWish() === $this) {
                $offerWishMatching->setWish(null);
            }
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|Department[]
     */
    public function getDepartments(): Collection
    {
        return $this->departments;
    }

    /**
     * @param Department $department
     * @return $this
     */
    public function addDepartment(Department $department): self
    {
        if (!$this->departments->contains($department)) {
            $this->departments[] = $department;
        }

        return $this;
    }

    /**
     * @param Department $department
     * @return $this
     */
    public function removeDepartment(Department $department): self
    {
        if ($this->departments->contains($department)) {
            $this->departments->removeElement($department);
        }

        return $this;
    }
}
