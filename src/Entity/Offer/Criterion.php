<?php

namespace App\Entity\Offer;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\CriterionRepository")
 */
class Criterion
{
    use TranslatableTrait;

    const TYPE_BOOL = 201;
    const TYPE_INTEGER = 202;
    const TYPE_SELECT = 203;
    const TYPE_DATE = 204;
    const TYPE_SPECIALTY = 205;
    // TODO Trouver comment gérer ce type et faire le js associé dans base.html.twig
    const TYPE_CHOICE_WEEK = 206;
    const TYPE_TEXT = 207;
    const TYPE_FILE = 208;
    const TYPE_MULTI_SELECT = 209;
    const TYPE_LOCATION = 210;
    const TYPE_ENTITY = 211;
    const TYPE_INPUT = 212;
    const TYPE_JSON = 213;

    const HAS_POSSIBLE_VALUE = [
        self::TYPE_SELECT,
        self::TYPE_MULTI_SELECT
    ];

    const TYPES = [
        self::TYPE_BOOL,
        self::TYPE_INTEGER,
        self::TYPE_SELECT,
        self::TYPE_DATE,
        self::TYPE_SPECIALTY,
        self::TYPE_CHOICE_WEEK,
        self::TYPE_TEXT,
        self::TYPE_FILE,
        self::TYPE_MULTI_SELECT,
        self::TYPE_JSON,
        self::TYPE_ENTITY,
        self::TYPE_INPUT,
        self::TYPE_LOCATION
    ];

    const LABEL = [
        self::TYPE_BOOL => 'Oui/Non',
        self::TYPE_INTEGER => "Nombre",
        self::TYPE_SELECT => "Liste de choix",
        self::TYPE_DATE => "Date",
        self::TYPE_SPECIALTY => "Spécialité/Compétence/Formation/Orientation",
        self::TYPE_CHOICE_WEEK => 'Jour de semaine',
        self::TYPE_TEXT => "Texte",
        self::TYPE_FILE => "Fichier",
        self::TYPE_MULTI_SELECT => "Liste à choix multiple",
        self::TYPE_LOCATION => "Région/Département",
        self::TYPE_JSON => "Json",
        self::TYPE_ENTITY => "Entité",
        self::TYPE_INPUT => "Champ texte de base"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\CriterionPossibleValue", mappedBy="criterion", orphanRemoval=true)
     */
    private $criterionPossibleValues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\SubstitutionCriterion", mappedBy="criterion", orphanRemoval=true)
     */
    private $substitutionCriteria;
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $dependency = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $relevantable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Criterion", inversedBy="criteriaDependencies")
     */
    private $criterionHeadDependency;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Criterion", mappedBy="criterionHeadDependency")
     */
    private $criteriaDependencies;

    public function __construct()
    {
        $this->criteriaDependencies = new ArrayCollection();
        $this->criterionPossibleValues = new ArrayCollection();
        $this->substitutionCriteria = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Criterion
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getDependency(): ?array
    {
        return $this->dependency;
    }

    /**
     * @param array|null $dependency
     * @return Criterion
     */
    public function setDependency(?array $dependency): self
    {
        $this->dependency = $dependency;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRelevantable(): ?bool
    {
        return $this->relevantable;
    }

    /**
     * @param bool $relevantable
     * @return Criterion
     */
    public function setRelevantable(bool $relevantable): self
    {
        $this->relevantable = $relevantable;

        return $this;
    }

    /**
     * @return Collection|CriterionPossibleValue[]
     */
    public function getCriterionPossibleValues(): Collection
    {
        return $this->criterionPossibleValues;
    }

    /**
     * @param CriterionPossibleValue $criterionPossibleValue
     * @return Criterion
     */
    public function addCriterionPossibleValue(CriterionPossibleValue $criterionPossibleValue): self
    {
        if (!$this->criterionPossibleValues->contains($criterionPossibleValue)) {
            $this->criterionPossibleValues[] = $criterionPossibleValue;
            $criterionPossibleValue->setCriterion($this);
        }

        return $this;
    }

    /**
     * @param CriterionPossibleValue $criterionPossibleValue
     * @return Criterion
     */
    public function removeCriterionPossibleValue(CriterionPossibleValue $criterionPossibleValue): self
    {
        if ($this->criterionPossibleValues->contains($criterionPossibleValue)) {
            $this->criterionPossibleValues->removeElement($criterionPossibleValue);
            // set the owning side to null (unless already changed)
            if ($criterionPossibleValue->getCriterion() === $this) {
                $criterionPossibleValue->setCriterion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SubstitutionCriterion[]
     */
    public function getSubstitutionCriteria(): Collection
    {
        return $this->substitutionCriteria;
    }

    /**
     * @param SubstitutionCriterion $substitutionCriterion
     * @return Criterion
     */
    public function addSubstitutionCriterion(SubstitutionCriterion $substitutionCriterion): self
    {
        if (!$this->substitutionCriteria->contains($substitutionCriterion)) {
            $this->substitutionCriteria[] = $substitutionCriterion;
            $substitutionCriterion->setCriterion($this);
        }

        return $this;
    }

    /**
     * @param SubstitutionCriterion $substitutionCriterion
     * @return Criterion
     */
    public function removeSubstitutionCriterion(SubstitutionCriterion $substitutionCriterion): self
    {
        if ($this->substitutionCriteria->contains($substitutionCriterion)) {
            $this->substitutionCriteria->removeElement($substitutionCriterion);
            // set the owning side to null (unless already changed)
            if ($substitutionCriterion->getCriterion() === $this) {
                $substitutionCriterion->setCriterion(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getLabel();
    }

    /**
     * @return Criterion|null
     */
    public function getCriterionHeadDependency(): ?self
    {
        return $this->criterionHeadDependency;
    }

    /**
     * @param self|null $criterionHeadDependency
     * @return Criterion
     */
    public function setCriterionHeadDependency(?self $criterionHeadDependency): self
    {
        $this->criterionHeadDependency = $criterionHeadDependency;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCriteriaDependencies(): Collection
    {
        return $this->criteriaDependencies;
    }

    /**
     * @param self $criteriaDependency
     * @return Criterion
     */
    public function addCriteriaDependency(self $criteriaDependency): self
    {
        if (!$this->criteriaDependencies->contains($criteriaDependency)) {
            $this->criteriaDependencies[] = $criteriaDependency;
            $criteriaDependency->setCriterionHeadDependency($this);
        }

        return $this;
    }

    /**
     * @param self $criteriaDependency
     * @return Criterion
     */
    public function removeCriteriaDependency(self $criteriaDependency): self
    {
        if ($this->criteriaDependencies->contains($criteriaDependency)) {
            $this->criteriaDependencies->removeElement($criteriaDependency);
            // set the owning side to null (unless already changed)
            if ($criteriaDependency->getCriterionHeadDependency() === $this) {
                $criteriaDependency->setCriterionHeadDependency(null);
            }
        }

        return $this;
    }
}
