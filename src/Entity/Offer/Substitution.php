<?php

namespace App\Entity\Offer;

use App\Entity\Core\Person;
use App\Entity\Core\Staff;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\SubstitutionRepository")
 * @InheritanceType( "JOINED" )
 * @DiscriminatorColumn( name = "SUBSTITUTION_TYPE", type = "string" )
 * @DiscriminatorMap( { "offer" = "Offer",
 *                      "wish" = "Wish" } )
 */
abstract class Substitution
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="date_immutable")
     */
    protected $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff", inversedBy="substitutions")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $creator;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Person", inversedBy="substitutions")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $person;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\SubstitutionType", inversedBy="substitutions")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $substitutionType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\SubstitutionCriterion", mappedBy="substitution", orphanRemoval=true)
     */
    protected $substitutionCriteria;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Canvas", inversedBy="substitutions")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $canvas;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastUpdateDate;

    /**
     * @ORM\Column(type="date")
     */
    private $startingDate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isModel;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lastFullUpdateAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    public function __construct()
    {
        $this->substitutionCriteria = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getCanvas()->getLabel() . ' (' . $this->getId() . ')';
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getCreationDate(): ?DateTimeImmutable
    {
        return $this->creationDate;
    }

    /**
     * @param DateTimeImmutable $creationDate
     * @return Substitution
     */
    public function setCreationDate(DateTimeImmutable $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getCreator(): ?Staff
    {
        return $this->creator;
    }

    /**
     * @param Staff|null $creator
     * @return Substitution
     */
    public function setCreator(?Staff $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person|null $person
     * @return Substitution
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return SubstitutionType|null
     */
    public function getSubstitutionType(): ?SubstitutionType
    {
        return $this->substitutionType;
    }

    /**
     * @param SubstitutionType|null $substitutionType
     * @return Substitution
     */
    public function setSubstitutionType(?SubstitutionType $substitutionType): self
    {
        $this->substitutionType = $substitutionType;

        return $this;
    }

    /**
     * @return Collection|SubstitutionCriterion[]
     */
    public function getSubstitutionCriteria(): Collection
    {
        return $this->substitutionCriteria;
    }

    /**
     * @param SubstitutionCriterion $substitutionCriterion
     * @return Substitution
     */
    public function addSubstitutionCriterion(SubstitutionCriterion $substitutionCriterion): self
    {
        if (!$this->substitutionCriteria->contains($substitutionCriterion)) {
            $this->substitutionCriteria[] = $substitutionCriterion;
            $substitutionCriterion->setSubstitution($this);
        }

        return $this;
    }

    /**
     * @param SubstitutionCriterion $substitutionCriterion
     * @return Substitution
     */
    public function removeSubstitutionCriterion(SubstitutionCriterion $substitutionCriterion): self
    {
        if ($this->substitutionCriteria->contains($substitutionCriterion)) {
            $this->substitutionCriteria->removeElement($substitutionCriterion);
            // set the owning side to null (unless already changed)
            if ($substitutionCriterion->getSubstitution() === $this) {
                $substitutionCriterion->setSubstitution(null);
            }
        }

        return $this;
    }

    /**
     * @return Canvas|null
     */
    public function getCanvas(): ?Canvas
    {
        return $this->canvas;
    }

    /**
     * @param Canvas|null $canvas
     * @return Substitution
     */
    public function setCanvas(?Canvas $canvas): self
    {
        $this->canvas = $canvas;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastUpdateDate(): ?DateTimeInterface
    {
        return $this->lastUpdateDate;
    }

    /**
     * @param DateTimeInterface $lastUpdateDate
     * @return Substitution
     */
    public function setLastUpdateDate(DateTimeInterface $lastUpdateDate): self
    {
        $this->lastUpdateDate = $lastUpdateDate;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getStartingDate(): ?DateTimeInterface
    {
        return $this->startingDate;
    }

    /**
     * @param DateTimeInterface $startingDate
     * @return Substitution
     */
    public function setStartingDate(DateTimeInterface $startingDate): self
    {
        $this->startingDate = $startingDate;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsModel(): ?bool
    {
        return $this->isModel;
    }

    /**
     * @param bool $isModel
     * @return Substitution
     */
    public function setIsModel(bool $isModel): self
    {
        $this->isModel = $isModel;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastFullUpdateAt(): ?\DateTimeInterface
    {
        return $this->lastFullUpdateAt;
    }

    /**
     * @param DateTimeInterface $lastFullUpdateAt
     * @return $this
     */
    public function setLastFullUpdateAt(\DateTimeInterface $lastFullUpdateAt): self
    {
        $this->lastFullUpdateAt = $lastFullUpdateAt;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * @param DateTimeInterface|null $endDate
     * @return $this
     */
    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getReadableEndDate()
    {
        $endDate = $this->getEndDate();
        if ($endDate === null) {
            return 'Non définie';
        }
        return $endDate->format('d/m/Y');
    }

    /**
     * Clear start and end dates.
     * @return $this
     */
    protected function clearDates(): self
    {
        $this->startingDate = null;
        $this->endDate = null;
        return $this;
    }

    /**
     * @param int $type
     * @return bool
     * @throws Exception
     */
    public function checkSubstitutionType(int $type): bool
    {
        if (!array_key_exists($type, SubstitutionType::TYPES_POSSIBLE)) {
            throw new Exception('$type given in Offer::checkSubstitutionType must be a key of SubstitutionType::TYPES_POSSIBLE array');
        }
        return in_array($this->getSubstitutionType()->getSubstitutionType(), SubstitutionType::TYPES_POSSIBLE[$type], true);
    }
}
