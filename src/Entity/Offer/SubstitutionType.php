<?php

namespace App\Entity\Offer;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\SubstitutionTypeRepository")
 */
class SubstitutionType
{
    use TranslatableTrait;

    const TYPE_LIBERAL_REPLACEMENT = 101;
    const TYPE_SALARIED_POSITION = 102;
    const TYPE_SELL_ASSOCIATION = 103;

    const TYPE_LIBERAL_REPLACEMENT_PUNCTUAL = 104;
    const TYPE_LIBERAL_REPLACEMENT_REGULAR = 105;
    const TYPE_LIBERAL_REPLACEMENT_ON_CALL = 106;
    const TYPE_LIBERAL_REPLACEMENT_LONG_TIME = 107;
    const TYPE_LIBERAL_REPLACEMENT_PARTNERSHIP = 108;
    const TYPE_LIBERAL_REPLACEMENT_TEMPORARY_PARTNERSHIP = 109;
    const TYPE_LIBERAL_REPLACEMENT_DECEASED_REPLACEMENT = 110; //tenue de poste

    const TYPE_SALARIED_POSITION_FIXED_TERM_CONTRACT = 111; //CDD
    const TYPE_SALARIED_POSITION_PERMANENT_CONTRACT = 112; //CDI
    const TYPE_SALARIED_POSITION_SHIFT = 113; //vacation

    const TYPE_SELL_ASSOCIATION_SESSION = 114;
    const TYPE_SELL_ASSOCIATION_DOCTOR_PARTNERSHIP = 115;
    const TYPE_SELL_ASSOCIATION_LOCATION = 116;
    const TYPE_SELL_ASSOCIATION_SELLING = 117;

    const TYPES_POSSIBLE = [
        self::TYPE_LIBERAL_REPLACEMENT => [
            self::TYPE_LIBERAL_REPLACEMENT_PUNCTUAL,
            self::TYPE_LIBERAL_REPLACEMENT_REGULAR,
            self::TYPE_LIBERAL_REPLACEMENT_ON_CALL,
            self::TYPE_LIBERAL_REPLACEMENT_LONG_TIME,
            self::TYPE_LIBERAL_REPLACEMENT_PARTNERSHIP,
            self::TYPE_LIBERAL_REPLACEMENT_TEMPORARY_PARTNERSHIP,
            self::TYPE_LIBERAL_REPLACEMENT_DECEASED_REPLACEMENT,
        ],
        self::TYPE_SALARIED_POSITION => [
            self::TYPE_SALARIED_POSITION_FIXED_TERM_CONTRACT,
            self::TYPE_SALARIED_POSITION_PERMANENT_CONTRACT,
            self::TYPE_SALARIED_POSITION_SHIFT,
        ],
        self::TYPE_SELL_ASSOCIATION => [
            self::TYPE_SELL_ASSOCIATION_SESSION,
            self::TYPE_SELL_ASSOCIATION_DOCTOR_PARTNERSHIP,
            self::TYPE_SELL_ASSOCIATION_LOCATION,
            self::TYPE_SELL_ASSOCIATION_SELLING
        ],
    ];

    const LABEL = [
        self::TYPE_SALARIED_POSITION => "Postes-Salariés",
        self::TYPE_LIBERAL_REPLACEMENT => "Remplacements libéraux",
        self::TYPE_SELL_ASSOCIATION => "Cessions/Associations",

        self::TYPE_LIBERAL_REPLACEMENT_PUNCTUAL => "Remplacement libéral ponctuel",
        self::TYPE_LIBERAL_REPLACEMENT_REGULAR => "Remplacement libéral régulier",
        self::TYPE_LIBERAL_REPLACEMENT_LONG_TIME => "Remplacement libéral de longue durée",
        self::TYPE_LIBERAL_REPLACEMENT_ON_CALL => "Remplacement de garde",
        self::TYPE_LIBERAL_REPLACEMENT_PARTNERSHIP => "Collaboration",
        self::TYPE_LIBERAL_REPLACEMENT_TEMPORARY_PARTNERSHIP => "Association temporaire",
        self::TYPE_LIBERAL_REPLACEMENT_DECEASED_REPLACEMENT => "Tenue de poste",

        self::TYPE_SALARIED_POSITION_FIXED_TERM_CONTRACT => "CDD",
        self::TYPE_SALARIED_POSITION_PERMANENT_CONTRACT => "CDI",
        self::TYPE_SALARIED_POSITION_SHIFT => "Vacation",

        self::TYPE_SELL_ASSOCIATION_SESSION => "Cession de cabinet",
        self::TYPE_SELL_ASSOCIATION_DOCTOR_PARTNERSHIP => "Association de medecin",
        self::TYPE_SELL_ASSOCIATION_LOCATION => "Location de locaux professionnels",
        self::TYPE_SELL_ASSOCIATION_SELLING => "Vente de local professionnels"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Correspond to the TYPES_POSSIBLE value
     * @ORM\Column(type="integer", length=3, unique=true)
     */
    private $substitutionType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\CriterionPossibleValue", mappedBy="substitutionType", orphanRemoval=true)
     */
    private $criterionPossibleValues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Substitution", mappedBy="substitutionType")
     */
    private $substitutions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Canvas", mappedBy="substitutionType")
     */
    private $canvas;

    public function __construct()
    {
        $this->criterionPossibleValues = new ArrayCollection();
        $this->substitutions = new ArrayCollection();
        $this->canvas = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return self::LABEL[$this->substitutionType];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getSubstitutionType(): ?int
    {
        return $this->substitutionType;
    }

    /**
     * @param int $substitutionType
     * @return SubstitutionType
     */
    public function setSubstitutionType(int $substitutionType): self
    {
        if(!array_key_exists($substitutionType,self::LABEL)){
            throw new \InvalidArgumentException($substitutionType.' is not a key of SubstitutionType::LABEL (it must be)');
        }
        $this->substitutionType = $substitutionType;
        return $this;
    }

    /**
     * @return Collection|CriterionPossibleValue[]
     */
    public function getCriterionPossibleValues(): Collection
    {
        return $this->criterionPossibleValues;
    }

    /**
     * @param CriterionPossibleValue $criterionPossibleValue
     * @return SubstitutionType
     */
    public function addCriterionPossibleValue(CriterionPossibleValue $criterionPossibleValue): self
    {
        if (!$this->criterionPossibleValues->contains($criterionPossibleValue)) {
            $this->criterionPossibleValues[] = $criterionPossibleValue;
            $criterionPossibleValue->setSubstitutionType($this);
        }

        return $this;
    }

    /**
     * @param CriterionPossibleValue $criterionPossibleValue
     * @return SubstitutionType
     */
    public function removeCriterionPossibleValue(CriterionPossibleValue $criterionPossibleValue): self
    {
        if ($this->criterionPossibleValues->contains($criterionPossibleValue)) {
            $this->criterionPossibleValues->removeElement($criterionPossibleValue);
            // set the owning side to null (unless already changed)
            if ($criterionPossibleValue->getSubstitutionType() === $this) {
                $criterionPossibleValue->setSubstitutionType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Substitution[]
     */
    public function getSubstitutions(): Collection
    {
        return $this->substitutions;
    }

    /**
     * @param Substitution $substitution
     * @return SubstitutionType
     */
    public function addSubstitution(Substitution $substitution): self
    {
        if (!$this->substitutions->contains($substitution)) {
            $this->substitutions[] = $substitution;
            $substitution->setSubstitutionType($this);
        }

        return $this;
    }

    /**
     * @param Substitution $substitution
     * @return SubstitutionType
     */
    public function removeSubstitution(Substitution $substitution): self
    {
        if ($this->substitutions->contains($substitution)) {
            $this->substitutions->removeElement($substitution);
            // set the owning side to null (unless already changed)
            if ($substitution->getSubstitutionType() === $this) {
                $substitution->setSubstitutionType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Canvas[]
     */
    public function getCanvas(): Collection
    {
        return $this->canvas;
    }

    /**
     * @param Canvas $canva
     * @return SubstitutionType
     */
    public function addCanva(Canvas $canva): self
    {
        if (!$this->canvas->contains($canva)) {
            $this->canvas[] = $canva;
            $canva->setSubstitutionType($this);
        }

        return $this;
    }

    /**
     * @param Canvas $canva
     * @return SubstitutionType
     */
    public function removeCanva(Canvas $canva): self
    {
        if ($this->canvas->contains($canva)) {
            $this->canvas->removeElement($canva);
            // set the owning side to null (unless already changed)
            if ($canva->getSubstitutionType() === $this) {
                $canva->setSubstitutionType(null);
            }
        }

        return $this;
    }
}
