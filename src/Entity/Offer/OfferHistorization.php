<?php

namespace App\Entity\Offer;

use App\Entity\Core\Staff;
use App\Service\Historization\HistorizationHandlerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\OfferHistorizationRepository")
 */
class OfferHistorization implements HistorizationHandlerInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Entity\Offer\Offer", inversedBy="oldStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $offer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $oldStatus;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Staff", inversedBy="offerHistorizations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $staff;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateChange;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $oldType;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \App\Entity\Offer\Offer|null
     */
    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    /**
     * @param \App\Entity\Offer\Offer|null $offer
     * @return $this
     */
    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOldStatus(): ?int
    {
        return $this->oldStatus;
    }

    /**
     * @param int|null $oldStatus
     * @return $this
     */
    public function setOldStatus(?int $oldStatus): self
    {
        $this->oldStatus = $oldStatus;

        return $this;
    }

    /**
     * @return Staff|null
     */
    public function getStaff(): ?Staff
    {
        return $this->staff;
    }

    /**
     * @param Staff|null $staff
     * @return $this
     */
    public function setStaff(?Staff $staff): self
    {
        $this->staff = $staff;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateChange(): ?\DateTimeInterface
    {
        return $this->dateChange;
    }

    /**
     * @param \DateTimeInterface|null $dateChange
     * @return $this
     */
    public function setDateChange(?\DateTimeInterface $dateChange): self
    {
        $this->dateChange = $dateChange;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOldType(): ?int
    {
        return $this->oldType;
    }

    /**
     * @param int|null $oldType
     * @return $this
     */
    public function setOldType(?int $oldType): self
    {
        $this->oldType = $oldType;

        return $this;
    }
}
