<?php

namespace App\Entity\Offer;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\SubstitutionCriterionRepository")
 */
class SubstitutionCriterion
{
    const IMPORTANT = 2;
    const SECONDARY = 1;
    const AVAILABLE = 0;

    const RELEVANTNESS = [
        self::AVAILABLE,
        self::SECONDARY,
        self::IMPORTANT,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Substitution", inversedBy="substitutionCriteria",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $substitution;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\Criterion", inversedBy="substitutionCriteria")
     * @ORM\JoinColumn(nullable=false)
     */
    private $criterion;

    /**
     * @ORM\Column(type="json_array", nullable=false)
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     */
    private $isRelevant = 0;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Substitution|null
     */
    public function getSubstitution(): ?Substitution
    {
        return $this->substitution;
    }

    /**
     * @param Substitution|null $substitution
     * @return SubstitutionCriterion
     */
    public function setSubstitution(?Substitution $substitution): self
    {
        $this->substitution = $substitution;

        return $this;
    }

    /**
     * @return Criterion|null
     */
    public function getCriterion(): ?Criterion
    {
        return $this->criterion;
    }

    /**
     * @param Criterion|null $criterion
     * @return SubstitutionCriterion
     */
    public function setCriterion(?Criterion $criterion): self
    {
        $this->criterion = $criterion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return SubstitutionCriterion
     */
    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsRelevant(): ?int
    {
        return $this->isRelevant;
    }

    /**
     * @param int $isRelevant
     * @return SubstitutionCriterion
     */
    public function setIsRelevant(int $isRelevant): self
    {
        if(!in_array($isRelevant,self::RELEVANTNESS)){
            throw new \InvalidArgumentException($isRelevant . ' could not be found in SubstitutionCriterion::RELEVANTNESS array');
        }
        $this->isRelevant = $isRelevant;
        return $this;
    }
}
