<?php

namespace App\Entity\Offer;

use App\Entity\Core\Department;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\Note;
use App\Entity\Core\Origin;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Service\Serializer\Serializable;
use App\Entity\Core\Staff;
use App\Service\Historization\HistorizableInterface;
use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\OfferRepository")
 */
class Offer extends Substitution implements Serializable, HistorizableInterface
{
    use TranslatableTrait;

    public const STATE_WAIT_INFO = 200;
    public const STATE_PROGRESS = 201;
    public const STATE_ALMOST_FILL = 202;
    public const STATE_FILLED_BY_US = 203;
    public const STATE_FILLED_BY_OTHER = 204;
    public const STATE_CANCELLED = 205;

    public const STATES = [
        self::STATE_WAIT_INFO,
        self::STATE_PROGRESS,
        self::STATE_ALMOST_FILL,
        self::STATE_FILLED_BY_US,
        self::STATE_FILLED_BY_OTHER,
        self::STATE_CANCELLED
    ];

    public const LABEL = [
        self::STATE_WAIT_INFO => 'En attente d\'information',
        self::STATE_PROGRESS => 'En cours',
        self::STATE_ALMOST_FILL => 'Quasiment pourvu',
        self::STATE_FILLED_BY_US => 'Pourvue par MS',
        self::STATE_FILLED_BY_OTHER => 'Pourvue HORS MS',
        self::STATE_CANCELLED => 'Annulée'
    ];

    public const TYPE_STANDARD_RELAY = 101;
    public const TYPE_EMERGENCY_RELAY = 102;
    public const TYPE_PSEUDO_EMERGENCY_RELAY = 103;
    public const TYPE_COMPETITION_RELAY = 104;
    public const TYPE_INFO_RELAY = 105;
    public const TYPE_STANDARD_PRIVILEGE = 106;
    public const TYPE_EMERGENCY_PRIVILEGE = 107;
    public const TYPE_GUARANTEE_1S_PRIVILEGE = 108;
    public const TYPE_GUARANTEE_2S_PRIVILEGE = 109;
    public const TYPE_PSEUDO_EMERGENCY_PRIVILEGE = 110;
    public const TYPE_INFO_PRIVILEGE = 111;
    public const TYPE_COMPETITION_PRIVILEGE = 112;
    public const TYPE_SUCCESS_WITHOUT_AGREEMENT = 113;
    public const TYPE_SUCCESS_WITH_AGREEMENT = 114;
    public const TYPE_RETROACTIVE_EMERGENCY = 115;
    public const TYPE_INFO = 116;
    public const TYPE_COMPETITION = 117;

    public const TYPES = [
        self::TYPE_STANDARD_RELAY,
        self::TYPE_EMERGENCY_RELAY,
        self::TYPE_PSEUDO_EMERGENCY_RELAY,
        self::TYPE_COMPETITION_RELAY,
        self::TYPE_INFO_RELAY,
        self::TYPE_STANDARD_PRIVILEGE,
        self::TYPE_EMERGENCY_PRIVILEGE,
        self::TYPE_GUARANTEE_1S_PRIVILEGE,
        self::TYPE_GUARANTEE_2S_PRIVILEGE,
        self::TYPE_PSEUDO_EMERGENCY_PRIVILEGE,
        self::TYPE_INFO_PRIVILEGE,
        self::TYPE_COMPETITION_PRIVILEGE,
        self::TYPE_SUCCESS_WITHOUT_AGREEMENT,
        self::TYPE_SUCCESS_WITH_AGREEMENT,
        self::TYPE_RETROACTIVE_EMERGENCY,
        self::TYPE_INFO,
        self::TYPE_COMPETITION
    ];

    public const TYPES_SALARIED_CESSION = [
        self::TYPE_SUCCESS_WITHOUT_AGREEMENT,
        self::TYPE_SUCCESS_WITH_AGREEMENT,
        self::TYPE_INFO,
        self::TYPE_COMPETITION
    ];

    public const TYPES_LABEL = [
        self::TYPE_STANDARD_RELAY => 'Standard relais',
        self::TYPE_EMERGENCY_RELAY => 'Urgence relais',
        self::TYPE_PSEUDO_EMERGENCY_RELAY => 'Pseudo Urgence relais',
        self::TYPE_COMPETITION_RELAY => 'Concurrence relais',
        self::TYPE_INFO_RELAY => 'Info relais',
        self::TYPE_STANDARD_PRIVILEGE => 'Standard privilège',
        self::TYPE_EMERGENCY_PRIVILEGE => 'Urgence privilège',
        self::TYPE_GUARANTEE_1S_PRIVILEGE => 'Garantie 1s privilège',
        self::TYPE_GUARANTEE_2S_PRIVILEGE => 'Garantie 2s privilège',
        self::TYPE_PSEUDO_EMERGENCY_PRIVILEGE => 'Pseudo Urgence privilège',
        self::TYPE_INFO_PRIVILEGE => 'Info privilège',
        self::TYPE_COMPETITION_PRIVILEGE => 'Concurrence privilège',
        self::TYPE_SUCCESS_WITHOUT_AGREEMENT => 'Réussite sans accord',
        self::TYPE_SUCCESS_WITH_AGREEMENT => 'Réussite avec accord',
        self::TYPE_RETROACTIVE_EMERGENCY => 'Urgence rétroactive',
        self::TYPE_INFO => 'Info',
        self::TYPE_COMPETITION => 'Concurrence',
    ];

    public const MODULARITY_TYPE_BEFORE = 1;
    public const MODULARITY_TYPE_AFTER = 2;
    public const MODULARITY_TYPE_BOTH = 3;

    public const MODULARITY_TYPES = [
        self::MODULARITY_TYPE_BEFORE,
        self::MODULARITY_TYPE_AFTER,
        self::MODULARITY_TYPE_BOTH
    ];

    public const MODULARITY_TYPE_LABEL = [
        self::MODULARITY_TYPE_BEFORE => 'Avant la date',
        self::MODULARITY_TYPE_AFTER => 'Après la date',
        self::MODULARITY_TYPE_BOTH => 'Avant et après',
    ];

    /**
     * @ORM\Id @ORM\OneToOne(targetEntity="App\Entity\Offer\Substitution" , fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $state;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Core\Note", mappedBy="offer", cascade={"persist"} , fetch="EXTRA_LAZY")
     */
    protected $notes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\PersonNatural", inversedBy="providedOffers" , fetch="EXTRA_LAZY")
     */
    protected $contractors;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Origin", inversedBy="offers" , fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $origin;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $pricing;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $complement;

    /**
     * @ORM\Column(type="boolean")
     */
    private $coordinatesRemoval;

    /**
     * @ORM\Column(type="integer")
     */
    private $billing = 1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nearestCity;

    /**
     * @ORM\Column(type="integer")
     */
    private $startingDateModularity = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\MedicalSpecialty" , fetch="EXTRA_LAZY")
     */
    private $specialty;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\OfferWishMatching", mappedBy="offer" , fetch="EXTRA_LAZY")
     */
    private $offerWishMatchings;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $startingDateModularityType;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $asSoonAsPossible = false;

    /**
     * @ORM\Column(type="json")
     */
    private $skills = [];

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Core\Person", mappedBy="offersSent" , fetch="EXTRA_LAZY")
     */
    private $receivers;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $onCall = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Department", inversedBy="offers" , fetch="EXTRA_LAZY")
     */
    private $department;

    /**
     * @ORM\Column(type="integer")
     */
    private $endDateModularity = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $endDateModularityType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\OfferHistorization", mappedBy="offer",fetch="EXTRA_LAZY")
     */
    private $oldStatus;

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->notes = new ArrayCollection();
        $this->contractors = new ArrayCollection();
        $this->offerWishMatchings = new ArrayCollection();
        $this->receivers = new ArrayCollection();
        $this->oldStatus = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getState(): ?int
    {
        return $this->state;
    }

    /**
     * @param int $state
     * @return Offer
     */
    public function setState(int $state): self
    {
        if (!in_array($state, self::STATES)) {
            throw new \InvalidArgumentException('$state must be defined in Offer::STATES array');
        }
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    /**
     * @param Note $note
     * @return Offer
     */
    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setOffer($this);
        }

        return $this;
    }

    /**
     * @param Note $note
     * @return Offer
     */
    public function removeNote(Note $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            if ($note->getOffer() === $this) {
                $note->setOffer(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|PersonNatural[]
     */
    public function getContractors(): Collection
    {
        return $this->contractors;
    }

    /**
     * @param PersonNatural $contractor
     * @return Offer
     */
    public function addContractor(PersonNatural $contractor): self
    {
        if (!$this->contractors->contains($contractor)) {
            $this->contractors[] = $contractor;
        }

        return $this;
    }

    /**
     * @param PersonNatural $contractor
     * @return Offer
     */
    public function removeContractor(PersonNatural $contractor): self
    {
        if ($this->contractors->contains($contractor)) {
            $this->contractors->removeElement($contractor);
        }
        return $this;
    }

    /**
     * @return Origin | null
     */
    public function getOrigin(): ?Origin
    {
        return $this->origin;
    }

    /**
     * @param Origin|null $origin
     * @return Offer
     */
    public function setOrigin(?Origin $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getId();
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return Offer
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPricing(): ?float
    {
        return $this->pricing;
    }

    /**
     * @param float $pricing
     * @return Offer
     */
    public function setPricing(float $pricing): self
    {
        $this->pricing = $pricing;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getComplement(): ?string
    {
        return $this->complement;
    }

    /**
     * @param string|null $complement
     * @return Offer
     */
    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCoordinatesRemoval(): ?bool
    {
        return $this->coordinatesRemoval;
    }

    /**
     * @param bool $coordinatesRemoval
     * @return Offer
     */
    public function setCoordinatesRemoval(bool $coordinatesRemoval): self
    {
        $this->coordinatesRemoval = $coordinatesRemoval;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBilling(): ?int
    {
        return $this->billing;
    }

    /**
     * @param int $billing
     * @return Offer
     */
    public function setBilling(int $billing): self
    {
        $this->billing = $billing;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNearestCity(): ?string
    {
        return $this->nearestCity;
    }

    /**
     * @param string|null $nearestCity
     * @return Offer
     */
    public function setNearestCity(?string $nearestCity): self
    {
        $this->nearestCity = $nearestCity;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStartingDateModularity(): ?int
    {
        return $this->startingDateModularity;
    }

    /**
     * @param int $startingDateModularity
     * @return Offer
     */
    public function setStartingDateModularity(int $startingDateModularity): self
    {
        $this->startingDateModularity = $startingDateModularity;

        return $this;
    }

    /**
     * @return MedicalSpecialty|null
     */
    public function getSpecialty(): ?MedicalSpecialty
    {
        return $this->specialty;
    }

    public function setSpecialty(MedicalSpecialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

    /**
     * @return Collection|OfferWishMatching[]
     */
    public function getOfferWishMatchings(): Collection
    {
        return $this->offerWishMatchings;
    }

    /**
     * @param OfferWishMatching $offerWishMatching
     * @return Offer
     */
    public function addOfferWishMatching(OfferWishMatching $offerWishMatching): self
    {
        if (!$this->offerWishMatchings->contains($offerWishMatching)) {
            $this->offerWishMatchings[] = $offerWishMatching;
            $offerWishMatching->setOffer($this);
        }

        return $this;
    }

    /**
     * @param OfferWishMatching $offerWishMatching
     * @return Offer
     */
    public function removeOfferWishMatching(OfferWishMatching $offerWishMatching): self
    {
        if ($this->offerWishMatchings->contains($offerWishMatching)) {
            $this->offerWishMatchings->removeElement($offerWishMatching);
            // set the owning side to null (unless already changed)
            if ($offerWishMatching->getOffer() === $this) {
                $offerWishMatching->setOffer(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfferWishMatchingNotActive()
    {
        $count = 0;
        foreach ($this->getOfferWishMatchings() as $offerWishMatching) {
            if (!$offerWishMatching->getIsActive()) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * @return int|null
     */
    public function getStartingDateModularityType(): ?int
    {
        return $this->startingDateModularityType;
    }

    /**
     * @param int|null $startingDateModularityType
     * @return Offer
     */
    public function setStartingDateModularityType(?int $startingDateModularityType): self
    {
        $this->startingDateModularityType = $startingDateModularityType;

        return $this;
    }

    /**
     * @return bool
     */
    public function getAsSoonAsPossible(): bool
    {
        return $this->asSoonAsPossible;
    }

    /**
     * @param bool $asSoonAsPossible
     * @return $this
     */
    public function setAsSoonAsPossible(bool $asSoonAsPossible): self
    {
        $this->asSoonAsPossible = $asSoonAsPossible;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartingDateOrAsap(): string
    {
        $result = 'Pas de date de début';
        if ($this->asSoonAsPossible === true) {
            $result = 'Dès que possible';
        } else {
            $date = $this->getStartingDate();
            if (isset($date)) {
                $result = $date->format('d/m/Y');
            }
        }
        return $result;
    }

    /**
     * @return Collection|Person[]
     */
    public function getReceivers(): Collection
    {
        return $this->receivers;
    }

    /**
     * @param Person $receiver
     * @return $this
     */
    public function addReceiver(Person $receiver): self
    {
        if (!$this->receivers->contains($receiver)) {
            $this->receivers[] = $receiver;
            $receiver->addOffersSent($this);
        }

        return $this;
    }

    /**
     * @param Person $receiver
     * @return $this
     */
    public function removeReceiver(Person $receiver): self
    {
        if ($this->receivers->contains($receiver)) {
            $this->receivers->removeElement($receiver);
            $receiver->removeOffersSent($this);
        }
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSkills(): ?array
    {
        return $this->skills;
    }

    /**
     * @param array $skills
     * @return $this
     */
    public function setSkills(array $skills): self
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOnCall(): ?array
    {
        return $this->onCall;
    }

    /**
     * @param array|null $onCall
     * @return $this
     */
    public function setOnCall(?array $onCall): self
    {
        $this->onCall = $onCall;
        return $this;
    }

    /**
     * @return Department|null
     */
    public function getDepartment(): ?Department
    {
        return $this->department;
    }

    /**
     * @param Department|null $department
     * @return $this
     */
    public function setDepartment(?Department $department): self
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEndDateModularity(): ?int
    {
        return $this->endDateModularity;
    }

    /**
     * @param int|null $endDateModularity
     * @return $this
     */
    public function setEndDateModularity(?int $endDateModularity): self
    {
        $this->endDateModularity = $endDateModularity;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getEndDateModularityType(): ?int
    {
        return $this->endDateModularityType;
    }

    /**
     * @param int|null $endDateModularityType
     * @return $this
     */
    public function setEndDateModularityType(?int $endDateModularityType): self
    {
        $this->endDateModularityType = $endDateModularityType;

        return $this;
    }

    /**
     * @return array
     */
    public function serialize(): array
    {
        return [
            'id' => $this->getId(),
            'creator' => $this->getCreator()->getNickname(),
            'origin' => $this->getOrigin()->getLabel(),
            'specialty' => $this->getSpecialty()->getOfficialLabel(),
            'state' => self::LABEL[$this->getState()],
            'type' => self::TYPES_LABEL[$this->getType()],
            'label' => $this->getCanvas()->getLabel(),
            'person' => $this->getPerson()->getName(),
            'startingDate' => $this->getStartingDateOrAsap(),
            'department' => $this->getDepartment()->getCode(),
            'endDate' => $this->getReadableEndDate(),
            'billing' => $this->getBilling(),
            'coordinatesRemoval' => $this->getCoordinatesRemoval(),
            'nearestCity' => $this->getNearestCity(),
            'onCall' => $this->getOnCall(),
            'pricing' => $this->getPricing(),
            'creationDate' => $this->getCreationDate(),
            'isModel' => $this->getIsModel(),
            'lastFullUpdateDate' => $this->getLastFullUpdateAt(),
            'lastUpdateAt' => $this->getLastUpdateDate(),
            'substitutionType' => SubstitutionType::LABEL[$this->getSubstitutionType()->getSubstitutionType()]
        ];
    }

    /**
     * Clear dates and their modularity
     * @return $this
     */
    public function fullClearDates(): self
    {
        $this->clearDates();
        $this->asSoonAsPossible = false;
        $this->startingDateModularity = 0;
        $this->startingDateModularityType = null;
        $this->endDateModularity = 0;
        $this->endDateModularityType = null;
        return $this;
    }

    /**
     * @return Collection|OfferHistorization[]
     */
    public function getOldStatus(): Collection
    {
        return $this->oldStatus;
    }

    /**
     * @param OfferHistorization $oldStatus
     * @return $this
     */
    public function addOldStatus(OfferHistorization $oldStatus): self
    {
        if (!$this->oldStatus->contains($oldStatus)) {
            $this->oldStatus[] = $oldStatus;
            $oldStatus->setOffer($this);
        }
        return $this;
    }

    /**
     * @param OfferHistorization $oldStatus
     * @return $this
     */
    public function removeOldStatus(OfferHistorization $oldStatus): self
    {
        if ($this->oldStatus->contains($oldStatus)) {
            $this->oldStatus->removeElement($oldStatus);
            // set the owning side to null (unless already changed)
            if ($oldStatus->getOffer() === $this) {
                $oldStatus->setOffer(null);
            }
        }
        return $this;
    }
}