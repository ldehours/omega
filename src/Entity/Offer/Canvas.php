<?php

namespace App\Entity\Offer;

use App\Traits\TranslatableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Offer\CanvasRepository")
 */
class Canvas
{
    use TranslatableTrait;

    const CANVAS_WISH = 1;
    const CANVAS_OFFER = 2;

    const LABEL = [
        self::CANVAS_WISH => "Souhait",
        self::CANVAS_OFFER => "Offre"
    ];

    const CANVAS_TYPES = [
        self::CANVAS_WISH,
        self::CANVAS_OFFER
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offer\Criterion")
     */
    private $criteria;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $formDisposition = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer\SubstitutionType", inversedBy="canvas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $substitutionType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer\Substitution", mappedBy="canvas")
     */
    private $substitutions;

    /**
     * @ORM\Column(type="integer")
     */
    private $canvasType = self::CANVAS_OFFER;

    /**
     * Canvas constructor.
     */
    public function __construct()
    {
        $this->criteria = new ArrayCollection();
        $this->substitutions = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Canvas
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Criterion[]
     */
    public function getCriteria(): Collection
    {
        return $this->criteria;
    }

    /**
     * @param Criterion $criterion
     * @return Canvas
     */
    public function addCriterion(Criterion $criterion): self
    {
        if (!$this->criteria->contains($criterion)) {
            $this->criteria[] = $criterion;
        }

        return $this;
    }

    /**
     * @param Criterion $criterion
     * @return Canvas
     */
    public function removeCriterion(Criterion $criterion): self
    {
        if ($this->criteria->contains($criterion)) {
            $this->criteria->removeElement($criterion);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->label;
    }

    /**
     * @return array|null
     */
    public function getFormDisposition(): ?array
    {
        return $this->formDisposition;
    }

    /**
     * @param array|null $formDisposition
     * @return Canvas
     */
    public function setFormDisposition(?array $formDisposition): self
    {
        $this->formDisposition = $formDisposition;

        return $this;
    }

    /**
     * @return SubstitutionType|null
     */
    public function getSubstitutionType(): ?SubstitutionType
    {
        return $this->substitutionType;
    }

    /**
     * @param SubstitutionType|null $substitutionType
     * @return Canvas
     */
    public function setSubstitutionType(?SubstitutionType $substitutionType): self
    {
        $this->substitutionType = $substitutionType;

        return $this;
    }

    /**
     * @return Collection|Substitution[]
     */
    public function getSubstitutions(): Collection
    {
        return $this->substitutions;
    }

    /**
     * @param Substitution $substitution
     * @return Canvas
     */
    public function addSubstitution(Substitution $substitution): self
    {
        if (!$this->substitutions->contains($substitution)) {
            $this->substitutions[] = $substitution;
            $substitution->setCanvas($this);
        }

        return $this;
    }

    /**
     * @param Substitution $substitution
     * @return Canvas
     */
    public function removeSubstitution(Substitution $substitution): self
    {
        if ($this->substitutions->contains($substitution)) {
            $this->substitutions->removeElement($substitution);
            // set the owning side to null (unless already changed)
            if ($substitution->getCanvas() === $this) {
                $substitution->setCanvas(null);
            }
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCanvasType(): ?int
    {
        return $this->canvasType;
    }

    /**
     * @param int $canvasType
     * @return Canvas
     */
    public function setCanvasType(int $canvasType): self
    {
        if (!in_array($canvasType, self::CANVAS_TYPES)) {
            throw new \InvalidArgumentException('$canvasType must be defined in Canvas::CANVAS_TYPES array');
        }
        $this->canvasType = $canvasType;
        return $this;
    }
}
