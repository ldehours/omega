<?php


namespace App\Traits;


use App\Utils\Translate;
use Exception;

trait TranslatableTrait
{
    /**
     * @return Translate
     * @throws Exception
     */
    public function getTranslate()
    {
        if (!defined('self::LABEL')) {
            throw new Exception('Please define LABEL constant in ' . self::class);
        }

        if (!defined('self::ICON')) {
            $icon = [];
        } else {
            $icon = self::ICON;
        }

        return new Translate(self::LABEL, $icon);

    }
}