<?php

namespace App\Traits;

use App\Annotation\NoTranslation;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\Person;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\Offer;
use App\Entity\Offer\SubstitutionType;
use App\Entity\Offer\Wish;
use App\Repository\Offer\CriterionRepository;
use DateInterval;
use DateTime;
use Exception;

trait MatchingTrait
{
    /**
     * @NoTranslation()
     * @param CriterionRepository $criterionRepository
     * @param Criterion $criterionInterval1
     * @param Criterion $criterionInterval2
     * @param Criterion $criterionPonderation
     * @param Criterion $criterionValeur
     * @param array $offerDates
     * @param MedicalSpecialty $offerSpecialty
     * @param array $arrayWishCriteria
     * @param array $arrayOfferCriteria
     * @param int $nbCriteresImperatifs
     * @param int $nbCriteresSecondaires
     * @param int $nbCriteresAdequationImperatifs
     * @param int $nbCriteresAdequationSecondaires
     * @return array
     * @throws Exception
     */
    public function matchingCriteria(
        CriterionRepository $criterionRepository,
        Criterion $criterionInterval1,
        Criterion $criterionInterval2,
        Criterion $criterionPonderation,
        Criterion $criterionValeur,
        array $offerDates,
        MedicalSpecialty $offerSpecialty,
        array $arrayWishCriteria,
        array $arrayOfferCriteria,
        int $nbCriteresImperatifs,
        int $nbCriteresSecondaires,
        int $nbCriteresAdequationImperatifs,
        int $nbCriteresAdequationSecondaires
    ): array {
        foreach ($arrayWishCriteria as $idCriterion => $arrayCriterion) { //pour chaque critère des souhaits
            $addCritereAdequationOk = false;
            if ($arrayCriterion['isRelevant'] === 2) {//critère très important -> c'est de lui que va découler le % de correspondance principalement.
                $nbCriteresImperatifs++;
            } elseif ($arrayCriterion['isRelevant'] === 1) {//critère moins important
                $nbCriteresSecondaires++;
            }

            if (array_key_exists($idCriterion, $arrayOfferCriteria)) {//si le critère existe coté offre
                if ($arrayCriterion['isRelevant'] === 1 || $arrayCriterion['isRelevant'] === 2) { //et s'il a une importance
                    switch ($arrayCriterion['criterion']->getLabel()) {
                        case 'Activité':
                            if (in_array('Tous secteurs', $arrayCriterion['value'], true)) {
                                $addCritereAdequationOk = true;
                            } elseif (in_array($arrayOfferCriteria[$idCriterion]['value'], $arrayCriterion['value'], true)) {
                                $addCritereAdequationOk = true;
                            }
                            break;
                        case 'Rétrocession':
                            if ($arrayCriterion['value'] <= $arrayOfferCriteria[$idCriterion]['value']) {
                                $addCritereAdequationOk = true;
                            }
                            break;
                        case 'Logiciel':
                            if (is_array($arrayCriterion['value']) && is_array($arrayOfferCriteria[$idCriterion]['value'])) {
                                if (!empty($arrayOfferCriteria[$idCriterion]['value']) && in_array('Oui', $arrayCriterion['value'], true)) {
                                    $addCritereAdequationOk = true;
                                } elseif (empty($arrayOfferCriteria[$idCriterion]['value']) && in_array('Non', $arrayCriterion['value'], true)) {
                                    $addCritereAdequationOk = true;
                                } elseif (count(array_intersect($arrayOfferCriteria[$idCriterion]['value'], $arrayCriterion['value'])) > 0) {
                                    $addCritereAdequationOk = true;
                                }
                            }
                            break;
                        case 'Astreintes':
                        case 'Gardes':
                            if ($arrayCriterion['value'] === $arrayOfferCriteria[$idCriterion]['value']) {
                                $addCritereAdequationOk = true;
                            }
                            break;
                        case 'Visites (%)':
                            if ($arrayCriterion['value'] === '-20%' && $arrayOfferCriteria[$idCriterion]['value'] < 20) {
                                $addCritereAdequationOk = true;
                            } elseif ($arrayCriterion['value'] === '-10%' && $arrayOfferCriteria[$idCriterion]['value'] < 10) {
                                $addCritereAdequationOk = true;
                            } elseif ($arrayCriterion['value'] === '-5%' && $arrayOfferCriteria[$idCriterion]['value'] < 5) {
                                $addCritereAdequationOk = true;
                            }
                            break;
                        case 'Logement nbr':
                            /**
                             * Le type de logement
                             * @var Criterion $logementType
                             */
                            $logementType = $arrayCriterion['criterion']->getCriterionHeadDependency();
                            if (array_key_exists($logementType->getId(), $arrayOfferCriteria)
                                && !in_array($arrayOfferCriteria[$logementType->getId()]['value'], ['Non logé', 'N/C'])) {
                                //on check si le nombre de personnes correspond
                                if (array_key_exists($idCriterion, $arrayOfferCriteria) &&
                                    (int)substr($arrayCriterion['value'], 0, 1) <=
                                    (int)substr($arrayOfferCriteria[$idCriterion]['value'], 0, 1)) {
                                    $addCritereAdequationOk = true;
                                }
                            }
                            break;
                        case 'Nombre de lits à gérer':
                            if ($arrayCriterion['value'] >= $arrayOfferCriteria[$idCriterion]['value']) {
                                $addCritereAdequationOk = true;
                            }
                            break;
                        case 'Prestation montant':
                            $addCritereAdequationOk = $this->checkPrestation(
                                $criterionRepository,
                                $arrayOfferCriteria,
                                $arrayWishCriteria,
                                $arrayCriterion,
                                $idCriterion
                            );
                            break;
                        default:
                            $addCritereAdequationOk = $this->checkByDefault($arrayOfferCriteria, $arrayCriterion, $idCriterion);
                            break;
                    }
                }
            } else {
                if ($arrayCriterion['criterion']->getLabel() === "Nombre d'actes /jour") {//ce critère a un comportement spécial.
                    $addCritereAdequationOk = $this->checkNombreActes(
                        $criterionInterval1,
                        $criterionInterval2,
                        $criterionPonderation,
                        $criterionValeur,
                        $arrayCriterion,
                        $arrayOfferCriteria,
                        $arrayWishCriteria
                    );
                } elseif ($arrayCriterion['criterion']->getLabel() === 'Département') {
                    $addCritereAdequationOk = true;
                } elseif ($arrayCriterion['criterion']->getLabel() === 'Durée de contrat recherchée') {
                    if (isset($offerDates['endMax'])) {
                        $weekQuantity = date_diff($offerDates['endMax'], $offerDates['startMin'])->format('%a');//nombre de jours de l'offre
                        if ($arrayCriterion['value'] === '1 à 2 semaines' && $weekQuantity >= 1 && $weekQuantity <= 14) {
                            $addCritereAdequationOk = true;
                        } elseif ($arrayCriterion['value'] === '3 à 4 semaines' && $weekQuantity >= 15 && $weekQuantity <= 28) {
                            $addCritereAdequationOk = true;
                        } elseif ($arrayCriterion['value'] === '1 mois et +' && $weekQuantity >= 29) {
                            $addCritereAdequationOk = true;
                        }
                    } else {
                        $addCritereAdequationOk = true;
                    }
                } elseif ($arrayCriterion['isRelevant'] !== 0 && $arrayCriterion['criterion']->getLabel() === 'Intérêt pour') {
                    if (in_array($offerSpecialty->getOfficialLabel(), $arrayCriterion['value'], true)) {
                        $addCritereAdequationOk = true;
                    }
                }
            }
            if ($addCritereAdequationOk) {
                if ($arrayCriterion['isRelevant'] === 2) {//critère très important -> c'est de lui que va découler le % de correspondance principalement.
                    $nbCriteresAdequationImperatifs++;
                } elseif ($arrayCriterion['isRelevant'] === 1) {//critère moins important
                    $nbCriteresAdequationSecondaires++;
                }
            }
        }
        return [
            'nbCriteresImperatifs' => $nbCriteresImperatifs,
            'nbCriteresAdequationImperatifs' => $nbCriteresAdequationImperatifs,
            'nbCriteresSecondaires' => $nbCriteresSecondaires,
            'nbCriteresAdequationSecondaires' => $nbCriteresAdequationSecondaires,
        ];
    }

    /**
     * Détermine la catégorie d'un souhait (Libéral / Salariat / Cession)
     * @NoTranslation()
     * @param Wish $wish
     * @return array
     */
    public function getArrayWishCriteria(
        Wish $wish
    ): array {
        $wishSubstitutionType = $wish->getSubstitutionType();
        if ($wishSubstitutionType === null) {
            return [];
        }
        $wishSubstitutionTypeCategory = SubstitutionType::TYPE_LIBERAL_REPLACEMENT;
        if (in_array(
            $wishSubstitutionType->getSubstitutionType(),
            SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SALARIED_POSITION],
            true
        )) {
            $wishSubstitutionTypeCategory = SubstitutionType::TYPE_SALARIED_POSITION;
        } else {
            if (in_array(
                $wishSubstitutionType->getSubstitutionType(),
                SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SELL_ASSOCIATION],
                true
            )) {
                $wishSubstitutionTypeCategory = SubstitutionType::TYPE_SELL_ASSOCIATION;
            }
        }

        $wishSubstitutionCriteria = $wish->getSubstitutionCriteria();
        $arrayWishCriteria = [];
        foreach ($wishSubstitutionCriteria as $substitutionCriterion) {
            $criterion = $substitutionCriterion->getCriterion();
            if ($criterion === null) {
                continue;
            }
            $arrayWishCriteria[$criterion->getId()] = [
                'criterion' => $criterion,
                'value' => $substitutionCriterion->getValue(),
                'isRelevant' => $substitutionCriterion->getIsRelevant()
            ];
        }
        return [
            'arrayWishCriteria' => $arrayWishCriteria,
            'wishSubstitutionTypeCategory' => $wishSubstitutionTypeCategory
        ];
    }

    /**
     * @NoTranslation()
     * @param Offer $offer
     * @param Wish $wish
     * @return bool
     * @throws Exception
     */
    public function doesDatesMatch(
        Offer $offer,
        Wish $wish
    ): bool {
        $offersDates = $this->getOfferDates($offer);
        $datesMatch = false;
        $now = new DateTime();

        if ($offersDates['endMax'] === null && $wish->getEndDate() === null) { // pas de DF (date de fin) sur offre ni sur souhait
            $datesMatch = true;
        } elseif ($offersDates['endMax'] === null && $wish->getEndDate() !== null) { // DF sur souhait mais pas sur offre
            if ($wish->getEndDate() > $now && $offersDates['startMin'] < $wish->getEndDate()) {
                $datesMatch = true;
            }
        } elseif ($offersDates['endMax'] !== null && $wish->getEndDate() === null) { // DF sur offre mais pas sur souhait
            if ($offersDates['endMax'] > $now && $wish->getStartingDate() < $offersDates['endMax']) {
                $datesMatch = true;
            }
        } else { // DF sur offre et sur souhait
            if ($offersDates['endMax'] > $now && $wish->getEndDate() > $now && $wish->getStartingDate() < $offersDates['endMax']) {
                $datesMatch = true;
            }
        }

        return $datesMatch;
    }

    /**
     * On récupère les "vraies" dates de l'offre => avec les modularités
     * @NoTranslation()
     * @param Offer $offer
     * @return array
     * @throws Exception
     */
    public function getOfferDates(Offer $offer): array
    {
        $offerEndDate = $offer->getEndDate();
        $offerStartMin = clone $offer->getStartingDate();
        $offerStartMax = clone $offer->getStartingDate();
        $offerEndMin = $offerEndDate !== null ? clone $offerEndDate : null;
        $offerEndMax = $offerEndDate !== null ? clone $offerEndDate : null;

        if ($offer->getStartingDateModularity() > 0) {
            $interval = new DateInterval('P' . $offer->getStartingDateModularity() . 'D');
            if (array_key_exists($offer->getStartingDateModularityType(), Offer::MODULARITY_TYPE_LABEL)) {
                switch ($offer->getStartingDateModularityType()) {
                    case Offer::MODULARITY_TYPE_BEFORE:
                        $offerStartMin->sub($interval);
                        break;
                    case Offer::MODULARITY_TYPE_AFTER:
                        $offerStartMax->add($interval);
                        break;
                    default:
                        $offerStartMin->sub($interval);
                        $offerStartMax->add($interval);
                        break;
                }
            } else {
                $offerStartMin->sub($interval);
                $offerStartMax->add($interval);
            }
        }

        if ($offerEndDate !== null && $offer->getEndDateModularity() > 0) {
            $interval = new DateInterval('P' . $offer->getEndDateModularity() . 'D');
            if (array_key_exists($offer->getEndDateModularityType(), Offer::MODULARITY_TYPE_LABEL)) {
                switch ($offer->getEndDateModularityType()) {
                    case Offer::MODULARITY_TYPE_BEFORE:
                        $offerEndMin->sub($interval);
                        break;
                    case Offer::MODULARITY_TYPE_AFTER:
                        $offerEndMax->add($interval);
                        break;
                    default:
                        $offerEndMin->sub($interval);
                        $offerEndMax->add($interval);
                        break;
                }
            } else {
                $offerEndMin->sub($interval);
                $offerEndMax->add($interval);
            }
        }

        return [
            'startMin' => $offerStartMin,
            'startMax' => $offerStartMax,
            'endMin' => $offerEndMin,
            'endMax' => $offerEndMax
        ];
    }

    /**
     * @NoTranslation()
     * @param CriterionRepository $criterionRepository
     * @param array $arrayOfferCriteria
     * @param array $arrayWishCriteria
     * @param array $arrayCriterion
     * @param int $idCriterion
     * @return bool
     */
    public function checkPrestation(
        CriterionRepository $criterionRepository,
        array $arrayOfferCriteria,
        array $arrayWishCriteria,
        array $arrayCriterion,
        int $idCriterion
    ): bool {
        $addCritereAdequationOk = false;
        $tauxJours = 21.75;
        $criterionPrestationBase = $criterionRepository->findOneByLabel('Prestation base');
        if (isset($criterionPrestationBase)
            && array_key_exists($criterionPrestationBase->getId(), $arrayOfferCriteria)
            && array_key_exists($criterionPrestationBase->getId(), $arrayWishCriteria)) {
            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'];
            switch ($arrayOfferCriteria[$criterionPrestationBase->getId()]['value']) {
                case 'Annuel':
                    switch ($arrayWishCriteria[$criterionPrestationBase->getId()]['value']) {
                        case 'Mensuel':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'] * 12;
                            break;
                        case 'Journalier':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'] * $tauxJours * 12;
                            break;
                        case 'Horaire':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'] * 7 * $tauxJours * 12;
                            break;
                    }
                    break;
                case 'Mensuel':
                    switch ($arrayWishCriteria[$criterionPrestationBase->getId()]['value']) {
                        case 'Annuel':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] * 12 - $arrayCriterion['value'];
                            break;
                        case 'Journalier':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'] * $tauxJours;
                            break;
                        case 'Horaire':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'] * 7 * $tauxJours;
                            break;
                    }
                    break;
                case 'Journalier':
                    switch ($arrayWishCriteria[$criterionPrestationBase->getId()]['value']) {
                        case 'Annuel':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] * $tauxJours * 12 - $arrayCriterion['value'];
                            break;
                        case 'Mensuel':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] * $tauxJours - $arrayCriterion['value'];
                            break;
                        case 'Horaire':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] - $arrayCriterion['value'] * 7;
                            break;
                    }
                    break;
                case 'Horaire':
                    switch ($arrayWishCriteria[$criterionPrestationBase->getId()]['value']) {
                        case 'Annuel':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] * 7 * $tauxJours * 12 - $arrayCriterion['value'];
                            break;
                        case 'Mensuel':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] * 7 * $tauxJours - $arrayCriterion['value'];
                            break;
                        case 'Journalier':
                            $montant = $arrayOfferCriteria[$idCriterion]['value'] * 7 - $arrayCriterion['value'];
                            break;
                    }
                    break;
            }
            if ($montant >= 0) {
                $addCritereAdequationOk = true;
            }
        }
        return $addCritereAdequationOk;
    }

    /**
     * @NoTranslation()
     * @param array $arrayOfferCriteria
     * @param array $arrayCriterion
     * @param int $idCriterion
     * @return bool
     */
    public function checkByDefault(
        array $arrayOfferCriteria,
        array $arrayCriterion,
        int $idCriterion
    ): bool {
        $addCritereAdequationOk = false;
        if (is_array($arrayOfferCriteria[$idCriterion]['value'])) {
            if (is_array($arrayCriterion['value'])) {
                //coté offre et wish -> array
                if (count(array_intersect($arrayOfferCriteria[$idCriterion]['value'], $arrayCriterion['value'])) > 0) {
                    $addCritereAdequationOk = true;
                }
            } else {
                //coté offre -> array | coté wish -> valeur
                if (in_array($arrayCriterion['value'], $arrayOfferCriteria[$idCriterion]['value'], true)) {
                    $addCritereAdequationOk = true;
                }
            }
        } else {
            if (is_array($arrayCriterion['value'])) {
                //coté offre -> valeur | wish -> array
                if (in_array($arrayOfferCriteria[$idCriterion]['value'], $arrayCriterion['value'], true)) {
                    $addCritereAdequationOk = true;
                }
            } else {
                //coté offre et wish -> valeur
                if ($arrayCriterion['value'] === $arrayOfferCriteria[$idCriterion]['value']) {
                    $addCritereAdequationOk = true;
                }
            }
        }
        return $addCritereAdequationOk;
    }

    /**
     * @NoTranslation()
     * @param Criterion $criterionInterval1
     * @param Criterion $criterionInterval2
     * @param Criterion $criterionPonderation
     * @param Criterion $criterionValeur
     * @param array $arrayCriterion
     * @param array $arrayOfferCriteria
     * @param array $arrayWishCriteria
     * @return bool
     */
    public function checkNombreActes(
        Criterion $criterionInterval1,
        Criterion $criterionInterval2,
        Criterion $criterionPonderation,
        Criterion $criterionValeur,
        array $arrayCriterion,
        array $arrayOfferCriteria,
        array $arrayWishCriteria
    ): bool {
        $addCritereAdequationOk = false;
        if (($arrayCriterion['isRelevant'] !== 0)
            && isset($criterionInterval1, $criterionInterval2)
            && array_key_exists($criterionInterval1->getId(), $arrayOfferCriteria)
            && array_key_exists($criterionInterval2->getId(), $arrayOfferCriteria)) {
            $valOfferInterval1 = $arrayOfferCriteria[$criterionInterval1->getId()]['value'];
            $valOfferInterval2 = $arrayOfferCriteria[$criterionInterval2->getId()]['value'];
            switch ($arrayCriterion['value']) {
                case '1 valeur':
                    if (isset($criterionPonderation, $criterionValeur)
                        && array_key_exists($criterionPonderation->getId(), $arrayWishCriteria)
                        && array_key_exists($criterionValeur->getId(), $arrayWishCriteria)) {
                        if ($arrayWishCriteria[$criterionPonderation->getId()]['value'] === 'au plus') {
                            if ($arrayWishCriteria[$criterionValeur->getId()]['value'] >= $valOfferInterval2) {
                                $addCritereAdequationOk = true;
                            }
                        } else {
                            if ($arrayWishCriteria[$criterionPonderation->getId()]['value'] === 'au moins') {
                                if ($arrayWishCriteria[$criterionValeur->getId()]['value'] <= $valOfferInterval1) {
                                    $addCritereAdequationOk = true;
                                }
                            }
                        }
                    }
                    break;
                case '1 intervalle':
                    if (array_key_exists($criterionInterval1->getId(), $arrayWishCriteria)
                        && array_key_exists($criterionInterval2->getId(), $arrayWishCriteria)
                        && $arrayWishCriteria[$criterionInterval1->getId()]['value'] >= $valOfferInterval1
                        && $arrayWishCriteria[$criterionInterval1->getId()]['value'] <= $valOfferInterval2
                        && $arrayWishCriteria[$criterionInterval2->getId()]['value'] >= $valOfferInterval1
                        && $arrayWishCriteria[$criterionInterval2->getId()]['value'] <= $valOfferInterval2) {
                        $addCritereAdequationOk = true;
                    }
                    break;
            }
        }
        return $addCritereAdequationOk;
    }

    /**
     * @NoTranslation()
     * @param int $nbCriteresImperatifs
     * @param int $nbCriteresAdequationImperatifs
     * @param int $nbCriteresSecondaires
     * @param int $nbCriteresAdequationSecondaires
     * @return float
     */
    public function getPercentage(
        int $nbCriteresImperatifs,
        int $nbCriteresAdequationImperatifs,
        int $nbCriteresSecondaires,
        int $nbCriteresAdequationSecondaires
    ): float {
        $percentage = 1;
        $tauxSuperieur = 0.8;
        $tauxInferieur = 0.2;
        //formules magiques, inch'
        if ($nbCriteresImperatifs === 0 && $nbCriteresSecondaires > 0) {
            $percentage = $tauxSuperieur + (($tauxInferieur / $nbCriteresSecondaires) * $nbCriteresAdequationSecondaires);
        } elseif ($nbCriteresImperatifs > 0 && $nbCriteresSecondaires === 0) {
            $percentage = $nbCriteresAdequationImperatifs / $nbCriteresImperatifs;
        } elseif ($nbCriteresImperatifs > 0 && $nbCriteresSecondaires > 0) {
            $percentage = ($nbCriteresAdequationImperatifs * $tauxSuperieur / $nbCriteresImperatifs) + ($nbCriteresAdequationSecondaires * $tauxInferieur / $nbCriteresSecondaires);
        }
        return (float)$percentage;
    }
}
