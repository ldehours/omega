<?php

namespace App\Security\Voters;

use App\Entity\Core\JobName;
use App\Entity\Core\Person;
use App\Entity\Core\PersonAccess;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PersonAccessVoter extends Voter
{

    /**
     * @inheritDoc
     */
    protected function supports($attribute, $subject): bool
    {
        if (!$subject instanceof Person) {
            return false;
        }

        //attribut existant dans ceux connus
        if (!in_array($attribute, PersonAccess::ACCESSES)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$subject instanceof Person) {
            return false;
        }
        foreach ($subject->getMainJobName()->getPersonAccesses() as $personAccessItem) {
            if ($personAccessItem->getType() === $attribute) {
                return true;
            }
        }

        if ($subject->getJobNames()->isEmpty()) {
            return false;
        }

        /** @var JobName $jobName */
        foreach ($subject->getJobNames() as $jobName) {
            if ($jobName->getPersonAccesses()->contains($attribute)) {
                return true;
            }
        }

        return false;
    }
}
