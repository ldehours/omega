<?php


namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class NoTranslation
 * @package App\Annotation
 * @Annotation
 * @Target("METHOD")
 */
class NoTranslation
{
    /*NoTranslation pour les fonctions Ajax dans les controlleurs */
}