<?php


namespace App\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Translation
 * @package App\Annotation
 * @Annotation
 * @Target("METHOD")
 */
class Translation
{
    /**
     * @Required
     * @var string
     */
    private $text;

    public function __construct(array $data)
    {
        $this->text = $data['value'];
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }


}