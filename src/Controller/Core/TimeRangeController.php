<?php


namespace App\Controller\Core;


use App\Annotation\Translation;
use App\Annotation\NoTranslation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Agenda;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\TimeRange;
use App\Form\Core\TimeRangeAjaxType;
use App\Form\Core\TimeRangeType;
use App\Repository\Core\AgendaRepository;
use App\Repository\Core\PersonRepository;
use App\Utils\DateTime;
use App\Utils\TimeRangeHelper;
use DateInterval;
use Exception;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * @Route("/core/timerange")
 */
class TimeRangeController extends BaseAbstractController
{
    /**
     * @Route("/new/{id}", name="timerange_new_ajax", methods={"POST"})
     * @Translation("Ajout d'une nouvelle période de temps")
     * @param Request $request
     * @param Agenda $agenda
     * @param TimeRangeHelper $timeRangeHelper
     * @return JsonResponse
     */
    public function newAjax(
        Request $request,
        Agenda $agenda,
        TimeRangeHelper $timeRangeHelper
    ): JsonResponse {
        $start = DateTime::createFromString($request->request->get('start'));
        $end = DateTime::createFromString($request->request->get('end'));

        if ($start > $end) {
            $temp = $start;
            $start = $end;
            $end = $temp;
        }

        $timerange = new TimeRange();
        $timerange->setCreator($this->getUser());
        $timerange->setStartDate($start);
        $timerange->setEndDate($end);
        $timerange->setNature($request->request->get('nature'));
        $timerange->setAgenda($agenda);

        $description = $request->request->get('description');
        if ($description !== '') {
            $timerange->setDescription($request->request->get('description'));
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($timerange);
        $entityManager->flush();
        return $this->successJsonResponse(['eventData' => $timeRangeHelper->getEventDataFromTimeRange($timerange)]);
    }

    /**
     * @Route("/new/timerange/{id}", name="timerange_new", methods={"GET","POST"})
     * @Translation("Ajout d'une nouvelle période de temps via formulaire")
     * @param Request $request
     * @param Agenda $agenda
     * @param PersonRepository $personRepository
     * @return Response
     * @throws Exception
     */
    public function new(
        Request $request,
        Agenda $agenda,
        PersonRepository $personRepository
    ): Response {
        $natures = TimeRange::NATURES_PERSON;
        $person = $personRepository->findOneByAgenda($agenda);
        if ($person === null) {
            $natures = array_diff(TimeRange::NATURES_STAFF, TimeRange::NATURES_STAFF_ALARM);
        }else if ($person instanceof PersonLegal){
            $this->addFlash('error', "Vous ne pouvez pas créer d'évènement directement depuis l'agenda d'un établissement");
            return $this->redirectToRoute('core_person_legal_show', ['id' => $person->getId()]);
        }

        $timerange = new TimeRange();
        $timerange->setCreator($this->getUser());
        $timerange->setAgenda($agenda);

        $form = $this->createForm(TimeRangeType::class, $timerange, ['natures' => $natures]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $requestValues = $request->request->get('time_range');
            $startDate = DateTime::createFromString($requestValues['startDate']);
            $endDate = DateTime::createFromString($requestValues['endDate']);
            if (!isset($startDate, $endDate)) {
                $this->addFlash('error', 'Un problème est survenu avec un de champs \'date\'');
                return $this->redirectToRoute(
                    'timerange_new',
                    [
                        'id' => $agenda->getId()
                    ]
                );
            }
            if ($startDate->getTimestamp() > $endDate->getTimestamp()) {
                $tmp = clone $startDate;
                $startDate = clone $endDate;
                $endDate = $tmp;
            }
            $timerange->setStartDate($startDate);
            $timerange->setEndDate($endDate);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($timerange);
            $entityManager->flush();
            $this->addFlash('success', 'Evènement créé du '.$timerange->getStartDate()->format('d/m/Y').' au '.$timerange->getEndDate()->format('d/m/Y'));
            return $this->redirectFromTimeRange(null, $agenda);
        }

        return $this->render(
            'Core/TimeRange/newTimeRange.html.twig',
            [
                'agenda' => $agenda,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/view/{id}", name="timerange_view_one", methods={"GET","POST"})
     * @Translation("Visualisation d'une période de temps")
     * @param TimeRange $timeRange
     * @param PersonRepository $personRepository
     * @param Request $request
     * @return Response
     */
    public function viewOneTimeRange(
        TimeRange $timeRange,
        PersonRepository $personRepository,
        Request $request
    ): Response {
        $agenda = $timeRange->getAgenda();
        if ($agenda === null) {
            throw new NotFoundResourceException('Une erreur est survenue lors de la liaison de la période avec l\'agenda');
        }
        $customer = null;
        $customerResult = null;

        /**
         * @var Person $person
         */
        $person = $personRepository->findOneBy(['agenda' => $agenda]);
        $natures = TimeRange::NATURES_PERSON;
        if ($person === null) {
            $natures = TimeRange::NATURES_STAFF;
            if(!in_array($timeRange->getNature(),TimeRange::NATURES_STAFF_ALARM)){
                $natures = array_diff(TimeRange::NATURES_STAFF, TimeRange::NATURES_STAFF_ALARM);
            }
        }

        $form = $this->createForm(TimeRangeType::class, $timeRange, ['natures' => $natures]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $requestValues = $request->request->get('time_range');
            $startDate = DateTime::createFromString($requestValues['startDate']);
            $endDate = DateTime::createFromString($requestValues['endDate']);
            if (!isset($startDate, $endDate)) {
                $this->addFlash('error', 'Un problème est survenu avec un des champs \'date\'');
                return $this->redirectToRoute(
                    'timerange_view_one',
                    [
                        'id' => $agenda->getId()
                    ]
                );
            }
            if ($startDate->getTimestamp() > $endDate->getTimestamp()) {
                $tmp = clone $startDate;
                $startDate = clone $endDate;
                $endDate = $tmp;
            }
            $timeRange->setStartDate($startDate);
            $timeRange->setEndDate($endDate);

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($timeRange);
            $manager->flush();

            $this->addFlash('success', 'Modifications enregistrées');
            return $this->redirectFromTimeRange($timeRange);
        }

        if (!isset($person) && $timeRange->getPerson()) {
            $customer = $timeRange->getPerson();
            if ($customer instanceof PersonNatural) {
                $customerResult['type'] = 'natural';
                $customerResult['person'] = $customer;
            } else {
                if ($customer instanceof PersonLegal) {
                    $customerResult['type'] = 'legal';
                    $customerResult['person'] = $customer;
                }
            }
        }

        return $this->render(
            'Core/TimeRange/ViewOneTimeRange.html.twig',
            [
                'timerange' => $timeRange,
                'customer' => $customerResult,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Translation("Récupérer tous les types de périodes de temps")
     * @Route("/nature/{nature}", name="timerange_nature", methods={"GET"})
     * @param string $nature
     * @return JsonResponse
     */
    public function getAllNature(string $nature): JsonResponse
    {
        $labels = TimeRange::LABEL;
        if ($nature === 'person') {
            $returnedLabel = [];
            $naturesType = TimeRange::NATURES_PERSON;
            foreach ($naturesType as $nat) {
                $returnedLabel[$nat] = $labels[$nat];
            }
            return new JsonResponse(
                [
                    'natures' => $returnedLabel
                ]
            );
        }
        $natureOther = TimeRange::NATURE_OTHER;
        $natureType = [
            'key' => $natureOther,
            'value' => $labels[$natureOther]
        ];
        return new JsonResponse(
            [
                'nature' => $natureType
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="timerange_edit_ajax", methods={"POST"})
     * @Translation("Modification d'une période de temps via agenda")
     * @param Request $request
     * @param TimeRange $timeRange
     * @return JsonResponse
     */
    public function editOneTimeRange(
        Request $request,
        TimeRange $timeRange
    ): JsonResponse {
        $manager = $this->getDoctrine()->getManager();
        $newStart = DateTime::createFromString($request->request->get('start'));
        $newEnd = DateTime::createFromString($request->request->get('end'));

        $timeRange->setStartDate($newStart);
        $timeRange->setEndDate($newEnd);

        $wish = $timeRange->getWish();
        if ($wish !== null) {
            $wish->setStartingDate($newStart);
            $wish->setEndDate($newEnd);
            $manager->persist($wish);
        }
        $manager->persist($timeRange);
        $manager->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/delete/{id}", name="timerange_delete_ajax", methods={"POST"})
     * @Translation("Suppression d'une période de temps via agenda")
     * @param TimeRange $timeRange
     * @return JsonResponse
     */
    public function ajaxDeleteOneTimeRange(TimeRange $timeRange): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($timeRange);
        $wish = $timeRange->getWish();
        if($wish !== null){
            $entityManager->remove($wish);
        }
        $entityManager->flush();

        return $this->successJsonResponse();
    }

    /**
     * @Route("/alarm/{id}/agenda/{agendaId}", name="define_alarm_recall", methods={"GET","POST"})
     * @Translation("Définir une évènement pour rappeller les clients")
     * @param Person $person
     * @param Request $request
     * @param AgendaRepository $agendaRepository
     * @param int $agendaId
     * @return Response
     * @throws Exception
     */
    public function defineAlarmForStaff(
        Person $person,
        Request $request,
        AgendaRepository $agendaRepository,
        int $agendaId
    ): Response {
        $user = $this->getUser();
        $agenda = $agendaRepository->findOneById($agendaId);
        $timerange = new TimeRange();
        $timerange->setCreator($user);
        $timerange->setAgenda($agenda);
        $timerange->setPerson($person);

        $form = $this->createForm(
            TimeRangeAjaxType::class,
            $timerange,
            [
                'action' => $this->generateUrl('define_alarm_recall',
                    ['id' => $person->getId(), 'agendaId' => $user->getAgenda()->getId()])
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $start = DateTime::createFromString($request->request->get('time_range_ajax')['startDate']);
            $end = DateTime::createFromString($request->request->get('time_range_ajax')['endDate']);
            if ($start > $end) {
                $temp = $start;
                $start = $end;
                $end = $temp;
            }
            $timerange->setStartDate($start);
            $timerange->setEndDate($end);
            $timerange->setNature($request->get('time_range_ajax')['nature']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($timerange);
            $entityManager->flush();

            $this->addFlash('success', 'Rappel enregistré ! :)');
            return $this->redirectToRoute('redirect_from_person_instance',['id' => $person->getId()]);
        }

        $defaultDate = new DateTime('+30 days');
        $defaultEndDate = new DateTime('+30 days');
        $defaultEndDate->add(new DateInterval('PT1H'));
        return $this->render(
            'Core/TimeRange/addAlarmForm.html.twig',
            [
                'person' => $person,
                'form' => $form->createView(),
                'default_date' => $defaultDate->format('d/m/Y H:i'),
                'default_end_date' => $defaultEndDate->format('d/m/Y H:i')
            ]
        );
    }

    /**
     * @Route("/redirect/from/{id}", name="timerange_redirect", methods={"GET"})
     * @Route("/redirect/from/agenda/{agendaId}", name="timerange_redirect_via_agenda", methods={"GET"})
     * @Translation("Revenir sur l'agenda en fonction du type de pesonne")
     * @param AgendaRepository $agendaRepository
     * @param TimeRange|null $timeRange
     * @param int|null $agendaId
     * @return RedirectResponse
     */
    public function redirectionFromTimerange(
        AgendaRepository $agendaRepository,
        ?TimeRange $timeRange = null,
        ?int $agendaId = null
    ): RedirectResponse {
        $agenda = null;
        if ($timeRange === null && $agendaId !== null) {
            $agenda = $agendaRepository->findOneById($agendaId);
        }
        if ($timeRange === null && $agenda === null) {
            throw new InvalidArgumentException();
        }
        return $this->redirectFromTimeRange($timeRange, $agenda);
    }

    /**
     * @Route("/delete/one/{id}", name="timerange_delete_one", methods={"GET"})
     * @Translation("Suppression d'une période de temps via sa vue")
     * @param TimeRange $timeRange
     * @return RedirectResponse
     */
    public function deleteOneTimeRange(
        TimeRange $timeRange
    ): RedirectResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($timeRange);
        $entityManager->flush();
        $this->addFlash('success', 'Période supprimée');
        return $this->redirectFromTimeRange($timeRange);
    }

    /**
     * @NoTranslation()
     * @param TimeRange|null $timeRange
     * @param Agenda|null $agenda
     * @return RedirectResponse
     */
    private function redirectFromTimeRange(?TimeRange $timeRange = null, ?Agenda $agenda = null): RedirectResponse
    {
        if ($agenda === null && $timeRange === null) {
            throw new InvalidArgumentException();
        }
        if ($agenda === null) {
            $agenda = $timeRange->getAgenda();
        }
        $personRepository = $this->getDoctrine()->getRepository(Person::class);

        /**
         * @var Person $person
         */
        $person = $personRepository->findOneByAgenda($agenda);
        if (isset($person)) {
            return $this->redirectToRoute('redirect_from_person_instance',['id' => $person->getId()]);
        } else {
            $response = $this->redirectToRoute('homepage');
        }
        return $response;
    }
}