<?php


namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Entity\Core\Agenda;
use App\Entity\Core\Staff;
use App\Form\Core\StaffType;
use App\Repository\Core\RoleRepository;
use App\Repository\Core\StaffHistoryRepository;
use App\Repository\Core\StaffRepository;
use App\Service\Historization\StaffHistorizationManager;
use App\Utils\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class StaffController
 * @package App\Controller\Core
 * @Route("/staff")
 */
class StaffController extends AbstractController
{
    /**
     * @Route("/", name="staff_index")
     * @Translation("Visualisation de la liste des Staff")
     * @param StaffRepository $staffRepository
     * @return Response
     */
    public function index(StaffRepository $staffRepository): Response
    {
        $staffs = $staffRepository->findBy([
            'isArchived' => false
        ]);

        return $this->render("Core/Staff/viewAllStaff.html.twig", [
            "staffs" => $staffs
        ]);
    }


    /**
     * @Route("/create", name="staff_new")
     * @Translation("Ajout d'un nouveau membre Staff")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param StaffHistorizationManager $staffHistorizationManager
     * @return Response
     * @throws \Exception
     */
    public function new(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        StaffHistorizationManager $staffHistorizationManager
    ): Response {
        $staff = new Staff();
        $form = $this->createForm(StaffType::class, $staff);
        $form->handleRequest($request);
        $staffRoles = [];

        if ($form->isSubmitted() && $form->isValid()) {

            $dateStart = $staff->getDateStart();
            $dateEnd = $staff->getDateEnd();
            $dateOfHiring = $staff->getDateOfHiring();

            if (($dateEnd != null && $dateStart->getTimestamp() > $dateEnd->getTimestamp()) || $dateStart->getTimestamp() < $dateOfHiring->getTimestamp()) {
                return $this->render('Core/Staff/newStaff.html.twig', [
                    'form' => $form->createView()
                ]);
            }

            if ($staff->getPassword() == null || $staff->getPassword() == '') {
                $form->get('password')->addError(new FormError('Le mot de passe ne peut être vide'));
            } else {
                // encode the plain password
                $staff->setPassword(
                    $passwordEncoder->encodePassword(
                        $staff,
                        $form->get('password')->getData()
                    )
                );
                foreach ($form->get('roles')->getData() as $role) {
                    $staffRoles[] = $role->getName();
                }

                $staff->setRoles($staffRoles);
                $staff->setIsActive(true);
                $staff->setIsArchived(false);

                $agenda = new Agenda();
                $staff->setAgenda($agenda);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($agenda);

                $entityManager->persist($staff);
                $entityManager->flush();

                $staffHistorizationManager->historize($staff, ['user' => $this->getUser()]);
                $staffHistorizationManager->save();

                $uploadedFile = $form['photo']->getData();
                $imageMimeType = ['image/png', 'image/jpeg', 'image/jpg'];
                if ($uploadedFile != null && in_array($uploadedFile->getMimeType(), $imageMimeType)) {
                    $destination = $this->getParameter('kernel.project_dir') . '/public' . $_ENV['STAFF_PICTURES_PATH'];
                    $newFilename = $staff->getNickname() . '.' . 'jpg';

                    $uploadedFile->move(
                        $destination,
                        $newFilename
                    );
                }

                $this->addFlash("success", "Staff créé avec succès");
                return $this->redirectToRoute("staff_index");
            }
        }

        return $this->render('Core/Staff/newStaff.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="staff_show", methods={"GET"})
     * @Translation("Visualisation d'un membre Staff")
     * @param Staff $staff
     * @return Response
     */
    public function show(Staff $staff): Response
    {
        $picturePath = file_exists('../public/uploads/staff_pictures/' . $staff->getNickname() . '.jpg') ? '/uploads/staff_pictures/' . $staff->getNickname() . '.jpg' : '/uploads/staff_pictures/placeholder.jpg';
        return $this->render('Core/Staff/viewOneStaff.html.twig', [
            'Staff' => $staff,
            "picture" => $picturePath
        ]);
    }

    /**
     * @Route("/edit/{id}",name="staff_edit")
     * @Translation("Modification d'un membre Staff")
     * @param Staff $staff
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @param StaffHistoryRepository $staffHistoryRepository
     * @param StaffRepository $staffRepository
     * @param StaffHistorizationManager $staffHistorizationManager
     * @return Response
     * @throws \Exception
     */
    public function edit(
        Staff $staff,
        Request $request,
        RoleRepository $roleRepository,
        StaffHistoryRepository $staffHistoryRepository,
        StaffRepository $staffRepository,
        StaffHistorizationManager $staffHistorizationManager
    ): Response {
        $staffPassword = $staff->getPassword();
        $destination = $this->getParameter('kernel.project_dir') . '/public' . $_ENV['STAFF_PICTURES_PATH'];

        $form = $this->createForm(StaffType::class, $staff);
        $form->handleRequest($request);
        $picturePath = $_ENV['STAFF_PICTURES_PATH'] . $staff->getNickname() . '.jpg';
        if (!file_exists($destination . '/' . $staff->getNickname() . '.jpg')) {
            $picturePath = $_ENV['STAFF_PICTURES_PATH'] . '/placeholder.jpg';
        }

        if ($form->isSubmitted()) {

            $description = '';
            $requestValues = $request->request->get('staff');
            $dateStart = DateTime::createFromString($requestValues['dateStart']);
            $dateEnd = DateTime::createFromString($requestValues['dateEnd']);

            if ($dateEnd != null && $dateStart->getTimestamp() > $dateEnd->getTimestamp()) {
                $this->addFlash('error', 'La date de fin ne peut pas être inférieure à la date de début');
                return $this->render("Core/Staff/editStaff.html.twig",
                    [
                        "staff" => $staff,
                        "form" => $form->createView(),
                        "roles" => $roleRepository->findAll(),
                        "staffRoles" => $staff->getRoles(),
                        "picture" => $picturePath
                    ]);
            }

            $dateOfHiring = DateTime::createFromString($requestValues['dateOfHiring']);
            $staffRoles = $request->request->get('roles') ?? [];

            $uploadedFile = $form['photo']->getData();
            $imageMimeType = ['image/png', 'image/jpeg', 'image/jpg'];
            if ($uploadedFile != null && in_array($uploadedFile->getMimeType(), $imageMimeType)) {
                $newFilename = $staff->getNickname() . '.' . 'jpg';
                $uploadedFile->move(
                    $destination,
                    $newFilename
                );
                $description .= 'modification de la photo du profil';
            }

            $entityManager = $this->getDoctrine()->getManager();
            $staff->setDateOfHiring($dateOfHiring);
            $staff->setDateStart($dateStart);
            $staff->setDateEnd($dateEnd);
            $staff->setPassword($staffPassword);
            $staff->setRoles($staffRoles);
            $entityManager->flush();

            $staffHistorizationManager->historize($staff,[
                'requestValues' => $requestValues,
                'description' => $description,
                'user' => $this->getUser(),
                'action' => 'edit'
            ]);
            $staffHistorizationManager->save();

            $this->addFlash("success", "Staff correctement modifié");
            return $this->redirectToRoute('staff_index');
        }

        return $this->render("Core/Staff/editStaff.html.twig",
            [
                "staff" => $staff,
                "form" => $form->createView(),
                "roles" => $roleRepository->findAll(),
                "staffRoles" => $staff->getRoles(),
                "picture" => $picturePath
            ]);
    }

    /**
     * @param Staff $staff
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @Route("/change/password/{id}",name="staff_change_password",methods={"GET","POST"})
     * @Translation("Changer Mot de Passe d'un membre Staff")
     */
    public function changePassword(
        Staff $staff,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response {

        $error = '';
        if ($request->isMethod('POST')) {
            $staffNewPassword = $request->request->get('staff')['password'];

            if ($staffNewPassword == null || $staffNewPassword == "") {
                $error = 'Le mot de passe ne doit pas être vide';

            } elseif (strlen($staffNewPassword) < 6) {
                $error = 'Mot de passe trop court';

            } else {

                $staff->setPassword($passwordEncoder->encodePassword($staff, $staffNewPassword));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($staff);
                $entityManager->flush();
                return $this->redirectToRoute('staff_index');
            }
        }

        $destination = 'uploads/staff_pictures/';
        $picturePath = $destination . $staff->getNickname() . '.jpg';

        if (!in_array($staff->getNickname() . '.jpg', scandir($destination))) {
            $picturePath = $destination . 'placeholder.jpg';
        }

        return $this->render("Core/Staff/changeStaffPassword.html.twig",
            [
                "staff" => $staff,
                'error' => $error,
                "picturePath" => $picturePath
            ]);
    }

    /**
     * @Route("/staffRoles",name="staff_roles", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param StaffRepository $staffRepository
     * @return JsonResponse
     */
    public function staffRoles(
        Request $request,
        StaffRepository $staffRepository
    ): JsonResponse {
        $staffId = $request->get('staffId');
        $staff = $staffRepository->find($staffId);
        $roles = $staff->getRoles();
        $rolesName = [];
        foreach ($roles as $role) {
            $rolesName[] = $role;
        }
        return new JsonResponse(['rolesName' => $rolesName]);
    }
}