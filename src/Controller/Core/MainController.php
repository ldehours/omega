<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Entity\Core\Staff;
use App\Utils\DashboardHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @Translation("Visualisation du tableau de bord")
     * ManagerRegistry $managerRegistry
     * @return Response
     * @throws Exception
     */
    public function homepage(ManagerRegistry $managerRegistry): Response
    {
        /** @var Staff $user */
        $user = $this->getUser();
        $isCfmlRA = $this->isGranted("ROLE_CFML_RESPONSABLE_ADMINISTRATIF");
        $isCommercial = $this->isGranted("ROLE_COMMERCIAL");
        $isCR = $this->isGranted("ROLE_CHARGE_DE_RECRUTEMENT");

        $results = AgendaController::getInfosForAgenda($user);

        // array pour les remplaçants datant de (- 1 semaine) ou (+1 semaine ou - 1 mois) CR1
        $recentSubstitutes = [
            "previousWeek" => [],
            "previousMonth" => []
        ];

        $infoServiceCards = [];

        $dashboardHelper = new DashboardHelper($managerRegistry);
        $dashboardHelper->getOfferByCreator($this->getUser());

        foreach ($user->getClientPortfolio() as $client) {
            // CR 1
            // $getPrevious[previousDate] = contient le previous soit month ou week / $getPrevious[data] = contient les données du client
            $previousInfo = $dashboardHelper->getPreviousWeekOrMonth($client);
            if ($previousInfo) {
                $recentSubstitutes[$previousInfo["period_length"]][] = $previousInfo["person_data"];
            }
        }

        $arrayReplacementOffersMonth = $dashboardHelper->getDataCR4();
        $arrayReplacementOffersThreeYears = $dashboardHelper->getDataCR3();
        $arrayReplacementOffersQP = $dashboardHelper->getDataCR5();
        $arrayCommercialFollowedContract = $dashboardHelper->getCommercial();

        if ($isCR) {
            $infoServiceCards[] = [
                'name_service' => 'Mes nouveaux remplaçants de la semaine et du mois',
                'title_service' => 'Tableau de bord des remplaçants du portefeuille de la semaine et du mois',
                'title_tab' => 'Semaine précédente',
                'title_tab_other' => 'Mois précédent',
                'number' => count($recentSubstitutes['previousWeek']) + count($recentSubstitutes['previousMonth']),
                'icon_name' => 'tim-icons icon-multiple-11',
                'array' => $recentSubstitutes['previousWeek'],
                'otherArray' => $recentSubstitutes['previousMonth']
            ];
            // add info pour le service de cards personnel des Portefeuille (remplaçants) ayant adhéré au CFML
            $infoServiceCards[] = [
                'name_service' => 'Remplaçants ayant adhéré au CFML',
                'title_service' => 'Tableau de bord des remplaçants de mon portefeuille ayant adhéré au CFML',
                'number' => '0',
                'icon_name' => 'tim-icons icon-wallet-43',
                'array' => false,
            ];
            // add info pour le service de cards personnel des Portefeuille (remplaçants) ayant adhéré au CFML CR3
            $infoServiceCards[] = [
                'name_service' => 'Remplacements libéraux ou Candidats salariés ayant travaillés par MS dans les 3 dernières années',
                'title_service' => 'Tableau de bord des remplacements libéraux ou salariés PAR MS dans les 3 dernières années',
                'number' => count($arrayReplacementOffersThreeYears),
                'icon_name' => 'tim-icons icon-zoom-split',
                'array' => $arrayReplacementOffersThreeYears,
            ];
            // add info pour des remplaçants de mon portefeuille pourvu des offres dans le mois en cours CR4
            $infoServiceCards[] = [
                'name_service' => 'Remplaçants/Candidats ayant pourvu des offres dans le mois en cours',
                'title_service' => 'Tableau de bord des remplaçants de mon portefeuille ayant pourvu des offres dans le mois en cours',
                'number' => count($arrayReplacementOffersMonth),
                'icon_name' => 'tim-icons icon-shape-star',
                'array' => $arrayReplacementOffersMonth,
            ];
            // listing des offres en QP avec les remplaçants des médecins / établissements de MON portefeuille
            $infoServiceCards[] = [
                'name_service' => 'Offres Quasiment Pourvues',
                'title_service' => 'Tableau de bord des listing des offres en QP avec les remplaçants des médecins / établissements de MON portefeuille',
                'number' => count($arrayReplacementOffersQP),
                'icon_name' => 'tim-icons icon-single-02',
                'array' => $arrayReplacementOffersQP,
            ];
            // des remplaçants de mon portefeuille ayant vu et/ou pris (avec coordonnées) des offres sur le site dans les 30 jours qui précèdent la date du jour
            $infoServiceCards[] = [
                'name_service' => 'Offres du site des 30 derniers jours',
                'title_service' => 'Tableau de bord des remplaçants de mon portefeuille ayant vu et/ou pris (avec coordonnées) des offres sur le site dans les 30 jours qui précèdent la date du jour',
                'number' => '0',
                'icon_name' => 'tim-icons icon-single-copy-04',
                'array' => false,
            ];
            // des remplaçants de mon portefeuille ayant vu et/ou pris (avec coordonnées) des offres sur le site dans les 30 jours qui précèdent la date du jour
            $infoServiceCards[] = [
                'name_service' => 'Remplaçants de mon portefeuille ayant été temporairement retirés des requêtes',
                'title_service' => 'Tableau de bord des remplaçants de mon portefeuille ayant été temporairement retirés des requêtes',
                'number' => '0',
                'icon_name' => 'tim-icons icon-scissors',
                'array' => false,
            ];
        }

        if ($isCfmlRA) {
            $infoServiceCards[] = [
                'name_service' => 'Adhésion à réaliser',
                'title_service' => 'Bulletin d\'adhésion validé par les clients',
                'number' => '0',
                'icon_name' => 'tim-icons icon-multiple-11',
                'array' => false
            ];
        }

        if ($isCommercial) {
            $infoServiceCards[] = [
                'name_service' => 'Offres Réussite sans accord en cours',
                'title_service' => 'Offres Réussite sans accord et sans accord suite concu qui sont en cours',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'fab fa-buffer',
                'array' => $arrayCommercialFollowedContract
            ];

            $infoServiceCards[] = [
                'name_service' => 'Cessions Réussite sans accord en cours',
                'title_service' => 'Cessions Réussite sans accord et sans accord suite concu qui sont en cours',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'fab fa-buffer',
                'array' => $arrayCommercialFollowedContract
            ];

            $infoServiceCards[] = [
                'name_service' => 'Suivi des docs envoyés',
                'title_service' => 'Suivi des documents envoyés(CFML, LMS, BNC, Privilège, Relais, Proposition Réussite) et dont le contact est antérieur à  1 semaine ou plus',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'tim-icons icon-multiple-11',
                'array' => $arrayCommercialFollowedContract
            ];

            $infoServiceCards[] = [
                'name_service' => ' Suivi des non renou Relais / Privilège du mois précédent',
                'title_service' => ' Suivi des non renou Relais / Privilège du mois précédent',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'tim-icons icon-multiple-11',
                'array' => $arrayCommercialFollowedContract
            ];

            $infoServiceCards[] = [
                'name_service' => ' Remplaçants ayant téléchargé le guide du remplaçant',
                'title_service' => ' Remplaçants ayant téléchargé le guide du remplaçant',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'tim-icons icon-multiple-11',
                'array' => $arrayCommercialFollowedContract
            ];

            $infoServiceCards[] = [
                'name_service' => ' Objectifs de ventes Mensuelles ',
                'title_service' => ' Indicateur d\'objectif des ventes mensuelles par région (CFML/LMS, Relais/Privilège, Offres, Cessions)',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'fas fa-signal',
                'array' => $arrayCommercialFollowedContract
            ];

            $infoServiceCards[] = [
                'name_service' => ' Objectifs de ventes Trimestrielles ',
                'title_service' => ' Indicateur d\'objectif des ventes trimestrielles par région et toutes régions confondues(CFML / LMS, Relais / Privilège, Offres, Cessions)',
                'number' => count($arrayCommercialFollowedContract),
                'id' => 'service_card_1',
                'icon_name' => 'fas fa-signal',
                'array' => $arrayCommercialFollowedContract
            ];
        }

        return $this->render(
            'Core/dashboard.html.twig',
            [
                'staff' => $user,
                'isCfmlRA' => $isCfmlRA,
                'isCommercial' => $isCommercial,
                'isCr' => $isCR,
                'tabTimeRangesWithPerson' => json_encode($results['tabTimeRangesWithPerson'], JSON_THROW_ON_ERROR),
                'tabNaturesWithNumberTimeRanges' => json_encode($results['tabNaturesWithNumberTimeRanges'], JSON_THROW_ON_ERROR),
                'infoServicesCards' => $infoServiceCards,

            ]
        );
    }
}