<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\MedicalSpecialtyQualification;
use App\Form\Core\MedicalSpecialtyQualificationType;
use App\Repository\Core\MedicalSpecialtyQualificationRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/medical/specialty/qualification")
 */
class MedicalSpecialtyQualificationController extends BaseAbstractController
{
    /**
     * @Route("/", name="core_medical_specialty_qualification_index", methods={"GET"})
     * @Translation("Visualisation des types de spécialités médicales")
     * @param MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository
     * @return Response
     */
    public function index(MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository): Response
    {
        return $this->render('Core/MedicalSpecialtyQualification/viewAllMedicalSpecialtyQualification.html.twig', [
            'medical_specialty_qualifications' => $medicalSpecialtyQualificationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="core_medical_specialty_qualification_new", methods={"POST"})
     * @Translation("Ajout d'un nouveau type de spécialité médicale")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $medicalSpecialtyQualification = new MedicalSpecialtyQualification();
        $form = $this->createForm(MedicalSpecialtyQualificationType::class, $medicalSpecialtyQualification,
            array('action' => $this->generateUrl('core_medical_specialty_qualification_new')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($medicalSpecialtyQualification);
            $entityManager->flush();

            return $this->redirectToRoute('core_medical_specialty_qualification_index');
        }

        return $this->render('Core/MedicalSpecialtyQualification/_form.html.twig', [
            'medical_specialty_qualification' => $medicalSpecialtyQualification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="core_medical_specialty_qualification_edit", methods={"POST"})
     * @Translation("Modification d'un type de spécialité médicale")
     * @param Request $request
     * @param MedicalSpecialtyQualification $medicalSpecialtyQualification
     * @return Response
     */
    public function edit(Request $request, MedicalSpecialtyQualification $medicalSpecialtyQualification): Response
    {
        $form = $this->createForm(MedicalSpecialtyQualificationType::class, $medicalSpecialtyQualification,
            array(
                'action' => $this->generateUrl('core_medical_specialty_qualification_edit',
                    array('id' => $medicalSpecialtyQualification->getId()))
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('core_medical_specialty_qualification_index', [
                'id' => $medicalSpecialtyQualification->getId(),
            ]);
        }

        return $this->render('Core/MedicalSpecialtyQualification/_form.html.twig', [
            'medical_specialty_qualification' => $medicalSpecialtyQualification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="core_medical_specialty_qualification_delete", methods={"DELETE"})
     * @Translation("Suppression d'un type de spécialité médicale")
     * @param Request $request
     * @param MedicalSpecialtyQualification $medicalSpecialtyQualification
     * @return JsonResponse
     */
    public function delete(Request $request, MedicalSpecialtyQualification $medicalSpecialtyQualification): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($medicalSpecialtyQualification);
        $entityManager->flush();

        return $this->successJsonResponse();
    }
}
