<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\JobName;
use App\Form\Core\JobNameType;
use App\Repository\Core\JobNameRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/jobname")
 */
class JobNameController extends BaseAbstractController
{
    /**
     * @Route("/", name="core_job_name_index", methods={"GET"})
     * @Translation("Visualisation de la liste des catégories de clients")
     * @param JobNameRepository $jobNameRepository
     * @return Response
     */
    public function index(JobNameRepository $jobNameRepository): Response
    {
        return $this->render('Core/JobName/viewAllJobName.html.twig', [
            'jobNames' => $jobNameRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="core_job_name_new", methods={"POST"})
     * @Translation("Ajout d'une nouvelle catégorie de client")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $jobName = new JobName();
        $form = $this->createForm(JobNameType::class, $jobName,
            array('action' => $this->generateUrl('core_job_name_new')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($jobName);
            $entityManager->flush();

            return $this->redirectToRoute('core_job_name_index');
        }

        return $this->render('/Core/JobName/_form.html.twig', [
            'JobName' => $jobName,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="core_job_name_edit", methods={"POST"})
     * @Translation("Modification d'une catégorie de client")
     * @param Request $request
     * @param JobName $jobName
     * @return Response
     */
    public function edit(Request $request, JobName $jobName): Response
    {
        $form = $this->createForm(JobNameType::class, $jobName,
            array('action' => $this->generateUrl('core_job_name_edit', array('id' => $jobName->getId()))));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('core_job_name_index');
        }

        return $this->render('/Core/JobName/_form.html.twig', [
            'JobName' => $jobName,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="core_job_name_delete", methods={"DELETE"})
     * @Translation("Suppression d'une catégorie de client")
     * @param Request $request
     * @param JobName $jobName
     * @return Response
     */
    public function delete(Request $request, JobName $jobName): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($jobName);
        $entityManager->flush();

        return $this->successJsonResponse();
    }
}
