<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Repository\Core\StaffRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @Translation("Se connecter à OMEGA")
     * @param AuthenticationUtils $authenticationUtils
     * @param StaffRepository $staffRepository
     * @param Request $request
     * @return Response
     */
    public function login(
        AuthenticationUtils $authenticationUtils,
        StaffRepository $staffRepository,
        Request $request
    ): Response {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        //Search username by IP
        $staff = $staffRepository->findOneByIp($request->getClientIp());
        $lastUsername = $staff ? $staff->getNickname() : '';


        return $this->render('Core/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }


    /**
     * @Route("/logout", name="app_logout")
     * @Translation("Se déconnecter d'OMEGA")
     */
    public function logout(): void
    {
        /*
         * This function work because Symfony knows what to do for logout someone
         * Juste create this function is necessary
         */
    }


}
