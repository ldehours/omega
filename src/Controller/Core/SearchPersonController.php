<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Entity\Core\JobName;
use App\Entity\Core\PersonLegal;
use App\Form\Core\SearchPersonType;
use App\Repository\Core\PersonLegalRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Repository\Core\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/person/search")
 */
class SearchPersonController extends AbstractController
{
    /**
     * @Route("/", name="core_search_person")
     * @Translation("Accès à la recherche multi-critères")
     * @param Request $request
     * @param PersonRepository $personRepository
     * @param PersonNaturalRepository $personNaturalRepository
     * @param PersonLegalRepository $personLegalRepository
     * @return Response
     */
    public function index(
        Request $request,
        PersonRepository $personRepository,
        PersonNaturalRepository $personNaturalRepository,
        PersonLegalRepository $personLegalRepository
    ): Response {
        $jobName = new JobName();
        $form = $this->createForm(SearchPersonType::class);
        $form->handleRequest($request);
        $persons = [
            'natural' => [],
            'legal' => []
        ];

        if ($form->isSubmitted()) {
            //on doit enlever les clés où il n'y a pas de valeur
            $optionsRequested = array_filter($request->get("search_person"));

            /*
             * Attention, le $requestNumberElements permet de vérifier si au moins un champs a ete rempli par l'utilisateur,
             * Comme de base le formulaire contient le jeton CSRF, on va faire des vérif plus bas pour
             * savoir si la taille du tableau de paramètres est supérieure ou non à 1
             */
            $requestNumberElements = count($optionsRequested);

            if ($requestNumberElements <= 1 || ($requestNumberElements === 2 &&
                    array_key_exists('exactResearch', $optionsRequested)) || $form->isEmpty()) {
                $this->addFlash('warning', 'Veuillez remplir au moins un des critères.');
                return $this->redirectToRoute('core_search_person');
            }

            if (!$form->isValid()) {
                $this->addFlash('warning', 'Un des champs saisi est trop court.');
                return $this->redirectToRoute('core_search_person');
            }

            $requestById = array_key_exists('id', $optionsRequested);
            if (array_key_exists('firstName', $optionsRequested) || $request->get("option_natural") === "true") { // search on person natural
                if ($requestById) {
                    $persons["natural"] = $personNaturalRepository->findOneBy(['id' => $optionsRequested['id']]) ?: [];
                } else {
                    $persons["natural"] = $personNaturalRepository->findByMultiCritera($optionsRequested);
                }
            } elseif ($request->get("option_legal") === "true") { // search on person legal
                if ($requestById) {
                    $persons["legal"] = $personLegalRepository->findOneBy(['id' => $optionsRequested['id']]) ?: [];
                } else {
                    $persons["legal"] = $personLegalRepository->findByMultiCritera($optionsRequested);
                }
            } else { // search on both person natural and legal
                if ($requestById) {
                    $person = $personRepository->findOneBy(['id' => $optionsRequested['id']]);
                    if ($person !== null) {
                        if (get_class($person) === PersonLegal::class) {
                            $persons["legal"] = [$person];
                        } else {
                            $persons["natural"] = [$person];
                        }
                    }
                } else {
                    $persons["natural"] = $personNaturalRepository->findByMultiCritera($optionsRequested);
                    $persons["legal"] = $personLegalRepository->findByMultiCritera($optionsRequested);
                }
            }

            if (count($persons["natural"]) > 250 || count($persons["legal"]) > 250) {
                $this->addFlash(
                    "error",
                    "Le nombre de personnes trouvées est trop élevé (>250). Soyez plus précis dans la recherche."
                );
                return $this->redirectToRoute('core_search_person');
            }

            $result = [];
            foreach ($persons as $personType => $listPerson) {
                foreach ($listPerson as $person) {
                    $result[$person->getId()] = ['person_type' => $personType, 'person' => $person];
                }
            }

            if (count($result) === 1) {
                $firstPersonArray = $result[array_key_first($result)];
                switch ($firstPersonArray['person_type']) {
                    case 'natural':
                        $redirectRoute = 'core_person_natural_show';
                        break;
                    case 'legal':
                        $redirectRoute = 'core_person_legal_show';
                        break;
                }

                return $this->redirectToRoute(
                    $redirectRoute,
                    ['id' => $firstPersonArray['person']->getId()]
                );
            }

            return $this->render(
                'Core/SearchPerson/_form.html.twig',
                [
                    'isPageSubmitted' => true,
                    'persons' => $result,
                    'jobName' => $jobName,
                    'form' => $form->createView()
                ]
            );
        }

        return $this->render(
            'Core/SearchPerson/_form.html.twig',
            [
                'persons' => $persons,
                'jobName' => $jobName,
                'form' => $form->createView()
            ]
        );
    }
}