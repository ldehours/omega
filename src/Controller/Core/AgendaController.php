<?php


namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Annotation\NoTranslation;
use App\Entity\Core\Agenda;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Core\TimeRange;
use App\Form\Core\TimeRangeCalendarType;
use App\Repository\Core\PersonRepository;
use App\Repository\Core\StaffRepository;
use App\Utils\DateTime;
use DateInterval;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;


/**
 * @Route("/core/agenda")
 */
class AgendaController extends AbstractController
{
    /**
     * @Route("/", name="agenda_view", methods={"GET"})
     * @Translation("Agenda")
     * @param Request $request
     * @return Response
     */
    public function calendar(Request $request): Response
    {
        if ($request->query->get('id')) {
            return $this->redirectToRoute('timerange_view_one', ['id' => $request->query->get('id')]);
        }
        return $this->render('BaseTheme/calendar.html.twig');
    }

    /**
     * @Route("/annual/{id}/{type}/", name="homepage_annual_agenda", methods={"POST","GET"})
     * @Route("/annual/{id}/{type}/{actionParameter}/{monthParameter}/{yearsParameter}", name="homepage_annual_agenda_calendar", methods={"POST","GET"})
     * @Translation("Visualisation de l'agenda annuel")
     * @param Agenda $agenda
     * @param string $type
     * @param string $actionParameter
     * @param string $monthParameter
     * @param string $yearsParameter
     * @param PersonRepository $personRepository
     * @param StaffRepository $staffRepository
     * @return Response
     * @throws Exception
     */
    public function annualAgenda(
        Agenda $agenda,
        string $type,
        string $actionParameter = null,
        string $monthParameter = null,
        string $yearsParameter = null,
        PersonRepository $personRepository,
        StaffRepository $staffRepository,
        Request $request
    ): Response {

        switch ($type) {
            case 'personNatural':
            case 'personLegal':
                $agendaOwner = $personRepository->findOneByAgenda($agenda);
                break;
            default:
                if ($type != "staff") {
                    $type = "staff";//on met le type par défaut à staff.
                }
                $agendaOwner = $staffRepository->findOneByAgenda($agenda);
                break;
        }
        if (!isset($agendaOwner)) {
            $this->addFlash("error", "Erreur lors de la génération de l'agenda annuel");
            return $this->redirectToRoute("core_search_person");
        }
        $result = self::getInfosForAgenda($agendaOwner);

        // form type pour un évenement sur le calendar
        $eventCalendar = new TimeRange();
        $formCalendar = $this->createForm(TimeRangeCalendarType::class, $eventCalendar);
        $formCalendar->handleRequest($request);

        if ($formCalendar->isSubmitted()) {
            $task = $formCalendar->getData();
            $start = DateTime::createFromString($request->request->get('time_range_calendar')['startDate']);
            $end = DateTime::createFromString($request->request->get('time_range_calendar')['endDate']);
            $nature = $task->getNature();

            $eventCalendar->setStartDate($start);
            $eventCalendar->setEndDate($end);
            $eventCalendar->setNature($nature);
            $eventCalendar->setAgenda($agenda);
            $eventCalendar->setCreator($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eventCalendar);
            $entityManager->flush();

            $agendaId = $agenda->getId();

            if ($monthParameter && $yearsParameter && $actionParameter) {
                return $this->redirectToRoute("homepage_annual_agenda_calendar", [
                    "id" => $agendaId,
                    "type" => $type,
                    "actionParameter" => $actionParameter,
                    "monthParameter" => $monthParameter,
                    "yearsParameter" => $yearsParameter
                ]);
            } else {
                return $this->redirectToRoute("homepage_annual_agenda", ["id" => $agendaId, "type" => $type], 301);
            }
        }
        $month = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"
        ];
        $positionDays = [
            ['days' => "Mon", 'position' => 0],
            ['days' => "Tue", 'position' => 1],
            ['days' => "Wed", 'position' => 2],
            ['days' => "Thu", 'position' => 3],
            ['days' => "Fri", 'position' => 4],
            ['days' => "Sat", 'position' => 5],
            ['days' => "Sun", 'position' => 6]
        ];
        // contient les mois du calendrier avec le mois actuel
        $monthCalendarActual = [];
        $datetime = new \DateTime();
        $datetimeYears = new \DateTime();

        // si ces variables existes, l'utilisateur à cliqué sur le btn n+ 1 ou n - 1
        if ($monthParameter && $yearsParameter && $actionParameter) {

            $monthQuery = $monthParameter;
            $yearsQuery = $yearsParameter;
            // représente l'action du l'utlisateur si n + 1 (next) ou n - 1 (back)
            $actionQuery = $actionParameter;
            // l'utilisateur veut aller à n'année n + 1
            if ($actionQuery === "next") {
                $datetime = new \DateTime("$yearsQuery-$monthQuery-01");
                $datetimeYears = new \DateTime("$yearsQuery-$monthQuery-01");
            }
            // l'utilisateur veut aller à n'année n - 1
            if ($actionQuery === "back") {
                $yearsQuery = intval($yearsQuery) - 1;
                $datetime = new \DateTime("$yearsQuery-$monthQuery-01");
                $datetimeYears = new \DateTime("$yearsQuery-$monthQuery-01");
            }
        }
        // inclu le mois actuel ainsi que son année
        $monthCalendarActual[] = [
            "month" => $datetime->format('M'),
            "years" => $datetime->format('Y')
        ];
        // boucle sur les mois suivant le mois actuel
        // ex (Mois actuel Juillet = ( Aout 2020 , Septembre 2020 , ... , Juin 2021 )
        for ($i = 0; $i < 11; $i++) {
            $currentMonth = $datetime->modify('+1 month')->format('M');
            $currentYears = $datetimeYears->modify('+1 month')->format('Y');
            $monthCalendarActual[] = [
                "month" => $currentMonth,
                "years" => $datetime->format('Y')
            ];
        }

        // boucle sur l'ensemble du tableau $monthCalendarActual qui permet de récupérer plusieurs info concernant le mois[$i]
        // ex pour Juillet récupère
        //  - le nbr de jours dans le mois
        //  - la position du mois
        //  - récupère le premier jour du mois (ex Lundi )
        // ....
        for ($i = 0; $i < count($monthCalendarActual); $i++) {
            // récupère le mois en format string (ex: Jull)
            $currentMonth = $monthCalendarActual[$i]['month'];
            // récupère l'année du mois (ex 2020 pour Juillet )
            $currentYears = $monthCalendarActual[$i]['years'];
            // récupère la position du mois ( return un chiffre ) (ex mois de Juillet position 6, return donc int 6)
            $positionMonth = array_search($currentMonth, $month);
            // récupère le premier jour du mois (ex : Lundi )
            $firstday = new DateTime("$currentYears-$currentMonth-01");
            // contient le nbr de jours dans le mois
            $number = cal_days_in_month(CAL_GREGORIAN, $positionMonth + 1, $monthCalendarActual[$i]['years']);
            // récupère la position du premier jour du mois concerné (ex Aout 2020 commence par Samedi, le 6ème jour : position 6 )
            $posDayStart = null;
            for ($j = 0; $j < count($positionDays); $j++) {
                if ($firstday->format('D') === $positionDays[$j]['days']) {
                    $posDayStart = $positionDays[$j]['position'];
                }
            }
            $monthCalendarActual[$i] +=
                [
                    "days" => $number,
                    "day_start" => $firstday->format('D'),
                    'first_day_index' => $posDayStart
                ];
        }
        $timeRangeClient = [];
        foreach ($agenda->getTimeRanges() as $timerange) {
            $color = "";
            foreach (TimeRange::NATURES_PERSON as $nature) {
                $color = TimeRange::NATURES_COLOR[$timerange->getNature()];
            }
            $id = $timerange->getId();
            $start = $timerange->getStartDate();
            $end = $timerange->getEndDate();
            $nature = $timerange->getNature();
            $week = $timerange->getWeekDay();

            if ($week != null) {
                $week = $week->getValue();
            }

            array_push($timeRangeClient, [
                "id" => $id,
                "start" => $start->format('Y/m/d'),
                "start_day" => $start->format('d'),
                "start_month_int" => $start->format('m'),
                "start_month_str" => $start->format('M'),
                "start_years" => $start->format('Y'),
                "end" => $end->format('Y/m/d'),
                "end_day" => $end->format('d'),
                "end_month_int" => $end->format('m'),
                "end_month_str" => $end->format('M'),
                "end_years" => $end->format('Y'),
                "week" => $week,
                "color" => $color,
                "nature" => $nature
            ]);
        }
        // array qui va contenir lesdates pour les btn n +1 ou n - 1
        $btncalendarDate = [
            "yearsNext" => $datetimeYears->format('Y'),
            "yearsBack" => $datetimeYears->modify('-1 years')->format('Y')
        ];

        return $this->render(
            'Core/annualAgenda.html.twig',
            [
                'type' => $type,
                'agendaOwner' => $agendaOwner,
                'tabTimeRangesWithPerson' => json_encode($result['tabTimeRangesWithPerson']),
                'tabNaturesWithNumberTimeRanges' => json_encode($result['tabNaturesWithNumberTimeRanges']),
                'calendar' => json_encode($monthCalendarActual),
                'formCalendar' => $formCalendar->createView(),
                'timeRange' => json_encode($timeRangeClient),
                'agenda' => $agenda->getId(),
                'btncalendarDate' => $btncalendarDate,
            ]
        );
    }

    /**
     * @param Staff|Person $agendaOwner
     * @return array
     * @throws Exception
     * @NoTranslation()
     */
    public static function getInfosForAgenda($agendaOwner): array
    {
        $result = [];
        $tabTimeRangesWithPerson = [];
        $tabNaturesWithNumberTimeRanges = [];
        foreach (TimeRange::NATURES_STAFF_ALARM as $nature) {
            $tabNaturesWithNumberTimeRanges[TimeRange::LABEL[$nature]] = 0;
        }
        $newDate = new DateTime();
        foreach ($agendaOwner->getAgenda()->getTimeRanges() as $timeRange) {
            $person = $timeRange->getPerson();
            $nature = $timeRange->getNature();
            if ($person != null) {
                if ($person instanceof PersonNatural) {
                    $tabTimeRangesWithPerson[$timeRange->getId()] = [
                        "nature" => TimeRange::LABEL[$nature],
                        "person" => [
                            "type" => "natural",
                            "id" => $person->getId(),
                            "lastName" => $person->getLastName(),
                            "firstName" => $person->getFirstName()
                        ]
                    ];
                } elseif ($person instanceof PersonLegal) {
                    $tabTimeRangesWithPerson[$timeRange->getId()] = [
                        "nature" => TimeRange::LABEL[$nature],
                        "person" => [
                            "type" => "legal",
                            "id" => $person->getId(),
                            "corporateName" => $person->getCorporatename()
                        ]
                    ];
                }
            }
            if (in_array($nature,
                    TimeRange::NATURES_STAFF_ALARM) && $timeRange->getStartDate()->format('d/m/Y') == $newDate->format('d/m/Y')) {
                $tabNaturesWithNumberTimeRanges[TimeRange::LABEL[$nature]]++;
            }
        }
        $result['tabTimeRangesWithPerson'] = $tabTimeRangesWithPerson;
        $result['tabNaturesWithNumberTimeRanges'] = $tabNaturesWithNumberTimeRanges;
        return $result;
    }
}