<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Entity\Core\Agenda;
use App\Entity\Core\ContactInstitution;
use App\Entity\Core\JobName;
use App\Entity\Core\Origin;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Form\Core\ContactInstitutionType;
use App\Repository\Core\ContactInstitutionRepository;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\OriginRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Service\CustomValidator\Core\ContactInstitutionValidator;
use App\Utils\DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/contact/institution")
 */
class ContactInstitutionController extends AbstractController
{
    /**
     * @Route("/{id}/new", name="contact_institution_new", methods={"GET","POST"})
     * @Translation("Créer un nouveau contact d'établissement")
     * @param PersonLegal $personLegal
     * @param Request $request
     * @param OriginRepository $originRepository
     * @param JobNameRepository $jobNameRepository
     * @return Response
     * @throws \Exception
     */
    public function createContactForInstitution(
        PersonLegal $personLegal,
        Request $request,
        PersonNaturalRepository $personNaturalRepository,
        ContactInstitutionRepository $contactInstitutionRepository,
        OriginRepository $originRepository,
        JobNameRepository $jobNameRepository
    ): Response {
        $contactInstitution = new ContactInstitution();
        $contactInstitution->setPersonLegal($personLegal);

        /** @var JobName $contactJobName */
        $contactJobName = $jobNameRepository->findOneBy(
            [
                'classification' => JobName::INSTITUTION_FUNCTION,
                'label' => 'Contact établissement'
            ]
        );

        if (!isset($contactJobName)) {
            $contactJobName = $jobNameRepository->findOneBy([]);
        }

        $contactInstitution->setFunction($contactJobName);

        $form = $this->createForm(ContactInstitutionType::class, $contactInstitution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var ContactInstitution $contactInstitution */
            $contactInstitution = $form->getData();
            $validator = new ContactInstitutionValidator($request, $originRepository);
            $errors = $validator->validate();

            if (!empty($errors)) {
                $message = '';
                foreach ($errors as $error) {
                    $message .= $error . ' ';
                }
                $this->addFlash('error', $message);
                return $this->redirectToRoute('contact_institution_new', ['id' => $personLegal->getId()]);
            }
            $entityManager = $this->getDoctrine()->getManager();

            $personNatural = new PersonNatural();
            $personNatural->setFirstName($request->request->get('firstname'));
            $personNatural->setLastName($request->request->get('lastname'));
            $origin = $originRepository->findOneBy(['id' => $request->request->get('origin')]);
            $personNatural->setOrigin($origin);
            $personNatural->setAgenda(new Agenda());
            $personNatural->setCreator($this->getUser());
            $personNatural->setCreationDate(new DateTime());
            $personNatural->setJobStatus([]);
            $personNatural->addPersonLegal($personLegal);
            $personNatural->setMainJobName($contactInstitution->getFunction());
            $personNatural->setGender(PersonNatural::GENDER_OTHER);

            $entityManager->persist($personNatural);

            $contactInstitution->setPersonNatural($personNatural);

            if (!$contactInstitution->getLabel()) {
                $contactInstitution->setLabel($contactInstitution->getFunction() . ', ' . $contactInstitution->getService());
            }

            $entityManager->persist($contactInstitution);
            $entityManager->flush();

            $this->addFlash('success', 'Contact enregistré');
            return $this->redirectToRoute(
                'core_person_legal_show',
                [
                    'id' => $personLegal->getId()
                ]
            );
        }
        return $this->render(
            'Core/ContactInstitution/newContactInstitution.html.twig',
            [
                'form' => $form->createView(),
                'personLegal' => $personLegal,
                'origins' => $originRepository->findOriginByUsage(Origin::PERSON)->getQuery()->getResult()
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="contact_institution_edit", methods={"GET","POST"})
     * @Translation("Editer un nouveau contact d'établissement")
     * @param ContactInstitution $contactInstitution
     * @param Request $request
     * @param OriginRepository $originRepository
     * @return Response
     * @throws \Exception
     */
    public function editContactForInstitution(
        ContactInstitution $contactInstitution,
        Request $request,
        OriginRepository $originRepository
    ): Response {
        /** @var PersonLegal $personLegal */
        $personLegal = $contactInstitution->getPersonLegal();

        /** @var PersonNatural $personNatural */
        $personNatural = $contactInstitution->getPersonNatural();

        $form = $this->createForm(ContactInstitutionType::class, $contactInstitution);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $validator = new ContactInstitutionValidator($request, $originRepository);
            $errors = $validator->validate();

            if (!empty($errors)) {
                $message = '';
                foreach ($errors as $error) {
                    $message .= $error . ' ';
                }
                $this->addFlash('error', $message);
                return $this->redirectToRoute('contact_institution_new', ['id' => $personLegal->getId()]);
            }
            $entityManager = $this->getDoctrine()->getManager();

            $personNatural->setFirstName($request->request->get('firstname'));
            $personNatural->setLastName($request->request->get('lastname'));
            $origin = $originRepository->findOneBy(['id' => $request->request->get('origin')]);
            $personNatural->setOrigin($origin);
            $personNatural->setMainJobName($contactInstitution->getFunction());

            $entityManager->persist($personNatural);

            if (!$contactInstitution->getLabel()) {
                $contactInstitution->setLabel($contactInstitution->getFunction() . ', ' . $contactInstitution->getService());
            }
            $contactInstitution->setPersonNatural($personNatural);

            $entityManager->persist($contactInstitution);
            $entityManager->flush();

            $this->addFlash('success', 'Contact modifié');
            return $this->redirectToRoute(
                'core_person_legal_show',
                [
                    'id' => $personLegal->getId()
                ]
            );
        }
        return $this->render(
            'Core/ContactInstitution/editContactInstitution.html.twig',
            [
                'form' => $form->createView(),
                'personLegal' => $personLegal,
                'origins' => $originRepository->findOriginByUsage(Origin::PERSON)->getQuery()->getResult(),
                'personNatural' => $personNatural
            ]
        );
    }
}
