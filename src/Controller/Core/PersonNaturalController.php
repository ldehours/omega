<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\CFML\MembershipCfml;
use App\Entity\CFML\MembershipCfmlSpecificStatusHistorization;
use App\Entity\Core\Agenda;
use App\Entity\Core\Discussion;
use App\Entity\Core\JobName;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Core\Telephone;
use App\Entity\Core\TimeRange;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\Offer;
use App\Entity\Offer\OfferWishMatching;
use App\Entity\Offer\SubstitutionType;
use App\Entity\Offer\Wish;
use App\Entity\Website\Coord;
use App\Entity\Website\User;
use App\Form\Core\AddOfferToPersonNaturalType;
use App\Form\Core\MembershipCfmlSpecificStatusHistorizationType;
use App\Form\Core\PersonNaturalType;
use App\Repository\CFML\MembershipCfmlSpecificStatusHistorizationRepository;
use App\Repository\CFML\MembershipRepository;
use App\Repository\Core\AddressRepository;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\DiscussionRepository;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\OriginRepository;
use App\Repository\Core\PersonLegalRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Repository\Core\ServiceRepository;
use App\Repository\Core\StaffRepository;
use App\Repository\Mail\PreWrittenMailRepository;
use App\Repository\Offer\CriterionRepository;
use App\Repository\Offer\OfferRepository;
use App\Repository\Offer\WishRepository;
use App\Repository\Website\AdhRepository;
use App\Repository\Website\CoordRepository;
use App\Repository\Website\UserRepository;
use App\Service\AssociatedPersonAddress\AddressPersonManager;
use App\Service\DatabaseManager;
use App\Service\LegacyAdapter;
use App\Service\Matching\DistanceHandler;
use App\Service\Uniqueness\PersonNaturalUniqueness;
use App\Service\WebsiteDatabase;
use App\Traits\MatchingTrait;
use App\Utils\DateTime;
use App\Utils\MailMessage;
use App\Utils\PHPHelper;
use DateInterval;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Swift_Mailer;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/person/natural")
 */
class PersonNaturalController extends BaseAbstractController
{
    use MatchingTrait;

    /**
     * @Route("/", name="core_person_natural_index", methods={"GET"})
     * @Translation("Visualisation de la liste des personnes physique")
     * @param PersonNaturalRepository $personNaturalRepository
     * @return Response
     */
    public function index(PersonNaturalRepository $personNaturalRepository): Response
    {
        $output = [];
        $badgeColors = [
            'primary' => 'primary',
            'red' => 'danger',
            'blue' => 'info',
            'orange' => 'warning',
            'green' => 'success'
        ];
        $color = $this->get('session')->get('color') == null ? 'primary' : $this->get('session')->get('color');

        $linkClass = $badgeColors[$color];
        foreach ($personNaturalRepository->getPersonNaturalInfos() as $person) {
            $id = $person['id'];
            $gender = PersonNatural::LABEL[$person['gender']];
            $mainJobNameLabel = $person['label'];
            $mailAddress = $person['address'];
            $postalCode = $person['postalCode'];
            $locality = $person['locality'];
            $fullName = $person['lastName'] . ' ' . $person['firstName'];
            $action = '<div class="row">
                                <div class="col-6">
                                    <a class="bubble-info" aria-label="Visualiser"
                                       href="' . $id . '"
                                       data-controller="Core\PersonNaturalController\show">
                                        <i class="fas fa-eye ' . $linkClass . '"></i>
                                    </a>
                                </div>
                                <div class="col-6">
                                    <a class="bubble-info" aria-label="Modifier"
                                     data-controller="Core\PersonNaturalController\edit"
                                       href="' . $id . '/edit">
                                        <i class="fas fa-pen ' . $linkClass . '"></i>
                                    </a>
                                </div>
                            </div>';
            $output[] = [
                'id' => '<a class="' . $linkClass . '" href=' . $id . '>' . $id . '</a>',
                'full_name' => '<a class="' . $linkClass . '" href=' . $id . '>' . $fullName . '</a>',
                'job_name' => '<a class="' . $linkClass . '" href=' . $id . '>' . $mainJobNameLabel . '</a>',
                'mail' => '<a class="' . $linkClass . '" href="' . $id . '/mail/exchange">' . $mailAddress . '</a>',
                'postal_code' => '<a class="' . $linkClass . '" href=' . $id . '>' . $postalCode . '</a>',
                'locality' => '<a class="' . $linkClass . '" href=' . $id . '>' . $locality . '</a>',
                'gender' => '<a class="' . $linkClass . '" href=' . $id . '>' . $gender . '</a>',
                'action' => $action
            ];
        }
        return $this->render(
            'Core/PersonNatural/viewAllPersonNatural.html.twig',
            [
                'count' => $personNaturalRepository->count([]),
                'data' => $output
            ]
        );
    }

    /**
     * @Route("/new", name="core_person_natural_new", methods={"GET","POST"})
     * @Translation("Ajout d'une nouvelle personne physique")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param CoordRepository $coordRepository
     * @param PersonNaturalRepository $personNaturalRepository
     * @param PersonLegalRepository $personLegalRepository
     * @param OriginRepository $originRepository
     * @param AddressRepository $addressRepository
     * @param JobNameRepository $jobNameRepository
     * @param DepartmentRepository $departmentRepository
     * @param AddressPersonManager $addressPersonManager
     * @return Response
     * @throws Exception
     */
    public function new(
        Request $request,
        UserRepository $userRepository,
        CoordRepository $coordRepository,
        PersonNaturalRepository $personNaturalRepository,
        PersonLegalRepository $personLegalRepository,
        OriginRepository $originRepository,
        AddressRepository $addressRepository,
        JobNameRepository $jobNameRepository,
        DepartmentRepository $departmentRepository,
        AddressPersonManager $addressPersonManager
    ): Response
    {
        $personNatural = new PersonNatural();
        $form = $this->createForm(PersonNaturalType::class, $personNatural);
        $form->handleRequest($request);

        // pour les RA ET RT Responsables cela va leurs permettre de voir oui ou non le champs input dropdown statut spécifique du client
        $isCFML = $this->isGranted('ROLE_CFML');

        $substituteJobNames = $jobNameRepository->findBy(['classification' => JobName::SUBSTITUTE]);
        $substituteJobNamesIds = [];
        foreach ($substituteJobNames as $jobName) {
            $substituteJobNamesIds[] = $jobName->getId();
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $distWebsiteDatabase = new WebsiteDatabase(false);
            $defaultEmail = $personNatural->getDefaultMail();

            if ($defaultEmail !== null && ($user = $userRepository->getByLogin(
                    $defaultEmail->getAddress(),
                    $distWebsiteDatabase
                )) !== null) {
                $coord = $coordRepository->getById($user->getId(), $distWebsiteDatabase);

                $errorMessage = 'Cette adresse mail par défaut ' . $defaultEmail->getAddress() . ' est déjà affectée à un compte site';
                if ($coord != null) {
                    $errorMessage .= $coord->getNom() . ' ' . $coord->getPrenom() . ' - ' . $coord->getCp() . ' ' . $coord->getVille() . ' Clé ' . $user->getCle_nantu();
                }

                $form->addError(new FormError($errorMessage));
                return $this->render(
                    'Core/PersonNatural/editPersonNatural.html.twig',
                    [
                        'personNatural' => $personNatural,
                        'form' => $form->createView(),
                        'substituteJobnames' => $substituteJobNamesIds
                    ]
                );
            }

            /** @var Staff $connectedUser */
            $connectedUser = $this->getUser();
            $addressPersonManager->setPersonNatural($personNatural);
            $addressPersonManager->handleRequest($request, $connectedUser);

            //NOM en majuscule sans accent et prénom 1ere lettre majuscule uniquement avec les accents
            $personNatural->setFirstName($personNatural->getFirstName());
            $personNatural->setLastName($personNatural->getLastName());
            $personNatural->setCreationDate(new DateTime());
            $agenda = new Agenda();
            $personNatural->setAgenda($agenda);
            $personNatural->setCreator($connectedUser);

            $entityManager->persist($personNatural);
            $entityManager->flush();
            $this->addFlash('success', 'Personne créée');
            return $this->redirectToRoute('core_person_natural_show', ['id' => $personNatural->getId()]);
        }

        return $this->render(
            'Core/PersonNatural/newPersonNatural.html.twig',
            [
                'personNatural' => $personNatural,
                'form' => $form->createView(),
                'isCFML' => $isCFML,
                'substituteJobnames' => $substituteJobNamesIds
            ]
        );
    }

    /**
     * @Route("/{id}", name="core_person_natural_show", methods={"GET"})
     * @Translation("Visualisation d'une personne physique")
     * @param PersonNatural $personNatural
     * @param UserRepository $userRepository
     * @param AdhRepository $adhRepository
     * @param DiscussionRepository $discussionRepository
     * @param MembershipRepository $membershipRepository
     * @param StaffRepository $staffRepository
     * @param MembershipCfmlSpecificStatusHistorizationRepository $membershipCfmlSpecificStatusHistorizationRepository
     * @return Response
     * @throws NonUniqueResultException
     */
    public function show(
        PersonNatural $personNatural,
        UserRepository $userRepository,
        AdhRepository $adhRepository,
        DiscussionRepository $discussionRepository,
        MembershipRepository $membershipRepository,
        StaffRepository $staffRepository,
        MembershipCfmlSpecificStatusHistorizationRepository $membershipCfmlSpecificStatusHistorizationRepository
    ): Response
    {
        $userWebsite = null;
        $adhWebsite = null;
        $distWebsiteDatabase = new WebsiteDatabase(true);

        $membershipCfmlSpecificStatusHistorizationType = new MembershipCfmlSpecificStatusHistorization();
        $formMembershipCfmlSpecificStatusHistorizationType = $this->createForm(MembershipCfmlSpecificStatusHistorizationType::class, $membershipCfmlSpecificStatusHistorizationType);

        if (($personNatural->getDefaultMail() != null) && ($personNatural->getWebsiteId() != null)) {
            $userWebsite = $userRepository->getByLogin(
                $personNatural->getDefaultMail()->getAddress(),
                $distWebsiteDatabase
            );
            $adhWebsite = $adhRepository->getById($personNatural->getWebsiteId(), $distWebsiteDatabase);
        }
        $lastDiscussionPhoneCallSend = $discussionRepository->findOneLastDiscussionByTypeAndPerson(
            $personNatural,
            Discussion::PHONECALL_SENT
        );

        $lastExchange = $discussionRepository->findOneLastDiscussionByTypeAndPerson($personNatural);

        $discussionsService = [];
        $iterator = 0;
        foreach ($personNatural->getDiscussions() as $discussion) {
            if ($discussion->getService() === $this->getUser()->getService()) {
                if (++$iterator > 10) {
                    break;
                }
                $discussionsService[] = $discussion;
            }
        }

        $this->manageBirthdate($personNatural);
        $isCFML = $this->isGranted("ROLE_CFML");

        $isRAorRT = $staffRepository->staffHasRole(['ROLE_CFML_TECHNIQUE', 'ROLE_CFML_ADMINISTRATIF'], $this->getUser());

        $membershipCfml = $membershipRepository->findOneBy(['member' => $personNatural->getId()]);
        $historizeStatus = null;

        if ($membershipCfml) {
            $historizeStatus = $membershipCfmlSpecificStatusHistorizationRepository->findBy(['membership' => $membershipCfml]);
        }

        return $this->render(
            'Core/PersonNatural/viewOnePersonNatural.html.twig',
            [
                'personNatural' => $personNatural,
                'userWebsite' => $userWebsite,
                'adhWebsite' => $adhWebsite,
                'lastDiscussionPhoneCallSend' => $lastDiscussionPhoneCallSend,
                'lastExchange' => $lastExchange,
                'discussionsService' => $discussionsService,
                'oneDiscussion' => new Discussion(),
                'specialty' => $personNatural->getSpecialty(),
                'oneOffer' => new Offer(),
                'isCFML' => $isCFML,
                'membershipCfml' => $membershipCfml,
                'formMembershipCfmlSpecificStatusHistorizationType' => $formMembershipCfmlSpecificStatusHistorizationType->createView(),
                'historizeStatus' => $historizeStatus,
                'isRAorRT' => $isRAorRT
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="core_person_natural_edit", methods={"GET", "POST"})
     * @Translation("Modification d'une personne physique")
     * @param Request $request
     * @param PersonNatural $personNatural
     * @param UserRepository $userRepository
     * @param CoordRepository $coordRepository
     * @param AddressPersonManager $addressPersonManager
     * @param JobNameRepository $jobNameRepository
     * @return Response
     * @throws Exception
     */
    public function edit(
        Request $request,
        PersonNatural $personNatural,
        UserRepository $userRepository,
        CoordRepository $coordRepository,
        AddressPersonManager $addressPersonManager,
        JobNameRepository $jobNameRepository
    ): Response
    {

        $oldDefaultMail = ($personNatural->getDefaultMail() === null) ? null : $personNatural->getDefaultMail()->getAddress();
        $form = $this->createForm(PersonNaturalType::class, $personNatural);
        $form->handleRequest($request);

        $isCFML = $this->isGranted('ROLE_CFML');

        if ($form->isSubmitted() && $form->isValid()) {
            $newDefaultEmail = $personNatural->getDefaultMail();
            if ($newDefaultEmail !== null) {
                $distWebsiteDatabase = new WebsiteDatabase(true);
                $user = $userRepository->getByLogin($newDefaultEmail->getAddress(), $distWebsiteDatabase);
                $render = $this->render(
                    'Core/PersonNatural/editPersonNatural.html.twig',
                    [
                        'personNatural' => $personNatural,
                        'form' => $form->createView(),
                        'isCFML' => $isCFML
                    ]
                );

                // Erreur
                if ($user !== null && $user->getId() !== $personNatural->getWebsiteId()) {
                    $coord = $coordRepository->getById($user->getId(), $distWebsiteDatabase);
                    $errorMessage = sprintf(
                        'Cette adresse mail par défaut "%s" est déjà affectée à un compte site',
                        $personNatural->getDefaultMail()->getAddress()
                    ); //TODO : afficher à quelle personne il est déjà affecté
                    // Si on a les coordonnées on les rajoute au message
                    if ($coord !== null) {
                        $errorMessage .= ' : ' . $coord->getNom() . ' ' . $coord->getPrenom() . ' - ' . $coord->getCp() . ' ' . $coord->getVille() . ' Clé ' . $user->getCle_nantu();
                    }
                    $this->addFlash('error', $errorMessage);
                    $form->addError(new FormError($errorMessage));
                    return $render;
                } elseif ($personNatural->getWebsiteId() !== null && $personNatural->getDefaultMail()->getAddress() !== $oldDefaultMail) {
                    $user = $userRepository->getByLogin($oldDefaultMail, $distWebsiteDatabase);
                    $localWebsiteDatabase = new WebsiteDatabase(false, true);
                    $manager = new DatabaseManager([$localWebsiteDatabase]);

                    if (strtolower($_ENV['APP_ENV']) === 'prod') {
                        $manager->addDatabase($distWebsiteDatabase);
                    }
                    $results = $userRepository->updateLogin($manager, $personNatural);

                    if (!DatabaseManager::areExpectedNumberLines($results, 1, '!=')) {
                        $this->addFlash(
                            'error',
                            'Une erreur est survenue au moment du changement de login : ' . $oldDefaultMail . ' par ' . $personNatural->getDefaultMail()->getAddress()
                        );
                        $form->addError(
                            new FormError(
                                'La mise à jour du login " ' . $oldDefaultMail . ' par ' . $personNatural->getDefaultMail()->getAddress() . ' " a échouée !'
                            )
                        );
                        return $render;
                    }

                    $this->addFlash(
                        'warning',
                        'Attention, le compte site du docteur ' . $personNatural->getLastName() . ' ' . $personNatural->getFirstName() . " sera désormais l'identifiant : " . $personNatural->getDefaultMail()->getAddress() . ' et le mot de passe : ' . ($user !== null ? $user->getPass() : '')
                    );
                }
            }
            $entityManager = $this->getDoctrine()->getManager();

            /** @var Staff $connectedUser */
            $connectedUser = $this->getUser();
            $addressPersonManager->setPersonNatural($personNatural);
            $addressPersonManager->handleRequest($request, $connectedUser);

            $personNatural->setFirstName($personNatural->getFirstName());
            $personNatural->setLastName(PHPHelper::strToUpperNoAccent($personNatural->getLastName()));

            $this->manageBirthdate($personNatural);

            $newJobName = $personNatural->getMainJobName();
            if (!empty($personNatural->getRealWishes())) {
                if ($newJobName->getClassification() !== JobName::SUBSTITUTE && strpos(
                        $newJobName->getLabel(),
                        'effectuant des remplacements'
                    ) === false) {
                    foreach ($personNatural->getRealWishes() as $wish) {
                        $wish->setIsActive(false);
                    }
                } else {
                    foreach ($personNatural->getRealWishes() as $wish) {
                        $wish->setIsActive(true);
                    }
                }
            }

            $this->manageBirthdate($personNatural);
            $entityManager->persist($personNatural);
            $entityManager->flush();
            $this->addFlash('success', 'Modifications enregistrées');
            return $this->redirectToRoute(
                'core_person_natural_show',
                [
                    'id' => $personNatural->getId(),
                ]
            );
        }
        $this->manageBirthdate($personNatural);


        $substituteJobNames = $jobNameRepository->findBy(['classification' => JobName::SUBSTITUTE]);
        $substituteJobNamesIds = [];
        foreach ($substituteJobNames as $jobName) {
            $substituteJobNamesIds[] = $jobName->getId();
        }
        return $this->render(
            'Core/PersonNatural/editPersonNatural.html.twig',
            [
                'personNatural' => $personNatural,
                'form' => $form->createView(),
                'isCFML' => $isCFML,
                'substituteJobnames' => $substituteJobNamesIds
            ]
        );
    }

    /**
     * @Route("/{id}/viewtimeline", name="core_person_natural_viewtimeline", methods={"GET", "POST"})
     * @Translation("Visualisation de l'historique des discussions")
     * @param Request $request
     * @param PersonNatural $personNatural
     * @param ServiceRepository $serviceRepository
     * @return Response
     * @throws Exception
     */
    public function viewTimeline(
        Request $request,
        PersonNatural $personNatural,
        ServiceRepository $serviceRepository
    ): Response
    {
        $sortedDiscussions = [
            Discussion::PERIOD_ONE_MONTH => [],
            Discussion::PERIOD_THREE_MONTH => [],
            Discussion::PERIOD_SIX_MONTH => [],
            Discussion::PERIOD_ONE_YEAR => [],
            Discussion::PERIOD_THREE_YEAR => [],
            Discussion::PERIOD_OLDEST => []
        ];

        foreach ($personNatural->getDiscussions() as $discussion) {
            $creationDate = $discussion->getCreationDate();
            if ($creationDate > (new DateTime('-1 month'))) {
                $sortedDiscussions[Discussion::PERIOD_ONE_MONTH][] = $discussion;
            } elseif ($creationDate > (new DateTime('-3 month'))) {
                $sortedDiscussions[Discussion::PERIOD_THREE_MONTH][] = $discussion;
            } elseif ($creationDate > (new DateTime('-6 month'))) {
                $sortedDiscussions[Discussion::PERIOD_SIX_MONTH][] = $discussion;
            } elseif ($creationDate > (new DateTime('-1 year'))) {
                $sortedDiscussions[Discussion::PERIOD_ONE_YEAR][] = $discussion;
            } elseif ($creationDate > (new DateTime('-3 year'))) {
                $sortedDiscussions[Discussion::PERIOD_THREE_YEAR][] = $discussion;
            } else {
                $sortedDiscussions[Discussion::PERIOD_OLDEST][] = $discussion;
            }
        }

        return $this->render(
            'Core/PersonNatural/viewTimelinePersonNatural.html.twig',
            [
                'PersonNatural' => $personNatural,
                'sortedDiscussions' => $sortedDiscussions,
                'oneDiscussion' => new Discussion(),
                'services' => $serviceRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="core_person_natural_delete", methods={"DELETE"})
     * @Translation("Suppression d'une personne physique")
     * @param Request $request
     * @param PersonNatural $personNatural
     * @return Response
     */
    public function delete(
        Request $request,
        PersonNatural $personNatural
    ): Response
    {
        if ($this->isCsrfTokenValid('delete' . $personNatural->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($personNatural);
            $entityManager->flush();
        }

        return $this->redirectToRoute('core_search_person');
    }

    /**
     * @Route("/{id}/create/website/account", name="core_person_natural_create_website_account", methods={"POST"})
     * @Translation("Création d'un compte site")
     * @param PersonNatural $personNatural
     * @param UserRepository $userRepository
     * @param CoordRepository $coordRepository
     * @return JsonResponse
     * @throws Exception
     */
    public function createWebsiteAccount(
        PersonNatural $personNatural,
        UserRepository $userRepository,
        CoordRepository $coordRepository
    ): Response
    {
        // Guardian
        if ($personNatural->getDefaultMail() === null) {
            return $this->errorJsonResponse('Cette personne ne possède pas de mail par défaut');
        }
        if ($personNatural->getWebsiteId() !== null) {
            return $this->errorJsonResponse('Un compte site est déjà existant pour cette adresse mail (' . $personNatural->getDefaultMail()->getAddress() . ')');
        }
        if (count($personNatural->getAddresses()) === 0) {
            return $this->errorJsonResponse('Les coordonnées postales de cette personne sont inexistantes. Merci de les renseigner avant la création d’un compte site');
        }

        $localWebsiteDatabase = new WebsiteDatabase(false, true);
        $manager = new DatabaseManager([$localWebsiteDatabase]);

        if (strtolower($_ENV['APP_ENV']) === 'prod') {
            $distWebsiteDatabase = new WebsiteDatabase(false);
            $manager->addDatabase($distWebsiteDatabase);
        }

        $user = new User();
        $user->setLogin($personNatural->getDefaultMail()->getAddress());

        $fullPassword = md5(mt_rand());
        $user->setPass(substr($fullPassword, 0, 7));
        try {
            $user->setStatut(LegacyAdapter::getStatusInitial($personNatural->getPersonStatus()));
        } catch (Exception $e) {
            return $this->errorJsonResponse('Le statut du client est incorrect');
        }

        $resultInsertUser = $userRepository->insert($user, $manager);
        if (!$resultInsertUser->getSuccess()) {
            return $this->errorJsonResponse($resultInsertUser->getMessage());
        }
        $websiteId = $resultInsertUser->getData()[0]->getId();

        $coord = new Coord();
        $coord->setId($websiteId);
        $coord->setNom($personNatural->getLastName());
        $coord->setPrenom($personNatural->getFirstName());

        $address = $personNatural->getAddresses()->first();
        $coord->setAdresse($address->getAddressLine1());
        if ($address->getAddressLine2() !== null) {
            $coord->setAd2($address->getAddressLine2());
        }
        try {
            $coord->setCp($address->getPostalCode());
        } catch (Exception $e) {
            $userRepository->deleteById($websiteId, $manager);
            return $this->errorJsonResponse('Le code postal doit être sur maximum 5 caractères');
        }
        $coord->setVille($address->getLocality());

        if ($personNatural->getTelephones()->count() > 0) {
            $telephonesWork = $personNatural->getTelephonesByType(Telephone::WORK);
            $telephonesHome = $personNatural->getTelephonesByType(Telephone::HOME);
            $telephonesCell = $personNatural->getTelephonesByType(Telephone::CELL);
            $telephonesFax = $personNatural->getTelephonesByType(Telephone::FAX);

            if ($telephonesWork->count() > 0) {
                $coord->setTel($telephonesWork->first()->getNumber());
            }
            if ($telephonesHome->count() > 0) {
                $coord->setTel($telephonesHome->first()->getNumber());
            }
            if ($telephonesCell->count() > 0) {
                $coord->setTel($telephonesCell->first()->getNumber());
            }
            if ($telephonesFax->count() > 0) {
                $coord->setTel($telephonesFax->first()->getNumber());
            }
        }

        $resultInsertCoord = $coordRepository->insert($coord, $manager);
        if ($resultInsertCoord->getSuccess()) {
            $personNatural->setWebsiteId($websiteId);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($personNatural);
            $entityManager->flush();
        } else {
            $userRepository->deleteById($websiteId, $manager);
            return $this->errorJsonResponse($resultInsertCoord->getMessage());
        }

        return $this->successJsonResponse([
            'message' => 'Compte site créé correctement avec le mail ' . $user->getLogin() . ' et le mot de passe : ' . ($user !== null ? $user->getPass() : '')
        ]);
    }

    /**
     * @Route("/{id}/create/website/eula", name="core_person_natural_validate_eula", methods={"POST"})
     * @Translation("Validation des CUAM")
     * @param PersonNatural $personNatural
     * @param UserRepository $userRepository
     * @param Swift_Mailer $mailer
     * @return JsonResponse
     */
    public function validateEULA(
        PersonNatural $personNatural,
        UserRepository $userRepository,
        Swift_Mailer $mailer
    ): JsonResponse
    {
        if ($personNatural->getWebsiteId() === null) {
            return $this->errorJsonResponse('Cette personne ne possède pas de compte site.');
        }
        $localWebsiteDatabase = new WebsiteDatabase(false, true);
        $manager = new DatabaseManager([$localWebsiteDatabase]);

        if (strtolower($_ENV['APP_ENV']) === 'prod') {
            $distWebsiteDatabase = new WebsiteDatabase(false);
            $manager->addDatabase($distWebsiteDatabase);
        }

        $results = $userRepository->updateCusEnvoi($manager, $personNatural);

        if (!DatabaseManager::areExpectedNumberLines($results, 1, '!=')) {
            return $this->errorJsonResponse('Une erreur est survenue lors de l\'envoi des CUAM');
        }

        $password = $userRepository->getPasswordByPersonNatural($personNatural, $localWebsiteDatabase);

        $advisor = $personNatural->getAdvisor();

        if ($this->getUser()->getId() === $advisor->getId()) {
            $content = MailMessage::getCUAM(
                $personNatural->getDefaultMail()->getAddress(),
                $password,
                // TODO Gérer les téléphones des CR
                true,
                '0450710505'
            );
        } else {
            $content = MailMessage::getCUAM($personNatural->getDefaultMail()->getAddress(), $password);
        }

        $message = (new \Swift_Message('Media-Santé - Activation de vos CUAM'))
            ->setFrom('noreply@media-sante.com')
            ->setTo($personNatural->getDefaultMail()->getAddress())
            ->setBody(
                $this->renderView(
                    'Mail/mailSimple.html.twig',
                    [
                        'greeting' => MailMessage::getGreeting($personNatural),
                        'content' => $content,
                        'thanks' => MailMessage::getThanks(),
                        'signature' => $advisor->getFirstName() . ' ' . $advisor->getLastName()
                    ]
                ),
                'text/html'
            );

        if (!$mailer->send($message)) {
            // Je n'ai jamais réussi a trouver un cas où le mail ne part pas =/
            return $this->errorJsonResponse('Les CUAM ont été envoyées mais pas le mail de notification');
        }
        return $this->successJsonResponse(['message' => 'CUAM bien envoyées']);
    }

    /**
     * @Route("/{id}/add/offer", name="core_person_natural_add_offer", methods={"GET", "POST"})
     * @Translation("Ajout d'une offre")
     * @param Request $request
     * @param PersonNatural $personNatural
     * @param OfferRepository $offerRepository
     * @return Response
     */
    public function addOffer(
        Request $request,
        PersonNatural $personNatural,
        OfferRepository $offerRepository
    ): Response
    {
        $form = $this->createForm(AddOfferToPersonNaturalType::class, $personNatural);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerId = $request->request->get('offer_to_associate');
            if (!preg_match("/^\d+$/", $offerId)) {
                $this->addFlash('error', "Le numéro de l'offre transmis n'est pas valide. Veuillez réessayer");
                return $this->redirectToRoute(
                    'core_person_natural_add_offer',
                    [
                        'id' => $personNatural->getId()
                    ]
                );
            }
            $offer = $offerRepository->findOneBy(
                [
                    'id' => $offerId
                ]
            );
            if ($offer === null || $offer->getState() === Offer::STATE_FILLED) {
                $this->addFlash(
                    'error',
                    "Une erreur est survenue. Il semblerait que l'offre n'ai pas été trouvé dans la base de données. Contactez un administrateur."
                );
                return $this->redirectToRoute(
                    'core_person_natural_add_offer',
                    [
                        'id' => $personNatural->getId()
                    ]
                );
            }
            if ($personNatural->getProvidedOffers()->contains($offer)) {
                $this->addFlash(
                    'error',
                    $personNatural->getFirstName() . ' ' . $personNatural->getLastName() . ' a déjà pourvu cette offre !'
                );
                return $this->redirectToRoute(
                    'core_person_natural_add_offer',
                    [
                        'id' => $personNatural->getId()
                    ]
                );
            }
            $personNatural->addProvidedOffer($offer);
            $offer->setState(Offer::STATE_FILLED);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($personNatural);
            $entityManager->persist($offer);
            $entityManager->flush();

            $this->addFlash('success', "L'offre a bien été associée");
            return $this->redirectToRoute('core_person_natural_show', ['id' => $personNatural->getId()]);
        }

        return $this->render(
            'Core/PersonNatural/addOfferToPersonNatural.html.twig',
            [
                'PersonNatural' => $personNatural,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/ajax/jobname/{id}", name="core_person_natural_jobname_classification", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param JobName $jobName
     * @return JsonResponse
     */
    public function ajaxJobNameClassification(Request $request, JobName $jobName): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($jobName->getClassification() === JobName::SUBSTITUTE);
    }

    /**
     * @Route("/{id}/notification/new", name="notification_new", methods={"GET"})
     * @Translation("Visualisation du formulaire de notification")
     * @param Person $person
     * @param StaffRepository $staffRepository
     * @return Response
     */
    public function viewNotificationForm(Person $person, StaffRepository $staffRepository): Response
    {
        return $this->render(
            'Notification/_form.html.twig',
            [
                'types' => Staff::NOTIFICATION_TYPE,
                'staffs' => $staffRepository->findBy(['isArchived' => false]),
                'personId' => $person->getId()
            ]
        );
    }

    /**
     * @Route("/ajax/uniqueness", name="core_person_natural_check_uniqueness", methods={"POST", "GET"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonNaturalUniqueness $personNaturalUniqueness
     * @return JsonResponse
     * @throws Exception
     */
    public function ajaxCheckUniqueness(
        Request $request,
        PersonNaturalUniqueness $personNaturalUniqueness
    ): JsonResponse
    {
        $result = $personNaturalUniqueness->checkPersonUniquenessFromRequest($request);
        return new JsonResponse($result);
    }

    /**
     * @Route("/{id}/mail/exchange", name="person_natural_mail_exchange", methods={"POST","GET"})
     * @Translation("Echanges de mails des personNatural")
     * @param DiscussionRepository $discussionRepository
     * @param PersonNatural $personNatural
     * @param PreWrittenMailRepository $preWrittenMailRepository
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws \Tipimail\Exceptions\TipimailException
     */
    public function mailExchanges(
        DiscussionRepository $discussionRepository,
        PersonNatural $personNatural,
        PreWrittenMailRepository $preWrittenMailRepository,
        PaginatorInterface $paginator
    ): Response
    {
        $staff = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $mails = MailMessage::mailExchanges(
            $discussionRepository,
            $entityManager,
            $personNatural,
            $staff,
            $preWrittenMailRepository,
            $paginator
        );

        return $this->render('Mail/mailBox.html.twig', $mails);
    }

    /**
     * @Route("/{id}/manage/contact", name="manage_person_to_contact", methods={"PUT"})
     * @Translation("ajouter/supprimer une personne à contacter")
     * @param Person $person
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function managePersonToContact(
        Person $person,
        Request $request
    ): JsonResponse
    {
        $value = $request->get('personToContact');
        if (empty($value)) {
            throw new Exception("La valeur doit ête remplie");
        }
        $isRemoval = $request->get('isRemoval');
        if ($isRemoval === "true") {
            $person->removePersonToContact($value);
        } else {
            if (in_array($value, $person->getPersonToContact())) {
                return $this->errorJsonResponse('Cette valeur existe déjà !');
            }
            $person->addPersonToContact($value);
        }
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($person);
        $manager->flush();
        return $this->successJsonResponse(['message' => 'Action effectuée avec succès']);
    }

    /**
     * @Route("/{id}/get/hired/persons", name="get_already_hired_persons", methods={"GET"})
     * @Translation("Récupération des rempla ayant déjà pourvus des offres de cette Person")
     * @param Person $person
     * @return JsonResponse
     * @throws Exception
     */
    public function getAlreadyHiredPersons(Person $person): JsonResponse
    {
        $result = [];
        foreach ($person->getRealOffers() as $offer) {
            foreach ($offer->getContractors() as $contractor) {
                if (!in_array($contractor, $result)) {
                    $result[] = [
                        'offer' => $offer->serialize(),
                        'person' => $contractor->serialize()
                    ];
                }
            }
        }
        return $this->successJsonResponse(['data' => $result]);
    }

    /**
     * @Route("/{id}/matching", name="matching_substitute", methods={"GET","POST"})
     * @Route("/{id}/matching/{oneWishId}", name="matching_substitute_one_wish", methods={"GET","POST"})
     * @Translation("Matching Remplaçant")
     * @param PersonNatural $person
     * @param OfferRepository $offerRepository
     * @param WishRepository $wishRepository
     * @param CriterionRepository $criterionRepository
     * @param int|null $oneWishId
     * @return Response
     * @throws Exception
     */
    public function matchingSubstitute(
        PersonNatural $person,
        OfferRepository $offerRepository,
        WishRepository $wishRepository,
        CriterionRepository $criterionRepository,
        ?int $oneWishId
    ): Response
    {
        // récupération des critères spécifiques de nombre d'acte
        $criterionInterval1 = $criterionRepository->findOneByLabel('Nombre d\'actes intervalle min');
        $criterionInterval2 = $criterionRepository->findOneByLabel('Nombre d\'actes intervalle max');
        $criterionPonderation = $criterionRepository->findOneByLabel('Nombre d\'actes pondération');
        $criterionValeur = $criterionRepository->findOneByLabel('Nombre d\'actes valeur');
        $manager = $this->getDoctrine()->getManager();
        $result = [];
        $isOneWish = true;
        if (isset($oneWishId)) { //si le matching est uniquement sur un seul souhait
            /*** @var Wish $oneWish */
            $oneWish = $wishRepository->find($oneWishId);

            if ($oneWish->getPerson() === $person) { // si la personne du souhait est bien la même que celle du matching (id de l'url)
                $specialty = $person->getSpecialty();
                if (isset($specialty)) { // si la personne a bien une spécialité
                    if ($oneWish->getTimeRange() === null) {
                        $this->addFlash(
                            'error',
                            'Un problème est survenu. Les dates ne sont pas renseignées correctement pour ce souhait.'
                        );
                        $this->redirectToRoute('offer_wish_index', ['personId' => $person->getId()]);
                    } elseif ($oneWish->getIsModel()) { // si c'est un modèle de souhait
                        $this->addFlash('error', 'Matching impossible sur un modèle');
                        $this->redirectToRoute('offer_wish_index', ['personId' => $person->getId()]);
                    }
                    //on récupère les offres possiblement matchables
                    $offersInProgress = $offerRepository->offersForMatching($oneWish);

                    // matching sur le souhait
                    $matchingResult = $this->matchingSubstituteOnWish(
                        $oneWish,
                        $manager,
                        $criterionRepository,
                        $criterionInterval1,
                        $criterionInterval2,
                        $criterionPonderation,
                        $criterionValeur,
                        $offersInProgress,
                        $result
                    );

                    $result = $matchingResult['result'];
                } else {
                    $this->addFlash(
                        'error',
                        "Le médecin n'a pas de spécialité renseignée dans son profil. Veuillez compléter ce champ puis réessayez."
                    );
                    $this->redirectToRoute(
                        'offer_wish_index',
                        [
                            'personId' => $person->getId()
                        ]
                    );
                }
            } else {
                $this->addFlash('error', "La personne associée au souhait n'est pas la même que celle de la fiche.");
                $this->redirectToRoute(
                    'offer_wish_index',
                    [
                        'personId' => $person->getId()
                    ]
                );
            }
        } else { //matching sur tous les souhaits du rempla
            //on récupère tous les souhaits du rempla
            $personWishes = $wishRepository->wishesForSubstituteMatching($person);
            $specialty = $person->getSpecialty();
            $result = ['Liberal' => [], 'Salariat' => [], 'Cession' => []];
            $i = 0;
            $isOneWish = false;
            if (isset($specialty)) { // si le rempla a une spécialité
                /** @var Wish $wish */
                foreach ($personWishes as $wish) {
                    if ($wish->getTimeRange() === null) { // si aucunes dates ne sont renseignées dans l'agenda
                        continue;
                    }
                    // on récupère les offres possiblement matchables
                    $offersInProgress = $offerRepository->offersForMatching($wish);
                    // on matche
                    $matchingResult = $this->matchingSubstituteOnWish(
                        $wish,
                        $manager,
                        $criterionRepository,
                        $criterionInterval1,
                        $criterionInterval2,
                        $criterionPonderation,
                        $criterionValeur,
                        $offersInProgress,
                        $result,
                        $i // indice du souhait dans le tableau
                    );
                    $result = $matchingResult['result'];
                    $i = $matchingResult['i']; // on récupère l'indice du souhait
                }
            } else {
                $this->addFlash(
                    'error',
                    "Le médecin n'a pas de spécialité renseignée dans son profil. Veuillez compléter ce champ puis réessayez."
                );
                $this->redirectToRoute(
                    'core_person_natural_show',
                    [
                        'id' => $person->getId()
                    ]
                );
            }
        }
        $numberWishes = [];
        foreach ($result as $type => $rows) {
            if (is_array($rows)) {
                $numberWishes[$type] = count($rows);
            }
        }
        return $this->render(
            'Core/PersonNatural/matchingSubstitute.html.twig',
            [
                'person' => $person,
                'resultMatching' => $result,
                'isOneWish' => $isOneWish,
                'numberWishes' => $numberWishes,
                'oneSubstitutionType' => new SubstitutionType() // pour les constantes dans twig
            ]
        );
    }

    /**
     * @param Wish $wish
     * @param ObjectManager $manager
     * @param CriterionRepository $criterionRepository
     * @param Criterion $criterionInterval1
     * @param Criterion $criterionInterval2
     * @param Criterion $criterionPonderation
     * @param Criterion $criterionValeur
     * @param array $offersInProgress
     * @param array $result
     * @param int|null $i
     * @return array
     * @throws Exception
     * @NoTranslation()
     */
    public function matchingSubstituteOnWish(
        Wish $wish,
        ObjectManager $manager,
        CriterionRepository $criterionRepository,
        Criterion $criterionInterval1,
        Criterion $criterionInterval2,
        Criterion $criterionPonderation,
        Criterion $criterionValeur,
        array $offersInProgress,
        array $result,
        int $i = null
    ): array
    {
        // on récupère les infos utiles pour le souhaits : sa catégorie (Salariat, Libéral, Cession), et les critères du souhait
        $getArrayWishCriteria = $this->getArrayWishCriteria($wish);
        $wishSubstitutionTypeCategory = null;
        $arrayWishCriteria = [];
        if (!empty($getArrayWishCriteria)) {
            $wishSubstitutionTypeCategory = $getArrayWishCriteria['wishSubstitutionTypeCategory']; // sa catégorie (Salariat, Libéral, Cession)
            $arrayWishCriteria = $getArrayWishCriteria['arrayWishCriteria']; // les critères du souhaits
        }

        if (isset($i)) { // si on est dans une boucle sur plusieurs souhaits
            switch ($wishSubstitutionTypeCategory) { // en fonction de la catégorie de souhait
                case SubstitutionType::TYPE_LIBERAL_REPLACEMENT:
                    $result['Liberal'][$i] = [
                        'wish' => $wish
                    ];
                    break;
                case SubstitutionType::TYPE_SALARIED_POSITION:
                    $result['Salariat'][$i] = [
                        'wish' => $wish
                    ];
                    break;
                case SubstitutionType::TYPE_SELL_ASSOCIATION:
                    $result['Cession'][$i] = [
                        'wish' => $wish
                    ];
                    break;
                default:
                    $result['Default'][$i] = [
                        'wish' => $wish
                    ];
                    break;
            }
        } else { // si on match sur un seul souhait
            $result = [
                'wish' => $wish,
                'offers' => []
            ];
        }
        $wishDepartments = [];
        $wishSubstitutionType = $wish->getSubstitutionType(); // type du souhait (remplacement lib. ponctuel, CDD, etc.)
        foreach ($wish->getDepartments() as $wishDepartment) {
            $wishDepartments[] = $wishDepartment->getCode();
        }

        if ($wish->getStartingDate() !== null) { // si le souhait a une date de début
            $wishOffersMatched = $wish->getOfferWishMatchings(); // les offerWishMatching qui sont déja en base (i.e les offres qui ont déja matchés ensemble avec le souhait)
            /** @var Offer $offer */
            foreach ($offersInProgress as $offer) {
                $wishPerson = $wish->getPerson();
                if ($wishPerson->getOffersSent()->contains($offer)) { // si l'offre n'est pas déjà envoyée au rempla
                    continue;
                }

                $offerIsActiveForThisWish = true;
                $lastOfferWishMatching = null;
                /*
                 * on parcours les offres ayant déja matchées avec le souhait
                 * pour vérifier si elles ne sont pas retirées du matching pour ce souhait (i.e isActive = false)
                 */
                foreach ($wishOffersMatched as $offerWishMatching) {
                    if ($offerWishMatching->getOffer() === $offer) {
                        $lastOfferWishMatching = $offerWishMatching;
                        if ($offerWishMatching->getIsActive() === false) {
                            $offerIsActiveForThisWish = false;
                            break;
                        }
                    }
                }
                if ($offerIsActiveForThisWish === false) { // si l'offre n'est pas active pour ce souhait
                    continue;
                }

                $offerSubstitutionType = $offer->getSubstitutionType();

                /*
                 * Si l'offre a un type (remp lib ponoctuel, cdd, etc.)
                 * ET
                 * que le type de l'offre est le même que celui du souhait
                 */
                if ($offerSubstitutionType !== null && $offerSubstitutionType === $wishSubstitutionType) {

                    // si les départements ne correspondent pas
                    if (!empty($wishDepartments) && !in_array($offer->getDepartment()->getCode(), $wishDepartments)) {
                        continue;
                    }

                    // si les dates ne correspondent pas
                    if (false === $this->doesDatesMatch($offer, $wish)) {
                        continue;
                    }

                    $nbCriteresImperatifs = 0;
                    $nbCriteresSecondaires = 0;
                    $nbCriteresAdequationImperatifs = 0;
                    $nbCriteresAdequationSecondaires = 0;
                    $offerSpecialty = $offer->getSpecialty();

                    /* on détermine la catégorie de l'offre (Salariat / Libéral / Cession) */
                    $offerSubstitutionTypeCategory = SubstitutionType::TYPE_LIBERAL_REPLACEMENT;
                    if (in_array(
                        $offerSubstitutionType->getSubstitutionType(),
                        SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SALARIED_POSITION],
                        true
                    )) {
                        $offerSubstitutionTypeCategory = SubstitutionType::TYPE_SALARIED_POSITION;
                    } elseif (in_array(
                        $offerSubstitutionType->getSubstitutionType(),
                        SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SELL_ASSOCIATION],
                        true
                    )) {
                        $offerSubstitutionTypeCategory = SubstitutionType::TYPE_SELL_ASSOCIATION;
                    }

                    // on récupère la distance entre l'offre et le souhait
                    $shortestDistance = DistanceHandler::getShortestDistance($offer, $wish);
                    if ($shortestDistance === null) {
                        continue;
                    }

                    $arrayOfferCriteria = [];
                    $offerSubstitutionCriteria = $offer->getSubstitutionCriteria();
                    foreach ($offerSubstitutionCriteria as $substitutionCriterion) {
                        $currentCriterion = $substitutionCriterion->getCriterion();
                        $arrayOfferCriteria[$currentCriterion->getId()] = [
                            'criterion' => $currentCriterion,
                            'value' => $substitutionCriterion->getValue(),
                            'isRelevant' => $substitutionCriterion->getIsRelevant()
                        ];
                    }

                    // matching sur les critères
                    $resultMatchingCriteria = $this->matchingCriteria(
                        $criterionRepository,
                        $criterionInterval1,
                        $criterionInterval2,
                        $criterionPonderation,
                        $criterionValeur,
                        $this->getOfferDates($offer),
                        $offerSpecialty,
                        $arrayWishCriteria,
                        $arrayOfferCriteria,
                        $nbCriteresImperatifs,
                        $nbCriteresSecondaires,
                        $nbCriteresAdequationImperatifs,
                        $nbCriteresAdequationSecondaires
                    );

                    $nbCriteresImperatifs = $resultMatchingCriteria['nbCriteresImperatifs'];
                    $nbCriteresAdequationImperatifs = $resultMatchingCriteria['nbCriteresAdequationImperatifs'];
                    $nbCriteresSecondaires = $resultMatchingCriteria['nbCriteresSecondaires'];
                    $nbCriteresAdequationSecondaires = $resultMatchingCriteria['nbCriteresAdequationSecondaires'];

                    if ($lastOfferWishMatching === null) { // s'il n'y avait pas encore de lien entre l'offre et le souhait
                        $offerWishMatching = new OfferWishMatching();
                        $offerWishMatching->setOffer($offer);
                        $offerWishMatching->setWish($wish);
                        $offerWishMatching->setIsActive(true);
                        $manager->persist($offerWishMatching);
                    }

                    // on récupère le taux de correspondance avec l'offre
                    $percentage = $this->getPercentage(
                        $nbCriteresImperatifs,
                        $nbCriteresAdequationImperatifs,
                        $nbCriteresSecondaires,
                        $nbCriteresAdequationSecondaires
                    );

                    // on construit le tableau de résultat qui sera retourné
                    $arrayDataOffer = [
                        'offer' => $offer,
                        'percentage' => $percentage * 100,
                        'isStartDateModulable' => $offer->getStartingDateModularity() > 0,
                        'isEndDateModulable' => ($offer->getEndDate() !== null && $offer->getEndDateModularity() > 0),
                        'distance' => $shortestDistance
                    ];
                    if (isset($i)) {
                        switch ($offerSubstitutionTypeCategory) {
                            case SubstitutionType::TYPE_LIBERAL_REPLACEMENT:
                                $result['Liberal'][$i]['offers'][] = $arrayDataOffer;
                                break;
                            case SubstitutionType::TYPE_SALARIED_POSITION:
                                $result['Salariat'][$i]['offers'][] = $arrayDataOffer;
                                break;
                            case SubstitutionType::TYPE_SELL_ASSOCIATION:
                                $result['Cession'][$i]['offers'][] = $arrayDataOffer;
                                break;
                        }
                    } else {
                        $result['offers'][] = $arrayDataOffer;
                    }
                }
                $manager->flush();
            }
            if (isset($i)) {
                $i++;
            }

            return [
                'result' => $result,
                'i' => $i
            ];
        }
        return [];
    }

    /**
     * TODO : Mettre autre part genre dans un service
     * @NoTranslation()
     * @param PersonNatural $personNatural
     * @throws Exception
     * Sert à gérer la date d'anniversaire dans l'agenda pour les médecins ayant renseigné une date de naissance
     */
    public function manageBirthdate(PersonNatural $personNatural): void
    {
        $timeRangeRepository = $this->getDoctrine()->getRepository(TimeRange::class);
        $manager = $this->getDoctrine()->getManager();
        $birthDate = $personNatural->getBirthDate();
        if ($birthDate !== null) {
            $timeRange = $timeRangeRepository->findOneBy(
                [
                    'agenda' => $personNatural->getAgenda(),
                    'nature' => TimeRange::NATURE_BIRTHDAY
                ]
            );
            $now = new DateTime();
            if (!empty($timeRange)) {
                if ($birthDate->format('m-d') !== $timeRange->getEndDate()->format('m-d')) {
                    $newDate = $now->format('Y') . '-' . $birthDate->format('m-d') . ' 00:00:00';
                    $newDate = DateTime::createFromFormat('Y-m-d H:i:s', $newDate);
                    if ($now->format('m') !== $birthDate->format('m')) {
                        if ($newDate->getTimestamp() < $now->getTimestamp()) {
                            $newDate->add(new DateInterval('P1Y'));
                        }
                    }
                    $timeRange->setStartDate($newDate);
                    $timeRange->setEndDate((clone $newDate)->add(new DateInterval('PT23H59M59S')));
                } else {
                    if ($timeRange->getEndDate()->format('Y') === $now->format('Y')) {
                        if ($timeRange->getEndDate()->format('m') !== $now->format('m')) {
                            if ($timeRange->getEndDate()->getTimestamp() < $now->getTimestamp()) {
                                $timeRange->setStartDate((clone $timeRange->getStartDate())->add(new DateInterval('P1Y')));
                                $timeRange->setEndDate((clone $timeRange->getEndDate())->add(new DateInterval('P1Y')));
                            }
                        }
                    } else {
                        $yearDifference = (int)$now->format('Y') - (int)$timeRange->getEndDate()->format('Y');
                        if ($yearDifference > 0) {
                            $timeRange->setStartDate((clone $timeRange->getStartDate())->add(new DateInterval('P' . (string)$yearDifference . 'Y')));
                            $timeRange->setEndDate((clone $timeRange->getEndDate())->add(new DateInterval('P' . (string)$yearDifference . 'Y')));
                            if ($timeRange->getEndDate()->format('m') !== $now->format('m')) {
                                if ($timeRange->getEndDate()->getTimestamp() < $now->getTimestamp()) {
                                    $timeRange->setStartDate((clone $timeRange->getStartDate())->add(new DateInterval('P1Y')));
                                    $timeRange->setEndDate((clone $timeRange->getEndDate())->add(new DateInterval('P1Y')));
                                }
                            }
                        } elseif ($yearDifference < -1) {
                            $timeRange->setStartDate(
                                (clone $timeRange->getStartDate())->sub(new DateInterval('P' . (string)($yearDifference + 1) . 'Y'))
                            );
                            $timeRange->setEndDate(
                                (clone $timeRange->getEndDate())->sub(new DateInterval('P' . (string)($yearDifference + 1) . 'Y'))
                            );
                        }
                    }
                }
            } else {
                $timeRange = new TimeRange();
                $timeRange->setCreator($this->getUser());
                $timeRange->setNature(TimeRange::NATURE_BIRTHDAY);
                $timeRange->setAgenda($personNatural->getAgenda());

                $newDate = $now->format('Y') . '-' . $birthDate->format('m-d') . ' 00:00:00';
                $newDate = DateTime::createFromFormat('Y-m-d H:i:s', $newDate);
                if ($now->format('m') !== $birthDate->format('m')) {
                    if ($newDate->getTimestamp() < $now->getTimestamp()) {
                        $newDate->add(new DateInterval('P1Y'));
                    }
                }
                $timeRange->setStartDate($newDate);
                $timeRange->setEndDate((clone $newDate)->add(new DateInterval('PT23H59M59S')));
            }
            $manager->persist($timeRange);
            $manager->flush();
        }
    }
}
