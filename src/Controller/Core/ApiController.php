<?php

namespace App\Controller\Core;

use App\Service\APIManager;
use App\Annotation\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/core/api/{nameApi}", name="core_api", methods={"GET"})
     * @Translation("Api call")
     * @param string $nameApi
     * @param APIManager $APIManager
     * @return JsonResponse
     */
    public function callApi(string $nameApi, APIManager $APIManager): JsonResponse
    {
        return new JsonResponse([
            "success" => $APIManager::get($nameApi)
        ]);
    }
}
