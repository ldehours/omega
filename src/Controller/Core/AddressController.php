<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\JobName;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Repository\Core\AddressRepository;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\PersonNaturalRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends BaseAbstractController
{
    /**
     * @Route("/core/address", name="address", methods={"POST", "GET"})
     * @NoTranslation()
     * @param Request $request
     * @param AddressRepository $addressRepository
     * @param PersonNaturalRepository $personNaturalRepository
     * @param JobNameRepository $jobNameRepository
     * @return JsonResponse
     */
    public function index(
        Request $request,
        AddressRepository $addressRepository,
        PersonNaturalRepository $personNaturalRepository,
        JobNameRepository $jobNameRepository
    ): JsonResponse
    {
        $associatedPersonLegal = [];
        $listPersonNotAssociatedButWithSameAddress = [];

        if ($request->request->get('postalCode') === null && $request->request->get('locality') === null) {
            return $this->errorJsonResponse('Le code postal ET la ville doivent être renseignés.');
        }

        $addressesFound = [];
        $addressesLine1 = $request->request->get('addressLine1');
        $addressesLine2 = $request->request->get('addressLine2');
        $postalCodes = $request->request->get('postalCode');
        $localities = $request->request->get('locality');
        $isCedex = $request->request->get('isCedex');
        $mainJobNameId = $request->request->get('mainJobName');
        $mainJobName = $jobNameRepository->find($mainJobNameId);
        if( !$mainJobName || $mainJobName->getClassification() === JobName::SUBSTITUTE ){
            return new JsonResponse(['success' => true]);
        }
        $currentPerson = null;

        $refererURL = explode("/", $request->headers->get("referer"));
        $currentPersonId = $refererURL[count($refererURL) - 2]; //-1 = "/new"|"/edit", -2 = id
        if ($refererURL[count($refererURL) - 1] === "edit") {
            $currentPerson = $personNaturalRepository->find($currentPersonId);
        }

        foreach ($addressesLine1 as $index => $addressLine1) {
            $cedex = $isCedex[$index];
            $addresses = $addressRepository->findBy([
                'addressLine1' => $addressLine1,
                'addressLine2' => empty($addressesLine2[$index]) ? null : $addressesLine2[$index],
                'postalCode' => $postalCodes[$index],
                'locality' => $localities[$index],
                'isCedex' => $cedex === 'false' ? false : true
            ]);
            if(!empty($addresses)){
                foreach ($addresses as $address){
                    $addressesFound[] = $address;
                }
            }
        }

        if ($addressesFound !== []) {
            foreach ($addressesFound as $address) {

                if ($address != null) {
                    $persons = $address->getPerson();
                    $cedex = ($address->getIsCedex()) ? 'cedex' : '';
                    foreach ($persons as $person) {
                        if($currentPerson && $person->getId() === $currentPerson->getId()){
                            continue;
                        }
                        $linked = false;

                        if ($person instanceof PersonLegal) {
                            if ($currentPerson != null) {
                                if ($person->getId() !== $currentPerson->getId()) {
                                    $currentPersonNaturalPersonLegals = [];
                                    foreach ($currentPerson->getPersonLegals() as $personLegal) {
                                        $currentPersonNaturalPersonLegals[] = $personLegal;
                                    }
                                    $linked = in_array($person, $currentPersonNaturalPersonLegals);
                                }
                            }

                            $corporateName = $person->getCorporateName();
                            $associatedPersonLegal[] = [
                                'id' => $person->getId(),
                                'corporateName' => $corporateName,
                                'postalCode' => $address->getPostalCode(),
                                'address' => $address->getAddressLine1() . ' ' . $address->getAddressLine2() . ' ' . $address->getLocality() . ' ' . $cedex . ' ' . $address->getPostalCode(),
                                'idAddress' => $address->getId(),
                                'linked' => $linked
                            ];
                        }

                        if ($person instanceof PersonNatural) {

                            $personLegalInPersonNatural = $person->getPersonLegals();
                            if (count($personLegalInPersonNatural) > 0) {
                                $currentAddress = [];

                                foreach ($personLegalInPersonNatural as $personLegal) {
                                    $personLegalAddresses = [];
                                    $currentAddress[] = $address;
                                    $personLegalAddresses[] = $personLegal->getAddresses();

                                    if (in_array($currentAddress, $personLegalAddresses) || $personLegal->getAddresses()[0] == null) {
                                        $associatedPersonLegal[] = [
                                            'id' => $personLegal->getId(),
                                            'corporateName' => $personLegal->getCorporateName(),
                                            'address' => $address->getAddressLine1() . ' ' . $address->getAddressLine2() . ' ' . $address->getLocality() . ' ' . $cedex . ' ' . $address->getPostalCode(),
                                            'idAddress' => $address->getId(),
                                            'linked' => $linked
                                        ];
                                    }
                                }
                            } else {
                                $listPersonNotAssociatedButWithSameAddress[] = [
                                    'id' => $person->getId(),
                                    'lastName' => $person->getLastName(),
                                    'firstName' => $person->getFirstName(),
                                    'address' => $address->getAddressLine1() . ' ' . $address->getAddressLine2() . ' ' . $address->getLocality() . ' ' . $cedex . ' ' . $address->getPostalCode(),
                                    'idAddress' => $address->getId()
                                ];
                            }
                        }
                    }
                }
            }
        }
        return $this->successJsonResponse([
            'hasOneMatch' => (count($associatedPersonLegal) > 0 || count($listPersonNotAssociatedButWithSameAddress) > 0),
            'personLegalFound' => $associatedPersonLegal,
            'personNotAssociatedButWithSameAddress' => $listPersonNotAssociatedButWithSameAddress
        ]);
    }

}
