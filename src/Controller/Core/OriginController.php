<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Entity\Core\Origin;
use App\Form\Core\OriginType;
use App\Repository\Core\OriginRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/origin")
 */
class OriginController extends AbstractController
{
    /**
     * @Route("/", name="core_origin_index", methods={"GET"})
     * @Translation("Visualisation des origines")
     * @param OriginRepository $originRepository
     * @return Response
     */
    public function index(OriginRepository $originRepository): Response
    {
        return $this->render('Core/Origin/viewAllOrigin.html.twig', [
            'Origins' => $originRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="core_origin_new", methods={"POST"})
     * @Translation("Ajout d'une nouvelle origine")
     */
    public function new(Request $request): Response
    {
        $origin = new Origin();
        $form = $this->createForm(OriginType::class, $origin,
            array('action' => $this->generateUrl('core_origin_new')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($origin);
            $entityManager->flush();

            return $this->redirectToRoute('core_origin_index');
        }

        return $this->render('Core/Origin/_form.html.twig', [
            'Origin' => $origin,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="core_origin_edit", methods={"POST"})
     * @Translation("Modification d'une origine")
     * @param Request $request
     * @param Origin $origin
     * @return Response
     */
    public function edit(Request $request, Origin $origin): Response
    {
        $form = $this->createForm(OriginType::class, $origin,
            array('action' => $this->generateUrl('core_origin_edit', array('id' => $origin->getId()))));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('core_origin_index');
        }

        return $this->render('Core/Origin/_form.html.twig', [
            'Origin' => $origin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="core_origin_delete", methods={"DELETE"})
     * @Translation("Suppression d'une origine")
     * @param Request $request
     * @param Origin $origin
     * @return Response
     */
    public function delete(Request $request, Origin $origin): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($origin);
        $entityManager->flush();

        return $this->redirectToRoute('core_origin_index');
    }
}
