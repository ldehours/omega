<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Entity\Core\JobName;
use App\Entity\Core\PersonAccess;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\PersonAccessRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/person/access")
 */
class PersonAccessController extends AbstractController
{

    /**
     * @Route("/", name="core_access_by_job_name_index",  methods={"GET"})
     * @Translation("Visualiser tableau des accès")
     * @param JobNameRepository $jobNameRepository
     * @param PersonAccessRepository $personAccessRepository
     * @return Response
     */
    public function index(
        JobNameRepository $jobNameRepository,
        PersonAccessRepository $personAccessRepository
    ): Response {
        $jobNames = $jobNameRepository->findBy([],['label'=>'ASC']);
        $accesses = $personAccessRepository->findAll();

        return $this->render('Core/PersonAccess/viewAllPersonAccess.html.twig', [
            'jobNames' => $jobNames,
            'accesses' => $accesses,
            'controller_name' => self::class,
            'toggleOneUrl' => '/core/person/access/toggleOne',
            'toggleRowUrl' => '/core/person/access/toggleAllByRow',
            'toggleColumnUrl' => '/core/person/access/toggleAllByColumn',
            'toggleAllUrl' => '/core/person/access/toggleAll'
        ]);
    }

    /**
     * @Route("/toggleOne", name="core_access_by_job_name_toggle_one",  methods={"POST"})
     * @Translation("Modifier un accès")
     * @param Request $request
     * @param PersonAccessRepository $personAccessRepository
     * @param JobNameRepository $jobNameRepository
     * @return JsonResponse
     */
    public function toggleOne(
        Request $request,
        PersonAccessRepository $personAccessRepository,
        JobNameRepository $jobNameRepository
    ): JsonResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $accessId = $request->get('access_id');
        $jobNameId = $request->get('jobname_id');
        $access = $personAccessRepository->findOneBy(['id' => $accessId]);
        $jobName = $jobNameRepository->findOneBy(['id' => $jobNameId]);

        $jobNameAccesses = $access->getJobName();
        $accessesJobNames = $jobName->getPersonAccesses();
        $isInAccessJobName = false;
        $isInJobNameAccess = false;
        foreach ($jobNameAccesses as $jobNameAccess) {
            if ($jobNameAccess->getId() == $jobNameId) {
                $isInAccessJobName = true;
                break;
            }
        }

        foreach ($accessesJobNames as $accessesJobName) {
            if ($accessesJobName->getId() == $accessId) {
                $isInJobNameAccess = true;
                break;
            }
        }

        if ($isInAccessJobName) {
            $access->removeJobName($jobName);
        } else {
            $access->addJobName($jobName);
        }

        if ($isInJobNameAccess) {
            $jobName->removePersonAccess($access);
        } else {
            $jobName->addPersonAccess($access);
        }
        $entityManager->persist($access);
        $entityManager->persist($jobName);
        $entityManager->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/toggleAllByColumn", name="core_access_by_job_name_toggle_one_column", methods={"POST"})
     * @Translation("Modifier une colonne de accès")
     * @param Request $request
     * @param PersonAccessRepository $personAccessRepository
     * @param JobNameRepository $jobNameRepository
     * @return JsonResponse
     */
    public function toggleAllByColumn(
        Request $request,
        PersonAccessRepository $personAccessRepository,
        JobNameRepository $jobNameRepository
    ): JsonResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $accessId = $request->get('access_id');
        $jobNameId = $request->get('jobnames_id');
        $access = $personAccessRepository->findOneBy(['id' => $accessId]);
        $jobNamesInAccess = $access->getJobName();
        if ($jobNameId == null) {
            foreach ($jobNamesInAccess as $jobNameInAccess) {
                $access->removeJobName($jobNameInAccess);
                $jobNameInAccess->removePersonAccess($access);
                $entityManager->persist($jobNameInAccess);
            }
        } else {
            $jobNames = $jobNameRepository->findBy(['id' => $jobNameId]);
            $jobNamesInAccesses = [];
            foreach ($jobNamesInAccesses as $jobNameInAccess) {
                $jobNamesInAccesses[] = $jobNameInAccess;
            }
            foreach ($jobNames as $jobName) {
                $accessesInJobName = [];
                $accessesInJobName[] = $jobName->getPersonAccesses();
                $this->saveChangeOnToggle($access, $jobName, $accessesInJobName, $jobNamesInAccess->toArray());
                $entityManager->persist($jobName);
            }
        }
        $entityManager->persist($access);
        $entityManager->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/toggleAllByRow", name="core_access_by_job_name_toggle_one_row",  methods={"POST"})
     * @Translation("Modifier une ligne de accès")
     * @param Request $request
     * @param PersonAccessRepository $personAccessRepository
     * @param JobNameRepository $jobNameRepository
     * @return JsonResponse
     */
    public function toggleAllByRow(
        Request $request,
        PersonAccessRepository $personAccessRepository,
        JobNameRepository $jobNameRepository
    ): JsonResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $jobNameId = $request->get('jobname_id');
        $accessId = $request->get('accesses_id');

        $jobName = $jobNameRepository->findOneBy(['id' => $jobNameId]);
        $accessesInJobName = $jobName->getPersonAccesses();
        if ($accessId == null) {
            foreach ($accessesInJobName as $accessInJobName) {
                $jobName->removePersonAccess($accessInJobName);
                $accessInJobName->removeJobName($jobName);
                $entityManager->persist($accessInJobName);
            }
        } else {
            $accesses = $personAccessRepository->findBy(['id' => $accessId]);
            $accessesInJobNames = [];
            foreach ($accessesInJobName as $accessInJobName) {
                $accessesInJobNames[] = $accessInJobName;
            }
            foreach ($accesses as $access) {
                $jobNameInAccesses = [];
                $jobNameInAccesses[] = $access->getJobName();
                $this->saveChangeOnToggle($access, $jobName, $jobNameInAccesses, $accessesInJobName->toArray());
                $entityManager->persist($access);
            }
        }
        $entityManager->persist($jobName);
        $entityManager->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/toggleAll", name="core_access_by_job_name_toggle_all",  methods={"POST"})
     * @Translation("Modifier tous les accès")
     * @param Request $request
     * @param PersonAccessRepository $personAccessRepository
     * @param JobNameRepository $jobNameRepository
     * @return JsonResponse
     */
    public function toggleAll(
        Request $request,
        PersonAccessRepository $personAccessRepository,
        JobNameRepository $jobNameRepository
    ): JsonResponse {
        $entityManager = $this->getDoctrine()->getManager();
        $allAccesses = $personAccessRepository->findAll();
        $allJobNames = $jobNameRepository->findAll();

        if (json_decode($request->get('checked_all'))) {
            foreach ($allJobNames as $jobName) {
                foreach ($allAccesses as $access) {
                    $access->addJobName($jobName);
                    $entityManager->persist($access);
                }
                $entityManager->persist($jobName);
            }
        } else {
            foreach ($allJobNames as $jobName) {
                foreach ($allAccesses as $access) {
                    $access->removeJobName($jobName);
                    $entityManager->persist($access);
                }
                $entityManager->persist($jobName);
            }
        }

        $entityManager->flush();
        return new JsonResponse();
    }

    /**
     * @NoTranslation()
     * @param PersonAccess $columnElement
     * @param JobName $rowElement
     * @param array $columnsElementsInRowElement
     * @param array $rowsElementsInColumnElement
     */
    public function saveChangeOnToggle(
        $columnElement,
        $rowElement,
        array $columnsElementsInRowElement,
        array $rowsElementsInColumnElement
    ) {
        if ($columnsElementsInRowElement == null || !(in_array($columnElement, $columnsElementsInRowElement))) {
            $rowElement->addPersonAccess($columnElement);
        }

        if ($rowsElementsInColumnElement == null || !(in_array($rowElement, $rowsElementsInColumnElement))) {
            $columnElement->addJobName($rowElement);
        }
    }
}
