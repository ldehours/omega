<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Repository\Core\FeatureRepository;
use App\Repository\Core\RoleRepository;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/permission")
 */
class PermissionController extends AbstractController
{
    /**
     * @Route("/", name="core_permission_index", methods={"GET","POST"})
     * @Translation("Visualisation du tableau des permissions")
     * @param RoleRepository $roleRepository
     * @param FeatureRepository $featureRepository
     * @return Response
     */
    public function index(
        RoleRepository $roleRepository,
        FeatureRepository $featureRepository
    ): Response {
        return $this->render('Core/Permission/managementPermissions.html.twig', [
            'features' => $featureRepository->findAllSorted(),
            'roles' => $roleRepository->findAll(),
        ]);
    }


    /**
     * @Route("/toggle", name="core_permission_toggle", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @param FeatureRepository $featureRepository
     * @return JsonResponse
     */
    public function toggle(
        Request $request,
        RoleRepository $roleRepository,
        FeatureRepository $featureRepository
    ): JsonResponse {
        $roleId = $request->get('role_id');
        $featureId = $request->get('feature_id');
        $role = $roleRepository->find($roleId);
        $feature = $featureRepository->find($featureId);
        $featuresRoles = $role->getFeatures();
        $isInRoleFeature = false;
        foreach ($featuresRoles as $featureRole) {
            if ($featureRole->getId() == $featureId) {
                $isInRoleFeature = true;
                break;
            }
        }
        if ($isInRoleFeature) {
            $role->removeFeature($feature);
        } else {
            $role->addFeature($feature);
        }
        $this->getDoctrine()->getManager()->persist($role);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/toggleAllByColumn", name="core_permission_toggle_all_by_column", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @param FeatureRepository $featureRepository
     * @return JsonResponse
     */
    public function toggleByColumn(
        Request $request,
        RoleRepository $roleRepository,
        FeatureRepository $featureRepository
    ): JsonResponse {
        $roleId = $request->get('role_id');
        $featuresId = $request->get('features_id');
        $role = $roleRepository->findOneBy(['id' => $roleId]);
        $featuresInRole = $role->getFeatures();
        if ($featuresId == null) {
            foreach ($featuresInRole as $featureInRole) {
                $featureInRole->removeRole($role);
                $this->getDoctrine()->getManager()->persist($featureInRole);
            }
        } else {
            $features = $featureRepository->findBy(['id' => $featuresId]);
            $featuresInRoles = [];
            foreach ($featuresInRole as $featureInRole) {
                array_push($featuresInRoles, $featureInRole);
            }
            foreach ($features as $feature) {
                $rolesInFeature = [];
                array_push($rolesInFeature, $feature->getRoles());
                if (!(in_array($role, $rolesInFeature)) or $rolesInFeature == null) {
                    $feature->addRole($role);
                }

                if (!(in_array($feature, $featuresInRoles)) or $featuresInRoles == null) {
                    $role->addFeature($feature);
                }
                $this->getDoctrine()->getManager()->persist($feature);
            }
        }
        $this->getDoctrine()->getManager()->persist($role);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/toggleAllByRow", name="core_permission_toggle_all_by_row", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @param FeatureRepository $featureRepository
     * @return JsonResponse
     */
    public function toggleByRow(
        Request $request,
        RoleRepository $roleRepository,
        FeatureRepository $featureRepository
    ): JsonResponse {
        $featureId = $request->get('feature_id');
        $rolesId = $request->get('roles_id');

        $feature = $featureRepository->findOneBy(['id' => $featureId]);
        $rolesInFeature = $feature->getRoles();
        if ($rolesId == null) {
            foreach ($rolesInFeature as $roleInFeature) {
                if ($roleInFeature->getName() != "ROLE_ROOT") {
                    $roleInFeature->removeFeature($feature);
                }
                $this->getDoctrine()->getManager()->persist($roleInFeature);
            }
        } else {
            $roles = $roleRepository->findBy(['id' => $rolesId]);
            $rolesInFeatures = [];
            foreach ($rolesInFeature as $roleInFeature) {
                array_push($rolesInFeatures, $roleInFeature);
            }
            foreach ($roles as $role) {
                $featureInRoles = [];
                array_push($featureInRoles, $role->getFeatures());
                if (!(in_array($feature, $featureInRoles)) or $featureInRoles == null) {
                    $role->addFeature($feature);
                }

                if (!(in_array($role, $rolesInFeatures)) or $rolesInFeatures == null) {
                    $feature->addRole($role);
                }
                $this->getDoctrine()->getManager()->persist($role);
            }
        }
        $this->getDoctrine()->getManager()->persist($feature);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/toggleAll", name="core_permission_toggle_all", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @param FeatureRepository $featureRepository
     * @return JsonResponse
     */
    public function toggleAll(
        Request $request,
        RoleRepository $roleRepository,
        FeatureRepository $featureRepository
    ): JsonResponse {
        $allRoles = $roleRepository->findAll();
        $allFeatures = $featureRepository->findAll();

        if (json_decode($request->get('checked_all'))) {
            foreach ($allFeatures as $feature) {
                if (strtolower(substr($feature->getName(), 0, 9)) != 'ajax_call') {
                    foreach ($allRoles as $role) {
                        $role->addFeature($feature);
                        $this->getDoctrine()->getManager()->persist($role);
                    }
                    $this->getDoctrine()->getManager()->persist($feature);
                }
            }
        } else {
            foreach ($allFeatures as $feature) {
                if (strtolower(substr($feature->getName(), 0, 9)) != 'ajax_call') {
                    foreach ($allRoles as $role) {
                        if ($role->getName() != "ROLE_ROOT") {
                            $role->removeFeature($feature);
                        }
                        $this->getDoctrine()->getManager()->persist($role);
                    }
                }
                $this->getDoctrine()->getManager()->persist($feature);
            }
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse();
    }

    /**
     * @Route("/update/json", name="core_permission_update_json", methods={"POST"})
     * @NoTranslation()
     * @param RoleRepository $roleRepository
     * @param ContainerInterface $container
     * @return JsonResponse
     */
    public function updateJSON(RoleRepository $roleRepository, ContainerInterface $container): JsonResponse
    {
        $roles = $roleRepository->findAll();
        $listControllerNameByRoleName = [];
        foreach ($roles as $role) {
            $roleName = $role->getName();
            $rolesFeature = $role->getFeatures();
            foreach ($rolesFeature as $feature) {
                $listControllerNameByRoleName[$roleName][] = $feature->getControllerName();
            }
        }
        $permissionFile = $_ENV['PERMISSION_FILE_PATH'];
        if (file_exists($permissionFile)) {
            unlink($permissionFile);
        }
        $fileSystem = new Filesystem();
        $fileSystem->dumpFile($permissionFile, json_encode($listControllerNameByRoleName));
        return new JsonResponse(['success' => file_exists($permissionFile)]);
    }

}
