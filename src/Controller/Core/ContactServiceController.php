<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\ContactService;
use App\Repository\Core\ContactServiceRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/contact/service")
 */
class ContactServiceController extends BaseAbstractController
{
    /**
     * @Route("/new", name="core_contact_service_new", methods={"POST","GET"})
     * @Translation("Créer un nouveau service pour les contacts d'établissements")
     * @param Request $request
     * @param ContactServiceRepository $contactServiceRepository
     * @return Response
     */
    public function new(
        Request $request,
        ContactServiceRepository $contactServiceRepository
    ): Response {
        if($request->isMethod('post')){
            $label = $request->request->get('label');
            if(!isset($label) || $label === '' || strlen($label) > 255){
                return $this->errorJsonResponse('Le libellé ne doit pas être vide et ne doit pas dépasser 255 charactères');
            }
            $existingService = $contactServiceRepository->findOneBy([
                'label' => $label
            ]);
            if($existingService !== null){
                return $this->errorJsonResponse('Un service avec le label '.$label. ' existe déjà.');
            }
            $contactService = new ContactService();
            $contactService->setLabel($label);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactService);
            $entityManager->flush();
            return $this->successJsonResponse([
                'serviceId' => $contactService->getId(),
                'serviceLabel' => $contactService->getLabel()
            ]);
        }
        return $this->render('Core/ContactService/newContactService.html.twig');
    }
}
