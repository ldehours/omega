<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Agenda;
use App\Entity\Core\Discussion;
use App\Entity\Core\JobName;
use App\Entity\Core\PersonLegal;
use App\Entity\Offer\Offer;
use App\Form\Core\PersonLegalType;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\DiscussionRepository;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\PersonLegalRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Repository\Mail\PreWrittenMailRepository;
use App\Utils\MailMessage;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/person/legal")
 */
class PersonLegalController extends BaseAbstractController
{
    /**
     * @Route("/", name="core_person_legal_index", methods={"GET","POST"})
     * @Translation("Visualisation de la liste des personnes morales")
     * @param PersonLegalRepository $personLegalRepository
     * @return Response
     */
    public function index(PersonLegalRepository $personLegalRepository): Response
    {
        $output = [];
        $badgeColors = [
            'primary' => 'primary',
            'red' => 'danger',
            'blue' => 'info',
            'orange' => 'warning',
            'green' => 'success'
        ];
        $color = ($this->get('session')->get('color') == null ? 'primary' : $this->get('session')->get('color'));

        $linkClass = $badgeColors[$color];
        $personLegalRepository->getPersonLegalInfos();
        foreach ($personLegalRepository->getPersonLegalInfos() as $person) {
            $id = $person['id'];
            $classification = JobName::LABEL[$person['classification']];
            $mainJobNameLabel = $person['label'];
            $mailAddress = $person['address'];
            $address = $person['addressLine1'] . ' ' . $person['addressLine2'] . ' ' . $person['postalCode'] . ' ' . $person['locality'];
            $corporateName = $person['corporateName'];

            $status = ($person['personStatus'] !== null) ? PersonLegal::LABEL[$person['personStatus']] : '';
            $action = '<div class="row">
                            <div class="col-6">
                                    <a class="bubble-info" aria-label="Visualiser"
                                       href="' . $id . '"
                                       data-controller="Core\PersonLegalController\show">
                                        <i class="fas fa-eye ' . $linkClass . '"></i>
                                    </a>
                                </div>
                            <div class="col-6">
                                    <a class="bubble-info" aria-label="Modifier"
                                     data-controller="Core\PersonNaturalController\edit"
                                       href="' . $id . '/edit">
                                        <i class="fas fa-pen ' . $linkClass . '"></i>
                                    </a>
                                </div>
                            </div>';
            $output[] = [
                'id' => $id,
                'corporateName' => $corporateName,
                'job_name_label' => $mainJobNameLabel,
                'mail' => $mailAddress,
                'address' => $address,
                'classification' => $classification,
                'status' => $status,
                'action' => $action
            ];
        }
        $numberOfPersonLegal = $personLegalRepository->count([]);
        return $this->render('Core/PersonLegal/viewAllPersonLegal.html.twig',
            [
                'count' => $numberOfPersonLegal,
                'data' => $output
            ]);
    }

    /**
     * @Route("/new", name="core_person_legal_new", methods={"GET","POST"})
     * @Translation("Ajout d'une nouvelle personne morale")
     * @param Request $request
     * @param JobNameRepository $jobNameRepository
     * @param PersonNaturalRepository $personNaturalRepository
     * @param DepartmentRepository $departmentRepository
     * @return Response
     */

    public function new(
        Request $request,
        JobNameRepository $jobNameRepository,
        PersonNaturalRepository $personNaturalRepository,
        DepartmentRepository $departmentRepository
    ): Response {
        $personLegal = new PersonLegal();
        $jobNames = $jobNameRepository->findBy(['classification' => JobName::INSTITUTION]);
        $form = $this->createForm(PersonLegalType::class, $personLegal,
            [
                'mainJobName' => $jobNames,
                'jobNames' => $jobNames,
                'personNaturals' => $personLegal->getPersonNaturals()
            ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $personNaturalsIds = $request->get('personNaturals') ?? [];
            foreach ($personNaturalsIds as $personNaturalId) {
                $personNatural = $personNaturalRepository->find($personNaturalId);
                $personNatural->addPersonLegal($personLegal);
                $personLegal->addPersonNatural($personNatural);
            }
            $agenda = new Agenda();
            $personLegal->setAgenda($agenda);
            $personLegal->setCreator($this->getUser());

            $personLegalAddresses = $personLegal->getAddresses();
            if ($personLegalAddresses != null) {
                foreach ($personLegalAddresses as $address) {
                    if ($address->getCountryCode() != 'FR') {
                        $address->setDepartment($departmentRepository->findOneBy(['code' => '99']));
                    } else {
                        $departmentCode = substr($address->getPostalCode(), 0, 2);
                        $department = $departmentRepository->findOneBy(['code' => $departmentCode]) ?? $departmentRepository->findOneBy([
                                'code' => substr($address->getPostalCode(), 0, 3)
                            ]);
                        $address->setDepartment($department);
                    }
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($personLegal);
            $entityManager->flush();
            $this->addFlash('success', 'Etablissement créé');
            return $this->redirectToRoute('core_person_legal_show', ['id' => $personLegal->getId()]);
        }
        return $this->render('Core/PersonLegal/newPersonLegal.html.twig', [
            'PersonLegal' => $personLegal,
            'form' => $form->createView(),
            'path' => '/core/person/legal/addPersonNaturalToPersonLegal',
            'id' => '',
        ]);
    }

    /**
     * @Route("/{id}", name="core_person_legal_show", methods={"GET"})
     * @Translation("Visualisation d'une personne morale")
     * @param Request $request
     * @param PersonLegal $personLegal
     * @param DiscussionRepository $discussionRepository
     * @return Response
     * @throws NonUniqueResultException
     */
    public function show(
        Request $request,
        PersonLegal $personLegal,
        DiscussionRepository $discussionRepository
    ): Response {
        $lastDiscussionPhoneCallSend = $discussionRepository->findOneLastDiscussionByTypeAndPerson($personLegal,
            Discussion::PHONECALL_SENT);
        $lastExchange = $discussionRepository->findOneLastDiscussionByTypeAndPerson($personLegal);

        return $this->render('Core/PersonLegal/viewOnePersonLegal.html.twig', [
            'PersonLegal' => $personLegal,
            'LastDiscussionPhoneCallSend' => $lastDiscussionPhoneCallSend,
            'LastExchange' => $lastExchange,
            'oneOffer' => new Offer()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="core_person_legal_edit", methods={"GET","POST"})
     * @Translation("Modification d'une personne morale")
     * @param Request $request
     * @param PersonLegal $personLegal
     * @param JobNameRepository $jobNameRepository
     * @param PersonNaturalRepository $personNaturalRepository
     * @param DepartmentRepository $departmentRepository
     * @return Response
     */
    public function edit(
        Request $request,
        PersonLegal $personLegal,
        JobNameRepository $jobNameRepository,
        PersonNaturalRepository $personNaturalRepository,
        DepartmentRepository $departmentRepository
    ): Response {
        $jobNames = $jobNameRepository->findBy(['classification' => JobName::INSTITUTION]);
        $mainJobNameChoices = $jobNames;
        unset($mainJobNameChoices[array_search($personLegal->getMainJobName(), $mainJobNameChoices)]);
        $form = $this->createForm(PersonLegalType::class, $personLegal,
            [
                'mainJobName' => $mainJobNameChoices,
                'jobNames' => $jobNames,
                'personNaturals' => $personLegal->getPersonNaturals()
            ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($request->get('personNaturals') != null) {
                foreach ($personLegal->getPersonNaturals() as $personNatural) {
                    if (!in_array($personNatural->getId(), $request->get('personNaturals'))) {
                        $personLegal->removePersonNatural($personNatural);
                        $personNatural->removePersonLegal($personLegal);
                    }
                }
                foreach ($request->get('personNaturals') as $personNaturalId) {
                    $personNatural = $personNaturalRepository->find($personNaturalId);
                    $personNatural->addPersonLegal($personLegal);
                    $personLegal->addPersonNatural($personNatural);

                }
            } else {
                foreach ($personLegal->getPersonNaturals() as $personNatural) {
                    $personLegal->removePersonNatural($personNatural);
                    $personNatural->removePersonLegal($personLegal);
                }
            }

            $personLegalAddresses = $personLegal->getAddresses();
            if ($personLegalAddresses != null) {
                foreach ($personLegalAddresses as $address) {
                    if ($address->getCountryCode() != 'FR') {
                        $address->setDepartment($departmentRepository->findOneBy(['code' => '99']));
                    } else {
                        $departmentCode = substr($address->getPostalCode(), 0, 2);
                        $department = $departmentRepository->findOneBy(['code' => $departmentCode]) ?? $departmentRepository->findOneBy([
                                'code' => substr($address->getPostalCode(), 0, 3)
                            ]);
                        $address->setDepartment($department);
                    }
                }
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success','Modifications enregistrées');
            return $this->redirectToRoute('core_person_legal_show', [
                'id' => $personLegal->getId(),
            ]);
        }

        return $this->render('Core/PersonLegal/editPersonLegal.html.twig', [
            'personLegal' => $personLegal,
            'form' => $form->createView(),
            'path' => '/core/person/legal/addPersonNaturalToPersonLegal',
            'id' => $personLegal->getId(),
        ]);
    }

    /**
     * @Route("/{id}", name="core_person_legal_delete", methods={"DELETE"})
     * @Translation("Suppression d'une personne morale")
     * @param Request $request
     * @param PersonLegal $personLegal
     * @return Response
     */
    public function delete(Request $request, PersonLegal $personLegal): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($personLegal);
        $entityManager->flush();

        return $this->successJsonResponse();
    }

    /**
     * @Route("/addPersonNaturalToPersonLegal", name="add_person_natural_to_person_legal", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonNaturalRepository $personNaturalRepository
     * @param PersonLegalRepository $personLegalRepository
     * @return JsonResponse
     */
    public function showListPersonNatural(
        Request $request,
        PersonNaturalRepository $personNaturalRepository,
        PersonLegalRepository $personLegalRepository
    ): JsonResponse {
        $data = [];
        $personLegalId = $request->get('personLegalId');
        $personNaturals = $personNaturalRepository->findAll();
        $personLegalPersonNaturals = [];

        if ($personLegalId != null) {
            foreach ($personLegalRepository->find($personLegalId)->getPersonNaturals() as $personNatural) {
                $personLegalPersonNaturals[] = $personNatural;
            }
        }

        foreach ($personNaturals as $personNatural) {
            $data[] = [
                'id' => $personNatural->getId(),
                'name' => $personNatural->getName(),
                'checked' => in_array($personNatural, $personLegalPersonNaturals)
            ];
        }

        return new JsonResponse(mb_convert_encoding($data, 'UTF8'));

    }

    /**
     * @Route("/{id}/mail/exchange", name="person_legal_mail_exchange", methods={"POST","GET"})
     * @Translation("Echanges de mails des personLegal")
     * @param PersonLegal $personLegal
     * @param PreWrittenMailRepository $PreWrittenMailRepository
     * @param DiscussionRepository $discussionRepository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function mailExchanges(
        PersonLegal $personLegal,
        PaginatorInterface $paginator,
        PreWrittenMailRepository $PreWrittenMailRepository,
        DiscussionRepository $discussionRepository
    ): Response {
        $staff = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();
        $mails = MailMessage::mailExchanges($discussionRepository, $entityManager, $personLegal, $staff,
            $PreWrittenMailRepository, $paginator);

        return $this->render('Mail/mailBox.html.twig', $mails);
    }
}