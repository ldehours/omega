<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\MedicalSpecialty;
use App\Form\Core\MedicalSpecialtyType;
use App\Repository\Core\MedicalSpecialtyRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/medical/specialty")
 */
class MedicalSpecialtyController extends BaseAbstractController
{
    /**
     * @Route("/", name="core_medical_specialty_index", methods={"GET", "POST"})
     * @Translation("Visualisation des listes de spécialités médicales")
     * @param MedicalSpecialtyRepository $medicalSpecialtyRepository
     * @return Response
     */
    public function index(MedicalSpecialtyRepository $medicalSpecialtyRepository): Response
    {
        return $this->render('Core/MedicalSpecialty/viewAllMedicalSpecialty.html.twig', [
            'medical_specialties' => $medicalSpecialtyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="core_medical_specialty_new", methods={"POST"})
     * @Translation("Ajout d'une nouvelle spécialité médicale")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $medicalSpecialty = new MedicalSpecialty();
        $form = $this->createForm(MedicalSpecialtyType::class, $medicalSpecialty,
            array('action' => $this->generateUrl('core_medical_specialty_new')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($medicalSpecialty);
            $entityManager->flush();

            return $this->redirectToRoute('core_medical_specialty_index');
        }

        return $this->render('Core/MedicalSpecialty/_form.html.twig', [
            'medical_specialty' => $medicalSpecialty,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="core_medical_specialty_edit", methods={"POST"})
     * @Translation("Modification d'une spécialité médicale")
     * @param Request $request
     * @param MedicalSpecialty $medicalSpecialty
     * @return Response
     */
    public function edit(Request $request, MedicalSpecialty $medicalSpecialty): Response
    {
        $form = $this->createForm(MedicalSpecialtyType::class, $medicalSpecialty,
            array(
                'action' => $this->generateUrl('core_medical_specialty_edit', array('id' => $medicalSpecialty->getId()))
            ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('core_medical_specialty_index', [
                'id' => $medicalSpecialty->getId(),
            ]);
        }

        return $this->render('Core/MedicalSpecialty/_form.html.twig', [
            'medical_specialty' => $medicalSpecialty,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="core_medical_specialty_delete", methods={"DELETE"})
     * @Translation("Suppression d'une spécialité médicale")
     * @param Request $request
     * @param MedicalSpecialty $medicalSpecialty
     * @return JsonResponse
     */
    public function delete(Request $request, MedicalSpecialty $medicalSpecialty): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($medicalSpecialty);
        $entityManager->flush();

        return $this->successJsonResponse();
    }
}
