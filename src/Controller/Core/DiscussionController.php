<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Discussion;
use App\Entity\Core\Person;
use App\Form\Core\DiscussionType;
use App\Utils\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/discussion")
 */
class DiscussionController extends BaseAbstractController
{


    /**
     * @Route("/new/{id}", name="core_discussion_new", methods={"GET","POST"})
     * @Translation("Ajout d'une nouvelle discussion")
     * @param Request $request
     * @param Person $person
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, Person $person): Response
    {
        $discussion = new Discussion();
        $discussion->setPerson($person);
        $discussion->setCreator($this->getUser());
        $form = $this->createForm(DiscussionType::class, $discussion,
            array('action' => $this->generateUrl('core_discussion_new', ['id' => $person->getId()])));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $discussion->setPerson($person);
            $discussion->setCreator($this->getUser());
            $discussion->setCreationDate(new DateTime());
            $discussion->setTag(1234657980);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($discussion);
            $entityManager->flush();

            return $this->redirectToRoute('core_person_natural_viewtimeline', [
                'id' => $discussion->getPerson()->getId(),
            ]);
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error','Le formulaire de discussion n\'est pas valide');
            return $this->redirectToRoute('redirect_from_person_instance',['id' => $person->getId()]);
        }

        return $this->render('Core/Discussion/_form.html.twig', [
            'discussion' => $discussion,
            'form' => $form->createView(),
            'defaultStatusDate' => (new DateTime())->format('d/m/Y H:i')
        ]);
    }

    /**
     * @Route("/{id}/edit", name="core_discussion_edit", methods={"GET","POST"})
     */
    /*public function edit(Request $request, Discussion $discussion): Response
    {
        $form = $this->createForm(DiscussionType::class, $discussion,
            array('action' => $this->generateUrl('core_discussion_edit', array('id' => $discussion->getId()))));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('core_discussion_index');
        }

        return $this->render('Core/Discussion/editDiscussion.html.twig', [
            'discussion' => $discussion,
            'form' => $form->createView(),
        ]);
    }*/

    /**
     * @Route("/{id}", name="core_discussion_delete", methods={"DELETE"})
     * @Translation("Suppression d'une discussion")
     * @param Request $request
     * @param Discussion $discussion
     * @return Response
     */
    public function delete(Request $request, Discussion $discussion): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($discussion);
        $entityManager->flush();

        return $this->successJsonResponse();
    }
}
