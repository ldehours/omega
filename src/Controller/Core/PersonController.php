<?php

namespace App\Controller\Core;

use App\Annotation\NoTranslation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Repository\Core\PersonRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/person")
 * Class PersonController
 * @package App\Controller\Core
 */
class PersonController extends BaseAbstractController
{
    /**
     * @Route(path="/{id}", name="redirect_from_person_instance", methods={"GET"})
     * @NoTranslation()
     * @param Person $person
     * @return RedirectResponse
     */
    public function redirectingFromInstance(
        Person $person
    ): RedirectResponse {
        if($person instanceof PersonLegal){
            return $this->redirectToRoute("core_person_legal_show",[
                'id' => $person->getId()
            ]);
        }else{
            return $this->redirectToRoute("core_person_natural_show",[
                'id' => $person->getId()
            ]);
        }
    }

    /**
     * @NoTranslation()
     * @param Person $person
     * @return string
     */
    public function generatingUrlFromInstance(
        Person $person
    ):string {
        if($person instanceof PersonLegal){
            return $this->generateUrl("core_person_legal_show",[
                'id' => $person->getId()
            ]);
        }else{
            return $this->generateUrl("core_person_natural_show",[
                'id' => $person->getId()
            ]);
        }
    }

    /**
     * @Route("/quick-search", name="quick_search_person_id", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonRepository $personRepository
     * @return JsonResponse
     */
    public function quickSearchPersonOnId(
        Request $request,
        PersonRepository $personRepository
    ):JsonResponse{
        $id = $request->request->get('id');
        $person = $personRepository->find($id);
        if(null !== $person){
            return $this->successJsonResponse(['redirectUrl' => $this->generatingUrlFromInstance($person)]);
        }else{
            return $this->errorJsonResponse('Aucun client trouvé avec ce numéro');
        }
    }

    /**
     * @Route("/check/exists", name="person_check_existence", methods={"GET"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonRepository $personRepository
     * @return JsonResponse
     */
    public function checkPersonExistence(Request $request, PersonRepository $personRepository): JsonResponse
    {
        $personId = $request->get('id');
        if (!isset($personId)) {
            return new JsonResponse(['result' => false]);
        }
        $person = $personRepository->findOneBy(['id' => $personId]);
        return new JsonResponse(['result' => ($person !== null && $person instanceof Person)]);
    }
}