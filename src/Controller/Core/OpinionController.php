<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Opinion;
use App\Repository\Core\OpinionRepository;
use App\Repository\Core\PersonRepository;
use App\Service\CustomValidator\Core\OpinionValidator;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/opinion")
 * Class OpinionController
 * @package App\Controller\Core
 */
class OpinionController extends BaseAbstractController
{
    /**
     * @Route("/new", name="create_opinion", methods={"POST"})
     * @Translation("Créer un avis")
     * @param Request $request
     * @param PersonRepository $personRepository
     * @param OpinionRepository $opinionRepository
     * @return JsonResponse
     */
    public function createOpinion(
        Request $request,
        PersonRepository $personRepository,
        OpinionRepository $opinionRepository
    ){
        $opinionValidator = new OpinionValidator($request, $opinionRepository, $personRepository);

        $error = $opinionValidator->validate();
        if($error !== null){
            return $this->errorJsonResponse($error);
        }

        $giver = $personRepository->find($request->get('giverId'));
        $receiver = $personRepository->find($request->get('receiverId'));

        $opinion = new Opinion();
        $opinion->setGiver($giver)->setReceiver($receiver)->setText($request->get('text'))->setCreatedAt(new DateTimeImmutable());
        $em = $this->getDoctrine()->getManager();
        $em->persist($opinion);
        $em->flush();

        return $this->successJsonResponse([
            'data' => [
                'giver' => [
                    'id' => $opinion->getGiver()->getId(),
                    'name' => $opinion->getGiver()->getName(),
                    'url' => $this->generateUrl('core_person_natural_show', ['id' => $opinion->getGiver()->getId()])
                ],
                'receiver' => [
                    'id' => $opinion->getReceiver()->getId(),
                    'name' => $opinion->getReceiver()->getName(),
                    'url' => $this->generateUrl('core_person_natural_show', ['id' => $opinion->getReceiver()->getId()])
                ],
                'text' => $opinion->getText(),
                'createdAt' => $opinion->getCreatedAt()->format('d/m/Y'),
                'deleteUrl' => $this->generateUrl('delete_one_opinion', ['id' => $opinion->getId()])
            ]
        ]);
    }

    /**
     * @Route("/{id}/delete", name="delete_one_opinion", methods={"DELETE"})
     * @Translation("Supprimer un avis")
     * @param Opinion $opinion
     * @return JsonResponse
     */
    public function deleteOpinion(Opinion $opinion){
        $em = $this->getDoctrine()->getManager();
        $em->remove($opinion);
        $em->flush();
        return $this->successJsonResponse();
    }
}