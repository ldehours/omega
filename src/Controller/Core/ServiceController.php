<?php

namespace App\Controller\Core;

use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Service;
use App\Form\Core\ServiceType;
use App\Repository\Core\ServiceRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/core/service")
 */
class ServiceController extends BaseAbstractController
{
    /**
     * @Route("/", name="core_service_index", methods={"GET","POST"})
     * @Translation("Visualisation de la liste des services existants")
     * @param ServiceRepository $serviceRepository
     * @param Request $request
     * @return Response
     */
    public function index(ServiceRepository $serviceRepository, Request $request): Response
    {
        return $this->render('Core/Service/viewAllService.html.twig', [
            'services' => $serviceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="core_service_new", methods={"POST"})
     * @Translation("Ajout d'un nouveau service")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $service = new Service();
        $form = $this->createForm(ServiceType::class, $service,
            array('action' => $this->generateUrl('core_service_new')));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($service);
            $entityManager->flush();

            return $this->redirectToRoute('core_service_index');
        }

        return $this->render('Core/Service/_form.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="core_service_edit", methods={"POST"})
     * @Translation("Modification d'un service")
     * @param Request $request
     * @param Service $service
     * @return Response
     */
    public function edit(Request $request, Service $service): Response
    {
        $form = $this->createForm(ServiceType::class, $service,
            array('action' => $this->generateUrl('core_service_edit', array('id' => $service->getId()))));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('core_service_index');
        }

        return $this->render('Core/Service/_form.html.twig', [
            'service' => $service,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="core_service_delete", methods={"DELETE"})
     * @Translation("Suppression d'un service")
     * @param Request $request
     * @param Service $service
     * @return Response
     */
    public function delete(Request $request, Service $service): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($service);
        $entityManager->flush();

        return $this->successJsonResponse();
    }
}
