<?php


namespace App\Controller;


use App\Annotation\NoTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class BaseAbstractController extends AbstractController
{
    /**
     * @NoTranslation()
     * @param string $message
     * @return JsonResponse
     */
    protected function errorJsonResponse(string $message = 'Une erreur est survenue'){
        return new JsonResponse([
            'success' => false,
            'message' => $message
        ]);
    }

    /**
     * @NoTranslation()
     * @param array $data
     * @return JsonResponse
     */
    protected function successJsonResponse(array $data = []){
        $response = ['success' => true];
        foreach ($data as $key => $value){
            $response[$key] = $value;
        }
        return new JsonResponse($response);
    }
}