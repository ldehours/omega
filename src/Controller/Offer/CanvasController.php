<?php

namespace App\Controller\Offer;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Form\Offer\CanvasType;
use App\Repository\Core\RegionRepository;
use App\Repository\Offer\CanvasRepository;
use App\Repository\Offer\CriterionPossibleValueRepository;
use App\Repository\Offer\CriterionRepository;
use App\Repository\Offer\SubstitutionTypeRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/offer/canvas")
 */
class CanvasController extends BaseAbstractController
{
    /**
     * @Route("/", name="offer_canvas_index", methods={"GET"})
     * @Translation("Visualisation des listes de canevas")
     * @param CanvasRepository $canvasRepository
     * @return Response
     */
    public function index(CanvasRepository $canvasRepository): Response
    {
        return $this->render(
            'Offer/Canvas/viewAllCanvas.html.twig',
            [
                'canvas' => $canvasRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/new", name="offer_canvas_new", methods={"GET","POST"})
     * @param Request $request
     * @param CriterionRepository $criterionRepository
     * @return Response
     * @Translation("Ajout d'un nouveau canevas")
     */
    public function new(
        Request $request,
        CriterionRepository $criterionRepository
    ): Response {
        $canvas = new Canvas();
        $form = $this->createForm(CanvasType::class, $canvas);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $criteria = $request->get('canvas')['criteria'];
            foreach ($criteria as $criterionId) {
                $canvas->addCriterion($criterionRepository->findOneById($criterionId));
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($canvas);
            $entityManager->flush();

            $this->addFlash('success', 'Canevas créé !');
            return $this->redirectToRoute(
                'offer_canvas_edit',
                [
                    'id' => $canvas->getId()
                ]
            );
        }
        return $this->render(
            'Offer/Canvas/newCanvas.html.twig',
            [
                'canvas' => $canvas,
                'oneCriterion' => new Criterion(),
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="offer_canvas_show", methods={"GET"})
     * @param Canvas $canvas
     * @param CriterionRepository $criterionRepository
     * @param ManagerRegistry $registry
     * @param RegionRepository $regionRepository
     * @return Response
     * @Translation("Visualisation d'un canevas")
     */
    public function show(
        Canvas $canvas,
        CriterionRepository $criterionRepository,
        ManagerRegistry $registry,
        RegionRepository $regionRepository
    ): Response {
        $formDisposition = $canvas->getFormDisposition();
        $criterionArray = [];
        $listCriterion = [];
        foreach ($formDisposition as $row) {
            foreach ($row as $value) {
                $listCriterion[] = $criterionRepository->findOneById($value[0]);
            }
        }
        foreach ($listCriterion as $criterion) {
            foreach ($criterion->getCriterionPossibleValues() as $criterionPossibleValue) {
                if ($criterionPossibleValue->getSubstitutionType() === $canvas->getSubstitutionType(
                    ) && $criterionPossibleValue->getSubstitutionNature() === $canvas->getCanvasType()) {
                    $criterionArray[$criterion->getId()] = [
                        'label' => $criterion->getLabel(),
                        'type' => $criterionPossibleValue->getCriterionType(),
                        'dependency' => []
                    ];
                    $dependency = $criterion->getDependency();
                    if (!empty($dependency)) {
                        $criterionArray[$criterion->getId()]['dependency'] = [
                            'id' => array_keys($dependency)[0],
                            'value' => $dependency[array_keys($dependency)[0]]
                        ];
                    }
                    $criterionArray[$criterion->getId()]['value'] = json_decode($criterionPossibleValue->getPossibleValue());
                    if ($criterionPossibleValue->getCriterionType() == Criterion::TYPE_ENTITY) {
                        $possibleValueString = json_decode($criterionPossibleValue->getPossibleValue());
                        $entityFullName = explode('::', $possibleValueString)[0];
                        $fieldName = explode('::', $possibleValueString)[1];

                        $targettedEntity = str_replace(
                            'Entity',
                            'Repository',
                            $entityFullName . 'Repository'
                        );
                        $targettedEntityInstance = new $targettedEntity($registry);

                        $targetLabels = [];

                        foreach ($targettedEntityInstance->findAll() as $element) {
                            $targetLabels[] = $element->{'get' . ucfirst($fieldName)}(); //sorry
                        }
                        $criterionArray[$criterion->getId()]['value'] = $targetLabels;
                    }
                    if ($criterionPossibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                        $jsonData = [];
                        foreach (json_decode($criterionPossibleValue->getPossibleValue()) as $source) {
                            $sourceKey = array_reverse(explode('/', explode('.', $source)[1]))[0];
                            foreach (json_decode(file_get_contents($source), true) as $a) {
                                $jsonData[$sourceKey][$a['code']] = $a['nom'];
                            }
                        }
                        $substitutionCriterionTab = $jsonData;
                        $criterionArray[$criterion->getId()]['criterionValue'] = $substitutionCriterionTab;
                    }
                    if($criterionPossibleValue->getCriterionType() === Criterion::TYPE_LOCATION){
                        $criterionArray[$criterion->getId()]['criterionValue'] = $regionRepository->asortFindAll();
                    }
                }
            }
        }
        return $this->render(
            'Offer/Canvas/viewOneCanvas.html.twig',
            [
                'canvas' => $canvas,
                'criterionArray' => $criterionArray,
                'canvasType' => ($canvas->getCanvasType() === Canvas::CANVAS_WISH) ? 'wish' : 'offer',
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="offer_canvas_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Canvas $canvas
     * @param CriterionRepository $criterionRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @return Response
     * @Translation("Modification d'un canevas")
     */
    public function edit(
        Request $request,
        Canvas $canvas,
        CriterionRepository $criterionRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository
    ): Response {
        $form = $this->createForm(CanvasType::class, $canvas);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $canvas->getCriteria()->clear();
            foreach ($request->request->get('canvas')['criteria'] as $criterionId) {
                $canvas->addCriterion($criterionRepository->findOneById($criterionId));
            }

            $formDisposition = $canvas->getFormDisposition();

            foreach ($formDisposition as $index => $row) {
                foreach ($row as $key => $element) {
                    if (!in_array($element[0], $request->request->get('canvas')['criteria'], true)) {
                        unset($formDisposition[$index][$key]);
                    }
                }
            }
            $canvas->setFormDisposition($formDisposition);

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Modifications enregistrées.');
            return $this->redirectToRoute(
                'offer_canvas_edit',
                [
                    'id' => $canvas->getId(),
                ]
            );
        }
        $criterionArray = [];
        foreach ($canvas->getFormDisposition() as $row) {
            foreach ($row as $value) {
                $criterion = $criterionRepository->findOneById($value[0]);
                $criterionPossibleValue = $criterionPossibleValueRepository->findOneBy(
                    [
                        'criterion' => $criterion,
                        'substitutionType' => $canvas->getSubstitutionType(),
                        'substitutionNature' => $canvas->getCanvasType()
                    ]
                );
                $criterionArray[$value[0]] = [
                    'label' => $criterion->getLabel(),
                    'type' => ($criterionPossibleValue !== null) ? $criterionPossibleValue->getCriterionType() : 'default'
                ];
            }
        }

        $canvasCriteria = [];

        foreach ($canvas->getCriteria() as $criterion) {
            $canvasCriteria[] = [
                'id' => $criterion->getId(),
                'label' => $criterion->getLabel(),
                'possible_value' => $criterionPossibleValueRepository->findOneBy(
                    [
                        'criterion' => $criterion,
                        'substitutionNature' => $canvas->getCanvasType(),
                        'substitutionType' => $canvas->getSubstitutionType()
                    ]
                )
            ];
        }

        usort(
            $canvasCriteria,
            static function ($subArray1, $subArray2) {
                return strcasecmp($subArray1['label'], $subArray2['label']);
            }
        );

        return $this->render(
            'Offer/Canvas/editCanvas.html.twig',
            [
                'canvas' => $canvas,
                'oneCriterion' => new Criterion(),
                'form' => $form->createView(),
                'criterionArray' => $criterionArray,
                'canvasCriteria' => $canvasCriteria
            ]
        );
    }

    /**
     * @Route("/{id}", name="offer_canvas_delete", methods={"DELETE"})
     * @Translation("Suppression d'un canevas")
     * @param Request $request
     * @param Canvas $canvas
     * @return Response
     */
    public function delete(Request $request, Canvas $canvas): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($canvas);
        $entityManager->flush();

        return $this->successJsonResponse();
    }

    /**
     * @Route("/{id}/edit/disposition", name="canvas_edit_disposition", methods={"POST"})
     * @Translation("Modification de la disposition des champs d'un canevas")
     * @param Request $request
     * @param Canvas $canvas
     * @return JsonResponse
     */
    public function addCanvasDisposition(Request $request, Canvas $canvas): JsonResponse
    {
        $jsonData = $request->getContent();
        $dataReturned = json_decode($jsonData);
        $canvas->setFormDisposition($dataReturned);

        $this->getDoctrine()->getManager()->flush();

        return $this->successJsonResponse();
    }

    /**
     * @Route("/ajax/{substitutionTypeId}/{substitutionNature}/criteria", name="canvas_ajax_criteria", methods={"POST"})
     * @NoTranslation()
     * @param SubstitutionTypeRepository $substitutionTypeRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @param int $substitutionTypeId
     * @param int $substitutionNature
     * @return JsonResponse
     */
    public function getCriteriaList(
        SubstitutionTypeRepository $substitutionTypeRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository,
        int $substitutionTypeId,
        int $substitutionNature
    ): JsonResponse {
        $substitutionType = $substitutionTypeRepository->findOneById($substitutionTypeId);
        $criterionPossiblesValues = $criterionPossibleValueRepository->findBy(
            [
                'substitutionType' => $substitutionType,
                'substitutionNature' => $substitutionNature
            ]
        );
        $criteriaList = [];
        foreach ($criterionPossiblesValues as $criterionPossibleValue) {
            $criterionId = $criterionPossibleValue->getCriterion()->getId();
            if (!in_array($criterionId, $criteriaList)) {
                $criteriaList[] = $criterionId;
            }
        }
        return new JsonResponse($criteriaList);
    }
}