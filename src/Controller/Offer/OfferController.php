<?php

namespace App\Controller\Offer;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Entity\Core\Telephone;
use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\Offer;
use App\Entity\Offer\OfferWishMatching;
use App\Entity\Offer\SubstitutionCriterion;
use App\Entity\Offer\SubstitutionType;
use App\Entity\Offer\Wish;
use App\Form\Offer\OfferPreselectType;
use App\Form\Offer\OfferType;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\MedicalSpecialtyQualificationRepository;
use App\Repository\Core\MedicalSpecialtyRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Repository\Offer\CanvasRepository;
use App\Repository\Offer\CriterionPossibleValueRepository;
use App\Repository\Offer\CriterionRepository;
use App\Repository\Offer\OfferRepository;
use App\Repository\Offer\OfferWishMatchingRepository;
use App\Repository\Offer\SubstitutionCriterionRepository;
use App\Repository\Offer\WishRepository;
use App\Service\Historization\OfferHistorizationManager;
use App\Service\Matching\DistanceHandler;
use App\Service\OfferFileManager;
use App\Service\SMSBox;
use App\Service\SMSManager;
use App\Service\SubstitutionDuplicaterService;
use App\Traits\MatchingTrait;
use App\Utils\CanvasHelper;
use App\Utils\MailMessage;
use App\Utils\MedicalSpecialtyHelper;
use App\Utils\OfferHelper;
use App\Utils\RoleHelper;
use App\Utils\TelephoneHelper;
use DateInterval;
use DateTime;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Offer\OfferHistorizationRepository;

/**
 * @Route("/offer/offer")
 */
class OfferController extends BaseAbstractController
{
    use MatchingTrait;

    /**
     * @Route("/", name="offer_offer_index", methods={"GET"})
     * @param OfferRepository $offerRepository
     * @return Response
     * @Translation("Visualisation de la liste des offres")
     */
    public function index(
        OfferRepository $offerRepository
    ): Response
    {
        return $this->render(
            'Offer/Offer/viewAllOffer.html.twig',
            ['offers' => $offerRepository->findAll()]
        );
    }

    /**
     * @Route("/new/{id}", name="offer_offer_new", methods={"GET","POST"})
     * @Translation("Ajout d'une nouvelle offre")
     * @param Request $request
     * @param CriterionRepository $criterionRepository
     * @param Person $person
     * @param MedicalSpecialtyRepository $medicalSpecialtyRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @param MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository
     * @param DepartmentRepository $departmentRepository
     * @param CanvasHelper $canvasHelper
     * @return Response
     * @throws Exception
     */
    public function new(
        Request $request,
        CriterionRepository $criterionRepository,
        Person $person,
        MedicalSpecialtyRepository $medicalSpecialtyRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository,
        MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository,
        DepartmentRepository $departmentRepository,
        CanvasHelper $canvasHelper
    ): Response
    {
        $preBuiltOffer = $request->get('preBuiltOffer');
        if ($preBuiltOffer !== null) {
            $offer = $preBuiltOffer;
            $request->request->remove('offer_preselect');
        } else {
            $offer = OfferHelper::createDefaultOffer($person, $this->getUser());
        }

        $form = $this->createForm(
            OfferType::class,
            $offer,
            [
                'action' => $this->generateUrl('offer_offer_new', ['id' => $person->getId()])
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $criterionOffer = $request->get('criterion_offer');
            $onCall = $request->get('onCall');
            $uploadedFiles = $request->files->get('criterion_offer');
            $isAsap = $request->request->get('select_date_type');
            $skills = $request->request->get('offer_skills');
            $canvas = $offer->getCanvas();
            $entityManager = $this->getDoctrine()->getManager();

            //storage files
            if (!empty($uploadedFiles)) {
                foreach ($uploadedFiles as $idCriterion => $file) {
                    if ($file != null) {//il y a un file uploadé
                        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        // this is needed to safely include the file name as part of the URL
                        $safeFilename = transliterator_transliterate(
                            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
                            $originalFilename
                        );
                        $newFilename = $safeFilename . '-' . uniqid('', true) . '.' . $file->guessExtension();
                        // Move the file to the directory
                        try {
                            $file->move(
                                $this->getParameter('img_offer_directory'),
                                $newFilename
                            );
                        } catch (FileException $e) {
                            return new JsonResponse(
                                [
                                    'error' => true,
                                    'text' => 'Une erreur est survenue lors de l\'upload de fichiers'
                                ]
                            );
                            // ... handle exception if something happens during file upload
                        }
                        $criterion = $criterionRepository->findOneById($idCriterion);

                        $substitutionCriterion = new SubstitutionCriterion();
                        $substitutionCriterion->setCriterion($criterion);
                        $substitutionCriterion->setValue($newFilename);
                        $substitutionCriterion->setSubstitution($offer);
                        $offer->addSubstitutionCriterion($substitutionCriterion);
                        $entityManager->persist($substitutionCriterion);
                    }
                }
            }
            if (($offer->getState() === Offer::STATE_FILLED_BY_US || $offer->getState() === Offer::STATE_FILLED_BY_OTHER) && ($person instanceof PersonNatural)) {
                $person->addProvidedOffer($offer);
                $entityManager->persist($person);
            }
            if (isset($criterionOffer)) {
                $substitutionType = $canvas->getSubstitutionType();
                foreach ($criterionOffer as $idCriterion => $valueCriterion) {
                    $criterion = $criterionRepository->findOneById($idCriterion);
                    $substitutionCriterion = new SubstitutionCriterion();

                    $possibleValue = $criterionPossibleValueRepository->findOneBy(
                        [
                            'criterion' => $criterion,
                            'substitutionType' => $substitutionType,
                            'substitutionNature' => $canvas->getCanvasType()
                        ]
                    );

                    //if it is a type of criterion 'choice week' then we formalize specially
                    if ($possibleValue->getCriterionType() === Criterion::TYPE_CHOICE_WEEK) {
                        $choiceWeekValue = 0;
                        foreach ($valueCriterion as $value) {
                            $choiceWeekValue += pow(2, $value);
                        }
                        $substitutionCriterion->setValue($choiceWeekValue);
                    } elseif ($possibleValue->getCriterionType() === Criterion::TYPE_LOCATION || $possibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                        $substitutionCriterion->setValue(array_unique($valueCriterion));
                    } else {
                        $substitutionCriterion->setValue($valueCriterion);
                    }
                    $substitutionCriterion->setCriterion($criterion);
                    $substitutionCriterion->setSubstitution($offer);
                    $offer->addSubstitutionCriterion($substitutionCriterion);
                    $entityManager->persist($substitutionCriterion);
                }
            }
            $newSkills = (new MedicalSpecialtyHelper($medicalSpecialtyRepository))->getFormattedOfferSkills($skills);
            $offer->setSkills($newSkills);
            if (isset($onCall) && !empty($onCall)) {
                if (isset($onCall['displayed']) && $onCall['displayed'] === 'true') {
                    $offer->setOnCall($onCall);
                }
            }
            $offer->setSubstitutionType($offer->getCanvas()->getSubstitutionType());
            $newDateTime = new DateTime();
            $newDateTime->add(new DateInterval('PT2H'));
            $offer->setLastUpdateDate($newDateTime);
            if ($isAsap !== null && $isAsap === 'asap') {
                $offer->setStartingDateModularity(0);
                $offer->setStartingDateModularityType(Offer::MODULARITY_TYPE_BOTH);
            }
            $offer->setAsSoonAsPossible($isAsap === 'asap');

            $department = $request->request->get('offer')['department'];
            if ($department != '') {
                $offer->setDepartment($departmentRepository->find($department));
            } elseif ($person->getDefaultAddress() != null) {
                $offer->setDepartment($person->getDefaultAddress()->getDepartment());
            }
            $offer->setLastFullUpdateAt(new DateTime());
            $entityManager->persist($offer);
            $entityManager->flush();

            $this->addFlash('success', 'Offre créée');
            return $this->redirectToRoute('redirect_from_person_instance', ['id' => $person->getId()]);
        }

        $criteria = $criterionRepository->findAll();
        $skillQualification = $medicalSpecialtyQualificationRepository->findOneBy(['label' => 'Compétence']);
        $medicalSpecialties = $medicalSpecialtyRepository->findAll();
        $medicalSpecialtiesInArray = [];
        foreach ($medicalSpecialties as $medicalSpecialty) {
            $medicalSpecialtiesInArray[$medicalSpecialty->getId()] = $medicalSpecialty->getOfficialLabel();
        }

        $criterionGarde = $criterionRepository->findOneBy(['label' => 'Gardes']);
        $resultCanvasHelper = $canvasHelper->getFormDispositionForOffer($offer);
        return $this->render(
            'Offer/Offer/newOffer.html.twig',
            [
                'offer' => $offer,
                'person' => $person,
                'form' => $form->createView(),
                'criteria' => $criteria,
                'criterion' => new Criterion(),
                'skillsSpecialties' => $skillQualification->getMedicalSpecialties(),
                'medicalSpecialties' => $medicalSpecialtiesInArray,
                'criterion_garde_id' => $criterionGarde ? $criterionGarde->getId() : null,
                'criterionArray' => $resultCanvasHelper['criterionArray'],
                'tabForOldFileInput' => $resultCanvasHelper['tabForOldFileInput'],
                'canvas' => $offer->getCanvas()
            ]
        );
    }

    /**
     * @Route("/{id}", name="offer_offer_show", methods={"GET"})
     * @param Offer $offer
     * @param OfferHistorizationRepository $offerHistorizationRepository
     * @param RoleHelper $roleHelper
     * @return Response
     * @Translation("Visualisation d'une offre")
     */
    public function show(
        Offer $offer,
        OfferHistorizationRepository $offerHistorizationRepository,
        RoleHelper $roleHelper
    ): Response
    {
        // pour les CR , les commerciales et la direction
        /** @var Staff $authUser */
        $authUser = $this->getUser();
        $isAuthorized = $roleHelper->userHasRoles(["ROLE_DIRECTION", "ROLE_CHARGE_DE_RECRUTEMENT", "ROLE_COMMERCIAL"], $authUser);

        $offerHistorizationList = $offerHistorizationRepository->findBy(['offer' => $offer->getId()]);

        $person = $offer->getPerson();
        $offerSubstitutionType = SubstitutionType::LABEL[$offer->getCanvas()->getSubstitutionType()->getSubstitutionType()];
        return $this->render(
            'Offer/Offer/viewOneOffer.html.twig',
            [
                'offer' => $offer,
                'person' => $person,
                'offerSubstitutionType' => $offerSubstitutionType,
                'offerHistorizationList' => $offerHistorizationList,
                "isAuthorized" => $isAuthorized
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="offer_offer_edit", methods={"GET","POST"})
     * @Route("/{id}/edit/from-model", name="offer_offer_edit_from_model", methods={"GET"})
     * @Translation("Modification d'une offre")
     * @param Request $request
     * @param Offer $offer
     * @param CriterionRepository $criterionRepository
     * @param SubstitutionCriterionRepository $substitutionCriterionRepository
     * @param MedicalSpecialtyRepository $medicalSpecialtyRepository
     * @param CanvasRepository $canvasRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @param MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository
     * @param CanvasHelper $canvasHelper
     * @param OfferHistorizationManager $offerHistorizationManager
     * @param RoleHelper $roleHelper
     * @return Response
     * @throws Exception
     */
    public function edit(
        Request $request,
        Offer $offer,
        CriterionRepository $criterionRepository,
        SubstitutionCriterionRepository $substitutionCriterionRepository,
        MedicalSpecialtyRepository $medicalSpecialtyRepository,
        CanvasRepository $canvasRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository,
        MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository,
        CanvasHelper $canvasHelper,
        OfferHistorizationManager $offerHistorizationManager,
        RoleHelper $roleHelper
    ): Response {
        $oldOffer = clone $offer;
        $fromModel = strstr($request->getUri(), 'from-model');
        if($fromModel !== false){
            $offer->fullClearDates();
        }
        $form = $this->createForm(OfferType::class, $offer, [
            'action' => $this->generateUrl('offer_offer_edit', ['id' => $offer->getId()])
        ]);
        $form->handleRequest($request);

        /** @var Staff $authUser */
        $authUser = $this->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $isAuthorized = $roleHelper->userHasRoles(["ROLE_ROOT", "ROLE_DIRECTION", "ROLE_CHARGE_DE_RECRUTEMENT", "ROLE_COMMERCIAL"], $authUser);
            if ($isAuthorized) {
                $historizationOptions = [
                    'oldOffer' => $oldOffer,
                    'staff' => $authUser
                ];
                $offerHistorizationManager->historize($offer, $historizationOptions);
                $offerHistorizationManager->save();
            }

            $manager = $this->getDoctrine()->getManager();
            $criterionOffer = $request->get('criterion_offer');
            $onCall = $request->get('onCall');
            $offer->getSubstitutionCriteria()->clear();
            $uploadedFiles = $request->files->get('criterion_offer');
            $oldFiles = $request->get('criterion_offer_old_file');
            $isAsap = $request->request->get('select_date_type');
            $skills = $request->request->get('offer_skills');
            //storage files
            if (!empty($uploadedFiles)) {
                foreach ($uploadedFiles as $idCriterion => $file) {
                    $oldFileName = '';
                    if (!empty($oldFiles) && array_key_exists($idCriterion, $oldFiles)) {
                        $oldFileName = $oldFiles[$idCriterion];
                    }
                    if ($file != null) {//il y a un file uploadé
                        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        // this is needed to safely include the file name as part of the URL
                        $safeFilename = transliterator_transliterate(
                            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
                            $originalFilename
                        );
                        $newFilename = $safeFilename . '-' . uniqid('', true) . '.' . $file->guessExtension();
                        // Move the file to the directory
                        try {
                            $file->move(
                                $this->getParameter('img_offer_directory'),
                                $newFilename
                            );
                        } catch (FileException $e) {
                            $this->addFlash('error', 'Une erreur est survenue lors de l\'upload de fichiers');
                            return new JsonResponse(
                                [
                                    'error' => true,
                                    'text' => 'Une erreur est survenue lors de l\'upload de fichiers'
                                ]
                            );
                            // ... handle exception if something happens during file upload
                        }
                        if ($oldFileName != '') {
                            $offerFileManager = new OfferFileManager();
                            $offerFileManager->removeFileAfterUpdate(
                                $this->getParameter('img_offer_directory'),
                                $oldFileName
                            );
                        }
                    } else {//rien n'a changé (pas de nouveau file)
                        $newFilename = $oldFileName;
                    }

                    unset($oldFiles[$idCriterion]);

                    $criterion = $criterionRepository->findOneById($idCriterion);
                    $substitutionCriterion = $substitutionCriterionRepository->findOneBy(
                        [
                            'substitution' => $offer->getId(),
                            'criterion' => $idCriterion
                        ]
                    );
                    if ($substitutionCriterion == null) {
                        $substitutionCriterion = new SubstitutionCriterion();
                    }
                    $substitutionCriterion->setCriterion($criterion);
                    $substitutionCriterion->setValue($newFilename);
                    $substitutionCriterion->setSubstitution($offer);
                    $offer->addSubstitutionCriterion($substitutionCriterion);
                    $this->getDoctrine()->getManager()->persist($substitutionCriterion);
                }
            }
            if (isset($oldFiles)) {
                foreach ($oldFiles as $idCriterion => $old_file_name_val) {
                    $offerFileManager = new OfferFileManager();
                    $offerFileManager->removeFileAfterUpdate(
                        $this->getParameter('img_offer_directory'),
                        $old_file_name_val
                    );
                }
            }

            $person = $offer->getPerson();
            if ($person instanceof PersonNatural) {
                if ($offer->getState() === Offer::STATE_FILLED_BY_US || $offer->getState() === Offer::STATE_FILLED_BY_OTHER) {
                    $person->addProvidedOffer($offer);
                } else {
                    $person->removeProvidedOffer($offer);
                }
                $this->getDoctrine()->getManager()->persist($person);
            }
            //updating criterion Values for offer
            if (isset($criterionOffer)) {
                $offer->getSubstitutionCriteria()->clear();
                foreach ($criterionOffer as $idCriterion => $valueCriterion) {
                    $criterion = $criterionRepository->findOneById($idCriterion);
                    $substitutionCriterion = $substitutionCriterionRepository->findOneBy(
                        [
                            'substitution' => $offer->getId(),
                            'criterion' => $idCriterion
                        ]
                    );

                    if ($substitutionCriterion == null) {
                        $substitutionCriterion = new SubstitutionCriterion();
                    }

                    $criterionPossibleValue = $criterionPossibleValueRepository->findOneBy(
                        [
                            'criterion' => $criterion,
                            'substitutionType' => $offer->getCanvas()->getSubstitutionType(),
                            'substitutionNature' => Canvas::CANVAS_OFFER
                        ]
                    );

                    //if it is a type of criterion 'choice week' then we formalize specially
                    if ($criterionPossibleValue->getCriterionType() == Criterion::TYPE_CHOICE_WEEK) {
                        $choiceWeekValue = 0;
                        foreach ($valueCriterion as $value) {
                            $choiceWeekValue += pow(2, $value);
                        }
                        $substitutionCriterion->setValue($choiceWeekValue);
                    } elseif ($criterionPossibleValue->getCriterionType() === Criterion::TYPE_LOCATION || $criterionPossibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                        $substitutionCriterion->setValue(array_unique($valueCriterion));
                    } else {
                        $substitutionCriterion->setValue($valueCriterion);
                    }
                    $substitutionCriterion->setCriterion($criterion);
                    $substitutionCriterion->setSubstitution($offer);
                    $offer->addSubstitutionCriterion($substitutionCriterion);
                    $manager->persist($substitutionCriterion);
                }
            }
            $canvas = $request->request->get('offer')['canvas'];
            if ($canvas != null && $canvas != '' && $canvas != $offer->getCanvas()->getId()) {
                $offer->setCanvas($canvasRepository->findOneById($canvas));
            }
            $offerIsActive = $request->get('offer_is_active');
            if (isset($offerIsActive)) {
                foreach ($offer->getOfferWishMatchings() as $offerWishMatching) {
                    $offerWishMatching->setIsActive(true);
                    $manager->persist($offerWishMatching);
                }
            }
            $newSkills = (new MedicalSpecialtyHelper($medicalSpecialtyRepository))->getFormattedOfferSkills($skills);
            $offer->setSkills($newSkills);
            if (isset($onCall) && !empty($onCall)) {
                if (isset($onCall['displayed']) && $onCall['displayed'] === 'true') {
                    $offer->setOnCall($onCall);
                }
            }
            $newDateTime = new DateTime();
            $newDateTime->add(new DateInterval('PT2H'));
            $offer->setLastUpdateDate($newDateTime);
            if ($isAsap !== null && $isAsap === 'asap') {
                $offer->setStartingDateModularity(0);
                $offer->setStartingDateModularityType(Offer::MODULARITY_TYPE_BOTH);
            }
            $offer->setAsSoonAsPossible($isAsap === 'asap');
            $manager->persist($offer);
            $manager->flush();
            $this->addFlash('success', 'Offre modifiée avec succès');
            return $this->redirectToRoute(
                'offer_offer_edit',
                [
                    'id' => $offer->getId(),
                ]
            );
        }

        $medicalSpecialties = $medicalSpecialtyRepository->findAll();
        $medicalSpecialtiesInArray = [];
        foreach ($medicalSpecialties as $medicalSpecialty) {
            $medicalSpecialtiesInArray[$medicalSpecialty->getId()] = $medicalSpecialty->getOfficialLabel();
        }

        $resultCanvasHelper = $canvasHelper->getFormDispositionForOffer($offer);

        $criterionArray = $resultCanvasHelper['criterionArray'];
        $tabForOldFileInput = $resultCanvasHelper['tabForOldFileInput'];
        $person = $offer->getPerson();

        $offerSubstitutionType = SubstitutionType::LABEL[$offer->getCanvas()->getSubstitutionType()->getSubstitutionType()];
        $skillQualification = $medicalSpecialtyQualificationRepository->findOneBy(['label' => 'Compétence']);
        $criterionGarde = $criterionRepository->findOneBy(['label' => 'Gardes']);
        return $this->render(
            'Offer/Offer/editOffer.html.twig',
            [
                'offer' => $offer,
                'form' => $form->createView(),
                'person' => $person,
                'medicalSpecialties' => $medicalSpecialtiesInArray,
                'canvas' => $offer->getCanvas(),
                'criterionArray' => $criterionArray,
                'criterion' => new Criterion(),
                'tabForOldFileInput' => $tabForOldFileInput,
                'offerSubstitutionType' => $offerSubstitutionType,
                'skillsSpecialties' => $skillQualification->getMedicalSpecialties(),
                'offerSkills' => $offer->getSkills(),
                'criterion_garde_id' => $criterionGarde ? $criterionGarde->getId() : null
            ]
        );
    }

    /**
     * @Route("/{id}", name="offer_offer_delete", methods={"DELETE"})
     * @Translation("Suppression d'une offre")
     * @param Request $request
     * @param Offer $offer
     * @return Response
     */
    public function delete(Request $request, Offer $offer): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($offer);
        $entityManager->flush();

        return $this->successJsonResponse();
    }

    /**
     * @Route("/{id}/send/{idPersonNatural}", name="offer_offer_send", methods={"POST"})
     * @Route("/multiple/send/{idPersonNatural}", name="offer_offers_send", methods={"POST"})
     * @Route("/{id}/multiple/send", name="offer_offer_send_persons", methods={"POST"})
     * @Translation("Envoi d'une ou plusieurs offre(s)")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @param PersonNaturalRepository $personNaturalRepository
     * @param OfferRepository $offerRepository
     * @param int|null $idPersonNatural
     * @param Offer|null $offer
     * @return JsonResponse
     */
    public function sendOffer(
        Request $request,
        Swift_Mailer $mailer,
        PersonNaturalRepository $personNaturalRepository,
        OfferRepository $offerRepository,
        Offer $offer = null,
        int $idPersonNatural = null
    ): JsonResponse
    {
        /**
         * @var PersonNatural $personNatural
         */
        if (isset($idPersonNatural)) {
            $personNatural = $personNaturalRepository->findOneById($idPersonNatural);
            if ($personNatural == null) {
                return $this->errorJsonResponse("Aucune personne ne possède l'identifiant numéro " . $idPersonNatural);
            }
        }

        $message = $request->request->get('message');
        $requestOffers = $request->request->get('offers');
        $offers = [];
        $requestPersons = $request->request->get('persons');
        $persons = [];
        $requestMailContent = $request->request->get('mail_content');
        if (isset($requestOffers)) {
            if (!empty($requestOffers)) {
                foreach ($requestOffers as $requestOfferId) {
                    $requestOffer = $offerRepository->findOneById($requestOfferId);
                    if (isset($requestOffer)) {
                        $offers[] = $requestOffer;
                    }
                }
            } else {
                return $this->errorJsonResponse('Veuillez sélectionner au moins une offre');
            }
        } else {
            if (isset($offer)) {
                $offers[] = $offer;
            }
        }

        if (isset($requestPersons) && !isset($idPersonNatural)) {
            if (!empty($requestPersons)) {
                foreach ($requestPersons as $requestPersonId) {
                    $requestPerson = $personNaturalRepository->findOneById($requestPersonId);
                    if (isset($requestPerson)) {
                        $persons[] = $requestPerson;
                    }
                }
            } else {
                return $this->errorJsonResponse('Veuillez sélectionner au moins une personne');
            }
        } else {
            $persons[] = $personNatural;
        }

        $errors = [];
        if ($request->request->get('isSMS') == 'true') {
            if ($_ENV['APP_ENV'] == 'dev') {
                if (!empty($persons)) {
                    foreach ($persons as $person) {
                        if (count($cells = $person->getTelephonesByType(Telephone::CELL)) > 0) {
                            $SMSManager = new SMSManager(new SMSBox($_ENV['SMS_API'], $message));
                            $SMSManager->addRecipient(TelephoneHelper::internationalizeTelephoneNumber($cells->first()));
                            if (!$SMSManager->send()) {
                                $errors[] = [
                                    'code' => 1,//Une erreur est survenue au moment de l'envoi du SMS
                                    'person' => $person->getId(),
                                ];
                                continue;
                            }
                            foreach ($offers as $offer) {
                                $person->addOffersSent($offer);
                            }
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($person);
                            $em->flush();
                        } else {
                            $errors[] = [
                                'code' => 2,//Cette personne ne possède pas de numéro de téléphone portable
                                'person' => $person->getId(),
                            ];
                            continue;
                        }
                    }
                }
            }
        } elseif ($request->request->get('isEmail') == 'true') {
            if (!empty($persons)) {
                /** @var Person $person */
                foreach ($persons as $person) {
                    if ($person->getDefaultMail() === null) {
                        $errors[] = [
                            'code' => 3,//Cette personne ne possède pas d'adresse mail par défaut
                            'person' => $person->getId(),
                        ];
                        continue;
                    }
                    $advisor = $person->getAdvisor();
                    if (isset($requestMailContent) && !empty($requestMailContent)) {
                        $mailGreetings = (count($persons) === 1) ? $requestMailContent['greetings'] : MailMessage::getGreeting($person);
                        $mailOffers = $requestMailContent['offers'];
                        $mailThanks = $requestMailContent['thanks'];
                        $mailSignature = $requestMailContent['signature'];
                    } else {
                        $mailGreetings = MailMessage::getGreeting($person);
                        $mailOffers = MailMessage::getOffers($offers);
                        $mailThanks = MailMessage::getThanks();
                        $signature = ($advisor !== null) ? $advisor->getFirstName() . ' ' . $advisor->getLastName() : '';
                        $mailSignature = $signature;
                    }
                    $message = (new Swift_Message('Media-Santé - Offre' . ((count($offers) > 1) ? 's' : '')))
                        ->setFrom('noreply@media-sante.com')
                        ->setTo($person->getDefaultMail()->getAddress())
                        ->setBody(
                            $this->renderView(
                                'Mail/mailSimple.html.twig',
                                [
                                    'greeting' => $mailGreetings,
                                    'content' => $mailOffers,
                                    'thanks' => $mailThanks,
                                    'signature' => $mailSignature
                                ]
                            ),
                            'text/html'
                        );

                    if (!$mailer->send($message)) {
                        $errors[] = [
                            'code' => 4,//Une erreur est survenue au moment de l'envoi de l'Email
                            'person' => $person->getId(),
                        ];
                        continue;
                    } else {
                        foreach ($offers as $offer) {
                            $person->addOffersSent($offer);
                        }
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($person);
                        $em->flush();
                    }
                }
            }
        } else {
            return $this->errorJsonResponse('Une offre ne peut être envoyée que par SMS ou par Email');
        }

        if (!empty($errors)) {
            $errorMessage = count($errors) . ' mails sur ' . count($persons) . " n'ont pas été envoyés à cause d'une erreur ou parce qu'ils n'ont pas d'adresse mail par défaut. 
            ID des personnes n'ayant pas eu le mail envoyé : ";
            foreach ($errors as $key => $error) {
                if ($key != 0) {
                    $errorMessage .= ',';
                }
                $errorMessage .= $error['person'];
            }
            return $this->errorJsonResponse($errorMessage);
        }

        $response = [];
        if (isset($idPersonNatural)) {
            $response['receiver'] = $personNatural->getFirstName() . ' ' . $personNatural->getLastName();
        }
        return $this->successJsonResponse($response);
    }

    /**
     * @Route("/ajax/get-mail-content/{id}", name="ajax_offer_get_content_mail", methods={"POST"})
     * @param Request $request
     * @param Person $person
     * @param OfferRepository $offerRepository
     * @return Response
     * @NoTranslation()
     */
    public function ajaxGetMailContent(
        Request $request,
        Person $person,
        OfferRepository $offerRepository
    ): Response
    {
        $requestOffers = $request->request->get('offers');
        $advisor = $this->getUser();
        $offers = [];
        if ($person instanceof PersonNatural) {
            $advisor = $person->getAdvisor();
        }
        if (isset($requestOffers)) {
            if (!empty($requestOffers)) {
                foreach ($requestOffers as $requestOfferId) {
                    $requestOffer = $offerRepository->findOneById($requestOfferId);
                    if (isset($requestOffer)) {
                        $offers[] = $requestOffer;
                    } else {
                        return $this->errorJsonResponse('Aucune offre trouvée avec le numéro ' . $requestOfferId);
                    }
                }
            } else {
                return $this->errorJsonResponse('Veuillez sélectionner au moins une offre');
            }
            //$content = "<span class='font-italic'>[Contenu des offres (non modifiable)]</span>";
        }
        $signature = '[SIGNATURE A RENSEIGNER]';
        if (isset($advisor)) {
            $signature = $advisor->getFirstName() . ' ' . $advisor->getLastName();
        }
        return $this->render(
            'Mail/mailSimple.html.twig',
            [
                'greeting' => MailMessage::getGreeting($person),
                'content' => MailMessage::getOffers($offers),
                'thanks' => MailMessage::getThanks(),
                'signature' => $signature
            ]
        );
    }

    /**
     * @Route("/ajax/category/{id}", name="ajax_offer_category_request", methods={"POST"})
     * @param Request $request
     * @param Canvas $canvas
     * @param CriterionRepository $criterionRepository
     * @param ManagerRegistry $registry
     * @return JsonResponse
     * @NoTranslation()
     */
    public function ajaxCategoryRequest(
        Request $request,
        Canvas $canvas,
        CriterionRepository $criterionRepository,
        ManagerRegistry $registry
    ): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }
        // Return the associated canvas and criterion on json format
        $canvasArray = [];
        $criterionArray = [];
        if ($canvas) {
            $canvasArray['id'] = $canvas->getId();
            $canvasArray['label'] = $canvas->getLabel();
            $canvasArray['formDisposition'] = $canvas->getFormDisposition();
            $listCriterion = [];
            foreach ($canvasArray['formDisposition'] as $row) {
                foreach ($row as $value) {
                    $listCriterion[] = $criterionRepository->findOneById($value[0]);
                }
            }
            foreach ($listCriterion as $criterion) {
                foreach ($criterion->getCriterionPossibleValues() as $criterionPossibleValue) {
                    if ($criterionPossibleValue->getSubstitutionType() == $canvas->getSubstitutionType()) {
                        $possibleValue = $criterionPossibleValue->getPossibleValue();
                        if ($criterionPossibleValue->getCriterionType() == Criterion::TYPE_ENTITY) {
                            $possibleValueString = json_decode($criterionPossibleValue->getPossibleValue());
                            $entityFullName = explode('::', $possibleValueString)[0];
                            $fieldName = explode('::', $possibleValueString)[1];

                            $targettedEntity = str_replace(
                                'Entity',
                                'Repository',
                                $entityFullName . 'Repository'
                            );
                            $targettedEntityInstance = new $targettedEntity($registry);

                            $targetLabels = [];

                            foreach ($targettedEntityInstance->findAll() as $element) {
                                $targetLabels[] = $element->{'get' . ucfirst($fieldName)}(); //sorry
                            }
                            $possibleValue = $targetLabels;
                        }
                        if ($criterionPossibleValue->getCriterionType() === Criterion::TYPE_LOCATION || $criterionPossibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                            $jsonData = [];
                            foreach (json_decode($criterionPossibleValue->getPossibleValue()) as $source) {
                                $sourceKey = array_reverse(explode('/', explode('.', $source)[1]))[0];
                                $jsonData[$sourceKey] = json_decode(file_get_contents($source));
                            }
                            $possibleValue = $jsonData;
                        }
                        $criterionArray[$criterion->getId()] = [
                            'label' => $criterion->getLabel(),
                            'type' => $criterionPossibleValue->getCriterionType(),
                            'value' => $possibleValue,
                            'dependency' => []
                        ];
                        $dependency = $criterion->getDependency();
                        if (!empty($dependency)) {
                            $criterionArray[$criterion->getId()]['dependency'] = [
                                'id' => array_keys($dependency)[0],
                                'value' => $dependency[array_keys($dependency)[0]]
                            ];
                        }
                    }
                }
            }
        }
        $response = [];
        $response['canvas'] = $canvasArray;
        $response['criterionArray'] = $criterionArray;
        $response['substitutionTypes'] = Offer::TYPES;
        if (!in_array(
            $canvas->getSubstitutionType()->getSubstitutionType(),
            SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_LIBERAL_REPLACEMENT]
        )) {
            $response['substitutionTypes'] = Offer::TYPES_SALARIED_CESSION;
        }
        if (in_array(
            $canvas->getSubstitutionType()->getSubstitutionType(),
            SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SALARIED_POSITION]
        )) {
            $response['substitutionHeadType'] = 'PS';
        }
        return new JsonResponse($response);
    }

    /**
     * @Route("/ajax/search", name="ajax_search_offer_request", methods={"POST"})
     * @param Request $request
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     * @NoTranslation()
     */
    public function ajaxSearchOffer(
        Request $request,
        OfferRepository $offerRepository
    ): JsonResponse
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $offerId = $request->request->get('offerId');
        if (!$offerId || preg_match('/^[0-9]+$/', $offerId) !== 1) {
            return $this->errorJsonResponse("L'ID de l'offre entré n'est pas correct. Il doit uniquement être composé d'une série de chiffres.");
        }
        $offer = $offerRepository->findOneBy(
            [
                'id' => $offerId
            ]
        );
        if (!isset($offer)) {
            return $this->errorJsonResponse('Aucune offre trouvée sous ce numéro.');
        }

        $resultOffer = [
            'id' => $offer->getId(),
            'state' => $offer->getState(),
            'person' => [
                'id' => $offer->getPerson()->getId(),
                'name' => $offer->getPerson()->getName(),
                'has_mail' => (count($offer->getPerson()->getMails()) > 0)
            ]
        ];
        return $this->successJsonResponse($resultOffer);
    }

    /**
     * @Route("/{id}/duplicate", name="offer_offer_duplicate", methods={"POST","GET"})
     * @Translation("Duplication d'une offre")
     * @param Request $request
     * @param Offer $offer
     * @param SubstitutionDuplicaterService $duplicateService
     * @return JsonResponse|RedirectResponse
     */
    public function duplicate(Request $request, Offer $offer, SubstitutionDuplicaterService $duplicateService)
    {
        $newOffer = $duplicateService->duplicateOffer($offer, $this->getUser(), $offer->getIsModel());
        $this->addFlash('success', 'Duplication de l\'offre effectuée');
        if ($request->isMethod('post')) {
            return $this->successJsonResponse();
        }

        return $this->redirectToRoute(
            'offer_offer_edit_from_model',
            [
                'id' => $newOffer->getId()
            ]
        );
    }

    /**
     * @Route("/create/model/{id}", name="offer_create_from_model", methods={"GET","POST"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonNatural $person
     * @param OfferRepository $offerRepository
     * @param SubstitutionDuplicaterService $duplicateService
     * @return JsonResponse|Response
     * @throws Exception
     */
    public function createOfferFromModel(
        Request $request,
        PersonNatural $person,
        OfferRepository $offerRepository,
        SubstitutionDuplicaterService $duplicateService
    )
    {
        if ($request->isMethod('post')) {
            $offerModelId = $request->request->get('model');

            /** @var Offer $offerModel */
            $offerModel = $offerRepository->findOneBy(
                [
                    'id' => $offerModelId
                ]
            );
            if (!$offerModel) {
                $this->addFlash('error', "Le modèle n'a pas été trouvé");
                return $this->redirectToRoute('offer_offer_new');
            }
            $newOffer = $duplicateService->duplicateOffer($offerModel, $this->getUser(), true);

            $this->addFlash('success', 'Offre créée');
            return $this->redirectToRoute('offer_offer_edit', ['id' => $newOffer->getId()]);
        }
        $offerModels = $offerRepository->findBy(
            [
                'person' => $person,
                'isModel' => true,
            ]
        );
        if (empty($offerModels)) {
            return $this->errorJsonResponse("Cette personne ne possède pas de modèle d'offre.");
        }
        $offers = [];
        foreach ($offerModels as $offer) {
            $offers[$offer->getId()] = [
                'id' => $offer->getId(),
                'substitutionType' => '[' . $offer->getId() . '] ' . SubstitutionType::LABEL[$offer->getSubstitutionType()->getSubstitutionType()]
            ];
        }
        return new Response(
            $this->renderView(
                'Offer/Model/createFromModel.html.twig',
                [
                    'url' => $this->generateUrl(
                        'offer_create_from_model',
                        [
                            'id' => $person->getId()
                        ]
                    ),
                    'elements' => $offers
                ]
            )
        );
    }

    /**
     * @NoTranslation()
     * @Route("/new/preselect/{id}", name="offer_preselect_canvas", methods={"GET","POST"})
     * @param Person $person
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function preSelectOfferCanvas(
        Person $person,
        Request $request
    ): Response
    {
        $offer = OfferHelper::createDefaultOffer($person, $this->getUser());
        $form = $this->createForm(
            OfferPreselectType::class,
            $offer,
            ['action' => $this->generateUrl('offer_preselect_canvas', ['id' => $person->getId()])]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $offer->setSubstitutionType($offer->getCanvas()->getSubstitutionType());
            return $this->forward(
                'App\Controller\Offer\OfferController::new',
                [
                    'id' => $person->getId(),
                    'preBuiltOffer' => $offer,
                ]
            );
        }
        return new Response(
            $this->renderView(
                'Offer/Offer/preSelectForm.html.twig',
                [
                    'form' => $form->createView()
                ]
            )
        );
    }

    /**
     * @Route("/{id}/reinstate", name="offer_offer_reinstate", methods={"POST"})
     * @Translation("Réintégration d'une offre au matching")
     * @param Offer $offer
     * @return JsonResponse
     */
    public function reinstateInMatching(Offer $offer): JsonResponse
    {
        $manager = $this->getDoctrine()->getManager();
        foreach ($offer->getOfferWishMatchings() as $offerWishMatching) {
            $offerWishMatching->setIsActive(true);
            $manager->persist($offerWishMatching);
        }
        $manager->flush();
        return $this->successJsonResponse();
    }

    /**
     * @Route("/matching/all", name="matching_offers", methods={"GET","POST"})
     * @Route("/matching/{oneOfferId}", name="matching_offers_one_offer", methods={"GET","POST"})
     * @Translation("Matching des offres")
     * @param OfferRepository $offerRepository
     * @param WishRepository $wishRepository
     * @param CriterionRepository $criterionRepository
     * @param int|null $oneOfferId
     * @return Response
     * @throws Exception
     */
    public function matchingOffer(
        OfferRepository $offerRepository,
        WishRepository $wishRepository,
        CriterionRepository $criterionRepository,
        ?int $oneOfferId
    ): Response
    {
        // récupération des critères spécifiques de nombre d'acte
        $criterionInterval1 = $criterionRepository->findOneByLabel('Nombre d\'actes intervalle min');
        $criterionInterval2 = $criterionRepository->findOneByLabel('Nombre d\'actes intervalle max');
        $criterionPonderation = $criterionRepository->findOneByLabel('Nombre d\'actes pondération');
        $criterionValeur = $criterionRepository->findOneByLabel('Nombre d\'actes valeur');
        $availableWishes = $wishRepository->wishesForOffersMatching();
        $result = [];
        $manager = $this->getDoctrine()->getManager();
        $isOneOffer = true;
        if (isset($oneOfferId)) { // si le matching est uniquement sur une seule offre
            /** @var Offer $oneOffer */
            $oneOffer = $offerRepository->findOneById($oneOfferId);
            if (!$oneOffer->getIsModel()) { // si c'est pas un modèle
                //matching sur 1 seule offre
                $functionResult = $this->matchingOneOffer(
                    $oneOffer,
                    $availableWishes,
                    $result,
                    $manager,
                    $criterionRepository,
                    $criterionInterval1,
                    $criterionInterval2,
                    $criterionPonderation,
                    $criterionValeur
                );
                $result = $functionResult['result'];
            }
        } else { //matching sur toutes les offres
            //on récupère les offres en cours
            $offersInProgress = $offerRepository->offersForOfferMatching();
            $result = ['Liberal' => [], 'Salariat' => [], 'Cession' => []];
            $i = 0; // indice du souhait dans le tableau
            $isOneOffer = false;
            foreach ($offersInProgress as $offer) {
                if (!$offer->getIsModel()) {
                    $functionResult = $this->matchingOneOffer(
                        $offer,
                        $availableWishes,
                        $result,
                        $manager,
                        $criterionRepository,
                        $criterionInterval1,
                        $criterionInterval2,
                        $criterionPonderation,
                        $criterionValeur,
                        $i // indice du souhait dans le tableau
                    );
                    $result = $functionResult['result'];
                    $i = $functionResult['i']; // on récupère l'indice du souhait
                }
            }
        }
        return $this->render(
            'Offer/Offer/matchingOffers.html.twig',
            [
                'resultMatching' => $result,
                'isOneOffer' => $isOneOffer,
                'oneSubstitutionType' => new SubstitutionType()
            ]
        );
    }

    /**
     * @param Offer $offer
     * @param array $availableWishes
     * @param array $result
     * @param ObjectManager $manager
     * @param CriterionRepository $criterionRepository
     * @param Criterion $criterionInterval1
     * @param Criterion $criterionInterval2
     * @param Criterion $criterionPonderation
     * @param Criterion $criterionValeur
     * @param int|null $i
     * @return array
     * @throws Exception
     * @NoTranslation()
     */
    public function matchingOneOffer(
        Offer $offer,
        array $availableWishes,
        array $result,
        ObjectManager $manager,
        CriterionRepository $criterionRepository,
        Criterion $criterionInterval1,
        Criterion $criterionInterval2,
        Criterion $criterionPonderation,
        Criterion $criterionValeur,
        int $i = null
    ): array
    {
        $offerSpecialty = $offer->getSpecialty();

        // on regarde la modularité
        $isStartDateModulable = false;
        $isEndDateModulable = false;
        if ($offer->getStartingDateModularity() > 0) {
            $isStartDateModulable = true;
        }
        if ($offer->getEndDateModularity() > 0 && $offer->getEndDateModularityType() !== null) {
            $isEndDateModulable = true;
        }

        $offerSubstitutionType = $offer->getSubstitutionType();
        if ($offerSubstitutionType === null) { // si une offre n'a pas de type
            throw new RuntimeException('$offer->getSubstitutionType() returned NULL');
        }

        // on détermine la catégorie de l'offre (Libéral / Salariat / Cession)
        $offerSubstitutionTypeCategory = SubstitutionType::TYPE_LIBERAL_REPLACEMENT;
        if (in_array(
            $offerSubstitutionType->getSubstitutionType(),
            SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SALARIED_POSITION],
            true
        )) {
            $offerSubstitutionTypeCategory = SubstitutionType::TYPE_SALARIED_POSITION;
        } else {
            if (in_array(
                $offerSubstitutionType->getSubstitutionType(),
                SubstitutionType::TYPES_POSSIBLE[SubstitutionType::TYPE_SELL_ASSOCIATION],
                true
            )) {
                $offerSubstitutionTypeCategory = SubstitutionType::TYPE_SELL_ASSOCIATION;
            }
        }

        // on construit le tableau des critère de l'offre
        $arrayOfferCriteria = [];
        $offerSubstitutionCriteria = $offer->getSubstitutionCriteria();
        foreach ($offerSubstitutionCriteria as $substitutionCriterion) {
            $currentCriterion = $substitutionCriterion->getCriterion();
            if ($currentCriterion === null) {
                continue;
            }
            $arrayOfferCriteria[$currentCriterion->getId()] = [
                'criterion' => $currentCriterion,
                'value' => $substitutionCriterion->getValue(),
                'isRelevant' => $substitutionCriterion->getIsRelevant()
            ];
        }

        if (isset($i)) { // si on est dans une boucle sur plusieurs offres
            $arrayData = [
                'offer' => $offer,
                'isStartDateModulable' => $isStartDateModulable,
                'isEndDateModulable' => $isEndDateModulable
            ];
            switch ($offerSubstitutionTypeCategory) {
                case SubstitutionType::TYPE_LIBERAL_REPLACEMENT:
                    $result['Liberal'][$i] = $arrayData;
                    break;
                case SubstitutionType::TYPE_SALARIED_POSITION:
                    $result['Salariat'][$i] = $arrayData;
                    break;
                case SubstitutionType::TYPE_SELL_ASSOCIATION:
                    $result['Cession'][$i] = $arrayData;
                    break;
            }
        } else {
            $result = [
                'offer' => $offer,
                'wishes' => [],
                'isStartDateModulable' => $isStartDateModulable,
                'isEndDateModulable' => $isEndDateModulable
            ];
        }

        $offerWishesMatched = $offer->getOfferWishMatchings(); // les offerWishMatching qui sont déja en base (i.e les souhaits qui ont déja matchés ensemble pour l'offre)
        /** @var Wish $wish */
        foreach ($availableWishes as $wish) {
            $offerPerson = $offer->getPerson();
            $wishPerson = $wish->getPerson();

            // on regarde si le souhait peut matcher avec l'offre (si ici la condition est vérifiée, ca veut dire que non)
            if ($wish->getSubstitutionType() !== $offerSubstitutionType || $offerPerson === $wishPerson || $wishPerson->getOffersSent()->contains(
                    $offer
                )) {
                continue;
            }

            $wishIsActiveForOffer = true;
            $lastOfferWishMatching = null;
            /*
             * on parcours les souhaits ayant déja matchés avec l'offre
             * pour vérifier si l'offre n'est pas retirées du matching pour le souhait (i.e isActive = false)
             */
            foreach ($offerWishesMatched as $offerWishMatching) {
                if ($offerWishMatching->getOffer() === $offer) {
                    $lastOfferWishMatching = $offerWishMatching;
                    if ($offerWishMatching->getIsActive() === false) {
                        $wishIsActiveForOffer = false;
                        break;
                    }
                }
            }
            if ($wishIsActiveForOffer === false) {
                continue;
            }

            //on vérifie que l'offre a bien un département et qu'il correspond avec le souhait
            $wishDepartments = $wish->getDepartments();
            if (count($wishDepartments) > 0) {
                $departmentFound = false;
                foreach ($wishDepartments as $wishDepartment) {
                    if ($offer->getDepartment()->getCode() == $wishDepartment->getCode()) {
                        $departmentFound = true;
                    }
                }
                if (!$departmentFound) {
                    continue;
                }
            }

            // si les dates ne correspondent pas
            if (false === $this->doesDatesMatch($offer, $wish)) {
                continue;
            }

            $nbCriteresImperatifs = 0;
            $nbCriteresSecondaires = 0;
            $nbCriteresAdequationImperatifs = 0;
            $nbCriteresAdequationSecondaires = 0;

            $wishDistances = $wish->getDistance();
            $capacities = $wish->getPerson()->getCapacities();
            $specialty = $wish->getPerson()->getSpecialty();
            if ($specialty === null && $capacities->isEmpty()) {
                continue;
            }

            // on récupère les infos utiles pour le souhaits : sa catégorie (Salariat, Libéral, Cession), et les critères du souhait
            $getArrayWishCriteria = $this->getArrayWishCriteria($wish);
            $wishSubstitutionTypeCategory = null;
            $arrayWishCriteria = [];
            if (!empty($getArrayWishCriteria)) {
                $wishSubstitutionTypeCategory = $getArrayWishCriteria['wishSubstitutionTypeCategory']; // sa catégorie (Salariat, Libéral, Cession)
                $arrayWishCriteria = $getArrayWishCriteria['arrayWishCriteria']; // les critères du souhaits
            }

            // si la spécialité de l'offre correspond avec une capacité du rempla
            $capacitiesContainsSpecialty = false;
            foreach ($capacities as $capacity) {
                if ($offerSpecialty === $capacity) {
                    $capacitiesContainsSpecialty = true;
                }
            }

            if ($wishSubstitutionTypeCategory !== $offerSubstitutionTypeCategory || !($offerSpecialty === $specialty
                    || $capacitiesContainsSpecialty)) {
                continue;
            }

            // on récupère la distance entre l'offre et le souhait
            $shortestDistance = DistanceHandler::getShortestDistance($offer, $wish);
            if ($shortestDistance === null) {
                continue;
            }

            // matching sur les critères
            $resultMatchingCriteria = $this->matchingCriteria(
                $criterionRepository,
                $criterionInterval1,
                $criterionInterval2,
                $criterionPonderation,
                $criterionValeur,
                $this->getOfferDates($offer),
                $offerSpecialty,
                $arrayWishCriteria,
                $arrayOfferCriteria,
                $nbCriteresImperatifs,
                $nbCriteresSecondaires,
                $nbCriteresAdequationImperatifs,
                $nbCriteresAdequationSecondaires
            );

            $nbCriteresImperatifs = $resultMatchingCriteria['nbCriteresImperatifs'];
            $nbCriteresAdequationImperatifs = $resultMatchingCriteria['nbCriteresAdequationImperatifs'];
            $nbCriteresSecondaires = $resultMatchingCriteria['nbCriteresSecondaires'];
            $nbCriteresAdequationSecondaires = $resultMatchingCriteria['nbCriteresAdequationSecondaires'];

            if ($lastOfferWishMatching === null) { // s'il n'y avait pas encore de lien entre l'offre et le souhait
                $offerWishMatching = new OfferWishMatching();
                $offerWishMatching->setOffer($offer);
                $offerWishMatching->setWish($wish);
                $offerWishMatching->setIsActive(true);
                $manager->persist($offerWishMatching);
            }

            // on récupère le taux de correspondance avec le souhait
            $percentage = $this->getPercentage(
                $nbCriteresImperatifs,
                $nbCriteresAdequationImperatifs,
                $nbCriteresSecondaires,
                $nbCriteresAdequationSecondaires
            );

            // on construit le tableau de résultat qui sera retourné
            $arrayDataWish = [
                'wish' => $wish,
                'percentage' => $percentage * 100,
                'distance' => $shortestDistance
            ];
            if (isset($i)) {
                switch ($offerSubstitutionTypeCategory) {
                    case SubstitutionType::TYPE_LIBERAL_REPLACEMENT:
                        $result['Liberal'][$i]['wishes'][] = $arrayDataWish;
                        break;
                    case SubstitutionType::TYPE_SALARIED_POSITION:
                        $result['Salariat'][$i]['wishes'][] = $arrayDataWish;
                        break;
                    case SubstitutionType::TYPE_SELL_ASSOCIATION:
                        $result['Cession'][$i]['wishes'][] = $arrayDataWish;
                        break;
                }
            } else {
                $result['wishes'][] = $arrayDataWish;
            }
        }
        $manager->flush();
        if (isset($i)) {
            $i++;
        }
        return [
            'result' => $result,
            'i' => $i
        ];
    }

    /**
     * @Route("/remove-from-matching", name="remove_offers_from_matching", methods={"POST"})
     * @Translation("Retirer des offres du matching")
     * @param Request $request
     * @param OfferRepository $offerRepository
     * @param WishRepository $wishRepository
     * @param OfferWishMatchingRepository $offerWishMatchingRepository
     * @return JsonResponse
     */
    public function ajaxRemoveOfferFromMatching(
        Request $request,
        OfferRepository $offerRepository,
        WishRepository $wishRepository,
        OfferWishMatchingRepository $offerWishMatchingRepository
    ): JsonResponse
    {
        $offerIds = $request->request->get('offerIds');
        $wishId = $request->request->get('wishId');
        $wish = $wishRepository->findOneById($wishId);
        if (!isset($wish)) {
            return $this->errorJsonResponse("Le souhait associé n'a pas été trouvé.");
        }
        $manager = $this->getDoctrine()->getManager();
        foreach ($offerIds as $id) {
            $offer = $offerRepository->findOneById($id);
            if (isset($offer)) {
                $offerWishMatching = $offerWishMatchingRepository->findOneBy(
                    [
                        'offer' => $offer,
                        'wish' => $wish,
                        'isActive' => true
                    ]
                );
                if (isset($offerWishMatching)) {
                    $offerWishMatching->setIsActive(false);
                    $manager->persist($offerWishMatching);
                }
            }
        }
        $manager->flush();
        return $this->successJsonResponse();
    }

    /**
     * @Route("/check/exists", name="offer_check_existence", methods={"GET"})
     * @NoTranslation()
     * @param Request $request
     * @param OfferRepository $offerRepository
     * @return JsonResponse
     */
    public function checkOfferExistence(Request $request, OfferRepository $offerRepository): JsonResponse
    {
        $offerId = $request->get('id');
        if (!isset($offerId)) {
            return new JsonResponse(['result' => false]);
        }
        $offer = $offerRepository->findOneBy(['id' => $offerId]);
        return new JsonResponse(['result' => ($offer !== null && $offer instanceof Offer)]);
    }
}
