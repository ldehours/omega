<?php

namespace App\Controller\Offer;

use App\Annotation\Translation;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Form\Offer\SearchOfferType;
use App\Repository\Offer\CriterionRepository;
use App\Repository\Offer\OfferRepository;
use App\Repository\Offer\SubstitutionCriterionRepository;
use App\Utils\DateTime;
use App\Utils\PHPHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/offer/search")
 */
class SearchOfferController extends AbstractController
{
    /**
     * @Route(path="/",name="search_offer_index",methods={"GET","POST"})
     * @Translation("Recherche multi-critères d'offres")
     * @param Request $request
     * @param OfferRepository $offerRepository
     * @return RedirectResponse|Response
     */
    public function index(
        Request $request,
        OfferRepository $offerRepository
    ):Response{
        $form = $this->createForm(SearchOfferType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //on enlève les clés où il n'y a pas de valeur
            $optionsRequested = array_filter($request->get("search_offer"));

            /*
             * Attention, le $requestNumberElements permet de vérifier si au moins un champs a été rempli par l'utilisateur,
             * Comme de base le formulaire contient le jeton CSRF, on va faire des vérifs plus bas pour
             * savoir si la taille du tableau de parametres est supérieure ou non à 1
             */
            $requestNumberElements = count($optionsRequested);

            if ($form->isEmpty() || $requestNumberElements <= 1 ) {
                $this->addFlash('warning','Veuillez remplir au moins un des critères.');
                return $this->redirectToRoute('search_offer_index');
            }

            $existsStartDate = array_key_exists('startDate', $optionsRequested);
            $existsEndDate = array_key_exists('endDate', $optionsRequested);
            if($existsEndDate && $existsStartDate){
                $startDate = DateTime::createFromString($optionsRequested['startDate']);
                $endDate = DateTime::createFromString($optionsRequested['endDate']);
                if($startDate->getTimestamp() > $endDate->getTimestamp()){
                    $this->addFlash('warning',"La date de début doit être antérieure à la date de fin.");
                    return $this->redirectToRoute('search_offer_index');
                }
            }

            if(array_key_exists('id',$optionsRequested)){
                $offer = $offerRepository->find($optionsRequested['id']);
                if(null !== $offer){
                    return $this->redirectToRoute('offer_offer_edit',[
                        'id' => $offer->getId()
                    ]);
                }
            }else{
                $offers = $offerRepository->findByMultiCritera($optionsRequested);
                $resultOffers = [];
                foreach ($offers as $offer){
                    $person = $offer->getPerson();
                    if(array_key_exists('personName',$optionsRequested)){
                        if($person instanceof PersonLegal){
                            if($person->getCorporateName() !== $optionsRequested['personName']){
                                continue;
                            }
                        }elseif($person instanceof PersonNatural){
                            if($person->getLastName() !== PHPHelper::strToUpperNoAccent($optionsRequested['personName'])){
                                continue;
                            }
                        }else{
                            continue;
                        }
                    }
                    $resultOffers[] = $offer;
                }
            }
            return $this->render('Offer/Offer/SearchOffer/searchOffer.html.twig',[
                'form' => $form->createView(),
                'offers' => $resultOffers ?? [],
                'isPageSubmitted' => true,
            ]);
        }
        return $this->render('Offer/Offer/SearchOffer/searchOffer.html.twig',[
            'form' => $form->createView()
        ]);
    }
}