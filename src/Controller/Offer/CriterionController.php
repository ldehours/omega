<?php

namespace App\Controller\Offer;

use App\Annotation\Translation;

use App\Controller\BaseAbstractController;
use App\Entity\Offer\Criterion;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/offer/criterion")
 */
class CriterionController extends BaseAbstractController
{
    /**
     * @Route("/{id}", name="offer_criterion_delete", methods={"DELETE"})
     * @Translation("Suppression d'un critère d'offres")
     * @param Request $request
     * @param Criterion $criterion
     * @return Response
     */
    public function delete(Request $request, Criterion $criterion): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($criterion);
        $entityManager->flush();

        return $this->successJsonResponse();
    }

    /**
     * @Route("/has/possible/value", name="offer_criterion_has_possible_value", methods={"GET"})
     * @Translation("Récupère les critères qui sont en choix multiples")
     */
    public function getCriterionTypeHasPossibleValue(): Response
    {
        return new JsonResponse(Criterion::HAS_POSSIBLE_VALUE);
    }
}
