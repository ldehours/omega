<?php

namespace App\Controller\Offer;

use App\Controller\BaseAbstractController;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\TimeRange;
use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\SubstitutionCriterion;
use App\Entity\Offer\SubstitutionType;
use App\Entity\Offer\Wish;
use App\Form\Offer\WishType;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\MedicalSpecialtyRepository;
use App\Repository\Core\PersonRepository;
use App\Repository\Core\RegionRepository;
use App\Repository\Core\TimeRangeRepository;
use App\Repository\Offer\CanvasRepository;
use App\Repository\Offer\CriterionPossibleValueRepository;
use App\Repository\Offer\CriterionRepository;
use App\Repository\Offer\SubstitutionCriterionRepository;
use App\Repository\Offer\WishRepository;
use App\Service\SubstitutionDuplicaterService;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Annotation\Translation;
use App\Annotation\NoTranslation;

/**
 * @Route("/offer/wish")
 */
class WishController extends BaseAbstractController
{
    /**
     * @Route("/{personId}/", name="offer_wish_index", methods={"GET"})
     * @Translation("Visualisation de la liste des souhaits pour un remplaçant/candidat")
     * @param WishRepository $wishRepository
     * @param PersonRepository $personRepository
     * @param int $personId
     * @return Response
     */
    public function index(
        WishRepository $wishRepository,
        PersonRepository $personRepository,
        int $personId
    ): Response
    {
        $person = $personRepository->findOneById($personId);
        return $this->render(
            'Offer/Wish/viewAllWishes.html.twig',
            [
                'wishes' => $wishRepository->findBy(
                    [
                        'person' => $person
                    ]
                ),
                'person' => $person,
                'substitutionType' => new SubstitutionType()
            ]
        );
    }

    /**
     * @Route("/{personId}/new", name="offer_wish_new", methods={"GET","POST"})
     * @Route("/{personId}/new/{timeRangeId}", name="offer_wish_new_with_timerange", methods={"GET","POST"})
     * @Translation("Création d'un souhait pour un remplaçant/candidat")
     * @param Request $request
     * @param PersonRepository $personRepository
     * @param int $personId
     * @param CriterionRepository $criterionRepository
     * @param CanvasRepository $canvasRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @param SubstitutionCriterionRepository $substitutionCriterionRepository
     * @param TimeRangeRepository $timeRangeRepository
     * @param DepartmentRepository $departmentRepository
     * @param int|null $timeRangeId
     * @return Response
     * @throws Exception
     */
    public function new(
        Request $request,
        PersonRepository $personRepository,
        int $personId,
        CriterionRepository $criterionRepository,
        CanvasRepository $canvasRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository,
        SubstitutionCriterionRepository $substitutionCriterionRepository,
        TimeRangeRepository $timeRangeRepository,
        DepartmentRepository $departmentRepository,
        ?int $timeRangeId
    ): Response
    {
        $person = $personRepository->findOneById($personId);
        $wish = new Wish();
        $timeRange = new TimeRange();
        if (isset($timeRangeId)) {
            $timeRange = $timeRangeRepository->findOneById($timeRangeId);
            if (isset($timeRange)) {
                $wish->setTimeRange($timeRange);
                $wish->setStartingDate($timeRange->getStartDate());
            }
        }
        $form = $this->createForm(WishType::class, $wish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $departments = $request->request->get('criterion_wish')[18] ?? [];

            if ($departments != []) {
                if (in_array('FE', $departments)) {
                    foreach ($departmentRepository->findAll() as $department) {
                        $wish->addDepartment($department);
                    }
                } else {
                    foreach ($departments as $departmentCode) {
                        $wish->addDepartment($departmentRepository->findOneBy(['code' => $departmentCode]));
                    }
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $relevantCriteria = $request->request->get('criteria');
            if ($wish->getTimeRange() != null) {
                $timeRange = $wish->getTimeRange();
            } else {
                $timeRange->setCreator($this->getUser());
                $timeRange->setAgenda($person->getAgenda());
                $timeRange->setEndDate((new DateTime($request->request->get('wish')['startingDate']))->add(new DateInterval('P1Y')));
                $timeRange->setNature(TimeRange::NATURE_SEARCH);
            }
            if (array_key_exists('endDate', $request->request->get('wish')) && !empty($request->request->get('wish')['endDate'])) {
                $timeRange->setEndDate(new DateTime($request->request->get('wish')['endDate']));
            } else {
                $timeRange->setEndDate((new DateTime($request->request->get('wish')['startingDate']))->add(new DateInterval('P1Y')));
            }
            $timeRange->setStartDate($wish->getStartingDate());

            $wish->setCreationDate(new DateTimeImmutable());
            $wish->setPerson($person);
            $wish->setCreator($this->getUser());
            $canvas = $canvasRepository->findOneById($request->request->get('wish')['canvas']);
            $wish->setCanvas($canvas);
            $wish->setSubstitutionType($canvas->getSubstitutionType());
            $newDateTime = new DateTime();
            $newDateTime->add(new DateInterval('PT2H'));
            $wish->setLastUpdateDate($newDateTime);
            $wish->setLastFullUpdateAt($newDateTime);

            $distances = $request->request->get('distance');
            $result_distances = [];
            for ($i = 0, $iMax = count($distances['km']); $i < $iMax; $i++) {
                if ($distances['km'][$i] != '' && $distances['cp'][$i] != '') {
                    $result_distances[] = [
                        'km' => $distances['km'][$i],
                        'cp' => $distances['cp'][$i]
                    ];
                }
            }

            $wish->setDistance($result_distances);
            foreach ($request->request->get('criterion_wish') as $idCriterion => $valueCriterion) {
                $criterion = $criterionRepository->findOneBy(['id' => $idCriterion]);

                $substitutionCriterion = new SubstitutionCriterion();
                $substitutionCriterion->setCriterion($criterion);
                $substitutionCriterion->setSubstitution($wish);

                $possibleValue = $criterionPossibleValueRepository->findOneBy(
                    [
                        'criterion' => $criterion,
                        'substitutionType' => $canvas->getSubstitutionType(),
                        'substitutionNature' => $canvas->getCanvasType()
                    ]
                );
                if ($possibleValue->getCriterionType() == Criterion::TYPE_CHOICE_WEEK) {
                    $choiceWeekValue = 0;
                    foreach ($valueCriterion as $value) {
                        $choiceWeekValue += pow(2, $value);
                    }
                    $substitutionCriterion->setValue($choiceWeekValue);
                } elseif ($possibleValue->getCriterionType() === Criterion::TYPE_LOCATION || $possibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                    $substitutionCriterion->setValue(array_unique($valueCriterion));
                } else {
                    $substitutionCriterion->setValue($valueCriterion);
                }

                if ($relevantCriteria !== null) {
                    if (array_key_exists(SubstitutionCriterion::IMPORTANT, $relevantCriteria) &&
                        in_array($idCriterion, $relevantCriteria[SubstitutionCriterion::IMPORTANT])) {
                        $substitutionCriterion->setIsRelevant(SubstitutionCriterion::IMPORTANT);
                    } elseif (array_key_exists(SubstitutionCriterion::SECONDARY, $relevantCriteria) &&
                        in_array($idCriterion, $relevantCriteria[SubstitutionCriterion::SECONDARY])) {
                        $substitutionCriterion->setIsRelevant(SubstitutionCriterion::SECONDARY);
                    } else {
                        $substitutionCriterion->setIsRelevant(SubstitutionCriterion::AVAILABLE);
                    }
                }
                $entityManager->persist($substitutionCriterion);
                if ($criterion != null) {
                    $wish->addSubstitutionCriterion($substitutionCriterion);
                }
            }
            $entityManager->flush();
            if ($relevantCriteria != null) {
                foreach ($relevantCriteria as $relevantcy => $criteria) {
                    foreach ($criteria as $criterionId) {
                        $substitutionCriterion = $substitutionCriterionRepository->findOneBy(
                            [
                                'criterion' => $criterionRepository->findOneById($criterionId),
                                'substitution' => $wish
                            ]
                        );
                        if ($substitutionCriterion != null) {
                            $substitutionCriterion->setIsRelevant($relevantcy);
                            $entityManager->persist($substitutionCriterion);
                        }
                    }
                }
            }
            $wish->setTimeRange($timeRange);
            $entityManager->persist($wish);
            $entityManager->flush();
            $this->addFlash('success', 'Souhait créé.');
            return $this->redirectToRoute('offer_wish_index', ['personId' => $personId]);
        }
        return $this->render(
            'Offer/Wish/newWish.html.twig',
            [
                'wish' => $wish,
                'form' => $form->createView(),
                'person' => $person,
                'criterion' => new Criterion(),
                'end_date' => $timeRange->getEndDate()
            ]
        );
    }

    /**
     * @Route("/{id}/show", name="offer_wish_show", methods={"GET"})
     * @Translation("Visualisation d'un souhait pour un remplaçant/candidat")
     * @param Wish $wish
     * @return Response
     */
    public function show(Wish $wish): Response
    {
        $wishSubstitutionType = SubstitutionType::LABEL[$wish->getCanvas()->getSubstitutionType()->getSubstitutionType()];
        $criteriaRelevantness = [
            0 => [],
            1 => [],
            2 => []
        ];
        foreach ($wish->getSubstitutionCriteria() as $substitutionCriterion) {
            $criteriaRelevantness[$substitutionCriterion->getIsRelevant()][] = $substitutionCriterion->getCriterion()->getLabel();
        }

        return $this->render(
            'Offer/Wish/viewOneWish.html.twig',
            [
                'wish' => $wish,
                'wishSubstitutionType' => $wishSubstitutionType,
                'criteriaRelevantness' => $criteriaRelevantness,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="offer_wish_edit", methods={"GET","POST"})
     * @Translation("Modification d'un souhait pour un remplaçant/candidat")
     * @param Request $request
     * @param Wish $wish
     * @param CanvasRepository $canvasRepository
     * @param MedicalSpecialtyRepository $medicalSpecialtyRepository
     * @param CriterionRepository $criterionRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @param SubstitutionCriterionRepository $substitutionCriterionRepository
     * @param DepartmentRepository $departmentRepository
     * @param RegionRepository $regionRepository ,
     * @param ManagerRegistry $registry
     * @return Response
     * @throws Exception
     */
    public function edit(
        Request $request,
        Wish $wish,
        CanvasRepository $canvasRepository,
        MedicalSpecialtyRepository $medicalSpecialtyRepository,
        CriterionRepository $criterionRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository,
        SubstitutionCriterionRepository $substitutionCriterionRepository,
        DepartmentRepository $departmentRepository,
        RegionRepository $regionRepository,
        ManagerRegistry $registry
    ): Response
    {
        $form = $this->createForm(WishType::class, $wish);
        $form->handleRequest($request);

        $canvas = $wish->getCanvas();
        $person = $wish->getPerson();

        if ($form->isSubmitted() && $form->isValid()) {

            $wishCriteria = $request->request->get('criterion_wish');
            $departments = (array_key_exists('18', $wishCriteria)&& $wishCriteria[18] != []) ? $wishCriteria[18] : [];

            if ($departments != []) {
                if (in_array('FE', $departments)) {
                    foreach ($departmentRepository->findAll() as $department) {
                        $wish->addDepartment($department);
                    }
                } else {
                    $wish->getDepartments()->clear();
                    foreach ($departments as $departmentCode) {
                        $wish->addDepartment($departmentRepository->findOneBy(['code' => $departmentCode]));
                    }
                }
            }
            $relevantCriteria = $request->request->get('criteria');
            $criterionWish = $request->get('criterion_wish');
            $fullUpdate = $request->request->get('wish_full_update');
            $timeRange = $wish->getTimeRange();
            if ($timeRange === null) {
                $timeRange = new TimeRange();
                $timeRange->setNature(TimeRange::NATURE_SEARCH);
                $timeRange->setAgenda($wish->getPerson()->getAgenda());
                $timeRange->setCreator($this->getUser());
            }

            $requestWish = $request->request->get('wish');
            $timeRange->setStartDate(new DateTime($requestWish['startingDate']));
            if (array_key_exists('endDate', $requestWish) && !empty($requestWish['endDate'])) {
                $timeRange->setEndDate(new DateTime($requestWish['endDate']));
            } else {
                $timeRange->setEndDate((new DateTime($requestWish['startingDate']))->add(new DateInterval('P1Y')));
            }

            $manager = $this->getDoctrine()->getManager();

            $canvasId = $requestWish['canvas'];
            if ($canvasId != $canvas->getId()) {
                $newCanvas = $canvasRepository->findOneById($canvasId);
                $wish->setCanvas($newCanvas);
                $wish->setSubstitutionType($canvas->getSubstitutionType());
            }

            $distances = $request->request->get('distance');
            $result_distances = [];
            for ($i = 0, $iMax = count($distances['km']); $i < $iMax; $i++) {
                if ($distances['km'][$i] !== '' && $distances['cp'][$i] !== '') {
                    $result_distances[] = [
                        'km' => $distances['km'][$i],
                        'cp' => $distances['cp'][$i]
                    ];
                }
            }
            $wish->setDistance($result_distances);

            $wish->getSubstitutionCriteria()->clear();
            if (isset($criterionWish)) {
                foreach ($criterionWish as $idCriterion => $valueCriterion) {
                    $criterion = $criterionRepository->findOneById($idCriterion);
                    if ($criterion === null) {
                        continue;
                    }
                    $substitutionCriterion = $substitutionCriterionRepository->findOneBy(
                        [
                            'substitution' => $wish->getId(),
                            'criterion' => $idCriterion
                        ]
                    );

                    if ($substitutionCriterion === null) {
                        $substitutionCriterion = new SubstitutionCriterion();
                    }

                    $criterionPossibleValue = $criterionPossibleValueRepository->findOneBy(
                        [
                            'criterion' => $criterion,
                            'substitutionType' => $canvas->getSubstitutionType(),
                            'substitutionNature' => Canvas::CANVAS_WISH
                        ]
                    );

                    //if it is a type of criterion 'choice week' then we formalize specially
                    if ($criterionPossibleValue->getCriterionType() === Criterion::TYPE_CHOICE_WEEK) {
                        $choiceWeekValue = 0;
                        foreach ($valueCriterion as $value) {
                            $choiceWeekValue += 2 ** $value;
                        }
                        $substitutionCriterion->setValue($choiceWeekValue);
                    } elseif ($criterionPossibleValue->getCriterionType() === Criterion::TYPE_LOCATION || $criterionPossibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                        $substitutionCriterion->setValue(array_unique($valueCriterion));
                    } else {
                        $substitutionCriterion->setValue($valueCriterion);
                    }
                    $substitutionCriterion->setCriterion($criterion);
                    $substitutionCriterion->setSubstitution($wish);
                    $wish->addSubstitutionCriterion($substitutionCriterion);
                    $manager->persist($substitutionCriterion);
                }

                if ($relevantCriteria !== null) {
                    foreach ($relevantCriteria as $relevantcy => $criteria) {
                        foreach ($criteria as $criterionId) {
                            $substitutionCriterion = $substitutionCriterionRepository->findOneBy(
                                [
                                    'criterion' => $criterionRepository->findOneById($criterionId),
                                    'substitution' => $wish
                                ]
                            );
                            if ($substitutionCriterion !== null) {
                                $substitutionCriterion->setIsRelevant($relevantcy);
                                $manager->persist($substitutionCriterion);
                            }
                        }
                    }
                }
            }
            $newDateTime = new DateTime();
            $newDateTime->add(new DateInterval('PT2H'));
            if($fullUpdate !== null){
                $wish->setLastFullUpdateAt($newDateTime);
            }
            $wish->setLastUpdateDate($newDateTime);
            $wish->setTimeRange($timeRange);

            $manager->persist($timeRange);
            $manager->persist($wish);
            $manager->flush();

            $this->addFlash('success', 'Souhait modifié avec succès');
            return $this->redirectToRoute('offer_wish_edit', ['id' => $wish->getId()]);
        }

        $medicalSpecialties = $medicalSpecialtyRepository->findAll();
        $medicalSpecialtiesInArray = [];
        foreach ($medicalSpecialties as $medicalSpecialty) {
            $medicalSpecialtiesInArray[$medicalSpecialty->getId()] = $medicalSpecialty->getOfficialLabel();
        }

        $criterionArray = [];
        $tabForOldFileInput = [];
        $criteriaRelevantness = [
            0 => [],
            1 => [],
            2 => []
        ];
        if ($canvas !== null) {
            $formDisposition = $canvas->getFormDisposition();
            $idsCriteria = [];
            foreach ($formDisposition as $row) {
                foreach ($row as $value) {
                    $idsCriteria[] = $value[0];
                }
            }
            $listCriterion = $criterionRepository->findById($idsCriteria);
            foreach ($listCriterion as $criterion) {
                foreach ($criterion->getCriterionPossibleValues() as $criterionPossibleValue) {

                    if ($criterionPossibleValue->getSubstitutionType() === $canvas->getSubstitutionType() && $criterionPossibleValue->getSubstitutionNature() === $canvas->getCanvasType()) {
                        $possibleValueString = $criterion->getId() == 18 ? $regionRepository->findAll() : json_decode($criterionPossibleValue->getPossibleValue(), true);
                        $criterionArray[$criterion->getId()] = [
                            'label' => $criterion->getLabel(),
                            'type' => $criterionPossibleValue->getCriterionType(),
                            'value' => $possibleValueString,
                            'dependency' => []
                        ];
                        $dependency = $criterion->getDependency();
                        if (!empty($dependency)) {
                            $criterionArray[$criterion->getId()]['dependency'] = [
                                'id' => array_keys($dependency)[0],
                                'value' => $dependency[array_keys($dependency)[0]]
                            ];
                        }
                        if ($criterion->getLabel() == 'Département') {
                            $criterionArray[$criterion->getId()]['criterionValue'] = $regionRepository->asortFindAll();
                        }
                        if ($criterionPossibleValue->getCriterionType() === Criterion::TYPE_ENTITY) {
                            $entityFullName = explode('::', $possibleValueString)[0];
                            $fieldName = explode('::', $possibleValueString)[1];

                            $targettedEntity = str_replace(
                                'Entity',
                                'Repository',
                                $entityFullName . 'Repository'
                            );
                            $targettedEntityInstance = new $targettedEntity($registry);

                            $targetLabels = [];

                            foreach ($targettedEntityInstance->findAll() as $element) {
                                $targetLabels[] = $element->{'get' . ucfirst($fieldName)}(); //sorry
                            }
                            $criterionArray[$criterion->getId()]['value'] = $targetLabels;
                        }
                    }
                }
                $tabToCheck = ['id' => $criterion->getId(), 'label' => $criterion->getLabel()];
                if ($criterion->getRelevantable()
                    && !in_array($tabToCheck, $criteriaRelevantness[0], true)
                    && !in_array($tabToCheck, $criteriaRelevantness[1], true)
                    && !in_array($tabToCheck, $criteriaRelevantness[2], true)) {
                    $criteriaRelevantness[0][] = $tabToCheck;
                }
            }

            foreach ($wish->getSubstitutionCriteria() as $substitutionCriterion) {
                $criterion = $criterionRepository->findOneById($substitutionCriterion->getCriterion()->getId());

                $criterionPossibleValue = $criterionPossibleValueRepository->findOneBy(
                    [
                        'criterion' => $criterion,
                        'substitutionType' => $canvas->getSubstitutionType(),
                        'substitutionNature' => Canvas::CANVAS_WISH
                    ]
                );

                $substitutionCriterionTab = [];
                switch ($criterionPossibleValue->getCriterionType()) {
                    case Criterion::TYPE_CHOICE_WEEK:
                        for ($i = 0; $i < 14; $i++) {
                            if (($substitutionCriterion->getValue() & (2 ** $i)) !== 0) {
                                $substitutionCriterionTab[$i] = 1;
                            } else {
                                $substitutionCriterionTab[$i] = 0;
                            }
                        }
                        break;
                    case Criterion::TYPE_LOCATION:
                        $substitutionCriterionTab = $regionRepository->asortFindAll();
                        if (count($wish->getDepartments()) == count($departmentRepository->findAll())) {
                            $criterionArray[$substitutionCriterion->getCriterion()->getId()]['selected'][] = 'FE';
                        } else {
                            foreach ($wish->getDepartments() as $department) {
                                $criterionArray[$substitutionCriterion->getCriterion()->getId()]['selected'][] = $department;
                            }
                        }
                        break;
                    case Criterion::TYPE_FILE:
                        $tabForOldFileInput[$substitutionCriterion->getCriterion()->getId()] = $substitutionCriterion->getValue();
                        break;
                    default:
                        $substitutionCriterionTab = $substitutionCriterion->getValue();
                        break;
                }
                $criterionArray[$substitutionCriterion->getCriterion()->getId()]['criterionValue'] = $substitutionCriterionTab;

                $tabToCheck = [
                    'id' => $criterion->getId(),
                    'label' => $criterion->getLabel()
                ];
                for ($i = 0; $i < 3; $i++) {
                    $search = array_search($tabToCheck, $criteriaRelevantness[$i], true);
                    if ($search !== false && $search !== null) {
                        unset($criteriaRelevantness[$i][$search]);
                    }
                }
                if ($substitutionCriterion->getIsRelevant() !== 0 || $criterion->getRelevantable()) {
                    $criteriaRelevantness[$substitutionCriterion->getIsRelevant()][] = $tabToCheck;
                }
            }
        }
        foreach ($criteriaRelevantness as $key => $arrayValues) {
            usort(
                $arrayValues,
                static function ($subArray1, $subArray2) {
                    return strcasecmp($subArray1['label'], $subArray2['label']);
                }
            );
            $criteriaRelevantness[$key] = $arrayValues;
        }
        $person = $wish->getPerson();
        $wishSubstitutionType = SubstitutionType::LABEL[$wish->getCanvas()->getSubstitutionType()->getSubstitutionType()];

        return $this->render(
            'Offer/Wish/editWish.html.twig',
            [
                'wish' => $wish,
                'person' => $person,
                'canvas' => $canvas,
                'criteriaRelevantness' => $criteriaRelevantness,
                'criterionArray' => $criterionArray,
                'criterion' => new Criterion(),
                'form' => $form->createView(),
                'wishSubstitutionType' => $wishSubstitutionType
            ]
        );
    }

    /**
     * @Route("/{id}/delete", name="offer_wish_delete", methods={"DELETE"})
     * @Translation("Suppression d'un souhait pour un remplaçant/candidat")
     * @param Request $request
     * @param Wish $wish
     * @return JsonResponse|RedirectResponse
     */
    public function delete(Request $request, Wish $wish)
    {
        $personId = $wish->getPerson()->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $timeRange = $wish->getTimeRange();
        if ($timeRange !== null) {
            $entityManager->remove($timeRange);
        }
        $entityManager->remove($wish);
        $entityManager->flush();

        if ($request->request->get('isAjaxCall') == "false") {
            return $this->redirectToRoute('offer_wish_index', ['personId' => $personId]);
        }
        return $this->successJsonResponse();
    }

    /**
     * @Route("/{id}/duplicate", name="offer_wish_duplicate", methods={"POST","GET"})
     * @Translation("Duplication d'un souhait pour un remplaçant/candidat")
     * @param Wish $wish
     * @param SubstitutionDuplicaterService $duplicateService
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     * @throws Exception
     */
    public function duplicate(Wish $wish, SubstitutionDuplicaterService $duplicateService, Request $request)
    {
        $newWish = $duplicateService->duplicateWish($wish, $this->getUser(), $wish->getIsModel());
        $this->addFlash('success', 'Duplication de souhait effectuée');
        if ($request->isMethod('get')) {
            return $this->redirectToRoute("offer_wish_edit", ['id' => $newWish->getId()]);
        }
        return $this->successJsonResponse();
    }

    /**
     * @Route("/create/model/{id}", name="wish_create_from_model", methods={"GET","POST"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonNatural $person
     * @param WishRepository $wishRepository
     * @param SubstitutionDuplicaterService $duplicateService
     * @return JsonResponse|Response
     * @throws Exception
     */
    public function createWishFromModel(
        Request $request,
        PersonNatural $person,
        WishRepository $wishRepository,
        SubstitutionDuplicaterService $duplicateService
    )
    {
        if ($request->isMethod('post')) {
            $wishModelId = $request->request->get('model');

            /** @var Wish $wishModel */
            $wishModel = $wishRepository->findOneBy(
                [
                    'id' => $wishModelId
                ]
            );
            if (!$wishModel) {
                $this->addFlash('error', "Le modèle n'a pas été trouvé");
                return $this->redirectToRoute('offer_wish_new');
            }
            $newWish = $duplicateService->duplicateWish($wishModel, $this->getUser(), true);

            $this->addFlash('success', 'Souhait créé');
            return $this->redirectToRoute('offer_wish_edit', ['id' => $newWish->getId()]);
        }
        /** @var Wish[] $wishModels */
        $wishModels = $wishRepository->findBy(
            [
                'person' => $person,
                'isModel' => true,
            ]
        );
        if (empty($wishModels)) {
            return $this->errorJsonResponse("Cette personne ne possède pas de modèle de souhait.");
        }
        $wishes = [];
        foreach ($wishModels as $wish) {
            $wishes[$wish->getId()] = [
                'id' => $wish->getId(),
                'substitutionType' => "[" . $wish->getId() . "] " . SubstitutionType::LABEL[$wish->getSubstitutionType()->getSubstitutionType()]
            ];
        }
        return new Response(
            $this->renderView(
                'Offer/Model/createFromModel.html.twig',
                [
                    'url' => $this->generateUrl(
                        'wish_create_from_model',
                        [
                            'id' => $person->getId()
                        ]
                    ),
                    'elements' => $wishes
                ]
            )
        );
    }

    /**
     * @Route("/ajax/{id}", name="offer_wish_ajax_wish_fields", methods={"POST"})
     * @NoTranslation()
     * @param Canvas $canvas
     * @param CriterionRepository $criterionRepository
     * @param CriterionPossibleValueRepository $criterionPossibleValueRepository
     * @param RegionRepository $regionRepository ,
     * @param ManagerRegistry $registry
     * @return JsonResponse
     */
    public function ajaxWishFields(
        Canvas $canvas,
        CriterionRepository $criterionRepository,
        CriterionPossibleValueRepository $criterionPossibleValueRepository,
        RegionRepository $regionRepository,
        ManagerRegistry $registry
    ): JsonResponse
    {
        $response = [];
        $substitutionType = $canvas->getSubstitutionType();
        foreach ($canvas->getFormDisposition() as $rowKey => $row) {
            $response[$rowKey] = [];
            foreach ($row as $element) {
                $criterion = $criterionRepository->findOneById($element[0]);
                if ($criterion === null) {
                    continue;
                }
                $possibleValue = $criterionPossibleValueRepository->findOneBy(
                    [
                        'criterion' => $criterion,
                        'substitutionType' => $substitutionType,
                        'substitutionNature' => Canvas::CANVAS_WISH
                    ]
                );
                if ($possibleValue === null) {
                    continue;
                }

                $criterionPossibleValue = $possibleValue->getPossibleValue();

                if ($possibleValue->getCriterionType() === Criterion::TYPE_LOCATION || $possibleValue->getCriterionType() === Criterion::TYPE_JSON) {
                    $jsonData = [];
                    foreach (json_decode($possibleValue->getPossibleValue()) as $source) {
                        $sourceKey = array_reverse(explode('/', explode('.', $source)[1]))[0];
                        $jsonData[$sourceKey] = json_decode(file_get_contents($source));
                    }
                    $criterionPossibleValue = $jsonData;
                }

                if ($possibleValue->getCriterionType() == Criterion::TYPE_ENTITY) {
                    $entityData = [];
                    $possibleValueString = json_decode($possibleValue->getPossibleValue());
                    $entityFullName = explode('::', $possibleValueString)[0];
                    $fieldName = explode('::', $possibleValueString)[1];

                    $targettedEntity = str_replace('Entity', 'Repository', $entityFullName . 'Repository');
                    $targettedEntityInstance = new $targettedEntity($registry);

                    foreach ($targettedEntityInstance->findAll() as $entityInvokation) {
                        $entityData[] = $entityInvokation->{'get' . ucfirst($fieldName)}(); //sorry
                    }
                    $criterionPossibleValue = $entityData;
                }

                if ($possibleValue->getCriterionType() === Criterion::TYPE_LOCATION) {
                    $regions = [];
                    foreach ($regionRepository->asortFindAll() as $region) {
                        $departments = [];
                        foreach ($region->getDepartments() as $department) {
                            $departments [] = [
                                'id' => $department->getId(),
                                'nom' => $department->getName(),
                                'code' => $department->getCode()
                            ];
                        }
                        $regions[] = [
                            'id' => $region->getId(),
                            'nom' => $region->getName(),
                            'code' => $region->getCode(),
                            'departments' => $departments
                        ];
                    }
                    $criterionPossibleValue = $regions;
                }

                $dependency = $criterion->getDependency();
                $dependencyTab = [];
                if (!empty($dependency)) {
                    $dependencyTab = [
                        'id' => array_keys($dependency)[0],
                        'value' => $dependency[array_keys($dependency)[0]]
                    ];
                }

                $response[$rowKey][] = [
                    'id' => $element[0],
                    'size' => $element[1],
                    'input_type' => $possibleValue->getCriterionType(),
                    'possible_value' => $criterionPossibleValue,
                    'label' => $criterion->getLabel(),
                    'dependency' => $dependencyTab,
                    'relevantable' => $criterion->getRelevantable()
                ];
            }
        }
        return new JsonResponse($response);
    }
}
