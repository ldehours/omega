<?php

namespace App\Controller\CFML;

use App\Entity\Core\PersonNatural;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Annotation\Translation;

/**
 * @Route("/cfml")
 */
class DsiDeclarationController extends AbstractController
{
    /**
     * @Route("/{id}/dsiDeclaration", name="cfml_dsi_declaration", methods={"GET"})
     * @Translation("Visualisation des déclarations DSI")
     * @param PersonNatural $personNatural
     */
    public function index(PersonNatural $personNatural)
    {

        return $this->render('Cfml/dsiDeclaration.html.twig', [
            'personNatural' => $personNatural,
        ]);
    }
}
