<?php

namespace App\Controller\CFML;

use App\Entity\Core\PersonNatural;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Annotation\Translation;

/**
 * @Route("/cfml")
 */
class UrssafDeclarationController extends AbstractController
{
    /**
     * @Route("/{id}/urssafDeclaration", name="cfml_urssaf_declaration", methods={"GET"})
     * @Translation("Visualisation des déclarations D'urssaf")
     * @param PersonNatural $personNatural
     */
    public function index(PersonNatural $personNatural)
    {

        return $this->render('Cfml/urssafDeclaration.html.twig', [
            'personNatural' => $personNatural
        ]);
    }
}
