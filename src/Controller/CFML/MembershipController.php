<?php

namespace App\Controller\CFML;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Controller\BaseAbstractController;
use App\Entity\CFML\MembershipCfml;
use App\Entity\Core\PersonNatural;
use App\Repository\CFML\MembershipCfmlSpecificStatusHistorizationRepository;
use App\Repository\CFML\MembershipRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Repository\Core\StaffRepository;
use App\Repository\Legacy\LegacyRepository;
use App\Service\Historization\MembershipCfmlHistorizationManager;
use App\Service\Membership\MembershipCfmlService;
use App\Service\Historization\MembershipCfmlSpecificStatusHistorizationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cfml/membership")
 */
class MembershipController extends BaseAbstractController
{
    /**
     * @Route("/", name="membership_cfml" , methods={"GET","POST"})
     * @param LegacyRepository $legacyRepository
     * @param Request $request
     * @param StaffRepository $staffRepository
     * @param PersonNaturalRepository $personNaturalRepository
     * @param Session $session
     * @param MembershipCfmlService $membershipCfmlService
     * @param MembershipCfmlHistorizationManager $membershipCfmlHistorizationService
     * @param MembershipRepository $membershipRepository
     * @return Response
     * @Translation("Visualisation et Ajout de client qui sont adhérents CFML dans le portefolio d'un staff")
     */
    public function index(LegacyRepository $legacyRepository, Request $request, MembershipCfmlService $membershipCfmlService, MembershipRepository $membershipRepository, StaffRepository $staffRepository, MembershipCfmlHistorizationManager $membershipCfmlHistorizationService, PersonNaturalRepository $personNaturalRepository, Session $session): Response
    {
        //TODO -  plus tard la table qui va permettre de récupérer les clients adhérents au CFML va changer
        $legacyMemberCFML = $legacyRepository->findByLegacyCFML();
        $memberships = $membershipRepository->findAll();

        $staffCFML = $staffRepository->staffByRoles(['ROLE_CFML_ADMINISTRATIF', 'ROLE_CFML_TECHNIQUE']);
        $arrayMember = $request->request->get('arrayPersons');
        // staff choisi
        $choiceStaffCfml = $request->request->get('choiceStaffCFML');
        $uncheckAllMember = $request->request->get('uncheckAllClient');
        $date = new \DateTime();
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        if ($uncheckAllMember) {
            $session->remove('arrayMemberSession');
        }

        if ($arrayMember && $choiceStaffCfml) {
            $session->set('arrayMemberSession', $arrayMember);
            $arrayMember = json_decode($arrayMember);
            $staff = $staffRepository->findOneBy(['id' => intval($choiceStaffCfml)]);

            foreach ($arrayMember->members_id as $memberId) {
                // création d'un tableau de données
                $arrayData = [
                    "member" => $personNaturalRepository->findOneBy(['id' => $memberId]),
                    "representativeStaff" => $staff,
                    "creator" => $this->getUser(),
                    "date" => $date,
                    "field" => null
                ];
                $membershipCfmlService->manageMember($arrayData);
                $membershipCfmlHistorizationService->historize($membershipCfmlService->getMember(), $membershipCfmlService->getArrayData());
                $membershipCfmlHistorizationService->save();
                $this->addFlash('success', 'Les clients ont étes attribués');
            }
            return $this->redirectToRoute("membership_cfml");
        }

        if (!$session->get('arrayMemberSession')) {
            $session->set('arrayMemberSession', "");
        }

        $arrayStatusCheck = [];
        $decodeArrayClientSession = json_decode($session->get('arrayMemberSession'));

        if (!empty($decodeArrayClientSession)) {
            foreach ($legacyMemberCFML as $legacyMember) {
                if (in_array($legacyMember->getPerson()->getId(), $decodeArrayClientSession->members_id)) {
                    array_push($arrayStatusCheck, $legacyMember->getPerson()->getId());
                }
            }
        }

        return $this->render('Cfml/Membership/index.html.twig', [
            'controller_name' => 'MembershipController',
            'memberCFML' => $legacyMemberCFML,
            'memberships' => $memberships,
            'staffCFML' => $staffCFML,
            'arrayStatusCheck' => $arrayStatusCheck,
            'arrayMemberSession' => $session->get('arrayMemberSession'),
        ]);
    }

    /**
     * @Route("/{id}/addSpecificStatusToMembershipCfml", name="add_specific_status_to_membeship_cfml" , methods={"POST"})
     * @param Request $request
     * @param PersonNatural $personNatural
     * @param PersonNaturalRepository $personNaturalRepository
     * @param MembershipCfmlService $membershipCfmlService
     * @param MembershipRepository $membershipRepository
     * @param MembershipCfmlSpecificStatusHistorizationRepository $membershipCfmlSpecificStatusHistorizationRepository
     * @param MembershipCfmlSpecificStatusHistorizationManager $membershipCfmlSpecificStatusHistorizationService
     * @return JsonResponse
     * @NoTranslation()
     * Ajax endpoint
     */
    public function addSpecificStatusToMember(Request $request, PersonNatural $personNatural, PersonNaturalRepository $personNaturalRepository, MembershipCfmlService $membershipCfmlService, MembershipRepository $membershipRepository, MembershipCfmlSpecificStatusHistorizationManager $membershipCfmlSpecificStatusHistorizationService, MembershipCfmlSpecificStatusHistorizationRepository $membershipCfmlSpecificStatusHistorizationRepository, int $id): JsonResponse
    {
        $modifiedField = json_decode($request->request->get('oldSpecificStatus'));
        $date = new \DateTime();
        $date->setTimezone(new \DateTimeZone('Europe/Paris'));

        $arrayData = [
            "member" => $personNaturalRepository->findOneBy(['id' => $id]),
            "creator" => $this->getUser(),
            "specificStatus" => $modifiedField,
            "date" => $date
        ];

        if (empty($modifiedField) || !in_array($modifiedField, MembershipCfml::SPECIFIC_STATUS)) {
            return $this->errorJsonResponse("Une erreur est survenue");
        }

        $membershipCfmlService->manageMember($arrayData);
        $membershipCfml = $membershipCfmlService->getMember();
        $membershipCfmlSpecificStatusHistorizationService->historize($membershipCfml, $membershipCfmlService->getArrayData());
        $membershipCfmlSpecificStatusHistorizationService->save();

        $membershipCfml = $membershipRepository->findOneBy(['member' => $id]);
        $historizationSpecificStatus = json_encode($membershipCfmlSpecificStatusHistorizationRepository->findBy(['membership' => $membershipCfml]));
        $membershipCfml = MembershipCfml::LABEL[$membershipCfml->getSpecificStatus()];

        return $this->successJsonResponse(["historize" => $historizationSpecificStatus, "status" => $membershipCfml]);

    }
}

