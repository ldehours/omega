<?php

namespace App\Controller\CFML;

use App\Entity\Core\PersonNatural;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Annotation\Translation;

/**
 * @Route("/cfml")
 */
class CarmfDeclarationController extends AbstractController
{
    /**
     * @Route("/{id}/carmfDeclaration", name="cfml_carmf_declaration", methods={"GET"})
     * @Translation("Visualisation des déclarations Carmf")
     * @param PersonNatural $personNatural
     */
    public function index(PersonNatural $personNatural)
    {

        return $this->render('Cfml/carmfDeclaration.html.twig', [
            'personNatural' => $personNatural
        ]);
    }
}
