<?php

namespace App\Controller\CFML;

use App\Entity\Core\PersonNatural;
use App\Repository\Legacy\LegacyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Annotation\Translation;

/**
 * @Route("/cfml")
 */
class CurpsController extends AbstractController
{
    /**
     * @Route("/{id}/curps", name="cfml_curps", methods={"GET"})
     * @Translation("Visualisation des Curps")
     * @param PersonNatural $personNatural
     * @param LegacyRepository $legacyRepository
     * @return Response
     */
    public function index(PersonNatural $personNatural, LegacyRepository $legacyRepository): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CFML", $this->getUser());
        // on check si la Person natural est bel et bien adhérente au CFML
        $isMemberCFML = $legacyRepository->findOneBy(['person' => $personNatural]);
        if (!$isMemberCFML) {
            $msgErreur = "Le client " . $personNatural->getLastName() . " " . $personNatural->getFirstName() . " n'est pas adhérent au CFML";
            throw $this->createAccessDeniedException($msgErreur);
        }
        return $this->render('Cfml/curps.html.twig', [
            'personNatural' => $personNatural,
        ]);
    }
}