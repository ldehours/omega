<?php

namespace App\Controller\CFML;

use App\Repository\Legacy\LegacyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Annotation\Translation;
use App\Entity\Core\PersonNatural;

/**
 * @Route("/cfml")
 */
class UrssafController extends AbstractController
{
    /**
     * @Route("/{id}/urssaf", name="cfml_urssaf", methods={"GET"})
     * @Translation("Visualisation de l'urssaf")
     * @param PersonNatural $personNatural
     * @param LegacyRepository $legacyRepository
     * @return Response
     */
    public function index(PersonNatural $personNatural, LegacyRepository $legacyRepository): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CFML", $this->getUser());
        // on check si la Person natural est bel et bien adhérente au CFML
        $isMemberCFML = $legacyRepository->findOneBy(['person' => $personNatural]);
        if (!$isMemberCFML) {
            $msgErreur = "Le client " . $personNatural->getLastName() . " " . $personNatural->getFirstName() . " n'est pas adhérent au CFML";
            throw $this->createAccessDeniedException($msgErreur);
        }

        return $this->render('Cfml/urssaf.html.twig', [
            'personNatural' => $personNatural
        ]);
    }
}