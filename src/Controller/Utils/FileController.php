<?php


namespace App\Controller\Utils;


use App\Annotation\Translation;
use App\Entity\Offer\Offer;
use App\Repository\Offer\OfferRepository;
use Symfony\Component\DependencyInjection\Tests\Compiler\J;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/file")
 */
class FileController extends AbstractController
{
    /**
     * @Route("/offer/import/{id}", name="file_import_offer")
     * @Translation("Ajout d'une image à une offre")
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function importOfferImage(Request $request, int $id): JsonResponse
    {
        if ($id <= 0) {
            $id = 'temp';
        }

        $randomName = substr(md5(rand()), 0, 7);
        $file = $request->files->get('file');
        $file->move($this->getParameter('kernel.project_dir') . "/public/images/offer/",
            $randomName . "-" . $id . "." . $file->getClientOriginalExtension());
        if (empty($file)) {
            return new JsonResponse("Error", 500);
        }
        return new JsonResponse($randomName . "-" . $id . "." . $file->getClientOriginalExtension());
    }
}