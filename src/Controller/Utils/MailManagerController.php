<?php

namespace App\Controller\Utils;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Entity\Core\Discussion;
use App\Entity\Mail\PreWrittenMail;
use App\Repository\Core\DiscussionRepository;
use App\Repository\Core\PersonRepository;
use App\Repository\Core\StaffRepository;
use App\Repository\Mail\PreWrittenMailRepository;
use App\Utils\APITipimailHelper;
use App\Utils\ConvertionHelper;
use App\Utils\MailMessage;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Tipimail;

/**
 * @Route("/mail/manager")
 */
class MailManagerController extends AbstractController
{
    private $discussionRepository;
    private $staffRepository;
    private $personRepository;
    private $preWrittenMailRepository;
    private $paginator;

    /**
     * @NoTranslation()
     * MailManagerController constructor.
     * @param DiscussionRepository $discussionRepository
     * @param StaffRepository $staffRepository
     * @param PersonRepository $personRepository
     * @param PreWrittenMailRepository $preWrittenMailRepository
     * @param PaginatorInterface $paginator
     */
    public function __construct(
        DiscussionRepository $discussionRepository,
        StaffRepository $staffRepository,
        PersonRepository $personRepository,
        PreWrittenMailRepository $preWrittenMailRepository,
        PaginatorInterface $paginator
    ) {
        $this->discussionRepository = $discussionRepository;
        $this->staffRepository = $staffRepository;
        $this->personRepository = $personRepository;
        $this->preWrittenMailRepository = $preWrittenMailRepository;
        $this->paginator = $paginator;
    }

    /**
     * @Translation("Accès boîte mail")
     * @Route("/", name="mail_manager_index", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $discussionRepository = $this->discussionRepository;
        $user = $this->getUser();
        $userMail = $user->getEmail();

        $inBox = [];//En attente pour l'instant
        APITipimailHelper::getMailSent($discussionRepository, $user, [$userMail], $this->getDoctrine()->getManager());
        $pagination = MailMessage::showListMails($discussionRepository, $this->paginator, 1, $user);

        //sentBox est vide car gérer par PAGINATOR
        $mailBox = ['inBox' => $inBox, 'sentBox' => []];
        return $this->render('Mail/mailBox.html.twig', [
            'mailBoxes' => $mailBox,
            'staff' => $user,
            'pagination' => $pagination,
            'preWrittenMailList' => $this->preWrittenMailRepository->findAll()
        ]);
    }

    /**
     * @Translation("Création mail prédéfini")
     * @Route("/create/mail", name="mail_create", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createPreWrittenMail()
    {
        return $this->render('Mail/createPreWrittenMail.html.twig');
    }

    /**
     * @NoTranslation()
     * @Route("/save/mail", name="mail_save", methods={"POST"})
     * @param Request $request
     * @param PreWrittenMailRepository $preWrittenMailRepository
     * @return JsonResponse
     */
    public function savePreWrittenMail(Request $request): JsonResponse
    {
        $mailSubject = $request->get('mail-title');
        $mailContent = $request->get('mail-content');
        $formStatus = $request->get('form-status');
        $preWrittenMailRepository = $this->preWrittenMailRepository;
        if (strlen(trim($mailSubject)) < 5) {
            return new JsonResponse([
                'save' => false,
                'message' => 'Le mail doit avoir un intitulé (5 caractères minimum) !'
            ]);
        }

        if (empty(trim($mailContent))) {
            return new JsonResponse([
                'save' => false,
                'message' => 'Le contenu mail ne peut pas être vide !'
            ]);
        }
        $title = strtolower(str_replace("'", "",
            implode("_", explode(" ", ConvertionHelper::strToNoAccent($mailSubject)))));
        $file = '../templates/Mail/_' . $title . 'Mail.html.twig';

        switch ($formStatus) {
            case "create":
                if ($preWrittenMailRepository->findBy(['subject' => $mailSubject]) != []) {
                    return new JsonResponse([
                        'save' => false,
                        'message' => 'Le mail ' . $mailSubject . ' existe déjà ! Allez dans modification mail prédéfini'
                    ]);
                }
                $mail = new preWrittenMail();
                break;
            case "edit":
                $mail = $preWrittenMailRepository->findOneBy(['id' => $request->get('mail-id')]);
                break;
        }
        $saveContent = file_put_contents($file, trim($mailContent), LOCK_EX);
        if ($saveContent === false) {
            return new JsonResponse([
                'save' => false,
                'message' => 'Une erreur est survenue !'
            ]);
        }
        $mail->setSubject($mailSubject);
        $mail->setContent($file);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($mail);
        $entityManager->flush();
        return new JsonResponse([
            'save' => true,
            'message' => $mailSubject
        ]);
    }

    /**
     * @Translation("Modifier mail prédéfini")
     * @Route("/edit/mail", name="mail_edit" , methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPreWrittenMail()
    {
        return $this->render('Mail/editPreWrittenMail.html.twig', [
            'preWrittenMailList' => $this->preWrittenMailRepository->findAll()
        ]);
    }

    /**
     * @NoTranslation()
     * @Route("/show/mail", name="mail_show" , methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function showPreWrittenMail(Request $request): JsonResponse
    {
        $mailId = $request->get('preWrittenMailId');
        $preWrittenMail = $this->preWrittenMailRepository->findOneBy(['id' => $mailId]);
        return new JsonResponse([
            'title' => $preWrittenMail->getSubject(),
            'content' => file_get_contents($preWrittenMail->getContent())
        ]);
    }

    /**
     * @Route("/read/mail" , name="read_mail", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @return JsonResponse
     */
    public function readMail(Request $request): JsonResponse
    {
        $mailUID = $request->get('mailUid');
        $box = $request->get('mailBox');
        $mailReplyTo = $request->get('mailSender') ?? $request->get('mailReceiver');
        $mailMessage = "aucun contenu";

        if ($box == "sentBox") {
            $attachments = [];
            $discussion = $this->discussionRepository->findOneBy(['tag' => $mailUID]);
            if ($discussion != null) {
                $userName = $discussion->getCreator()->getEmail();
                $mailSubject = $discussion->getSubject();
                $mailMessage = $discussion->getMemo();
                if ($discussion->getPerson() != null) {
                    $mailReplyTo = $discussion->getPerson()->getDefaultMail()->getAddress();
                }
                $attachmentsDir = $_ENV['ATTACHMENT_DIR'] . 'sent/' . $mailUID;
                if (is_dir($attachmentsDir)) {
                    foreach (scandir($attachmentsDir) as $index => $file) {
                        if ($file != "." && $file != "..") {
                            $attachments[$index - 2]['file'] = $file;
                            $attachments[$index - 2]['link'] = "/" . $attachmentsDir . "/" . $file;
                        }
                    }
                }
            } else {
                $mailMessage = "Mail envoyé avant l'implémentation de la sauvegarde  ou bien l' app:i a été exécutée!";
            }
            $mailAttachments = $attachments;
        }
        return new JsonResponse([
            'uid' => $mailUID,
            'user' => $userName,
            'replyTo' => $mailReplyTo,
            'message' => $mailMessage,
            'subject' => $mailSubject,
            'attachments' => $mailAttachments,
            'box' => $box
        ]);
    }

    /**
     * @Route("/send/mail" , name="send_mail", methods={"POST"})
     * @NoTranslation()
     * @param Request $request
     * @param PersonRepository $personRepository
     * @return JsonResponse
     */
    public function sendEmail(Request $request, PersonRepository $personRepository): JsonResponse
    {
        $user = $this->getUser();
        $withAttachments = ($_FILES != []);
        $allAttachments = null;
        $mailDeliveryStatus = false;

        if ($withAttachments) {
            $attachments = $_FILES['attachments'];
            $uploadErrorMessage = [
                0 => 'There is no error, the file uploaded with success',
                1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
                2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
                3 => 'The uploaded file was only partially uploaded',
                4 => 'No file was uploaded',
                6 => 'Missing a temporary folder',
                7 => 'Failed to write file to disk.',
                8 => 'A PHP extension stopped the file upload.',
            ];
            $allAttachmentsErrors = $attachments['error'];
            $allAttachmentsNames = $attachments['name'];
            $allAttachmentsSize = $attachments['size'];
            $allAttachments = [
                'exist' => $withAttachments,
                'allAttachmentsTmp_names' => $attachments['tmp_name'],
                'allAttachmentsType' => $attachments['type'],
                'allAttachmentsNames' => $allAttachmentsNames
            ];

            foreach ($allAttachmentsErrors as $index => $error) {
                if ($error != 0) {
                    return new JsonResponse([
                        'status' => $mailDeliveryStatus,
                        'message' => $allAttachmentsNames[$index] . " " . $uploadErrorMessage[$error],
                    ]);
                }
                if ($allAttachmentsSize[$index] > 4000000) { //4000000 = 4Mo, la taille Max par défaut d'un fichier que l'on peut uplaoder
                    return new JsonResponse([
                        'status' => $mailDeliveryStatus,
                        'message' => $allAttachmentsNames[$index] . " " . $uploadErrorMessage[$error],
                    ]);
                }
            }
        }
        //try catch pour n'enregistrer le mail que si il est bien envoyé
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $subject = $request->request->get('mailSubject');
            $recepientMail = $request->request->get('mailReceiver');
            $recepientId = $request->request->get('clientId');
            $messageMail = $request->request->get('mailMessage');
            $sendingMessage = APITipimailHelper::sendMailWithAPI($user, $recepientMail, $recepientId, $subject,
                $messageMail, $personRepository, $entityManager, $allAttachments);
            return new JsonResponse([
                'status' => $sendingMessage['deliveryStatus'],
                'message' => $sendingMessage['message']
            ]);

        } catch (Tipimail\Exceptions\TipimailException $e) {
            return new JsonResponse([
                'status' => $mailDeliveryStatus,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @NoTranslation()
     * @Route("/paginate/mail", name="paginate_mail", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function paginateMailsList(Request $request): JsonResponse
    {
        $list = [];
        $personId = $request->get('personId');
        $staffId = $request->get('staffId');

        $person = $personId != -1 ? $this->personRepository->find($personId) : null;
        $staff = $staffId != -1 ? $this->staffRepository->find($staffId) : null;
        $page = $request->get('page');
        $pagination = MailMessage::showListMails($this->discussionRepository, $this->paginator, $page, $staff, $person);
        $status = Discussion::STATUS;
        foreach ($pagination->getItems() as $key => $discussion) {
            if ($discussion->getPerson() == null) {
                $mail = APITipimailHelper::getMailDetailFromTipimail($discussion->getTag());
                $list[$key]['clientMail'] = $mail->getMsg()->getEmail();
            } else {
                $list[$key]['clientMail'] = $discussion->getPerson()->getDefaultMail()->getAddress();
            }
            $list[$key]['tag'] = $discussion->getTag();
            $list[$key]['subject'] = $discussion->getSubject();
            $list[$key]['status'] = ucfirst($status[$discussion->getStatus()]);
            $list[$key]['statusDate'] = $discussion->getStatusDate()->format('d M Y à H:i:s');
            $list[$key]['creationDate'] = $discussion->getCreationDate()->format('d M Y à H:i:s');
            $list[$key]['userMail'] = $this->getUser()->getEMail();
        }

        return new JsonResponse([
            'pageNumber' => ceil($pagination->getTotalItemCount() / $pagination->getItemNumberPerPage()),
            'items' => $list
        ]);
    }
}
