<?php

namespace App\Controller\Utils;

use App\Annotation\Translation;
use App\Entity\Core\Staff;
use App\Repository\Core\PersonRepository;
use App\Repository\Core\StaffRepository;
use Mgilet\NotificationBundle\Manager\NotificationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NotificationController
 * @package App\Controller\Utils
 * @Route("/notification")
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/send", name="notification_send")
     * @Translation("Envoi d'une notification")
     * @param NotificationManager $notificationManager
     * @param Request $request
     * @param StaffRepository $staffRepository
     * @param PersonRepository $personRepository
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function sendNotification(
        NotificationManager $notificationManager,
        Request $request,
        StaffRepository $staffRepository,
        PersonRepository $personRepository
    ): Response {
        $personId = $request->get('personID');
        $person = $personRepository->findOneBy(['id' => $personId]);
        $link = $this->generateUrl('core_person_natural_show',[
            'id' => $personId
        ]);
        /** @var Staff $user */
        $user = $this->getUser();
        $staffs = $staffRepository->findById($request->get('staffs'));
        $subject = '['.$user->getInitials().'] ';
        $subjects = $request->get('subjects');
        if(isset($subjects) && !empty($subjects)){
            foreach ($subjects as $keySubject){
                $subject .= Staff::NOTIFICATION_TYPE[$keySubject]." / ";
            }
            $subject = substr($subject, 0, -3);
        }
        $subject .= ' ('. $person->getName().')';
        $notif = $notificationManager->createNotification($subject, $request->get('message'), $link);
        $notificationManager->addNotification($staffs, $notif, true);

        $this->addFlash("success", "Notification envoyée !");
        return $this->redirectToRoute('core_person_natural_show',[
            'id' => $request->get('personID')
        ]);
    }

    /**
     * @Route("/mark", name="notification_mark_as_seen")
     * @Translation("Marquer une notification comme lue")
     * @param NotificationManager $notificationManager
     * @param StaffRepository $staffRepository
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\EntityNotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function markAsSeenNotification
    (
        NotificationManager $notificationManager,
        StaffRepository $staffRepository,
        Request $request
    ): JsonResponse {
        $notificationManager->markAsSeen($staffRepository->findOneById($request->get('userID')),
            $notificationManager->getNotification($request->get('notificationID')), true);
        return new JsonResponse();
    }

    /**
     * @Route("/mark/all", name="notification_mark_all_as_seen")
     * @Translation("Marquer toutes les notifications comme lues")
     * @param NotificationManager $notificationManager
     * @param StaffRepository $staffRepository
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function markAllAsSeenNotification
    (
        NotificationManager $notificationManager,
        StaffRepository $staffRepository,
        Request $request
    ): JsonResponse {
        $notificationManager->markAllAsSeen($staffRepository->findOneById($request->get('userID')), true);
        return new JsonResponse();
    }

    /**
     * @Route("/delete", name="notification_delete")
     * @Translation("Supprimer une notification")
     * @param NotificationManager $notificationManager
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteOneNotification(
        NotificationManager $notificationManager,
        Request $request
    ):Response{
        $notificationManager->deleteNotification($request->get('notificationID',true));
        return $this->redirectToRoute('homepage');
    }
}