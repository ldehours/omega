<?php


namespace App\Controller\Utils;


use App\Annotation\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/front")
 */
class FrontController extends AbstractController
{
    /**
     * @Route("/toggle/sidebar",name="toggle_sidebar")
     * @Translation("Basculer la taille du menu")
     */
    public function toggleSidebar(): JsonResponse
    {
        $session = $this->get('session');
        $miniMenu = $session->get('mini_menu');
        if ($miniMenu && $miniMenu == true) {
            $session->set('mini_menu', false);
        } else {
            $session->set('mini_menu', true);
        }
        return new JsonResponse();
    }

    /**
     * @Route("/toggle/background",name="toggle_background" ,methods={"POST"})
     * @Translation("Basculer entre mode jour et mode nuit")
     * @return JsonResponse
     */
    public function toggleBackground(): JsonResponse
    {
        $session = $this->get('session');
        $whiteColor = ($session->get('white_color') ?? true);
        if ($whiteColor == true) {
            $session->set('white_color', false);
        } else {
            $session->set('white_color', true);
        }
        return new JsonResponse(['whiteColor' => $whiteColor]);
    }

    /**
     * @Route("/change/menu/color/{color}",name="change_menu_color")
     * @Translation("Changer la couleur du menu")
     */
    public function changeMenuColor(string $color): JsonResponse
    {
        $session = $this->get('session');
        $sessionColor = $session->get('color');
        if (!$sessionColor) {
            $session->set('color', 'primary');
        } else {
            $session->set('color', $color);
        }
        return new JsonResponse();
    }
}