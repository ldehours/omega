<?php


namespace App\Service\CustomValidator;

/**
 * Interface CustomValidatorInterface
 * @package App\Service\CustomValidator
 */
interface CustomValidatorInterface
{
    /**
     * @return array|string|null
     */
    public function validate();

    /**
     * @return bool
     */
    public function isValid():bool;
}