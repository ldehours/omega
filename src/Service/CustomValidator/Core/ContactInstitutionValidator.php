<?php


namespace App\Service\CustomValidator\Core;


use App\Repository\Core\OriginRepository;
use App\Service\CustomValidator\CustomValidatorInterface;
use Symfony\Component\HttpFoundation\Request;

class ContactInstitutionValidator implements CustomValidatorInterface
{

    private $data;

    /**
     * @var OriginRepository
     */
    private $originRepository;

    public function __construct(Request $request, OriginRepository $originRepository)
    {
        $this->data = [
            'personFirstName' => $request->request->get('firstname'),
            'personLastName' => $request->request->get('lastname'),
            'originId' => $request->request->get('origin')
        ];
        $this->originRepository = $originRepository;
    }

    public function validate(): array
    {
        $errors = [];

        $personFirstName = $this->data['personFirstName'];
        $personLastName = $this->data['personLastName'];
        $originId = $this->data['originId'];
        if(!$personFirstName || !$personLastName || strlen($personFirstName) > 255 || strlen($personFirstName) > 255 || $personFirstName === '' || $personLastName === ''){
            $errors[] = 'Le prénom et le nom de famille doivent être remplis et doivent faire moins de 255 charactères.';
        }
        if(!$originId || preg_match('/^[0-9]+$/',$originId) !== 1 ){
            $errors[] = "L'origine n'est pas correcte, veuillez réessayer.";
        }
        $origin = $this->originRepository->findOneBy([
            'id' => $originId
        ]);
        if(!isset($origin)){
            $errors[] = "Il semblerait que l'origine selectionnée n'est pas/plus en base, veuillez réessayer ou contacter un administrateur.";
        }

        return $errors;
    }

    public function isValid(): bool
    {
        return empty($this->validate());
    }
}