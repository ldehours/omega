<?php


namespace App\Service\CustomValidator\Core;


use App\Repository\Core\OpinionRepository;
use App\Repository\Core\PersonRepository;
use App\Service\CustomValidator\CustomValidatorInterface;
use Symfony\Component\HttpFoundation\Request;

class OpinionValidator implements CustomValidatorInterface
{

    /**
     * @var array
     */
    private $data;

    /**
     * @var OpinionRepository
     */
    private $opinionRepository;

    /**
     * @var PersonRepository
     */
    private $personRepository;

    public function __construct(
        Request $request,
        OpinionRepository $opinionRepository,
        PersonRepository $personRepository
    ){
        $this->data = [
            'giverId' => $request->request->get('giverId'),
            'receiverId' => $request->request->get('receiverId'),
            'text' => $request->request->get('text')
        ];
        $this->opinionRepository = $opinionRepository;
        $this->personRepository = $personRepository;
    }

    /**
     * @return string|null
     */
    public function validate(): ?string
    {
        $giverId = $this->data['giverId'];
        $receiverId = $this->data['receiverId'];
        $text = $this->data['text'];
        if(empty($giverId) || empty($receiverId) || empty($text)){
            return "Les données transmises sont incomplètes (avez-vous rempli tous les champs ?)";
        }

        $giver = $this->personRepository->find($giverId);
        $receiver = $this->personRepository->find($receiverId);
        if(!$giver){
            return 'La personne ayant pour identifiant ' . $giverId . ' n\'a pas été trouvée';
        } else if(!$receiver){
            return 'La personne ayant pour identifiant ' . $receiverId . ' n\'a pas été trouvée';
        }else if(empty($text)){
            return 'Un avis doit être rempli';
        }

        $existingOpinion = $this->opinionRepository->findOneBy([
            'giver' => $giver,
            'receiver' => $receiver
        ]);
        if($existingOpinion !== null){
            return 'Il existe déjà un commentaire de ' . $giver->getName() . ' concernant ' . $receiver->getName() .
                ' (renseigné le '. $existingOpinion->getCreatedAt()->format('d/m/Y') .')';
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->validate() === null;
    }
}