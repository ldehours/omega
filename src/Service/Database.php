<?php


namespace App\Service;


interface Database
{
    /**
     * Returns the result in an array of objects for Select query
     * otherwise returns the number of modified rows
     * @param string $query
     * @param bool $isRollback
     * @return DatabaseResult
     */
    function query(string $query, bool $isRollback = true): DatabaseResult;

}