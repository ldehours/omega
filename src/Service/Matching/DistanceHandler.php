<?php


namespace App\Service\Matching;


use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;

class DistanceHandler
{

    public static function getShortestDistance(Offer $offer, Wish $wish): ?float
    {
        $wishDistances = $wish->getDistance();
        $shortestDistance = null;

        if (!empty($wishDistances)) {
            $nbDistanceMatching = 0;
            $personOfferAddress = '';

            if ($offer->getPerson() !== null && $offer->getPerson()->getDefaultAddress() !== null) {
                $personOfferAddress = $offer->getPerson()->getDefaultAddress()->getReadableAddress();
            }

            foreach ($wishDistances as $wishDistance) {
                $resultApiJson = file_get_contents(
                    sprintf(
                        'http://10.113.101.74:5050/compute_two_points?address_1=%s&address_2=%s&method=%s',
                        urlencode($wishDistance['cp']),
                        urlencode($personOfferAddress),
                        'fast'
                    )
                );
                $resultApi = json_decode($resultApiJson, true, 512, JSON_THROW_ON_ERROR);
                if (!isset($resultApi['distance'])) {
                    continue;
                }

                $distanceKm = (float)$resultApi['distance'];
                if (($distanceKm / 1000) <= $wishDistance['km']) {
                    $nbDistanceMatching++;
                    if ($shortestDistance === null || $distanceKm < $shortestDistance) {
                        $shortestDistance = floor($distanceKm / 1000);
                    }
                }
                if ($nbDistanceMatching === 0) {
                    continue;
                }
            }
        } elseif (($wish->getPerson() !== null && $wish->getPerson()->getDefaultAddress() !== null) &&
            ($offer->getPerson() !== null && $offer->getPerson()->getDefaultAddress() !== null)) {

            $personWishAddress = $wish->getPerson()->getDefaultAddress()->getReadableAddress();
            $personOfferAddress = $offer->getPerson()->getDefaultAddress()->getReadableAddress();
            $resultApiJson = file_get_contents(
                sprintf(
                    'http://10.113.101.74:5050/compute_two_points?address_1=%s&address_2=%s&method=%s',
                    urlencode($personWishAddress),
                    urlencode($personOfferAddress),
                    'fast'
                )
            );
            $resultApi = json_decode($resultApiJson, true, 512,
                JSON_THROW_ON_ERROR); // ["distance"=>"X","duration"=>"Y"]
            if (!array_key_exists('error', $resultApi)) {
                $shortestDistance = floor($resultApi['distance'] / 1000);
            }
        }

        return $shortestDistance;
    }
}