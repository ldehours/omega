<?php


namespace App\Service;


use App\Utils\APIHelper;

class SMSBox implements SMSInterface
{
    // TODO trouver un moyen d'utiliser le service.yaml pour mettre cette url
    private $url = 'https://api.smsbox.fr/1.1/api.php';
    private $apiHelper;

    private $apiKey;
    private $message;
    private $recipients;
    private $options = ['mode' => 'Standard', 'strategy' => '4', 'charset' => 'utf-8'];

    /**
     * SMSBox constructor.
     * @param string $apiKey
     * @param string $message
     * @param array $options
     */
    public function __construct(string $apiKey, string $message, array $options = [])
    {
        $this->apiKey = $apiKey;
        $this->message = $message;
        $this->recipients = [];
        $this->options = array_merge($this->options, $options);
        $this->apiHelper = new APIHelper();
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        $result = false;
        $recipient = implode(',', $this->recipients);
        if (is_string($recipient) && preg_match('/([0-9]{11}(,))*([0-9]{11})+$/', $recipient)) {
            $parameters = ['apikey' => $this->apiKey, 'msg' => $this->message, 'dest' => $recipient];
            $parameters = array_merge($parameters, $this->options);
            $result = $this->apiHelper->send(APIHelper::METHOD_POST, $this->url, $parameters);
        }

        return $result;
    }

    /**
     * @param array $recipients
     */
    public function setRecipients(array $recipients): void
    {
        $this->recipients = $recipients;
    }

    /**
     * @param string $number
     */
    public function addRecipient(string $number): void
    {
        $this->recipients[] = $number;
    }

    /**
     * @return array
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @param string $number
     */
    public function removeRecipient(string $number): void
    {
        $newRecipients = [];
        foreach ($this->recipients as $recipient){
            if($recipient === $number){
                continue;
            }
            $newRecipients[] = $recipient;
        }
        $this->recipients = $newRecipients;
    }
}