<?php


namespace App\Service;

/**
 * Interface SMSInterface
 * @package App\Service
 */
interface SMSInterface
{
    /**
     * SMSInterface constructor.
     * @param string $apiKey
     * @param string $message
     * @param array $options
     */
    public function __construct(string $apiKey, string $message, array $options = []);

    /**
     * @return bool
     */
    public function send(): bool;

    /**
     * @param array $recipients
     */
    public function setRecipients(array $recipients): void;

    /**
     * @param string $number
     */
    public function addRecipient(string $number): void;

    /**
     * @param string $number
     */
    public function removeRecipient(string $number): void;

    /**
     * @return array
     */
    public function getRecipients():array;
}