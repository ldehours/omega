<?php


namespace App\Service;


use App\Utils\API;
use Symfony\Component\HttpFoundation\JsonResponse;

class APIManager
{
    private static $APIManager;
    private static $APIs = [];

    /**
     * APIManager constructor.
     */
    public function __construct()
    {
        self::$APIs['departements'] = new API('https://geo.api.gouv.fr/departements');
        self::$APIs['regions'] = new API('https://geo.api.gouv.fr/regions');
    }

    /**
     * @param string $APIid
     * @return bool|null
     */
    public static function get(string $APIid): ?bool
    {
        self::$APIManager = self::$APIManager ?? new APIManager();
        if(key_exists($APIid,self::$APIs)){
            return self::save($APIid,self::$APIs[$APIid]->call());
        }
        return null;
    }

    /**
     * @param string $APIid
     * @param JsonResponse $data
     * @return bool
     */
    public static function save(string $APIid, JsonResponse $data)
    {
        $dataArray = json_decode($data->getContent());
        if($data == null || empty(json_decode($dataArray))){
            return false;
        }
        $fileName = $APIid.".json";
        file_put_contents($fileName,$dataArray);
        return true;
    }

    /**
     * @return array
     */
    public function getAPIs():array
    {
        return self::$APIs;
    }
}