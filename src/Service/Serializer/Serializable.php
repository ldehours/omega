<?php


namespace App\Service\Serializer;

/**
 * Interface Serializable
 * @package App\Service\Serializer
 */
interface Serializable
{
    /**
     * @return array
     */
    public function serialize(): array;
}