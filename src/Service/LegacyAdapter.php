<?php


namespace App\Service;


use App\Entity\Core\JobName;

class LegacyAdapter
{
    /**
     * @param int $status
     * @param bool $returnLower
     * @return string
     */
    public static function getStatusInitial(int $status, bool $returnLower = false): string
    {
        $initial = null;

        switch ($status) {
            case JobName::BASED:
                $initial = 'I';
                break;

            case JobName::SUBSTITUTE:
                $initial = 'R';
                break;

            case JobName::INSTITUTION:
                $initial = 'E';
                break;

            default:
                $initial = 'A';
                break;
        }

        if ($returnLower) {
            $initial = strtolower($initial);
        }

        return $initial;
    }
}