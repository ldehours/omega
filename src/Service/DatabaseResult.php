<?php


namespace App\Service;


class DatabaseResult
{
    private $numberLines;

    private $data;

    private $error;

    /**
     * DatabaseResult constructor.
     * @param int $numberLines
     * @param array $data
     * @param DatabaseErrorInfo $error
     */
    public function __construct(int $numberLines, array $data, DatabaseErrorInfo $error = null)
    {
        $this->numberLines = $numberLines;
        $this->data = $data;
        $this->error = $error;
    }


    /**
     * @return int
     */
    public function getNumberLines(): int
    {
        return $this->numberLines;
    }

    /**
     * @param int $number
     * @return $this
     */
    public function setNumberLines(int $number):self
    {
        $this->numberLines = $number;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        $this->numberLines = count($data);
        return $this;
    }

    /**
     * @return DatabaseErrorInfo
     */
    public function getError(): ?DatabaseErrorInfo
    {
        return $this->error;
    }

    /**
     * @param DatabaseErrorInfo $error
     * @return $this
     */
    public function setError(DatabaseErrorInfo $error):self
    {
        $this->error = $error;
        return $this;
    }
}