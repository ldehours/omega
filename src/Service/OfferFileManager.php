<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Class OfferFileManager
 * @package App\Service
 */
class OfferFileManager
{
    /**
     * @var Finder
     */
    private $finder;

    public function __construct(Finder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param string $imageFolderPath
     * @param string $fileName
     */
    public function removeFileAfterUpdate(string $imageFolderPath, string $fileName):void{
        $this->finder->in($imageFolderPath);
        $this->finder->files()->name($fileName);

        foreach ($this->finder as $file) {
            $filesystem = new Filesystem();
            $filesystem->remove($file->getPathname());
        }
    }
}