<?php


namespace App\Service;

/**
 * Class SMSManager
 * @package App\Service
 */
class SMSManager
{
    private $SMSService;

    /**
     * SMSManager constructor.
     * @param SMSInterface $SMSService
     */
    public function __construct(SMSInterface $SMSService)
    {
        $this->SMSService = $SMSService;
    }

    /**
     * @return bool
     */
    public function send(): bool
    {
        return $this->SMSService->send();
    }

    /**
     * @param array $recipients
     */
    public function setRecipients(array $recipients): void
    {
        $this->SMSService->setRecipients($recipients);
    }

    /**
     * @param string $number
     */
    public function addRecipient(string $number): void
    {
        $this->SMSService->addRecipient($number);
    }

    /**
     * @param string $number
     */
    public function removeRecipient(string $number): void
    {
        $this->SMSService->removeRecipient($number);
    }

    /**
     * @return array
     */
    public function getRecipients():array
    {
        return $this->SMSService->getRecipients();
    }
}