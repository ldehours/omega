<?php


namespace App\Service;


use App\Utils\PHPHelper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class DatabaseManager
{
    /**
     * @var Collection|Database[]
     */
    private $databases;

    /**
     * DatabaseManager constructor.
     * @param Database[] $databases
     */
    public function __construct(array $databases)
    {
        $this->databases = new ArrayCollection($databases);
    }

    /**
     * @param Database $database
     * @return DatabaseManager
     */
    public function addDatabase(Database $database): self
    {
        if (!$this->databases->contains($database)) {
            $this->databases[] = $database;
        }

        return $this;
    }

    /**
     * @param Database $database
     * @return DatabaseManager
     */
    public function removeDatabase(Database $database): self
    {
        if ($this->databases->contains($database)) {
            $this->databases->removeElement($database);
        }

        return $this;
    }

    /**
     * @return Collection|Database[]
     */
    public function getDatabases(): Collection
    {
        return $this->databases;
    }

    /**
     * @param string $query
     * @param bool $isRollback
     * @return array of DatabaseResult
     */
    public function query(string $query, bool $isRollback = true): array
    {
        $results = array();
        foreach ($this->databases as $database) {
            $results[] = $database->query($query, $isRollback);
        }
        return $results;
    }

    /**
     * Check if the number of elements in an array is $operator than/to $expectedNumberLines
     * @param DatabaseResult[] $databaseResults
     * @param int $expectedNumberLines
     * @param string $operator
     * @return bool
     */
    public static function areExpectedNumberLines(
        array $databaseResults,
        int $expectedNumberLines,
        string $operator
    ): bool {
        foreach ($databaseResults as $result) {
            if (!PHPHelper::condition($result->getNumberLines(), $operator, $expectedNumberLines)) {
                return false;
            }
        }
        return true;
    }

}