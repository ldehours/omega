<?php


namespace App\Service;


use App\Entity\Core\Staff;
use App\Entity\Core\TimeRange;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SubstitutionDuplicaterService
 * @package App\Service
 */
class SubstitutionDuplicaterService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SubstitutionDuplicaterService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Wish $wish
     * @param Staff $user
     * @param bool $createFromModel
     * @return Wish
     * @throws \Exception
     */
    public function duplicateWish(
        Wish $wish,
        Staff $user,
        bool $createFromModel = false
    ){
        $newWish = clone $wish;
        $timeRange = $wish->getTimeRange();
        if (isset($timeRange)) {
            $newTimeRange = clone $wish->getTimeRange();
        } else {
            $newTimeRange = new TimeRange();
            $newTimeRange->setStartDate(new DateTime());
            $newTimeRange->setEndDate((new DateTime())->add(new DateInterval('P1Y')));
            $newTimeRange->setAgenda($wish->getPerson()->getAgenda());
            $newTimeRange->setNature(TimeRange::NATURE_SEARCH);
            $newTimeRange->setCreator($user);
        }
        $newWish->setTimeRange($newTimeRange);
        $newWish->setLastUpdateDate(new DateTime());
        if($createFromModel){
            $newWish->setIsModel(false);
        }
        $newWish->setCreationDate(new DateTimeImmutable());
        foreach ($wish->getSubstitutionCriteria() as $substitutionCriterion) {
            $newSubstitutionCriterion = clone $substitutionCriterion;
            $newSubstitutionCriterion->setSubstitution($newWish);
            $newWish->addSubstitutionCriterion($newSubstitutionCriterion);
            $this->entityManager->persist($newSubstitutionCriterion);
        }
        $this->entityManager->persist($newTimeRange);
        $this->entityManager->persist($newWish);
        $this->entityManager->flush();

        return $newWish;
    }

    /**
     * @param Offer $offer
     * @param Staff $user
     * @param bool $createFromModel
     * @return Offer
     */
    public function duplicateOffer(
        Offer $offer,
        Staff $user,
        bool $createFromModel = false
    ){
        $newOffer = clone $offer;
        $newOffer->setCreator($user);
        $newOffer->setCreationDate(new DateTimeImmutable());
        $newOffer->setLastUpdateDate(new DateTime());
        foreach ($offer->getSubstitutionCriteria() as $substitutionCriterion) {
            $newSubstitutionCriterion = clone $substitutionCriterion;
            $newSubstitutionCriterion->setSubstitution($newOffer);
            $newOffer->addSubstitutionCriterion($newSubstitutionCriterion);
            $this->entityManager->persist($newSubstitutionCriterion);
        }
        if($createFromModel){
            $newOffer->setIsModel(false);
        }
        $this->entityManager->persist($newOffer);
        $this->entityManager->flush();

        return $newOffer;
    }

}