<?php


namespace App\Service;

/**
 * Get Results from a database
 * Class Result
 * @package App\Service
 */
class Result
{

    /**
     * If the query succeed
     * @var bool
     */
    private $success;

    /**
     * error message
     * @var string
     */
    private $message;

    /**
     * Rows returned
     * @var array
     */
    private $data;


    public function __construct(bool $success, string $message, array $data = [])
    {
        $this->success = $success;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return Result
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Result
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return Result
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

}