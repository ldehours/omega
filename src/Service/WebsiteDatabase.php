<?php


namespace App\Service;


/**
 * Class WebsiteDatabase
 * @package App\Service
 */
class WebsiteDatabase implements Database
{
    /**
     * @var \PDO $database
     */
    private $database;

    /**
     * Construct a database connection with PDO
     * SiteDatabase constructor.
     * @param bool $useDistDatabase
     * @param bool $isTestDatabase
     */
    public function __construct(bool $useDistDatabase = false, bool $isTestDatabase = false)
    {
        $this->database = $instance ?? new \PDO($_ENV['DATABASE_SITE_URL'], $_ENV['DATABASE_SITE_USERNAME'],
                $_ENV['DATABASE_SITE_PASSWORD']);

        if ($useDistDatabase) {
            $this->database = $instance ?? new \PDO($_ENV['DATABASE_REMOTE_SITE_URL'],
                    $_ENV['DATABASE_REMOTE_SITE_USERNAME'],
                    $_ENV['DATABASE_REMOTE_SITE_PASSWORD']);
        }

        // Convert database to InnoDB
        if ($isTestDatabase && !$useDistDatabase) {
            self::convertDatabase($this->database);
        }
    }

    /**
     * Return the result in an array of objects for Select query
     * else returns the number of modified rows
     * @param string $query
     * @param bool $isRollback
     * @return DatabaseResult
     */
    public function query(string $query, bool $isRollback = true): DatabaseResult
    {
        $this->database->beginTransaction();

        $firstWord = explode(' ', trim(strtolower($query)))[0];

        if ($firstWord == "select") {
            $result = array();
            $query = $this->database->query($query);
            if ($query) {
                $result = $query->fetchAll(\PDO::FETCH_OBJ);
            }
            $databaseResult = new DatabaseResult(count($result), $result,
                self::PDOErrorToDatabaseErrorInfo($this->database->errorInfo()));
        } else {
            $result = $this->database->exec($query);
            $databaseResult = new DatabaseResult($result, array(),
                self::PDOErrorToDatabaseErrorInfo($this->database->errorInfo()));
        }

        if ($isRollback) {
            $this->database->rollBack();
        } else {
            $this->database->commit();
        }

        return $databaseResult;
    }

    /**
     * convert a database to another STORAGE_ENGINE
     * You can see the different engine with SHOW ENGINES;
     * Possible value available : InnoDB, MyISAM, MRG_MYISAM, MEMORY, BLACKHOLE, CSV, ARCHIVE, PERFORMANCE_SCHEMA, FEDERATED
     * @param \PDO $database
     * @param bool $debug
     * @param string $from
     * @param string $to
     */
    public static function convertDatabase(
        \PDO $database,
        $debug = false,
        string $from = 'MyISAM',
        string $to = 'INNODB'
    ): void
    {
        $result = $database->query("
            SELECT TABLE_NAME
            FROM information_schema.TABLES
            WHERE TABLE_SCHEMA = 'media_sante'
            AND ENGINE = '$from'
        ");

        foreach ($result as $row) {
            set_time_limit(600);
            $success = $database->exec("ALTER TABLE {$row['TABLE_NAME']} ENGINE = $to");
            if (!$success && $debug) {
                $info = $database->errorInfo();
                dump("{$row['TABLE_NAME']} - failed: $info[2]\n");
            }
        }
    }

    /**
     * @param array $PDOError
     * @return DatabaseErrorInfo|null
     */
    private static function PDOErrorToDatabaseErrorInfo(array $PDOError): ?DatabaseErrorInfo
    {
        return $PDOError[2] != null ? new DatabaseErrorInfo(intval($PDOError[0]), $PDOError[2]) : null;
    }
}