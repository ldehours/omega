<?php

namespace App\Service\Historization;

use App\Entity\CFML\MembershipCfml;
use App\Entity\CFML\MembershipCfmlHistorization;
use App\Service\Historization\AbstractHistorizationManager;
use App\Service\Historization\HistorizableInterface;
use App\Service\Historization\HistorizationManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MembershipCfmlServiceHistorization
 * @package App\Service\Membership
 */
class MembershipCfmlHistorizationManager extends AbstractHistorizationManager implements HistorizationManagerInterface
{
    const OPTION_INITIAL_VALUES =
        [
            "member" => null,
            "representativeStaff" => null,
            "creator" => null,
            "date" => null,
            "field" => null
        ];

    /**
     * MembershipCfmlHistorizationManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param HistorizableInterface $member
     * @param array $arrayData
     * @return mixed|void
     */
    public function historize(HistorizableInterface $member, array $options = self::OPTION_INITIAL_VALUES): void
    {
        $options = array_merge(self::OPTION_INITIAL_VALUES, $options);

        $memberHistorization = new MembershipCfmlHistorization();
        $memberHistorization->setMembership($member);
        $memberHistorization->setDispatcher($options['creator']);
        $memberHistorization->setReceiver($options['representativeStaff']);
        $memberHistorization->setModifiedField($options['field']);
        $memberHistorization->setDateHistorize($options['date']);
        $this->addHistorizedItem($memberHistorization);
    }

    /**
     * @return mixed|void
     */
    public function save()
    {
        $this->saveHistorizedItems();
    }
}

