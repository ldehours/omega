<?php

namespace App\Service\Historization;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 *
 * Classe mère à faire étendre aux différents HistorisationManager
 * pour qu'ils aient ainsi les bases (manager pour insérer + une collection d'objets à historiser)
 *
 * Class AbstractHistorizationManager
 * @package App\Service\Historization
 */
abstract class AbstractHistorizationManager
{
    const OPTION_INITIAL_VALUES = [];

    /**
     * @var EntityManagerInterface
     */
    protected $manager;

    /**
     * @var ArrayCollection | HistorizationHandlerInterface[]
     */
    protected $historizedItems;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->historizedItems = new ArrayCollection();
    }

    /**
     * @param HistorizationHandlerInterface $historization
     * @return $this
     */
    protected function addHistorizedItem(HistorizationHandlerInterface $historization): self
    {
        if (!$this->historizedItems->contains($historization)) {
            $this->historizedItems->add($historization);
        }
        return $this;
    }

    /**
     * Method that can be implemented in children in order to store the historized items
     */
    protected function saveHistorizedItems()
    {
        foreach ($this->historizedItems as $historizedItem) {
            $this->manager->persist($historizedItem);
        }
        $this->manager->flush();
    }
}