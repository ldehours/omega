<?php


namespace App\Service\Historization;


/**
 *
 * Interface servant pour chaque HistorizationManager (ie : OfferHistorizationManager, etc.)
 * Elle fait implémenter les deux méthodes principales servant pour historiser nos entités.
 *
 * Interface HistorizationManagerInterface
 * @package App\Service\Historization
 */
interface HistorizationManagerInterface
{
    /**
     * @param HistorizableInterface $object
     * @param array $options
     * @return mixed
     */
    public function historize(HistorizableInterface $object, array $options = []);

    /**
     * @return mixed
     */
    public function save();
}