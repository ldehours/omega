<?php


namespace App\Service\Historization;

/**
 *
 * Interface à faire implémenter à toutes les entités d'historisation (OfferHistorization, StaffHistory, etc.)
 *
 * Interface HistorizationHandlerInterface
 * @package App\Service\Historization
 */
interface HistorizationHandlerInterface{}