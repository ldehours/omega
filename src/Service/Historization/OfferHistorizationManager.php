<?php

namespace App\Service\Historization;

use App\Entity\Offer\Offer;
use App\Entity\Offer\OfferHistorization;

/**
 *
 * HistorizationManager pour les Offres.
 *
 * Class OfferHistorizationManager
 * @package App\Service\Historization
 */
class OfferHistorizationManager extends AbstractHistorizationManager implements HistorizationManagerInterface
{
    const OPTION_INITIAL_VALUES = [
        'oldOffer' => null,
        'staff' => null,
    ];

    /**
     * @param HistorizableInterface $offer
     * @param array $options
     * @throws \Exception
     */
    public function historize(HistorizableInterface $offer, array $options = self::OPTION_INITIAL_VALUES)
    {
        $options = array_merge(self::OPTION_INITIAL_VALUES, $options);
        /** @var Offer $offer */

        $offerHistorization = new OfferHistorization();
        $now = new \DateTimeImmutable('now', new \DateTimeZone('Europe/Paris'));

        /** @var Offer $oldOffer */
        $oldOffer = $options['oldOffer'];
        $staff = $options['staff'];

        if ($oldOffer !== $offer) {
            $offerHistorization->setOldStatus($oldOffer->getState());
            $offerHistorization->setOldType($oldOffer->getType());
            $offerHistorization->setOffer($offer);
            $offerHistorization->setStaff($staff);
            $offerHistorization->setDateChange($now);
            $this->addHistorizedItem($offerHistorization);
        }
    }

    /**
     * @return mixed|void
     */
    public function save()
    {
        $this->saveHistorizedItems();
    }
}