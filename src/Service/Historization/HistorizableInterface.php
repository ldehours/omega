<?php


namespace App\Service\Historization;

/**
 *
 * Interface à faire implémenter à toutes les entités pouvant être historisées (Staff, Offer, etc.)
 *
 * Interface HistorizableInterface
 * @package App\Service\Historization
 */
interface HistorizableInterface{}