<?php

namespace App\Service\Historization;

use App\Entity\CFML\MembershipCfml;
use App\Entity\CFML\MembershipCfmlSpecificStatusHistorization;
use App\Repository\Core\StaffHistoryRepository;
use App\Service\Historization\AbstractHistorizationManager;
use App\Service\Historization\HistorizableInterface;
use App\Service\Historization\HistorizationManagerInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MembershipCfmlSpecificStatusHistorizationManager
 * @package App\Service\Membership
 */
class MembershipCfmlSpecificStatusHistorizationManager extends AbstractHistorizationManager implements HistorizationManagerInterface
{
    const OPTION_INITIAL_VALUES =
        [
            "member" => null,
            "creator" => null,
            "specificStatus" => null,
            "date" => null
        ];

    /**
     * MembershipCfmlHistorizationManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
    }

    /**
     * @param HistorizableInterface $member
     * @param array $options
     */
    public function historize(HistorizableInterface $member, array $options = self::OPTION_INITIAL_VALUES): void
    {
        $options = array_merge(self::OPTION_INITIAL_VALUES, $options);
        $isExistSpecficStatus = $member->getSpecificStatus();

        if (!$isExistSpecficStatus) {
            $isExistSpecficStatus = $options['specificStatus'];
        }

        $membershipCfmlSpecificStatusHistorization = new MembershipCfmlSpecificStatusHistorization();
        $membershipCfmlSpecificStatusHistorization->setMembership($member);
        $membershipCfmlSpecificStatusHistorization->setDispatcher($options['creator']);
        $membershipCfmlSpecificStatusHistorization->setOldSpecificStatus($isExistSpecficStatus);
        $membershipCfmlSpecificStatusHistorization->setDateHistorize($options['date']);
        $this->addHistorizedItem($membershipCfmlSpecificStatusHistorization);
    }

    /**
     * @return mixed|void
     */
    public function save()
    {
        $this->saveHistorizedItems();
    }
}

