<?php


namespace App\Service\Historization;

use App\Entity\Core\Staff;
use App\Entity\Core\StaffHistory;
use App\Repository\Core\StaffHistoryRepository;
use App\Repository\Core\StaffRepository;
use App\Utils\DateTime;
use Doctrine\ORM\EntityManagerInterface;

/**
 *
 * HistorizationManager pour les Staffs.
 *
 * Class StaffHistorizationManager
 * @package App\Service\Historization
 */
class StaffHistorizationManager extends AbstractHistorizationManager implements HistorizationManagerInterface
{
    const OPTION_INITIAL_VALUES = [
        'requestValues' => null,
        'description' => '',
        'user' => null,
        'action' => 'new'
    ];

    /**
     * @var StaffRepository
     */
    private $staffHistoryRepository;

    public function __construct(EntityManagerInterface $manager, StaffHistoryRepository $staffHistoryRepository)
    {
        parent::__construct($manager);
        $this->staffHistoryRepository = $staffHistoryRepository;
    }

    /**
     * @param HistorizableInterface $staffEdited
     * @param array $options
     * @return mixed|void
     * @throws \Exception
     */
    public function historize(HistorizableInterface $staffEdited, array $options = self::OPTION_INITIAL_VALUES)
    {
        $options = array_merge(self::OPTION_INITIAL_VALUES, $options);
        /** @var Staff $staffEdited */

        $staffPersonalPhones = [];
        $staffEditedRoles = $staffEdited->getRoles();
        $dateStart = $staffEdited->getDateStart();
        $dateEnd = $staffEdited->getDateEnd();
        $dateOfHiring = $staffEdited->getDateOfHiring();
        foreach ($staffEdited->getExternalPhones() as $phone) {
            if ($phone->getTranslate()->getLabel($phone->getPhoneType()) == "Personnel") {
                $staffPersonalPhones[] = $phone->getNumber();
            }
        }

        $staffHistory = new StaffHistory();
        $staffHistory->setDateOfSave(new DateTime());

        if ($options['action'] === "edit") {
            $dateStart = DateTime::createFromString($options['requestValues']['dateStart']);
            $dateEnd = DateTime::createFromString($options['requestValues']['dateEnd']);
            $dateOfHiring = DateTime::createFromString($options['requestValues']['dateOfHiring']);

            $staffStored = $this->staffHistoryRepository->findLastSave($staffEdited->getId());
            if ($staffStored === null) {
                $staffStored = new StaffHistory();
            }

            if ($staffStored->getNickname() !== $staffEdited->getNickname()) {
                $options['description'] .= 'modification du pseudo ' . $staffStored->getNickname() . ' en ' . $options['requestValues']['nickname'] . '<br>';
            }

            if ($staffStored->getFirstName() !== $staffEdited->getFirstName()) {
                $options['description'] .= 'modification du prenom ' . $staffStored->getFirstName() . ' en ' . $options['requestValues']['firstName'] . '<br>';
            }

            if ($staffStored->getLastName() !== $staffEdited->getLastName()) {
                $options['description'] .= 'modification du nom ' . $staffStored->getLastName() . ' en ' . $options['requestValues']['lastName'] . '<br>';
            }

            if ($staffStored->getRoles() !== $staffEdited->getRoles()) {
                if ($dateEnd == null) {
                    $staffStored->setDateEnd(new DateTime());
                }
                $options['description'] .= 'modification des rôles [';

                foreach ($staffStored->getRoles() as $role) {
                    $options['description'] .= ($role !== $staffStored->getRoles()[array_key_last($staffStored->getRoles())]) ? $role . ', ' : $role;
                }
                $options['description'] .= '] en [';
                foreach ($staffEditedRoles as $role) {
                    $options['description'] .= ($role !== $staffEditedRoles[array_key_last($staffEditedRoles)]) ? $role . ', ' : $role;
                }
                $options['description'] .= ']<br>';
            }

            $personalChange = false;
            foreach ($staffPersonalPhones as $staffPersonalPhone) {
                if (!in_array($staffPersonalPhone, $staffStored->getPersonalPhones(), true)) {
                    $personalChange = true;
                    break;
                }
            }

            if ($personalChange) {
                $options['description'] .= 'modification des téléphones personnels [';

                foreach ($staffStored->getPersonalPhones() as $phone) {
                    $options['description'] .= ($phone !== $staffEditedRoles[array_key_last($staffStored->getPersonalPhones())]) ? $phone . ', ' : $phone;
                }
                $options['description'] .= '] en [';
                foreach ($staffPersonalPhones as $phone) {
                    $options['description'] .= ($phone !== $staffEditedRoles[array_key_last($staffPersonalPhones)]) ? $phone . ', ' : $phone;
                }
                $options['description'] .= ']<br>';
            }

            if ($dateStart !== null && $staffStored->getDateStart() !== $dateStart) {
                $options['description'] .= sprintf(
                    'modification de la date de début de prise de poste "%s" en "%s"<br>',
                    ($staffStored->getDateStart() === null) ? 'aucune' : $staffStored->getDateStart()->format('d/m/Y H:i'),
                    $dateStart->format('d/m/Y H:i')
                );
            }

            if ($staffStored->getDateEnd() !== $dateEnd && $staffStored->getDateEnd() === null) {
                $options['description'] .= sprintf(
                    'modification de la date de fin de prise de poste "%s" en "%s"<br>',
                    ($staffStored->getDateEnd() === null) ? 'aucune' : $staffStored->getDateEnd()->format('d/m/Y H:i'),
                    $dateEnd->format('d/m/Y H:i')
                );
            }

            if ($dateOfHiring !== null && $staffStored->getDateOfHiring() !== $dateOfHiring) {
                $options['description'] .= sprintf(
                    'modification de la date d\'embauche "%s" en "%s"<br>',
                    ($staffStored->getDateOfHiring() === null) ? 'aucune' : $staffStored->getDateOfHiring()->format('d/m/Y H:i'),
                    $dateOfHiring->format('d/m/Y H:i')
                );
            }

            if ($staffStored->getJobTitle() !== $staffEdited->getJobTitle()) {
                $options['description'] .= sprintf(
                    'modification l\'intitulé du poste "%s" en "%s"<br>',
                    $staffStored->getJobTitle(),
                    $options['requestValues']['jobTitle']
                );
            }

            if ($staffStored->getIsActive() !== $staffEdited->getIsActive()) {
                $oldStatut = $staffStored->getIsActive() ? 'Inactive' : 'Active';
                $newStatut = $staffEdited->getIsActive() ? 'Inactive' : 'Active';
                $options['description'] .= 'modification du statut  "' . $oldStatut . '" en "' . $newStatut . '"<br>';
            }

            if ($staffStored->getGender() !== null && $staffStored->getGender() !== $staffEdited->getGender()) {
                $oldGender = Staff::LABEL[$staffStored->getGender()];
                $newGender = Staff::LABEL[$staffEdited->getGender()];
                $options['description'] .= 'modification du genre  "' . $oldGender . '" en "' . $newGender . '"<br>';
            }

            if ($staffStored->getIp() !== $staffEdited->getIp()) {
                $options['description'] .= 'modification de l\'adresse "' . $staffStored->getIp() . '" en "' . $options['requestValues']['ip'] . '"<br>';
            }

            if ($staffEdited->getService() !== null && $staffStored->getService() !== $staffEdited->getService()) {
                $options['description'] .= 'modification du prenom "' . $staffStored->getService() . '" en "' . $staffEdited->getService()->getName() . '"<br>';
            }

            if ($staffStored->getContract() !== $staffEdited->getContract()) {
                $options['description'] .= 'modification du contract de  "' . $staffStored->getContract() . 'h" en "' . $options['requestValues']['contract'] . 'h"<br>';
            }

            if ($staffStored->getInternalPhone() !== $staffEdited->getInternalPhone()) {
                $options['description'] .= 'modification du téléphone interne "' . $staffStored->getInternalPhone(
                    ) . '" en "' . $options['requestValues']['internalPhone'] . '"<br>';
            }

            if ($staffStored->getEmail() !== $staffEdited->getEmail()) {
                $options['description'] .= 'modification de l\'email "' . $staffStored->getEmail() . '" en "' . $options['requestValues']['email'] . '"<br>';
            }

            if ($options['description'] === '') {
                return "Aucune modification éffectuée";
            } else {
                $options['description'] = $options['user']->getUsername() . ' a effectué(e) le(s) modification(s) suivante(s) le ' . $staffHistory->getDateOfSave()->format(
                        'd/m/Y H:i'
                    ) . ':<br>' . $options['description'];
            }
        } else {
            $options['description'] = $options['user']->getUsername() . ' a créé le staff "' . $staffEdited->getNickname() . '" (' . $staffEdited->getLastName(
                ) . ' ' . $staffEdited->getFirstName() . ') le ' . $staffHistory->getDateOfSave()->format(
                    'd/m/Y H:i'
                ) . ', au poste de "' . $staffEdited->getJobTitle() . '"<br>';
        }

        $staffHistory->setStaffId($staffEdited->getId());
        $staffHistory->setNickname($staffEdited->getNickname());
        $staffHistory->setLastName($staffEdited->getLastName());
        $staffHistory->setFirstName($staffEdited->getFirstName());
        $staffHistory->setRoles($staffEdited->getRoles());
        $staffHistory->setJobTitle($staffEdited->getJobTitle());
        $staffHistory->setEmail($staffEdited->getEmail());
        $staffHistory->setInternalPhone($staffEdited->getInternalPhone());
        $staffHistory->setPersonalPhones($staffPersonalPhones);
        $staffHistory->setIp($staffEdited->getIp());
        $staffHistory->setIsActive($staffEdited->getIsActive());
        $staffHistory->setGender($staffEdited->getGender());
        $staffHistory->setService($staffEdited->getService()->getName());
        $staffHistory->setContract($staffEdited->getContract());
        $staffHistory->setDateStart($dateStart);
        $staffHistory->setDateEnd($dateEnd);
        $staffHistory->setDateOfHiring($dateOfHiring);
        $staffHistory->setCreator($options['user']->getNickName());
        $staffHistory->setDescription($options['description']);

        $this->addHistorizedItem($staffHistory);
    }

    /**
     * @return mixed|void
     */
    public function save()
    {
        $this->saveHistorizedItems();
    }
}