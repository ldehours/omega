<?php


namespace App\Service\AssociatedPersonAddress;


use App\Entity\Core\Agenda;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use App\Repository\Core\AddressRepository;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\OriginRepository;
use App\Repository\Core\PersonLegalRepository;
use App\Repository\Core\PersonNaturalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class AddressPersonManager
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var PersonNatural
     */
    private $personNatural;

    /**
     * @var DepartmentRepository
     */
    private $departmentRepository;

    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @var PersonNaturalRepository
     */
    private $personNaturalRepository;

    /**
     * @var OriginRepository
     */
    private $originRepository;

    /**
     * @var JobNameRepository
     */
    private $jobNameRepository;

    /**
     * @var PersonLegalRepository
     */
    private $personLegalRepository;

    public function __construct(
        EntityManagerInterface $manager,
        DepartmentRepository $departmentRepository,
        AddressRepository $addressRepository,
        PersonNaturalRepository $personNaturalRepository,
        OriginRepository $originRepository,
        JobNameRepository $jobNameRepository,
        PersonLegalRepository $personLegalRepository
    ) {
        $this->manager = $manager;
        $this->departmentRepository = $departmentRepository;
        $this->addressRepository = $addressRepository;
        $this->personNaturalRepository = $personNaturalRepository;
        $this->originRepository = $originRepository;
        $this->jobNameRepository = $jobNameRepository;
        $this->personLegalRepository = $personLegalRepository;
    }

    /**
     * The PersonNatural which will be used in the functions below
     * @param PersonNatural $personNatural
     */
    public function setPersonNatural(PersonNatural $personNatural): void
    {
        $this->personNatural = $personNatural;
    }

    /**
     * Combine the 3 functions below with a direct request handling
     * @param Request $request
     * @param Staff $staff
     */
    public function handleRequest(Request $request, Staff $staff): void
    {
        $personLegals = $request->request->get('personLegals') ?? [];
        $newPersonLegals = $request->request->get('newPersonLegals') ?? [];
        $this->handleExistingPersonLegal($personLegals);
        $this->handleNewPersonLegal($newPersonLegals, $staff);
        $this->manageDepartments();
    }

    /**
     * Manage the PersonNatural assignment to an existing PersonLegal
     * @param array $personLegalsInfos
     */
    public function handleExistingPersonLegal(array $personLegalsInfos): void
    {
        if (!empty($personLegalsInfos)) {
            foreach ($this->personNatural->getPersonLegals() as $personLegal) {
                if (count(
                        array_filter(
                            $personLegalsInfos,
                            function ($item) use ($personLegal) {
                                return array_key_exists('id', $item) && intval($item['id']) === $personLegal->getId();
                            }
                        )
                    ) === 0) {
                    $this->personNatural->removePersonLegal($personLegal);
                    $personLegal->removePersonNatural($this->personNatural);
                    $this->manager->persist($personLegal);
                }
            }
            foreach ($personLegalsInfos as $personLegalInfos) {
                if (!array_key_exists('id', $personLegalInfos)) {
                    continue;
                }
                $address = $this->addressRepository->find($personLegalInfos['idAddress']);
                $personLegal = $this->personLegalRepository->find($personLegalInfos['id']);
                $this->personNatural->addPersonLegal($personLegal);
                $personLegal->addAddress($address);
                $address->addPerson($personLegal);
                $personLegal->addPersonNatural($this->personNatural);
                $this->manager->persist($address);
                $this->manager->persist($personLegal);
            }
        } else {
            foreach ($this->personNatural->getPersonLegals() as $personLegal) {
                $this->personNatural->removePersonLegal($personLegal);
                $personLegal->removePersonNatural($this->personNatural);
                $this->manager->persist($personLegal);
            }
        }
    }

    /**
     * Manage the PersonLegal creation from a personNatural address
     * @param array $newPersonLegals
     * @param Staff $staff
     */
    public function handleNewPersonLegal(array $newPersonLegals, Staff $staff): void
    {
        if (!empty($newPersonLegals)) {
            foreach ($newPersonLegals as $newPersonLegalInfos) {
                if (array_key_exists('create', $newPersonLegalInfos) && $newPersonLegalInfos['create'] === 'on') {
                    $personLegal = new PersonLegal();
                    $agenda = new Agenda();
                    $address = $this->addressRepository->find($newPersonLegalInfos['idAddress']);
                    $address->addPerson($personLegal);

                    /** @var PersonNatural $personNaturalFound */
                    $personNaturalFound = $this->personNaturalRepository->find($newPersonLegalInfos['personNatural']);
                    $newPersonLegalCorporateName = (empty($newPersonLegalInfos['corporateName'])) ?
                        "cabinet (" . $personNaturalFound->getName() . "-" . $this->personNatural->getName() . ")" :
                        $newPersonLegalInfos['corporateName'];
                    $personLegal->setAgenda($agenda);
                    $personLegal->setCorporateName($newPersonLegalCorporateName);
                    $personLegal->setOrigin($this->originRepository->findOneBy(['label' => 'Automatique']));
                    $personLegal->setMainJobName($this->jobNameRepository->findOneBy(['label' => 'Cabinet de groupe']));
                    $personLegal->setCreator($staff);
                    $personLegal->addAddress($address);
                    $personLegal->addPersonNatural($personNaturalFound);
                    $personLegal->addPersonNatural($this->personNatural);
                    $this->personNatural->addPersonLegal($personLegal);
                    $personNaturalFound->addPersonLegal($personLegal);
                    $this->manager->persist($address);
                    $this->manager->persist($personLegal);
                }
            }
        }
    }

    /**
     * Set the right department to the current PersonNatural
     */
    public function manageDepartments(): void
    {
        $personNaturalAddresses = $this->personNatural->getAddresses();
        if ($personNaturalAddresses != null) {
            foreach ($personNaturalAddresses as $address) {
                if ($address->getCountryCode() != 'FR') {
                    $address->setDepartment($this->departmentRepository->findOneBy(['code' => '99']));
                } else {
                    $departmentCode = substr($address->getPostalCode(), 0, 2);
                    $department = $this->departmentRepository->findOneBy(['code' => $departmentCode]) ??
                        $this->departmentRepository->findOneBy(
                            [
                                'code' => substr($address->getPostalCode(), 0, 3)
                            ]
                        );
                    $address->setDepartment($department);
                }
            }
        }
    }
}