<?php

namespace App\Service\Membership;

use App\Entity\CFML\MembershipCfml;
use App\Repository\CFML\MembershipRepository;
use App\Repository\Core\StaffRepository;
use App\Service\Historization\MembershipCfmlHistorizationManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MembershipCfmlService
 * @package App\Service\Membership
 */
class MembershipCfmlService
{
    /**
     * @var array
     */
    private $arrayData = [];
    /**
     * @var MembershipRepository
     */
    private $membershipRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MembershipCfmlHistorizationManager
     */
    private $membershipCfmlHistorizationService;
    /**
     * @var MembershipCfml
     */
    private $member;
    /**
     * @var StaffRepository
     */
    private $staffRepository;

    /**
     * MembershipCfmlService constructor.
     * @param MembershipRepository $membershipRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        MembershipRepository $membershipRepository,
        EntityManagerInterface $entityManager,
        MembershipCfmlHistorizationManager $membershipCfmlHistorizationService,
        StaffRepository $staffRepository
    )
    {
        $this->membershipRepository = $membershipRepository;
        $this->entityManager = $entityManager;
        $this->membershipCfmlHistorizationService = $membershipCfmlHistorizationService;
        $this->staffRepository = $staffRepository;
    }

    /**
     * @param array $arrayData
     */
    public function manageMember(array $arrayData): void
    {
        $this->setArrayData($arrayData);
        // check si le membre exist dans la table membership_cfml
        $memberRepository = $this->membershipRepository->findOneBy(['member' => $arrayData['member']]);
        // update
        if ($memberRepository) {
            $this->update($memberRepository);
        } else {
            // add
            $this->add();
        }
    }

    private function add(): void
    {
        $newMembershipCFML = new MembershipCfml();
        $this->setDataToMembership($newMembershipCFML);
        $this->entityManager->persist($newMembershipCFML);
        $this->entityManager->flush();
        $this->member = $newMembershipCFML;
    }

    /**
     * @param MembershipCfml $membre
     */
    private function update(MembershipCfml $membre): void
    {
        $this->setDataToMembership($membre);
        $this->entityManager->flush();
        $this->member = $membre;
    }

    /**
     * @param MembershipCfml $object
     */
    private function setDataToMembership(MembershipCfml $object): void
    {
        if (!empty($this->arrayData['member'])) {
            $object->setMember($this->arrayData['member']);
        }

        if (!empty($this->arrayData['representativeStaff'])) {
            $this->addToRepresentativeStaff($object);
        }

        if (!empty($this->arrayData['creator'])) {
            $object->setCreator($this->arrayData['creator']);
        }

        if (!empty($this->arrayData['date'])) {
            $object->setCreationDate($this->arrayData['date']);
        }

        if (!empty($this->arrayData['specificStatus'])) {
            $object->setSpecificStatus($this->arrayData['specificStatus']);
        }
    }

    /**
     * @param MembershipCfml $object
     */
    private function addToRepresentativeStaff(MembershipCfml $object): void
    {

        $representativeStaff = $this->arrayData['representativeStaff'];

        $hasRoleRA = $this->staffRepository->staffHasRole(['ROLE_CFML_TECHNIQUE'], $this->arrayData['representativeStaff']);
        $hasRoleRT = $this->staffRepository->staffHasRole(['ROLE_CFML_ADMINISTRATIF'], $this->arrayData['representativeStaff']);

        if ($hasRoleRA) {
            $object->setMainRepresentative($representativeStaff);
            $this->arrayData['field'] = "RA";
        }

        if ($hasRoleRT) {
            $object->setSecondaryRepresentative($representativeStaff);
            $this->arrayData['field'] = "RT";
        }
    }

    /**
     * @return array
     */
    public function getArrayData(): array
    {
        return $this->arrayData;
    }

    /**
     * @param array $arrayData
     */
    public function setArrayData(array $arrayData): void
    {
        $this->arrayData = $arrayData;
    }

    /**
     * @return MembershipCfml
     */
    public function getMember(): MembershipCfml
    {
        return $this->member;
    }

    /**
     * @param MembershipCfml $member
     */
    public function setMember(MembershipCfml $member): void
    {
        $this->member = $member;
    }
}

