<?php


namespace App\Service\Uniqueness;


use App\Entity\Core\PersonNatural;
use App\Entity\Core\Telephone;
use App\Repository\Core\MailRepository;
use App\Repository\Core\PersonNaturalRepository;
use App\Repository\Core\TelephoneRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PersonNaturalUniqueness
{
    /**
     * @var PersonNaturalRepository
     */
    private $personNaturalRepository;
    /**
     * @var MailRepository
     */
    private $mailRepository;
    /**
     * @var TelephoneRepository
     */
    private $telephoneRepository;

    public function __construct(
        PersonNaturalRepository $personNaturalRepository,
        MailRepository $mailRepository,
        TelephoneRepository $telephoneRepository
    ){
        $this->personNaturalRepository = $personNaturalRepository;
        $this->mailRepository = $mailRepository;
        $this->telephoneRepository = $telephoneRepository;
    }

    /**
     * @param Request $request
     * @return array|array[]|bool[]
     * @throws \Exception
     */
    public function checkPersonUniquenessFromRequest(Request $request):array
    {
        $mail = $request->get('mail');
        $phoneNumber = $request->get('phone_number');
        $lastName = $request->get('last_name');
        $firstName = $request->get('first_name');
        if ($mail !== null) {
            return $this->checkPersonMailUniqueness($mail,(int)$request->get('id'));
        } elseif ($phoneNumber !== null) {
            return $this->checkPersonTelephoneUniqueness($phoneNumber);
        } elseif ($lastName !== null && $firstName !== null) {
            return $this->checkPersonNameUniqueness($lastName,$firstName);
        }
        return ['result' => false];
    }

    /**
     * @param string $mail
     * @param int $personId
     * @return array|array[]|bool[]
     * @throws \Exception
     */
    public function checkPersonMailUniqueness(string $mail, int $personId = 0):array
    {
        $similarPersons = [];
        $matchingMail = $this->mailRepository->findBy(['address' => $mail]);
        if (empty($matchingMail)) {
            return ['result' => false];
        }
        foreach ($matchingMail as $mail) {
            foreach ($mail->getPersons() as $person) {
                if ($person instanceof PersonNatural && $person->getId() !== $personId) {
                    $address = $person->getDefaultAddress();
                    $similarPersons[] = [
                        'id' => $person->getId(),
                        'lastName' => $person->getLastName(),
                        'firstName' => $person->getFirstName(),
                        'postalCode' => ($address !== null ? $address->getPostalCode() : ''),
                        'city' => ($address !== null ? $address->getLocality() : '')
                    ];
                }
            }
        }
        if (empty($similarPersons)) {
            return ['result' => false];
        }
        return ['result' => $similarPersons];
    }

    /**
     * @param string $telephone
     * @return array|array[]|bool[]
     * @throws \Exception
     */
    public function checkPersonTelephoneUniqueness(string $telephone):array
    {
        $similarPersons = [];
        $matchingPhones = $this->telephoneRepository->findBy(['number' => $telephone]);
        if (empty($matchingPhones)) {
            return ['result' => false];
        }

        foreach ($matchingPhones as $phone) {
            /** @var PersonNatural $person */
            foreach ($phone->getPersons() as $person) {
                if ($person instanceof PersonNatural) {
                    $address = $person->getDefaultAddress();
                    $similarPersons[] = [
                        'id' => $person->getId(),
                        'lastName' => $person->getLastName(),
                        'firstName' => $person->getFirstName(),
                        'postalCode' => ($address !== null ? $address->getPostalCode() : ''),
                        'city' => ($address !== null ? $address->getLocality() : '')
                    ];
                }
            }
        }

        if (empty($similarPersons)) {
            return ['result' => false];
        }
        return ['result' => $similarPersons];
    }

    /**
     * @param string $lastName
     * @param string $firstName
     * @return array[]|bool[]
     * @throws \Exception
     */
    public function checkPersonNameUniqueness(string $lastName, string $firstName)
    {
        $similarPersons = [];
        $matchingPersonNaturals = $this->personNaturalRepository->findBy(
            [
                'lastName' => $lastName,
                'firstName' => $firstName
            ]
        );
        if(empty($matchingPersonNaturals)){
            return ['result' => false];
        }

        for ($i = 0, $iMax = count($matchingPersonNaturals); $i < $iMax; $i++) {
            $address = $matchingPersonNaturals[$i]->getDefaultAddress();
            $similarPersons[$i] = [
                'id' => $matchingPersonNaturals[$i]->getId(),
                'last_name' => $matchingPersonNaturals[$i]->getLastName(),
                'first_name' => $matchingPersonNaturals[$i]->getFirstName(),
                'postalCode' => ($address !== null ? $address->getPostalCode() : ''),
                'city' => ($address !== null ? $address->getLocality() : '')
            ];
            foreach ($matchingPersonNaturals[$i]->getMails() as $mail) {
                $similarPersons[$i]['mails'][] = $mail->getAddress();
            }
            foreach ($matchingPersonNaturals[$i]->getTelephones() as $telephone) {
                $similarPersons[$i]['phone_numbers'][] = $telephone->getNumber();
            }
        }

        if (empty($similarPersons)) {
            return ['result' => false];
        }
        return ['result' => $similarPersons];
    }
}