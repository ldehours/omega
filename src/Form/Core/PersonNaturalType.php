<?php

namespace App\Form\Core;

use App\Entity\Core\JobName;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\Origin;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Service;
use App\Entity\Core\Staff;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\MedicalSpecialtyRepository;
use App\Repository\Core\OriginRepository;
use App\Repository\Core\StaffRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class PersonNaturalType
 * @package App\Form\Core
 */
class PersonNaturalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $personNatural = $options['data'];

        // Création
        // On ne va chercher que les origines active pour les personnes
        if ($personNatural->getId() === null) {
            $originOption = [
                'class' => Origin::class,
                'query_builder' => static function (
                    OriginRepository $originRepository
                ) {
                    return $originRepository->findOriginByUsage(Origin::PERSON);
                },
                'attr' => ['required' => 'required']
            ];
            //Edition
            // On laisse accessible toute les origines à l'édition car elles ne sont pas visible mais sont nécessaire à la mise à jour
        } else {
            $originOption = [
                'class' => Origin::class,
                'attr' => ['required' => 'required']
            ];
        }

        $builder
            ->add(
                'personStatus',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(PersonNatural::LABEL), PersonNatural::STATUS),
                    'attr' => ['required' => 'required']
                ]
            )
            ->add(
                'mainJobName',
                EntityType::class,
                [
                    'class' => JobName::class,
                    'query_builder' => function (JobNameRepository $jobNameRepository) {
                        return $jobNameRepository->createQueryBuilder('j')->where(
                            'j.classification =' . JobName::BASED . ' or j.classification =' . JobName::SUBSTITUTE . ' or j.classification =' . JobName::INSTITUTION_FUNCTION
                        );
                    },
                    'attr' => ['required' => 'required']
                ]
            )
            ->add('origin', EntityType::class, $originOption)
            ->add(
                'gender',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(PersonNatural::LABEL), PersonNatural::GENDER),
                    'attr' => ['required' => 'required']
                ]
            )
            ->add('lastName')
            ->add('firstName')
            ->add(
                'addresses',
                CollectionType::class,
                [
                    'entry_type' => AddressType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'phone'],
                    ],
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrencollection',
                    ],
                ]
            )
            ->add(
                'mails',
                CollectionType::class,
                [
                    'entry_type' => MailType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'phone'],
                    ],
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrencollection',
                    ],
                ]
            )
            ->add(
                'telephones',
                CollectionType::class,
                [
                    'entry_type' => TelephoneType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'phone'],
                    ],
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrencollection',
                    ],
                ]
            )
            ->add(
                'services',
                EntityType::class,
                [
                    'class' => Service::class,
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add(
                'advisor',
                EntityType::class,
                [
                    'class' => Staff::class,
                    'query_builder' => function (
                        StaffRepository $staffRepository
                    ) {
                        return $staffRepository->createQueryBuilder('m')
                            ->andWhere("m.isActive = true")
                            ->orderBy('m.lastName', 'ASC');
                    },
                    'required' => true
                ]
            )
            ->add(
                'birthDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'required' => false,
                ]
            )
            ->add(
                'specialty',
                EntityType::class,
                [
                    'class' => MedicalSpecialty::class,
                    'query_builder' => function (
                        MedicalSpecialtyRepository $medicalSpecialtyRepository
                    ) {
                        return $medicalSpecialtyRepository->createQueryBuilder('m')->join(
                            'm.relatedType',
                            'q'
                        )->andWhere("q.label = 'Spécialité'");
                    },
                    'multiple' => false,
                    'required' => false
                ]
            )
            ->add(
                'skills',
                EntityType::class,
                [
                    'class' => MedicalSpecialty::class,
                    'query_builder' => function (
                        MedicalSpecialtyRepository $medicalSpecialtyRepository
                    ) {
                        return $medicalSpecialtyRepository->createQueryBuilder('m')->leftJoin(
                            'm.relatedType',
                            'q'
                        )->andWhere("q.label = 'Compétence'");
                    },
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add(
                'formations',
                EntityType::class,
                [
                    'class' => MedicalSpecialty::class,
                    'query_builder' => function (
                        MedicalSpecialtyRepository $medicalSpecialtyRepository
                    ) {
                        return $medicalSpecialtyRepository->createQueryBuilder('m')->join(
                            'm.relatedType',
                            'q'
                        )->andWhere("q.label = 'Formation'");
                    },
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add(
                'capacities',
                EntityType::class,
                [
                    'class' => MedicalSpecialty::class,
                    'query_builder' => function (
                        MedicalSpecialtyRepository $medicalSpecialtyRepository
                    ) {
                        return $medicalSpecialtyRepository->createQueryBuilder('m')->join(
                            'm.relatedType',
                            'q'
                        )->andWhere("q.label = 'Capacité'");
                    },
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add(
                'orientations',
                EntityType::class,
                [
                    'class' => MedicalSpecialty::class,
                    'query_builder' => function (
                        MedicalSpecialtyRepository $medicalSpecialtyRepository
                    ) {
                        return $medicalSpecialtyRepository->createQueryBuilder('m')->join(
                            'm.relatedType',
                            'q'
                        )->andWhere("q.label = 'Expérience'");
                    },
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add(
                'discussions',
                CollectionType::class,
                [
                    'entry_type' => DiscussionType::class,
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrencollection',
                    ],
                ]
            )
            ->add(
                'jobStatus',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(PersonNatural::LABEL), PersonNatural::JOB_STATUS),
                    'required' => false,
                    'multiple' => true
                ]
            )
            ->add(
                'diploma',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(PersonNatural::LABEL), PersonNatural::DIPLOMA),
                    'required' => false
                ]
            )
            ->add(
                'yearsExperience',
                IntegerType::class,
                [
                    'attr' => ['min' => 0, 'max' => 99],
                    'required' => false
                ]
            )
            ->add('RPPSNumber')
            ->add('additionalInformation', NoteInformationType::class, ['required' => false, 'by_reference' => false])
            ->add('personLegals')
            ->add('submit', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => PersonNatural::class,
                'cascade_validation' => true
            ]
        );
    }
}