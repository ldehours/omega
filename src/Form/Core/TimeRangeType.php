<?php

namespace App\Form\Core;

use App\Entity\Core\TimeRange;
use App\Utils\DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $startDate = new DateTime();
        $endDate = new DateTime();
        if ($options['data'] instanceof TimeRange) {
            if ($options['data']->getStartDate() !== null) {
                $startDate = $options['data']->getStartDate();
            }
            if ($options['data']->getEndDate() !== null) {
                $endDate = $options['data']->getEndDate();
            }
        }
        $builder
            ->add(
                'nature',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(TimeRange::LABEL), $options['natures']),
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control selectpicker'
                    ]
                ]
            )
            ->add(
                'startDate',
                DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'format' => 'd/m/Y H:i',
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control datetimepicker',
                        'value' => $startDate->format('d/m/Y H:i')
                    ],
                ]
            )
            ->add(
                'endDate',
                DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'format' => 'd/m/Y H:i',
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control datetimepicker',
                        'value' => $endDate->format('d/m/Y H:i')
                    ],
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => TimeRange::class,
                'natures' => []
            ]
        );
    }
}