<?php

namespace App\Form\Core;

use App\Entity\Core\TimeRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class TimeRangeAjaxType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $natures = TimeRange::NATURES_STAFF_ALARM;
        if(!$this->security->isGranted("ROLE_CHARGE_DE_RECRUTEMENT") && !$this->security->isGranted("ROLE_COMMERCIAL")){
            $natures = [TimeRange::NATURE_APPOINTMENT_CUSTOMER];
        }
        $builder
            ->add(
                'nature',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(TimeRange::LABEL), $natures),
                    'attr' => ['required' => 'required']
                ]
            )
            ->add(
                'startDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => [
                        'required' => 'required'
                    ],
                ]
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => [
                        'required' => 'required'
                    ],
                ]
            )
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => TimeRange::class,
            ]
        );
    }
}