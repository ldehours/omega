<?php


namespace App\Form\Core;


use App\Entity\Core\Mail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('address')
            ->add('mailType', ChoiceType::class, [
                'choices' => array_flip(Mail::LABEL),
                'attr' => ['required' => 'required']
            ])
            ->add('isDefault');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mail::class,
        ]);
    }

}