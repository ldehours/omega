<?php


namespace App\Form\Core;


use App\Entity\Core\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('domiciliations', ChoiceType::class, [
                'choices' => array_intersect(array_flip(Address::LABEL), Address::TYPEADRESSFORM),
                'multiple' => true,
                'attr' => ['required' => 'required']
            ])
            ->add('usages', ChoiceType::class, [
                'choices' => array_intersect(array_flip(Address::LABEL), Address::USAGES),
                'multiple' => true,
                'attr' => ['required' => 'required']
            ])
            ->add('isCedex')
            ->add('countryCode', CountryType::class,
                ['preferred_choices' => ['FR'], 'attr' => ['required' => 'required']])
            ->add('administrativeArea')
            ->add('locality')
            ->add('dependentLocality')
            ->add('postalCode')
            ->add('sortingCode')
            ->add('addressLine1')
            ->add('addressLine2', TextType::class, ['required' => false])
            ->add('locale');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }

}