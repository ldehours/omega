<?php

namespace App\Form\Core;

use App\Entity\Core\JobName;
use App\Entity\Core\Origin;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Service;
use App\Repository\Core\OriginRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonLegalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $jobNames = $options['jobNames'] ?? null;
        $mainJobNameChoices = $options['mainJobName'] ?? null;

        if ($jobNames === null || $mainJobNameChoices === null) {
            throw new InvalidArgumentException("Missing parameters");
        }

        $builder
            ->add(
                'personStatus',
                ChoiceType::class,
                [
                    'choices' => array_flip(PersonLegal::LABEL),
                    'attr' => ['required' => 'required']
                ]
            )
            ->add(
                'mainJobName',
                EntityType::class,
                [
                    'class' => JobName::class,
                    'choices' => $jobNames,
                    'required' => true
                ]
            )
            ->add(
                'jobNames',
                EntityType::class,
                [
                    'class' => JobName::class,
                    'choices' => $mainJobNameChoices,
                    'required' => false,
                    'multiple' => true
                ]
            )
            ->add(
                'origin',
                EntityType::class,
                [
                    'class' => Origin::class,
                    'attr' => ['required' => 'required'],
                    'query_builder' => static function (OriginRepository $originRepository) {
                        return $originRepository->findOriginByUsage(Origin::PERSON);
                    }
                ]
            )
            ->add('corporateName')
            ->add(
                'addresses',
                CollectionType::class,
                [
                    'entry_type' => AddressType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'phone'],
                    ],
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrenCollection',
                    ],
                ]
            )
            ->add(
                'mails',
                CollectionType::class,
                [
                    'entry_type' => MailType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'phone'],
                    ],
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrenCollection',
                    ],
                ]
            )
            ->add(
                'telephones',
                CollectionType::class,
                [
                    'entry_type' => TelephoneType::class,
                    'entry_options' => [
                        'attr' => ['class' => 'phone'],
                    ],
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'attr' => [
                        'data-provide' => 'childrenCollection',
                    ],
                ]
            )
            ->add(
                'services',
                EntityType::class,
                [
                    'class' => Service::class,
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add(
                'personNaturals',
                EntityType::class,
                [
                    'class' => PersonNatural::class,
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add('additionalInformation', NoteInformationType::class, ['required' => false, 'by_reference' => false])
            ->add('submit', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => PersonLegal::class,
                'cascade_validation' => true,
                'personNaturals' => [],
                'jobNames' => null,
                'mainJobName' => null
            ]
        );
    }
}
