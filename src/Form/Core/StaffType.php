<?php

namespace App\Form\Core;

use App\Entity\Core\Role;
use App\Entity\Core\Service;
use App\Entity\Core\Staff;
use App\Utils\DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StaffType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $staff = $options['data'];

        $startDate = ($staff->getId() == null) ? new DateTime() : $staff->getDateStart();
        $hiringDate = ($staff->getId() == null) ? new DateTime() : $staff->getDateOfHiring();
        $endDate = ($staff->getDateEnd() == null) ? null : $staff->getDateEnd()->format('d/m/Y H:i');

        $builder
            ->add('photo', FileType::class, [
                'mapped' => false,
                'required' => false
            ])
            ->add('roles', EntityType::class, [
                'class' => Role::class,
                'multiple' => true,
                'required' => true
            ])
            ->add(
                'gender',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(Staff::LABEL), Staff::GENDER),
                    'attr' => ['required' => 'required']
                ]
            )
            ->add('contract')
            ->add('nickname')
            ->add('password', TextType::class, ['help' => 'Mot de passe (6 caractères minimum)', 'required' => false])
            ->add('lastName')
            ->add('firstName')
            ->add('email')
            ->add('service', EntityType::class, [
                'class' => Service::class,
                'multiple' => false,
                'required' => true
            ])
            ->add('dateOfHiring', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'format' => 'd/m/Y H:i',
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control datetimepicker',
                        'value' => $hiringDate->format('d/m/Y H:i')
                    ],
                ])
            ->add('dateStart', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'format' => 'd/m/Y H:i',
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control datetimepicker',
                        'value' => $startDate->format('d/m/Y H:i')
                    ],
                ])
            ->add('dateEnd', DateTimeType::class,
                [
                    'widget' => 'single_text',
                    'format' => 'd/m/Y H:i',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control datetimepicker',
                        'value' => $endDate
                    ],
                ])
            ->add('jobTitle')
            ->add('internalPhone')
            ->add('externalPhones', CollectionType::class, [
                // each entry in the array will be an "email" field
                'entry_type' => TelephoneType::class,
                // these options are passed to each "email" type
                'entry_options' => [
                    'attr' => ['class' => 'phone'],
                ],
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ])
            ->add('ip')
            ->add('isActive')
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Staff::class
        ]);
    }
}
