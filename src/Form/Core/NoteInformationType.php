<?php

namespace App\Form\Core;


use App\Entity\Core\Note;
use App\Entity\Core\Staff;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class NoteInformationType extends AbstractType
{
    protected $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('noteType', ChoiceType::class, [
            'choices' => [Note::TYPE_INFORMATION],
            'data' => Note::TYPE_INFORMATION
        ])
            ->add('text', TextareaType::class, ['required' => false])
            ->add('creator', EntityType::class, [
                'class' => Staff::class,
                'data' => $this->tokenStorage->getToken()->getUser()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Note::class,
        ]);
    }
}
