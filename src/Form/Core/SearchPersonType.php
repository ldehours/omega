<?php


namespace App\Form\Core;


use App\Entity\Core\JobName;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchPersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', IntegerType::class, [
                'attr' => [
                    'min' => 1
                ],
                'required' => false
            ])
            ->add('classification', ChoiceType::class, [
                'required' => false,
                'multiple' => true,
                'attr' => [
                    'required' => false
                ],
                'choices' => array_flip(JobName::LABEL)
            ])
            ->add('lastName', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('firstName', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('corporateName', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('email', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('telephone', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('postalCode', TextType::class, [
                'attr' => [
                    'minlength' => 2
                ],
                'required' => false
            ])
            ->add('locality', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('exactResearch',CheckboxType::class,[
                'required' => false
            ])
            ->add('submit', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'cascade_validation' => true
        ]);
    }
}