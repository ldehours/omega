<?php


namespace App\Form\Core;


use App\Entity\Core\Telephone;
use App\Form\Core\DTO\TelephoneDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TelephoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('number')
            ->add('phoneType', ChoiceType::class, [
                'choices' => array_flip(Telephone::LABEL),
                'attr' => ['required' => 'required']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Telephone::class,
        ]);
    }

}