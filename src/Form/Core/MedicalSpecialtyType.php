<?php

namespace App\Form\Core;

use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\MedicalSpecialtyQualification;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MedicalSpecialtyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('officialLabel')
            ->add('jobTitle')
            ->add('relatedType', EntityType::class, array(
                'class' => MedicalSpecialtyQualification::class,
                'multiple' => true,
                'required' => true
            ))
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MedicalSpecialty::class,
        ]);
    }
}
