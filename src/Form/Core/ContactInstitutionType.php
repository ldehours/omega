<?php

namespace App\Form\Core;

use App\Entity\Core\ContactInstitution;
use App\Entity\Core\ContactService;
use App\Entity\Core\JobName;
use App\Repository\Core\ContactServiceRepository;
use App\Repository\Core\JobNameRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactInstitutionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label',TextType::class,[
                'required' => false
            ])
            ->add('service',
                EntityType::class,
                [
                    'class' => ContactService::class,
                    'query_builder' => function (ContactServiceRepository $contactServiceRepository) {
                        return $contactServiceRepository->createQueryBuilder('c');
                    },
                    'required' => true
                ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactInstitution::class,
        ]);
    }
}
