<?php

namespace App\Form\Core;

use App\Entity\CFML\MembershipCfmlSpecificStatusHistorization;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use App\Entity\CFML\MembershipCfml;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class MembershipCfmlSpecificStatusHistorizationType
 * @package App\Form\Core
 */
class MembershipCfmlSpecificStatusHistorizationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'oldSpecificStatus',
            ChoiceType::class,
            [
                'choices' => array_intersect(array_flip(MembershipCfml::LABEL), MembershipCfml::SPECIFIC_STATUS),
                'attr' => ['required' => 'required']
            ]
        )
            ->add('submit', SubmitType::class);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => MembershipCfmlSpecificStatusHistorization::class,
                'cascade_validation' => true
            ]
        );
    }

}
