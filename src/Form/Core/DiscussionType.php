<?php

namespace App\Form\Core;

use App\Entity\Core\Discussion;
use App\Entity\Core\Service;
use App\Repository\Core\ServiceRepository;
use App\Utils\DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraints\Date;

class DiscussionType extends AbstractType
{
    public $serviceRepository;
    public $security;

    public function __construct(ServiceRepository $serviceRepository, Security $security)
    {
        $this->serviceRepository = $serviceRepository;
        $this->security = $security;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => array_intersect(array_flip(Discussion::LABEL), Discussion::TYPES),
                'required' => true
            ])
            ->add('service', EntityType::class, [
                'class' => Service::class,
                'multiple' => false,
                'required' => true,
                'data' => $this->serviceRepository->findOneById($this->security->getUser()->getService()->getId()),
            ])
            ->add('subject', TextType::class, [
                'required' => true
            ])
            ->add('memo', TextareaType::class)
            ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Discussion::class,
        ]);
    }
}
