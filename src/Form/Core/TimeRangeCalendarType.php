<?php

namespace App\Form\Core;

use App\Entity\Core\TimeRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeRangeCalendarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nature',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(TimeRange::LABEL), TimeRange::NATURES_PERSON),
                    'attr' => ['required' => 'required']
                ]
            )
            ->add(
                'startDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control datepicker'
                    ],
                ]
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => [
                        'required' => 'required',
                        'class' => 'form-control datepicker'
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => TimeRange::class,
            ]
        );
    }
}