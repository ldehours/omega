<?php

namespace App\Form\Core;


use App\Entity\Core\Note;
use App\Entity\Core\Staff;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class NoteType
 * @package App\Form\Core
 */
class NoteType extends AbstractType
{
    protected $tokenStorage;

    /**
     * NoteType constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('noteType', ChoiceType::class, [
            'choices' => array_flip(Note::LABEL)
        ])
            ->add('text', TextareaType::class)
            ->add('creator', EntityType::class, [
                'class' => Staff::class,
                'data' => $this->tokenStorage->getToken()->getUser()
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Note::class,
        ]);
    }
}
