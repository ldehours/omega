<?php

namespace App\Form\Offer;

use App\Entity\Core\MedicalSpecialty;
use App\Entity\Offer\Offer;
use App\Entity\Offer\SubstitutionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $substitutionTypes = SubstitutionType::LABEL;
        $newSubstitutionTypes = [];
        foreach ($substitutionTypes as $key=>$substitutionType){
            if(!in_array($key,[SubstitutionType::TYPE_LIBERAL_REPLACEMENT,SubstitutionType::TYPE_SALARIED_POSITION,SubstitutionType::TYPE_SELL_ASSOCIATION])){
                $newSubstitutionTypes[$key] = $substitutionType;
            }
        }
        $builder
            ->add('id', IntegerType::class, [
                'attr' => [
                    'min' => 1
                ],
                'required' => false
            ])
            ->add('specialty', EntityType::class, [
                'class' => MedicalSpecialty::class,
                'choice_label' => 'officialLabel',
                'required' => false,
            ])
            ->add('type', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip($newSubstitutionTypes)
            ])
            ->add('state', ChoiceType::class, [
                'required' => false,
                'choices' => array_flip(Offer::LABEL)
            ])
            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'required' => false,
            ])
            ->add('personName', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('postalCode', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('locality', TextType::class, [
                'attr' => [
                    'minlength' => 3
                ],
                'required' => false
            ])
            ->add('submit', SubmitType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'cascade_validation' => true
        ]);
    }
}