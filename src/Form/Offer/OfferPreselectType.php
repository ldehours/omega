<?php

namespace App\Form\Offer;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\Offer;
use App\Repository\Offer\CanvasRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferPreselectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'canvas',
                EntityType::class,
                [
                    'class' => Canvas::class,
                    'required' => true,
                    'query_builder' => function (CanvasRepository $canvasRepository) {
                        return $canvasRepository->createQueryBuilder('c')
                            ->andWhere('c.canvasType = :type')->setParameter('type',Canvas::CANVAS_OFFER);
                    },
                ]
            )
            ->add('continue', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
