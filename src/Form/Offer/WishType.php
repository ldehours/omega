<?php

namespace App\Form\Offer;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\Wish;
use App\Repository\Offer\CanvasRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'canvas',
                EntityType::class,
                [
                    'class' => Canvas::class,
                    'query_builder' => function (
                        CanvasRepository $canvasRepository
                    ) {
                        return $canvasRepository->findCanvasByType(Canvas::CANVAS_WISH);
                    },
                    'attr' => [
                        'required' => true
                    ]
                ]
            )
            ->add(
                'startingDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'required' => true,
                ]
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'required' => false
                ]
            )
            ->add(
                'isModel',
                CheckboxType::class,
                [
                    'required' => false,
                ]
            )
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Wish::class,
            ]
        );
    }
}
