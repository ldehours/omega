<?php

namespace App\Form\Offer;

use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\SubstitutionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CanvasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add(
                'substitutionType',
                EntityType::class,
                [
                    'class' => SubstitutionType::class,
                    'required' => true
                ]
            )
            ->add(
                'criteria',
                EntityType::class,
                [
                    'class' => Criterion::class,
                    'multiple' => true,
                    'required' => true
                ]
            )
            ->add(
                'canvasType',
                ChoiceType::class,
                [
                    'choices' => array_flip(Canvas::LABEL),
                    'required' => true,
                    'multiple' => false
                ]
            )
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Canvas::class,
            ]
        );
    }
}

