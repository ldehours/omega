<?php

namespace App\Form\Offer;

use App\Entity\Core\Department;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Offer\Canvas;
use App\Entity\Offer\Offer;
use App\Form\Core\NoteType;
use App\Entity\Core\Origin;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\MedicalSpecialtyRepository;
use App\Repository\Core\OriginRepository;
use App\Repository\Offer\CanvasRepository;
use App\Utils\PHPHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OfferType
 * @package App\Form\Offer
 */
class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Offer $offer */
        $offer = $builder->getData();
        $builder
            ->add(
                'origin',
                EntityType::class,
                [
                    'class' => Origin::class,
                    'query_builder' => function (
                        OriginRepository $originRepository
                    ) {
                        return $originRepository->findOriginByUsage(Origin::OFFER);
                    },
                    'required' => true
                ]
            )
            ->add(
                'state',
                ChoiceType::class,
                [
                    'choices' => array_intersect(array_flip(PHPHelper::sortArrayFromValue(Offer::LABEL)), Offer::STATES),
                    'required' => true
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [//TODO : gérer ces types en fonction du type de contrat adhérent
                    'choices' => array_intersect(array_flip(PHPHelper::sortArrayFromValue(Offer::TYPES_LABEL)), Offer::TYPES),
                    'required' => true
                ]
            )
            ->add(
                'pricing',
                IntegerType::class,
                [
                    'required' => true,
                ]
            )
            ->add(
                'complement',
                TextareaType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'coordinatesRemoval',
                CheckboxType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'isModel',
                CheckboxType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'nearestCity',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'startingDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'required' => true,
                ]
            )
            ->add(
                'startingDateModularity',
                IntegerType::class,
                [
                    'required' => true,
                ]
            )->add(
                'startingDateModularityType',
                ChoiceType::class,
                [
                    'required' => true,
                    'choices' => array_flip(Offer::MODULARITY_TYPE_LABEL),
                    'data' => $offer->getStartingDateModularityType() ?? Offer::MODULARITY_TYPE_BOTH
                ]
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'required' => false
                ]
            )
            ->add(
                'endDateModularity',
                IntegerType::class,
                [
                    'required' => true,
                ]
            )->add(
                'endDateModularityType',
                ChoiceType::class,
                [
                    'required' => false,
                    'choices' => array_flip(Offer::MODULARITY_TYPE_LABEL)
                ]
            )
            ->add(
                'canvas',
                EntityType::class,
                [
                    'class' => Canvas::class,
                    'query_builder' => function (
                        CanvasRepository $canvasRepository
                    ) {
                        return $canvasRepository->findCanvasByType(Canvas::CANVAS_OFFER);
                    },
                    'required' => true
                ]
            )
            ->add(
                'specialty',
                EntityType::class,
                [
                    'class' => MedicalSpecialty::class,
                    'query_builder' => function (
                        MedicalSpecialtyRepository $medicalSpecialtyRepository
                    ) {
                        return $medicalSpecialtyRepository->createQueryBuilder('m')
                            ->join('m.relatedType', 'q')
                            ->andWhere("q.label = 'Spécialité'")
                            ->orderBy('m.officialLabel','ASC');
                    },
                    'required' => true
                ]
            )
            ->add(
                'notes',
                CollectionType::class,
                [
                    'entry_type' => NoteType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'by_reference' => false
                ]
            )
            ->add('department', EntityType::class, [
                'class' => Department::class,
                'required' => false,
                'query_builder' => function (DepartmentRepository $department) {
                    return $department->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },
                'choice_label' => function ($department) {
                    return $department->getName() . ' (' . $department->getCode() . ')';
                }
            ])
            ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Offer::class,
            ]
        );
    }
}
