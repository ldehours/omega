<?php

namespace App\Repository\Core;

use App\Entity\Core\StaffHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StaffHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method StaffHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method StaffHistory[]    findAll()
 * @method StaffHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StaffHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StaffHistory::class);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function findLastSave($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.staffId = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
