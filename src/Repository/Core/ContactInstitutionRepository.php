<?php

namespace App\Repository\Core;

use App\Entity\Core\ContactInstitution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ContactInstitution|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactInstitution|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactInstitution[]    findAll()
 * @method ContactInstitution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactInstitutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactInstitution::class);
    }

    // /**
    //  * @return ContactInstitution[] Returns an array of ContactInstitution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactInstitution
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
