<?php

namespace App\Repository\Core;

use App\Entity\Core\MedicalSpecialtyQualification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MedicalSpecialtyQualification|null find($id, $lockMode = null, $lockVersion = null)
 * @method MedicalSpecialtyQualification|null findOneBy(array $criteria, array $orderBy = null)
 * @method MedicalSpecialtyQualification[]    findAll()
 * @method MedicalSpecialtyQualification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MedicalSpecialtyQualificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MedicalSpecialtyQualification::class);
    }

    // /**
    //  * @return MedicalSpecialtyQualification[] Returns an array of MedicalSpecialtyQualification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MedicalSpecialtyQualification
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
