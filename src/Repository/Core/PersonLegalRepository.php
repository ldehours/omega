<?php

namespace App\Repository\Core;

use App\Entity\Core\PersonLegal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PersonLegal|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonLegal|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonLegal[]    findAll()
 * @method PersonLegal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonLegalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonLegal::class);
    }

    public function getPersonLegalInfos()
    {
        return $this->createQueryBuilder('p')
            ->select('p.id, p.corporateName, p.personStatus, m.address, ad.addressLine1, ad.addressLine2, ad.postalCode, ad.locality, pj.label, pj.classification')
            ->leftJoin('p.mails', 'm')
            ->leftJoin('p.addresses', 'ad')
            ->join('p.mainJobName', 'pj')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $search
     * @return mixed
     */
    public function searchByCorporateName(string $search)
    {
        return $this->createQueryBuilder('p')
            ->where('p.corporateName like :query')
            ->setParameter('query', '%' . $search . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $search
     * @return array
     * @throws \Exception
     */
    public function searchPersonsByStatus(string $search): array
    {
        $personsFound = [];
        if ($this->count([]) > 0) {

            $statuses = [];
            foreach (PersonLegal::STATUS as $status) {
                if (stristr(PersonLegal::LABEL[$status], $search) !== false) {
                    $statuses[] = $status;
                }
            }

            $personsFound = $this->createQueryBuilder('p')
                ->where('p.personStatus in (:statuses)')
                ->setParameter('statuses', $statuses)
                ->getQuery()
                ->getResult();
        }
        return $personsFound;
    }

    /**
     * @param array $optionsRequested
     * @return mixed
     */
    public function findByMultiCritera(array $optionsRequested)
    {
        $queryBuilder = $this->createQueryBuilder('p');
        /* We do the 'joins' */
        if (key_exists('classification', $optionsRequested) && !empty($optionsRequested['classification'])) {
            $queryBuilder->innerJoin('p.mainJobName', 'j');
        }
        if (key_exists('email', $optionsRequested)) {
            $queryBuilder->innerJoin('p.mails', 'm');
        }
        if (key_exists('telephone', $optionsRequested)) {
            $queryBuilder->innerJoin('p.telephones', 't');
        }
        if (key_exists('postalCode', $optionsRequested) || key_exists('locality', $optionsRequested)) {
            $queryBuilder->innerJoin('p.addresses', 'a');
        }
        /*******************/

        /* We do the 'where' */
        if (key_exists('classification', $optionsRequested) && !empty($optionsRequested['classification'])) {
            $queryBuilder->andWhere('j.classification IN (:classification)')->setParameter('classification',
                $optionsRequested['classification']);
        }
        if (key_exists('corporateName', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('p.corporateName = :corporateName')->setParameter('corporateName',
                    $optionsRequested['corporateName']);
            } else {
                $queryBuilder->andWhere('p.corporateName like :corporateName')->setParameter('corporateName',
                    $optionsRequested['corporateName'] . "%");
            }
        } elseif (key_exists('lastName', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('p.corporateName = :corporateName')->setParameter('corporateName',
                    $optionsRequested['lastName']);
            } else {
                $queryBuilder->andWhere('p.corporateName like :corporateName')->setParameter('corporateName',
                    $optionsRequested['lastName'] . "%");
            }
        }
        if (key_exists('email', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('m.address = :mail')->setParameter('mail', $optionsRequested['email']);
            } else {
                $queryBuilder->andWhere('m.address like :mail')->setParameter('mail',
                    "%" . $optionsRequested['email'] . "%");
            }
        }
        if (key_exists('telephone', $optionsRequested)) {
            $queryBuilder->andWhere('t.number = :telephone')->setParameter('telephone', $optionsRequested['telephone']);
        }
        if (key_exists('postalCode', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('a.postalCode = :postalCode')->setParameter('postalCode',
                    $optionsRequested['postalCode']);
            } else {
                $queryBuilder->andWhere('a.postalCode like :postalCode')->setParameter('postalCode',
                    $optionsRequested['postalCode'] . "%");
            }
        }
        if (key_exists('locality', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('a.locality = :locality')->setParameter('locality',
                    $optionsRequested['locality']);
            } else {
                $queryBuilder->andWhere('a.locality like :locality')->setParameter('locality',
                    "%" . $optionsRequested['locality'] . "%");
            }
        }
        /*********************/
        return $queryBuilder->getQuery()->getResult();
    }
}
