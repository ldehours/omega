<?php

namespace App\Repository\Core;

use App\Entity\Core\Discussion;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use App\Entity\Core\Staff;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Discussion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Discussion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Discussion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscussionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Discussion::class);
    }

    /**
     * @param Person $person
     * @param int|null $type
     * @return Discussion|null
     * @throws NonUniqueResultException
     */
    public function findOneLastDiscussionByTypeAndPerson(Person $person, int $type = null): ?Discussion
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->andWhere('d.person = :person')
            ->setParameter('person', $person)
            ->orderBy('d.creationDate', 'DESC')
            ->setMaxResults(1);

        if ($type != null) {
            $queryBuilder
                ->andWhere('d.type = :type')
                ->setParameter('type', $type);
        }
        return $queryBuilder->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Person $person
     * @param int|null $type
     * @return Collection|Discussion[] |null
     */
    public function findFiveLastDiscussionByTypeAndPerson(Person $person, int $type = null)
    {
        $queryBuilder = $this->createQueryBuilder('d')
            ->andWhere('d.person = :person')
            ->setParameter('person', $person)
            ->orderBy('d.creationDate', 'DESC')
            ->setMaxResults(5);

        if ($type != null) {
            $queryBuilder
                ->andWhere('d.type = :type')
                ->setParameter('type', $type);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int $type
     * @param Staff|null $staff
     * @param Person|null $person
     * @return QueryBuilder
     */
    public function getListDiscussion(
        int $type,
        Staff $staff = null,
        Person $person = null
    ): QueryBuilder {
        $queryBuilder = $this->createQueryBuilder('d')->where('d.type =:type')->setParameter('type', $type);

        if ($staff != null) {
            $queryBuilder->andWhere('d.creator=:creator')->setParameter('creator', $staff);
        }

        if ($person != null) {
            $queryBuilder->andWhere('d.person=:person')->setParameter('person', $person);
        }
        return $queryBuilder->orderBy('d.id', 'DESC');
    }

    /**
     * @param PersonNatural $personNatural
     * @return Discussion|null
     */
    public function findNoteAboutPerson(
        PersonNatural $personNatural
    ): ?Discussion {
        // soit égal à 6 ou 8 pour le type
        $type = Discussion::MEMO_RECRUITMENT_DEPT;
        $typeOther = Discussion::MEMO_BUSINESS_DEPT;

        $queryBuilder = $this->createQueryBuilder('d')
            ->andWhere('d.person = :person')
            ->setParameter('person', $personNatural)
            ->andWhere("d.type = $type OR d.type = $typeOther")
            ->orderBy('d.creationDate', "DESC")
            ->setMaxResults(5);

        return $queryBuilder->getQuery()->getResult();
    }

    public function LastPersonDiscussion(Person $Person)
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->andWhere('d.person = :person')
            ->setParameter('person', $Person)
            ->orderBy('d.creationDate', "DESC")
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getResult();
    }


}
