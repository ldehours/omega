<?php

namespace App\Repository\Core;

use App\Entity\Core\Origin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Origin|null find($id, $lockMode = null, $lockVersion = null)
 * @method Origin|null findOneBy(array $criteria, array $orderBy = null)
 * @method Origin[]    findAll()
 * @method Origin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OriginRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Origin::class);
    }

    /**
     * @param int $usage
     * @return mixed
     */
    public function findOriginByUsage(int $usage)
    {
        if (!in_array($usage, Origin::ENTITY_USAGES)) {
            throw new \InvalidArgumentException('$usage must be defined in ENTITY_USAGES array');
        }
        return $this->createQueryBuilder('m')
            ->andWhere("m.isActive = true")
            ->andWhere("m.usages like '%" . $usage . "%'")
            ->orderBy('m.label', 'ASC');
    }


    /*
    public function findOneBySomeField($value): ?Origin
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
