<?php

namespace App\Repository\Core;

use App\Entity\Core\Mail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Mail|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mail|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mail[]    findAll()
 * @method Mail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mail::class);
    }

    /**
     * @param string $value
     * @return array
     */
    public function searchPersonsByMail(string $value): array
    {
        $mailQuery = $this->createQueryBuilder('ml');
        $firstHalfMailsFound = [];// check characters before @
        $secondHalfMailsFound = [];// check characters after @
        $personsFoundByMail = [];

        $exactMail = $this->findByAddress($value);
        if ($exactMail != []) {
            $mailsFound = $exactMail;
        } else {
            $mailFirstPart = stristr($value, '@', true);
            $mailSecondPart = stristr($value, '@');
            if ($mailFirstPart !== false && $mailFirstPart !== '') {
                $firstHalfMailsFound = $mailQuery->andwhere('ml.address like :mail and ml.isDefault = true')->setParameter('mail',
                    trim($mailFirstPart) . '%')->getQuery()->getResult();
            }

            if ($mailSecondPart != false) {
                $secondHalfMailsFound = $mailQuery->andwhere('ml.address like :mail and ml.isDefault = true')->setParameter('mail',
                    '%'.trim($mailSecondPart) . '%')->getQuery()->getResult();
            }

            if ($firstHalfMailsFound != [] && $secondHalfMailsFound != []) {
                $mailsFound = array_intersect($firstHalfMailsFound, $secondHalfMailsFound);
            } else {
                $mailsFound = array_values(array_merge($firstHalfMailsFound, $secondHalfMailsFound));
            }
        }

        foreach ($mailsFound as $mail) {
            foreach ($mail->getPersons() as $person) {
                $personsFoundByMail[] = $person;
            }
        }

        return $personsFoundByMail;
    }
}
