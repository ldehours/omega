<?php

namespace App\Repository\Core;

use App\Entity\Core\Staff;
use App\Utils\RoleHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Staff|null find($id, $lockMode = null, $lockVersion = null)
 * @method Staff|null findOneBy(array $criteria, array $orderBy = null)
 * @method Staff[]    findAll()
 * @method Staff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StaffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, RoleHelper $roleHelper)
    {
        parent::__construct($registry, Staff::class);
    }

    /**
     * @param array $arrayRoles
     * @return array
     */
    public function staffByRoles(array $arrayRoles): array
    {
        $allStaff = $this->findBy(['isActive' => true]);
        $staffByRole = [];

        foreach ($allStaff as $staff) {
            foreach ($staff->getRoles() as $role) {
                if (in_array($role, $arrayRoles)) {
                    $staffByRole[] = $staff;
                }
            }
        }
        return $staffByRole;
    }

    /**
     * @param array $arrayRoles
     * @param Staff $staff
     * @return bool
     */
    public function staffHasRole(array $arrayRoles, Staff $staff): bool
    {
        foreach ($staff->getRoles() as $role) {
            if (in_array($role, $arrayRoles)) {
                return true;
            }
        }
        return false;
    }
}

