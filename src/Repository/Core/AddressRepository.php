<?php

namespace App\Repository\Core;

use App\Entity\Core\Address;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Address::class);
    }

    /**
     * @param string $value
     * @return array
     */
    public function searchPersonsByPostalCode(string $value): array
    {
        $personsFoundByPostalCode = [];
        if (is_numeric($value) && $this->count(['postalCode' => $value]) > 0) {
            foreach ($this->findBy(['postalCode' => $value]) as $key => $address) {
                if (in_array(Address::FISCAL, $address->getUsages()) || in_array(Address::CORRESPONDENCE, $address->getUsages())) {
                    foreach ($address->getPerson() as $person) {
                        $personsFoundByPostalCode[] = $person;
                    }
                }
            }
        }
        return $personsFoundByPostalCode;
    }

    /**
     * @param string $value
     * @return array
     */
    public function searchPersonsByLocality(string $value): array
    {
        $personFoundByLocality = [];
        $addresses = $this->createQueryBuilder('ad')->andwhere('ad.locality like :query')->setParameter('query',
            '%' . $value . '%')->getQuery()->getResult();
        if ($addresses != null) {
            foreach ($addresses as $address) {
                if (in_array(Address::FISCAL, $address->getUsages()) || in_array(Address::CORRESPONDENCE, $address->getUsages())) {
                    foreach ($address->getPerson() as $person) {
                        $personFoundByLocality[] = $person;
                    }
                }
            }
        }
        return $personFoundByLocality;
    }
}
