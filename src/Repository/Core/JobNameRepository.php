<?php

namespace App\Repository\Core;

use App\Entity\Core\JobName;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method JobName|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobName|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobName[]    findAll()
 * @method JobName[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobNameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobName::class);
    }

    /**
     * @param string $search
     * @param ServiceEntityRepository $repository
     * @return array
     * @throws \Exception
     * TODO: a bouger dans PersonRepository
     */
    public function searchPersonsByJobNameAndClassification(string $search, ServiceEntityRepository $repository): array
    {
        $jobNamesIdFound = $this->filterByJobNameAndClassification($search, $repository);
        return $personFoundByJobName = $repository->createQueryBuilder('p')->andWhere('p.mainJobName IN (:jobName)')
            ->setParameter('jobName', $jobNamesIdFound)->getQuery()->getResult();
    }

    /**
     * @param $search
     * @return mixed
     * @throws \Exception
     */
    public function filterByJobNameAndClassification($search, $repository)
    {
        $classification = [];
        $jobNameQuery = $this->createQueryBuilder('jn');
        $jobNameIdFound = [];

        foreach ($this->findAll() as $jobName) {
            if (strpos(strtolower("*" . $jobName->getTranslate()->getLabel($jobName->getClassification())),
                strtolower($search))) {
                if ($classification == []) {
                    $classification[] = $jobName->getClassification();
                } else {
                    if (!in_array($jobName->getClassification(), $classification)) {
                        $classification[] = $jobName->getClassification();
                    }
                }
            }
        }

        $delimiter = "\ ";
        $entity = explode(trim($delimiter), $repository->getClassName());

        if (end($entity) == "PersonLegal") {
            $jobNameFound = $jobNameQuery->select('jn.id')->andwhere('jn.label like :label or jn.classification IN (:classification)')->setParameter('label',
                "%" . $search . "%")->setParameter('classification',
                $classification)->getQuery()->getResult();
            $jobNameIdFound = [];
            foreach ($jobNameFound as $jobNameId) {
                $jobNameIdFound[] = $jobNameId['id'];
            }
        }

        if (end($entity) == "PersonNatural") {
            $jobNameFound = $jobNameQuery->select('jn.id')->andwhere('jn.label like :label')->setParameter('label',
                "%" . $search . "%")->getQuery()->getResult();
            $jobNameIdFound = [];
            foreach ($jobNameFound as $jobNameId) {
                $jobNameIdFound[] = $jobNameId['id'];
            }
        }

        return $jobNameIdFound;
    }

    /**
     * @param array $classifications
     * @return QueryBuilder
     */
    public function searchByClassification(array $classifications)
    {
        $qb = $this->createQueryBuilder('j');
        foreach ($classifications as $classification){
            $qb->orWhere('j.classification = :classification')->setParameter('classification',$classification);
        }
        return $qb->getQuery()->getResult();
    }
}
