<?php

namespace App\Repository\Core;

use App\Entity\Core\Address;
use App\Entity\Core\JobName;
use App\Entity\Core\Mail;
use App\Entity\Core\Person;
use App\Entity\Core\PersonNatural;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PersonNatural|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonNatural|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonNatural[]    findAll()
 * @method PersonNatural[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonNaturalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonNatural::class);
    }

    public function getPersonNaturalInfos()
    {
        return $this->createQueryBuilder('p')
            ->select('p.id, p.gender, p.lastName, p.firstName, m.address, ad.postalCode, ad.locality, pj.label')
            ->leftJoin('p.mails', 'm')
            ->leftJoin('p.addresses', 'ad')
            ->join('p.mainJobName', 'pj')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function searchByNames($search)
    {
        return $this->createQueryBuilder('p')
            ->where('p.firstName like :query or p.lastName like :query')
            ->setParameter('query', '%' . $search . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function searchByGenders($search)
    {
        $gender = null;
        foreach (PersonNatural::LABEL as $index => $value) {
            if (mb_strtoupper($search) == mb_strtoupper($value)) {
                $gender = $index;
                break;
            }
        }
        return $this->createQueryBuilder('p')
            ->where('p.gender =:query')
            ->setParameter('query', $gender)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $classification
     * @return int|mixed|string
     */
    public function searchByClassification(int $classification)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.mainJobName','j')
            ->where('j.classification = :classification')
            ->setParameter('classification', $classification)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $value
     * @param array $ids
     * @return mixed
     */
    public function showSelectablePersons($value, array $ids)
    {
        return $this->createQueryBuilder('p')
            ->where('p.firstName like :query or p.id in (:ids) or p.lastName like :query')
            ->setParameters(['ids' => $ids, 'query' => '%' . $value . '%'])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $optionsRequested
     * @return mixed
     */
    public function findByMultiCritera(array $optionsRequested)
    {
        $queryBuilder = $this->createQueryBuilder('p');
        /* We do the 'joins' */
        if (key_exists('classification', $optionsRequested) && !empty($optionsRequested['classification'])) {
            $queryBuilder->innerJoin('p.mainJobName', 'j');
        }
        if (key_exists('email', $optionsRequested)) {
            $queryBuilder->innerJoin('p.mails', 'm');
        }
        if (key_exists('telephone', $optionsRequested)) {
            $queryBuilder->innerJoin('p.telephones', 't');
        }
        if (key_exists('postalCode', $optionsRequested) || key_exists('locality', $optionsRequested)) {
            $queryBuilder->innerJoin('p.addresses', 'a');
        }
        /*******************/

        /* We do the 'where' */
        if (key_exists('classification', $optionsRequested) && !empty($optionsRequested['classification'])) {
            $queryBuilder->andWhere('j.classification IN (:classifications)')->setParameter('classifications',
                $optionsRequested['classification']);
        }
        if (key_exists('lastName', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('p.lastName = :lastName')->setParameter('lastName',
                    $optionsRequested['lastName']);
            } else {
                $queryBuilder->andWhere('p.lastName like :lastName')->setParameter('lastName',
                    $optionsRequested['lastName'] . "%");
            }
        }
        if (key_exists('firstName', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('p.firstName = :firstName')->setParameter('firstName',
                    $optionsRequested['firstName']);
            } else {
                $queryBuilder->andWhere('p.firstName like :firstName')->setParameter('firstName',
                    $optionsRequested['firstName'] . "%");
            }
        }
        if (key_exists('email', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('m.address = :mail')->setParameter('mail', $optionsRequested['email']);
            } else {
                $queryBuilder->andWhere('m.address like :mail')->setParameter('mail',
                    "%" . $optionsRequested['email'] . "%");
            }
        }
        if (key_exists('telephone', $optionsRequested)) {
            $queryBuilder->andWhere('t.number = :telephone')->setParameter('telephone', $optionsRequested['telephone']);
        }
        if (key_exists('postalCode', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('a.postalCode = :postalCode')->setParameter('postalCode',
                    $optionsRequested['postalCode']);
            } else {
                $queryBuilder->andWhere('a.postalCode like :postalCode')->setParameter('postalCode',
                    $optionsRequested['postalCode'] . "%");
            }
        }
        if (key_exists('locality', $optionsRequested)) {
            if (key_exists('exactResearch', $optionsRequested)) {
                $queryBuilder->andWhere('a.locality = :locality')->setParameter('locality',
                    $optionsRequested['locality']);
            } else {
                $queryBuilder->andWhere('a.locality like :locality')->setParameter('locality',
                    "%" . $optionsRequested['locality'] . "%");
            }
        }
        /*********************/
        return $queryBuilder->getQuery()->getResult();
    }
}
