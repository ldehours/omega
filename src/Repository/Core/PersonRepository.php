<?php

namespace App\Repository\Core;

use App\Entity\Core\Address;
use App\Entity\Core\JobName;
use App\Entity\Core\Mail;
use App\Entity\Core\Person;
use App\Entity\Core\PersonLegal;
use App\Entity\Core\PersonNatural;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;
use InvalidArgumentException;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Person::class);
    }

    /**
     * @param array $data
     * @param ServiceEntityRepository $personRepository
     * @param int $page
     * @param int|null $max
     * @return mixed
     * @throws Exception
     */
    public function instantSearch(
        array $data,
        ServiceEntityRepository $personRepository,
        int $page = 0,
        ?int $max = null
    )
    {
        if ($personRepository->getClassName() != PersonLegal::class && $personRepository->getClassName() != PersonNatural::class) {
            throw new InvalidArgumentException("The repository provided (" . $personRepository->getClassName() .
                ") is not a valid Person repository.");
        }

        $addressRepository = $this->getEntityManager()->getRepository(Address::class);
        $jobNameRepository = $this->getEntityManager()->getRepository(JobName::class);
        $mailRepository = $this->getEntityManager()->getRepository(Mail::class);
        $queryBuilder = $personRepository->createQueryBuilder('p');
        $userInput = trim($data['query']);

        $personsFound = [];

        if (!empty($userInput)) {

            /*RECUPERATION DES ELEMENTS SAISIS*/
            //transforme la chaîne saisie en tableau de valeur
            $words = explode(" ", $userInput);

            /*DEBUT DE LA RECHERCHE*/
            foreach ($words as $word) {

                if (is_numeric($word)) {

                    /*Recherche par ID de PERSON*/
                    $search = $personRepository->find($word);
                    if(isset($search)){
                        $personsFound[] = $search;
                    }
                    /*Recherche par CODE POSTAL*/
                    $personsFound = array_merge($personsFound, $addressRepository->searchPersonsByPostalCode($word));

                } else {

                    /*Recherche par ADRESSE MAIL*/
                    if (strpos($word, "@") !== false) {

                        $personsFound = array_merge($personsFound, $mailRepository->searchPersonsByMail($word));

                    } else {

                        /*Recherche par JOBNAME et CLASSIFICATION*/

                        $personsFound = array_merge($personsFound, $jobNameRepository->searchPersonsByJobName($word, $personRepository));

                        /*Recherche par VILLE*/
                        $personsFound = array_merge($personsFound, $addressRepository->searchPersonsByLocality($word));

                        if ($personRepository->getClassName() == PersonLegal::class) {

                            /*Recherche par STATUT*/
                            $personsFound = array_merge($personsFound, $personRepository->searchPersonsByStatus($word));

                            /*Recherche par RAISON SOCIALE*/
                            $personsFound = array_merge($personsFound, $personRepository->searchByCorporateName($word));
                        } else {

                            /*Recherche par NOM et PRENOM*/
                            $personsFound = array_merge($personsFound, $personRepository->searchByNames($word));
                        }
                    }
                }
            }
            /*FIN DE LA RECHERCHE*/
            $personsId = [];

            foreach ($personsFound as $person) {
                if (!in_array($person->getId(), $personsId)) {
                    $personsId[] = $person->getId();
                }
            }

            $queryBuilder->andWhere('p.id in (:id)')->setParameter('id', $personsId);
        }

        /*ENVOI DU RESULTAT*/
        if ($max != null) {
            $preparedQuery = $queryBuilder->getQuery()->setMaxResults($max)->setFirstResult($page * $max);
        } else {
            $preparedQuery = $queryBuilder->getQuery()->setMaxResults($page * $data['length']);
        }

        return $preparedQuery->getResult();
    }
}
