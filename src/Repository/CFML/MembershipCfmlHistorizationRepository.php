<?php

namespace App\Repository\CFML;

use App\Entity\CFML\MembershipCfmlHistorization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MembershipCfmlHistorization|null find($id, $lockMode = null, $lockVersion = null)
 * @method MembershipCfmlHistorization|null findOneBy(array $criteria, array $orderBy = null)
 * @method MembershipCfmlHistorization[]    findAll()
 * @method MembershipCfmlHistorization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MembershipCfmlHistorizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MembershipCfmlHistorization::class);
    }

    // /**
    //  * @return MembershipCfmlHistorizationManager[] Returns an array of MembershipCfmlHistorizationManager objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MembershipCfmlHistorizationManager
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
