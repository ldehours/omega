<?php

namespace App\Repository\CFML;

use App\Entity\CFML\MembershipCfml;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MembershipCfml|null find($id, $lockMode = null, $lockVersion = null)
 * @method MembershipCfml|null findOneBy(array $criteria, array $orderBy = null)
 * @method MembershipCfml[]    findAll()
 * @method MembershipCfml[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MembershipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MembershipCfml::class);
    }

    /**
     * @param $id
     * @return int|mixed|string
     */
    public function getLastSecondaryRepresentativeById($id)
    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.member = :id')
//            ->andWhere('m.secondaryRepresentative is NOT NULL')
//            ->setParameter('id', $id)
//            ->orderBy('m.dispatchDate', 'DESC')
//            ->setMaxResults(1)
//            ->getQuery()
//            ->getResult();
    }

    /**
     * @param $id
     * @return int|mixed|string
     */
    public function getLastMainRepresentativeById($id)
    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.member = :id')
//            ->andWhere('m.mainRepresentative is NOT NULL')
//            ->setParameter('id', $id)
//            ->orderBy('m.dispatchDate', 'DESC')
//            ->setMaxResults(1)
//            ->getQuery()
//            ->getResult();
    }


    // /**
    //  * @return MembershipCfmlService[] Returns an array of MembershipCfmlService objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MembershipCfmlService
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
