<?php

namespace App\Repository\CFML;

use App\Entity\CFML\MembershipCfml;
use App\Entity\CFML\MembershipCfmlSpecificStatusHistorization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MembershipCfmlSpecificStatusHistorization|null find($id, $lockMode = null, $lockVersion = null)
 * @method MembershipCfmlSpecificStatusHistorization|null findOneBy(array $criteria, array $orderBy = null)
 * @method MembershipCfmlSpecificStatusHistorization[]    findAll()
 * @method MembershipCfmlSpecificStatusHistorization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MembershipCfmlSpecificStatusHistorizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MembershipCfmlSpecificStatusHistorization::class);
    }

    public function findBeforeLast(MembershipCfml $membershipCfml)
    {
        // trouve l'avant dernier id
        $query = $this->createQueryBuilder('m')
            ->andWhere('m.membership = :val')
            ->setParameter('val', $membershipCfml)
            ->getQuery()
            ->getResult();

        $count = count($query);

        if (!$count) {
            return false;
        }

        // -2 pour avoir l'avant dernier , et pas -1 car les array commence par 0 et par 1
        return $query;

    }

    // /**
    //  * @return MembershipCfmlSpecificStatusHistorization[] Returns an array of MembershipCfmlSpecificStatusHistorization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MembershipCfmlSpecificStatusHistorization
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
