<?php

namespace App\Repository\Offer;

use App\Entity\Offer\CriterionPossibleValue;
use App\Entity\Offer\SubstitutionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CriterionPossibleValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CriterionPossibleValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CriterionPossibleValue[]    findAll()
 * @method CriterionPossibleValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CriterionPossibleValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CriterionPossibleValue::class);
    }

    // /**
    //  * @return CriterionPossibleValue[] Returns an array of CriterionPossibleValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param $value
     * @return int|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findSubstitutionTypeId($value): ?int
    {
        $substitutionTypeRepository = $this->getEntityManager()->getRepository(SubstitutionType::class);
        return $substitutionTypeRepository->findOneBySubstitutionTypeLabel($value);
    }
}
