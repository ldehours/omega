<?php

namespace App\Repository\Offer;

use App\Entity\Core\PersonNatural;
use App\Entity\Offer\Offer;
use App\Entity\Offer\Wish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

use function Doctrine\ORM\QueryBuilder;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }

    /**
     * @param Wish $wish
     * @return int|mixed|string
     */
    public function offersForMatching(Wish $wish)
    {
        /** @var PersonNatural $wishPerson */
        $wishPerson = $wish->getPerson();

        $queryBuilder = $this->createQueryBuilder('o');
        $queryBuilder->andWhere('o.state = :state')
            ->andWhere('o.isModel <> true')
            ->andWhere('o.substitutionType = :substitutionType')
            ->andWhere('o.person <> :wishPerson')
            ->andWhere('o.specialty = :wishPersonSpecialty OR o.specialty in (:wishPersonCapacities)')
            ->setParameter('state', Offer::STATE_PROGRESS)
            ->setParameter('substitutionType', $wish->getSubstitutionType())
            ->setParameter('wishPerson', $wishPerson)
            ->setParameter('wishPersonSpecialty', $wishPerson->getSpecialty())
            ->setParameter('wishPersonCapacities', $wishPerson->getCapacities());

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function offersForOfferMatching()
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT o FROM App\Entity\Offer\Offer o WHERE o.state = :state and o.isModel <> true'
        )
            ->setParameter('state', Offer::STATE_PROGRESS);
        return $query->getResult();
    }

    /**
     * @param array $optionsRequested
     * @return mixed
     */
    public function findByMultiCritera(array $optionsRequested)
    {
        $existsPostalCode = array_key_exists('postalCode', $optionsRequested);
        $existsLocality = array_key_exists('locality', $optionsRequested);
        $existsType = array_key_exists('type', $optionsRequested);
        $existsState = array_key_exists('state', $optionsRequested);
        $existsStartDate = array_key_exists('startDate', $optionsRequested);
        $existsEndDate = array_key_exists('endDate', $optionsRequested);
        $existsSpecialty = array_key_exists('specialty', $optionsRequested);

        $queryBuilder = $this->createQueryBuilder('o');
        /* We do the 'joins' */
        if ($existsPostalCode || $existsLocality) {
            $queryBuilder->innerJoin('o.person', 'p');
            $queryBuilder->innerJoin('p.addresses', 'a');
        }
        if ($existsType) {
            $queryBuilder->innerJoin('o.substitutionType', 'st');
        }
        if ($existsSpecialty) {
            $queryBuilder->innerJoin('o.specialty', 'sp');
        }
        /*******************/

        /* We do the 'where' */
        if ($existsType) {
            $queryBuilder->andWhere('st.substitutionType = :type')->setParameter(
                'type',
                $optionsRequested['type']
            );
        }
        if ($existsState) {
            $queryBuilder->andWhere('o.state = :state')->setParameter(
                'state',
                $optionsRequested['state']
            );
        }
        if ($existsStartDate) {
            $queryBuilder->andWhere('o.startingDate >= :startDate')->setParameter(
                'startDate',
                $optionsRequested['startDate']
            );
        }
        if ($existsEndDate) {
            $queryBuilder->andWhere('o.endDate is not NULL and o.endDate <= :endDate')->setParameter(
                'endDate',
                $optionsRequested['endDate']
            );
        }
        if ($existsSpecialty) {
            $queryBuilder->andWhere('sp.id = :specialty')->setParameter(
                'specialty',
                $optionsRequested['specialty']
            );
        }
        if ($existsPostalCode) {
            $queryBuilder->andWhere('a.postalCode = :postalCode')->setParameter(
                'postalCode',
                $optionsRequested['postalCode']
            );
        }
        if ($existsLocality) {
            $queryBuilder->andWhere('a.locality = :locality')->setParameter(
                'locality',
                $optionsRequested['locality']
            );
        }
        /*********************/
        return $queryBuilder->getQuery()->getResult();
    }
}
