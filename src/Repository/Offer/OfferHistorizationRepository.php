<?php

namespace App\Repository\Offer;

use App\Entity\Offer\OfferHistorization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OfferHistorization|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferHistorization|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferHistorization[]    findAll()
 * @method OfferHistorization[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferHistorizationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferHistorization::class);
    }

    // /**
    //  * @return OfferHistorization[] Returns an array of OfferHistorization objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferHistorization
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
