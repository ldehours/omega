<?php

namespace App\Repository\Offer;

use App\Entity\Offer\Canvas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Canvas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Canvas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Canvas[]    findAll()
 * @method Canvas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CanvasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Canvas::class);
    }

    /**
     * @param int $type
     * @return QueryBuilder
     */
    public function findCanvasByType(int $type)
    {
        if (!in_array($type, Canvas::CANVAS_TYPES)) {
            throw new \InvalidArgumentException('$type must be defined in CANVAS_TYPES array');
        }
        return $this->createQueryBuilder('c')
            ->andWhere("c.canvasType = " . $type)
            ->orderBy('c.label','ASC');
    }

}
