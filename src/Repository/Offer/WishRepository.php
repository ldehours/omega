<?php

namespace App\Repository\Offer;

use App\Entity\Core\Person;
use App\Entity\Offer\Wish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wish[]    findAll()
 * @method Wish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WishRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wish::class);
    }

    /**
     * @param Person $person
     * @return mixed
     */
    public function wishesForSubstituteMatching(Person $person)
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT w FROM App\Entity\Offer\Wish w JOIN w.timeRange t WHERE w.person = :person and w.isModel <> true and t.endDate > CURRENT_TIMESTAMP() and w.isActive <> false'
        )
            ->setParameter('person', $person);
        return $query->getResult();
    }

    /**
     * @return int|mixed|string
     */
    public function wishesForOffersMatching()
    {
        $queryBuilder = $this->createQueryBuilder('w');

        $queryBuilder->andWhere('w.isModel <> true')
            ->join('w.timeRange', 't')
            ->andWhere('t.endDate > CURRENT_TIMESTAMP()')
            ->andWhere('w.isActive <> false');

        return $queryBuilder->getQuery()->getResult();
    }

    // /**
    //  * @return Wish[] Returns an array of Wish objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wish
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
