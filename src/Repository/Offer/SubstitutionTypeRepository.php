<?php

namespace App\Repository\Offer;

use App\Entity\Offer\SubstitutionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubstitutionType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubstitutionType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubstitutionType[]    findAll()
 * @method SubstitutionType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstitutionTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubstitutionType::class);
    }

    /**
     * @param string $value
     * @return int|null
     */
    public function findOneBySubstitutionTypeLabel(string $value): ?int
    {
        foreach ($this->findAll() as $substitutionType) {
            if ($substitutionType->__toString() == $value) {
                return $substitutionType->getId();
            }
        }
        return null;
    }
}
