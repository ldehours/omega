<?php

namespace App\Repository\Offer;

use App\Entity\Offer\OfferWishMatching;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OfferWishMatching|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfferWishMatching|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfferWishMatching[]    findAll()
 * @method OfferWishMatching[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferWishMatchingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfferWishMatching::class);
    }

    // /**
    //  * @return OfferWishMatching[] Returns an array of OfferWishMatching objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfferWishMatching
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
