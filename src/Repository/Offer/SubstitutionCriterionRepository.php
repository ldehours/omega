<?php

namespace App\Repository\Offer;

use App\Entity\Offer\SubstitutionCriterion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubstitutionCriterion|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubstitutionCriterion|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubstitutionCriterion[]    findAll()
 * @method SubstitutionCriterion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstitutionCriterionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubstitutionCriterion::class);
    }

    // /**
    //  * @return SubstitutionCriterion[] Returns an array of SubstitutionCriterion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SubstitutionCriterion
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
