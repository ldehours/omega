<?php

namespace App\Repository\Offer;

use App\Entity\Offer\CriterionValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CriterionValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method CriterionValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method CriterionValue[]    findAll()
 * @method CriterionValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CriterionValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CriterionValue::class);
    }

    // /**
    //  * @return CriteriaValue[] Returns an array of CriteriaValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CriteriaValue
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
