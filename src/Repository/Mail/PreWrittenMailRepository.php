<?php

namespace App\Repository\Mail;

use App\Entity\Mail\PreWrittenMail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PreWrittenMail|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreWrittenMail|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreWrittenMail[]    findAll()
 * @method PreWrittenMail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreWrittenMailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PreWrittenMail::class);
    }

    // /**
    //  * @return PreWrittenMail[] Returns an array of PreWrittenMail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PreWrittenMail
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
