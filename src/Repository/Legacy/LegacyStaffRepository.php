<?php

namespace App\Repository\Legacy;

use App\Entity\Legacy\LegacyStaff;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LegacyStaff|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegacyStaff|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegacyStaff[]    findAll()
 * @method LegacyStaff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegacyStaffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegacyStaff::class);
    }

    // /**
    //  * @return LegacyStaff[] Returns an array of LegacyStaff objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LegacyStaff
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
