<?php

namespace App\Repository\Legacy;

use App\Entity\Legacy\Legacy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Legacy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Legacy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Legacy[]    findAll()
 * @method Legacy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegacyRepository extends ServiceEntityRepository
{
    private $legacyRepository;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $entityManager)
    {
        parent::__construct($registry, Legacy::class);
        $this->legacyRepository = $entityManager;

    }

    /**
     * @return int|mixed|string
     */
    public function findByLegacyCFML()
    {
        $legacyRepository = $this->legacyRepository->getRepository(Legacy::class);
        return $legacyRepository
            ->createQueryBuilder('l')
            ->andWhere('l.idCfml <> 0')
            ->andWhere('l.idCfml IS NOT NULL')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Legacy[] Returns an array of Legacy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l . exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l . id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Legacy
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l . exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
