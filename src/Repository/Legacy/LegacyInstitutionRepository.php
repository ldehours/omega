<?php

namespace App\Repository\Legacy;

use App\Entity\Legacy\LegacyInstitution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LegacyInstitution|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegacyInstitution|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegacyInstitution[]    findAll()
 * @method LegacyInstitution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegacyInstitutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegacyInstitution::class);
    }

    // /**
    //  * @return LegacyInstitution[] Returns an array of LegacyInstitution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LegacyInstitution
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
