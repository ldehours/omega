<?php

namespace App\Repository\Legacy;

use App\Entity\Legacy\LegacyCriterion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LegacyCriterion|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegacyCriterion|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegacyCriterion[]    findAll()
 * @method LegacyCriterion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegacyCriterionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegacyCriterion::class);
    }

    // /**
    //  * @return LegacyCriterion[] Returns an array of LegacyCriterion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LegacyCriterion
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
