<?php


namespace App\Repository\Website;


use App\Entity\Core\PersonNatural;
use App\Entity\Website\User;
use App\Service\Database;
use App\Service\DatabaseManager;
use App\Service\DatabaseResult;
use App\Utils\DateTime;
use App\Service\Result;

class UserRepository
{
    /**
     * @param User $user
     * @param DatabaseManager $databaseManager
     * @return Result
     * @throws \Exception
     */
    public function insert(User $user, DatabaseManager $databaseManager): Result
    {
        // Guardian
        if ($user->getLogin() == null
            || $user->getPass() == null
            || $user->getStatut() == null
        ) {
            return new Result(false, "L'une des propriétés suivantes est fausse : 
            Identifiant (" . $user->getLogin() . "), 
            Mot de passe (" . $user->getPass() . "), 
            Statut (" . $user->getStatut() . ")");
        }

        // Check login already exist
        /*
         * @var DatabaseResult[] $results
         */
        $results = $databaseManager->query("SELECT * FROM media_sante.ms_users WHERE login LIKE '" . $user->getLogin() . "'");

        if (DatabaseManager::areExpectedNumberLines($results, 0, ">")) {
            return new Result(false,
                "un compte site est déjà lié à cette adresse mail: " . $user->getLogin() . " existe déjà");
        }

        $results = $databaseManager->query("INSERT INTO media_sante.ms_users (datecrea,login,pass,statut) 
                    VALUES ('" . $user->getDatecrea()->getEnglishFormat() . "','" . $user->getLogin() . "','" . $user->getPass() . "','" . $user->getStatut() . "')",
            false);

        if (DatabaseManager::areExpectedNumberLines($results, 1, "!=")) {
            return new Result(false, "INSERT INTO media_sante.ms_users (datecrea,login,pass,statut) 
                    VALUES ('" . $user->getDatecrea()->getEnglishFormat() . "','" . $user->getLogin() . "','" . $user->getPass() . "','" . $user->getStatut() . "')");
        }

        /**
         * @var DatabaseResult $result
         */
        $user = $this->getByLogin($user->getLogin(), $databaseManager->getDatabases()->first());

        return new Result(true, "OK", array($user));
    }

    /**
     * @param string $login
     * @param Database $database
     * @return User|null
     */
    public function getByLogin(string $login, Database $database): ?User
    {
        $result = $database->query("SELECT * FROM media_sante.ms_users WHERE login LIKE '" . $login . "'")->getData();
        if (count($result) == 0) {
            return null;
        }
        $onceResult = $result[0];

        $user = new User();
        foreach ($onceResult as $property => $value) {
            if (in_array($property, array("datecrea", "cus_envoi", "ut"))) {
                $value = DateTime::createFromString($value);
            }
            $func = "set" . ucwords($property);
            $user->$func($value);
        }
        return $user;

    }

    /**
     * @param int $id
     * @param DatabaseManager $databaseManager
     * @return Result
     */
    public function deleteById(int $id, DatabaseManager $databaseManager): Result
    {
        /*
         * @var DatabaseResult[] $results
         */
        $results = $databaseManager->query("DELETE FROM media_sante.ms_users WHERE id = " . $id,false);

        if (!DatabaseManager::areExpectedNumberLines($results, 1, "=<")) {
            return new Result(false, "Erreur plus d'une ligne ont été supprimées !");
        }
        return new Result(true, "OK");

    }

    /*
     * @param PersonNatural $personNatural
     * @param Database $database
     * @return string|null
     */
    public function getPasswordByPersonNatural(PersonNatural $personNatural, Database $database): ?string
    {
        if ($personNatural->getWebsiteId() == null) {
            return null;
        }

        $result = $database->query('SELECT pass FROM ms_users WHERE id=' . $personNatural->getWebsiteId())->getData()[0];
        return $result->pass;
    }

    /**
     * @param DatabaseManager $databaseManager
     * @param PersonNatural $personNatural
     * @return array|null
     */
    public function updateCusEnvoi(DatabaseManager $databaseManager, PersonNatural $personNatural): ?array
    {
        if ($personNatural->getWebsiteId() == null) {
            return null;
        }

        $results = $databaseManager->query("UPDATE ms_users SET `cus_envoi` = NOW() WHERE id=" . $personNatural->getWebsiteId(),
            false);

        return $results;
    }

    public function updateLogin(DatabaseManager $databaseManager, PersonNatural $personNatural): ?array
    {
        if ($personNatural->getWebsiteId() == null) {
            return null;
        }

        $results = $databaseManager->query("UPDATE ms_users SET `login` = '" . $personNatural->getDefaultMail()->getAddress() . "' WHERE id=" . $personNatural->getWebsiteId(),
            false);

        return $results;
    }
}