<?php


namespace App\Repository\Website;


use App\Entity\Website\Coord;
use App\Service\Database;
use App\Service\DatabaseManager;
use App\Utils\DateTime;
use App\Service\Result;

class CoordRepository
{
    /**
     * @param Coord $coord
     * @param DatabaseManager $databaseManager
     * @return Result
     */
    public function insert(Coord $coord, DatabaseManager $databaseManager): Result
    {
        // Guardian
        if ($coord->getId() == null
            || $coord->getNom() == null
            || $coord->getPrenom() == null
            || $coord->getAdresse() == null
            || $coord->getCp() == null
            || $coord->getVille() == null
        ) {
            return new Result(false,
                "L'une des propriétés suivantes est fausse : 
                    ID (" . $coord->getId() . "), 
                    Nom (" . $coord->getNom() . "), 
                    Prenom (" . $coord->getPrenom() . "), 
                    Adresse (" . $coord->getAdresse() . "), 
                    CodePostal (" . $coord->getCp() . "), 
                    Ville (" . $coord->getVille() . ")
                    ");
        }

        // Check if login already exists
        $results = $databaseManager->query("SELECT * FROM media_sante.ms_coord WHERE id=" . $coord->getId());

        if (DatabaseManager::areExpectedNumberLines($results, 0, ">")) {
            return new Result(false, "L'ID site " . $coord->getId() . " existe déjà dans coord");
        }

        $results = $databaseManager->query("INSERT INTO media_sante.ms_coord (id,nom,prenom,adresse,ad2,ad3,cp,ville,tel,port,fax,lic,rpps,odm,annee_naissance) 
                    VALUES ('" . $coord->getId() . "','" . $coord->getNom() . "','" . $coord->getPrenom() . "','" . $coord->getAdresse() . "','" . $coord->getAd2() . "','" . $coord->getAd3() . "','" . $coord->getCp() . "','" . $coord->getVille() . "','" . $coord->getTel() . "','" . $coord->getPort() . "','" . $coord->getFax() . "','" . $coord->getLic() . "','" . $coord->getRpps() . "','" . $coord->getOdm() . "','" . $coord->getAnnee_Naissance() . "')",
            false);

        if (!DatabaseManager::areExpectedNumberLines($results, 1, "==")) {
            return new Result(false, "Echec de l'insertion dans la base");
        }

        return new Result(true, "OK");
    }

    /**
     * @param int $id
     * @param Database $database
     * @return Coord|null
     */
    public function getById(int $id, Database $database): ?Coord
    {
        $result = $database->query("SELECT * FROM media_sante.ms_coord WHERE id = '" . $id . "'")->getData();
        if (count($result) == 0) {
            return null;
        }
        $firstResult = $result[0];

        $coord = new Coord();
        foreach ($firstResult as $property => $value) {
            if (in_array($property, array("ut"))) {
                $value = DateTime::createFromString($value);
            }
            $func = "set" . ucwords($property);
            $coord->$func($value);
        }
        return $coord;

    }
}