<?php


namespace App\Repository\Website;


use App\Entity\Website\Adh;
use App\Service\Database;
use App\Utils\DateTime;

class AdhRepository
{
    /**
     * @param int $id
     * @param Database $database
     * @return Adh|null
     */
    public function getById(int $id, Database $database): ?Adh
    {
        $result = $database->query("SELECT * FROM media_sante.ms_adh WHERE id LIKE '" . $id . "'")->getData();
        if (count($result) == 0) {
            return null;
        }

        $firstResult = $result[0];
        $member = new Adh();

        foreach ($firstResult as $property => $value) {
            if (in_array($property,
                array(
                    "ml",
                    "ps",
                    "cs",
                    "cfml",
                    "bnc",
                    "lms",
                    "hotline_fiscale",
                    "re",
                    "pr",
                    "cus_valide",
                    "contact_nantu",
                    "ut"
                ))) {
                $value = DateTime::createFromString($value);
                $setter = "set" . ucwords($property);
                $member->$setter($value);
            }

        }
        return $member;
    }
}