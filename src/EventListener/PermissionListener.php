<?php

namespace App\EventListener;

use App\Entity\Core\Role;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PermissionListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * PermissionListener constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManager $entityManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManager $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * @param ControllerEvent $event
     */
    public function onKernelController(ControllerEvent $event)
    {
        // On vérifie si on appelle un controller de App
        // On vérifie que ce n'est pas la homepage pour ne pas boucler
        // On vérifie que l'utilisateur est connecté
        if (!is_array($event->getController())) {
            return;
        }
        if (strpos(get_class($event->getController()[0]), 'App\\Controller') != false
            && !$this->tokenStorage->getToken() instanceof AnonymousToken) {
            $controllerName = str_replace('App\\Controller\\', '',
                get_class($event->getController()[0]) . '\\' . $event->getController()[1]);
            $roleRepository = $this->entityManager->getRepository(Role::class);
            $isGranted = false;
            foreach ($this->tokenStorage->getToken()->getUser()->getRoles() as $role) {
                $result = $roleRepository->countRoleByRoleNameAndFeatureName($role, $controllerName);
                if ($result > 0) {
                    $isGranted = true;
                    break;
                }
            }
            if (!$isGranted) {
                throw new AccessDeniedHttpException();
            }
        }
    }
}