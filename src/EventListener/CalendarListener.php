<?php

namespace App\EventListener;

use App\Entity\Core\Agenda;
use App\Entity\Core\TimeRange;
use App\Repository\Core\AgendaRepository;
use App\Repository\Core\TimeRangeRepository;
use App\Utils\TimeRangeHelper;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\Time;

/**
 * Class CalendarListener
 * @package App\EventListener
 */
class CalendarListener
{
    /**
     * @var TimeRangeRepository
     */
    private $timeRangeRepository;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var TimeRangeHelper
     */
    private $timeRangeHelper;

    /**
     * CalendarListener constructor.
     * @param TimeRangeRepository $timeRangeRepository
     * @param UrlGeneratorInterface $router
     * @param TimeRangeHelper $timeRangeHelper
     */
    public function __construct(
        TimeRangeRepository $timeRangeRepository,
        UrlGeneratorInterface $router,
        TimeRangeHelper $timeRangeHelper
    ) {
        $this->timeRangeRepository = $timeRangeRepository;
        $this->router = $router;
        $this->timeRangeHelper = $timeRangeHelper;
    }

    /**
     * @param CalendarEvent $calendar
     */
    public function load(CalendarEvent $calendar): void
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();

        if(!empty($calendar->getFilters())){
            $agendaId = $calendar->getFilters()['agendaId'];
        } else {
            $request = Request::createFromGlobals();//on peut pas faire un 'Request $request' via les paramètres de la fonction du coup on crée la requête là.
            $agendaId = $request->get('agendaId');
        }

        $timeRanges = $this->timeRangeRepository
            ->createQueryBuilder('t')
            ->where('t.startDate BETWEEN :start and :end')
            ->orWhere('t.endDate BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->andWhere('t.agenda = :agenda')
            ->setParameter('agenda', $agendaId)
            ->getQuery()
            ->getResult();
        /** @var TimeRange $timeRange */
        foreach ($timeRanges as $timeRange) {
            if($timeRange->getNature() == TimeRange::NATURE_OTHER && $timeRange->getDescription() != null && $timeRange->getDescription() != ""){
                $title = $timeRange->getDescription();
            }else{
                $title = TimeRange::LABEL[$timeRange->getNature()];
            }
            if($timeRange->getWish() !== null && $timeRange->getWish()->getIsModel()){
                continue;
            }
            $agendaEvent = new Event(
                $title,
                $timeRange->getStartDate(),
                $timeRange->getEndDate() // If the end date is null or not defined, an all day event is created.
            );

            $agendaEvent->setOptions($this->timeRangeHelper->getEventDataFromTimeRange($timeRange));

            $calendar->addEvent($agendaEvent);
        }
    }
}