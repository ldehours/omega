<?php


namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class SnapshotSaveCommand extends Command
{
    protected static $defaultName = "app:snapshot:save";
    private $entityManager;
    private $kernel;

    /**
     * SnapshotSaveCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param KernelInterface $kernel
     */
    public function __construct(EntityManagerInterface $entityManager, KernelInterface $kernel)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->kernel = $kernel;
    }

    protected function configure()
    {
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* liste et ordre de sauvegarde des entités dans le fichier de configuration app/snapshotable-entities.json */
        $rootDir = $this->kernel->getProjectDir();

        if (!is_readable($rootDir . '/public/snapshots/')) {
            @mkdir($rootDir . '/public/snapshots/');
        }

        $snapshotConfigFile = $rootDir . '/config/app/snapshotable-entities.json';

        if (file_exists($snapshotConfigFile)) {
            $dataSnapshot = json_decode(file_get_contents($snapshotConfigFile), true);

            if ($dataSnapshot != "") {
                $entitiesToSave = $dataSnapshot['entitiesToSave'];

                $snapshotFile = $rootDir . '/public/snapshots/snapshot_' . date('YmdHis') . '.json';
                $output->writeln("creating snapshot...");
                $export = [];

                foreach ($entitiesToSave as $entity => $classPath) {
                    $export['entitySaved'][$entity] = $classPath;/* listes des entités sauvegardées à écrire dans le JSON */
                    $output->writeln("finding all ".$classPath);
                    $export['data'][$entity] = serialize($this->entityManager->getRepository($classPath)->findAll());
                    $output->writeln(substr($classPath, strripos($classPath, '\\') + 1) . " saved. ");
                }

                $filesystem = new Filesystem();
                $filesystem->dumpFile($snapshotFile, json_encode($export));
                $output->writeln("snapshot created !");

            } else {
                $output->writeln("Configuration file for snapshot is empty!");
            }
        } else {
            $output->writeln("No configuration file found for snapshot !");
        }
    }
}


