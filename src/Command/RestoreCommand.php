<?php

namespace App\Command;

use App\Entity\Core\Agenda;
use App\Entity\Core\Department;
use App\Entity\Core\JobName;
use App\Entity\Core\MedicalSpecialty;
use App\Entity\Core\MedicalSpecialtyQualification;
use App\Entity\Core\Origin;
use App\Entity\Core\PersonAccess;
use App\Entity\Core\Region;
use App\Entity\Core\Service;
use App\Entity\Core\Staff;
use App\Entity\Core\Telephone;
use App\Entity\Legacy\LegacyCriterion;
use App\Entity\Offer\Canvas;
use App\Entity\Offer\Criterion;
use App\Entity\Offer\CriterionPossibleValue;
use App\Entity\Offer\SubstitutionType;
use App\Repository\Core\DepartmentRepository;
use App\Repository\Core\JobNameRepository;
use App\Repository\Core\MedicalSpecialtyQualificationRepository;
use App\Repository\Core\MedicalSpecialtyRepository;
use App\Repository\Core\OriginRepository;
use App\Repository\Core\PersonAccessRepository;
use App\Repository\Core\RegionRepository;
use App\Repository\Core\ServiceRepository;
use App\Repository\Core\StaffRepository;
use App\Repository\Offer\CriterionRepository;
use App\Repository\Offer\SubstitutionTypeRepository;
use App\Utils\DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RestoreCommand extends Command
{
    protected static $defaultName = 'app:restore';
    private $entityManager;
    private $passwordEncoder;
    private $staffRepository;
    private $serviceRepository;
    private $medicalSpecialtyQualificationRepository;
    private $jobNameRepository;
    private $originRepository;
    private $substitutionRepository;
    private $criterionRepository;
    private $substitutionTypeRepository;
    private $medicalSpecialtyRepository;
    private $personAccessRepository;
    private $regionRepository;
    private $departmentRepository;
    private $kernel;

    public const SERVICES = [
        ['name' => 'Informatique', 'codification' => 'IT', 'roles' => ['ROLE_INFORMATIQUE', 'ROLE_ROOT']],
        ['name' => 'CFML', 'codification' => 'CFML', 'roles' => ['ROLE_CFML']],
        ['name' => 'Commercial', 'codification' => 'Comm', 'roles' => ['ROLE_COMMERCIAL']],
        ['name' => 'Chargés de recrutement', 'codification' => 'CR', 'roles' => ['ROLE_CHARGE_DE_RECRUTEMENT']],
        ['name' => 'Direction', 'codification' => 'Dir', 'roles' => ['ROLE_DIRECTION']],
        ['name' => 'Comptabilité', 'codification' => 'Compta', 'roles' => ['ROLE_COMPTABLE']],
    ];

    /**
     * RestoreCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param StaffRepository $staffRepository
     * @param ServiceRepository $serviceRepository
     * @param MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository
     * @param JobNameRepository $jobNameRepository
     * @param OriginRepository $originRepository
     * @param SubstitutionTypeRepository $substitutionRepository
     * @param CriterionRepository $criterionRepository
     * @param SubstitutionTypeRepository $substitutionTypeRepository
     * @param MedicalSpecialtyRepository $medicalSpecialtyRepository
     * @param PersonAccessRepository $personAccessRepository
     * @param RegionRepository $regionRepository
     * @param DepartmentRepository $departmentRepository
     * @param KernelInterface $kernel
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        StaffRepository $staffRepository,
        ServiceRepository $serviceRepository,
        MedicalSpecialtyQualificationRepository $medicalSpecialtyQualificationRepository,
        JobNameRepository $jobNameRepository,
        OriginRepository $originRepository,
        SubstitutionTypeRepository $substitutionRepository,
        CriterionRepository $criterionRepository,
        SubstitutionTypeRepository $substitutionTypeRepository,
        MedicalSpecialtyRepository $medicalSpecialtyRepository,
        PersonAccessRepository $personAccessRepository,
        RegionRepository $regionRepository,
        DepartmentRepository $departmentRepository,
        KernelInterface $kernel
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->staffRepository = $staffRepository;
        $this->serviceRepository = $serviceRepository;
        $this->medicalSpecialtyQualificationRepository = $medicalSpecialtyQualificationRepository;
        $this->jobNameRepository = $jobNameRepository;
        $this->originRepository = $originRepository;
        $this->substitutionRepository = $substitutionRepository;
        $this->criterionRepository = $criterionRepository;
        $this->substitutionTypeRepository = $substitutionTypeRepository;
        $this->medicalSpecialtyRepository = $medicalSpecialtyRepository;
        $this->personAccessRepository = $personAccessRepository;
        $this->regionRepository = $regionRepository;
        $this->departmentRepository = $departmentRepository;
        $this->kernel = $kernel;
    }

    protected function configure()
    {
        $this->setDescription('Restore saved values');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (count($this->serviceRepository->findAll()) == 0) {
            $this->createService();
            $output->writeln('Service created !');
        } else {
            $output->writeln('Service already exists !');
        }

        if (!$this->staffRepository->findByNickname('admin')) {
            $this->createAdmin();
            $output->writeln('Admin created with login:[admin] and password:[johndoe] !');
        } else {
            $output->writeln('Admin already exists !');
        }

        if (!$this->staffRepository->findByNickname('system')) {
            $this->createSystem();
            $output->writeln('System created !');
        } else {
            $output->writeln('System already exists !');
        }

        if (count($this->medicalSpecialtyQualificationRepository->findAll()) == 0) {
            $this->createMedicalSpecialtyQualifications();
            $output->writeln('Medical specialty qualification created !');
        } else {
            $output->writeln('Medical specialty qualification already exists !');
        }

        if (count($this->medicalSpecialtyRepository->findAll()) == 0) {
            $this->createMedicalSpecialty();
            $output->writeln('Medical specialties created !');
        } else {
            $output->writeln('Medical specialties already exists !');
        }

        if (count($this->jobNameRepository->findAll()) == 0) {
            $this->createJobNames();
            $output->writeln('JobNames created !');
        } else {
            $output->writeln('JobNames already exists !');
        }

        if (count($this->originRepository->findAll()) == 0) {
            $this->createOrigins();
            $output->writeln('Origins created !');
        } else {
            $output->writeln('Origins already exists !');
        }

        if (count($this->substitutionRepository->findAll()) == 0) {
            $this->createSubstitutionType();
            $output->writeln('SubsitutionsTypes created !');
        } else {
            $output->writeln('SubsitutionsTypes already exists !');
        }

        if (count($this->criterionRepository->findAll()) == 0) {
            $this->createCriteria();
            $output->writeln('Criterions created !');
        } else {
            $output->writeln('Criterions already exists !');
        }

        if (count($this->personAccessRepository->findAll()) == 0) {
            $this->createAccesses();
            $output->writeln("PersonAccess created !");
        } else {
            $output->writeln("PersonAccess already exists !");
        }

        if (count($this->regionRepository->findAll()) == 0) {
            $this->createRegions();
            $output->writeln("Regions created !");
        } else {
            $output->writeln("regions already exists !");
        }

        if (count($this->departmentRepository->findAll()) == 0) {
            $this->createDepartments();
            $output->writeln("Departments created !");
        } else {
            $output->writeln("Departments already exists !");
        }

        $output->writeln('Restoring data completed!');
    }

    public function createMedicalSpecialty(): void
    {
        $medicalSpecialty = new MedicalSpecialty();
        $medicalSpecialty->setJobTitle('Médecin des ordinateurs');
        $medicalSpecialty->setOfficialLabel('Ordinateurologue');
        $medicalSpecialty->addRelatedType($this->medicalSpecialtyQualificationRepository->findOneByLabel('Spécialité'));
        $medicalSpecialty->addRelatedType($this->medicalSpecialtyQualificationRepository->findOneByLabel('Formation'));
        $medicalSpecialty->addRelatedType($this->medicalSpecialtyQualificationRepository->findOneByLabel('Expérience'));
        $medicalSpecialty->addRelatedType($this->medicalSpecialtyQualificationRepository->findOneByLabel('Compétence'));
        $medicalSpecialty->addRelatedType($this->medicalSpecialtyQualificationRepository->findOneByLabel('Capacité'));
        $this->entityManager->persist($medicalSpecialty);
        $this->entityManager->flush();
    }

    public function createService(): void
    {
        foreach (self::SERVICES as $infoService) {
            $service = new Service();
            $service->setName($infoService['name']);
            $service->setCodification($infoService['codification']);
            $this->entityManager->persist($service);
        }
        $this->entityManager->flush();
    }

    public function createAdmin(): void
    {
        $telephone = new Telephone();
        $telephone->setNumber('0606060606');
        $telephone->setPhoneType(Telephone::WORK);
        $staff = new Staff();
        $staff->setNickname('admin');
        $staff->setLastName('Sacha');
        $staff->setFirstName('Du bourg-palette');
        $staff->setEmail('admin@media-sante.com');
        $staff->setJobTitle('SuperAdmin');
        $staff->setContract(35);
        $staff->setDateStart(new DateTime());
        $staff->setDateOfHiring(new DateTime());
        $staff->setInternalPhone('');
        $staff->setGender(Staff::GENDER_MALE);
        $staff->addExternalPhone($telephone);
        $staff->setIp('10.113.101.62');
        $staff->setIsActive(true);
        $staff->setRoles(['ROLE_ROOT']);
        $staff->setPassword(
            $this->passwordEncoder->encodePassword(
                $staff,
                'johndoe'
            )
        );
        $staff->setAgenda(new Agenda());
        $staff->setService($this->serviceRepository->findOneBy([]));
        $staff->setIsArchived(false);
        $this->entityManager->persist($staff);
        $this->entityManager->flush();
    }

    public function createSystem(): void
    {
        $telephone = new Telephone();
        $telephone->setNumber('0606060606');
        $telephone->setPhoneType(Telephone::WORK);
        $staff = new Staff();
        $staff->setNickname('system');
        $staff->setLastName('Pika');
        $staff->setFirstName('Chu');
        $staff->setContract(29);
        $staff->setEmail('system@media-sante.com');
        $staff->setJobTitle('SuperSystem');
        $staff->setGender(Staff::GENDER_FEMALE);
        $staff->setInternalPhone('');
        $staff->setDateStart(new DateTime());
        $staff->setDateOfHiring(new DateTime());
        $staff->addExternalPhone($telephone);
        $staff->setIp('10.113.101.62');
        $staff->setIsActive(true);
        $staff->setIsArchived(false);
        $staff->setRoles(['ROLE_ROOT']);
        $staff->setPassword(
            $this->passwordEncoder->encodePassword(
                $staff,
                substr(md5(rand()), 0, 7)
            )
        );
        $staff->setAgenda(new Agenda());
        $staff->setService($this->serviceRepository->findOneBy([]));
        $this->entityManager->persist($staff);
        $this->entityManager->flush();
    }

    public function createMedicalSpecialtyQualifications(): void
    {
        $listMedicalSpecialtyQualifications = ['Spécialité', 'Formation', 'Expérience', 'Compétence', 'Capacité'];
        foreach ($listMedicalSpecialtyQualifications as $medicalSpecialtyQualificationLabel) {
            $medicalSpecialtyQualification = new MedicalSpecialtyQualification();
            $medicalSpecialtyQualification->setLabel($medicalSpecialtyQualificationLabel);
            $this->entityManager->persist($medicalSpecialtyQualification);
            $this->entityManager->flush();
        }
    }

    public function createJobNames(): void
    {
        $listJobNames = [
            'Médecin Installé effectuant des remplacements' => JobName::BASED,
            'Centre de Vaccination' => JobName::INSTITUTION,
            'Médecin Installé' => JobName::BASED,
            'Médecin Remplaçant/Candidat' => JobName::SUBSTITUTE,
            'Médecin Collaborateur' => JobName::BASED,
            'Médecin Collaborateur effectuant des remplacements' => JobName::BASED,
            'Médecin Retraité effectuant des remplacements' => JobName::SUBSTITUTE,
            'Médecin Retraité installé' => JobName::BASED,
            'Association' => JobName::INSTITUTION,
            'Centre Hospitalier' => JobName::INSTITUTION,
            'Centre de Santé' => JobName::INSTITUTION,
            'Clinique' => JobName::INSTITUTION,
            'HAD' => JobName::INSTITUTION,
            'HEPAD' => JobName::INSTITUTION,
            'Médicaux Sociaux' => JobName::INSTITUTION,
            'PMI' => JobName::INSTITUTION,
            'Polyclinique' => JobName::INSTITUTION,
            'SRR' => JobName::INSTITUTION,
            'Cabinet de groupe' => JobName::INSTITUTION,
            'Contact établissement' => JobName::INSTITUTION_FUNCTION
        ];
        foreach ($listJobNames as $key => $value) {
            $jobName = new JobName();
            $jobName->setLabel($key);
            $jobName->setClassification($value);
            $this->entityManager->persist($jobName);
        }
        $this->entityManager->flush();
    }

    public function createOrigins(): void
    {
        $listOrigins = [
            'Automatique' => [Origin::PERSON],
            'Création Compte Site' => [Origin::PERSON],
            'Appel en direct' => [Origin::PERSON, Origin::OFFER],
            'Téléchargement R10' => [Origin::PERSON],
            'Facebook' => [Origin::PERSON, Origin::OFFER],
            'Linkedin' => [Origin::PERSON, Origin::OFFER],
            'Viadeo' => [Origin::PERSON, Origin::OFFER],
            'Bouche à oreille' => [Origin::PERSON],
            'Commande LMS' => [Origin::PERSON],
            'Demande depuis le site' => [Origin::PERSON, Origin::OFFER],
            'Pige' => [Origin::PERSON, Origin::OFFER],
            'Parrainage' => [Origin::OFFER],
            'Parrainage SIHP' => [Origin::PERSON],
            'Parrainage SMMQ' => [Origin::PERSON],
            'Parrainage SRP IMG' => [Origin::PERSON],
            'Parrainage APIHL' => [Origin::PERSON],
            'Parrainage REAGJIR IDF' => [Origin::PERSON],
            'Indeed' => [Origin::PERSON],
        ];


        foreach ($listOrigins as $key => $value) {
            $origin = new Origin();
            $origin->setLabel($key);
            $origin->setusages($value);
            $origin->setIsActive(true);
            $this->entityManager->persist($origin);
            $this->entityManager->flush();
        }

        $listOriginsInactive = [
            'Nantu : Autres' => [Origin::PERSON, Origin::OFFER],
            'Nantu : Contact HMS' => [Origin::PERSON],
            'Nantu : Spontané' => [Origin::PERSON],
            'Nantu : Journaux' => [Origin::PERSON],
            'Nantu : Transfert MA/CR' => [Origin::PERSON],
            'Nantu : Parrainage' => [Origin::PERSON],
            'Nantu : Internet' => [Origin::PERSON],
            'Nantu : ANAPL' => [Origin::PERSON],
            'Nantu : Transfert I/R' => [Origin::PERSON],
            'Nantu : Parrainage SRP IMG' => [Origin::PERSON],
            'Nantu : Parrainage SIHP' => [Origin::PERSON]
        ];

        foreach ($listOriginsInactive as $key => $value) {
            $origin = new Origin();
            $origin->setLabel($key);
            $origin->setusages($value);
            $origin->setIsActive(false);
            $this->entityManager->persist($origin);
            $this->entityManager->flush();
        }
    }

    public function createSubstitutionType(): void
    {
        foreach (SubstitutionType::TYPES_POSSIBLE as $headType => $substitutionTypesPossible) {
            foreach ($substitutionTypesPossible as $substitutionTypePossible) {
                $substitutionType = new SubstitutionType();
                $substitutionType->setSubstitutionType($substitutionTypePossible);
                $this->entityManager->persist($substitutionType);
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @throws Exception
     */
    public function createCriteria(): void
    {
        $bool = Criterion::TYPE_BOOL;
        $integer = Criterion::TYPE_INTEGER;
        $select = Criterion::TYPE_SELECT;
        $date = Criterion::TYPE_DATE;
        $specialty = Criterion::TYPE_SPECIALTY;
        $weekChoice = Criterion::TYPE_CHOICE_WEEK;
        $text = Criterion::TYPE_TEXT;
        $file = Criterion::TYPE_FILE;
        $multiSelect = Criterion::TYPE_MULTI_SELECT;
        $location = Criterion::TYPE_LOCATION;
        $entity = Criterion::TYPE_ENTITY;
        $input = Criterion::TYPE_INPUT;
        $json = Criterion::TYPE_JSON;

        $postesSalaries = SubstitutionType::TYPE_SALARIED_POSITION;
        $remplacementsLiberaux = SubstitutionType::TYPE_LIBERAL_REPLACEMENT;
        $cessionsAssociations = SubstitutionType::TYPE_SELL_ASSOCIATION;

        $liberalReplacementPunctual = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_PUNCTUAL;//pontuel
        $liberalReplacementRegular = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_REGULAR;//régulier
        $liberalReplacementOnCall = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_ON_CALL;//de garde
        $liberalReplacementLongTime = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_LONG_TIME;//longue durée
        $liberalReplacementPartnership = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_PARTNERSHIP;//collaboration
        $liberalReplacementTemporaryPartnership = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_TEMPORARY_PARTNERSHIP;//asso temporaire
        $liberalReplacementDeceasedReplacement = SubstitutionType::TYPE_LIBERAL_REPLACEMENT_DECEASED_REPLACEMENT;//tenue de poste

        $salariedPositionFixedTermContract = SubstitutionType::TYPE_SALARIED_POSITION_FIXED_TERM_CONTRACT;//CDD
        $salariedPositionPermanentContract = SubstitutionType::TYPE_SALARIED_POSITION_PERMANENT_CONTRACT;//CDI
        $salariedPositionShift = SubstitutionType::TYPE_SALARIED_POSITION_SHIFT;//Vacations

        $sellAssociationSession = SubstitutionType::TYPE_SELL_ASSOCIATION_SESSION;//Cession de cabinet
        $sellAssociationDoctorPatnership = SubstitutionType::TYPE_SELL_ASSOCIATION_DOCTOR_PARTNERSHIP;//association de médecins
        $sellAssociationLocation = SubstitutionType::TYPE_SELL_ASSOCIATION_LOCATION;//location de local professionnel
        $sellAssociationSelling = SubstitutionType::TYPE_SELL_ASSOCIATION_SELLING;//vente local professionnel

        $logiciels = [
            '123 SANTE',
            'Affid vital',
            'Almapro',
            'Altyse',
            'Axisanté',
            'Axilog',
            'Axisanté',
            'Coccilog',
            'Coxxi 32',
            'Crossway',
            'DBMED',
            'Dia',
            'Docware',
            'EASYPRAT',
            'Eglantine',
            'EO medecins',
            'Hellodoc',
            'Hypermed',
            'Magicmed',
            'Medi 2000',
            'Medi 4000',
            'Médicalp',
            'Medicawin',
            'Mediclick',
            'Médigest',
            'Medimust',
            'Médistory',
            'Médiwin',
            'Megabase',
            'Ordogest',
            'Ordovital',
            'Papyrus',
            'Pratis Santé',
            'Shaman',
            'Télétransmission',
            'Thales',
            'Fisimed',
            'Glantine'
        ];
        /*
          Format du tableau :
            1000 => [
                "Date de début" => [
                    "label" => "dummy label",
                    "offer" => [
                        $variable => ["criterion_type" => $type, "possible_value" => []]
                        ...
                    ],
                    "wish" => [
                        $variable => ["criterion_type" => $type, "possible_value" => []]
                        ...
                    ],
                    "dependency" => [],
                    "relevantable" => true,
                ]
            ],
         * */
        $criteria = [
            1 => [
                'Secteur' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'I',
                                'II',
                                'Hors Convention'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            2 => [
                'Zone franche' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationSession => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationLocation => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationSelling => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']]
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationSession => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationLocation => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationSelling => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                    ],
                    'relevantable' => true,
                ]
            ],
            3 => [
                'Secrétariat' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Telephonique',
                                'Au cabinet',
                                'Internet',
                                'Pas de secrétariat'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            4 => [
                'Rétrocession minimum' => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            5 => [
                'Cabinet informatisé' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ]
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ]
                    ],
                    'relevantable' => true,
                ]
            ],
            6 => [
                'Atouts' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ]
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Piscine',
                                'Balneaire',
                                'Thermal',
                                'Ski',
                                'Touristique',
                                'Bord de mer'
                            ]
                        ]
                    ],
                    'relevantable' => true,
                ]
            ],
            7 => [
                'Type de Clientèle' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ]
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Diversifiee',
                                'Agee',
                                'Enfants',
                                'Touristique',
                                'Femme'
                            ]
                        ]
                    ],
                    'relevantable' => true,
                ]
            ],
            8 => [
                'Gardes' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            9 => [
                "Nombre d'actes select" => [
                    'label' => "Nombre d'actes /jour",
                    'offer' => [],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 valeur',
                                '1 intervalle'
                            ]
                        ]
                    ],
                    'headDependency' => [10, 11, 12, 13],
                    'relevantable' => true,
                ]
            ],
            10 => [
                "Nombre d'actes pondération" => [
                    'offer' => [],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'au plus',
                                'au moins'
                            ]
                        ]
                    ],
                    'dependency' => [9 => '1 valeur'],
                    'relevantable' => false,
                ]
            ],
            11 => [
                "Nombre d'actes valeur" => [
                    'offer' => [],
                    'wish' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $sellAssociationSession => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $integer, 'possible_value' => []]
                    ],
                    'dependency' => [9 => '1 valeur'],
                    'relevantable' => false,
                ]
            ],
            12 => [
                "Nombre d'actes intervalle min" => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'dependency' => [9 => '1 intervalle'],
                    'relevantable' => false,
                ]
            ],
            13 => [
                "Nombre d'actes intervalle max" => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'dependency' => [9 => '1 intervalle'],
                    'relevantable' => false,
                ]
            ],
            14 => [
                'Motorisé' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Prêt de voiture',
                                'Non obligatoire'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            15 => [
                'Visites (%)' => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '-5%',
                                '-10%',
                                '-20%'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '-5%',
                                '-10%',
                                '-20%'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '-5%',
                                '-10%',
                                '-20%'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '-5%',
                                '-10%',
                                '-20%'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '-5%',
                                '-10%',
                                '-20%'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '-5%',
                                '-10%',
                                '-20%'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            16 => [
                'Logement' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Non logé',
                                'Domicile',
                                'Hôtel',
                                'Gîtes rural',
                                'Indépendant',
                                'Au cabinet',
                                "Chambre d'hôte"
                            ]
                        ],
                    ],
                    'headDependency' => [17],
                    'relevantable' => true,
                ]
            ],
            17 => [
                'Logement nbr' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '1 Personne',
                                '2 Personnes',
                                '3 Personnes',
                                '4 Personnes et +'
                            ]
                        ],
                    ],
                    'dependency' => [
                        16 => [
                            'Domicile',
                            'Hôtel',
                            'Gîtes rural',
                            'Indépendant',
                            'Au cabinet',
                            "Chambre d'hôte"
                        ]
                    ],
                    'relevantable' => true,
                ]
            ],
            18 => [
                'Département' => [
                    'offer' => [],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $location,
                            'possible_value' => [
                                './departements.json',
                                './regions.json'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            19 => [
                'Nombre de Km' => [
                    'offer' => [],
                    'wish' => [],// select oui non
                    'relevantable' => false,
                    'headDependency' => [20, 21],
                ]
            ],
            20 => [
                'Nombre de Km value' => [
                    'offer' => [],
                    'wish' => [],// integer
                    'relevantable' => false,
                    'dependency' => [19 => 'Oui']
                ]
            ],
            21 => [
                'Nombre de Km Code Postal' => [
                    'offer' => [],
                    'wish' => [],// input
                    'relevantable' => false,
                    'dependency' => [19 => 'Oui']
                ]
            ],
            22 => [
                'Activité' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ]
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementOnCall => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationSession => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Tous secteurs',
                                'Rural',
                                'Semi-Rural',
                                'Urbain'
                            ]
                        ]
                    ],
                    'relevantable' => true,
                ]
            ],
            23 => [
                'Cabinet de groupe' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationSession => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationLocation => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationSelling => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']]
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationSession => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationLocation => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationSelling => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']]
                    ],
                    'relevantable' => true,
                ]
            ],
            24 => [
                'Consultation' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Sur RDV',
                                'Libres'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            25 => [
                'Astreintes' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            26 => [
                'Intérêt pour' => [
                    'offer' => [],
                    'wish' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $entity,
                            'possible_value' => MedicalSpecialty::class . '::officialLabel'
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $entity,
                            'possible_value' => MedicalSpecialty::class . '::officialLabel'
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $entity,
                            'possible_value' => MedicalSpecialty::class . '::officialLabel'
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            27 => [
                'Temps de travail recherché' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Temps complet',
                                'Temps partiel',
                                "jusqu'à 30%",
                                "jusqu'à 50%",
                                "jusqu'à 80%"
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Temps complet',
                                'Temps partiel',
                                "jusqu'à 30%",
                                "jusqu'à 50%",
                                "jusqu'à 80%"
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Temps complet',
                                'Temps partiel',
                                "jusqu'à 30%",
                                "jusqu'à 50%",
                                "jusqu'à 80%"
                            ]
                        ],
                    ],
                    'wish' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Temps complet',
                                'Temps partiel',
                                "jusqu'à 30%",
                                "jusqu'à 50%",
                                "jusqu'à 80%"
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Temps complet',
                                'Temps partiel',
                                "jusqu'à 30%",
                                "jusqu'à 50%",
                                "jusqu'à 80%"
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Temps complet',
                                'Temps partiel',
                                "jusqu'à 30%",
                                "jusqu'à 50%",
                                "jusqu'à 80%"
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            28 => [
                'Durée de contrat recherchée' => [
                    'offer' => [],
                    'wish' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Jours ponctuels',
                                '1 à 2 semaines',
                                '3 à 4 semaines',
                                '1 mois et +'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Jours ponctuels',
                                '1 à 2 semaines',
                                '3 à 4 semaines',
                                '1 mois et +'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Jours ponctuels',
                                '1 à 2 semaines',
                                '3 à 4 semaines',
                                '1 mois et +'
                            ]
                        ],
                    ],
                    'relevantable' => false,
                ]
            ],
            29 => [
                'Nombre de lits à gérer' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'wish' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'relevantable' => true,
                ]
            ],
            30 => [
                "Types d'établissement" => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'EPHAD',
                                'HAD',
                                'SSR',
                                'Centre de santé',
                                'Centre hospitalier'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'EPHAD',
                                'HAD',
                                'SSR',
                                'Centre de santé',
                                'Centre hospitalier'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'EPHAD',
                                'HAD',
                                'SSR',
                                'Centre de santé',
                                'Centre hospitalier'
                            ]
                        ],
                    ],
                    'wish' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'EPHAD',
                                'HAD',
                                'SSR',
                                'Centre de santé',
                                'Centre hospitalier'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'EPHAD',
                                'HAD',
                                'SSR',
                                'Centre de santé',
                                'Centre hospitalier'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'EPHAD',
                                'HAD',
                                'SSR',
                                'Centre de santé',
                                'Centre hospitalier'
                            ]
                        ],
                    ],//multiselect
                    'relevantable' => true,
                ]
            ],
            31 => [
                "Secteur d'établissement" => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Privé',
                                'Public'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Privé',
                                'Public'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => ['Privé', 'Public']
                        ],
                    ],
                    'wish' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Privé',
                                'Public'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Privé',
                                'Public'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                'Privé',
                                'Public'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            32 => [
                'Connaissance logiciel' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $salariedPositionShift => ['criterion_type' => $multiSelect, 'possible_value' => $logiciels],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => $logiciels
                        ],
                        $salariedPositionShift => ['criterion_type' => $multiSelect, 'possible_value' => $logiciels],
                    ],
                    'relevantable' => true,
                ]
            ],
            33 => [
                'Prestation base' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Annuel',
                                'Mensuel',
                                'Journalier',
                                'Horaire'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Annuel',
                                'Mensuel',
                                'Journalier',
                                'Horaire'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Annuel',
                                'Mensuel',
                                'Journalier',
                                'Horraire'
                            ]
                        ],
                    ],
                    'wish' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Annuel',
                                'Mensuel',
                                'Journalier',
                                'Horaire'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Annuel',
                                'Mensuel',
                                'Journalier',
                                'Horaire'
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Annuel',
                                'Mensuel',
                                'Journalier',
                                'Horaire'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            34 => [
                'Prestation montant' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'wish' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'relevantable' => true,
                ]
            ],
            35 => [
                'Pluralité remplaçants' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            36 => [
                'Modularité période minimum (nb de jours)' => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementOnCall => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            37 => [
                'Durée de diffusion' => [
                    'offer' => [
                        $sellAssociationSession => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '3 mois',
                                '6 mois',
                                '12 mois',
                                'Illimitée'
                            ]
                        ],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '3 mois',
                                '6 mois',
                                '12 mois',
                                'Illimitée'
                            ]
                        ],
                        $sellAssociationLocation => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '3 mois',
                                '6 mois',
                                '12 mois',
                                'Illimitée'
                            ]
                        ],
                        $sellAssociationSelling => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                '3 mois',
                                '6 mois',
                                '12 mois',
                                'Illimitée'
                            ]
                        ]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            38 => [
                'Jour(s) de fermeture' => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $weekChoice, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $weekChoice, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $weekChoice, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $weekChoice, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $weekChoice,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $weekChoice,
                            'possible_value' => []
                        ],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            39 => [
                'Télétransmission' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'wish' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                    ],
                    'relevantable' => true,
                ]
            ],
            40 => [
                'Avis Remplaçants' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementRegular => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementOnCall => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['Oui', 'Non']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            41 => [
                'Nombre de salariés' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $integer,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $integer,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => ['Oui', 'Non']],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            42 => [
                'Atouts Etablissement' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $text, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            43 => [
                'Services en place' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $text, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            44 => [
                'Nb Heures Hebdo' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            45 => [
                "Poste au sein de l'équipe" => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                    'headDependency' => [46, 47],
                ]
            ],
            46 => [
                "Nb medecins dans l'équipe" => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'dependency' => [45 => 'Oui'],
                    'relevantable' => false,
                ]
            ],
            47 => [
                'Equipe pluridisciplinaire' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $bool,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $bool,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $salariedPositionShift => ['criterion_type' => $bool, 'possible_value' => ['Oui', 'Non']],
                    ],
                    'wish' => [],//nothing
                    'dependency' => [45 => 'Oui'],
                    'relevantable' => false,
                ]
            ],
            48 => [
                'Mission(s) du poste' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $text, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            49 => [
                'Avantages sociaux' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $text, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $text, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            50 => [
                'Profil recherché' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                "Thésé et inscrit au conseil de l'ordre",
                                "Titulaire d'une licence de remplacement en cours de validité"
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                "Thésé et inscrit au conseil de l'ordre",
                                "Titulaire d'une licence de remplacement en cours de validité"
                            ]
                        ],
                        $salariedPositionShift => [
                            'criterion_type' => $multiSelect,
                            'possible_value' => [
                                "Thésé et inscrit au conseil de l'ordre",
                                "Titulaire d'une licence de remplacement en cours de validité"
                            ]
                        ],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            51 => [
                'Rémunération du poste' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionPermanentContract => ['criterion_type' => $integer, 'possible_value' => []],
                        $salariedPositionShift => ['criterion_type' => $integer, 'possible_value' => []],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => true,
                ]
            ],
            52 => [
                'Rémunération du poste (net/brut)' => [
                    'offer' => [
                        $salariedPositionFixedTermContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Net',
                                'Brut'
                            ]
                        ],
                        $salariedPositionPermanentContract => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Net',
                                'Brut'
                            ]
                        ],
                        $salariedPositionShift => ['criterion_type' => $select, 'possible_value' => ['Net', 'Brut']],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            53 => [
                'Ajouter une photo' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $file, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $file, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $file, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $file, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            54 => [
                'CA du cabinet' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $integer, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            55 => [
                'Echéance transaction' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $date, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $date, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $date, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $date, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            56 => [
                'Superficie du local professionnel' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $integer, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $integer, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            57 => [
                'Description du local professionnel' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $text, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $text, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $text, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $text, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            58 => [
                "Détails de l'offre" => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $text, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $text, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $text, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $text, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            59 => [
                'Successeur' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $input, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $input, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $input, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $input, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            60 => [
                "Perspectives d'avenir" => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $input, 'possible_value' => []],
                        $sellAssociationDoctorPatnership => ['criterion_type' => $input, 'possible_value' => []],
                        $sellAssociationLocation => ['criterion_type' => $input, 'possible_value' => []],
                        $sellAssociationSelling => ['criterion_type' => $input, 'possible_value' => []]
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            61 => [
                'Jours Réguliers' => [
                    'offer' => [
                        $liberalReplacementRegular => ['criterion_type' => $weekChoice, 'possible_value' => []],
                    ],
                    'wish' => [
                        $liberalReplacementRegular => ['criterion_type' => $weekChoice, 'possible_value' => []],
                    ],
                    'relevantable' => true,
                ]
            ],
            62 => [
                'Zone de revitalisation rurale' => [
                    'offer' => [
                        $sellAssociationSession => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationLocation => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationSelling => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']]
                    ],
                    'wish' => [
                        $sellAssociationSession => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationDoctorPatnership => [
                            'criterion_type' => $select,
                            'possible_value' => [
                                'Oui',
                                'Non'
                            ]
                        ],
                        $sellAssociationLocation => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']],
                        $sellAssociationSelling => ['criterion_type' => $select, 'possible_value' => ['Oui', 'Non']]
                    ],
                    'relevantable' => true,
                ]
            ],
            63 => [
                'Nb consultations minimum assuré' => [
                    'offer' => [
                        $liberalReplacementPunctual => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementRegular => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementLongTime => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementPartnership => ['criterion_type' => $integer, 'possible_value' => []],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $integer,
                            'possible_value' => []
                        ],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
            64 => [
                'Nb consult. mini. assuré pondération' => [
                    'offer' => [
                        $liberalReplacementPunctual => [
                            'criterion_type' => $select,
                            'possible_value' => ['assurés jour par jour', 'assurés sur la moyenne du remplacement']
                        ],
                        $liberalReplacementRegular => [
                            'criterion_type' => $select,
                            'possible_value' => ['assurés jour par jour', 'assurés sur la moyenne du remplacement']
                        ],
                        $liberalReplacementLongTime => [
                            'criterion_type' => $select,
                            'possible_value' => ['assurés jour par jour', 'assurés sur la moyenne du remplacement']
                        ],
                        $liberalReplacementPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => ['assurés jour par jour', 'assurés sur la moyenne du remplacement']
                        ],
                        $liberalReplacementTemporaryPartnership => [
                            'criterion_type' => $select,
                            'possible_value' => ['assurés jour par jour', 'assurés sur la moyenne du remplacement']
                        ],
                        $liberalReplacementDeceasedReplacement => [
                            'criterion_type' => $select,
                            'possible_value' => ['assurés jour par jour', 'assurés sur la moyenne du remplacement']
                        ],
                    ],
                    'wish' => [],//nothing
                    'relevantable' => false,
                ]
            ],
        ];
        foreach ($criteria as $key => $criterion_params) {
            $criterion = new Criterion();
            foreach ($criterion_params as $criterion_name => $criterion_param) {
                $criterion->setRelevantable($criterion_param['relevantable'] ?? false);
                $label = $criterion_name;
                if (!empty($criterion_param['label'])) {
                    $label = $criterion_param['label'];
                }
                $criterion->setLabel($label);
                if (!empty($criterion_param['dependency'])) {
                    if (!empty(array_values($criteria[array_keys($criterion_param['dependency'])[0]])[0]['label'])) {
                        $labelDependency = array_values($criteria[array_keys($criterion_param['dependency'])[0]])[0]['label'];
                    } else {
                        $labelDependency = array_keys($criteria[array_keys($criterion_param['dependency'])[0]])[0];
                    }
                    $dependencyCriterion = $this->criterionRepository->findOneByLabel($labelDependency);
                    if ($dependencyCriterion === null) {
                        throw new Exception("La dépendance n'a pas pu se faire pour le critère " . $criterion_name);
                    }
                    $criterion->setDependency([$dependencyCriterion->getId() => array_values($criterion_param['dependency'])[0]]);
                    $criterion->setCriterionHeadDependency($dependencyCriterion);
                }
                foreach (Canvas::CANVAS_TYPES as $nature) {
                    $currentNature = 'offer';
                    if ($nature === Canvas::CANVAS_WISH) {
                        $currentNature = 'wish';
                    }
                    if (!empty($criterion_param[$currentNature])) {
                        foreach ($criterion_param[$currentNature] as $substitutionType => $criterionPossibleValueDatas) {
                            if ($criterionPossibleValueDatas !== null) {
                                $criterionPossibleValue = new CriterionPossibleValue();
                                $criterionPossibleValue->setSubstitutionNature($nature);
                                $criterionPossibleValue->setCriterionType($criterionPossibleValueDatas['criterion_type']);
                                if (!empty($criterionPossibleValueDatas['possible_value'])) {
                                    $criterionPossibleValue->setPossibleValue(json_encode($criterionPossibleValueDatas['possible_value']));
                                } else {
                                    $criterionPossibleValue->setPossibleValue(json_encode([]));
                                }
                                $criterionPossibleValue->setCriterion($criterion);
                                $substitutionType = $this->substitutionTypeRepository->findOneBySubstitutionType($substitutionType);
                                if ($substitutionType !== null) {
                                    $criterionPossibleValue->setSubstitutionType($substitutionType);
                                    $this->entityManager->persist($criterionPossibleValue);
                                }
                            }
                        }
                    }
                }
            }
            $legacyCriterion = new LegacyCriterion();
            $legacyCriterion->setCriterion($criterion);
            $legacyCriterion->setIdentifier('C' . $key);// utile uniquement pour le convertisseur pour ... identifier le critère :)
            $this->entityManager->persist($criterion);
            $this->entityManager->persist($legacyCriterion);
            $this->entityManager->flush();
        }
    }

    protected function createAccesses(): void
    {
        foreach (PersonAccess::LABEL as $key => $value) {
            $right = new PersonAccess();
            $right->setLabel($value);
            $right->setType($key);
            $this->entityManager->persist($right);
            $this->entityManager->flush();
        }
    }

    protected function createRegions()
    {
        $regions = Json_decode(file_get_contents($this->kernel->getProjectDir() . '/public/regions.json'), true);
        foreach ($regions as $value) {
            $region = new Region();
            $region->setName($value['nom']);
            $region->setCode($value['code']);
            $this->entityManager->persist($region);
            $this->entityManager->flush();
        }
    }

    protected function createDepartments()
    {
        $departments = Json_decode(file_get_contents($this->kernel->getProjectDir() . '/public/departements.json'),
            true);
        foreach ($departments as $value) {
            $department = new Department();
            $department->setName($value['nom']);
            $department->setCode($value['code']);
            $region = $this->regionRepository->findOneBy(['code' => $value['codeRegion']]);
            $department->setRegion($region);
            $region->addDepartment($department);
            $this->entityManager->persist($department);
            $this->entityManager->flush();
        }
    }
}