<?php

namespace App\Command;

use App\Annotation\NoTranslation;
use App\Annotation\Translation;
use App\Entity\Core\Feature;
use App\Entity\Core\Role;
use App\Repository\Core\FeatureRepository;
use App\Repository\Core\RoleRepository;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class CreateRoleCommand extends Command
{
    protected static $defaultName = "app:role";
    private $entityManager;
    private $container;
    private $roleRepository;
    private $featureRepository;

    /**
     * CreateRoleCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface $container
     * @param RoleRepository $roleRepository
     * @param FeatureRepository $featureRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container,
        RoleRepository $roleRepository,
        FeatureRepository $featureRepository
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->roleRepository = $roleRepository;
        $this->featureRepository = $featureRepository;

    }

    protected function configure()
    {
        $this->setDescription("Generate the permission matrix");
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Create permission matrix start !");
        $permissionFile = $_ENV['PERMISSION_FILE_PATH'];

        if (file_exists($permissionFile)) {
            unlink($permissionFile);
        }

        $listTranslation = [];
        $roles = $this->container->getParameter('security.role_hierarchy.roles');
        $finder = new Finder();
        $finder->files()->in('src/Controller/');
        $rootRole = null;

        foreach ($roles as $role => $hierarchy) {
            if ($this->roleRepository->findBy(['name' => $role]) == null) {
                $newRole = new Role();
                $newRole->setName($role);
                $this->entityManager->persist($newRole);
                $this->entityManager->flush();
                if ($role == 'ROLE_ROOT') {
                    $rootRole = $newRole;
                }
            }
        }

        foreach ($finder as $file) {
            $controller = str_replace(["src", "/"], ["App", "\\"],
                    $file->getPath()) . "\\" . $file->getFilenameWithoutExtension();
            $reflection = new ReflectionClass($controller);
            foreach ($reflection->getMethods() as $function) {
                if ($function->class == $controller) {
                    $controllerPath = str_replace('App\\Controller\\', '',
                        $function->getDeclaringClass()->getName() . '\\' . $function->getName());
                    $annotation = new AnnotationReader();
                    $hasNoTranslation = $annotation->getMethodAnnotation($function, NoTranslation::class) != null;
                    $hasTranslation = $annotation->getMethodAnnotation($function, Translation::class) != null;
                    if (!$hasNoTranslation && $hasTranslation) {
                        $translation = $annotation->getMethodAnnotation($function, Translation::class);
                        if ($translation == null) {
                            throw new Exception('The controller ' . $controllerPath . ' does not contain an annotation Translation');
                        }
                    } else {
                        if ($hasNoTranslation && !$hasTranslation) {
                            $translation = new Translation(['value' => 'ajax_call ' . $controllerPath]);
                        } else {
                            throw new Exception('The controller ' . $controllerPath . ' does not contain neither an annotation Translation nor an annotation NoTranslation');
                        }
                    }
                    $listTranslation[$controllerPath] = $translation->getText();
                }
            }
        }

        natcasesort($listTranslation);//tri par ordre alphabéthique des features
        $allRoles = $this->roleRepository->findAll();
        foreach ($listTranslation as $item) {
            if ($this->featureRepository->findBy(['name' => $item]) == null) {
                $feature = new Feature();
                $feature->setControllerName(array_search($item, $listTranslation));
                $feature->setName($item);
                if ($rootRole == null && $this->roleRepository->findOneBy(['name' => 'ROLE_ROOT']) == null) {
                    $rootRole = new Role();
                    $rootRole->setName('ROLE_ROOT');
                    $this->entityManager->persist($rootRole);
                    $feature->addRole($rootRole);
                }
                foreach ($allRoles as $role) {
                    if (strtolower(substr($item, 0, 9)) == 'ajax_call') {
                        $feature->addRole($role);
                    }
                }
                $this->entityManager->persist($feature);
                $this->entityManager->flush();
            }
        }
        $output->writeln(file_exists($permissionFile));
        if (!file_exists($permissionFile)) {
            $roles = $this->roleRepository->findAll();
            $listFeature = [];
            foreach ($roles as $role) {
                $roleName = $role->getName();
                $rolesFeature = $role->getFeatures();
                foreach ($rolesFeature as $feature) {
                    $listFeature[$roleName][] = $feature->getControllerName();
                }
            }

            $fileSystem = new Filesystem();
            $fileSystem->dumpFile($permissionFile, json_encode($listFeature));
        }

        $output->writeln("Create permission matrix completed !");

        return 0;
    }
}