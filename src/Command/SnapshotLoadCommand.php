<?php


namespace App\Command;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;

class SnapshotLoadCommand extends Command
{
    protected static $defaultName = "app:snapshot:load";
    private $entityManager;
    private $kernel;

    /**
     * SnapshotLoadCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param KernelInterface $kernel
     */
    public function __construct(EntityManagerInterface $entityManager, KernelInterface $kernel)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->kernel = $kernel;
    }

    protected function configure()
    {
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws AnnotationException
     * @throws DBALException
     * @throws \ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* liste et ordre de sauvegarde des entités dans le fichier de configuration app/snapshotable-entities.json */

        $rootDir = $this->kernel->getProjectDir();
        if (!is_readable($rootDir . '/public/snapshots/')) {
            @mkdir($rootDir . '/public/snapshots/');
        }
        $snapshot_dir = $rootDir . '/public/snapshots/';
        $finder = new Finder();
        $finder->files()->in($snapshot_dir);

        if ($finder->hasResults()) {
            $files_in_dir = [];
            foreach ($finder as $file) {
                $files_in_dir[$file->getATime()] = $file->getFilename();
            }

            /*lecture du dernier fichier Json de sauvegarde*/
            $lastSnapshot = $files_in_dir[max(array_keys($files_in_dir))];
            $dataSnapshot = json_decode(file_get_contents($snapshot_dir . $lastSnapshot), true);

            /*récupération de la liste des entités sauvegardées à loader */
            $entityToLoad = $dataSnapshot['entitySaved'];

            /* récupération des données à remettre en base*/
            $dataToLoad = $dataSnapshot['data'];

            /*Parcours des tables (entités) à loader*/
            foreach ($entityToLoad as $entity => $classPath) {
                $tableName = $this->entityManager->getClassMetadata($classPath)->getTableName();
                $connexion = $this->entityManager->getConnection();

                /*Désérialisation des données dans le fichier JSON de SnapshotSaveCommand*/
                foreach (unserialize($dataToLoad[$entity]) as $indexObject => $valueObject) {

                    $annotationReader = new AnnotationReader();
                    $propertyAnnotations = new ReflectionClass($classPath);
                    $relationProperties = [];

                    /*Parcours de chaque propriété de l'entité courante*/
                    foreach ($propertyAnnotations->getProperties() as $property) {

                        /*On ne récupères  les propriétés (relations) ayant l'annotation ManyToOne */
                        $propertyAnnotation = $annotationReader->getPropertyAnnotations($propertyAnnotations->getProperty($property->getName()));
                        $relationProperties = array_filter(array_merge($relationProperties, $propertyAnnotation),
                            function ($element) {
                                if ($element instanceof ManyToOne || $element instanceof OneToOne) {
                                    return $element;
                                }
                            });
                    }

                    if ($relationProperties != []) {

                        /*Pour chaque propriété on récupère l'entité cible*/
                        foreach ($relationProperties as $relationProperty) {
                            $targetEntity = new ReflectionClass($relationProperty->targetEntity);

                            /*On parcours les propriétés de l'entité cible */
                            foreach ($targetEntity->getProperties() as $key => $targetEntityProperty) {
                                $targetProperties = $annotationReader->getPropertyAnnotations($targetEntity->getProperty($targetEntityProperty->getName()));

                                /*Pour chaque propriété de l'entité cible, on ne considère que ceux dont l'annotation UNIQUE = TRUE */
                                foreach ($targetProperties as $targetProperty) {

                                    if (property_exists(get_class($targetProperty), 'unique')) {
                                        /*on récupère de la base de données l'objet de l'entité cible répondant au critère des propriétés UNIQUE */
                                        if ($targetProperty->unique) {
                                            /*si l'entité cible est de même type que l'entité à loader (exemple Criterion dépend de Critérion)*/
                                            if ($targetEntity->getName() == $classPath) {
                                                $targetEntityToCheckIfAlreadyExists = $valueObject
                                                    ->{"get" . ucfirst($targetEntityProperty->getName())}();
                                                $setterMethod = "set" . ucfirst($targetEntityProperty->getName());

                                            } else {

                                                /*si l'entité cible n'est pas de même type que l'entité à loader (exemple Staff dépend d'Agenda)*/
                                                $targetEntityToCheckIfAlreadyExists = $valueObject
                                                    ->{"get" . substr($targetEntityProperty->class,
                                                        strrpos($targetEntityProperty->class, '\\') + 1)}()
                                                    ->{"get" . ucfirst($targetEntity->getProperty($targetEntityProperty->getName())->getName())}();
                                                $setterMethod = "set" . substr($targetEntityProperty->class,
                                                        strrpos($targetEntityProperty->class, '\\') + 1);
                                            }

                                            $objectFound = $this->entityManager->getRepository($targetEntityProperty->class)->findOneBy([
                                                $targetEntity->getProperty($targetEntityProperty->getName())->getName() => $targetEntityToCheckIfAlreadyExists
                                            ]);

                                            /*Si l'objet existe déjà dans la base de données, on l'associe à l'entité courante pour éviter de créer le même objet à l'insertion */
                                            if ($objectFound != null) {
                                                $valueObject->{$setterMethod}($objectFound);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    /*Si l'entité courante existe déjà dans la base de donnée, on la met à jour selon les informations du dernier SnapshotSaveCommand et des critères d'unicité des
                    propriétés UNIQUE des entités (relations) qui lui sont associées.*/
                    if ($this->entityManager->getRepository($classPath)->findOneBy(['id' => $valueObject->getId()]) !== null) {
                        $this->entityManager->merge($valueObject);
                    } else {
                        /*si l'entité n'existe pas dans la base de donnée, on l'insère telle qu'elle est dans SnapshotSaveCommand en désactivant temporairement l'auto-incrémentation
                         de l'Id à l'insertion*/
                        $connexion->exec("ALTER TABLE " . $tableName . " AUTO_INCREMENT = 0;");
                        $this->entityManager->getClassMetadata($classPath)->setIdGenerator(new AssignedGenerator());
                        $this->entityManager->getClassMetadata($classPath)->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
                        $this->entityManager->persist($valueObject);
                    }
                    $this->entityManager->flush();
                }
                $output->writeln("Entity " . $entity . " loaded");
            }
            $output->writeln("snapshot loaded !");
        } else {
            $output->writeln("Not found any snapshot file to load!!");
        }
    }
}