composer.lock: composer.json
	composer update --prefer-stable --no-interaction

vendor: composer.lock
	composer install --no-interaction --quiet

yarn.lock: package.json
	yarn upgrade --no-progress --non-interactive --silent

node_modules: yarn.lock
	yarn install --freeze-lock --check-files --no-progress --non-interactive --silent

.PHONY: db base-data dataset build test unit-test integration-test install install-dataset cache deploy
db:
	php bin/console doctrine:database:drop --if-exists --no-interaction --force --quiet
	php bin/console doctrine:database:create --no-interaction --quiet
	php bin/console doctrine:migration:migrate --no-interaction --no-ansi --quiet

base-data: db
	php bin/console app:restore
	php bin/console app:role

dataset: base-data
	php bin/console doctrine:fixtures:load --append

build: node_modules
	yarn build --silent

test:
	php bin/phpunit
	npm test

unit-test:
	php bin/phpunit --testsuite unit

integration-test:
	php bin/phpunit --testsuite integration

cache:
	php bin/console cache:clear --quiet --no-warmup
	php bin/console cache:warmup --quiet

install: vendor node_modules base-data build cache

install-dataset: install dataset

deploy: install
	php bin/console doctrine:fixtures:load --append --group canvas
	php bin/console doctrine:fixtures:load --append --group PersonAccess